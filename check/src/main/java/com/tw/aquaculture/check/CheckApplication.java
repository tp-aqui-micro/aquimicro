package com.tw.aquaculture.check;

import com.bizunited.platform.core.annotations.EnablePlatformCore;
import com.bizunited.platform.rbac.security.starter.annotations.EnableNebulaDefaultSecurity;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author chenr
 */
@SpringBootApplication(scanBasePackages = {"com.bizunited.platform.core","com.bizunited.platform.kuiper","com.tw.aquaculture"})
@EnableFeignClients
@EnableSwagger2
@EnableJpaRepositories(basePackages = {"com.bizunited.platform.core.repository", "com.tw.aquaculture.check.repository", "com.bizunited.platform.kuiper.starter.repository"})
@EntityScan(basePackages = {"com.bizunited.platform.core.entity", "com.bizunited.platform.kuiper.entity", "com.bizunited.platform.titan.entity", "com.bizunited.platform.proxima.entity", "com.tw.aquaculture.common.entity"})
@EnableNebulaDefaultSecurity
@EnablePlatformCore
public class CheckApplication {

  public static void main(String[] args) {
    SpringApplication.run(CheckApplication.class, args);
  }

  @Bean
  RestTemplate restTemplate(){
    return new RestTemplate();
  }
}
