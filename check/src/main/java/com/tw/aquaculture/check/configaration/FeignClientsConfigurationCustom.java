package com.tw.aquaculture.check.configaration;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * @author 陈荣
 * @date 2019/12/25 18:33
 */
@Configuration
@EnableFeignClients(basePackages = "com.tw.aquaculture.check.feign")
public class FeignClientsConfigurationCustom implements RequestInterceptor {

  @Override
  public void apply(RequestTemplate template) {

    RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
    if (requestAttributes == null) {
      return;
    }

    HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
    Enumeration<String> headerNames = request.getHeaderNames();
    if (headerNames != null) {
      while (headerNames.hasMoreElements()) {
        String name = headerNames.nextElement();
        Enumeration<String> values = request.getHeaders(name);
        while (values.hasMoreElements()) {
          String value = values.nextElement();
          template.header(name, value);
        }
      }
    }

  }
}
