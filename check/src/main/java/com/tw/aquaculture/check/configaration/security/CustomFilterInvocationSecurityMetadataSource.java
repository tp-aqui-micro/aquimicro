package com.tw.aquaculture.check.configaration.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;

/**
 * 匿名权限赋予
 * @author yinwenjie
 */
@Component("customFilterInvocationSecurityMetadataSource")
public class CustomFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {
  /**
   * 匿名用户鉴权体系
   */
  private static final SecurityConfig ANONYMOUS_CONFIG = new SecurityConfig("ANONYMOUS");
  
  /* (non-Javadoc)
   * @see org.springframework.security.access.SecurityMetadataSource#getAttributes(java.lang.Object)
   */
  @Override
  public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
    /*
     * 当前拿到的信息，直接赋予ANONYMOUS_CONFIG角色
     * 意思就是在业务中台的所有连接接口，目前皆可由匿名角色访问
     * */
    List<ConfigAttribute> configs = new ArrayList<>();
    configs.add(ANONYMOUS_CONFIG);
    return configs;
  }

  /* (non-Javadoc)
   * @see org.springframework.security.access.SecurityMetadataSource#getAllConfigAttributes()
   */
  @Override
  public Collection<ConfigAttribute> getAllConfigAttributes() {
    return Collections.emptyList();
  }

  /* (non-Javadoc)
   * @see org.springframework.security.access.SecurityMetadataSource#supports(java.lang.Class)
   */
  @Override
  public boolean supports(Class<?> clazz) {
    return true;
  }
}