package com.tw.aquaculture.check.controller.userextend;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.check.service.userextend.PositionExtendEntityService;
import com.tw.aquaculture.common.entity.userextend.PositionExtendEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 *   岗位信息 扩展信息同步hr系统
 * @author fangfu.luo
 * @date 2019/12/16
 */
@RestController
@RequestMapping("/v1/positionExtendEntitys")
@ApiModel("岗位信息 扩展信息同步hr系统")
public class PositionExtendEntityController extends BaseController {
  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(PositionExtendEntityController.class);
  
  @Autowired
  private PositionExtendEntityService positionExtendEntityService;

  @ApiOperation(value = "相关的创建过程，http接口。请注意该创建过程除了可以创建 PositionExtendEntity中的基本信息以外，" +
          "还可以对 positionExtendEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（ PositionExtendEntity）" +
          "模型的创建操作传入的 PositionExtendEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。" +
          "另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value="")
  public ResponseModel create(@RequestBody @ApiParam(name="PositionExtendEntity" , value="保存PositionExtendEntity") PositionExtendEntity positionExtendEntity) {
    try {
      PositionExtendEntity current = this.positionExtendEntityService.create(positionExtendEntity);
      return this.buildHttpResultW(current);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }
  @ApiOperation(value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，" +
          "包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，" +
          "但需要开发人员自行在Service层完善其更新过程注意：基于模型（PositionExtendEntity）的修改操作传入的PositionExtendEntityJSON对象，" +
          "其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value="/update")
  public ResponseModel update(@RequestBody @ApiParam(name="PositionExtendEntity" , value="修改PositionExtendEntity") PositionExtendEntity positionExtendEntity) {
    try {
      PositionExtendEntity current = this.positionExtendEntityService.update(positionExtendEntity);
      return this.buildHttpResultW(current);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "按照PositionExtendEntity的主键编号，查询指定的数据信息（不包括任何关联信息）")
  @GetMapping(value="/findById")
 public ResponseModel findById(@RequestParam("id") @ApiParam("主键") String id){
    try {
      PositionExtendEntity current = this.positionExtendEntityService.findById(id);
      return this.buildHttpResultW(current);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "查询所有 存在的岗位扩展信息")
  @GetMapping(value="/findAll")
  public ResponseModel findAll(){
    try {
      List<PositionExtendEntity> current = this.positionExtendEntityService.findAll();
      return this.buildHttpResultW(current);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = " 按照主键进行信息的真删除")
  @GetMapping(value="/deleteById")
  public void deleteById(@RequestParam("id") @ApiParam("主键") String id){
    try {
     this.positionExtendEntityService.deleteById(id);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
    }
  }

  @ApiOperation(value = "批量保存PositionExtendEntity信息")
  @GetMapping(value="/batchSave")
  public void batchSave(@ApiParam("需要保存的PositionExtendEntity信息") List<PositionExtendEntity> positionExtendEntityList){
    try {
      this.positionExtendEntityService.batchSave(positionExtendEntityList);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
    }
  }

  @ApiOperation(value = "批量修改PositionExtendEntity信息")
  @GetMapping(value="/batchUpdate")
  public void batchUpdate(@ApiParam("需要修改的PositionExtendEntity信息") List<PositionExtendEntity> positionExtendEntityList){
    try {
      this.positionExtendEntityService.batchUpdate(positionExtendEntityList);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
    }
  }
} 
