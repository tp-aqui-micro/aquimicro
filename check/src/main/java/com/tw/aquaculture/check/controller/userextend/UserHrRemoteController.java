package com.tw.aquaculture.check.controller.userextend;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.check.remote.UserHrRemoteService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陈荣
 * @date 2019/12/26 13:42
 */
@RestController
@RequestMapping(value="/v1/userHrRemote")
public class UserHrRemoteController extends BaseController {

  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(UserHrRemoteController.class);
  @Autowired
  private UserHrRemoteService userHrRemoteService;

  @ApiOperation(value = "查询所有 存在的用户扩展信息")
  @GetMapping(value="/pullAllMatters")
  public ResponseModel pullAllMatters(){
    try {
      this.userHrRemoteService.pullAllMatters();
      return this.buildHttpResult();
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }
}
