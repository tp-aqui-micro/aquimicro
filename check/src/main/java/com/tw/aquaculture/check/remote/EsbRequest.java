package com.tw.aquaculture.check.remote;

/**
 * esb 对接hr请求报文
 */
public class EsbRequest {

    private String tId="";

    private String bizTransactionID ="";

    private Integer count;

    private String consumer ="";

    private String srvLevel="";

    private String account="";

    private String pwd;

    private String transName="";

    private String  mode="";

    private String transDate="";

    private Integer fromNumber;

    private Integer toNumber;

    public String gettId() {
        return tId;
    }

    public void settId(String tId) {
        this.tId = tId;
    }

    public String getBizTransactionID() {
        return bizTransactionID;
    }

    public void setBizTransactionID(String bizTransactionID) {
        this.bizTransactionID = bizTransactionID;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getConsumer() {
        return consumer;
    }

    public void setConsumer(String consumer) {
        this.consumer = consumer;
    }

    public String getSrvLevel() {
        return srvLevel;
    }

    public void setSrvLevel(String srvLevel) {
        this.srvLevel = srvLevel;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getTransName() {
        return transName;
    }

    public void setTransName(String transName) {
        this.transName = transName;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public Integer getFromNumber() {
        return fromNumber;
    }

    public void setFromNumber(Integer fromNumber) {
        this.fromNumber = fromNumber;
    }

    public Integer getToNumber() {
        return toNumber;
    }

    public void setToNumber(Integer toNumber) {
        this.toNumber = toNumber;
    }
}
