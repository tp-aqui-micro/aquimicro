package com.tw.aquaculture.check.remote;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * 对接人力资源 webservice 帮助类
 * @author  fangfu.luo
 * @date 2019/12/13
 */
@Component
public class HrWadlRemoteUtil{
    //webservice 访问地址
    private static final String INVOICE_WS_URL = "http://fbcosb.tongwei.com/WP_FBC_SOA/APP_HR_SERVICE/Proxy_Services/Synchronous_ORG_HR_PS";
    //日志
    private static final Logger LOGGER = LoggerFactory.getLogger(HrWadlRemoteUtil.class);
    //尝试请求次数
    private  Integer count = 0;
    /**超时最多尝试次数*/
    private  static final Integer MAX_TIME = 3;
    /**超时最多尝试次数*/
    private  static final Integer ZERO = 0;
    //获取我们需要的组织机构数据过滤
    @Value("${findHrCondition.organization}")
    public String[] organization;

    /**
     * 拼接请求数据报文后提供给其它地方调用
     * @param esbRequest
     * @return
     */
    public StringBuilder exposeLoadWebsService(EsbRequest esbRequest){
        //拼接请求报文
        String sendMsg = appendXmlContext(esbRequest);
        return loadOvertimeWebService(sendMsg);
    }
    /**
     * 获取数据的总条数
     *
     * @param esbRequest 请求参数
     * @return 总条数
     */
    public Integer getTotalCount(EsbRequest esbRequest) {
        Integer totalCount = ZERO;
        esbRequest.setFromNumber(ZERO);
        esbRequest.setToNumber(ZERO);
        esbRequest.settId("INQUIRY");
        StringBuilder stringBuilderGetCount = exposeLoadWebsService(esbRequest);
        //处理返回数据
        if (StringUtils.isNotEmpty(stringBuilderGetCount)) {
            List<JSONObject> jsonObject = getFullData("return", stringBuilderGetCount);
            if (!CollectionUtils.isEmpty(jsonObject)) {
                totalCount = jsonObject.get(ZERO).getInteger("Count");
            }
        }
        return totalCount;
    }
    /**
     *   拼接数据请求报文
     */
    private  String appendXmlContext(EsbRequest esbRequest) {
        // 构建请求报文
        StringBuilder stringBuilder = new StringBuilder(
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:esb=\"http://esb.ekingwin.com\">" +
                        "   <soapenv:Header/>" +
                        "   <soapenv:Body>" +
                        "      <esb:REQUEST>" +
                        "         <esb:HEAD>" +
                        "            <esb:TID>"+esbRequest.gettId()+"</esb:TID>" +
                        "            <esb:BIZTransactionID>"+esbRequest.getBizTransactionID()+"</esb:BIZTransactionID>" +
                        "            <esb:Count>"+esbRequest.getCount()+"</esb:Count>" +
                        "            <esb:Consumer>"+esbRequest.getConsumer()+"</esb:Consumer>" +
                        "            <esb:SrvLevel>"+esbRequest.getSrvLevel()+"</esb:SrvLevel>" +
                        "            <esb:Account>"+esbRequest.getAccount()+"</esb:Account>" +
                        "            <esb:Password>"+esbRequest.getPwd()+"</esb:Password>" +
                        "            <esb:TransName>"+esbRequest.getTransName()+"</esb:TransName>" +
                        "            <esb:Mode>"+esbRequest.getMode()+"</esb:Mode>" +
                        "            <esb:TransDate>"+esbRequest.getTransDate()+"</esb:TransDate>" +
                        "            <esb:FromNumber>"+esbRequest.getFromNumber()+"</esb:FromNumber>" +
                        "            <esb:ToNumber>"+esbRequest.getToNumber()+"</esb:ToNumber>" +
                        "         </esb:HEAD>" +
                        "         <esb:test></esb:test>" +
                        "      </esb:REQUEST>" +
                        "   </soapenv:Body>" +
                        "</soapenv:Envelope>");
        return stringBuilder.toString();
    }
    /**
     *  如果请求失败则 再次发起请求，连续三次，失败则返null；
     * @param sendMsg
     * @return
     */
    private  StringBuilder loadOvertimeWebService(String sendMsg ){
        StringBuilder stringBuilder = null ;
        if(count < MAX_TIME) {
            try {
                stringBuilder = loadWebService(sendMsg);
                return stringBuilder;
            } catch (Exception e) {
                count ++ ;
                LOGGER.error(e.getMessage());
                loadOvertimeWebService(sendMsg);
            }
        }
        return stringBuilder;
    }


    /**
     * 加载WS
     * @param sendMsg 请求报文参数
     * @return 请求结果xml
     * @throws Exception
     */
    private  StringBuilder loadWebService(String sendMsg) throws IOException {
        // 开启HTTP连接ַ
        InputStreamReader isr = null;
        BufferedReader inReader = null;
        OutputStream outObject = null;
        StringBuilder result = null;
        try {
            URL url = new URL(INVOICE_WS_URL);
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            // 设置HTTP请求相关信息
            httpConn.setRequestProperty("Content-Length",
                    String.valueOf(sendMsg.getBytes().length));
            httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);

            // 进行HTTP请求
            outObject = httpConn.getOutputStream();
            outObject.write(sendMsg.getBytes());
            if (200 != (httpConn.getResponseCode())) {
                throw new IOException("HTTP Request is not success, Response code is " + httpConn.getResponseCode());
            }
            // 获取HTTP响应数据
            isr = new InputStreamReader(
                    httpConn.getInputStream(), "utf-8");
            inReader = new BufferedReader(isr);
            result = new StringBuilder();
            String inputLine;
            while ((inputLine = inReader.readLine()) != null) {
                result.append(inputLine);
            }
            return result;
        } finally {
            // 关闭输入流
            if (inReader != null) {
                inReader.close();
            }
            if (isr != null) {
                isr.close();
            }
            // 关闭输出流
            if (outObject != null) {
                outObject.close();
            }
        }

    }
    /**
     * 加载xml 文件
     * @param request xml 字符串
     * @return Document 文件
     * @throws Exception
     */
    private  Document loadXMLString(String request) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        try {
            db = dbf.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(request));
            return db.parse(is);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    /**
     *  xml 文件转换为 JSONArray 格式 文件
     * @param tagName 目标节点 名称
     * @param request 需要转换为xml字符串
     * @return 转换后的JSONArray
     * @throws Exception
     */
    public  List<JSONObject> getFullData(String tagName, StringBuilder request)  {
        List<JSONObject> resultArray = Lists.newArrayList();
        String xmlResult = request.toString().replace("<esb:", "<").replace("</esb:", "</");
        Document xmlDoc = loadXMLString(xmlResult);
        if(xmlDoc != null ) {
            NodeList nodeList = xmlDoc.getElementsByTagName(tagName);
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    NodeList childNodeList = nodeList.item(i).getChildNodes();
                    for (int j = 0; j < childNodeList.getLength(); j++) {
                        resultArray.add(setJsonObject(childNodeList.item(j).getChildNodes()));
                    }

                }
            }
        }
        return resultArray;
    }

    /**
     * 封装列为json
     * @param infoNodeList 需要封装的节点
     * @return
     */
    private JSONObject setJsonObject(  NodeList infoNodeList){
        JSONObject rootObject = new JSONObject();
        for (int k = 0; k < infoNodeList.getLength(); k++) {
            Node  node = infoNodeList.item(k);
            String value = "";
            if (node.getFirstChild() != null) {
                value = node.getFirstChild().getNodeValue();
            }
            rootObject.put(node.getNodeName(), value);
        }
        return rootObject;
    }
}
