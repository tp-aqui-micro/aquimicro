package com.tw.aquaculture.check.remote;



/**
 * 组织结构信息同步，
 * 人力资源管理系统 ->  本系统
 *
 * @author fangfu.luo
 * @date 2019/12/12
 */
public interface OrganizationHrRemoteService {

  /**
   * 组织机构同步对外调用 接口
   */
  void pullMatters() ;

}
