package com.tw.aquaculture.check.remote;

import com.alibaba.fastjson.JSONObject;
import com.bizunited.platform.core.entity.OrganizationEntity;
import com.google.common.collect.Lists;
import com.tw.aquaculture.check.service.remote.CodeGenerateService;
import com.tw.aquaculture.check.service.userextend.OrganizationExtendEntityService;
import com.tw.aquaculture.common.entity.userextend.OrganizationExtendEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.enums.OrgTypeEnmu;
import com.tw.aquaculture.common.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * 组织结构信息同步，
 * 人力资源管理系统 ->  本系统
 *
 * @author fangfu.luo
 * @date 2019/12/12
 */
@Service("organizationHrRemoteService")
public class OrganizationHrRemoteServiceImpl implements OrganizationHrRemoteService {
  @Autowired
  private HrWadlRemoteUtil hrWadlRemoteUtil;
  @Autowired
  private OrganizationExtendEntityService organizationExtendEntityService;
  @Autowired
  private CodeGenerateService codeGenerateService;
  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(OrganizationHrRemoteServiceImpl.class);
  //查询终止
  private static Integer toNumber = 1000;
  //判断需要进行部门层级关系组装的标识
  private static final String CHECK_TRANS_NAME = "D";
  //机构全称
  private static final String DESCR="DESCR";
  //机构简称
  private static final String DESCRSHORT="DESCRSHORT";
  //机构Id
  private static final String COMPANY="COMPANY";
  //部门Id
  private static final String DEPTID="DEPTID";
  //状态
  private static final String EFF_STATUS="EFF_STATUS";
  /**
   * 组织机构同步对外调用 接口
   */
  @Override
  public void pullMatters() {
    EsbRequest esbRequest = new EsbRequest();
    esbRequest.setBizTransactionID(UUID.randomUUID().toString());
    esbRequest.setMode("F");
    String date = DateUtils.format2String(DateUtils.currentTimestamp(), DateUtils.DEFAULT_DATE_SIMPLE_PATTERN);
    esbRequest.setTransDate(date);
    LOGGER.info("【查询数据库原有的组织机构信息开始】.....");
    Long startTimeQuery = System.currentTimeMillis();
    List<OrganizationExtendEntity> organizationExtendEntityLists = organizationExtendEntityService.findAll();
    Long endTimeQuery = System.currentTimeMillis();
    LOGGER.info("【查询数据库原有的组织机构信息结束】.....用时：{}S", ((endTimeQuery - startTimeQuery) / 1000));
    LOGGER.info("【同步公司到组织机构信息开始】.....");
    Long startTime = System.currentTimeMillis();
    //同步公司信息到机构
    synCompany(esbRequest, organizationExtendEntityLists);
    Long endTime = System.currentTimeMillis();
    LOGGER.info("【同步公司到组织机构信息结束】.....用时：{}S", ((endTime - startTime) / 1000));
    //同步部门信息到机构
    synDept(esbRequest, organizationExtendEntityLists);
    Long endTime2 = System.currentTimeMillis();
    LOGGER.info("【同步部门到组织机构信息结束】.....用时：{}S", ((endTime2 - endTime) / 1000));
  }

  /**
   * 同步hr的公司信息到我们的组织机构
   *
   * @param esbRequest 查询条件
   */
  private void synCompany(EsbRequest esbRequest, List<OrganizationExtendEntity> organizationExtendEntityLists) {
    esbRequest.setTransName("C");
    Integer totalCount = hrWadlRemoteUtil.getTotalCount(esbRequest);
    Integer allCount = totalCount / toNumber;
    List<OrganizationExtendEntity> organizationExtendEntitys = Lists.newArrayList();
    for (int i = 0; i <= allCount; i++) {
      esbRequest.setFromNumber(i * toNumber);
      esbRequest.setToNumber((i + 1) * toNumber - 1);
      List<OrganizationExtendEntity> organizationExtendEntityList = setOrganizationByCompany(esbRequest);
      organizationExtendEntitys.addAll(organizationExtendEntityList);
    }
    saveDuplicateRemovalInfo(organizationExtendEntitys, esbRequest.getTransName(), organizationExtendEntityLists);
  }

  /**
   * 同步hr的部门信息到我们的组织机构
   *
   * @param esbRequest 查询条件
   */
  private void synDept(EsbRequest esbRequest, List<OrganizationExtendEntity> organizationExtendEntityLists) {
    esbRequest.setTransName("D");
    Integer totalCount = hrWadlRemoteUtil.getTotalCount(esbRequest);
    Integer allCount = totalCount / toNumber;
    List<OrganizationExtendEntity> organizationExtendEntitys = Lists.newArrayList();
    for (int i = 0; i <= allCount; i++) {
      esbRequest.setFromNumber(i * toNumber);
      esbRequest.setToNumber((i + 1) * toNumber - 1);
      List<OrganizationExtendEntity> organizationExtendEntityList = setOrganizationByDept(esbRequest);
      organizationExtendEntitys.addAll(organizationExtendEntityList);
    }
    saveDuplicateRemovalInfo(organizationExtendEntitys, esbRequest.getTransName(), organizationExtendEntityLists);
  }

  /**
   * 请求公司json 数据组装为组织机构实体
   *
   * @param esbRequest 请求参数
   */
  private List<OrganizationExtendEntity> setOrganizationByCompany(EsbRequest esbRequest) {
    esbRequest.settId("GETCOMPANY");
    StringBuilder stringBuilder = hrWadlRemoteUtil.exposeLoadWebsService(esbRequest);
    List<OrganizationExtendEntity> list = Lists.newArrayList();
    //处理返回数据
    if (StringUtils.isNotEmpty(stringBuilder)) {
      List<JSONObject> jsonObject = hrWadlRemoteUtil.getFullData("COMPANYLIST", stringBuilder);
      if (!CollectionUtils.isEmpty(jsonObject)) {
        List<String> orgs = Arrays.stream(hrWadlRemoteUtil.organization).collect(Collectors.toList());
        jsonObject.stream().filter(f -> orgs.contains(f.getString(COMPANY)))
                .filter(f->Objects.equals(f.getString(EFF_STATUS),"A")).forEach(m -> {
          OrganizationEntity organizationEntity = new OrganizationEntity();
          organizationEntity.setCode(codeGenerateService.generateBusinessCode(CodeTypeEnum.ORGANIZATION_ENTITY));
          organizationEntity.setOrgName(m.getString(DESCR));
          organizationEntity.setType(OrgTypeEnmu.BASE_GROUP.getCode());
          organizationEntity.setCreateTime(DateUtils.getCurrentDate());
          organizationEntity.setDescription(m.getString(DESCRSHORT));
          organizationEntity.setId(m.getString(COMPANY));
          String effStatus = m.getString(EFF_STATUS);
          if (effStatus.equals("A")) {
            organizationEntity.setTstatus(EnableStateEnum.ENABLE.getState());
          } else {
            organizationEntity.setTstatus(EnableStateEnum.DISABLE.getState());
          }
          OrganizationExtendEntity organizationExtendEntity = new OrganizationExtendEntity();
          organizationExtendEntity.setDescrshort(m.getString(DESCRSHORT));
          organizationExtendEntity.setEffdt(m.getString("EFFDT"));
          organizationExtendEntity.setHrOrganizationId(m.getString(COMPANY));
          organizationExtendEntity.setTwAction(m.getString("TW_ACTION"));
          organizationExtendEntity.setTwOrgType(m.getString("TW_ORG_TYPE"));
          organizationExtendEntity.setOrganization(organizationEntity);
          organizationExtendEntity.setDescr(m.getString(DESCR));
          list.add(organizationExtendEntity);
        });
      }
    }
    return list;
  }

  /**
   * 请求部门json 数据组装为组织机构实体
   *
   * @param esbRequest 请求参数
   */
  private List<OrganizationExtendEntity> setOrganizationByDept(EsbRequest esbRequest) {
    esbRequest.settId("GETDEPT");
    StringBuilder stringBuilder = hrWadlRemoteUtil.exposeLoadWebsService(esbRequest);
    List<OrganizationExtendEntity> list = Lists.newArrayList();
    //处理返回数据
    if (StringUtils.isNotEmpty(stringBuilder)) {
      List<JSONObject> jsonObject = hrWadlRemoteUtil.getFullData("DEPTLIST", stringBuilder);
      if (!CollectionUtils.isEmpty(jsonObject)) {
        List<String> orgs = Arrays.stream(hrWadlRemoteUtil.organization).collect(Collectors.toList());
        jsonObject.stream()
                .filter(f->Objects.equals(f.getString(EFF_STATUS),"A"))
                .filter(f -> orgs.contains(f.getString(DEPTID))).forEach(m -> {
          OrganizationEntity organizationEntity = new OrganizationEntity();
          organizationEntity.setCode(codeGenerateService.generateCode(CodeTypeEnum.ORGANIZATION_ENTITY));
          organizationEntity.setOrgName(m.getString(DESCR));
          organizationEntity.setType(OrgTypeEnmu.BASE_GROUP.getCode());
          organizationEntity.setCreateTime(DateUtils.getCurrentDate());
          organizationEntity.setDescription(m.getString(DESCRSHORT));
          organizationEntity.setId(m.getString(DEPTID));
          String effStatus = m.getString(EFF_STATUS);
          if (effStatus.equals("A")) {
            organizationEntity.setTstatus(EnableStateEnum.ENABLE.getState());
          } else {
            organizationEntity.setTstatus(EnableStateEnum.DISABLE.getState());
          }
          OrganizationExtendEntity organizationExtendEntity = new OrganizationExtendEntity();
          organizationExtendEntity.setDescrshort(m.getString(DESCRSHORT));
          organizationExtendEntity.setEffdt(m.getString("EFFDT"));
          organizationExtendEntity.setHrOrganizationId(m.getString(DEPTID));
          organizationExtendEntity.setTwAction(m.getString("TW_ACTION"));
          organizationExtendEntity.setTwOrgType(m.getString("TW_ORG_TYPE"));
          organizationExtendEntity.setOrganization(organizationEntity);
          organizationExtendEntity.setDescr(m.getString(DESCR));
          organizationExtendEntity.setTwDescr2(m.getString("DESCR2"));
          organizationExtendEntity.setTwItwStatus(m.getString("TW_ITW_STATUS"));
          organizationExtendEntity.setTwUpDeptId(m.getString("TW_UP_DEPTID"));
          organizationExtendEntity.setCompany(m.getString(COMPANY));
          organizationExtendEntity.setTwDeptType(m.getString("TW_DEPT_TYPE"));
          organizationExtendEntity.setTwDeptProperty(m.getString("TW_DEPT_PROPERTY"));
          list.add(organizationExtendEntity);
        });
      }
    }
    return list;
  }

  /**
   * 数据出去数据库已经存在的信息，进行增量或是修改操作的数据
   * 保存到数据库
   *
   * @param organizationExtendEntitys 需要保存的数据
   * @patam transName 进入方法的规则 D部门 C 公司
   */
  private void saveDuplicateRemovalInfo(List<OrganizationExtendEntity> organizationExtendEntitys, String transName, List<OrganizationExtendEntity> organizationExtendEntityLists) {
    if (!CollectionUtils.isEmpty(organizationExtendEntitys)) {
      if (CollectionUtils.isEmpty(organizationExtendEntityLists)) {
        //组装部门层级关系
        if (CHECK_TRANS_NAME.equals(transName)) {
          setDeptHierarchy(organizationExtendEntitys);
        }
        organizationExtendEntityService.batchSave(organizationExtendEntitys);
      } else {
        List<String> hrOrganizationIds = organizationExtendEntityLists.stream().map(OrganizationExtendEntity::getHrOrganizationId).collect(Collectors.toList());
        //获取需要添加的数据
        List<OrganizationExtendEntity> addOrganizationExtendEntitys = organizationExtendEntitys.stream().filter(organizationExtendEntity ->
                !hrOrganizationIds.contains(organizationExtendEntity.getHrOrganizationId())).collect(Collectors.toList());
        //获取需要修改的数据
        List<OrganizationExtendEntity> updateOrganizationExtendEntitys = organizationExtendEntitys.stream().map(m ->
                organizationExtendEntityLists.stream()
                        .filter(l -> Objects.equals(m.getHrOrganizationId(),l.getHrOrganizationId()))
                        .filter(l -> !Objects.equals(m.getDescrshort(),l.getDescrshort())
                                || !Objects.equals(m.getEffdt(),l.getEffdt())
                                || !Objects.equals(m.getTwAction(),l.getTwAction())
                                || !Objects.equals(m.getTwOrgType(),l.getTwOrgType())
                                || !Objects.equals(m.getDescr(),l.getDescr())
                                || !Objects.equals(m.getOrganization().getTstatus(),l.getOrganization().getTstatus())
                                || !Objects.equals(m.getOrganization().getDescription(),l.getOrganization().getDescription())
                                || !Objects.equals(m.getOrganization().getOrgName(),l.getOrganization().getOrgName())
                        ).findFirst()
                        .map(l -> {
                          l.setDescrshort(m.getDescrshort());
                          l.setDescr(m.getDescr());
                          l.setEffdt(m.getEffdt());
                          l.setTwOrgType(m.getTwOrgType());
                          l.setTwAction(m.getTwAction());
                          l.getOrganization().setTstatus(m.getOrganization().getTstatus());
                          l.getOrganization().setDescription(m.getOrganization().getDescription());
                          l.getOrganization().setOrgName(m.getOrganization().getOrgName());
                          return l;
                        }).orElse(m)).collect(Collectors.toList());
        if (CHECK_TRANS_NAME.equals(transName)) {
          setDeptCompany(addOrganizationExtendEntitys, organizationExtendEntityLists);
        }
        organizationExtendEntityService.batchSave(addOrganizationExtendEntitys);
        organizationExtendEntityService.batchUpdate(updateOrganizationExtendEntitys);
      }
    }
  }

  /**
   * 组装部门层级关系
   *
   * @param organizationExtendEntitys     获取到的信息
   * @param organizationExtendEntityLists 数据库中已经存在的机构信息
   * @return 组装部门层级的组织关系
   */
  private void setDeptCompany(List<OrganizationExtendEntity> organizationExtendEntitys, List<OrganizationExtendEntity> organizationExtendEntityLists) {
    setDeptHierarchy(organizationExtendEntitys);
    if (!CollectionUtils.isEmpty(organizationExtendEntityLists)) {
      Map<String, OrganizationEntity> organizationEntityMap = organizationExtendEntityLists.stream()
              .collect(Collectors.toMap(OrganizationExtendEntity::getHrOrganizationId, OrganizationExtendEntity::getOrganization));
      organizationExtendEntitys.forEach(m -> {
        //将部门挂载在公司下
        if (StringUtils.isEmpty(m.getTwUpDeptId()) && StringUtils.isNotEmpty(m.getCompany())) {
          m.getOrganization().setParent(organizationEntityMap.get(m.getCompany()));
          //如果数据库存在先挂载数据库的
        }else if(StringUtils.isNotEmpty(m.getTwUpDeptId()) && m.getOrganization().getParent() == null){
          m.getOrganization().setParent(organizationEntityMap.get(m.getTwUpDeptId()));
        }
      });
    }
  }

  /**
   * 组装部门层级关系
   *
   * @param organizationExtendEntitys 获取到的信息
   * @return 处理层级后的 组织机构
   */
  private void setDeptHierarchy(List<OrganizationExtendEntity> organizationExtendEntitys) {
    Map<String, OrganizationEntity> organizationEntityMap = organizationExtendEntitys.stream().collect(Collectors.toMap(OrganizationExtendEntity::getHrOrganizationId, OrganizationExtendEntity::getOrganization));
    organizationExtendEntitys.forEach(m -> {
      if (StringUtils.isNotEmpty(m.getTwUpDeptId())) {
        m.getOrganization().setParent(organizationEntityMap.get(m.getTwUpDeptId()));
      }
    });
  }
}
