package com.tw.aquaculture.check.remote;

import com.tw.aquaculture.common.entity.userextend.PositionExtendEntity;

import java.util.List;

/**
 *  岗位信息同步，
 * 人力资源管理系统 ->  本系统
 *
 * @author fangfu.luo
 * @date 2019/12/16
 */
public interface PositionHrRemoteService {

  /**
   * 组织机构同步对外调用 接口
   */
  void pullMatters();
  /**
   * 获取所有hr系统的岗位
   * @return 所有hr系统的岗位
   */
  List<PositionExtendEntity> getAllPositionExtendEntity();
  /**
   * 数据出去数据库已经存在的信息，进行增量或是修改操作的数据
   * 保存到数据库
   *
   * @param positionExtendEntities 需要保存的数据
   */
  void saveDuplicateRemovalInfo(List<PositionExtendEntity> positionExtendEntities);
}
