package com.tw.aquaculture.check.remote;

import com.alibaba.fastjson.JSONObject;
import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.core.entity.PositionEntity;
import com.bizunited.platform.core.entity.RoleEntity;
import com.bizunited.platform.core.entity.UserEntity;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.tw.aquaculture.check.service.remote.CodeGenerateService;
import com.tw.aquaculture.check.service.userextend.PositionExtendEntityService;
import com.tw.aquaculture.check.service.userextend.RoleExtendEntityService;
import com.tw.aquaculture.common.entity.userextend.PositionExtendEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toCollection;

/**
 *  岗位信息同步，
 * 人力资源管理系统 ->  本系统
 *
 * @author fangfu.luo
 * @date 2019/12/16
 */
@Service("positionHrRemoteService")
public class PositionHrRemoteServiceImpl implements  PositionHrRemoteService {
  @Autowired
  private HrWadlRemoteUtil hrWadlRemoteUtil;
  @Autowired
  private PositionExtendEntityService positionExtendEntityService;
  @Autowired
  private CodeGenerateService codeGenerateService;
  @Autowired
  private RoleExtendEntityService roleExtendEntityService;
  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(PositionHrRemoteServiceImpl.class);
  //查询终止
  private static Integer toNumber = 1000;

  /**
   * 组织机构同步对外调用 接口
   */
  @Override
  public void pullMatters() {
    LOGGER.info("【同步岗位信息开始】.....");
    Long startTime = System.currentTimeMillis();
    //同步hr岗位到我们的岗位信息
    List<PositionExtendEntity> positionExtendEntitys = getAllPositionExtendEntity();
    saveDuplicateRemovalInfo(positionExtendEntitys);
    Long endTime = System.currentTimeMillis();
    LOGGER.info("【同步岗位信息结束】.....用时：{}S", ((endTime - startTime) / 1000));

  }

  /**
   * 获取所有hr系统的岗位
   *
   * @return 所有hr系统的岗位
   */
  @Override
  public List<PositionExtendEntity> getAllPositionExtendEntity() {
    EsbRequest esbRequest = new EsbRequest();
    esbRequest.setBizTransactionID(UUID.randomUUID().toString());
    esbRequest.setMode("F");
    String date = DateUtils.format2String(DateUtils.currentTimestamp(), DateUtils.DEFAULT_DATE_SIMPLE_PATTERN);
    esbRequest.setTransDate(date);
    esbRequest.setTransName("J");
    Integer totalCount = hrWadlRemoteUtil.getTotalCount(esbRequest);
    Integer allCount = totalCount / toNumber;
    List<PositionExtendEntity> positionExtendEntitys = Lists.newArrayList();
    for (int i = 0; i <= allCount; i++) {
      esbRequest.setFromNumber(i * toNumber);
      esbRequest.setToNumber((i + 1) * toNumber - 1);
      List<PositionExtendEntity> positionExtendEntityList = setPositionByPosition(esbRequest);
      positionExtendEntitys.addAll(positionExtendEntityList);
    }
    return positionExtendEntitys;
  }

  /**
   * 请求公司json 数据组装为岗位实体
   *
   * @param esbRequest 请求参数
   */
  private List<PositionExtendEntity> setPositionByPosition(EsbRequest esbRequest) {
    esbRequest.settId("GETJOBCODE");
    StringBuilder stringBuilder = hrWadlRemoteUtil.exposeLoadWebsService(esbRequest);
    List<PositionExtendEntity> positionExtendEntitylist = Lists.newArrayList();
    //处理返回数据
    if (StringUtils.isNotEmpty(stringBuilder)) {
      List<JSONObject> jsonObject = hrWadlRemoteUtil.getFullData("JOBCODELIST", stringBuilder);
      if (!CollectionUtils.isEmpty(jsonObject)) {
        jsonObject.stream()
                .filter(f -> Objects.equals(f.getString("EFF_STATUS"), "A")).forEach(m -> {
          PositionEntity positionEntity = new PositionEntity();
          positionEntity.setCode(codeGenerateService.generateBusinessCode(CodeTypeEnum.POSITION_ENTITY));
          positionEntity.setCreateTime(DateUtils.getCurrentDate());
          positionEntity.setName(m.getString("DESCR"));
          String effStatus = m.getString("EFF_STATUS");
          if (effStatus.equals("A")) {
            positionEntity.setTstatus(EnableStateEnum.ENABLE.getState());
          } else {
            positionEntity.setTstatus(EnableStateEnum.DISABLE.getState());
          }
          PositionExtendEntity positionExtendEntity = new PositionExtendEntity();
          positionExtendEntity.setDescrshort(m.getString("DESCRSHORT"));
          positionExtendEntity.setEffdt(m.getString("EFFDT"));
          positionExtendEntity.setPosition(positionEntity);
          positionExtendEntity.setTwAction(m.getString("TW_ACTION"));
          positionExtendEntity.setTwOrgType(m.getString("TW_ORG_TYPE"));
          positionExtendEntity.setHrPositionId(m.getString("JOBCODE"));
          positionExtendEntity.setJobFamily(m.getString("JOB_FAMILY"));
          positionExtendEntity.setJobFunction(m.getString("JOB_FUNCTION"));
          positionExtendEntity.setJobSubFunc(m.getString("JOB_SUB_FUNC"));
          positionExtendEntity.setMedChkupReq(m.getString("MED_CHKUP_REQ"));
          positionExtendEntity.setDescr(m.getString("DESCR"));
          positionExtendEntitylist.add(positionExtendEntity);
        });
      }
    }
    return positionExtendEntitylist;
  }

  /**
   * 数据出去数据库已经存在的信息，进行增量或是修改操作的数据
   * 保存到数据库
   *
   * @param positionExtendEntities 需要保存的数据
   */
  @Override
  public void saveDuplicateRemovalInfo(List<PositionExtendEntity> positionExtendEntities) {
    if (!CollectionUtils.isEmpty(positionExtendEntities)) {
      List<PositionExtendEntity> positionExtendEntityLists = positionExtendEntityService.findAll();
      if (CollectionUtils.isEmpty(positionExtendEntityLists)) {
        //同步角色
        synRole(positionExtendEntities);
        positionExtendEntityService.batchSave(positionExtendEntities);
      } else {
        List<String> hrPositionIds = positionExtendEntityLists.stream().map(PositionExtendEntity::getHrPositionId).collect(Collectors.toList());
        //获取需要添加的数据
        List<PositionExtendEntity> addPositionExtendEntitys = positionExtendEntities.stream().filter(positionExtendEntity ->
                !hrPositionIds.contains(positionExtendEntity.getHrPositionId())).collect(Collectors.toList());
        //删除用户已经存在需要添加的人员的岗位；
        addPositionExtendEntitys.forEach(m ->
                positionExtendEntityLists.forEach(u -> {
                  List<String> newUser = m.getPosition().getUsers().stream().map(UserEntity::getId).collect(Collectors.toList());
                  List<UserEntity> oldUser = Lists.newArrayList(u.getPosition().getUsers());
                  oldUser.stream().filter(item -> newUser.contains(item.getId())).collect(Collectors.toList()).forEach(del -> positionExtendEntityService.deletePositionById(u.getId()));
                })
        );
        //获取需要修改的数据
        List<PositionExtendEntity> updatePositionExtendEntitys = positionExtendEntities.stream().map(m ->
                positionExtendEntityLists.stream()
                        .filter(l -> Objects.equals(m.getHrPositionId(), l.getHrPositionId()))
                        .filter(l -> !Objects.equals(m.getDescrshort(), l.getDescrshort())
                                || !Objects.equals(m.getEffdt(), l.getEffdt())
                                || !Objects.equals(m.getTwAction(), l.getTwAction())
                                || !Objects.equals(m.getTwOrgType(), l.getTwOrgType())
                                || !Objects.equals(m.getPosition().getTstatus(), l.getPosition().getTstatus())
                        ).findFirst()
                        .map(l -> {
                          l.setDescrshort(m.getDescrshort());
                          l.setDescr(m.getDescr());
                          l.setEffdt(m.getEffdt());
                          l.setTwOrgType(m.getTwOrgType());
                          l.setTwAction(m.getTwAction());
                          l.getPosition().setTstatus(m.getPosition().getTstatus());
                          return l;
                        }).orElse(m)).collect(Collectors.toList());

        //同步角色
        synRole(addPositionExtendEntitys);
        positionExtendEntityService.batchSave(addPositionExtendEntitys);
        positionExtendEntityService.batchUpdate(updatePositionExtendEntitys);
      }
    }
  }

  /**
   * 同步岗位信息新增到角色
   *
   * @param positionExtendEntities 岗位信息
   */
  private void synRole(List<PositionExtendEntity> positionExtendEntities) {
    //角色添加
    if (!CollectionUtils.isEmpty(positionExtendEntities)) {
      LOGGER.info("【同步岗位信息到角色开始】.....");
      Long startTime = System.currentTimeMillis();
      //角色对应的组织机构
      Map<String, Set<OrganizationEntity>> organizationEntityMap = Maps.newHashMap();
      //角色对应的人员
      Map<String, Set<UserEntity>> userEntityMap = Maps.newHashMap();
      setMap(positionExtendEntities,organizationEntityMap,userEntityMap);
      List<RoleEntity> roleEntityList = positionExtendEntities.stream()
              .filter(f -> f.getPosition() != null).map(m -> {
                RoleEntity roleEntity = new RoleEntity();
                roleEntity.setRoleCode(codeGenerateService.generateBusinessCode(CodeTypeEnum.ROLE_ENTITY));
                roleEntity.setComment(m.getDescrshort());
                roleEntity.setCreateDate(DateUtils.getCurrentDate());
                roleEntity.setRoleName(m.getDescr());
                roleEntity.setTstatus(m.getPosition().getTstatus());
                Set<OrganizationEntity> organizationEntitySet = organizationEntityMap.get(m.getDescr());
                if(!CollectionUtils.isEmpty(organizationEntitySet)){
                  Set<OrganizationEntity> organizationEntitys = organizationEntitySet.stream().collect(Collectors.collectingAndThen(toCollection(() -> new TreeSet<>(Comparator.comparing(OrganizationEntity::getId))), HashSet::new));
                  roleEntity.setOrganizations(organizationEntitys);
                }
                Set<UserEntity> userEntitySet = userEntityMap.get(m.getDescr());
                if(!CollectionUtils.isEmpty(userEntitySet)) {
                  Set<UserEntity> userUsers = userEntitySet.stream().collect(Collectors.collectingAndThen(toCollection(() -> new TreeSet<>(Comparator.comparing(UserEntity::getId))), HashSet::new));
                  roleEntity.setUsers(userUsers);
                }
                roleEntity.setRoleType("T001");
                return roleEntity;
              }).collect(Collectors.toList());
      List<RoleEntity> roleEntities = roleExtendEntityService.findAll();
      if (!CollectionUtils.isEmpty(roleEntities)) {
        List<String> roleNames = roleEntities.stream().map(RoleEntity::getRoleName).collect(Collectors.toList());
        roleEntityList = roleEntityList.stream().filter(f -> !roleNames.contains(f.getRoleName())).collect(Collectors.toList());
      }
      //根据名称去除重复角色
      roleEntityList = roleEntityList.stream()
              .collect(Collectors.collectingAndThen(toCollection(() -> new TreeSet<>(Comparator.comparing(RoleEntity::getRoleName))), ArrayList::new));

      List<RoleEntity> roleEntities1 = roleExtendEntityService.batchSave(roleEntityList);
      roleEntities1.addAll(roleEntities);
      //绑定角色与岗位之间的关系
      positionExtendEntities.forEach(m ->
              roleEntities1.forEach(n -> {
                if (Objects.equals(m.getDescr(), n.getRoleName())) {
                  Set<RoleEntity> roleEntitySet = Sets.newHashSet(n);
                  m.getPosition().setRoles(roleEntitySet);
                }
              }));
      Long endTime = System.currentTimeMillis();
      LOGGER.info("【同步岗位信息到角色结束】.....用时：{}S", ((endTime - startTime) / 1000));
    }
  }

  /**
   *  提取角色对应的组织机构 角色对应的人员
   * @param positionExtendEntities 岗位转换为角色
   * @param organizationEntityMap 角色对应的组织机构
   * @param userEntityMap 角色对应的 人员
   */
  private void setMap(List<PositionExtendEntity> positionExtendEntities, Map<String, Set<OrganizationEntity>> organizationEntityMap, Map<String, Set<UserEntity>> userEntityMap){
    positionExtendEntities.forEach(f->{
      Set<OrganizationEntity> organizationEntitySet = organizationEntityMap.get(f.getDescr());
      if (CollectionUtils.isEmpty(organizationEntitySet)) {
        organizationEntitySet = Sets.newHashSet();
        organizationEntityMap.put(f.getDescr(), organizationEntitySet);
      }
      organizationEntitySet.add(f.getPosition().getOrganization());
      Set<UserEntity> userEntitySet = userEntityMap.get(f.getDescr());
      if (CollectionUtils.isEmpty(userEntitySet)) {
        userEntitySet = Sets.newHashSet();
        userEntityMap.put(f.getDescr(), userEntitySet);
      }
      userEntitySet.addAll(f.getPosition().getUsers());
    });
  }
}
