package com.tw.aquaculture.check.remote;

/**
 * 用户信息同步，
 * 人力资源管理系统 ->  本系统
 *
 * @author fangfu.luo
 * @date 2019/12/12
 */
public interface UserHrRemoteService {
  /**
   * 用户同步对外调用 接口
   */
   void pullMatters();

    /**
     * hr系统同步组织机构岗位角色用户信息对外调用 接口
     */
    void pullAllMatters();

}
