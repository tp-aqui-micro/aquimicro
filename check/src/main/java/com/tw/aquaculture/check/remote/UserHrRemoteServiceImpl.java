package com.tw.aquaculture.check.remote;

import com.alibaba.fastjson.JSONObject;
import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.core.entity.PositionEntity;
import com.bizunited.platform.core.entity.UserEntity;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.tw.aquaculture.check.service.remote.CodeGenerateService;
import com.tw.aquaculture.check.service.userextend.OrganizationExtendEntityService;
import com.tw.aquaculture.check.service.userextend.UserExtendEntityService;
import com.tw.aquaculture.common.entity.userextend.OrganizationExtendEntity;
import com.tw.aquaculture.common.entity.userextend.PositionExtendEntity;
import com.tw.aquaculture.common.entity.userextend.UserExtendEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toCollection;

/**
 * 用户信息同步，
 * 人力资源管理系统 ->  本系统
 *
 * @author fangfu.luo
 * @date 2019/12/12
 */
@Service("userHrRemoteService")
public class UserHrRemoteServiceImpl implements UserHrRemoteService{
  @Autowired
  private HrWadlRemoteUtil hrWadlRemoteUtil;
  @Autowired
  private UserExtendEntityService userExtendEntityService;
  @Autowired
  private OrganizationExtendEntityService organizationExtendEntityService;
  @Autowired
  private PositionHrRemoteService positionHrRemoteService;
  @Autowired
  private CodeGenerateService codeGenerateService;
  @Autowired
  @Qualifier("passwordEncoder")
  @Lazy
  private PasswordEncoder passwordEncoder;

  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(UserHrRemoteServiceImpl.class);
  //查询终止
  private static Integer toNumber = 2000;
  //默认密码设置
  private static final String DEFAULT = "12345678";
  //用户集合获取
  private static final String USEREXTENDENTITYS = "userExtendEntitys";
  //岗位集合信息
  private static final String SAVEPOSITIONEXTENDENTITY = "savePositionExtendEntity";

  /**
   * 用户同步对外调用 接口
   */
  @Override
  public void pullMatters() {
    LOGGER.info("【同步用户岗位角色信息开始】.....");
    Long startTime = System.currentTimeMillis();
    //同步公司信息到机构
    sysUserExtendEntity();
    Long endTime = System.currentTimeMillis();
    LOGGER.info("【同步用户岗位角色信息结束】.....用时：{}S",((endTime-startTime)/1000));
  }
  /**
   * hr系统同步组织机构岗位角色用户信息对外调用 接口
   */
  @Override
  @Transactional
  public void pullAllMatters(){
    //组织机构同步
    organizationExtendEntityService.pullMatters();
    //用户岗位角色同步
    this.pullMatters();
  }
  /**
   * 同步hr的用户信息到我们的用户
   */
  private void sysUserExtendEntity(){
    EsbRequest esbRequest = new EsbRequest();
    esbRequest.setBizTransactionID(UUID.randomUUID().toString());
    esbRequest.setMode("F");
    String date = DateUtils.format2String(DateUtils.currentTimestamp(), DateUtils.DEFAULT_DATE_SIMPLE_PATTERN);
    esbRequest.setTransDate(date);
    esbRequest.setTransName("P");
    Integer totalCount = hrWadlRemoteUtil.getTotalCount(esbRequest);
    Integer allCount = totalCount / toNumber;
    //获取所有组织机构
    List<OrganizationExtendEntity> organizationExtendEntities = organizationExtendEntityService.findAll();
    List<PositionExtendEntity> positionExtendEntities = positionHrRemoteService.getAllPositionExtendEntity();
    List<UserExtendEntity> userExtendEntities = Lists.newArrayList();
    for (int i = 0; i <= allCount; i++) {
      esbRequest.setFromNumber(i * toNumber);
      esbRequest.setToNumber((i + 1) * toNumber - 1);
      userExtendEntities.addAll(setUserByUser(esbRequest));
    }
    saveDuplicateRemovalInfo(userExtendEntities,organizationExtendEntities,positionExtendEntities);

  }

  /**
   * 请求公司json 数据组装为组织机构实体
   *
   * @param esbRequest 请求参数
   */
  private List<UserExtendEntity> setUserByUser(EsbRequest esbRequest) {
    esbRequest.settId("GETPERSON");
    StringBuilder stringBuilder = hrWadlRemoteUtil.exposeLoadWebsService(esbRequest);
    List<UserExtendEntity> list = Lists.newArrayList();
    //处理返回数据
    if (StringUtils.isNotEmpty(stringBuilder)) {
      List<JSONObject> jsonObject = hrWadlRemoteUtil.getFullData("PERSONLIST", stringBuilder);
      if (!CollectionUtils.isEmpty(jsonObject)) {
        List<String> orgs = Arrays.stream(hrWadlRemoteUtil.organization).collect(Collectors.toList());
        jsonObject.stream().filter(f -> orgs.contains(f.getString("DEPTID")))
                .filter(f->Objects.equals(f.getString("HR_STATUS"),"A"))
                .filter(f-> StringUtils.isNotEmpty(f.getString("TW_USERACCOUNT"))).forEach(m -> {
          UserEntity userEntity = new UserEntity();
          userEntity.setId(m.getString("EMPLID"));
          userEntity.setCreateTime(DateUtils.getCurrentDate());
          String sex =  m.getString("SEX");
          userEntity.setGender(toSex(sex));
          userEntity.setPhone(m.getString("MOBILE_PHONE"));
          userEntity.setEntryTime(m.getDate("ORG_INST_SRV_DT"));
          userEntity.setAccount(m.getString("TW_USERACCOUNT").toLowerCase());
          userEntity.setUserName(m.getString("NAME"));
          String effStatus = m.getString("HR_STATUS");
          //默认状态为禁用
          userEntity.setUseStatus(EnableStateEnum.DISABLE.getState());
//          if (effStatus.equals("A")) {
//            userEntity.setUseStatus(EnableStateEnum.ENABLE.getState());
//          } else {
//            userEntity.setUseStatus(EnableStateEnum.DISABLE.getState());
//          }
          userEntity.setPassword(passwordEncoder.encode(DEFAULT));
          UserExtendEntity userExtendEntity = new UserExtendEntity();
          userExtendEntity.setUser(userEntity);
          userExtendEntity.setHrUserId(m.getString("EMPLID"));
          userExtendEntity.setHrEmplRcd(m.getInteger("EMPL_RCD"));
          userExtendEntity.setHrEffdt(m.getString("EFFDT"));
          userExtendEntity.setHrEffseq(m.getInteger("EFFSEQ"));
          userExtendEntity.setHrJobIndicator(m.getString("JOB_INDICATOR"));
          userExtendEntity.setHrNameAc(m.getString("NAME_AC"));
          userExtendEntity.setHrBirthdate(m.getDate("BIRTHDATE"));
          userExtendEntity.setHrMarStatus(m.getString("MAR_STATUS"));
          userExtendEntity.setHrJpmDescr90(m.getString("JPM_DESCR90"));
          userExtendEntity.setHrJpmText2541(m.getString("JPM_TEXT254_1"));
          userExtendEntity.setHrNationalIdType(m.getString("NATIONAL_ID_TYPE"));
          userExtendEntity.setHrNationalId(m.getString("NATIONAL_ID"));
          userExtendEntity.setHrEmailAddr(m.getString("EMAIL_ADDR"));
          userExtendEntity.setHrPhone(m.getString("PHONE"));
          userExtendEntity.setHrSeniorityPayDt(m.getDate("SENIORITY_PAY_DT"));
          userExtendEntity.setHrCompany(m.getString("COMPANY"));
          userExtendEntity.setHrDeptid(m.getString("DEPTID"));
          userExtendEntity.setHrTwDeptLine(m.getString("TW_DEPT_LINE"));
          userExtendEntity.setHrDescr(m.getString("DESCR"));
          userExtendEntity.setHrTwDeptProperty(m.getString("TW_DEPT_PROPERTY"));
          userExtendEntity.setHrDescr1(m.getString("DESCR1"));
          userExtendEntity.setHrAction(m.getString("ACTION"));
          userExtendEntity.setHrDescr3(m.getString("DESCR3"));
          userExtendEntity.setHrPositionEntryDt(m.getString("POSITION_ENTRY_DT"));
          userExtendEntity.setHrPositionNbr(m.getString("POSITION_NBR"));
          userExtendEntity.setHrDescr4(m.getString("DESCR4"));
          userExtendEntity.setHrJobcode(m.getString("JOBCODE"));
          userExtendEntity.setHrDescr5(m.getString("DESCR5"));
          userExtendEntity.setHrLocation(m.getString("LOCATION"));
          userExtendEntity.setHrTwJobLevel(m.getString("TW_JOB_LEVEL"));
          userExtendEntity.setHrDescr6(m.getString("DESCR6"));
          userExtendEntity.setHrTwPosIncharge(m.getString("TW_POS_INCHARGE"));
          userExtendEntity.setHrUnionCd(m.getString("UNION_CD"));
          userExtendEntity.setHrFlag(m.getString("FLAG"));
          userExtendEntity.setHrMedChkupReq(m.getString("MED_CHKUP_REQ"));
          userExtendEntity.setHrJobFunction(m.getString("JOB_FUNCTION"));
          userExtendEntity.setHrDescr7(m.getString("DESCR7"));
          userExtendEntity.setHrJobSubFunc(m.getString("JOB_SUB_FUNC"));
          userExtendEntity.setHrDescr8(m.getString("DESCR8"));
          userExtendEntity.setHrEmplClass(m.getString("EMPL_CLASS"));
          userExtendEntity.setHrDescr2(m.getString("DESCR_2"));
          userExtendEntity.setHrSupervisorId(m.getString("SUPERVISOR_ID"));
          userExtendEntity.setHrTwAction(m.getString("TW_ACTION"));
          userExtendEntity.setHrTwComDept(m.getString("TW_COM_DEPT"));
          userExtendEntity.setHrLastName(m.getString("LAST_NAME"));
          userExtendEntity.setHrFirstName(m.getString("FIRST_NAME"));
          userExtendEntity.setHrTwAweLine(m.getString("TW_AWE_LINE"));
          userExtendEntity.setHrTwAweLineDescr(m.getString("TW_AWE_LINE_DESCR"));
          userExtendEntity.setHrTwWxId(m.getString("TW_WX_ID"));
          userExtendEntity.setHrExtension(m.getString("EXTENSION"));
          list.add(userExtendEntity);
        });
      }
    }
    return list;
  }

  /**
   * 数据出去数据库已经存在的信息，进行增量或是修改操作的数据
   * 保存到数据库
   *
   * @param userExtendEntityInfo 需要保存的数据
   */
  @SuppressWarnings("unchecked")
  private void saveDuplicateRemovalInfo(List<UserExtendEntity> userExtendEntityInfo, List<OrganizationExtendEntity> organizationExtendEntities,  List<PositionExtendEntity> positionExtendEntities) {
    if (!CollectionUtils.isEmpty(userExtendEntityInfo)) {
      Map<String, Object> map = setPositionHierarchy(userExtendEntityInfo,organizationExtendEntities,positionExtendEntities);
      List<UserExtendEntity>  userExtendEntitys = (List<UserExtendEntity>)map.get(USEREXTENDENTITYS);
      if (!CollectionUtils.isEmpty(userExtendEntitys)) {
        LOGGER.info("【同步用户信息开始】.....");
        Long startTime2 = System.currentTimeMillis();
        List<UserExtendEntity> userExtendEntityLists = userExtendEntityService.findAll();
        if (CollectionUtils.isEmpty(userExtendEntityLists)) {
          userExtendEntityService.batchSave(userExtendEntitys);
        } else {
          List<String> hrUserIds = userExtendEntityLists.stream().map(UserExtendEntity::getHrUserId).collect(Collectors.toList());
          List<String> userAccounts = userExtendEntityLists.stream().map(m -> m.getUser().getAccount().toLowerCase()).collect(Collectors.toList());
          //获取需要添加的数据
          List<UserExtendEntity> addUserExtendEntitys = userExtendEntitys.stream().filter(userExtendEntity ->
                  !hrUserIds.contains(userExtendEntity.getHrUserId())).filter(userExtendEntity ->
                  !userAccounts.contains(userExtendEntity.getUser().getAccount())).collect(Collectors.toList());
          //获取需要修改的数据
          List<UserExtendEntity> updateUserExtendEntitys = userExtendEntitys.stream().map(m ->
                  userExtendEntityLists.stream()
                          .filter(l -> Objects.equals(m.getHrUserId(), l.getHrUserId())
                                  ||Objects.equals(m.getUser().getAccount(), l.getUser().getAccount()))
                          .filter(l -> !Objects.equals(m.getHrPhone(), l.getHrPhone())
                                  || !Objects.equals(m.getHrDeptid(), l.getHrDeptid())
                                  || !Objects.equals(m.getHrCompany(), l.getHrCompany())
                                  || !Objects.equals(m.getHrJobcode(), l.getHrJobcode())
                                  || !Objects.equals(m.getHrSupervisorId(), l.getHrSupervisorId())
                                  || !Objects.equals(m.getUser().getAccount(), l.getUser().getAccount())
                                  || !Objects.equals(m.getUser().getUserName(), l.getUser().getUserName())
                                  || !Objects.equals(m.getUser().getEntryTime(), l.getUser().getEntryTime())
                                  || !Objects.equals(m.getUser().getPhone(), l.getUser().getPhone())
                                  || !Objects.equals(m.getUser().getGender(), l.getUser().getGender())
                                  || !Objects.equals(m.getUser().getUseStatus(), l.getUser().getUseStatus())
                          ).findFirst()
                          .map(l -> {
                            l.setHrPhone(m.getHrPhone());
                            l.setHrDeptid(m.getHrDeptid());
                            l.setHrCompany(m.getHrCompany());
                            l.setHrJobcode(m.getHrJobcode());
                            l.setHrSupervisorId(m.getHrSupervisorId());
                            l.getUser().setAccount(m.getUser().getAccount());
                            l.getUser().setUserName(m.getUser().getUserName());
                            l.getUser().setEntryTime(m.getUser().getEntryTime());
                            l.getUser().setPhone(m.getUser().getPhone());
                            l.getUser().setGender(m.getUser().getGender());
                            l.getUser().setUseStatus(m.getUser().getUseStatus());
                            return l;
                          }).orElse(m)).collect(Collectors.toList());
          userExtendEntityService.batchSave(addUserExtendEntitys);
          userExtendEntityService.batchUpdate(updateUserExtendEntitys);
        }
        Long endTime2 = System.currentTimeMillis();
        LOGGER.info("【同步用户信息结束】.....用时：{}S" ,((endTime2-startTime2)/1000));
      }
      //存储岗位信息到数据库
      Map<String,PositionExtendEntity> savePositionExtendEntity = (Map<String,PositionExtendEntity>)map.get(SAVEPOSITIONEXTENDENTITY);
      if(!CollectionUtils.isEmpty(savePositionExtendEntity)){
        LOGGER.info("【同步岗位信息开始】.....");
        Long startTime = System.currentTimeMillis();
        savePositionExtendEntity(savePositionExtendEntity);
        Long endTime = System.currentTimeMillis();
        LOGGER.info("【同步岗位信息结束】.....用时：{}S" ,((endTime-startTime)/1000));
      }
    }
  }

  /**
   * 组装岗位层级关系 并修改数据库的岗位层级关系
   * @param userExtendEntitys 获取到的信息
   * @return 绑定岗位后的用户扩展集合
   */
  private Map<String,Object>  setPositionHierarchy(List<UserExtendEntity> userExtendEntitys,List<OrganizationExtendEntity> organizationExtendEntities,  List<PositionExtendEntity> positionExtendEntities ) {
    //数据库存在的user
    List<UserEntity> userEntityLists = userExtendEntityService.findUserAll();
    userExtendEntitys.forEach(m->
            userEntityLists.forEach(p->{
              if(!Objects.equals(p.getId(),m.getUser().getId()) && Objects.equals(p.getPhone(),m.getUser().getPhone())){
                m.getUser().setPhone(null);
              }
            }));
    //去除重复用户信息
    List<UserExtendEntity> userExtendEntitieList = userExtendEntitys.stream()
            .collect(Collectors.collectingAndThen(toCollection(() -> new TreeSet<>(Comparator.comparing(o->o.getHrJobcode()+"#"+o.getHrUserId()+"#"+o.getHrDeptid()))), ArrayList::new));
    Map<String,Object> map = Maps.newHashMap();
    //组织机构id+岗位Code 对应的岗位信息
    Map<String, PositionExtendEntity> orgPositionCodeToPositionMap = Maps.newHashMap();
    if (CollectionUtils.isEmpty(positionExtendEntities)) {
      map.put(USEREXTENDENTITYS,userExtendEntitieList);
      map.put(SAVEPOSITIONEXTENDENTITY,orgPositionCodeToPositionMap);
      return map;
    }

    Map<String, OrganizationExtendEntity> organizationNameMap = organizationExtendEntities.stream().collect(Collectors.toMap(OrganizationExtendEntity::getHrOrganizationId, m -> m));
    Map<String, PositionExtendEntity> positionExtendEntityMap = positionExtendEntities.stream().collect(Collectors.toMap(PositionExtendEntity::getHrPositionId, m -> m));
    //人对应的岗位Code
    Map<String, List<String>> userToPositionMap = Maps.newHashMap();
    //获取人对应的岗位 岗位对应的人
    userExtendEntitieList.forEach(m -> {
      List<String> userToPositionList = userToPositionMap.get(m.getHrUserId());
      if (CollectionUtils.isEmpty(userToPositionList)) {
        userToPositionList = Lists.newArrayList();
      }
      userToPositionList.add(m.getHrJobcode());
      userToPositionMap.put(m.getHrUserId(), userToPositionList);
    });
    userExtendEntitieList.forEach(m ->setPosition( positionExtendEntityMap.get(m.getHrJobcode()),m,orgPositionCodeToPositionMap,organizationNameMap));
    //最终储存数据去重；
    //去除重复用户信息
    userExtendEntitieList = userExtendEntitys.stream()
            .collect(Collectors.collectingAndThen(toCollection(() -> new TreeSet<>(Comparator.comparing(UserExtendEntity::getHrUserId))), ArrayList::new));
    map.put(USEREXTENDENTITYS,userExtendEntitieList);
    map.put(SAVEPOSITIONEXTENDENTITY,orgPositionCodeToPositionMap);
    return map;
  }

  /**
   * 设置每个人对应的岗位关系与人的关系
   * @param positionExtendEntity 人员对应的岗位岗位
   * @param m 需要添加的人员
   * @param orgPositionCodeToPositionMap  组织机构对应的岗位 Map
   * @param organizationNameMap 组织机构名称Map
   */
  private void setPosition(PositionExtendEntity positionExtendEntity, UserExtendEntity m, Map<String,PositionExtendEntity> orgPositionCodeToPositionMap,Map<String, OrganizationExtendEntity> organizationNameMap) {
    //绑定组织机构
    OrganizationEntity organizationEntity;
    if (StringUtils.isNotEmpty(m.getHrDeptid())) {
      organizationEntity = organizationNameMap.get(m.getHrDeptid()).getOrganization();
    } else {
      organizationEntity = organizationNameMap.get(m.getHrCompany()).getOrganization();
    }
    PositionExtendEntity positionExtendEntity2 = orgPositionCodeToPositionMap.get(m.getHrDeptid()+positionExtendEntity.getHrPositionId());
    if(positionExtendEntity2 == null ){
      positionExtendEntity2 = new PositionExtendEntity();
      BeanUtils.copyProperties(positionExtendEntity,positionExtendEntity2,"position");
      PositionEntity positionEntity = new PositionEntity();
      BeanUtils.copyProperties(positionExtendEntity.getPosition(),positionEntity,"users","organization");
      positionExtendEntity2.setPosition(positionEntity);
      orgPositionCodeToPositionMap.put(m.getHrDeptid()+positionExtendEntity.getHrPositionId(),positionExtendEntity2);
    }
    positionExtendEntity2.getPosition().setOrganization(organizationEntity);
    Set<OrganizationEntity> orgs = m.getUser().getOrgs();
    if (!CollectionUtils.isEmpty(orgs)) {
      orgs.add(organizationEntity);
      Set<OrganizationEntity> orgSet = orgs.stream().collect(Collectors.collectingAndThen(toCollection(() -> new TreeSet<>(Comparator.comparing(OrganizationEntity::getId))), HashSet::new));
      m.getUser().setOrgs(orgSet);
    } else {
      orgs = Sets.newHashSet();
      orgs.add(organizationEntity);
      m.getUser().setOrgs(orgs);
    }
    Set<UserEntity> users = positionExtendEntity2.getPosition().getUsers();
    if (!CollectionUtils.isEmpty(users)) {
      users.add(m.getUser());
      Set<UserEntity> positionUsers = users.stream().collect(Collectors.collectingAndThen(toCollection(() -> new TreeSet<>(Comparator.comparing(UserEntity::getId))), HashSet::new));
      positionExtendEntity2.getPosition().setUsers(positionUsers);
    } else {
      users = Sets.newHashSet();
      users.add(m.getUser());
      positionExtendEntity2.getPosition().setUsers(users);
    }
  }

  /**
   * 组装岗位信息positionExtendEntity 保存到数据库  一个人一个岗位
   * @param savePositionExtendEntity  需要保存的岗位信息
   */
  private void  savePositionExtendEntity( Map<String,PositionExtendEntity> savePositionExtendEntity){
    if(!CollectionUtils.isEmpty(savePositionExtendEntity)) {
      List<PositionExtendEntity> positionExtendEntities = Lists.newArrayList();
      savePositionExtendEntity.forEach((k,m)-> {
        if(m.getPosition().getUsers() !=null){
          List<UserEntity> list = Lists.newArrayList( m.getPosition().getUsers());
          for (int i = 0; i<list.size();i++){
            PositionExtendEntity positionExtendEntity = new PositionExtendEntity();
            BeanUtils.copyProperties(m,positionExtendEntity,"position");
            positionExtendEntity.setId(null);
            PositionEntity positionEntity = new PositionEntity();
            BeanUtils.copyProperties(m.getPosition(),positionEntity,"users");
            positionEntity.setId(null);
            DecimalFormat df=new DecimalFormat("00");
            String name = positionEntity.getName()+df.format(i+1L);
            if(name.length()>64){
              name = name.substring(name.length()-64);
            }
            name = positionEntity.getOrganization().getOrgName() +name;
            positionEntity.setName(name);
            positionEntity.setCode(codeGenerateService.generateBusinessCode(CodeTypeEnum.POSITION_ENTITY));
            Set<UserEntity> users= Sets.newHashSet(list.get(i));
            positionEntity.setUsers(users);
            positionExtendEntity.setPosition(positionEntity);
            positionExtendEntities.add(positionExtendEntity);
          }
        }else{
          m.setId(null);
          m.getPosition().setId(null);
          String name = m.getPosition().getName();
          name =  m.getPosition().getOrganization().getOrgName() +name;
          m.getPosition().setName(name);
          positionExtendEntities.add(m);
        }
      });
      positionHrRemoteService.saveDuplicateRemovalInfo(positionExtendEntities);
    }
  }
  /**
   *   性别转换
   */
  private Integer toSex(String sex) {
    Integer gender = 0;
    switch (sex) {
      case "F":
        gender = 2;
        break;
      case "M":
        gender = 1;
        break;
      default:
        gender = 0;
        break;
    }
    return gender;
  }
}
