package com.tw.aquaculture.check.repository;

import com.tw.aquaculture.common.entity.check.CheckFishEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * CheckFishEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_CheckFishEntityRepository")
public interface CheckFishEntityRepository
    extends
      JpaRepository<CheckFishEntity, String>
      ,JpaSpecificationExecutor<CheckFishEntity>
  {

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct checkFishEntity from CheckFishEntity checkFishEntity "
      + " left join fetch checkFishEntity.pond checkFishEntity_pond "
      + " left join fetch checkFishEntity_pond.base base "
      + " left join fetch checkFishEntity.fishType checkFishEntity_fishType "
      + " where checkFishEntity.id=:id ")
  public CheckFishEntity findDetailsById(@Param("id") String id);
  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   * @param formInstanceId 表单实例编号
   * */
  @Query("select distinct checkFishEntity from CheckFishEntity checkFishEntity "
      + " left join fetch checkFishEntity.pond checkFishEntity_pond "
      + " left join fetch checkFishEntity_pond.base base "
      + " left join fetch checkFishEntity.fishType checkFishEntity_fishType "
      + " where checkFishEntity.formInstanceId=:formInstanceId ")
  public CheckFishEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);
  /**
   * 按照表单实例编号进行查询
   * @param formInstanceId 表单实例编号
   * */
  @Query(" from CheckFishEntity f where f.formInstanceId = :formInstanceId")
  public CheckFishEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);



}