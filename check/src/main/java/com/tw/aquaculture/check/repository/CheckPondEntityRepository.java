package com.tw.aquaculture.check.repository;

import com.tw.aquaculture.common.entity.check.CheckPondEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * CheckPondEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_CheckPondEntityRepository")
public interface CheckPondEntityRepository
        extends
        JpaRepository<CheckPondEntity, String>
        , JpaSpecificationExecutor<CheckPondEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct checkPondEntity from CheckPondEntity checkPondEntity "
          + " left join fetch checkPondEntity.pond checkPondEntity_pond "
          + " left join fetch checkPondEntity_pond.base base "
          + " where checkPondEntity.id=:id ")
  public CheckPondEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct checkPondEntity from CheckPondEntity checkPondEntity "
          + " left join fetch checkPondEntity.pond checkPondEntity_pond "
          + " left join fetch checkPondEntity_pond.base base "
          + " where checkPondEntity.formInstanceId=:formInstanceId ")
  public CheckPondEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from CheckPondEntity f where f.formInstanceId = :formInstanceId")
  public CheckPondEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据塘口和用户姓名查询当天巡塘次数
   * @param pondId
   * @param username
   * @param dayStart
   * @return
   */
  @Query("select count(*) from CheckPondEntity checkPondEntity "
          + " left join checkPondEntity.pond checkPondEntity_pond "
          + " where checkPondEntity_pond.id=:pondId and checkPondEntity.createName=:username and checkPondEntity.createTime>=:dayStart ")
  int findCountByPondAndUserDay(@Param("pondId") String pondId, @Param("username") String username, @Param("dayStart") Date dayStart);

  /**
   * 根据塘口和用户姓名查询当月巡塘次数
   * @param pondId
   * @param username
   * @param monthStart
   * @return
   */
  @Query("select count(*) from CheckPondEntity checkPondEntity "
          + " left join checkPondEntity.pond checkPondEntity_pond "
          + " where checkPondEntity_pond.id=:pondId and checkPondEntity.createName=:username and checkPondEntity.createTime>=:monthStart ")
  int findCountByPondAndUserMonth(@Param("pondId") String pondId, @Param("username") String username, @Param("monthStart") Date monthStart);

  /**
   * 根据塘口id和时间区间查询数量
   * @param pondId
   * @param start
   * @return
   */
  @Query("select count(*) from CheckPondEntity checkPondEntity "
          + " left join checkPondEntity.pond checkPondEntity_pond "
          + " where checkPondEntity_pond.id=:pondId and checkPondEntity.createTime>=:start ")
  int findCountByPondIdAndTimes(String pondId, Date start);

  /**
   * 根据塘口查询按时间倒序排序
   * @param pondId
   * @return
   */
  @Query("select checkPondEntity from CheckPondEntity checkPondEntity "
          + " left join fetch checkPondEntity.pond checkPondEntity_pond "
          + " where checkPondEntity_pond.id=:pondId order by checkPondEntity.createTime desc ")
  List<CheckPondEntity> findLastCheckPondByPond(@Param("pondId") String pondId);
}