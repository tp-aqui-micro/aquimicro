package com.tw.aquaculture.check.repository;

import com.tw.aquaculture.common.entity.check.CheckTaskBackEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * CheckTaskBackEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_CheckTaskBackEntityRepository")
public interface CheckTaskBackEntityRepository
    extends
      JpaRepository<CheckTaskBackEntity, String>
      ,JpaSpecificationExecutor<CheckTaskBackEntity>
  {
  /**
   * 按照临时任务进行详情查询（包括关联信息）
   * @param checkTask 临时任务
   * */
  @Query("select distinct checkTaskBackEntity from CheckTaskBackEntity checkTaskBackEntity "
      + " left join fetch checkTaskBackEntity.checkTask checkTaskBackEntity_checkTask "
      + " left join fetch checkTaskBackEntity.user checkTaskBackEntity_user "
       + " where checkTaskBackEntity_checkTask.id = :id")
  public Set<CheckTaskBackEntity> findDetailsByCheckTask(@Param("id") String id);
  /**
   * 按照用户进行详情查询（包括关联信息）
   * @param user 用户
   * */
  @Query("select distinct checkTaskBackEntity from CheckTaskBackEntity checkTaskBackEntity "
      + " left join fetch checkTaskBackEntity.checkTask checkTaskBackEntity_checkTask "
      + " left join fetch checkTaskBackEntity.user checkTaskBackEntity_user "
       + " where checkTaskBackEntity_user.id = :id")
  public Set<CheckTaskBackEntity> findDetailsByUser(@Param("id") String id);

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct checkTaskBackEntity from CheckTaskBackEntity checkTaskBackEntity "
      + " left join fetch checkTaskBackEntity.checkTask checkTaskBackEntity_checkTask "
      + " left join fetch checkTaskBackEntity.user checkTaskBackEntity_user "
      + " where checkTaskBackEntity.id=:id ")
  public CheckTaskBackEntity findDetailsById(@Param("id") String id);



}