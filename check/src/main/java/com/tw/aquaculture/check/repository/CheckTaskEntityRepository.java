package com.tw.aquaculture.check.repository;

import com.tw.aquaculture.common.entity.check.CheckTaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * CheckTaskEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_CheckTaskEntityRepository")
public interface CheckTaskEntityRepository
    extends
      JpaRepository<CheckTaskEntity, String>
      ,JpaSpecificationExecutor<CheckTaskEntity>
  {

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct checkTaskEntity from CheckTaskEntity checkTaskEntity "
      + " left join fetch checkTaskEntity.pond checkTaskEntity_pond "
      + " left join fetch checkTaskEntity_pond.base base "
      + " left join fetch checkTaskEntity.receiveUser checkTaskEntity_receiveUser "
      + " left join fetch checkTaskEntity.checkTaskBacks checkTaskEntity_checkTaskBacks "
      + " where checkTaskEntity.id=:id ")
  public CheckTaskEntity findDetailsById(@Param("id") String id);
  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   * @param formInstanceId 表单实例编号
   * */
  @Query("select distinct checkTaskEntity from CheckTaskEntity checkTaskEntity "
      + " left join fetch checkTaskEntity.pond checkTaskEntity_pond "
          + " left join fetch checkTaskEntity_pond.base base "
      + " left join fetch checkTaskEntity.receiveUser checkTaskEntity_receiveUser "
      + " left join fetch checkTaskEntity.checkTaskBacks checkTaskEntity_checkTaskBacks "
      + " where checkTaskEntity.formInstanceId=:formInstanceId ")
  public CheckTaskEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);
  /**
   * 按照表单实例编号进行查询
   * @param formInstanceId 表单实例编号
   * */
  @Query(" from CheckTaskEntity f where f.formInstanceId = :formInstanceId")
  public CheckTaskEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);



}