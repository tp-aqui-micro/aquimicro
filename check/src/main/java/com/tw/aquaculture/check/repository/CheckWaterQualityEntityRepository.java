package com.tw.aquaculture.check.repository;

import com.tw.aquaculture.common.entity.check.CheckWaterQualityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * CheckWaterQualityEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_CheckWaterQualityEntityRepository")
public interface CheckWaterQualityEntityRepository
        extends
        JpaRepository<CheckWaterQualityEntity, String>
        , JpaSpecificationExecutor<CheckWaterQualityEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct checkWaterQualityEntity from CheckWaterQualityEntity checkWaterQualityEntity "
          + " left join fetch checkWaterQualityEntity.pond checkWaterQualityEntity_pond "
          + " left join fetch checkWaterQualityEntity_pond.base base "
          + " where checkWaterQualityEntity.id=:id ")
  public CheckWaterQualityEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct checkWaterQualityEntity from CheckWaterQualityEntity checkWaterQualityEntity "
          + " left join fetch checkWaterQualityEntity.pond checkWaterQualityEntity_pond "
          + " left join fetch checkWaterQualityEntity_pond.base base "
          + " where checkWaterQualityEntity.formInstanceId=:formInstanceId ")
  public CheckWaterQualityEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from CheckWaterQualityEntity f where f.formInstanceId = :formInstanceId")
  public CheckWaterQualityEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据塘口id查询最后一次检测
   * @param pondId
   * @return
   */
  @Query("select distinct checkWaterQualityEntity from CheckWaterQualityEntity checkWaterQualityEntity "
          + " left join fetch checkWaterQualityEntity.pond checkWaterQualityEntity_pond "
          + " where checkWaterQualityEntity_pond.id=:pondId order by checkWaterQualityEntity.createTime desc nulls last ")
  List<CheckWaterQualityEntity> findLastCheckWaterByPondId(@Param("pondId") String pondId);
}