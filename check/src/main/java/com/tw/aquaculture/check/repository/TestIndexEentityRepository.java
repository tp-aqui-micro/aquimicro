package com.tw.aquaculture.check.repository;

import com.tw.aquaculture.common.entity.base.TestIndexEentity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * TestIndexEentity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_TestIndexEentityRepository")
public interface TestIndexEentityRepository
    extends
      JpaRepository<TestIndexEentity, String>
      ,JpaSpecificationExecutor<TestIndexEentity>
  {

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct testIndexEentity from TestIndexEentity testIndexEentity "
      + " where testIndexEentity.id=:id ")
  public TestIndexEentity findDetailsById(@Param("id") String id);
  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   * @param formInstanceId 表单实例编号
   * */
  @Query("select distinct testIndexEentity from TestIndexEentity testIndexEentity "
      + " where testIndexEentity.formInstanceId=:formInstanceId ")
  public TestIndexEentity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);
  /**
   * 按照表单实例编号进行查询
   * @param formInstanceId 表单实例编号
   * */
  @Query(" from TestIndexEentity f where f.formInstanceId = :formInstanceId")
  public TestIndexEentity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);



}