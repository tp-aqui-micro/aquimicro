package com.tw.aquaculture.check.repository.userextend;

import com.tw.aquaculture.common.entity.userextend.OrganizationExtendEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * UserExtendEntity 业务模型的数据库方法支持 机构扩展属性模块
 *
 * @author saturn
 */
@Repository("_OrganizationExtendEntityRepository")
public interface OrganizationExtendEntityRepository extends JpaRepository<OrganizationExtendEntity, String>
        , JpaSpecificationExecutor<OrganizationExtendEntity> {

    List<OrganizationExtendEntity>  findByHrOrganizationIdIn(List<String> hrOrganizationIds);




}