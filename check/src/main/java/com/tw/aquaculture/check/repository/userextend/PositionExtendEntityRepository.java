package com.tw.aquaculture.check.repository.userextend;

import com.tw.aquaculture.common.entity.userextend.PositionExtendEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * PositionExtendEntity 业务模型的数据库方法支持 岗位扩展属性模块
 *
 * @author saturn
 */
@Repository("_PositionExtendEntityRepository")
public interface PositionExtendEntityRepository extends JpaRepository<PositionExtendEntity, String>
        , JpaSpecificationExecutor<PositionExtendEntity> {
    List<PositionExtendEntity> findByHrPositionIdIn(List<String> hrPositionIds);
    PositionExtendEntity findByPositionId(String positionId);
}