package com.tw.aquaculture.check.repository.userextend;

import com.tw.aquaculture.common.entity.userextend.UserExtendEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * UserExtendEntity 业务模型的数据库方法支持 用户扩展属性模块
 *
 * @author saturn
 */
@Repository("_UserExtendEntityRepository")
public interface UserExtendEntityRepository extends JpaRepository<UserExtendEntity, String>
        , JpaSpecificationExecutor<UserExtendEntity> {
    UserExtendEntity findByUserId(String userId);
}