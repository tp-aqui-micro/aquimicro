package com.tw.aquaculture.check.repository.userextend.internal;

import com.tw.aquaculture.common.entity.userextend.OrganizationExtendEntity;

import java.util.List;

/**
 * PositionExtendEntity 业务模型的数据库方法支持 岗位扩展属性模块
 *
 * @author fangfu.luo
 */
public interface ToOrganizationExtendEntityRepository {
    /**
     * 批量保存信息
     * @param organizationExtendEntityList 需要保存的 数据
     */

    void batchSave(List<OrganizationExtendEntity> organizationExtendEntityList);

}