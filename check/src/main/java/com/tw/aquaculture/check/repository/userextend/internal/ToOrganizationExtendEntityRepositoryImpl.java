package com.tw.aquaculture.check.repository.userextend.internal;

import com.bizunited.platform.core.entity.OrganizationEntity;
import com.tw.aquaculture.common.entity.userextend.OrganizationExtendEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author fangfu.luo
 * @date 2019/12/18
 */
@Repository("toOrganizationExtendEntityRepositoryImpl")
public class ToOrganizationExtendEntityRepositoryImpl implements ToOrganizationExtendEntityRepository {

  @Autowired
  private EntityManager entityManager;

  @Override
  @Transactional
  public void batchSave(List<OrganizationExtendEntity> organizationExtendEntityList) {
    StringBuilder hql = new StringBuilder("INSERT INTO `engine_org`(`id`, `org_code`, `create_time`, `org_desc`, `org_name`, `org_sort_index`, `tstatus`, `org_type`, `parent_id`) VALUES ");
    hql.append("(?,?,?,?,?,?,?,?,?)");
    Query query = entityManager.createNativeQuery(hql.toString());
    StringBuilder hql2 = new StringBuilder(" INSERT INTO `tw_organization_extend`(`id`, `hr_descrshort`, `hr_effdt`, `hr_organization_id`, `hr_tw_action`, `hr_tw_org_type`, `organization_id`, `hr_company`");
    hql2.append(" , `hr_tw_dept_type`, `hr_tw_descr2`, `hr_tw_itw_status`, `hr_tw_dept_property`, `hr_tw_up_deptid`, `hr_descr`) VALUES ");
    hql2.append("(uuid(),?,?,?,?,?,?,?,?,?,?,?,?,?)");
    Query query2 = entityManager.createNativeQuery(hql2.toString());
    organizationExtendEntityList.forEach(m -> {
      OrganizationEntity organizationEntity = m.getOrganization();
      query.setParameter(1, organizationEntity.getId());
      query.setParameter(2, organizationEntity.getCode());
      query.setParameter(3, organizationEntity.getCreateTime());
      query.setParameter(4, organizationEntity.getDescription());
      query.setParameter(5, organizationEntity.getOrgName());
      query.setParameter(6, organizationEntity.getSortIndex());
      query.setParameter(7, organizationEntity.getTstatus());
      query.setParameter(8, organizationEntity.getType());
      if (organizationEntity.getParent() != null) {
        query.setParameter(9, organizationEntity.getParent().getId());
      } else {
        query.setParameter(9, null);
      }
      query.executeUpdate();
      query2.setParameter(1, m.getDescrshort());
      query2.setParameter(2, m.getEffdt());
      query2.setParameter(3, m.getHrOrganizationId());
      query2.setParameter(4, m.getTwAction());
      query2.setParameter(5, m.getTwOrgType());
      query2.setParameter(6,organizationEntity.getId());
      query2.setParameter(7, m.getCompany());
      query2.setParameter(8, m.getTwDeptType());
      query2.setParameter(9, m.getTwDescr2());
      query2.setParameter(10,m.getTwItwStatus());
      query2.setParameter(11,m.getTwDeptProperty());
      query2.setParameter(12,m.getTwUpDeptId());
      query2.setParameter(13,m.getDescr());
      query2.executeUpdate();
    });
  }
}
