package com.tw.aquaculture.check.repository.userextend.internal;

import com.tw.aquaculture.common.entity.userextend.PositionExtendEntity;

import java.util.List;

/**
 * PositionExtendEntity 业务模型的数据库方法支持 岗位扩展属性模块
 *
 * @author fangfu.luo
 */
public interface ToPositionExtendEntityRepository {
    /**
     * 批量保存信息
     * @param positionExtendEntityList 需要保存的 数据
     */

    void batchSave(List<PositionExtendEntity> positionExtendEntityList);

    /**
     * 批量修改信息
     * @param positionExtendEntityList  需要修改的数据库
     */
    List<PositionExtendEntity>  batchUpdate(List<PositionExtendEntity> positionExtendEntityList);

}