package com.tw.aquaculture.check.repository.userextend.internal;

import com.bizunited.platform.core.entity.PositionEntity;
import com.tw.aquaculture.common.entity.userextend.PositionExtendEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.Iterator;
import java.util.List;

/**
 * @author fangfu.luo
 * @date 2019/12/18
 */
@Repository("toPositionExtendEntityRepositoryImpl")
public class ToPositionExtendEntityRepositoryImpl implements ToPositionExtendEntityRepository {

  @Autowired
  private EntityManager entityManager;

  private static final Integer BATCH_SIZE = 500;

  @Override
  @Transactional
  public void batchSave(List<PositionExtendEntity> positionExtendEntityList) {
    StringBuilder hql = new StringBuilder("INSERT INTO `engine_position` ( `id`, `code`, `create_time`, `name`, `tstatus`, `organization_id` ) VALUES(?,?,?,?,?,?)");
    Query query = entityManager.createNativeQuery(hql.toString());
    StringBuilder hql2 = new StringBuilder(" INSERT INTO `tw_position_extend`(`id`,`hr_descrshort`, `hr_effdt`, `hr_position_id`, `hr_job_family`, `hr_job_function`,");
    hql2.append(" `hr_job_sub_func`, `hr_med_chkup_req`, `hr_tw_action`, `hr_tw_org_type`, `parent_id`, `position_id`) VALUES");
    hql2.append("(uuid(),?,?,?,?,?,?,?,?,?,?,?)");
    Query query2 = entityManager.createNativeQuery(hql2.toString());
    positionExtendEntityList.forEach(m -> {
      PositionEntity positionEntity = m.getPosition();
      query.setParameter(1, positionEntity.getId());
      query.setParameter(2, positionEntity.getCode());
      query.setParameter(3, positionEntity.getCreateTime());
      query.setParameter(4, positionEntity.getName());
      query.setParameter(5, positionEntity.getTstatus());
      if (positionEntity.getOrganization() != null) {
        query.setParameter(6, positionEntity.getOrganization().getId());
      } else {
        query.setParameter(6, null);
      }
      query.executeUpdate();
      query2.setParameter(1, m.getDescrshort());
      query2.setParameter(2, m.getEffdt());
      query2.setParameter(3, m.getHrPositionId());
      query2.setParameter(4, m.getJobFamily());
      query2.setParameter(5, m.getJobFunction());
      query2.setParameter(6, m.getJobSubFunc());
      query2.setParameter(7, m.getMedChkupReq());
      query2.setParameter(8, m.getTwAction());
      query2.setParameter(9, m.getTwOrgType());
      if (m.getParent() != null) {
        query2.setParameter(10, m.getParent().getId());
      } else {
        query2.setParameter(10, null);
      }
      query2.setParameter(11, positionEntity.getId());
      query2.executeUpdate();
    });
  }

  @Override
  @Transactional
  public List<PositionExtendEntity> batchUpdate(List<PositionExtendEntity> positionExtendEntityList) {
      Iterator<PositionExtendEntity> iterator = positionExtendEntityList.iterator();
      int index = 0;
      while (iterator.hasNext()){
        entityManager.merge(iterator.next());
        index++;
        if (index % BATCH_SIZE == 0){
          entityManager.flush();
          entityManager.clear();
        }
      }
      if (index % BATCH_SIZE != 0){
        entityManager.flush();
        entityManager.clear();
      }
      return positionExtendEntityList;
    }
}
