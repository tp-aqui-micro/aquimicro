package com.tw.aquaculture.check.repository.userextend.internal;

import com.tw.aquaculture.common.entity.userextend.UserExtendEntity;

import java.util.List;

/**
 * PositionExtendEntity 业务模型的数据库方法支持 岗位扩展属性模块
 *
 * @author fangfu.luo
 */
public interface ToUserExtendEntityRepository {
    /**
     * 批量保存信息
     * @param userExtendEntityList 需要保存的 数据
     */

    void batchSave(List<UserExtendEntity> userExtendEntityList);

}