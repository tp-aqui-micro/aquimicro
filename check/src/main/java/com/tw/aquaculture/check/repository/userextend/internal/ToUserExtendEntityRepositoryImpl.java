package com.tw.aquaculture.check.repository.userextend.internal;

import com.bizunited.platform.core.entity.UserEntity;
import com.tw.aquaculture.common.entity.userextend.UserExtendEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author fangfu.luo
 * @date 2019/12/18
 */
@Repository("toUserExtendEntityRepositoryImpl")
public class ToUserExtendEntityRepositoryImpl implements ToUserExtendEntityRepository {


  @Autowired
  private EntityManager entityManager;

  @Override
  @Transactional
  public void batchSave(List<UserExtendEntity> userExtendEntityList) {
    StringBuilder hql = new StringBuilder(" INSERT INTO `engine_user`(`id`, `user_account`, `create_time`, `entry_time`, `gender`");
    hql.append(", `lastlogin_time`, `user_password`, `user_phone`, `use_status`, `user_head`, `user_name`) VALUES");
    hql.append("(?,?,?,?,?,?,?,?,?,?,?)");
    Query query = entityManager.createNativeQuery(hql.toString());
    StringBuilder hql2 = new StringBuilder("INSERT INTO `tw_user_extend`(`id`, `hr_user_id`, `user_id`, `hr_action`, `hr_birthdate`, `hr_company`, `hr_deptid`, `hr_descr`, ");
    hql2.append("`hr_descr1`, `hr_descr3`, `hr_descr4`, `hr_descr5`, `hr_descr6`, `hr_descr7`, `hr_descr8`, `hr_descr_2`, `hr_effdt`, `hr_effseq`, `hr_email_addr`, `hr_empl_class`, `hr_empl_rcd`, ");
    hql2.append("`hr_extension`, `hr_first_name`, `hr_flag`, `hr_job_function`, `hr_job_indicator`, `hr_job_sub_func`, `hr_jobcode`, `hr_jpm_descr90`, `hr_jpm_text254_1`, `hr_last_name`,");
    hql2.append(" `hr_location`, `hr_mar_status`, `hr_med_chkup_req`, `hr_name_ac`, `hr_national_id`, `hr_national_id_type`, `hr_phone`, `hr_position_entry_dt`, `hr_position_nbr`,");
    hql2.append(" `hr_seniority_pay_dt`, `hr_supervisor_id`, `hr_tw_action`, `hr_tw_awe_line`, `hr_tw_awe_line_descr`, `hr_tw_com_dept`, `hr_tw_dept_line`, `hr_tw_dept_property`, `hr_tw_job_level`, ");
    hql2.append("`hr_tw_pos_incharge`, `hr_tw_wx_id`, `hr_union_cd`) VALUES ");
    hql2.append("(uuid(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    Query query2 = entityManager.createNativeQuery(hql2.toString());
    StringBuilder hql3 = new StringBuilder(" INSERT INTO `engine_org_user_mapping`(`org_id`, `user_id`) VALUES (?,?)");
    Query query3 = entityManager.createNativeQuery(hql3.toString());

    userExtendEntityList.forEach(m -> {
      UserEntity userEntity = m.getUser();
      query.setParameter(1, userEntity.getId());
      query.setParameter(2, userEntity.getAccount());
      query.setParameter(3, userEntity.getCreateTime());
      query.setParameter(4, userEntity.getEntryTime());
      query.setParameter(5, userEntity.getGender());
      query.setParameter(6, userEntity.getLastloginTime());
      query.setParameter(7, userEntity.getPassword());
      query.setParameter(8, userEntity.getPhone());
      query.setParameter(9, userEntity.getUseStatus());
      query.setParameter(10, userEntity.getUserHead());
      query.setParameter(11, userEntity.getUserName());
      query.executeUpdate();

      if(!CollectionUtils.isEmpty(userEntity.getOrgs())) {
        userEntity.getOrgs().forEach(o->{
          query3.setParameter(1, o.getId());
          query3.setParameter(2, userEntity.getId());
          query3.executeUpdate();
        });
      }
      query2.setParameter(1, m.getHrUserId());
      query2.setParameter(2,userEntity.getId());
      query2.setParameter(3, m.getHrAction());
      query2.setParameter(4, m.getHrBirthdate());
      query2.setParameter(5, m.getHrCompany());
      query2.setParameter(6, m.getHrDeptid());
      query2.setParameter(7, m.getHrDescr());
      query2.setParameter(8, m.getHrDescr1());
      query2.setParameter(9, m.getHrDescr3());
      query2.setParameter(10, m.getHrDescr4());

      query2.setParameter(11, m.getHrDescr5());
      query2.setParameter(12, m.getHrDescr6());
      query2.setParameter(13, m.getHrDescr7());
      query2.setParameter(14, m.getHrDescr8());
      query2.setParameter(15, m.getHrDescr2());
      query2.setParameter(16, m.getHrEffdt());
      query2.setParameter(17, m.getHrEffseq());
      query2.setParameter(18, m.getHrEmailAddr());
      query2.setParameter(19, m.getHrEmplClass());
      query2.setParameter(20, m.getHrEmplRcd());

      query2.setParameter(21, m.getHrExtension());
      query2.setParameter(22, m.getHrFirstName());
      query2.setParameter(23, m.getHrFlag());
      query2.setParameter(24, m.getHrJobFunction());
      query2.setParameter(25, m.getHrJobIndicator());
      query2.setParameter(26, m.getHrJobSubFunc());
      query2.setParameter(27, m.getHrJobcode());
      query2.setParameter(28, m.getHrJpmDescr90());
      query2.setParameter(29, m.getHrJpmText2541());
      query2.setParameter(30, m.getHrLastName());

      query2.setParameter(31, m.getHrLocation());
      query2.setParameter(32, m.getHrMarStatus());
      query2.setParameter(33, m.getHrMedChkupReq());
      query2.setParameter(34, m.getHrNameAc());
      query2.setParameter(35, m.getHrNationalId());
      query2.setParameter(36, m.getHrNationalIdType());
      query2.setParameter(37, m.getHrPhone());
      query2.setParameter(38, m.getHrPositionEntryDt());
      query2.setParameter(39, m.getHrPositionNbr());
      query2.setParameter(40, m.getHrSeniorityPayDt());

      query2.setParameter(41, m.getHrSupervisorId());
      query2.setParameter(42, m.getHrTwAction());
      query2.setParameter(43, m.getHrTwAweLine());
      query2.setParameter(44, m.getHrTwAweLineDescr());
      query2.setParameter(45, m.getHrTwComDept());
      query2.setParameter(46, m.getHrTwDeptLine());
      query2.setParameter(47, m.getHrTwDeptProperty());
      query2.setParameter(48, m.getHrTwJobLevel());
      query2.setParameter(49, m.getHrTwPosIncharge());
      query2.setParameter(50, m.getHrTwWxId());
      query2.setParameter(51, m.getHrUnionCd());
      query2.executeUpdate();
    });
  }
}
