package com.tw.aquaculture.check.service.internal;

import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.rbac.server.service.UserService;
import com.bizunited.platform.rbac.server.vo.UserVo;
import com.google.common.collect.Sets;
import com.tw.aquaculture.check.repository.CheckTaskBackEntityRepository;
import com.tw.aquaculture.check.service.CheckTaskBackEntityService;
import com.tw.aquaculture.check.service.CheckTaskEntityService;
import com.tw.aquaculture.common.entity.check.CheckTaskBackEntity;
import com.tw.aquaculture.common.entity.check.CheckTaskEntity;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;

/**
 * CheckTaskBackEntity业务模型的服务层接口实现
 * @author saturn
 */
@Service("CheckTaskBackEntityServiceImpl")
public class CheckTaskBackEntityServiceImpl implements CheckTaskBackEntityService {
  @Autowired
  private CheckTaskEntityService checkTaskEntityService;
  @Autowired
  private CheckTaskBackEntityRepository checkTaskBackEntityRepository;
  @Autowired
  private UserService userService;
  @Transactional
  @Override
  public CheckTaskBackEntity create(CheckTaskBackEntity checkTaskBackEntity) {
    CheckTaskBackEntity current = this.createForm(checkTaskBackEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  } 
  @Transactional
  @Override
  public CheckTaskBackEntity createForm(CheckTaskBackEntity checkTaskBackEntity) {
   /* 
    * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
    * 1、如果当前模型对象不是主模型
    * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
    * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
    * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
    * 2、如果当前模型对象是主业务模型
    *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
    *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
    *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
    * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
    *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
    *   2.3.2、以及验证每个分组的OneToMany明细信息
    * */
    this.createValidation(checkTaskBackEntity);
    
    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    String account = SecurityContextHolder.getContext().getAuthentication().getName();
    Validate.notNull(account, "当前登录账号已过期，请重新登录");
    UserVo userVo = userService.findByAccount(account);
    UserEntity userEntity = new UserEntity();
    userEntity.setId(userVo.getId());
    checkTaskBackEntity.setUser(userEntity);
    this.checkTaskBackEntityRepository.saveAndFlush(checkTaskBackEntity);
    if(checkTaskBackEntity.getCheckTask()!=null &&checkTaskBackEntity.getCheckTask().getId()!=null){
      CheckTaskEntity checkTaskEntity = checkTaskEntityService.findDetailsById(checkTaskBackEntity.getCheckTask().getId());
      checkTaskEntity.setState(EnableStateEnum.ENABLE.getState());
      checkTaskEntityService.update(checkTaskEntity);
    }
    // 返回最终处理的结果，里面带有详细的关联信息
    return checkTaskBackEntity;
  }
  /**
   * 在创建一个新的CheckTaskBackEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(CheckTaskBackEntity checkTaskBackEntity) {
    Validate.notNull(checkTaskBackEntity , "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(checkTaskBackEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    checkTaskBackEntity.setId(null);
    Validate.notNull(checkTaskBackEntity.getBackTime(), "添加信息时，反馈时间不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
  }
  @Transactional
  @Override
  public CheckTaskBackEntity update(CheckTaskBackEntity checkTaskBackEntity) {
    CheckTaskBackEntity current = this.updateForm(checkTaskBackEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  } 
  @Transactional
  @Override
  public CheckTaskBackEntity updateForm(CheckTaskBackEntity checkTaskBackEntity) {
    /* 
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     * 
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    
    this.updateValidation(checkTaskBackEntity);
    // ===================基本信息
    String currentId = checkTaskBackEntity.getId();
    Optional<CheckTaskBackEntity> op_currentCheckTaskBackEntity = this.checkTaskBackEntityRepository.findById(currentId);
    CheckTaskBackEntity currentCheckTaskBackEntity = op_currentCheckTaskBackEntity.orElse(null);
    currentCheckTaskBackEntity = Validate.notNull(currentCheckTaskBackEntity ,"未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentCheckTaskBackEntity.setContent(checkTaskBackEntity.getContent());
    currentCheckTaskBackEntity.setPicture(checkTaskBackEntity.getPicture());
    currentCheckTaskBackEntity.setBackTime(checkTaskBackEntity.getBackTime());
    currentCheckTaskBackEntity.setCheckTask(checkTaskBackEntity.getCheckTask());
    currentCheckTaskBackEntity.setUser(checkTaskBackEntity.getUser());
    currentCheckTaskBackEntity.setCheckTask(checkTaskBackEntity.getCheckTask());
    
    this.checkTaskBackEntityRepository.saveAndFlush(currentCheckTaskBackEntity);
    return currentCheckTaskBackEntity;
  }
  /**
   * 在更新一个已有的CheckTaskBackEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(CheckTaskBackEntity checkTaskBackEntity) {
    Validate.isTrue(!StringUtils.isBlank(checkTaskBackEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
    
    // 基础信息判断，基本属性，需要满足not null
    Validate.notNull(checkTaskBackEntity.getBackTime(), "修改信息时，反馈时间不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true

    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
  } 
  @Override
  public Set<CheckTaskBackEntity> findDetailsByCheckTask(String checkTask) {
    if(StringUtils.isBlank(checkTask)) {
      return Sets.newHashSet();
    }
    return this.checkTaskBackEntityRepository.findDetailsByCheckTask(checkTask);
  }
  @Override
  public Set<CheckTaskBackEntity> findDetailsByUser(String user) {
    if(StringUtils.isBlank(user)) {
      return Sets.newHashSet();
    }
    return this.checkTaskBackEntityRepository.findDetailsByUser(user);
  }
  @Override
  public CheckTaskBackEntity findDetailsById(String id) {
    if(StringUtils.isBlank(id)) {
      return null;
    }
    return this.checkTaskBackEntityRepository.findDetailsById(id);
  }
  @Override
  public CheckTaskBackEntity findById(String id) {
    if(StringUtils.isBlank(id)) {
      return null;
    }
    
    Optional<CheckTaskBackEntity> op = checkTaskBackEntityRepository.findById(id);
    return op.orElse(null); 
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id , "进行删除时，必须给定主键信息!!");
    CheckTaskBackEntity current = this.findById(id);
    if(current != null) { 
      this.checkTaskBackEntityRepository.delete(current);
    }
  }
} 
