package com.tw.aquaculture.check.service.internal;

import com.tw.aquaculture.check.repository.CheckWaterQualityEntityRepository;
import com.tw.aquaculture.check.service.CheckWaterQualityEntityService;
import com.tw.aquaculture.check.service.remote.CodeGenerateService;
import com.tw.aquaculture.check.service.remote.PondEntityService;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.check.CheckWaterQualityEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * CheckWaterQualityEntity业务模型的服务层接口实现
 * @author saturn
 */
@Service("CheckWaterQualityEntityServiceImpl")
public class CheckWaterQualityEntityServiceImpl implements CheckWaterQualityEntityService {
  @Autowired
  private PondEntityService pondEntityService;
  @Autowired
  private CheckWaterQualityEntityRepository checkWaterQualityEntityRepository;
  @Autowired
  private CodeGenerateService codeGenerateService;
  @Transactional
  @Override
  public CheckWaterQualityEntity create(CheckWaterQualityEntity checkWaterQualityEntity) {
    CheckWaterQualityEntity current = this.createForm(checkWaterQualityEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  } 
  @Transactional
  @Override
  public CheckWaterQualityEntity createForm(CheckWaterQualityEntity checkWaterQualityEntity) {
   /* 
    * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
    * 1、如果当前模型对象不是主模型
    * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
    * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
    * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
    * 2、如果当前模型对象是主业务模型
    *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
    *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
    *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
    * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
    *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
    *   2.3.2、以及验证每个分组的OneToMany明细信息
    * */
    checkWaterQualityEntity.setCode(codeGenerateService.generateBusinessCode(CodeTypeEnum.CHECK_WATER_QUALITY_ENTITY));
    this.createValidation(checkWaterQualityEntity);
    
    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================

    this.checkWaterQualityEntityRepository.saveAndFlush(checkWaterQualityEntity);
    
    // 返回最终处理的结果，里面带有详细的关联信息
    return checkWaterQualityEntity;
  }
  /**
   * 在创建一个新的CheckWaterQualityEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(CheckWaterQualityEntity checkWaterQualityEntity) {
    Validate.notNull(checkWaterQualityEntity , "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(checkWaterQualityEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    checkWaterQualityEntity.setId(null);
    Validate.notBlank(checkWaterQualityEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(checkWaterQualityEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(checkWaterQualityEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(checkWaterQualityEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notBlank(checkWaterQualityEntity.getCode(), "添加信息时，编码不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(checkWaterQualityEntity.getFormInstanceId() == null || checkWaterQualityEntity.getFormInstanceId().length() < 255 , "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(checkWaterQualityEntity.getCreateName() == null || checkWaterQualityEntity.getCreateName().length() < 255 , "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(checkWaterQualityEntity.getCreatePosition() == null || checkWaterQualityEntity.getCreatePosition().length() < 255 , "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(checkWaterQualityEntity.getModifyName() == null || checkWaterQualityEntity.getModifyName().length() < 255 , "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(checkWaterQualityEntity.getModifyPosition() == null || checkWaterQualityEntity.getModifyPosition().length() < 255 , "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(checkWaterQualityEntity.getCode() == null || checkWaterQualityEntity.getCode().length() < 255 , "编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(checkWaterQualityEntity.getAlgaeColor() == null || checkWaterQualityEntity.getAlgaeColor().length() < 255 , "藻相,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(checkWaterQualityEntity.getNote() == null || checkWaterQualityEntity.getNote().length() < 1000 , "备注,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(checkWaterQualityEntity.getPicture() == null || checkWaterQualityEntity.getPicture().length() < 10000 , "图片,在进行添加时填入值超过了限定长度(255)，请检查!");
    CheckWaterQualityEntity currentCheckWaterQualityEntity = this.findByFormInstanceId(checkWaterQualityEntity.getFormInstanceId());
    Validate.isTrue(currentCheckWaterQualityEntity == null, "表单实例编号已存在,请检查");
    // 验证ManyToOne关联：塘口绑定值的正确性
    PondEntity currentPond = checkWaterQualityEntity.getPond();
    Validate.notNull(currentPond , "塘口信息必须传入，请检查!!");
    String currentPkPond = currentPond.getId();
    Validate.notBlank(currentPkPond, "创建操作时，当前塘口信息必须关联！");
    Validate.notNull(this.pondEntityService.findById(currentPkPond) , "塘口关联信息未找到，请检查!!");
  }
  @Transactional
  @Override
  public CheckWaterQualityEntity update(CheckWaterQualityEntity checkWaterQualityEntity) {
    CheckWaterQualityEntity current = this.updateForm(checkWaterQualityEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  } 
  @Transactional
  @Override
  public CheckWaterQualityEntity updateForm(CheckWaterQualityEntity checkWaterQualityEntity) {
    /* 
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     * 
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    
    this.updateValidation(checkWaterQualityEntity);
    // ===================基本信息
    String currentId = checkWaterQualityEntity.getId();
    Optional<CheckWaterQualityEntity> op_currentCheckWaterQualityEntity = this.checkWaterQualityEntityRepository.findById(currentId);
    CheckWaterQualityEntity currentCheckWaterQualityEntity = op_currentCheckWaterQualityEntity.orElse(null);
    currentCheckWaterQualityEntity = Validate.notNull(currentCheckWaterQualityEntity ,"未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentCheckWaterQualityEntity.setFormInstanceId(checkWaterQualityEntity.getFormInstanceId());
    currentCheckWaterQualityEntity.setCreateTime(checkWaterQualityEntity.getCreateTime());
    currentCheckWaterQualityEntity.setCreateName(checkWaterQualityEntity.getCreateName());
    currentCheckWaterQualityEntity.setCreatePosition(checkWaterQualityEntity.getCreatePosition());
    currentCheckWaterQualityEntity.setModifyTime(checkWaterQualityEntity.getModifyTime());
    currentCheckWaterQualityEntity.setModifyName(checkWaterQualityEntity.getModifyName());
    currentCheckWaterQualityEntity.setModifyPosition(checkWaterQualityEntity.getModifyPosition());
    currentCheckWaterQualityEntity.setCode(checkWaterQualityEntity.getCode());
    currentCheckWaterQualityEntity.setdO(checkWaterQualityEntity.getdO());
    currentCheckWaterQualityEntity.setPh(checkWaterQualityEntity.getPh());
    currentCheckWaterQualityEntity.setWaterTemperature(checkWaterQualityEntity.getWaterTemperature());
    currentCheckWaterQualityEntity.setAn(checkWaterQualityEntity.getAn());
    currentCheckWaterQualityEntity.setNitrite(checkWaterQualityEntity.getNitrite());
    currentCheckWaterQualityEntity.setAlgaeColor(checkWaterQualityEntity.getAlgaeColor());
    currentCheckWaterQualityEntity.setCheckTime(checkWaterQualityEntity.getCheckTime());
    currentCheckWaterQualityEntity.setNote(checkWaterQualityEntity.getNote());
    currentCheckWaterQualityEntity.setPicture(checkWaterQualityEntity.getPicture());
    currentCheckWaterQualityEntity.setPond(checkWaterQualityEntity.getPond());
    currentCheckWaterQualityEntity.setTransparency(checkWaterQualityEntity.getTransparency());
    currentCheckWaterQualityEntity.setRemainChlorine(checkWaterQualityEntity.getRemainChlorine());
    
    this.checkWaterQualityEntityRepository.saveAndFlush(currentCheckWaterQualityEntity);
    return currentCheckWaterQualityEntity;
  }
  /**
   * 在更新一个已有的CheckWaterQualityEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(CheckWaterQualityEntity checkWaterQualityEntity) {
    Validate.isTrue(!StringUtils.isBlank(checkWaterQualityEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
    
    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(checkWaterQualityEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(checkWaterQualityEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(checkWaterQualityEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(checkWaterQualityEntity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    Validate.notBlank(checkWaterQualityEntity.getCode(), "修改信息时，编码不能为空！");
    
    // 重复性判断，基本属性，需要满足unique = true
    CheckWaterQualityEntity currentForFormInstanceId = this.findByFormInstanceId(checkWaterQualityEntity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId() , checkWaterQualityEntity.getId()) , "表单实例编号已存在,请检查"); 
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(checkWaterQualityEntity.getFormInstanceId() == null || checkWaterQualityEntity.getFormInstanceId().length() < 255 , "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(checkWaterQualityEntity.getCreateName() == null || checkWaterQualityEntity.getCreateName().length() < 255 , "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(checkWaterQualityEntity.getCreatePosition() == null || checkWaterQualityEntity.getCreatePosition().length() < 255 , "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(checkWaterQualityEntity.getModifyName() == null || checkWaterQualityEntity.getModifyName().length() < 255 , "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(checkWaterQualityEntity.getModifyPosition() == null || checkWaterQualityEntity.getModifyPosition().length() < 255 , "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(checkWaterQualityEntity.getCode() == null || checkWaterQualityEntity.getCode().length() < 255 , "编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(checkWaterQualityEntity.getAlgaeColor() == null || checkWaterQualityEntity.getAlgaeColor().length() < 255 , "藻相,在进行修改时填入值超过了限定长度(255)，请检查!");

    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
    PondEntity currentForPond = checkWaterQualityEntity.getPond();
    Validate.notNull(currentForPond , "修改信息时，塘口必须传入，请检查!!");
  } 
  @Override
  public CheckWaterQualityEntity findDetailsById(String id) {
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(id)) { 
      return null; 
    } 
    CheckWaterQualityEntity current = this.checkWaterQualityEntityRepository.findDetailsById(id);
    if(current == null) {
      return null;
    } 
    return current;
  }
  @Override
  public CheckWaterQualityEntity findById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    
    Optional<CheckWaterQualityEntity> op = checkWaterQualityEntityRepository.findById(id);
    return op.orElse(null); 
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id , "进行删除时，必须给定主键信息!!");
    CheckWaterQualityEntity current = this.findById(id);
    if(current != null) { 
      this.checkWaterQualityEntityRepository.delete(current);
    }
  }
  @Override
  public CheckWaterQualityEntity findDetailsByFormInstanceId(String formInstanceId) {
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(formInstanceId)) { 
      return null; 
    } 
    CheckWaterQualityEntity current = this.checkWaterQualityEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if(current == null) {
      return null;
    } 
    return current;
  }
  @Override
  public CheckWaterQualityEntity findByFormInstanceId(String formInstanceId) {
    if(StringUtils.isBlank(formInstanceId)) { 
      return null;
    }
    return this.checkWaterQualityEntityRepository.findByFormInstanceId(formInstanceId);
  }

  @Override
  public CheckWaterQualityEntity findLastCheckWaterByPondId(String pondId) {
    if(StringUtils.isBlank(pondId)){
      return null;
    }
    List<CheckWaterQualityEntity> checkWaterQualityEntityList = checkWaterQualityEntityRepository.findLastCheckWaterByPondId(pondId);
    if(CollectionUtils.isEmpty(checkWaterQualityEntityList)){
      return null;
    }
    return checkWaterQualityEntityList.get(0);
  }
} 
