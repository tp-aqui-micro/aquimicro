package com.tw.aquaculture.check.service.internal;

import com.tw.aquaculture.check.repository.TestIndexEentityRepository;
import com.tw.aquaculture.check.service.TestIndexEentityService;
import com.tw.aquaculture.check.service.remote.CodeGenerateService;
import com.tw.aquaculture.common.entity.base.TestIndexEentity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * TestIndexEentity业务模型的服务层接口实现
 * @author saturn
 */
@Service("TestIndexEentityServiceImpl")
public class TestIndexEentityServiceImpl implements TestIndexEentityService {
  @Autowired
  private TestIndexEentityRepository testIndexEentityRepository;
  @Autowired
  private CodeGenerateService codeGenerateService;
  @Transactional
  @Override
  public TestIndexEentity create(TestIndexEentity testIndexEentity) {
    TestIndexEentity current = this.createForm(testIndexEentity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  } 
  @Transactional
  @Override
  public TestIndexEentity createForm(TestIndexEentity testIndexEentity) {
   /* 
    * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
    * 1、如果当前模型对象不是主模型
    * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
    * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
    * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
    * 2、如果当前模型对象是主业务模型
    *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
    *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
    *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
    * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
    *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
    *   2.3.2、以及验证每个分组的OneToMany明细信息
    * */
    testIndexEentity.setCode(codeGenerateService.generateCode(CodeTypeEnum.TEST_INDEX_ENTITY));
    this.createValidation(testIndexEentity);
    
    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    this.testIndexEentityRepository.saveAndFlush(testIndexEentity);
    // 返回最终处理的结果，里面带有详细的关联信息
    return testIndexEentity;
  }
  /**
   * 在创建一个新的TestIndexEentity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(TestIndexEentity testIndexEentity) {
    Validate.notNull(testIndexEentity , "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(testIndexEentity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    testIndexEentity.setId(null);
    Validate.notBlank(testIndexEentity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(testIndexEentity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(testIndexEentity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(testIndexEentity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notBlank(testIndexEentity.getCode(), "添加信息时，检测指标编码不能为空！");
    Validate.notBlank(testIndexEentity.getName(), "添加信息时，检测指标名称不能为空！");
    Validate.notNull(testIndexEentity.getMinValue(), "添加信息时，最小值不能为空！");
    Validate.notNull(testIndexEentity.getMaxValue(), "添加信息时，最大值不能为空！");
    Validate.notNull(testIndexEentity.getAccuracy(), "添加信息时，精确度不能为空！");
    Validate.notNull(testIndexEentity.getEnableState(), "添加信息时，生效状态不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(testIndexEentity.getFormInstanceId() == null || testIndexEentity.getFormInstanceId().length() < 255 , "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(testIndexEentity.getCreateName() == null || testIndexEentity.getCreateName().length() < 255 , "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(testIndexEentity.getCreatePosition() == null || testIndexEentity.getCreatePosition().length() < 255 , "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(testIndexEentity.getModifyName() == null || testIndexEentity.getModifyName().length() < 255 , "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(testIndexEentity.getModifyPosition() == null || testIndexEentity.getModifyPosition().length() < 255 , "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(testIndexEentity.getCode() == null || testIndexEentity.getCode().length() < 255 , "检测指标编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(testIndexEentity.getName() == null || testIndexEentity.getName().length() < 255 , "检测指标名称,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(testIndexEentity.getUnit() == null || testIndexEentity.getUnit().length() < 255 , "单位,在进行添加时填入值超过了限定长度(255)，请检查!");
    TestIndexEentity currentTestIndexEentity = this.findByFormInstanceId(testIndexEentity.getFormInstanceId());
    Validate.isTrue(currentTestIndexEentity == null, "表单实例编号已存在,请检查");
  }
  @Transactional
  @Override
  public TestIndexEentity update(TestIndexEentity testIndexEentity) {
    TestIndexEentity current = this.updateForm(testIndexEentity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  } 
  @Transactional
  @Override
  public TestIndexEentity updateForm(TestIndexEentity testIndexEentity) {
    /* 
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     * 
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    
    this.updateValidation(testIndexEentity);
    // ===================基本信息
    String currentId = testIndexEentity.getId();
    Optional<TestIndexEentity> op_currentTestIndexEentity = this.testIndexEentityRepository.findById(currentId);
    TestIndexEentity currentTestIndexEentity = op_currentTestIndexEentity.orElse(null);
    currentTestIndexEentity = Validate.notNull(currentTestIndexEentity ,"未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentTestIndexEentity.setFormInstanceId(testIndexEentity.getFormInstanceId());
    currentTestIndexEentity.setCreateTime(testIndexEentity.getCreateTime());
    currentTestIndexEentity.setCreateName(testIndexEentity.getCreateName());
    currentTestIndexEentity.setCreatePosition(testIndexEentity.getCreatePosition());
    currentTestIndexEentity.setModifyTime(testIndexEentity.getModifyTime());
    currentTestIndexEentity.setModifyName(testIndexEentity.getModifyName());
    currentTestIndexEentity.setModifyPosition(testIndexEentity.getModifyPosition());
    currentTestIndexEentity.setCode(testIndexEentity.getCode());
    currentTestIndexEentity.setName(testIndexEentity.getName());
    currentTestIndexEentity.setMinValue(testIndexEentity.getMinValue());
    currentTestIndexEentity.setMaxValue(testIndexEentity.getMaxValue());
    currentTestIndexEentity.setUnit(testIndexEentity.getUnit());
    currentTestIndexEentity.setAccuracy(testIndexEentity.getAccuracy());
    currentTestIndexEentity.setEnableState(testIndexEentity.getEnableState());
    
    this.testIndexEentityRepository.saveAndFlush(currentTestIndexEentity);
    return currentTestIndexEentity;
  }
  /**
   * 在更新一个已有的TestIndexEentity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(TestIndexEentity testIndexEentity) {
    Validate.isTrue(!StringUtils.isBlank(testIndexEentity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
    
    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(testIndexEentity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(testIndexEentity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(testIndexEentity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(testIndexEentity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    Validate.notBlank(testIndexEentity.getCode(), "修改信息时，检测指标编码不能为空！");
    Validate.notBlank(testIndexEentity.getName(), "修改信息时，检测指标名称不能为空！");
    Validate.notNull(testIndexEentity.getMinValue(), "修改信息时，最小值不能为空！");
    Validate.notNull(testIndexEentity.getMaxValue(), "修改信息时，最大值不能为空！");
    Validate.notNull(testIndexEentity.getAccuracy(), "修改信息时，精确度不能为空！");
    Validate.notNull(testIndexEentity.getEnableState(), "修改信息时，生效状态不能为空！");
    
    // 重复性判断，基本属性，需要满足unique = true
    TestIndexEentity currentForFormInstanceId = this.findByFormInstanceId(testIndexEentity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId() , testIndexEentity.getId()) , "表单实例编号已存在,请检查"); 
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(testIndexEentity.getFormInstanceId() == null || testIndexEentity.getFormInstanceId().length() < 255 , "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(testIndexEentity.getCreateName() == null || testIndexEentity.getCreateName().length() < 255 , "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(testIndexEentity.getCreatePosition() == null || testIndexEentity.getCreatePosition().length() < 255 , "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(testIndexEentity.getModifyName() == null || testIndexEentity.getModifyName().length() < 255 , "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(testIndexEentity.getModifyPosition() == null || testIndexEentity.getModifyPosition().length() < 255 , "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(testIndexEentity.getCode() == null || testIndexEentity.getCode().length() < 255 , "检测指标编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(testIndexEentity.getName() == null || testIndexEentity.getName().length() < 255 , "检测指标名称,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(testIndexEentity.getUnit() == null || testIndexEentity.getUnit().length() < 255 , "单位,在进行修改时填入值超过了限定长度(255)，请检查!");
  } 
  @Override
  public TestIndexEentity findDetailsById(String id) {
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(id)) { 
      return null; 
    } 
    TestIndexEentity current = this.testIndexEentityRepository.findDetailsById(id);
    if(current == null) {
      return null;
    } 
    return current;
  }
  @Override
  public TestIndexEentity findById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    
    Optional<TestIndexEentity> op = testIndexEentityRepository.findById(id);
    return op.orElse(null); 
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id , "进行删除时，必须给定主键信息!!");
    TestIndexEentity current = this.findById(id);
    if(current != null) { 
      this.testIndexEentityRepository.delete(current);
    }
  }
  @Override
  public TestIndexEentity findDetailsByFormInstanceId(String formInstanceId) {
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(formInstanceId)) { 
      return null; 
    } 
    TestIndexEentity current = this.testIndexEentityRepository.findDetailsByFormInstanceId(formInstanceId);
    if(current == null) {
      return null;
    } 
    return current;
  }
  @Override
  public TestIndexEentity findByFormInstanceId(String formInstanceId) {
    if(StringUtils.isBlank(formInstanceId)) { 
      return null;
    }
    return this.testIndexEentityRepository.findByFormInstanceId(formInstanceId);
  } 
} 
