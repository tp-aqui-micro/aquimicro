package com.tw.aquaculture.check.service.remote.internal;

import com.tw.aquaculture.check.feign.plan.CodeGenerateFeign;
import com.tw.aquaculture.check.service.remote.CodeGenerateService;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.utils.CodeGeneratorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 陈荣
 * @date 2019/11/22 11:22
 */
@Service
public class CodeGenerateServiceImpl implements CodeGenerateService {

  @Autowired
  CodeGenerateFeign codeGenerateFeign;

  @Autowired
  CodeGeneratorUtil codeGeneratorUtil;

  @Override
  @Transactional
  public String generateCode(String codeType, String prefix, int length) {

    return null;
  }

  @Override
  public String generateCode(CodeTypeEnum codeTypeEnum) {
    if (codeTypeEnum == null) {
      return null;
    }
    return codeGenerateFeign.generateCode(codeTypeEnum);
  }

  @Override
  public String generateBusinessCode(CodeTypeEnum codeTypeEnum) {
    if (codeTypeEnum == null) {
      return null;
    }
    return codeGenerateFeign.generateBusinessCode(codeTypeEnum);
  }

  @Override
  public String generateBusinessCode(String name, String prefix, int i) {
    return null;
  }
}
