package com.tw.aquaculture.check.service.remote.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.check.feign.process.FishTypeFeign;
import com.tw.aquaculture.check.service.remote.FishTypeEntityService;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * FishTypeEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("FishTypeEntityServiceImpl")
public class FishTypeEntityServiceImpl implements FishTypeEntityService {

  @Autowired
  private FishTypeFeign fishTypeFeign;

  @Transactional
  @Override
  public FishTypeEntity create(FishTypeEntity fishTypeEntity) {
    ResponseModel responseModel = fishTypeFeign.create(fishTypeEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishTypeEntity>() {
    });
  }

  @Transactional
  @Override
  public FishTypeEntity createForm(FishTypeEntity fishTypeEntity) {
    return null;
  }

  @Transactional
  @Override
  public FishTypeEntity update(FishTypeEntity fishTypeEntity) {
    ResponseModel responseModel = fishTypeFeign.update(fishTypeEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishTypeEntity>() {
    });
  }

  @Transactional
  @Override
  public FishTypeEntity updateForm(FishTypeEntity fishTypeEntity) {
    return null;
  }

  @Override
  public FishTypeEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = fishTypeFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishTypeEntity>() {
    });
  }

  @Override
  public FishTypeEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = fishTypeFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishTypeEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    if (StringUtils.isBlank(id)) {
      return;
    }
    fishTypeFeign.deleteById(id);
  }

  @Override
  public FishTypeEntity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = fishTypeFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishTypeEntity>() {
    });
  }

  @Override
  public FishTypeEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = fishTypeFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishTypeEntity>() {
    });
  }

  @Override
  public List<FishTypeEntity> findAll() {
    ResponseModel responseModel = fishTypeFeign.findAll();
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<FishTypeEntity>>() {
    });
  }
} 
