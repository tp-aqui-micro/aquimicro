package com.tw.aquaculture.check.service.remote.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.check.feign.plan.ShareModelFeign;
import com.tw.aquaculture.check.service.remote.ShareModelEntityService;
import com.tw.aquaculture.common.entity.base.ShareModelEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * ShareModelEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("ShareModelEntityServiceImpl")
public class ShareModelEntityServiceImpl implements ShareModelEntityService {
  @Autowired
  private ShareModelFeign shareModelFeign;

  @Transactional
  @Override
  public ShareModelEntity create(ShareModelEntity shareModelEntity) {
    ShareModelEntity current = this.createForm(shareModelEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  }

  @Transactional
  @Override
  public ShareModelEntity createForm(ShareModelEntity shareModelEntity) {
    ResponseModel responseModel = shareModelFeign.create(shareModelEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ShareModelEntity>() {
    });
  }

  @Transactional
  @Override
  public ShareModelEntity update(ShareModelEntity shareModelEntity) {
    ResponseModel responseModel = shareModelFeign.update(shareModelEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ShareModelEntity>() {
    });
  }

  @Transactional
  @Override
  public ShareModelEntity updateForm(ShareModelEntity shareModelEntity) {
    return null;
  }

  @Override
  public ShareModelEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = shareModelFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ShareModelEntity>() {
    });
  }

  @Override
  public ShareModelEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = shareModelFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ShareModelEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    if (StringUtils.isBlank(id)) {
      return;
    }
    shareModelFeign.deleteById(id);
  }

  @Override
  public ShareModelEntity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = shareModelFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ShareModelEntity>() {
    });
  }

  @Override
  public ShareModelEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = shareModelFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ShareModelEntity>() {
    });
  }
} 
