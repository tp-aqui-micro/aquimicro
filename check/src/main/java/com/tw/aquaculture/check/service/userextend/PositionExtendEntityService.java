package com.tw.aquaculture.check.service.userextend;

import com.bizunited.platform.core.annotations.NebulaService;
import com.bizunited.platform.core.annotations.NebulaServiceMethod;
import com.bizunited.platform.core.annotations.NebulaServiceMethod.ScopeType;
import com.tw.aquaculture.common.entity.userextend.PositionExtendEntity;

import java.util.List;

/**
 * 组织结构扩展信息
 * @author fangfu.luo
 * @data 2019/12/16
 */
@NebulaService
public interface PositionExtendEntityService {
  /**
   * 创建一个新的positionExtendEntity模型对象（包括了可能的第三方系统调用、复杂逻辑处理等）
   */
  @NebulaServiceMethod(name="PositionExtendEntityService.create" ,
          desc="创建一个新的positionExtendEntity模型对象（包括了可能的第三方系统调用、复杂逻辑处理等）",
          returnPropertiesFilter="" , scope=ScopeType.WRITE ,
          recoveryService="PositionExtendEntityService.updateForm")
  PositionExtendEntity create(PositionExtendEntity positionExtendEntity);
  /**
   * 创建一个新的PositionExtendEntity模型对象
   * 该代码由satrun骨架生成，默认不包括任何可能第三方系统调用、任何复杂逻辑处理等，主要应用场景为前端表单数据的暂存功能</br>
   * 该方法与本接口中的updateFrom方法呼应
   */
  @NebulaServiceMethod(name="PositionExtendEntityService.createForm" ,
          desc="创建一个新的positionExtendEntity模型对象（默认不包括任何可能第三方系统调用、任何复杂逻辑处理等）" ,
          returnPropertiesFilter="" , scope=ScopeType.WRITE ,
          recoveryService="PositionExtendEntityService.updateForm")
  PositionExtendEntity createForm(PositionExtendEntity positionExtendEntity);
  /**
   * 更新一个已有的PositionExtendEntity模型对象，其主键属性必须有值(1.1.4-release版本调整)。
   * 这个方法实际上一共分为三个步骤（默认）：</br>
   * 1、调用updateValidation方法完成表单数据更新前的验证</br>
   * 2、调用updateForm方法完成表单数据的更新</br>
   * 3、完成开发人员自行在本update方法中书写的，进行第三方系统调用（或特殊处理过程）的执行。</br>
   * 这样做的目的，实际上是为了保证updateForm方法中纯粹是处理表单数据的，在数据恢复表单引擎默认调用updateForm方法时，不会影响任何第三方业务数据
   * （当然，如果系统有特别要求，可由开发人员自行完成代码逻辑调整）
   */
  @NebulaServiceMethod(name="PositionExtendEntityService.update" ,
          desc="更新一个已有的positionExtendEntity模型对象，其主键属性必须有值(1.1.4-release版本调整)。" ,
          returnPropertiesFilter="", scope=ScopeType.WRITE ,
          recoveryService="PositionExtendEntityService.updateForm")
  PositionExtendEntity update(PositionExtendEntity positionExtendEntity);
  /**
   * 该方法只用于处理业务表单信息，包括了主业务模型、其下的关联模型、分组信息和明细细信息等
   * 该方法非常重要，因为如果进行静态表单的数据恢复，那么表单引擎将默认调用主业务模型（服务层）的这个方法。</br>
   * 这样一来保证了数据恢复时，不会涉及任何第三方系统的调用（当然，如果开发人员需要涉及的，可以自行进行修改）
   */
  @NebulaServiceMethod(name="PositionExtendEntityService.updateForm" ,
          desc="该方法只用于处理业务表单信息，包括了主业务模型、其下的关联模型、分组信息和明细细信息等" ,
          returnPropertiesFilter="", scope=ScopeType.WRITE,
          recoveryService="PositionExtendEntityService.updateForm")
  PositionExtendEntity updateForm(PositionExtendEntity positionExtendEntity);
  /**
   * 按照positionExtendEntity的主键编号，查询指定的数据信息（不包括任何关联信息）
   * @param id 主键
   * */
  PositionExtendEntity findById(String id);
  /**
   * 按照positionEntity的主键编号，查询指定的岗位扩展数据信息
   * @param  id positionEntity主键
   * */
  PositionExtendEntity findByPositionId(String id);
  /**
   * 按照positionExtendEntity的hrPositionIds集合，查询指定的数据信息（不包括任何关联信息）
   * @param hrPositionIds hrPositionIds集合
   * */
  List<PositionExtendEntity> findByHrPositionIdIn(List<String> hrPositionIds);
  /**
   *  查询所有 存在的结构扩展信息
   * */
  List<PositionExtendEntity> findAll();
  /**
   *  按照主键进行信息的真删除
   * @param id 主键
   */
  void deleteById(String id);
  /**
   *  按照主键进行信息的真删除岗位信息
   * @param id 主键
   */
  void deletePositionById(String id);

  /**
   * 批量保存信息
   * @param positionExtendEntityList 需要保存的 数据
   */
  void batchSave(List<PositionExtendEntity> positionExtendEntityList);

  /**
   * 批量修改信息
   * @param positionExtendEntityList  需要修改的数据库
   */
  void batchUpdate(List<PositionExtendEntity> positionExtendEntityList);

  /**
   * 同步hr系统的机构信息数据到我们数据库
   */
  void pullMatters();
}