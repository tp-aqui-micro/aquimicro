package com.tw.aquaculture.check.service.userextend;

import com.bizunited.platform.core.entity.RoleEntity;

import java.util.List;

/**
 * 角色扩展信息
 * @author fangfu.luo
 * @data 2019/12/16
 */
public interface RoleExtendEntityService {
  /**
   *  通过主键查询
   * */
  RoleEntity findById(String id);
  /**
   *  查询所有 角色
   * */
  List<RoleEntity> findAll();
  /**
   *  按照主键进行信息的真删除
   * @param id 主键
   */
  void deleteById(String id);

  /**
   * 批量保存信息
   * @param roleEntityList 需要保存的 数据
   */
  List<RoleEntity> batchSave(List<RoleEntity> roleEntityList);

    /**
     * 批量修改信息
   * @param roleEntityList  需要修改的数据库
   */
  List<RoleEntity> batchUpdate(List<RoleEntity> roleEntityList);
}