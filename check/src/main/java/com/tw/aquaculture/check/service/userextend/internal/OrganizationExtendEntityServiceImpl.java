package com.tw.aquaculture.check.service.userextend.internal;

import com.tw.aquaculture.check.remote.OrganizationHrRemoteService;
import com.tw.aquaculture.check.repository.userextend.OrganizationExtendEntityRepository;
import com.tw.aquaculture.check.repository.userextend.internal.ToOrganizationExtendEntityRepository;
import com.tw.aquaculture.check.service.userextend.OrganizationExtendEntityService;
import com.tw.aquaculture.common.entity.userextend.OrganizationExtendEntity;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * OrganizationExtendEntityService组织机构扩展属性业务实现层
 * @author fangfu.luo
 * @data 2019/12/16
 */
@Service("organizationExtendEntityServiceImpl")
public class OrganizationExtendEntityServiceImpl implements OrganizationExtendEntityService {
  @Autowired
  private OrganizationExtendEntityRepository organizationExtendEntityRepository;
  @Autowired
  private ToOrganizationExtendEntityRepository toOrganizationExtendEntityRepository;
  @Autowired
  private OrganizationHrRemoteService organizationHrRemoteService;

  @Transactional
  @Override
  public OrganizationExtendEntity create(OrganizationExtendEntity organizationExtendEntity) {
    return this.createForm(organizationExtendEntity);
   }

  @Transactional
  @Override
  public OrganizationExtendEntity createForm(OrganizationExtendEntity organizationExtendEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *   2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    this.createValidation(organizationExtendEntity);

    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================

    this.organizationExtendEntityRepository.save(organizationExtendEntity);

    // 返回最终处理的结果，里面带有详细的关联信息
    return organizationExtendEntity;
  }

  @Override
  public void batchSave(List<OrganizationExtendEntity> organizationExtendEntityList) {
    //批量保存
    organizationExtendEntityList =  organizationExtendEntityList.stream().filter(s -> Objects.nonNull(s.getHrOrganizationId())).collect(Collectors.toList());
    this.toOrganizationExtendEntityRepository.batchSave(organizationExtendEntityList);
  }

  @Override
  public void batchUpdate(List<OrganizationExtendEntity> organizationExtendEntityList) {
    organizationExtendEntityList =  organizationExtendEntityList.stream()
            .filter(s -> Objects.nonNull(s.getHrOrganizationId()))
            .filter(s -> Objects.nonNull(s.getId()))
            .collect(Collectors.toList());

    this.organizationExtendEntityRepository.saveAll(organizationExtendEntityList);
  }

  /**
   * 在创建一个新的StockEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(OrganizationExtendEntity organizationExtendEntity) {
    Validate.notNull(organizationExtendEntity, "进行当前操作时，信息对象必须传入!!");
  }

  @Transactional
  @Override
  public OrganizationExtendEntity update(OrganizationExtendEntity organizationExtendEntity) {
    return this.updateForm(organizationExtendEntity);
  }

  @Transactional
  @Override
  public OrganizationExtendEntity updateForm(OrganizationExtendEntity organizationExtendEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     *
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */

    this.updateValidation(organizationExtendEntity);
    // ===================基本信息
    String currentId = organizationExtendEntity.getId();
    Optional<OrganizationExtendEntity> opCurrentStockEntity = this.organizationExtendEntityRepository.findById(currentId);
    OrganizationExtendEntity currentOrganizationExtendEntity = opCurrentStockEntity.orElse(null);
    Validate.notNull(currentOrganizationExtendEntity, "未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentOrganizationExtendEntity.setOrganization(organizationExtendEntity.getOrganization());
    currentOrganizationExtendEntity.setTwOrgType(organizationExtendEntity.getTwOrgType());
    currentOrganizationExtendEntity.setTwAction(organizationExtendEntity.getTwAction());
    currentOrganizationExtendEntity.setHrOrganizationId(organizationExtendEntity.getHrOrganizationId());
    currentOrganizationExtendEntity.setEffdt(organizationExtendEntity.getEffdt());
    currentOrganizationExtendEntity.setDescrshort(organizationExtendEntity.getDescrshort());
    this.organizationExtendEntityRepository.saveAndFlush(currentOrganizationExtendEntity);
    return currentOrganizationExtendEntity;
  }

  /**
   * 在更新一个已有的StockEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(OrganizationExtendEntity organizationExtendEntity) {
    Validate.isTrue(!StringUtils.isBlank(organizationExtendEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
  }

  @Override
  public OrganizationExtendEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }

    Optional<OrganizationExtendEntity> op = organizationExtendEntityRepository.findById(id);
    return op.orElse(null);
  }
  @Override
  public List<OrganizationExtendEntity> findByHrOrganizationIdIn(List<String> hrOrganizationId){
    if (CollectionUtils.isEmpty(hrOrganizationId)) {
      return Collections.emptyList();
    }

    List<OrganizationExtendEntity> organizationExtendEntities = organizationExtendEntityRepository.findByHrOrganizationIdIn(hrOrganizationId);
    return organizationExtendEntities;
  }
  @Override
  public List<OrganizationExtendEntity> findAll() {
    return organizationExtendEntityRepository.findAll();
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    OrganizationExtendEntity current = this.findById(id);
    if (current != null) {
      this.organizationExtendEntityRepository.delete(current);
    }
  }
  @Override
  public void pullMatters() {
    organizationHrRemoteService.pullMatters();
  }
}
