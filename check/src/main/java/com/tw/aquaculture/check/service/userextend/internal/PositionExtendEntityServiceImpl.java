package com.tw.aquaculture.check.service.userextend.internal;

import com.bizunited.platform.core.entity.PositionEntity;
import com.bizunited.platform.core.repository.PositionRepository;
import com.tw.aquaculture.check.remote.PositionHrRemoteService;
import com.tw.aquaculture.check.repository.userextend.PositionExtendEntityRepository;
import com.tw.aquaculture.check.service.userextend.PositionExtendEntityService;
import com.tw.aquaculture.common.entity.userextend.PositionExtendEntity;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * PositionExtendEntityService组织机构扩展属性业务实现层
 * @author fangfu.luo
 * @data 2019/12/16
 */
@Service("positionExtendEntityServiceImpl")
public class PositionExtendEntityServiceImpl implements PositionExtendEntityService {
  @Autowired
  private PositionExtendEntityRepository positionExtendEntityRepository;
  @Autowired
  private PositionRepository positionRepository;
  @Autowired
  private PositionHrRemoteService positionHrRemoteService;

  @Transactional
  @Override
  public PositionExtendEntity create(PositionExtendEntity positionExtendEntity) {
    return this.createForm(positionExtendEntity);
  }

  @Transactional
  @Override
  public PositionExtendEntity createForm(PositionExtendEntity positionExtendEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *   2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    this.createValidation(positionExtendEntity);

    // ===============================
    //  和业务有关的验证填写在这个区域
    // ===============================

    this.positionExtendEntityRepository.save(positionExtendEntity);

    // 返回最终处理的结果，里面带有详细的关联信息
    return positionExtendEntity;
  }

  @Override
  public void batchSave(List<PositionExtendEntity> positionExtendEntityList) {
    //批量保存
    positionExtendEntityList =  positionExtendEntityList.stream().filter(s -> Objects.nonNull(s.getHrPositionId())).collect(Collectors.toList());
    if(!CollectionUtils.isEmpty(positionExtendEntityList)) {
      this.positionExtendEntityRepository.saveAll(positionExtendEntityList);
    }
  }

  @Override
  public void batchUpdate(List<PositionExtendEntity> positionExtendEntityList) {
    positionExtendEntityList =  positionExtendEntityList.stream()
            .filter(s -> Objects.nonNull(s.getHrPositionId()))
            .filter(s -> Objects.nonNull(s.getId()))
            .collect(Collectors.toList());
    if(!CollectionUtils.isEmpty(positionExtendEntityList)) {
      this.positionExtendEntityRepository.saveAll(positionExtendEntityList);
    }
  }

  /**
   * 在创建一个新的StockEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(PositionExtendEntity positionExtendEntity) {
    Validate.notNull(positionExtendEntity, "进行当前操作时，信息对象必须传入!!");
  }

  @Transactional
  @Override
  public PositionExtendEntity update(PositionExtendEntity positionExtendEntity) {
    return this.updateForm(positionExtendEntity);
  }

  @Transactional
  @Override
  public PositionExtendEntity updateForm(PositionExtendEntity positionExtendEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     *
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */

    this.updateValidation(positionExtendEntity);
    // ===================基本信息
    String currentId = positionExtendEntity.getId();
    Optional<PositionExtendEntity> opCurrentStockEntity = this.positionExtendEntityRepository.findById(currentId);
    PositionExtendEntity currentpositionExtendEntity = opCurrentStockEntity.orElse(null);
    Validate.notNull(currentpositionExtendEntity, "未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性

    this.positionExtendEntityRepository.saveAndFlush(currentpositionExtendEntity);
    return currentpositionExtendEntity;
  }

  /**
   * 在更新一个已有的StockEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(PositionExtendEntity positionExtendEntity) {
    Validate.isTrue(!StringUtils.isBlank(positionExtendEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
  }

  @Override
  public PositionExtendEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    Optional<PositionExtendEntity> op = positionExtendEntityRepository.findById(id);
    return op.orElse(null);
  }
  @Override
  public PositionExtendEntity findByPositionId(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    PositionExtendEntity op = positionExtendEntityRepository.findByPositionId(id);
    return op;
  }
  @Override
  public List<PositionExtendEntity> findByHrPositionIdIn(List<String> hrPositionIds) {
    if (CollectionUtils.isEmpty(hrPositionIds)) {
      return Collections.emptyList();
    }
    return positionExtendEntityRepository.findByHrPositionIdIn(hrPositionIds);
  }

  @Override
  public List<PositionExtendEntity> findAll() {
    return positionExtendEntityRepository.findAll();
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    PositionExtendEntity current = this.findById(id);
    if (current != null) {
      this.positionExtendEntityRepository.delete(current);
    }
  }

  @Override
  @Transactional
  public void deletePositionById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    Optional<PositionEntity> op = positionRepository.findById(id);
    if (op.isPresent()) {
      this.positionRepository.delete(op.get());
    }
  }
  @Override
  public void pullMatters() {
    positionHrRemoteService.pullMatters();
  }
}
