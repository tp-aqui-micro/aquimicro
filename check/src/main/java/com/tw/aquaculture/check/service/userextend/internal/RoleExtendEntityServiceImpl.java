package com.tw.aquaculture.check.service.userextend.internal;

import com.bizunited.platform.core.entity.RoleEntity;
import com.bizunited.platform.core.repository.RoleRepository;
import com.tw.aquaculture.check.service.userextend.RoleExtendEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * UserExtendEntityService组织机构扩展属性业务实现层
 * @author fangfu.luo
 * @data 2019/12/16
 */
@Service("roleExtendEntityServiceImpl")
public class RoleExtendEntityServiceImpl implements RoleExtendEntityService {
  @Autowired
  private RoleRepository roleRepository;

  @Override
  public List<RoleEntity> batchSave(List<RoleEntity> roleEntityList) {
    return this.roleRepository.saveAll(roleEntityList);
  }
  @Override
  public List<RoleEntity> batchUpdate(List<RoleEntity> roleEntityList) {
    roleEntityList = roleEntityList.stream()
            .filter(s -> Objects.nonNull(s.getId()))
            .collect(Collectors.toList());

    return this.roleRepository.saveAll(roleEntityList);
  }
  @Override
  public RoleEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }

    Optional<RoleEntity> op = roleRepository.findById(id);
    return op.orElse(null);
  }

  @Override
  public List<RoleEntity> findAll() {
    return roleRepository.findAll();
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    RoleEntity current = this.findById(id);
    if (current != null) {
      this.roleRepository.delete(current);
    }
  }
}
