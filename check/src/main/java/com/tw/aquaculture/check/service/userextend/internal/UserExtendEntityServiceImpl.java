package com.tw.aquaculture.check.service.userextend.internal;

import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.core.repository.UserRepository;
import com.tw.aquaculture.check.remote.UserHrRemoteService;
import com.tw.aquaculture.check.repository.userextend.UserExtendEntityRepository;
import com.tw.aquaculture.check.repository.userextend.internal.ToUserExtendEntityRepository;
import com.tw.aquaculture.check.service.userextend.UserExtendEntityService;
import com.tw.aquaculture.common.entity.userextend.UserExtendEntity;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * UserExtendEntityService组织机构扩展属性业务实现层
 * @author fangfu.luo
 * @data 2019/12/16
 */
@Service("userExtendEntityServiceImpl")
public class UserExtendEntityServiceImpl implements UserExtendEntityService {
  @Autowired
  private UserExtendEntityRepository userExtendEntityRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private ToUserExtendEntityRepository toUserExtendEntityRepository;
  @Autowired
  private UserHrRemoteService userHrRemoteService;

  @Transactional
  @Override
  public UserExtendEntity create(UserExtendEntity userExtendEntity) {
    return this.createForm(userExtendEntity);
  }

  @Transactional
  @Override
  public UserExtendEntity createForm(UserExtendEntity userExtendEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *   2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    this.createValidation(userExtendEntity);

    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================

    this.userExtendEntityRepository.save(userExtendEntity);

    // 返回最终处理的结果，里面带有详细的关联信息
    return userExtendEntity;
  }

  @Override
  public void batchSave(List<UserExtendEntity> userExtendEntityList) {
    //批量保存
    userExtendEntityList =  userExtendEntityList.stream().filter(s -> Objects.nonNull(s.getHrUserId()))
            .filter(s -> StringUtils.isNotEmpty(s.getUser().getAccount())).collect(Collectors.toList());
    if(!CollectionUtils.isEmpty(userExtendEntityList)) {
      this.toUserExtendEntityRepository.batchSave(userExtendEntityList);
    }
  }

  @Override
  public void batchUpdate(List<UserExtendEntity> userExtendEntityList) {
    userExtendEntityList = userExtendEntityList.stream()
            .filter(s -> StringUtils.isNotEmpty(s.getHrUserId()))
            .filter(s -> StringUtils.isNotEmpty(s.getId()))
            .filter(s -> StringUtils.isNotEmpty(s.getUser().getAccount()))
            .collect(Collectors.toList());
    if (!CollectionUtils.isEmpty(userExtendEntityList)) {
      this.userExtendEntityRepository.saveAll(userExtendEntityList);
    }
  }

  /**
   * 在创建一个新的StockEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(UserExtendEntity userExtendEntity) {
    Validate.notNull(userExtendEntity, "进行当前操作时，信息对象必须传入!!");
  }

  @Transactional
  @Override
  public UserExtendEntity update(UserExtendEntity userExtendEntity) {
    return this.updateForm(userExtendEntity);
  }

  @Transactional
  @Override
  public UserExtendEntity updateForm(UserExtendEntity userExtendEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     *
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */

    this.updateValidation(userExtendEntity);
    // ===================基本信息
    String currentId = userExtendEntity.getId();
    Optional<UserExtendEntity> opCurrentStockEntity = this.userExtendEntityRepository.findById(currentId);
    UserExtendEntity currentUserExtendEntity = opCurrentStockEntity.orElse(null);
    Validate.notNull(currentUserExtendEntity, "未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    this.userExtendEntityRepository.saveAndFlush(currentUserExtendEntity);
    return currentUserExtendEntity;
  }

  /**
   * 在更新一个已有的StockEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(UserExtendEntity userExtendEntity) {
    Validate.isTrue(!StringUtils.isBlank(userExtendEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
  }

  @Override
  public UserExtendEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }

    Optional<UserExtendEntity> op = userExtendEntityRepository.findById(id);
    return op.orElse(null);
  }
  @Override
  public UserExtendEntity findByUserId(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }

    UserExtendEntity op = userExtendEntityRepository.findByUserId(id);
    return op;
  }

  @Override
  public List<UserExtendEntity> findAll() {
    return userExtendEntityRepository.findAll();
  }
  @Override
  public List<UserEntity> findUserAll(){
    return userRepository.findAll();
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    UserExtendEntity current = this.findById(id);
    if (current != null) {
      this.userExtendEntityRepository.delete(current);
    }
  }
  @Override
  public void pullMatters() {
    userHrRemoteService.pullMatters();
  }
}
