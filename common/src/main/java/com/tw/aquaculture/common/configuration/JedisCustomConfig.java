//package com.tw.aquaculture.common.configuration;
//
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import com.bizunited.platform.rbac.server.configuration.RedisCustomProperties;
//
//import redis.clients.jedis.JedisPool;
//import redis.clients.jedis.JedisPoolConfig;
//
//@Configuration
//public class JedisCustomConfig {
//
//	private static final Logger LOGGER = LoggerFactory.getLogger(JedisCustomConfig.class);
//
//	@Autowired
//	private RedisCustomProperties redisCustomProperties;
//
//	@Bean
//	public JedisPoolConfig getRedisConfig() {
//		return new JedisPoolConfig();
//	}
//
//	@Bean
//	public JedisPool getJedisPool() {
//		//GenericObjectPoolConfig poolConfig, String host, int port, int timeout, String password, int database
//		JedisPoolConfig config = getRedisConfig();
//		String firstAddress = redisCustomProperties.getAddress()[0];
//		String[] address = firstAddress.split(":");
//		JedisPool jp;
//		if(StringUtils.isBlank(redisCustomProperties.getPassword())){
//			jp = new JedisPool(config, address[0], Integer.valueOf(address[1]));
//		}else{
//			jp = new JedisPool(config, address[0], Integer.valueOf(address[1]),
//							redisCustomProperties.getTimeout(),redisCustomProperties.getPassword(), 0);
//		}
//		LOGGER.debug("此时获取到JEDIS POLL，地址:{},端口:{}", address[0], address[1]);
//		return jp;
//	}
//
//}
