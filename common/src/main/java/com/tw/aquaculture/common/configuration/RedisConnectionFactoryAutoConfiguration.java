package com.tw.aquaculture.common.configuration;

import com.bizunited.platform.rbac.server.configuration.RedisCustomProperties;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * spring session type为redis时，需要 Spring Data Redis,
 * <p>
 * redisson官方有专门的集成方案, 后期研究后酌情替换
 * https://github.com/redisson/redisson/tree/master/redisson-spring-data#spring-data-redis-integration
 * https://github.com/redisson/redisson/tree/master/redisson-spring-boot-starter#spring-boot-starter
 * <p>
 * 目前解决方案, 也就是需要配置Spring Data Redis需要的 {@link RedisConnectionFactory}
 * <p>
 *
 * @author zizhou
 * @date 2019/12/30
 */
@Configuration
@EnableConfigurationProperties({RedisCustomProperties.class})
@ConditionalOnProperty(prefix = "redis.connection-factory", name = "enabled", havingValue = "true", matchIfMissing = true)
public class RedisConnectionFactoryAutoConfiguration {

  @Autowired
  private RedisCustomProperties redisCustomProperties;

  @Bean
  @ConditionalOnMissingBean
  public RedisConnectionFactory connectionFactory() {

    String model = redisCustomProperties.getModel();
    // 单机
    if (StringUtils.equals(model, "single")) {
      RedisStandaloneConfiguration redisClusterConfiguration = new RedisStandaloneConfiguration();

      String[] split = StringUtils.split(redisCustomProperties.getAddress()[0], ":");
      redisClusterConfiguration.setHostName(split[0]);
      redisClusterConfiguration.setPort(Integer.parseInt(split[1]));
      redisClusterConfiguration.setPassword(redisCustomProperties.getPassword());

      return new JedisConnectionFactory(redisClusterConfiguration);
    }

    // 哨兵 不需要支持
    if (StringUtils.equals(model, "sentinel")) {
      throw new UnsupportedOperationException("暂不支持哨兵模式");
    }

    // 集群模式
    if (StringUtils.equals(model, "cluster")) {
      // 集群配置的address是跟上了 redis:// 这个前缀
      String[] address = redisCustomProperties.getAddress();
      List<String> clusterNodes = Arrays.stream(address).map(adr -> StringUtils.replace(adr, "redis://", "")).collect(Collectors.toList());
      RedisClusterConfiguration redisClusterConfiguration = new RedisClusterConfiguration(clusterNodes);
      redisClusterConfiguration.setPassword(redisCustomProperties.getPassword());
      return new JedisConnectionFactory(redisClusterConfiguration);
    }

    throw new IllegalArgumentException("redisson model 配置异常 无法创建bean");
  }

}
