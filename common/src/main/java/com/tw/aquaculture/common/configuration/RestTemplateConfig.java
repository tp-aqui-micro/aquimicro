package com.tw.aquaculture.common.configuration;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {

  @Bean
  RestTemplate restTemplateBean() {
    RestTemplate restTemplate = new RestTemplate();
    FormHttpMessageConverter fc = new FormHttpMessageConverter();
    StringHttpMessageConverter stringConverter =
        new StringHttpMessageConverter(StandardCharsets.UTF_8);
    List<HttpMessageConverter<?>> partConverters = new ArrayList<>();
    partConverters.add(stringConverter);
    partConverters.add(new ResourceHttpMessageConverter());
    fc.setPartConverters(partConverters);
    restTemplate.getMessageConverters().add(fc);
    return restTemplate;
  }
}
