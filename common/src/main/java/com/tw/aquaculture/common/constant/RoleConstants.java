package com.tw.aquaculture.common.constant;

/**
 * 这个常量类用于存放水产系统中特殊角色的标识
 *
 * @author zizhou
 * @date 2019/12/28
 */
public class RoleConstants {

  private RoleConstants() {
    throw new UnsupportedOperationException("不支持实例化");
  }

  /**
   * 拥有此角色的账号在 数据视图查询时，可以查到父级组织和父级组织下所有组织的数据
   */
  public static final String UP_SELECT_ROLE = "UP_SELECT_ROLE";
}
