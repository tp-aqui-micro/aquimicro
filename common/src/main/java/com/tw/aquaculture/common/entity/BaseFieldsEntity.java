package com.tw.aquaculture.common.entity;

import com.bizunited.platform.kuiper.entity.FormInstanceUuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * @author 陈荣
 * @date 2019/11/18 10:11
 */
@MappedSuperclass
public class BaseFieldsEntity extends FormInstanceUuidEntity {

  private static final long serialVersionUID = -8565547849220119877L;

  @ApiModelProperty(name = "createTime", value = "创建时间")
  @Column(name = "create_time", columnDefinition = "datetime COMMENT '创建时间'")
  @SaturnColumn(description = "创建时间")
  private Date createTime = new Date();

  @ApiModelProperty(name = "createName", value = "创建人姓名")
  @Column(name = "create_name", columnDefinition = "varchar(255) COMMENT '创建人姓名'")
  @SaturnColumn(description = "创建人姓名")
  private String createName;

  @ApiModelProperty(name = "createPosition", value = "创建人职位")
  @Column(name = "create_position", columnDefinition = "varchar(255) COMMENT '创建人职位'")
  @SaturnColumn(description = "创建人职位")
  private String createPosition;

  @ApiModelProperty(name = "modifyTime", value = "修改时间")
  @Column(name = "modify_time", columnDefinition = "datetime COMMENT '修改时间'")
  @SaturnColumn(description = "修改时间")
  private Date modifyTime = new Date();

  @ApiModelProperty(name = "modifyName", value = "修改人姓名")
  @Column(name = "modify_name", columnDefinition = "varchar(255) COMMENT '修改人姓名'")
  @SaturnColumn(description = "修改人姓名")
  private String modifyName;

  @ApiModelProperty(name = "modifyPosition", value = "修改人职位")
  @Column(name = "modify_position", columnDefinition = "varchar(255) COMMENT '修改人职位'")
  @SaturnColumn(description = "修改人职位")
  private String modifyPosition;

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public Date getModifyTime() {
    return modifyTime;
  }

  public void setModifyTime(Date modifyTime) {
    this.modifyTime = modifyTime;
  }

  public String getCreateName() {
    return createName;
  }

  public void setCreateName(String createName) {
    this.createName = createName;
  }

  public String getCreatePosition() {
    return createPosition;
  }

  public void setCreatePosition(String createPosition) {
    this.createPosition = createPosition;
  }

  public String getModifyName() {
    return modifyName;
  }

  public void setModifyName(String modifyName) {
    this.modifyName = modifyName;
  }

  public String getModifyPosition() {
    return modifyPosition;
  }

  public void setModifyPosition(String modifyPosition) {
    this.modifyPosition = modifyPosition;
  }
}
