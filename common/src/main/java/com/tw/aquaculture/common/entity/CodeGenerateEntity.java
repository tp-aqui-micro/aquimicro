package com.tw.aquaculture.common.entity;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;

/**
 * @author 陈荣
 * @date 2019/11/22 11:08
 */
@Entity
@Table(name = "tw_code_generate", uniqueConstraints = {
        @UniqueConstraint(name = "unique", columnNames = {"code_type","prefix"})
})
@SaturnDomain("base")
public class CodeGenerateEntity implements Serializable {

  private static final long serialVersionUID = -3849032970010457318L;

  @GeneratedValue(strategy= GenerationType.IDENTITY)
  @Id
  private int id;

  @SaturnColumn(description = "编码类型")
  @Column(name = "code_type", nullable = false, columnDefinition = "varchar(64) comment'编码类型,如BaseEntity'")
  private String codeType;

  @SaturnColumn(description = "前缀")
  @Column(name = "prefix", nullable = false, columnDefinition = "varchar(6) comment'前缀，如ZZ'")
  private String prefix;

  @SaturnColumn(description = "数字长度")
  @Column(name = "length",nullable = false, columnDefinition = "int(11) comment'数字部分长度'")
  private int length;

  @SaturnColumn(description = "值")
  @Column(name = "value", nullable = false, columnDefinition = "bigint(32) comment'最近使用值'")
  private long value;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getCodeType() {
    return codeType;
  }

  public void setCodeType(String codeType) {
    this.codeType = codeType;
  }

  public String getPrefix() {
    return prefix;
  }

  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  public int getLength() {
    return length;
  }

  public void setLength(int length) {
    this.length = length;
  }

  public long getValue() {
    return value;
  }

  public void setValue(long value) {
    this.value = value;
  }
}
