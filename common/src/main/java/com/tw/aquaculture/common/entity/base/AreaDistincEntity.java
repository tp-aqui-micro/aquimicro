package com.tw.aquaculture.common.entity.base;

import com.bizunited.platform.core.entity.UuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author 陈荣
 * @date 2019/12/12 17:50
 */
@Entity
@Table(name = "tw_area_distinc", schema = "行政区域")
@SaturnDomain(value = "base")
@ApiModel(value = "AreaDistincEntity", description = "行政区域")
public class AreaDistincEntity extends UuidEntity {

  private static final long serialVersionUID = -4190861070618552114L;

  @Column(name = "name", columnDefinition = "varchar(32) comment'名称'")
  @SaturnColumn(description = "name")
  @ApiModelProperty(name = "name", value = "name", required = true)
  private String name;

  @Column(name = "parent_id", columnDefinition = "varchar(32) comment''")
  @SaturnColumn(description = "parentId")
  @ApiModelProperty(name = "parentId", value = "parentId", required = true)
  private String parentId;


  @Column(name = "short_name", columnDefinition = "varchar(32) comment''")
  @SaturnColumn(description = "shortName")
  @ApiModelProperty(name = "shortName", value = "shortName", required = true)
  private String shortName;

  @Column(name = "level_type", columnDefinition = "int(2) comment''")
  @SaturnColumn(description = "levelType")
  @ApiModelProperty(name = "levelType", value = "levelType", required = true)
  private Integer levelType;

  @Column(name = "city_code", columnDefinition = "varchar(32) comment''")
  @SaturnColumn(description = "cityCode")
  @ApiModelProperty(name = "cityCode", value = "cityCode", required = true)
  private String cityCode;

  @Column(name = "zip_code", columnDefinition = "varchar(32) comment''")
  @SaturnColumn(description = "zipCode")
  @ApiModelProperty(name = "zipCode", value = "zipCode", required = true)
  private String zipCode;

  @Column(name = "merger_name", columnDefinition = "varchar(255) comment''")
  @SaturnColumn(description = "mergerName")
  @ApiModelProperty(name = "mergerName", value = "mergerName", required = true)
  private String mergerName;

  @Column(name = "lat", columnDefinition = "varchar(32) comment''")
  @SaturnColumn(description = "lat")
  @ApiModelProperty(name = "lat", value = "lat", required = true)
  private String lat;

  @Column(name = "lng", columnDefinition = "varchar(32) comment''")
  @SaturnColumn(description = "lng")
  @ApiModelProperty(name = "lng", value = "lng", required = true)
  private String lng;

  @Column(name = "pingyin", columnDefinition = "varchar(32) comment''")
  @SaturnColumn(description = "pingyin")
  @ApiModelProperty(name = "pingyin", value = "pingyin", required = true)
  private String pingyin;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  public String getShortName() {
    return shortName;
  }

  public void setShortName(String shortName) {
    this.shortName = shortName;
  }

  public Integer getLevelType() {
    return levelType;
  }

  public void setLevelType(Integer levelType) {
    this.levelType = levelType;
  }

  public String getCityCode() {
    return cityCode;
  }

  public void setCityCode(String cityCode) {
    this.cityCode = cityCode;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getMergerName() {
    return mergerName;
  }

  public void setMergerName(String mergerName) {
    this.mergerName = mergerName;
  }

  public String getLat() {
    return lat;
  }

  public void setLat(String lat) {
    this.lat = lat;
  }

  public String getLng() {
    return lng;
  }

  public void setLng(String lng) {
    this.lng = lng;
  }

  public String getPingyin() {
    return pingyin;
  }

  public void setPingyin(String pingyin) {
    this.pingyin = pingyin;
  }
}










