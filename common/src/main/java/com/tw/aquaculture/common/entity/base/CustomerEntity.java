package com.tw.aquaculture.common.entity.base;

import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author 陈荣
 * @date 2019/11/12 10:51
 */
@Entity
@Table(name = "tw_customer", schema = "客户信息")
@SaturnDomain(value = "base")
@ApiModel(value = "CustomerEntity", description = "客户信息")
public class CustomerEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = 6741020637505598145L;

  @Column(name = "code", nullable = false, columnDefinition = "varchar(32) comment'客户编码'")
  @SaturnColumn(description = "客户编码")
  @ApiModelProperty(name = "code", value = "客户编码", required = true)
  private String code;

  @Column(name = "EBS_code", columnDefinition = "varchar(100) comment'EBS编码'")
  @SaturnColumn(description = "EBS编码")
  @ApiModelProperty(name = "ebsCode", value = "EBS编码")
  private String ebsCode;

  @Column(name = "name", nullable = false, columnDefinition = "varchar(32) comment'客户名称'")
  @SaturnColumn(description = "客户名称")
  @ApiModelProperty(name = "name", value = "客户名称", required = true)
  private String name;

  @JoinColumn(name = "company_id", columnDefinition = "varchar(100) comment'组织id'")
  @SaturnColumn(description = "公司")
  @ApiModelProperty(name = "company", value = "公司", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private OrganizationEntity company;

  @Column(name = "customer_type", columnDefinition = "varchar(100) comment'客户类型编码'")
  @SaturnColumn(description = "客户类型")
  @ApiModelProperty(name = "customerType", value = "客户类型", required = true)
  private String customerType;

  @Column(name = "link_name", nullable = false, columnDefinition = "varchar(10) comment'客户联系人'")
  @SaturnColumn(description = "客户联系人")
  @ApiModelProperty(name = "linkName", value = "客户联系人", required = true)
  private String linkName;

  @Column(name = "phone", nullable = false, columnDefinition = "varchar(11) comment'客户电话'")
  @SaturnColumn(description = "客户电话")
  @ApiModelProperty(name = "phone", value = "客户电话", required = true)
  private String phone;

  @JoinColumn(name = "tw_user_id", columnDefinition = "varchar(255) comment'通威联系人id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "联系人")
  @ApiModelProperty(name = "twLinkUser", value = "联系人")
  private UserEntity twLinkUser;

  @Column(name = "enable_state", nullable = false, columnDefinition = "int(11) default 1 comment'生效状态，1：生效，0失效'")
  @SaturnColumn(description = "生效状态")
  @ApiModelProperty(name = "enableState", value = "生效状态", required = true)
  private Integer enableState;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getEbsCode() {
    return ebsCode;
  }

  public void setEbsCode(String ebsCode) {
    this.ebsCode = ebsCode;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public OrganizationEntity getCompany() {
    return company;
  }

  public void setCompany(OrganizationEntity company) {
    this.company = company;
  }

  public String getCustomerType() {
    return customerType;
  }

  public void setCustomerType(String customerType) {
    this.customerType = customerType;
  }

  public String getLinkName() {
    return linkName;
  }

  public void setLinkName(String linkName) {
    this.linkName = linkName;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public UserEntity getTwLinkUser() {
    return twLinkUser;
  }

  public void setTwLinkUser(UserEntity twLinkUser) {
    this.twLinkUser = twLinkUser;
  }

  public Integer getEnableState() {
    return enableState;
  }

  public void setEnableState(Integer enableState) {
    this.enableState = enableState;
  }

}
