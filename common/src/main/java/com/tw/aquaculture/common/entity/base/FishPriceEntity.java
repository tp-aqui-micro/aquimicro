package com.tw.aquaculture.common.entity.base;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * 价格信息
 * @author 陈荣
 * @date 2019/11/12 13:40
 */
@Entity
@Table(name = "tw_fish_price")
@SaturnDomain(value = "base")
@ApiModel(value = "FishPriceEntity", description = "价格")
public class FishPriceEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = -3600314047695798150L;

  @JoinColumn(name = "base_id", nullable = false, columnDefinition = "varchar(255) comment'基地id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "基地")
  @ApiModelProperty(name = "base", value = "基地", required = true)
  private TwBaseEntity base;

  @JoinColumn(name = "fish_type", nullable = false, columnDefinition = "varchar(64) comment'鱼种'")
  @SaturnColumn(description = "鱼种")
  @ApiModelProperty(name = "fishType", value = "鱼种", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private FishTypeEntity fishType;

  @Column(name = "specs_min", nullable = false, columnDefinition = "double(10, 2) comment'规格斤/尾，最小值'")
  @SaturnColumn(description = "最小值")
  @ApiModelProperty(name = "specsMin", value = "最小值", required = true)
  private Double specsMin;

  @Column(name = "specs_max", nullable = false, columnDefinition = "double(10, 2) comment'规格斤/尾，最小值'")
  @SaturnColumn(description = "最大值")
  @ApiModelProperty(name = "specsMax", value = "最大值", required = true)
  private Double specsMax;

  @Column(name = "price", nullable = false, columnDefinition = "double(10, 2) comment'价格'")
  @SaturnColumn(description = "价格")
  @ApiModelProperty(name = "price", value = "价格", required = true)
  private Double price;

  @Column(name = "unit", columnDefinition = "varchar(100) comment'计量单位'")
  @SaturnColumn(description = "计量单位")
  @ApiModelProperty(name = "unit", value = "计量单位")
  private String unit;

  @Column(name = "start_time", nullable = false, columnDefinition = "datetime default current_timestamp comment'开始时间'")
  @SaturnColumn(description = "开始时间")
  @ApiModelProperty(name = "startTime", value = "开始时间")
  private Date startTime;

  @Column(name = "end_time", nullable = false, columnDefinition = "datetime default current_timestamp comment'结束时间'")
  @SaturnColumn(description = "结束时间")
  @ApiModelProperty(name = "endTime", value = "结束时间", required = true)
  private Date endTime;

  @Column(name = "province", nullable = false, columnDefinition = "varchar(100) comment'省'")
  @SaturnColumn(description = "省")
  @ApiModelProperty(name = "province", value = "省", required = true)
  private String province;

  @Column(name = "city", nullable = false, columnDefinition = "varchar(100) comment'市'")
  @SaturnColumn(description = "市")
  @ApiModelProperty(name = "city", value = "市", required = true)
  private String city;

  @Column(name = "area", columnDefinition = "varchar(100) comment'区/县'")
  @SaturnColumn(description = "区/县")
  @ApiModelProperty(name = "area", value = "区/县", required = true)
  private String area;

  @Column(name = "enable_state", nullable = false, columnDefinition = "int(11) default 1 comment'生效状态，1：生效，0失效'")
  @SaturnColumn(description = "生效状态")
  @ApiModelProperty(name = "enableState", value = "生效状态",required = true)
  private Integer enableState;

  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public FishTypeEntity getFishType() {
    return fishType;
  }

  public void setFishType(FishTypeEntity fishType) {
    this.fishType = fishType;
  }

  public Double getSpecsMin() {
    return specsMin;
  }

  public void setSpecsMin(Double specsMin) {
    this.specsMin = specsMin;
  }

  public Double getSpecsMax() {
    return specsMax;
  }

  public void setSpecsMax(Double specsMax) {
    this.specsMax = specsMax;
  }

  public Double getPrice() {
    return price;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }

  public Integer getEnableState() {
    return enableState;
  }

  public void setEnableState(Integer enableState) {
    this.enableState = enableState;
  }

}
