package com.tw.aquaculture.common.entity.base;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author 陈荣
 * @date 2019/11/15 18:52
 */
@Entity
@Table(name = "tw_fish_type")
@ApiModel(description = "鱼种", value = "FishTypeEntity")
@SaturnDomain(value = "base")
public class FishTypeEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = -4440510682905160303L;

  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment'鱼种编码'")
  @SaturnColumn(description = "鱼种编码")
  @ApiModelProperty(name = "code", value = "鱼种编码", required = true)
  private String code;

  @Column(name = "name", nullable = false, columnDefinition = "varchar(100) comment'鱼种名称'")
  @SaturnColumn(description = "鱼种名称")
  @ApiModelProperty(name = "name", value = "鱼种名称", required = true)
  private String name;

  @Column(name = "enable_state", nullable = false, columnDefinition = "int(11) comment'生效状态，1：生效，0：失效'")
  @SaturnColumn(description = "生效状态")
  @ApiModelProperty(name = "enableState", value = "生效状态，1：生效，0：失效", required = true)
  private int enableState;

  @Column(name = "note", nullable = false, columnDefinition = "varchar(10000) comment'备注'")
  @SaturnColumn(description = "备注")
  @ApiModelProperty(name = "code", value = "备注", required = true)
  private String note;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getEnableState() {
    return enableState;
  }

  public void setEnableState(int enableState) {
    this.enableState = enableState;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }
}
