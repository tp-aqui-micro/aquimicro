package com.tw.aquaculture.common.entity.base;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by chenrong on 2020/1/15
 */
@ApiModel(value = "FishUpRateEntity", description = "增长率")
@SaturnDomain(value = "base")
@Entity
@Table(name = "tw_fish_up_rate")
public class FishUpRateEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = 664668701067448561L;

  @ApiModelProperty(name = "pondType", value = "塘口分类", required = true)
  @SaturnColumn(description = "塘口分类")
  @JoinColumn(name = "pond_type_id", nullable = false, columnDefinition = "varchar(255) comment'塘口分类id'")
  @ManyToOne(fetch = FetchType.LAZY)
  private PondTypeEntity pondType;

  @ApiModelProperty(name = "fishType", required = true, value = "鱼种")
  @SaturnColumn(description = "鱼种")
  @JoinColumn(name = "fish_type_id", nullable = false, columnDefinition = "varchar(255) comment'鱼种id'")
  @ManyToOne(fetch = FetchType.LAZY)
  private FishTypeEntity fishType;

  @ApiModelProperty(name = "rate", required = true, value = "增长率")
  @SaturnColumn(description = "增长率")
  @Column(name = "rate", nullable = false, columnDefinition = "double(14, 6) comment'增长率'")
  private Double rate;

  public PondTypeEntity getPondType() {
    return pondType;
  }

  public void setPondType(PondTypeEntity pondType) {
    this.pondType = pondType;
  }

  public FishTypeEntity getFishType() {
    return fishType;
  }

  public void setFishType(FishTypeEntity fishType) {
    this.fishType = fishType;
  }

  public Double getRate() {
    return rate;
  }

  public void setRate(Double rate) {
    this.rate = rate;
  }
}
