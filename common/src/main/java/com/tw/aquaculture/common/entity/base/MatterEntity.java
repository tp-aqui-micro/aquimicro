package com.tw.aquaculture.common.entity.base;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Date;

/**
 * 物料
 * @author 陈荣
 * @date 2019/11/12 13:08
 */
@Entity
@Table(name = "tw_matter", uniqueConstraints = {
        @UniqueConstraint(columnNames={"code"}),
        @UniqueConstraint(columnNames={"ebs_code", "ebs_org_code"})
})
@SaturnDomain(value = "base")
@ApiModel(value = "MatterEntity", description = "物料")
public class MatterEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = -3257544431762744392L;

  @Column(name = "code", nullable = false, columnDefinition = "varchar(100) comment'物料编码'")
  @SaturnColumn(description = "物料编码")
  @ApiModelProperty(name = "code", value = "物料编码", required = true)
  private String code;

  @Column(name = "ebs_code", nullable = false, columnDefinition = "varchar(100) comment'EBS物料编码'")
  @SaturnColumn(description = "物料编码")
  @ApiModelProperty(name = "ebsCode", value = "EBS物料编码", required = true)
  private String ebsCode;

  @Column(name = "name", nullable = false, columnDefinition = "varchar(64) comment'物料名称'")
  @SaturnColumn(description = "物料名称")
  @ApiModelProperty(name = "name", value = "物料名称", required = true)
  private String name;

  @Column(name = "matter_type_code", nullable = false, columnDefinition = "varchar(255) comment'物料类型'")
  @SaturnColumn(description = "物料类型")
  @ApiModelProperty(name = "matterTypeCode", value = "物料类型", required = true)
  private String matterTypeCode;

  @Column(name = "matter_type_name", columnDefinition = "varchar(255) comment'物料类型描述'")
  @SaturnColumn(description = "物料类型")
  @ApiModelProperty(name = "matterTypeName", value = "物料类型", required = true)
  private String matterTypeName;

  @Column(name = "matter_type_second_code", columnDefinition = "varchar(255) comment'物料类型中类'")
  @SaturnColumn(description = "物料类型中类")
  @ApiModelProperty(name = "matterTypeSecondCode", value = "物料类型中类", required = true)
  private String matterTypeSecondCode;

  @Column(name = "matter_type_second_name", columnDefinition = "varchar(255) comment'物料类型中类描述'")
  @SaturnColumn(description = "物料类型中类")
  @ApiModelProperty(name = "matterTypeSecondName", value = "物料类型中类", required = true)
  private String matterTypeSecondName;

  @Column(name = "matter_type_third_code", nullable = false, columnDefinition = "varchar(255) comment'物料类型大类'")
  @SaturnColumn(description = "物料类型大类")
  @ApiModelProperty(name = "matterTypeThirdCode", value = "物料类型大类", required = true)
  private String matterTypeThirdCode;

  @Column(name = "matter_type_third_name", columnDefinition = "varchar(255) comment'物料类型大类描述'")
  @SaturnColumn(description = "物料类型大类")
  @ApiModelProperty(name = "matterTypeThirdName", value = "物料类型大类", required = true)
  private String matterTypeThirdName;

  @Column(name = "matter_host_unit_code", columnDefinition = "varchar(100) comment'主单位编码'")
  @SaturnColumn(description = "主单位编码")
  @ApiModelProperty(name = "matterHostUnitCode", value = "主单位编码")
  private String matterHostUnitCode;

  @Column(name = "matter_host_unit", columnDefinition = "varchar(100) comment'主单位'")
  @SaturnColumn(description = "主单位")
  @ApiModelProperty(name = "matterHostUnit", value = "主单位")
  private String matterHostUnit;

  @Column(name = "matter_assist_unit_code", columnDefinition = "varchar(100) comment'辅单位编码'")
  @SaturnColumn(description = "辅单位编码")
  @ApiModelProperty(name = "matterAssistUnitCode", value = "辅单位编码")
  private String matterAssistUnitCode;

  @Column(name = "matter_assist_unit", columnDefinition = "varchar(100) comment'辅单位'")
  @SaturnColumn(description = "辅单位")
  @ApiModelProperty(name = "matterAssistUnit", value = "辅单位")
  private String matterAssistUnit;

  @Column(name = "specs", columnDefinition = "varchar(100) comment'规格，如：50kg/袋'")
  @SaturnColumn(description = "规格")
  @ApiModelProperty(name = "specs", value = "规格")
  private String specs;

  @Column(name = "enable_state", nullable = false, columnDefinition = "int(11) default 1 comment'生效状态，1：生效，0失效'")
  @SaturnColumn(description = "生效状态")
  @ApiModelProperty(name = "enableState", value = "生效状态", required = true)
  private Integer enableState;

  @Column(name = "scale_factor", columnDefinition = "double(10, 2) comment'换算系数'")
  @SaturnColumn(description = "换算系数")
  @ApiModelProperty(name = "scaleFactor", value = "换算系数")
  private String scaleFactor;

  @Column(name = "unit_code", columnDefinition = "varchar(100) comment'计量单位编码'")
  @SaturnColumn(description = "计量单位编码")
  @ApiModelProperty(name = "unitCode", value = "计量单位编码")
  private String unitCode;

  @Column(name = "unit_name", columnDefinition = "varchar(100) comment'计量单位'")
  @SaturnColumn(description = "计量单位")
  @ApiModelProperty(name = "unitCode", value = "计量单位")
  private String unitName;

  @Column(name = "pice_weight", columnDefinition = "varchar(100) comment'件重'")
  @SaturnColumn(description = "件重")
  @ApiModelProperty(name = "piceWeight", value = "件重")
  private String piceWeight;

  @Column(name = "last_update_time", columnDefinition = "datetime comment'最后一次更新时间'")
  @SaturnColumn(description = "最后一次更新时间")
  @ApiModelProperty(name = "lastUpdateTime", value = "最后一次更新时间")
  private Date lastUpdateTime;

  @Column(name = "company_code", columnDefinition = "varchar(100) comment'公司编码'")
  @SaturnColumn(description = "公司编码")
  @ApiModelProperty(name = "companyCode", value = "公司编码")
  private String companyCode;

  @Column(name = "company_id", columnDefinition = "int(11) comment'公司id'")
  @SaturnColumn(description = "公司id")
  @ApiModelProperty(name = "companyId", value = "公司id")
  private Integer companyId;

  @Column(name = "company_name", columnDefinition = "varchar(100) comment'公司名称'")
  @SaturnColumn(description = "公司名称")
  @ApiModelProperty(name = "companyName", value = "公司名称")
  private String companyName;

  @Column(name = "aqui_state", columnDefinition = "int(2) default 1 comment'是否水产养殖物料，0：不是，1：是'")
  @SaturnColumn(description = "是否水产养殖物料")
  @ApiModelProperty(name = "companyId", value = "是否水产养殖物料")
  private Integer aquiState;

  /**库存组织id*/
  @Column(name = "ebs_org_id", columnDefinition = "varchar(255) comment'库存组织id'")
  @SaturnColumn(description = "库存组织id")
  @ApiModelProperty(name = "ebsOrgId", value = "库存组织id")
  private String ebsOrgId;

  /**库存组织编码*/
  @Column(name = "ebs_org_code", columnDefinition = "varchar(255) comment'库存组织编码'")
  @SaturnColumn(description = "库存组织编码")
  @ApiModelProperty(name = "ebsOrgCode", value = "库存组织编码")
  private String ebsOrgCode;

  /**库存组织名称*/
  @Column(name = "ebs_org_name", columnDefinition = "varchar(255) comment'库存组织名称'")
  @SaturnColumn(description = "库存组织名称")
  @ApiModelProperty(name = "ebsOrgName", value = "库存组织名称")
  private String ebsOrgName;

  public Integer getAquiState() {
    return aquiState;
  }

  public void setAquiState(Integer aquiState) {
    this.aquiState = aquiState;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getMatterTypeCode() {
    return matterTypeCode;
  }

  public void setMatterTypeCode(String matterTypeCode) {
    this.matterTypeCode = matterTypeCode;
  }

  public String getMatterHostUnit() {
    return matterHostUnit;
  }

  public void setMatterHostUnit(String matterHostUnit) {
    this.matterHostUnit = matterHostUnit;
  }

  public String getMatterAssistUnit() {
    return matterAssistUnit;
  }

  public void setMatterAssistUnit(String matterAssistUnit) {
    this.matterAssistUnit = matterAssistUnit;
  }

  public String getSpecs() {
    return specs;
  }

  public void setSpecs(String specs) {
    this.specs = specs;
  }

  public Integer getEnableState() {
    return enableState;
  }

  public void setEnableState(Integer enableState) {
    this.enableState = enableState;
  }

  public String getScaleFactor() {
    return scaleFactor;
  }

  public void setScaleFactor(String scaleFactor) {
    this.scaleFactor = scaleFactor;
  }

  public String getUnitCode() {
    return unitCode;
  }

  public void setUnitCode(String unitCode) {
    this.unitCode = unitCode;
  }

  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }

  public String getMatterTypeName() {
    return matterTypeName;
  }

  public void setMatterTypeName(String matterTypeName) {
    this.matterTypeName = matterTypeName;
  }

  public String getMatterTypeSecondCode() {
    return matterTypeSecondCode;
  }

  public void setMatterTypeSecondCode(String matterTypeSecondCode) {
    this.matterTypeSecondCode = matterTypeSecondCode;
  }

  public String getMatterTypeSecondName() {
    return matterTypeSecondName;
  }

  public void setMatterTypeSecondName(String matterTypeSecondName) {
    this.matterTypeSecondName = matterTypeSecondName;
  }

  public String getMatterTypeThirdCode() {
    return matterTypeThirdCode;
  }

  public void setMatterTypeThirdCode(String matterTypeThirdCode) {
    this.matterTypeThirdCode = matterTypeThirdCode;
  }

  public String getMatterTypeThirdName() {
    return matterTypeThirdName;
  }

  public void setMatterTypeThirdName(String matterTypeThirdName) {
    this.matterTypeThirdName = matterTypeThirdName;
  }

  public String getMatterHostUnitCode() {
    return matterHostUnitCode;
  }

  public void setMatterHostUnitCode(String matterHostUnitCode) {
    this.matterHostUnitCode = matterHostUnitCode;
  }

  public String getMatterAssistUnitCode() {
    return matterAssistUnitCode;
  }

  public void setMatterAssistUnitCode(String matterAssistUnitCode) {
    this.matterAssistUnitCode = matterAssistUnitCode;
  }

  public String getPiceWeight() {
    return piceWeight;
  }

  public void setPiceWeight(String piceWeight) {
    this.piceWeight = piceWeight;
  }

  public Date getLastUpdateTime() {
    return lastUpdateTime;
  }

  public void setLastUpdateTime(Date lastUpdateTime) {
    this.lastUpdateTime = lastUpdateTime;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public Integer getCompanyId() {
    return companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getEbsOrgId() {
    return ebsOrgId;
  }

  public void setEbsOrgId(String ebsOrgId) {
    this.ebsOrgId = ebsOrgId;
  }

  public String getEbsOrgCode() {
    return ebsOrgCode;
  }

  public void setEbsOrgCode(String ebsOrgCode) {
    this.ebsOrgCode = ebsOrgCode;
  }

  public String getEbsOrgName() {
    return ebsOrgName;
  }

  public void setEbsOrgName(String ebsOrgName) {
    this.ebsOrgName = ebsOrgName;
  }

  public String getEbsCode() {
    return ebsCode;
  }

  public void setEbsCode(String ebsCode) {
    this.ebsCode = ebsCode;
  }
}
