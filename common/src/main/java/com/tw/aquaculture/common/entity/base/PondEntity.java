package com.tw.aquaculture.common.entity.base;

import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Set;

/**
 * 塘口
 * @author 陈荣
 * @date 2019/11/11 14:51
 */
@Entity
@Table(name = "tw_pond")
@SaturnDomain(value = "base")
@ApiModel(description = "塘口", value = "PondEntity")
public class PondEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = 5893922452723337424L;

  @ApiModelProperty(value = "code")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment '编码'")
  @SaturnColumn(description = "编码")
  private String code;

  @ApiModelProperty(value = "塘口名称")
  @Column(name = "name", nullable = false, columnDefinition = "varchar(100) comment '塘口名称'")
  @SaturnColumn(description = "塘口名称")
  private String name;

  @ApiModelProperty(value = "EBS编码")
  @Column(name = "ebs_code", columnDefinition = "varchar(100) comment 'EBS编码'")
  @SaturnColumn(description = "EBS编码")
  private String ebsCode;

  @ApiModelProperty(value = "基地")
  @JoinColumn(name = "base_id", nullable = false, columnDefinition = "varchar(255) comment '基地id'")
  @SaturnColumn(description = "基地")
  @ManyToOne(fetch = FetchType.LAZY)
  private TwBaseEntity base;

  @ApiModelProperty(value = "养殖小组")
  @JoinColumn(name = "tw_group_id", columnDefinition = "varchar(255) comment '养殖小组id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "养殖小组")
  private TwGroupEntity tgroup;

  @ApiModelProperty(value = "养殖小组组长")
  @JoinColumn(name = "group_leader", columnDefinition = "varchar(255) comment '养殖小组组长'")
  @SaturnColumn(description = "养殖小组组长")
  @ManyToOne(fetch = FetchType.LAZY)
  private UserEntity groupLeader;

  @ApiModelProperty(value = "塘口状态，0：正常，1：发病，2：闲置")
  @Column(name = "state", columnDefinition = "int(11) comment '塘口状态，0：正常，1：发病，2：闲置'")
  @SaturnColumn(description = "塘口状态")
  private Integer state;

  @ApiModelProperty(value = "养殖工人")
  @JoinColumn(name = "worker", columnDefinition = "varchar(100) comment '养殖工人'")
  @SaturnColumn(description = "养殖工人")
  @ManyToOne(fetch = FetchType.LAZY)
  private UserEntity worker;

  @ApiModelProperty(value = "技术员")
  @JoinColumn(name = "technician", columnDefinition = "varchar(100) comment '技术员'")
  @SaturnColumn(description = "技术员")
  @ManyToOne(fetch = FetchType.LAZY)
  private UserEntity technician;

  @ApiModelProperty(value = "塘口面积（亩）")
  @Column(name = "pond_size", columnDefinition = "double(10, 2) comment '塘口面积(亩)'")
  @SaturnColumn(description = "塘口面积(亩)")
  private Double pondSize;

  @JoinColumn(name = "share_model_id", columnDefinition = "varchar(255) comment'养殖分摊模式id'")
  @SaturnColumn(description = "养殖分摊模式")
  @ApiModelProperty(name = "shareModel", value = "养殖分摊模式")
  private ShareModelEntity shareModel;

  @ApiModelProperty(value = "启用状态，1：1启用，0禁用")
  @Column(name = "enable_state", columnDefinition = "int(11) comment '启用状态，1：1启用，0禁用'")
  @SaturnColumn(description = "启用状态")
  private Integer enableState;

  @Column(name = "water_deep", columnDefinition = "double(10,2) comment'水深'")
  @SaturnColumn(description = "水深")
  @ApiModelProperty(name = "waterDeep", value = "水深")
  private Double waterDeep;

  @ApiModelProperty(value = "仓库")
  @ManyToMany
  @JoinTable(name="pond_store_mapping",	//用来指定中间表的名称
          //用于指定本表在中间表的字段名称，以及中间表依赖的是本表的哪个字段
          joinColumns= {@JoinColumn(name="pond_id",referencedColumnName="id")},
          //用于指定对方表在中间表的字段名称，以及中间表依赖的是它的哪个字段
          inverseJoinColumns= {@JoinColumn(name="store_id",referencedColumnName="id")}
  )
  @SaturnColumn(description = "仓库")
  private Set<StoreEntity> stores;

  @ApiModelProperty(name = "modelTypeCode", value = "模式类型编码")
  @Column(name = "model_type_code", columnDefinition = "varchar(255) comment'模式类型编码'")
  @SaturnColumn(description = "模式类型编码")
  private String modelTypeCode;

  @ApiModelProperty(name = "pondType", value = "塘口分类")
  @SaturnColumn(description = "塘口分类")
  @JoinColumn(name = "pond_type_id", columnDefinition = "varchar(255) comment'塘口分类id'")
  @ManyToOne(fetch = FetchType.LAZY)
  private PondTypeEntity pondType;

  public String getModelTypeCode() {
    return modelTypeCode;
  }

  public void setModelTypeCode(String modelTypeCode) {
    this.modelTypeCode = modelTypeCode;
  }

  public Set<StoreEntity> getStores() {
    return stores;
  }

  public void setStores(Set<StoreEntity> stores) {
    this.stores = stores;
  }

  public TwGroupEntity getTgroup() {
    return tgroup;
  }

  public void setTgroup(TwGroupEntity tgroup) {
    this.tgroup = tgroup;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public ShareModelEntity getShareModel() {
    return shareModel;
  }

  public void setShareModel(ShareModelEntity shareModel) {
    this.shareModel = shareModel;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEbsCode() {
    return ebsCode;
  }

  public void setEbsCode(String ebsCode) {
    this.ebsCode = ebsCode;
  }

  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public UserEntity getGroupLeader() {
    return groupLeader;
  }

  public void setGroupLeader(UserEntity groupLeader) {
    this.groupLeader = groupLeader;
  }

  public Integer getState() {
    return state;
  }

  public void setState(Integer state) {
    this.state = state;
  }

  public UserEntity getWorker() {
    return worker;
  }

  public void setWorker(UserEntity worker) {
    this.worker = worker;
  }

  public UserEntity getTechnician() {
    return technician;
  }

  public void setTechnician(UserEntity technician) {
    this.technician = technician;
  }

  public Double getPondSize() {
    return pondSize;
  }

  public void setPondSize(Double pondSize) {
    this.pondSize = pondSize;
  }

  public Integer getEnableState() {
    return enableState;
  }

  public void setEnableState(Integer enableState) {
    this.enableState = enableState;
  }

  public Double getWaterDeep() {
    return waterDeep;
  }

  public void setWaterDeep(Double waterDeep) {
    this.waterDeep = waterDeep;
  }

  public PondTypeEntity getPondType() {
    return pondType;
  }

  public void setPondType(PondTypeEntity pondType) {
    this.pondType = pondType;
  }
}
