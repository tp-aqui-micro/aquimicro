package com.tw.aquaculture.common.entity.base;

import com.bizunited.platform.kuiper.entity.FormInstanceUuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by chenrong on 2020/1/15
 */
@ApiModel(value = "PondTypeEntity", description = "塘口类型")
@SaturnDomain(value = "base")
@Entity
@Table(name = "tw_pond_type")
public class PondTypeEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = 2285585809362928917L;

  @ApiModelProperty(name = "name", required = true, value = "名称")
  @SaturnColumn(description = "名称")
  @Column(name = "name", nullable = false, columnDefinition = "varchar(255) comment'名称'")
  private String name;

  @ApiModelProperty(name = "typeForm", required = true, value = "分类类型")
  @SaturnColumn(description = "分类类型")
  @Column(name = "type_form", nullable = false, columnDefinition = "varchar(255) comment'分类类型'")
  private String typeForm;

  @ApiModelProperty(name = "parent", value = "父级分类")
  @SaturnColumn(description = "父级分类")
  @JoinColumn(name = "parent_id", columnDefinition = "varchar(255) comment'父级分类'")
  @ManyToOne(fetch = FetchType.LAZY)
  private PondTypeEntity parent;

  @ApiModelProperty(name = "enableState", required = true, value = "生效状态,0:失效，1生效")
  @SaturnColumn(description = "生效状态,0:失效，1生效")
  @Column(name = "enable_state", nullable = false, columnDefinition = "int(1) comment'生效状态,0:失效，1生效'")
  private int enableState;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTypeForm() {
    return typeForm;
  }

  public void setTypeForm(String typeForm) {
    this.typeForm = typeForm;
  }

  public PondTypeEntity getParent() {
    return parent;
  }

  public void setParent(PondTypeEntity parent) {
    this.parent = parent;
  }

  public int getEnableState() {
    return enableState;
  }

  public void setEnableState(int enableState) {
    this.enableState = enableState;
  }
}
