package com.tw.aquaculture.common.entity.base;

import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.kuiper.entity.FormInstanceUuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * 产品
 * @author 陈荣
 * @date 2019/11/12 11:26
 */
@Entity
@Table(name = "tw_product")
@SaturnDomain(value = "base")
@ApiModel(description = "产品", value = "ProductEntity")
public class ProductEntity extends FormInstanceUuidEntity {

  private static final long serialVersionUID = 3646928572413902137L;

  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment'产品编码'")
  @SaturnColumn(description = "产品编码")
  @ApiModelProperty(name = "code", value = "产品编码", required = true)
  private String code;

  @Column(name = "EBS_code", nullable = false, columnDefinition = "varchar(64) comment'EBS编码'")
  @SaturnColumn(description = "EBS编码")
  @ApiModelProperty(name = "ebsCode", value = "EBS编码", required = true)
  private String ebsCode;

  @Column(name = "name", nullable = false, columnDefinition = "varchar(100) comment'产品名称'")
  @SaturnColumn(description = "产品名称")
  @ApiModelProperty(name = "name", value = "产品名称", required = true)
  private String name;

  @Column(name = "breed_code", nullable = false, columnDefinition = "varchar(64) comment'品种code'")
  @SaturnColumn(description = "品种")
  @ApiModelProperty(name = "breedCode", value = "品种code", required = true)
  private String breedCode;

  @Column(name = "min_weight", nullable = false, columnDefinition = "double(10, 2) comment'最小单条重量'")
  @SaturnColumn(description = "规格小值（斤/尾）")
  @ApiModelProperty(name = "minWeight", value = "规格小值（斤/尾）")
  private Double minWeight;

  @Column(name = "max_weight", nullable = false, columnDefinition = "double(10, 2) comment'最大单条重量'")
  @SaturnColumn(description = "规格大值（斤/尾）")
  @ApiModelProperty(name = "maxWeight", value = "规格大值（斤/尾）", required = true)
  private Double maxWeight;

  @Column(name = "note", columnDefinition = "varchar(512) comment'备注'")
  @SaturnColumn(description = "备注")
  @ApiModelProperty(name = "note", value = "备注")
  private String note;

  @Column(name = "enable_state", nullable = false, columnDefinition = "int(11) default 1 comment'生效状态，1：生效，0失效'")
  @SaturnColumn(description = "生效状态")
  @ApiModelProperty(name = "enableState", value = "生效状态", required = true)
  private Integer enableState;

  @Column(name = "creater", columnDefinition = "varchar(100) comment'创建人'")
  @SaturnColumn(description = "创建人")
  @ApiModelProperty(name = "creater", value = "创建人")
  private UserEntity creater;

  @Column(name = "create_time", columnDefinition = "datetime default current_timestamp comment'创建时间'")
  @SaturnColumn(description = "创建时间")
  @ApiModelProperty(name = "createTime", value = "创建时间")
  private Date createTime;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getEbsCode() {
    return ebsCode;
  }

  public void setEbsCode(String ebsCode) {
    this.ebsCode = ebsCode;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBreedCode() {
    return breedCode;
  }

  public void setBreedCode(String breedCode) {
    this.breedCode = breedCode;
  }

  public Double getMinWeight() {
    return minWeight;
  }

  public void setMinWeight(Double minWeight) {
    this.minWeight = minWeight;
  }

  public Double getMaxWeight() {
    return maxWeight;
  }

  public void setMaxWeight(Double maxWeight) {
    this.maxWeight = maxWeight;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public Integer getEnableState() {
    return enableState;
  }

  public void setEnableState(Integer enableState) {
    this.enableState = enableState;
  }

  public UserEntity getCreater() {
    return creater;
  }

  public void setCreater(UserEntity creater) {
    this.creater = creater;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }
}
