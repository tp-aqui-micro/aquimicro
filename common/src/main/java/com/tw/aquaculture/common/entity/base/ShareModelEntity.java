package com.tw.aquaculture.common.entity.base;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

/**
 * 养殖分摊模式
 * @author 陈荣
 * @date 2019/11/12 14:16
 */
@Entity
@Table(name = "tw_share_model")
@SaturnDomain(value = "base")
@ApiModel(value = "ShareModelEntity", description = "养殖分摊模式")
public class ShareModelEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = -2045177332365009239L;

  @ApiModelProperty(name = "code", value = "编码")
  @SaturnColumn(description = "编码")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment '编码'")
  private String code;

  @Column(name = "name", nullable = false, columnDefinition = "varchar(100) comment'模式名称'")
  @SaturnColumn(description = "模式名称")
  @ApiModelProperty(name = "name", value = "模式名称")
  private String name;

  @SaturnColumn(description = "分摊模式明细")
  @ApiModelProperty(name = "shareModelItem", value = "分摊模式明细")
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "shareModel")
  private Set<ShareModelItemEntity> shareModelItems;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<ShareModelItemEntity> getShareModelItems() {
    return shareModelItems;
  }

  public void setShareModelItems(Set<ShareModelItemEntity> shareModelItems) {
    this.shareModelItems = shareModelItems;
  }
}
