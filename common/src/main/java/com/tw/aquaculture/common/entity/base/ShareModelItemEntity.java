package com.tw.aquaculture.common.entity.base;

import com.bizunited.platform.core.entity.UuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author 陈荣
 * @date 2019/11/15 17:12
 */
@Entity
@Table(name = "tw_share_model_item")
@SaturnDomain(value = "base")
@ApiModel(value = "ShareModelItemEntity", description = "养殖分摊模式行项目")
public class ShareModelItemEntity extends UuidEntity {

  private static final long serialVersionUID = -9129596582350475608L;

  @JoinColumn(name = "fish_type", nullable = false, columnDefinition = "varchar(100) comment'鱼种'")
  @SaturnColumn(description = "鱼种")
  @ApiModelProperty(name = "fishType", value = "鱼种", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private FishTypeEntity fishType;

  @Column(name = "fish_state", nullable = false, columnDefinition = "int(11) comment'是否主养鱼，1：是，0：否'")
  @SaturnColumn(description = "是否主养鱼")
  @ApiModelProperty(name = "name", value = "是否主养鱼，1：是，0：否")
  private int fishState;

  @Column(name = "ratio", nullable = false, columnDefinition = "varchar(100) comment'比例'")
  @SaturnColumn(description = "比例")
  @ApiModelProperty(name = "name", value = "比例")
  private Double ratio;

  @JoinColumn(name ="share_model_id", nullable = false, columnDefinition = "varchar(64) comment'分摊模式id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "分摊模式")
  @ApiModelProperty(name = "shareModel", value = "分摊模式")
  private ShareModelEntity shareModel;

  public FishTypeEntity getFishType() {
    return fishType;
  }

  public void setFishType(FishTypeEntity fishType) {
    this.fishType = fishType;
  }

  public int getFishState() {
    return fishState;
  }

  public void setFishState(int fishState) {
    this.fishState = fishState;
  }

  public Double getRatio() {
    return ratio;
  }

  public void setRatio(Double ratio) {
    this.ratio = ratio;
  }

  public ShareModelEntity getShareModel() {
    return shareModel;
  }

  public void setShareModel(ShareModelEntity shareModel) {
    this.shareModel = shareModel;
  }
}
