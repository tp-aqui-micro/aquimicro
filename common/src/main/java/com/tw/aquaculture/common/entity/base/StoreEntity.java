package com.tw.aquaculture.common.entity.base;

import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Set;

/**
 * 仓库管理管理
 *
 * @author bo shi
 */
@ApiModel(value = "StoreEntity", description = "仓库管理")
@Entity
@Table(name = "tw_store")
@SaturnDomain(value = "base")
public class StoreEntity extends BaseFieldsEntity {
 /**
  * 
  */
  private static final long serialVersionUID = -4516755241699814699L;

  @ApiModelProperty(name = "code", value = "编码", required = true)
  @SaturnColumn(description = "编码")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(255) comment '仓库编码'")
  private String code;

  @ApiModelProperty(name = "name", value = "仓库编码")
  @SaturnColumn(description = "仓库名称")
  @Column(name = "name", columnDefinition = "varchar(128) comment '仓库名称'")
  private String name;

  @ApiModelProperty(name = "company", value = "公司")
  @JoinColumn(name = "org_id", columnDefinition = "varchar(255) comment'公司id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "公司id")
  private OrganizationEntity company;

  @ApiModelProperty(name = "twBase", value = "基地")
  @JoinColumn(name = "tw_base_id", columnDefinition = "varchar(255) comment'基地id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "基地id")
  private TwBaseEntity twBase;

  @ApiModelProperty(name = "storeType", value = "仓库类型(字典表编码)")
  @SaturnColumn(description = "仓库类型(字典表编码)")
  @Column(name = "storeType", columnDefinition = "varchar(255) comment '仓库类型(字典表编码)'")
  private String storeType;

  @ApiModelProperty(name = "state", value = "生效状态 0-禁用 1-启用")
  @SaturnColumn(description = "生效状态 0-禁用 1-启用")
  @Column(name = "state", columnDefinition = "bool comment '生效状态 0-禁用 1-启用'")
  private boolean state;

  @ApiModelProperty(name = "ponds", value = "塘口")
  @SaturnColumn(description = "塘口")
  @ManyToMany(mappedBy="stores")
  private Set<PondEntity> ponds;

  public Set<PondEntity> getPonds() {
    return ponds;
  }

  public void setPonds(Set<PondEntity> ponds) {
    this.ponds = ponds;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public OrganizationEntity getCompany() {
    return company;
  }

  public void setCompany(OrganizationEntity company) {
    this.company = company;
  }

  public TwBaseEntity getTwBase() {
    return twBase;
  }

  public void setTwBase(TwBaseEntity twBase) {
    this.twBase = twBase;
  }

  public String getStoreType() {
    return storeType;
  }

  public void setStoreType(String storeType) {
    this.storeType = storeType;
  }

  public boolean getState() {
    return state;
  }

  public void setState(boolean state) {
    this.state = state;
  }

}
