package com.tw.aquaculture.common.entity.base;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author 陈荣
 * @date 2019/11/12 13:28
 */
@Entity
@Table(name = "tw_test_index")
@SaturnDomain(value = "base")
@ApiModel(description = "检测指标", value = "TestIndexEentity")
public class TestIndexEentity extends BaseFieldsEntity {

  private static final long serialVersionUID = 3639255370967012342L;

  @Column(name = "code", nullable = false, columnDefinition = "varchar(100) comment'检测指标编码'")
  @SaturnColumn(description = "检测指标编码")
  @ApiModelProperty(name = "code", value = "检测指标编码", required = true)
  private String code;

  @Column(name = "name", nullable = false, columnDefinition = "varchar(100) comment'检测指标名称'")
  @SaturnColumn(description = "检测指标名称")
  @ApiModelProperty(name = "name", value = "检测指标名称", required = true)
  private String name;

  @Column(name = "min_value", nullable = false, columnDefinition = "double(16, 6) comment'最小值'")
  @SaturnColumn(description = "最小值")
  @ApiModelProperty(name = "minValue", value = "最小值", required = true)
  private Double minValue;

  @Column(name = "max_value", nullable = false, columnDefinition = "double(16, 6) comment'最大值'")
  @SaturnColumn(description = "最大值")
  @ApiModelProperty(name = "maxValue", value = "最大值", required = true)
  private Double maxValue;

  @Column(name = "unit", columnDefinition = "varchar(100) comment'单位'")
  @SaturnColumn(description = "单位")
  @ApiModelProperty(name = "unit", value = "单位")
  private String unit;

  @Column(name = "accuracy", nullable = false, columnDefinition = "double(16, 6) comment'精确度'")
  @SaturnColumn(description = "精确度")
  @ApiModelProperty(name = "accuracy", value = "精确度", required = true)
  private Double accuracy;

  @Column(name = "enable_state", nullable = false, columnDefinition = "int(11) default 1 comment'生效状态，1：生效，0失效'")
  @SaturnColumn(description = "生效状态")
  @ApiModelProperty(name = "enableState", value = "生效状态", required = true)
  private Integer enableState;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Double getMinValue() {
    return minValue;
  }

  public void setMinValue(Double minValue) {
    this.minValue = minValue;
  }

  public Double getMaxValue() {
    return maxValue;
  }

  public void setMaxValue(Double maxValue) {
    this.maxValue = maxValue;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public Double getAccuracy() {
    return accuracy;
  }

  public void setAccuracy(Double accuracy) {
    this.accuracy = accuracy;
  }

  public Integer getEnableState() {
    return enableState;
  }

  public void setEnableState(Integer enableState) {
    this.enableState = enableState;
  }
}
