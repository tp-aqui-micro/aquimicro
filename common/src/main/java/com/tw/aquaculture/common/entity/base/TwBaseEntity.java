package com.tw.aquaculture.common.entity.base;

import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Set;

/**
 * 基地
 * @author 陈荣
 * @date 2019/11/616:01
 */
@Entity
@Table(name = "tw_base",uniqueConstraints = {@UniqueConstraint(columnNames = {"base_org_id"}),
	@UniqueConstraint(columnNames = {"code"})})
@SaturnDomain("base")
@ApiModel(description = "基地", value = "TwBaseEntity")
public class TwBaseEntity extends BaseFieldsEntity {

	private static final long serialVersionUID = -8487122434951388519L;

	@Column(name = "code", columnDefinition = "varchar(64) comment'编码'")
	@SaturnColumn(description = "编码")
	@ApiModelProperty(name = "code", value = "编码")
	private String code;

	@Column(name = "name", columnDefinition = "varchar(64) comment'名称'")
	@SaturnColumn(description = "名称")
	@ApiModelProperty(name = "name", value = "名称")
	private String name;

	@Column(name = "abbreviation", columnDefinition = "varchar(100) comment'简称'")
	@SaturnColumn(description = "简称")
	@ApiModelProperty(name = "abbreviation", value = "简称")
	private String abbreviation;

	@JoinColumn(name = "base_org_id", nullable = false, columnDefinition = "varchar(100) comment '所属基地组织'")
	@SaturnColumn(description = "所属基地")
	@ManyToOne(fetch = FetchType.LAZY)
	@ApiModelProperty(name = "baseOrg", value = "所属基地", required = true)
	private OrganizationEntity baseOrg;

	@SaturnColumn(description = "塘口")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "base")
	@ApiModelProperty(name = "ponds", value = "塘口")
	private Set<PondEntity> ponds;

	@JoinColumn(name = "leader_id", columnDefinition = "varchar(255) comment '基地负责人id'")
	@SaturnColumn(description = "基地负责人")
	@ManyToOne(fetch = FetchType.LAZY)
	@ApiModelProperty(name = "leader", value = "基地负责人")
	private UserEntity leader;

	@Column(name = "area_size", nullable = false, columnDefinition = "double(10, 2) comment '面积'")
	@SaturnColumn(description = "面积")
	@ApiModelProperty(name = "areaSize", value = "面积", required = true)
	private Double areaSize;

	@Column(name = "address", columnDefinition = "varchar(255) comment '地址'")
	@SaturnColumn(description = "地址")
	@ApiModelProperty(name = "address", value = "地址")
	private String address;

	@Column(name = "province", nullable = false, columnDefinition = "varchar(100) comment'省'")
	@SaturnColumn(description = "省")
	@ApiModelProperty(name = "province", value = "省", required = true)
	private String province;

	@Column(name = "city", nullable = false, columnDefinition = "varchar(100) comment'市'")
	@SaturnColumn(description = "市")
	@ApiModelProperty(name = "city", value = "市", required = true)
	private String city;

	@Column(name = "area", nullable = false, columnDefinition = "varchar(100) comment'区/县'")
	@SaturnColumn(description = "区/县")
	@ApiModelProperty(name = "area", value = "区/县", required = true)
	private String area;

	@Column(name = "enable_state", columnDefinition = "int(11) comment '生效状态，1：生效，0：失效'")
	@SaturnColumn(description = "生效状态")
	@ApiModelProperty(name = "enableState", value = "生效状态")
	private Integer enableState;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getEnableState() {
		return enableState;
	}

	public void setEnableState(Integer enableState) {
		this.enableState = enableState;
	}

	public Double getAreaSize() {
		return areaSize;
	}

	public void setAreaSize(Double areaSize) {
		this.areaSize = areaSize;
	}

	public String getAddress() {
		return address;
	}

	public OrganizationEntity getBaseOrg() {
		return baseOrg;
	}

	public void setBaseOrg(OrganizationEntity baseOrg) {
		this.baseOrg = baseOrg;
	}

	public UserEntity getLeader() {
		return leader;
	}

	public void setLeader(UserEntity leader) {
		this.leader = leader;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Set<PondEntity> getPonds() {
		return ponds;
	}

	public void setPonds(Set<PondEntity> ponds) {
		this.ponds = ponds;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
}
