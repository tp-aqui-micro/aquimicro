package com.tw.aquaculture.common.entity.base;

import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author 陈荣
 * @date 2019/12/4 10:16
 */
@Entity
@Table(name = "tw_group")
@SaturnDomain(value = "base")
@Api(value = "TwGroupEntity", description = "养殖小组")
public class TwGroupEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = 8025914108373197974L;

  @Column(name = "code", columnDefinition = "varchar(64) comment'编码'")
  @SaturnColumn(description = "编码")
  @ApiModelProperty(name = "code", value = "编码")
  private String code;

  @Column(name = "name", columnDefinition = "varchar(64) comment'名称'")
  @SaturnColumn(description = "名称")
  @ApiModelProperty(name = "name", value = "名称")
  private String name;

  @JoinColumn(name = "group_org_id", nullable = false, columnDefinition = "varchar(255) comment'所属养殖组id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "所属养殖组")
  @ApiModelProperty(value = "所属养殖组", name = "groupOrg", required = true)
  private OrganizationEntity groupOrg;

  @JoinColumn(name = "group_leader_id", columnDefinition = "varchar(255) comment'养殖组长id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "养殖组长")
  @ApiModelProperty(value = "养殖组长", name = "groupLeader")
  private UserEntity groupLeader;

  @JoinColumn(name = "tw_base_id", nullable = false, columnDefinition = "varchar(255) comment'所属基地id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "所属基地")
  @ApiModelProperty(value = "所属基地", name = "base", required = true)
  private TwBaseEntity base;

  @Column(name = "enable_state",nullable = false, columnDefinition = "int(11) comment'生效状态'")
  @SaturnColumn(description = "生效状态")
  @ApiModelProperty(name = "enableState", value = "生效状态", required = true)
  private int enableState = 1;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public OrganizationEntity getGroupOrg() {
    return groupOrg;
  }

  public void setGroupOrg(OrganizationEntity groupOrg) {
    this.groupOrg = groupOrg;
  }

  public UserEntity getGroupLeader() {
    return groupLeader;
  }

  public void setGroupLeader(UserEntity groupLeader) {
    this.groupLeader = groupLeader;
  }

  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public int getEnableState() {
    return enableState;
  }

  public void setEnableState(int enableState) {
    this.enableState = enableState;
  }
}
