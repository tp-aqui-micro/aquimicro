
package com.tw.aquaculture.common.entity.check;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author 陈荣
 * @date 2019/11/14 15:54
 */

@Entity
@Table(name = "tw_check_fish")
@ApiModel(description = "鱼体检测", value = "CheckFishEntity")
@SaturnDomain("check")
public class CheckFishEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = -3525321011903000998L;

  @ApiModelProperty(value = "编码", name = "code")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment'编码'")
  @SaturnColumn(description = "编码")
  private String code;

  @ApiModelProperty(value = "塘口", required = true)
  @JoinColumn(name = "pond_id",nullable = false, columnDefinition = "varchar(255) comment'塘口id'")
  @SaturnColumn(description = "塘口")
  @ManyToOne(fetch = FetchType.LAZY)
  private PondEntity pond;

  @JoinColumn(name = "fish_type", nullable = false, columnDefinition = "varchar(100) comment'鱼种'")
  @SaturnColumn(description = "鱼种")
  @ApiModelProperty(name = "fishType", value = "鱼种", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private FishTypeEntity fishType;

  @Column(name = "surface_state",  columnDefinition = "int(11) comment'体表状态,0：异常，1：正常'")
  @SaturnColumn(description = "体表状态")
  @ApiModelProperty(value = "体表状态,0：异常，1：正常", name = "surfaceState")
  private Integer surfaceState;

  @Column(name = "surface",  columnDefinition = "varchar(255) comment'体表'")
  @SaturnColumn(description = "体表")
  @ApiModelProperty(value = "体表", name = "surface")
  private String surface;

  @Column(name = "gill_state", columnDefinition = "int(11) comment'鱼鳃状态,0：异常，1：正常'")
  @SaturnColumn(description = "鱼鳃状态")
  @ApiModelProperty(value = "鱼鳃状态,0：异常，1：正常", name = "gillState")
  private Integer gillState;

  @Column(name = "gill", columnDefinition = "varchar(255) comment'鱼鳃'")
  @SaturnColumn(description = "鱼鳃")
  @ApiModelProperty(value = "鱼鳃", name = "gill")
  private String gill;

  @Column(name = "hepar_state", columnDefinition = "int(11) comment'肝胰脏状态,0：异常，1：正常'")
  @SaturnColumn(description = "肝胰脏状态")
  @ApiModelProperty(value = "肝胰脏状态,0：异常，1：正常", name = "heparState")
  private Integer heparState;

  @Column(name = "hepar", columnDefinition = "varchar(255) comment'肝胰脏'")
  @SaturnColumn(description = "肝胰脏")
  @ApiModelProperty(value = "肝胰脏", name = "hepar")
  private String hepar;

  @Column(name = "gut_state", columnDefinition = "int(11) comment'肠道状态,0：异常，1：正常'")
  @SaturnColumn(description = "肠道状态")
  @ApiModelProperty(value = "肠道状态,0：异常，1：正常",name = "gutState")
  private Integer gutState;

  @Column(name = "gut", columnDefinition = "varchar(255) comment'肠道'")
  @SaturnColumn(description = "肠道")
  @ApiModelProperty(value = "肠道",name = "gut")
  private String gut;

  @Column(name = "guts_state", columnDefinition = "int(11) comment'胆囊状态,0：异常，1：正常'")
  @SaturnColumn(description = "胆囊状态")
  @ApiModelProperty(value = "胆囊状态,0：异常，1：正常", name = "gutsState")
  private Integer gutsState;

  @Column(name = "guts", columnDefinition = "varchar(255) comment'胆囊'")
  @SaturnColumn(description = "胆囊")
  @ApiModelProperty(value = "胆囊", name = "guts")
  private String guts;

  @Column(name = "spleen_state", columnDefinition = "int(11) comment'脾脏状态,0：异常，1：正常'")
  @SaturnColumn(description = "脾脏状态")
  @ApiModelProperty(value = "脾脏状态,0：异常，1：正常", name = "spleenState")
  private Integer spleenState;

  @Column(name = "spleen", columnDefinition = "varchar(255) comment'脾脏状态'")
  @SaturnColumn(description = "脾脏")
  @ApiModelProperty(value = "脾脏", name = "spleen")
  private String spleen;

  @Column(name = "bladder_state", columnDefinition = "int(11) comment'鱼鳔状态,0：异常，1：正常'")
  @SaturnColumn(description = "鱼鳔状态")
  @ApiModelProperty(value = "鱼鳔状态,0：异常，1：正常", name = "bladderState")
  private Integer bladderState;

  @Column(name = "bladder", columnDefinition = "varchar(255) comment'鱼鳔'")
  @SaturnColumn(description = "鱼鳔")
  @ApiModelProperty(value = "鱼鳔", name = "bladder")
  private String bladder;

  @Column(name = "viscus_weight", columnDefinition = "double(10, 2) comment'内脏重量/kg'")
  @SaturnColumn(description = "内脏重量")
  @ApiModelProperty(value = "内脏重量/kg", name = "viscusWeight")
  private Double viscusWeight;

  @Column(name = "hepar_weight", columnDefinition = "double(10, 2) comment'肝脏重量/kg'")
  @SaturnColumn(description = "肝脏重量")
  @ApiModelProperty(value = "肝脏重量/kg", name = "heparWeight")
  private Double heparWeight;

  @Column(name = "gut_weight", columnDefinition = "double(10, 2) comment'肠重/kg'")
  @SaturnColumn(description = "肠重")
  @ApiModelProperty(value = "肠重/kg", name = "gutWeight")
  private Double gutWeight;

  @Column(name = "gut_length", columnDefinition = "double(10, 2) comment'肠长/cm'")
  @SaturnColumn(description = "肠长")
  @ApiModelProperty(value = "肠长/cm", name = "gutLength")
  private Double gutLength;

  @Column(name = "fish_length", columnDefinition = "double(10, 2) comment'体长/cm'")
  @SaturnColumn(description = "体长")
  @ApiModelProperty(value = "体长/cm", name = "fishLength")
  private Double fishLength;

  @Column(name = "check_time", columnDefinition = "datetime default current_timestamp comment'检查时间'")
  @SaturnColumn(description = "检查时间")
  @ApiModelProperty(value = "检查时间", name = "checkTime")
  private Date checkTime;

  @Column(name = "note", columnDefinition = "varchar(1000) comment'备注'")
  @SaturnColumn(description = "备注")
  @ApiModelProperty(value = "备注", name = "note")
  private String note;

  @Column(name = "picture", columnDefinition = "varchar(2000) comment'图片'")
  @SaturnColumn(description = "图片")
  @ApiModelProperty(value = "图片地址", name = "picture")
  private String picture;

  @ApiModelProperty(value = "基地")
  @JoinColumn(name = "base_id", columnDefinition = "varchar(255) comment '基地id'")
  @SaturnColumn(description = "基地")
  @ManyToOne(fetch = FetchType.LAZY)
  private TwBaseEntity base;

  @ApiModelProperty(name = "fatness", value = "肥满度")
  @Column(name = "fatness", columnDefinition = "varchar(255) comment'肥满度'")
  @SaturnColumn(description = "肥满度")
  private String fatness;

  @ApiModelProperty(name = "cavity", value = "腹腔")
  @Column(name = "cavity", columnDefinition = "varchar(255) comment'腹腔'")
  @SaturnColumn(description = "腹腔")
  private String cavity;

  @ApiModelProperty(name = "kidney", value = "肾脏")
  @Column(name = "kidney", columnDefinition = "varchar(255) comment'肾脏'")
  @SaturnColumn(description = "肾脏")
  private String kidney;

  @ApiModelProperty(name = "anus", value = "肛门")
  @Column(name = "anus", columnDefinition = "varchar(255) comment'肛门'")
  @SaturnColumn(description = "肛门")
  private String anus;

  @ApiModelProperty(name = "fin", value = "鱼鳍")
  @Column(name = "fin", columnDefinition = "varchar(255) comment'鱼鳍'")
  @SaturnColumn(description = "鱼鳍")
  private String fin;

  @ApiModelProperty(name = "muscle", value = "肌肉")
  @Column(name = "muscle", columnDefinition = "varchar(255) comment'肌肉'")
  @SaturnColumn(description = "肌肉")
  private String muscle;

  @ApiModelProperty(name = "speed", value = "血液")
  @Column(name = "speed", columnDefinition = "varchar(255) comment'血液'")
  @SaturnColumn(description = "血液")
  private String speed;

  @ApiModelProperty(name = "eyes", value = "眼睛")
  @Column(name = "eyes", columnDefinition = "varchar(255) comment'眼睛'")
  @SaturnColumn(description = "眼睛")
  private String eyes;

  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public FishTypeEntity getFishType() {
    return fishType;
  }

  public void setFishType(FishTypeEntity fishType) {
    this.fishType = fishType;
  }

  public Integer getSurfaceState() {
    return surfaceState;
  }

  public void setSurfaceState(Integer surfaceState) {
    this.surfaceState = surfaceState;
  }

  public Integer getGillState() {
    return gillState;
  }

  public void setGillState(Integer gillState) {
    this.gillState = gillState;
  }

  public Integer getHeparState() {
    return heparState;
  }

  public void setHeparState(Integer heparState) {
    this.heparState = heparState;
  }

  public Integer getGutState() {
    return gutState;
  }

  public void setGutState(Integer gutState) {
    this.gutState = gutState;
  }

  public Integer getGutsState() {
    return gutsState;
  }

  public void setGutsState(Integer gutsState) {
    this.gutsState = gutsState;
  }

  public Integer getSpleenState() {
    return spleenState;
  }

  public void setSpleenState(Integer spleenState) {
    this.spleenState = spleenState;
  }

  public Integer getBladderState() {
    return bladderState;
  }

  public void setBladderState(Integer bladderState) {
    this.bladderState = bladderState;
  }

  public Double getViscusWeight() {
    return viscusWeight;
  }

  public void setViscusWeight(Double viscusWeight) {
    this.viscusWeight = viscusWeight;
  }

  public Double getHeparWeight() {
    return heparWeight;
  }

  public void setHeparWeight(Double heparWeight) {
    this.heparWeight = heparWeight;
  }

  public Double getGutWeight() {
    return gutWeight;
  }

  public void setGutWeight(Double gutWeight) {
    this.gutWeight = gutWeight;
  }

  public Double getGutLength() {
    return gutLength;
  }

  public void setGutLength(Double gutLength) {
    this.gutLength = gutLength;
  }

  public Double getFishLength() {
    return fishLength;
  }

  public void setFishLength(Double fishLength) {
    this.fishLength = fishLength;
  }

  public Date getCheckTime() {
    return checkTime;
  }

  public void setCheckTime(Date checkTime) {
    this.checkTime = checkTime;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public String getFatness() {
    return fatness;
  }

  public void setFatness(String fatness) {
    this.fatness = fatness;
  }

  public String getCavity() {
    return cavity;
  }

  public void setCavity(String cavity) {
    this.cavity = cavity;
  }

  public String getKidney() {
    return kidney;
  }

  public void setKidney(String kidney) {
    this.kidney = kidney;
  }

  public String getAnus() {
    return anus;
  }

  public void setAnus(String anus) {
    this.anus = anus;
  }

  public String getFin() {
    return fin;
  }

  public void setFin(String fin) {
    this.fin = fin;
  }

  public String getMuscle() {
    return muscle;
  }

  public void setMuscle(String muscle) {
    this.muscle = muscle;
  }

  public String getSpeed() {
    return speed;
  }

  public void setSpeed(String speed) {
    this.speed = speed;
  }

  public String getEyes() {
    return eyes;
  }

  public void setEyes(String eyes) {
    this.eyes = eyes;
  }

  public String getSurface() {
    return surface;
  }

  public void setSurface(String surface) {
    this.surface = surface;
  }

  public String getGill() {
    return gill;
  }

  public void setGill(String gill) {
    this.gill = gill;
  }

  public String getHepar() {
    return hepar;
  }

  public void setHepar(String hepar) {
    this.hepar = hepar;
  }

  public String getGut() {
    return gut;
  }

  public void setGut(String gut) {
    this.gut = gut;
  }

  public String getGuts() {
    return guts;
  }

  public void setGuts(String guts) {
    this.guts = guts;
  }

  public String getSpleen() {
    return spleen;
  }

  public void setSpleen(String spleen) {
    this.spleen = spleen;
  }

  public String getBladder() {
    return bladder;
  }

  public void setBladder(String bladder) {
    this.bladder = bladder;
  }
}
