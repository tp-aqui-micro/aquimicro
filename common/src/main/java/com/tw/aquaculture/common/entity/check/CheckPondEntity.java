package com.tw.aquaculture.common.entity.check;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author 陈荣
 * @date 2019/11/14 15:15
 */

@Entity
@Table(name = "tw_check_pond")
@ApiModel(description = "巡塘检测", value = "CheckPondEntity")
@SaturnDomain("check")
public class CheckPondEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = -2524310851595136332L;

  @ApiModelProperty(value = "编码", name = "code")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment'编码'")
  @SaturnColumn(description = "编码")
  private String code;

  @ApiModelProperty(value = "塘口", name = "pond", required = true)
  @JoinColumn(name = "pond_id", nullable = false, columnDefinition = "varchar(255) comment'塘口id'")
  @SaturnColumn(description = "塘口")
  @ManyToOne(fetch = FetchType.LAZY)
  private PondEntity pond;

  @Column(name = "d_o", columnDefinition = "double(10, 2) comment'溶氧量,mg/L'")
  @SaturnColumn(description = "溶氧量")
  @ApiModelProperty(value = "溶氧量,mg/L", name = "dO")
  private Double dO;

  @Column(name = "ph", columnDefinition = "double(10, 2) comment'ph值'")
  @SaturnColumn(description = "ph值")
  @ApiModelProperty(value = "ph值", name = "ph")
  private Double ph;

  @Column(name = "water_temperature", columnDefinition = "double(10, 2) comment'水温，摄氏度'")
  @SaturnColumn(description = "水温")
  @ApiModelProperty(value = "水温，摄氏度", name = "waterTemperature")
  private Double waterTemperature;

  @Column(name = "water_deep", columnDefinition = "double(10, 2) comment'水深'")
  @SaturnColumn(description = "水深")
  @ApiModelProperty(value = "水深", name = "waterDeep")
  private Double waterDeep;

  @Column(name = "an", columnDefinition = "double(10, 2) comment'氨氮,mg/L'")
  @SaturnColumn(description = "氨氮量")
  @ApiModelProperty(value = "氨氮,mg/L")
  private Double an;

  @Column(name = "nitrite", columnDefinition = "double(10, 2) comment'亚硝酸盐,mg/L'")
  @SaturnColumn(description = "亚硝酸盐")
  @ApiModelProperty(value = "亚硝酸盐,mg/L", name = "nitrite")
  private Double nitrite;

  @Column(name = "algae_color", columnDefinition = "varchar(100) comment'藻相'")
  @SaturnColumn(description = "藻相")
  @ApiModelProperty(value = "藻相", name = "algaeColor")
  private String algaeColor;

  @Column(name = "seepage_state", columnDefinition = "int(11) default 1 comment'塘口渗水，0:否，1：是'")
  @SaturnColumn(description = "塘口渗水")
  @ApiModelProperty(value = "塘口渗水,0:否，1：是", name = "seepageState")
  private Integer seepageState;

  @Column(name = "equipment_state", columnDefinition = "int(11) default 1 comment'设备状态，0:异常，1：正常'")
  @SaturnColumn(description = "设备状态")
  @ApiModelProperty(value = "设备状态,0:异常，1：正常", name = "equipmentState")
  private Integer equipmentState;

  @Column(name = "unusual_equipment", columnDefinition = "varchar(1000) default 1 comment'异常设备'")
  @SaturnColumn(description = "异常设备")
  @ApiModelProperty(value = "异常设备", name = "equipmentState")
  private String unusualEquipments;

  @Column(name = "food_state", columnDefinition = "int(11) default 1 comment'吃食情况，0:异常，1：正常'")
  @SaturnColumn(description = "吃食情况")
  @ApiModelProperty(value = "吃食情况,0:异常，1：正常", name = "foodState")
  private Integer foodState;

  @Column(name = "check_time", nullable = false, columnDefinition = "datetime default current_timestamp comment'检查时间'")
  @SaturnColumn(description = "检查时间")
  @ApiModelProperty(value = "检查时间", name = "checkTime", required = true)
  private Date checkTime;

  @Column(name = "note", columnDefinition = "varchar(1000) comment'备注'")
  @SaturnColumn(description = "备注")
  @ApiModelProperty(value = "备注", name = "note")
  private String note;

  @Column(name = "picture", columnDefinition = "varchar(5000) comment'图片'")
  @SaturnColumn(description = "图片")
  @ApiModelProperty(value = "图片地址", name = "picture")
  private String picture;

  @ApiModelProperty(value = "基地")
  @JoinColumn(name = "base_id", columnDefinition = "varchar(255) comment '基地id'")
  @SaturnColumn(description = "基地")
  @ManyToOne(fetch = FetchType.LAZY)
  private TwBaseEntity base;

  @Column(name = "fish_damage_state", columnDefinition = "int(11) default 0 comment'是否损鱼：是、否'")
  @SaturnColumn(description = "是否损鱼：是、否")
  @ApiModelProperty(value = "是否损鱼：是、否")
  private Integer fishDamageState;

  @Column(name = "up_feed_state", columnDefinition = "int(11) default 0 comment'是否有漂浮饲料：是、否'")
  @SaturnColumn(description = "是否有漂浮饲料：是、否")
  @ApiModelProperty(value = "是否有漂浮饲料：是、否")
  private Integer upFeedState;

  @Column(name = "water_state", columnDefinition = "int(11) default 0 comment'水质：正常、异常'")
  @SaturnColumn(description = "水质：正常、异常")
  @ApiModelProperty(value = "水质：正常、异常")
  private Integer waterState;

  @Column(name = "fish_state", columnDefinition = "int(11) default 0 comment'鱼是否正常：正常、异常'")
  @SaturnColumn(description = "鱼是否正常：正常、异常")
  @ApiModelProperty(value = "鱼是否正常：正常、异常")
  private Integer fishState;

  @Column(name = "mire_state", columnDefinition = "int(11) default 0 comment'淤泥情况：正常、异常'")
  @SaturnColumn(description = "淤泥情况：正常、异常")
  @ApiModelProperty(value = "淤泥情况：正常、异常")
  private Integer mireState;

  @Column(name = "steal_state", columnDefinition = "int(11) default 0 comment'是否偷盗：是、否'")
  @SaturnColumn(description = "是否偷盗：是、否")
  @ApiModelProperty(value = "是否偷盗：是、否")
  private Integer stealState;

  @ApiModelProperty(name = "transparency", value = "透明度")
  @Column(name = "transparency", columnDefinition = "double(10, 2) comment'透明度'")
  @SaturnColumn(description = "透明度")
  private Double transparency;

  public Integer getFishDamageState() {
    return fishDamageState;
  }

  public void setFishDamageState(Integer fishDamageState) {
    this.fishDamageState = fishDamageState;
  }

  public Integer getUpFeedState() {
    return upFeedState;
  }

  public void setUpFeedState(Integer upFeedState) {
    this.upFeedState = upFeedState;
  }

  public Integer getWaterState() {
    return waterState;
  }

  public void setWaterState(Integer waterState) {
    this.waterState = waterState;
  }

  public Integer getFishState() {
    return fishState;
  }

  public void setFishState(Integer fishState) {
    this.fishState = fishState;
  }

  public Integer getMireState() {
    return mireState;
  }

  public void setMireState(Integer mireState) {
    this.mireState = mireState;
  }

  public Integer getStealState() {
    return stealState;
  }

  public void setStealState(Integer stealState) {
    this.stealState = stealState;
  }

  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public Double getdO() {
    return dO;
  }

  public void setdO(Double dO) {
    this.dO = dO;
  }

  public Double getPh() {
    return ph;
  }

  public void setPh(Double ph) {
    this.ph = ph;
  }

  public Double getWaterTemperature() {
    return waterTemperature;
  }

  public void setWaterTemperature(Double waterTemperature) {
    this.waterTemperature = waterTemperature;
  }

  public Double getWaterDeep() {
    return waterDeep;
  }

  public void setWaterDeep(Double waterDeep) {
    this.waterDeep = waterDeep;
  }

  public Double getAn() {
    return an;
  }

  public void setAn(Double an) {
    this.an = an;
  }

  public Double getNitrite() {
    return nitrite;
  }

  public void setNitrite(Double nitrite) {
    this.nitrite = nitrite;
  }

  public String getAlgaeColor() {
    return algaeColor;
  }

  public void setAlgaeColor(String algaeColor) {
    this.algaeColor = algaeColor;
  }

  public Integer getSeepageState() {
    return seepageState;
  }

  public void setSeepageState(Integer seepageState) {
    this.seepageState = seepageState;
  }

  public Integer getEquipmentState() {
    return equipmentState;
  }

  public void setEquipmentState(Integer equipmentState) {
    this.equipmentState = equipmentState;
  }

  public Integer getFoodState() {
    return foodState;
  }

  public void setFoodState(Integer foodState) {
    this.foodState = foodState;
  }

  public Date getCheckTime() {
    return checkTime;
  }

  public void setCheckTime(Date checkTime) {
    this.checkTime = checkTime;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public String getUnusualEquipments() {
    return unusualEquipments;
  }

  public void setUnusualEquipments(String unusualEquipments) {
    this.unusualEquipments = unusualEquipments;
  }

  public Double getTransparency() {
    return transparency;
  }

  public void setTransparency(Double transparency) {
    this.transparency = transparency;
  }
}
