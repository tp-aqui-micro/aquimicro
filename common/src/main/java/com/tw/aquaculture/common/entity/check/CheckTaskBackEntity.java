package com.tw.aquaculture.common.entity.check;

import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.core.entity.UuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author 陈荣
 * @date 2019/12/6 15:53
 */
@Entity
@Table(name = "tw_check_task_back")
@ApiModel(description = "临时任务回复", value = "CheckTaskBackEntity")
@SaturnDomain("check")
public class CheckTaskBackEntity extends UuidEntity {

  private static final long serialVersionUID = -8300975532736817179L;

  @JoinColumn(name = "check_task_id", nullable = false, columnDefinition = "varchar(255) comment'临时任务id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "临时任务")
  @ApiModelProperty(name = "checkTask", required = true, value = "临时任务")
  private CheckTaskEntity checkTask;

  @JoinColumn(name = "user_id", nullable = false, columnDefinition = "varchar(255) comment'用户id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "用户")
  @ApiModelProperty(name = "user", value = "用户", required = true)
  private UserEntity user;

  @Column(name = "content", columnDefinition = "varchar(1000) comment'回复内容'")
  @SaturnColumn(description = "回复内容")
  @ApiModelProperty(name = "content", value = "回复内容")
  private String content;

  @Column(name = "picture", columnDefinition = "varchar(1000) comment'图片'")
  @SaturnColumn(description = "图片")
  @ApiModelProperty(name = "picture", value = "图片")
  private String picture;

  @Column(name = "back_time", nullable = false, columnDefinition = "datetime comment'反馈时间'")
  @SaturnColumn(description = "反馈时间")
  @ApiModelProperty(name = "backTime", value = "反馈时间", required = true)
  private Date backTime;

  public CheckTaskEntity getCheckTask() {
    return checkTask;
  }

  public void setCheckTask(CheckTaskEntity checkTask) {
    this.checkTask = checkTask;
  }

  public UserEntity getUser() {
    return user;
  }

  public void setUser(UserEntity user) {
    this.user = user;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public Date getBackTime() {
    return backTime;
  }

  public void setBackTime(Date backTime) {
    this.backTime = backTime;
  }
}
