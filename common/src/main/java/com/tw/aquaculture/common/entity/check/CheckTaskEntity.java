package com.tw.aquaculture.common.entity.check;

import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Set;

/**
 * @author 陈荣
 * @date 2019/11/14 16:33
 */

@Entity
@Table(name = "tw_check_task", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"code"})
})
@ApiModel(description = "临时任务")
@SaturnDomain("check")
public class CheckTaskEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = 8579677869274295733L;

  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment'任务编码'")
  @SaturnColumn(description = "任务编码")
  @ApiModelProperty(value = "任务编码", name = "code", required = true)
  private String code;

  @Column(name = "send_user_id", nullable = false, columnDefinition = "varchar(255) comment'发起人id'")
  @SaturnColumn(description = "发起人id")
  @ApiModelProperty(value = "发起人id", name = "sendUserId", required = true)
  private String sendUserId;

  @Column(name = "name", nullable = false, columnDefinition = "varchar(100) comment'任务名称'")
  @SaturnColumn(description = "任务名称")
  @ApiModelProperty(value = "任务名称", name = "name", required = true)
  private String name;

  @Column(name = "state", nullable = false, columnDefinition = "int(11) comment'任务状态（1已完成，2未完成）'")
  @SaturnColumn(description = "任务状态")
  @ApiModelProperty(value = "任务状态", name = "name", required = true)
  private Integer state;

  @ApiModelProperty(value = "塘口", name = "pond", required = true)
  @JoinColumn(name = "pond_id", nullable = false, columnDefinition = "varchar(255) comment'塘口id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "塘口")
  private PondEntity pond;

  @JoinColumn(name = "receive_user_id", nullable = false, columnDefinition = "varchar(255) comment'用户id'")
  @SaturnColumn(description = "接收人")
  @ApiModelProperty(value = "接收人", name = "user", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private UserEntity receiveUser;

  @Column(name = "content", columnDefinition = "varchar(1000) comment'任务详情'")
  @SaturnColumn(description = "任务详情")
  @ApiModelProperty(value = "任务详情", name = "content")
  private String content;

  @ApiModelProperty(value = "基地")
  @JoinColumn(name = "base_id", columnDefinition = "varchar(255) comment '基地id'")
  @SaturnColumn(description = "基地")
  @ManyToOne(fetch = FetchType.LAZY)
  private TwBaseEntity base;

  @ApiModelProperty(name = "checkTaskBacks", value = "任务反馈")
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "checkTask")
  @SaturnColumn(description = "任务反馈")
  private Set<CheckTaskBackEntity> checkTaskBacks;

  public String getSendUserId() {
    return sendUserId;
  }

  public void setSendUserId(String sendUserId) {
    this.sendUserId = sendUserId;
  }

  public void setState(Integer state) {
    this.state = state;
  }

  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public UserEntity getReceiveUser() {
    return receiveUser;
  }

  public void setReceiveUser(UserEntity receiveUser) {
    this.receiveUser = receiveUser;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public int getState() {
    return state;
  }

  public void setState(int state) {
    this.state = state;
  }

  public Set<CheckTaskBackEntity> getCheckTaskBacks() {
    return checkTaskBacks;
  }

  public void setCheckTaskBacks(Set<CheckTaskBackEntity> checkTaskBacks) {
    this.checkTaskBacks = checkTaskBacks;
  }
}
