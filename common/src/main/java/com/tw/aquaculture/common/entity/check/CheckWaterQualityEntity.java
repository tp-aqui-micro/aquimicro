package com.tw.aquaculture.common.entity.check;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author 陈荣
 * @date 2019/11/14 15:48
 */

@Entity
@Table(name = "tw_check_water_quality")
@ApiModel(description = "水质检测", value = "CheckWaterQualityEntity")
@SaturnDomain("check")
public class CheckWaterQualityEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = 1807276725845243714L;

  @ApiModelProperty(value = "编码", name = "code")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment'编码'")
  @SaturnColumn(description = "编码")
  private String code;

  @ApiModelProperty(value = "塘口", name = "pond", required = true)
  @JoinColumn(name = "pond_id", nullable = false, columnDefinition = "varchar(255) comment'塘口id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "塘口")
  private PondEntity pond;

  @Column(name = "d_o", columnDefinition = "double(10, 2) comment'溶氧量,mg/L'")
  @SaturnColumn(description = "溶解氧")
  @ApiModelProperty(value = "溶解氧", name = "dO")
  private Double dO;

  @Column(name = "ph", columnDefinition = "double(10, 2) comment'ph值'")
  @SaturnColumn(description = "ph值")
  @ApiModelProperty(value = "ph值", name = "ph")
  private Double ph;

  @Column(name = "water_temperature", columnDefinition = "double(10, 2) comment'水温，摄氏度'")
  @SaturnColumn(description = "水温")
  @ApiModelProperty(value = "水温，摄氏度", name = "waterTemperature")
  private Double waterTemperature;

  @Column(name = "an", columnDefinition = "double(10, 2) comment'氨氮,mg/L'")
  @SaturnColumn(description = "氨氮")
  @ApiModelProperty(value = "氨氮", name = "an")
  private Double an;

  @Column(name = "nitrite", columnDefinition = "double(10, 2) comment'亚硝酸盐,mg/L'")
  @SaturnColumn(description = "亚硝酸盐")
  @ApiModelProperty(value = "亚硝酸盐,mg/L", name = "nitrite")
  private Double nitrite;

  @Column(name = "algae_color", columnDefinition = "varchar(100) comment'藻相'")
  @SaturnColumn(description = "藻相")
  @ApiModelProperty(value = "藻相", name = "algaeColor")
  private String algaeColor;

  @Column(name = "check_time", columnDefinition = "datetime default current_timestamp comment'检查时间'")
  @SaturnColumn(description = "检查时间")
  @ApiModelProperty(value = "检查时间", name = "checkTime")
  private Date checkTime;

  @Column(name = "note", columnDefinition = "varchar(1000) comment'备注'")
  @SaturnColumn(description = "备注")
  @ApiModelProperty(value = "备注", name = "note")
  private String note;

  @Column(name = "picture", columnDefinition = "varchar(2000) comment'图片'")
  @SaturnColumn(description = "图片")
  @ApiModelProperty(value = "图片地址", name = "picture")
  private String picture;

  @ApiModelProperty(value = "基地")
  @JoinColumn(name = "base_id", columnDefinition = "varchar(255) comment '基地id'")
  @SaturnColumn(description = "基地")
  @ManyToOne(fetch = FetchType.LAZY)
  private TwBaseEntity base;

  @ApiModelProperty(name = "transparency", value = "透明度")
  @Column(name = "transparency", columnDefinition = "double(10, 2) comment'透明度'")
  @SaturnColumn(description = "透明度")
  private Double transparency;

  @ApiModelProperty(name = "remainChlorine", value = "余氯")
  @Column(name = "remain_chlorine", columnDefinition = "varchar(255) comment'余氯'")
  @SaturnColumn(description = "余氯")
  private String remainChlorine;

  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public Double getdO() {
    return dO;
  }

  public void setdO(Double dO) {
    this.dO = dO;
  }

  public Double getPh() {
    return ph;
  }

  public void setPh(Double ph) {
    this.ph = ph;
  }

  public Double getWaterTemperature() {
    return waterTemperature;
  }

  public void setWaterTemperature(Double waterTemperature) {
    this.waterTemperature = waterTemperature;
  }

  public Double getAn() {
    return an;
  }

  public void setAn(Double an) {
    this.an = an;
  }

  public Double getNitrite() {
    return nitrite;
  }

  public void setNitrite(Double nitrite) {
    this.nitrite = nitrite;
  }

  public String getAlgaeColor() {
    return algaeColor;
  }

  public void setAlgaeColor(String algaeColor) {
    this.algaeColor = algaeColor;
  }

  public Date getCheckTime() {
    return checkTime;
  }

  public void setCheckTime(Date checkTime) {
    this.checkTime = checkTime;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public Double getTransparency() {
    return transparency;
  }

  public void setTransparency(Double transparency) {
    this.transparency = transparency;
  }

  public String getRemainChlorine() {
    return remainChlorine;
  }

  public void setRemainChlorine(String remainChlorine) {
    this.remainChlorine = remainChlorine;
  }
}

