package com.tw.aquaculture.common.entity.plan;

import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 出鱼计划
 *
 * @author bo shi
 */
@ApiModel(value = "OutFishPlanEntity", description = "出鱼计划")
@Entity
@Table(name = "tw_out_fish_plan")
@SaturnDomain(value = "plan")
public class OutFishPlanEntity extends BaseFieldsEntity {

  /**
   * 
   */
  private static final long serialVersionUID = 890220231507494226L;

  @ApiModelProperty(name = "code",required = true, value = "计划编码")
  @SaturnColumn(description = "计划编码")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(255) comment '计划编码'")
  private String code;

  @ApiModelProperty(name = "year", value = "年度")
  @SaturnColumn(description = "年度")
  @Column(name = "year", columnDefinition = "varchar(10) comment '年度'")
  private String year;

  @ApiModelProperty(name = "company", value = "公司")
  @JoinColumn(name = "org_id", columnDefinition = "varchar(255) comment'公司id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "公司id")
  private OrganizationEntity company;

  @ApiModelProperty(name = "twBase", value = "基地")
  @JoinColumn(name = "tw_base_id", columnDefinition = "varchar(255) comment'基地id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "基地id")
  private TwBaseEntity twBase;

  @ApiModelProperty(name = "pond", value = "塘口")
  @JoinColumn(name = "pond_id", columnDefinition = "varchar(255) comment'塘口id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "塘口id")
  private PondEntity pond;

  @JoinColumn(name = "fish_type", nullable = false, columnDefinition = "varchar(255) comment'鱼种'")
  @SaturnColumn(description = "鱼种")
  @ApiModelProperty(name = "fishType", value = "鱼种", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private FishTypeEntity fishType;

  @ApiModelProperty(name = "outTime", value = "出鱼时间")
  @SaturnColumn(description = "出鱼时间")
  @Column(name = "out_time", columnDefinition = "datetime comment '出鱼时间'")
  private Date outTime;

  @ApiModelProperty(name = "spec", value = "出鱼规格(数据字典编码)")
  @SaturnColumn(description = "出鱼规格(斤/尾)")
  @Column(name = "spec", columnDefinition = "varchar(255) comment '出鱼规格(数据字典编码)'")
  private String spec;

  @ApiModelProperty(name = "fishPrice", value = "出鱼单价")
  @SaturnColumn(description = "出鱼单价")
  @Column(name = "fish_price", columnDefinition = "DECIMAL(10,5) comment '出鱼单价'")
  private BigDecimal fishPrice;

  @ApiModelProperty(name = "outFishQuantity", value = "出鱼尾数")
  @SaturnColumn(description = "出鱼尾数")
  @Column(name = "out_fish_quantity", columnDefinition = "int comment '出鱼尾数'")
  private int outFishQuantity;

  @ApiModelProperty(name = "", value = "出鱼重量")
  @SaturnColumn(description = "出鱼重量")
  @Column(name = "out_fish_weight", columnDefinition = "DECIMAL(30,2) comment '出鱼重量'")
  private BigDecimal outFishWeight;

  @ApiModelProperty(name = "outFishTotalPrice", value = "出鱼金额(元)")
  @SaturnColumn(description = "出鱼金额(元)")
  @Column(name = "out_fish_total_price", columnDefinition = "DECIMAL(30,5) comment '出鱼金额(元)'")
  private BigDecimal outFishTotalPrice;

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public OrganizationEntity getCompany() {
    return company;
  }

  public void setCompany(OrganizationEntity company) {
    this.company = company;
  }

  public TwBaseEntity getTwBase() {
    return twBase;
  }

  public void setTwBase(TwBaseEntity twBase) {
    this.twBase = twBase;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public Date getOutTime() {
    return outTime;
  }

  public void setOutTime(Date outTime) {
    this.outTime = outTime;
  }

  public String getSpec() {
    return spec;
  }

  public void setSpec(String spec) {
    this.spec = spec;
  }

  public BigDecimal getFishPrice() {
    return fishPrice;
  }

  public void setFishPrice(BigDecimal fishPrice) {
    this.fishPrice = fishPrice;
  }

  public int getOutFishQuantity() {
    return outFishQuantity;
  }

  public void setOutFishQuantity(int outFishQuantity) {
    this.outFishQuantity = outFishQuantity;
  }

  public BigDecimal getOutFishWeight() {
    return outFishWeight;
  }

  public void setOutFishWeight(BigDecimal outFishWeight) {
    this.outFishWeight = outFishWeight;
  }

  public BigDecimal getOutFishTotalPrice() {
    return outFishTotalPrice;
  }

  public void setOutFishTotalPrice(BigDecimal outFishTotalPrice) {
    this.outFishTotalPrice = outFishTotalPrice;
  }


  public FishTypeEntity getFishType() {
    return fishType;
  }

  public void setFishType(FishTypeEntity fishType) {
    this.fishType = fishType;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
