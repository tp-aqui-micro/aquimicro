package com.tw.aquaculture.common.entity.plan;

import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 投苗计划
 *
 * @author bo shi
 */
@ApiModel(value = "PutFryPlanEntity", description = "投苗计划")
@Entity
@Table(name = "tw_put_fry_plan")
@SaturnDomain(value = "planmanagement")
public class PutFryPlanEntity extends BaseFieldsEntity {

  /**
   * 
   */
  private static final long serialVersionUID = 814135073469758215L;

  @ApiModelProperty(name = "code", value = "计划编码")
  @SaturnColumn(description = "计划编码")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(255) comment '计划编码'")
  private String code;

  @ApiModelProperty(name = "year", value = "年度")
  @SaturnColumn(description = "年度")
  @Column(name = "year", columnDefinition = "varchar(10) comment '年度'")
  private String year;

  @ApiModelProperty(name = "company", value = "公司")
  @JoinColumn(name = "org_id", columnDefinition = "varchar(255) comment'公司id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "公司id")
  private OrganizationEntity company;

  @ApiModelProperty(name = "twBase", value = "基地")
  @JoinColumn(name = "tw_base_id", columnDefinition = "varchar(255) comment'基地id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "基地id")
  private TwBaseEntity twBase;

  @ApiModelProperty(name = "pond", value = "塘口")
  @JoinColumn(name = "pond_id", columnDefinition = "varchar(255) comment'塘口id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "塘口id")
  private PondEntity pond;

  @JoinColumn(name = "fish_type", nullable = false, columnDefinition = "varchar(255) comment'鱼种'")
  @SaturnColumn(description = "鱼种")
  @ApiModelProperty(name = "fishType", value = "鱼种", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private FishTypeEntity fishType;

  @ApiModelProperty(name = "putFryTime", value = "投苗时间")
  @SaturnColumn(description = "投苗时间")
  @Column(name = "put_fry_time", columnDefinition = "datetime comment '投苗时间'")
  private Date putFryTime;

  @ApiModelProperty(name = "putFryQuantity", value = "投苗数量")
  @SaturnColumn(description = "投苗数量")
  @Column(name = "put_fry_quantity", columnDefinition = "int comment '投苗数量'")
  private int putFryQuantity;

  @ApiModelProperty(name = "putFryWeight", value = "投苗重量")
  @SaturnColumn(description = "投苗重量")
  @Column(name = "put_fry_weight", columnDefinition = "DECIMAL(30,2) comment '投苗重量'")
  private BigDecimal putFryWeight;

  @ApiModelProperty(name = "fishFryPrice", value = "鱼苗单价")
  @SaturnColumn(description = "鱼苗单价")
  @Column(name = "fish_fry_price", columnDefinition = "DECIMAL(10,5) comment '鱼苗单价'")
  private BigDecimal fishFryPrice;

  @ApiModelProperty(name = "fishFryTotalPrice", value = "投苗金额")
  @SaturnColumn(description = "投苗金额")
  @Column(name = "fish_fry_total_price", columnDefinition = "DECIMAL(30,5) comment '投苗金额'")
  private BigDecimal fishFryTotalPrice;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public OrganizationEntity getCompany() {
    return company;
  }

  public void setCompany(OrganizationEntity company) {
    this.company = company;
  }

  public TwBaseEntity getTwBase() {
    return twBase;
  }

  public void setTwBase(TwBaseEntity twBase) {
    this.twBase = twBase;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public FishTypeEntity getFishType() {
    return fishType;
  }

  public void setFishType(FishTypeEntity fishType) {
    this.fishType = fishType;
  }

  public Date getPutFryTime() {
    return putFryTime;
  }

  public void setPutFryTime(Date putFryTime) {
    this.putFryTime = putFryTime;
  }

  public int getPutFryQuantity() {
    return putFryQuantity;
  }

  public void setPutFryQuantity(int putFryQuantity) {
    this.putFryQuantity = putFryQuantity;
  }

  public BigDecimal getPutFryWeight() {
    return putFryWeight;
  }

  public void setPutFryWeight(BigDecimal putFryWeight) {
    this.putFryWeight = putFryWeight;
  }

  public BigDecimal getFishFryPrice() {
    return fishFryPrice;
  }

  public void setFishFryPrice(BigDecimal fishFryPrice) {
    this.fishFryPrice = fishFryPrice;
  }

  public BigDecimal getFishFryTotalPrice() {
    return fishFryTotalPrice;
  }

  public void setFishFryTotalPrice(BigDecimal fishFryTotalPrice) {
    this.fishFryTotalPrice = fishFryTotalPrice;
  }

}
