package com.tw.aquaculture.common.entity.plan;

import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * 年度计划
 *
 * @author bo shi
 */
@ApiModel(value = "YearlyPlanEntity", description = "年度计划")
@Entity
@Table(name = "tw_yearly_plan")
@SaturnDomain(value = "planmanagement")
public class YearlyPlanEntity extends BaseFieldsEntity {

  /**
   * 
   */
  private static final long serialVersionUID = 9099994888801088142L;

  @ApiModelProperty(name = "code", value = "规划编码")
  @SaturnColumn(description = "规划编码")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(255) comment '规划编码'")
  private String code;

  @ApiModelProperty(name = "year", value = "年度")
  @SaturnColumn(description = "年度")
  @Column(name = "year", columnDefinition = "varchar(10) comment '年度'")
  private String year;

  @ApiModelProperty(name = "company", value = "公司")
  @JoinColumn(name = "org_id", columnDefinition = "varchar(255) comment'公司id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "公司id")
  private OrganizationEntity company;

  @ApiModelProperty(name = "twBase", value = "基地")
  @JoinColumn(name = "tw_base_id", columnDefinition = "varchar(255) comment'基地id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "基地id")
  private TwBaseEntity twBase;

  @ApiModelProperty(name = "pond", value = "塘口")
  @JoinColumn(name = "pond_id", columnDefinition = "varchar(255) comment'塘口id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "塘口id")
  private PondEntity pond;

  @JoinColumn(name = "fish_type", nullable = false, columnDefinition = "varchar(64) comment'鱼种'")
  @SaturnColumn(description = "鱼种")
  @ApiModelProperty(name = "fishType", value = "鱼种", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private FishTypeEntity fishType;

  @ApiModelProperty(name = "putFryQuantity", value = "投苗数量(尾)")
  @SaturnColumn(description = "投苗数量(尾)")
  @Column(name = "put_fry_quantity", columnDefinition = "DECIMAL(30,5) comment '投苗数量(尾)'")
  private int putFryQuantity;

  @ApiModelProperty(name = "putFryAmount", value = "投苗金额(元)")
  @SaturnColumn(description = "投苗金额(元)")
  @Column(name = "put_fry_amount", columnDefinition = "DECIMAL(30,5) comment '投苗金额(元)'")
  private BigDecimal putFryAmount;

  @ApiModelProperty(name = "putFryWeight", value = "投苗重量(斤)")
  @SaturnColumn(description = "投苗重量(斤)")
  @Column(name = "put_fry_weight", columnDefinition = "DECIMAL(30,2) comment '投苗重量(斤)'")
  private BigDecimal putFryWeight;

  @ApiModelProperty(name = "outFishWeight", value = "出鱼重量(斤)")
  @SaturnColumn(description = "出鱼重量(斤)")
  @Column(name = "out_fish_weight", columnDefinition = "DECIMAL(30, 2) comment'出鱼重量(斤)'")
  private BigDecimal outFishWeight;

  @ApiModelProperty(name = "outFishQuantity", value = "出鱼数量(尾)")
  @SaturnColumn(description = "出鱼数量(尾)")
  @Column(name = "out_fish_quantity", columnDefinition = "DECIMAL(30,5) comment '出鱼数量(尾)'")
  private int outFishQuantity;

  @ApiModelProperty(name = "outFishAmount", value = "出鱼金额(元)")
  @SaturnColumn(description = "出鱼金额(元)")
  @Column(name = "out_fish_amount", columnDefinition = "DECIMAL(30,5) comment '出鱼金额(元)'")
  private BigDecimal outFishAmount;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public OrganizationEntity getCompany() {
    return company;
  }

  public void setCompany(OrganizationEntity company) {
    this.company = company;
  }

  public TwBaseEntity getTwBase() {
    return twBase;
  }

  public void setTwBase(TwBaseEntity twBase) {
    this.twBase = twBase;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public FishTypeEntity getFishType() {
    return fishType;
  }

  public void setFishType(FishTypeEntity fishType) {
    this.fishType = fishType;
  }

  public int getPutFryQuantity() {
    return putFryQuantity;
  }

  public void setPutFryQuantity(int putFryQuantity) {
    this.putFryQuantity = putFryQuantity;
  }

  public BigDecimal getPutFryAmount() {
    return putFryAmount;
  }

  public void setPutFryAmount(BigDecimal putFryAmount) {
    this.putFryAmount = putFryAmount;
  }

  public BigDecimal getPutFryWeight() {
    return putFryWeight;
  }

  public void setPutFryWeight(BigDecimal putFryWeight) {
    this.putFryWeight = putFryWeight;
  }

  public BigDecimal getOutFishWeight() {
    return outFishWeight;
  }

  public void setOutFishWeight(BigDecimal outFishWeight) {
    this.outFishWeight = outFishWeight;
  }

  public int getOutFishQuantity() {
    return outFishQuantity;
  }

  public void setOutFishQuantity(int outFishQuantity) {
    this.outFishQuantity = outFishQuantity;
  }

  public BigDecimal getOutFishAmount() {
    return outFishAmount;
  }

  public void setOutFishAmount(BigDecimal outFishAmount) {
    this.outFishAmount = outFishAmount;
  }

}
