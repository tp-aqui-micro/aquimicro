package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author 陈荣
 * @date 2019/11/14 13:23
 */
@Entity
@Table(name = "tw_bask_pond")
@ApiModel(description = "晒塘")
@SaturnDomain("process")
public class BaskPondEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = -8641739573101709485L;

  @ApiModelProperty(value = "编码", name = "code")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment'编码'")
  @SaturnColumn(description = "编码")
  private String code;

  @JoinColumn(name = "pond_id", nullable = false, columnDefinition = "varchar(255) comment'塘口id'")
  @SaturnColumn(description = "塘口")
  @ManyToOne(fetch = FetchType.LAZY)
  @ApiModelProperty(value = "塘口id", name = "pond")
  private PondEntity pond;

  @Column(name = "note", columnDefinition = "varchar(10000) comment'备注'")
  @SaturnColumn(description = "备注")
  @ApiModelProperty(value = "备注", name = "note")
  private String note;

  @Column(name = "picture", columnDefinition = "varchar(1000) comment'图片'")
  @SaturnColumn(description = "图片")
  @ApiModelProperty(value = "图片地址", name = "picture")
  private String picture;

  @JoinColumn(name = "work_order_id", columnDefinition = "varchar(255) comment'工单id'")
  @SaturnColumn(description = "工单")
  @ApiModelProperty(value = "工单", name = "workOrder")
  @ManyToOne(fetch = FetchType.LAZY)
  private WorkOrderEntity workOrder;

  @ApiModelProperty(value = "基地")
  @JoinColumn(name = "base_id", columnDefinition = "varchar(255) comment '基地id'")
  @SaturnColumn(description = "基地")
  @ManyToOne(fetch = FetchType.LAZY)
  private TwBaseEntity base;

  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public WorkOrderEntity getWorkOrder() {
    return workOrder;
  }

  public void setWorkOrder(WorkOrderEntity workOrder) {
    this.workOrder = workOrder;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }
}
