package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author 陈荣
 * @date 2019/11/14 13:22
 */
@Entity
@Table(name = "tw_clean_pond")
@ApiModel(description = "清塘")
@SaturnDomain("process")
public class CleanPondEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = -568610294741263488L;

  @ApiModelProperty(value = "编码", name = "code")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment'编码'")
  @SaturnColumn(description = "编码")
  private String code;

  @JoinColumn(name = "pond_id", nullable = false, columnDefinition = "varchar(255) comment'塘口id'")
  @SaturnColumn(description = "塘口")
  @ManyToOne(fetch = FetchType.LAZY)
  @ApiModelProperty(value = "塘口", name ="pond", required = true)
  private PondEntity pond;

  @Column(name = "reason", columnDefinition = "varchar(1000) comment'清塘原因'")
  @SaturnColumn(description = "清塘原因")
  @ApiModelProperty(value = "清塘原因", name = "reason")
  private String reason;

  @Column(name = "note", columnDefinition = "varchar(1000) comment'备注'")
  @SaturnColumn(description = "备注")
  @ApiModelProperty(value = "备注", name = "note")
  private String note;

  @Column(name = "picture", columnDefinition = "varchar(10000) comment'图片'")
  @SaturnColumn(description = "图片")
  @ApiModelProperty(value = "图片地址", name = "picture")
  private String picture;

  @JoinColumn(name = "work_order_id", columnDefinition = "varchar(255) comment'工单id'")
  @SaturnColumn(description = "工单")
  @ApiModelProperty(value = "工单", name = "workOrder")
  @ManyToOne(fetch = FetchType.LAZY)
  private WorkOrderEntity workOrder;

  @ApiModelProperty(value = "基地")
  @JoinColumn(name = "base_id", columnDefinition = "varchar(255) comment '基地id'")
  @SaturnColumn(description = "基地")
  @ManyToOne(fetch = FetchType.LAZY)
  private TwBaseEntity base;

  @ApiModelProperty(value = "预计清塘时间", required = true)
  @Column(name = "clean_time", columnDefinition = "datetime comment'预计清塘时间'")
  @SaturnColumn(description = "预计清塘时间")
  private Date cleanTime;

  @ApiModelProperty(name="pushWaterTime", value = "预计注水时间", required = true)
  @Column(name = "push_water_time", columnDefinition = "datetime comment'预计注水时间'")
  @SaturnColumn(description = "预计注水时间")
  private Date pushWaterTime;

  @ApiModelProperty(name = "silt", value = "淤泥情况")
  @Column(name = "silt", columnDefinition = "double(10, 2) comment'淤泥情况'")
  @SaturnColumn(description = "淤泥情况")
  private Double silt;

  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public WorkOrderEntity getWorkOrder() {
    return workOrder;
  }

  public void setWorkOrder(WorkOrderEntity workOrder) {
    this.workOrder = workOrder;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public Date getCleanTime() {
    return cleanTime;
  }

  public void setCleanTime(Date cleanTime) {
    this.cleanTime = cleanTime;
  }

  public Date getPushWaterTime() {
    return pushWaterTime;
  }

  public void setPushWaterTime(Date pushWaterTime) {
    this.pushWaterTime = pushWaterTime;
  }

  public Double getSilt() {
    return silt;
  }

  public void setSilt(Double silt) {
    this.silt = silt;
  }
}
