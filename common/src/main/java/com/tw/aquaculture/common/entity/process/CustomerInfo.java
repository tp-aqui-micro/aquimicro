package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.core.entity.UuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author 陈荣
 * @date 2019/11/14 10:47
 */
@Entity
@Table(name = "tw_customer_info")
@ApiModel(description = "客户信息", value = "CustomerInfo")
@SaturnDomain("process")
public class CustomerInfo extends UuidEntity {

  private static final long serialVersionUID = -6008558656954533006L;

  @Column(name = "name", nullable = false, columnDefinition = "varchar(100) comment'客户名称'")
  @SaturnColumn(description = "客户名称")
  @ApiModelProperty(value = "客户名称", name = "name", required = true)
  private String name;

  @Column(name = "link_man", nullable = false, columnDefinition = "varchar(100) comment'联系人'")
  @SaturnColumn(description = "联系人")
  @ApiModelProperty(value = "联系人", name = "linkMan", required = true)
  private String linkMan;

  @Column(name = "link_phone",nullable = false, columnDefinition = "varchar(100) comment'联系电话'")
  @SaturnColumn(description = "联系电话")
  @ApiModelProperty(value = "联系电话", name = "linkPhone", required = true)
  private String linkPhone;

  @Column(name = "need_specs", columnDefinition = "varchar(100) comment'需求规格'")
  @SaturnColumn(description = "需求规格")
  @ApiModelProperty(value = "需求规格", name = "needSpecs")
  private String needSpecs;

  @Column(name = "need_weight", columnDefinition = "double(10, 2) comment'需求数量，实际为重量/斤'")
  @SaturnColumn(description = "需求数量")
  @ApiModelProperty(value = "需求数量，实际为重量/斤", name = "needWeight")
  private Double needWeight;

  @JoinColumn(name = "fish_type", nullable = false, columnDefinition = "varchar(64) comment'鱼种'")
  @SaturnColumn(description = "鱼种")
  @ApiModelProperty(name = "fishType", value = "鱼种", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private FishTypeEntity fishType;

  @Column(name = "price", columnDefinition = "double(10, 2) comment'塘口价'")
  @SaturnColumn(description = "塘口价")
  @ApiModelProperty(value = "塘口价", name = "price")
  private Double price;

  @Column(name = "transport_money", columnDefinition = "double(10, 2) comment'运费'")
  @SaturnColumn(description = "运费")
  @ApiModelProperty(value = "运费", name = "transportMoney")
  private Double transportMoney;

  @Column(name = "price_by_time", nullable = false, columnDefinition = "datetime comment'价格截至时间'")
  @SaturnColumn(description = "价格截至时间")
  @ApiModelProperty(value = "价格截至时间", name = "priceByTime", required = true)
  private Date priceByTime;

  @JoinColumn(name = "out_fish_apply_id", columnDefinition = "varchar(255) comment'出鱼申请id'")
  @SaturnColumn(description = "出鱼申请")
  @ApiModelProperty(value = "出鱼申请")
  @ManyToOne(fetch = FetchType.LAZY)
  private OutFishApplyEntity outFishApply;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLinkMan() {
    return linkMan;
  }

  public void setLinkMan(String linkMan) {
    this.linkMan = linkMan;
  }

  public String getLinkPhone() {
    return linkPhone;
  }

  public void setLinkPhone(String linkPhone) {
    this.linkPhone = linkPhone;
  }

  public String getNeedSpecs() {
    return needSpecs;
  }

  public void setNeedSpecs(String needSpecs) {
    this.needSpecs = needSpecs;
  }

  public Double getNeedWeight() {
    return needWeight;
  }

  public void setNeedWeight(Double needWeight) {
    this.needWeight = needWeight;
  }

  public FishTypeEntity getFishType() {
    return fishType;
  }

  public void setFishType(FishTypeEntity fishType) {
    this.fishType = fishType;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Double getTransportMoney() {
    return transportMoney;
  }

  public void setTransportMoney(Double transportMoney) {
    this.transportMoney = transportMoney;
  }

  public Date getPriceByTime() {
    return priceByTime;
  }

  public void setPriceByTime(Date priceByTime) {
    this.priceByTime = priceByTime;
  }

  public OutFishApplyEntity getOutFishApply() {
    return outFishApply;
  }

  public void setOutFishApply(OutFishApplyEntity outFishApply) {
    this.outFishApply = outFishApply;
  }
}
