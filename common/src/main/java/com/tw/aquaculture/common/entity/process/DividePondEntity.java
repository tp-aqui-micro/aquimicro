package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.Set;

/**
 * @author 陈荣
 * @date 2019/11/14 9:50
 */
@Entity
@Table(name = "tw_divide_pond")
@ApiModel(description = "转/分塘", value = "DividePondEntity")
@SaturnDomain("process")
public class DividePondEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = 6538194592347777349L;

  @ApiModelProperty(value = "编码", name = "code")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment'编码'")
  @SaturnColumn(description = "编码")
  private String code;

  @JoinColumn(name = "out_pond_id", nullable = false, columnDefinition = "varchar(255) comment'转出塘口id'")
  @SaturnColumn(description = "转出塘口")
  @ManyToOne(fetch = FetchType.LAZY)
  @ApiModelProperty(value = "转出塘口", name = "outPond", required = true)
  private PondEntity outPond;

  @JoinColumn(name = "in_pond_id", nullable = false, columnDefinition = "varchar(255) comment'转入塘口id'")
  @SaturnColumn(description = "转入塘口")
  @ManyToOne(fetch = FetchType.LAZY)
  @ApiModelProperty(value = "转入塘口id", name = "inPond", required = true)
  private PondEntity inPond;

  @JoinColumn(name = "fish_type", nullable = false, columnDefinition = "varchar(64) comment'鱼种'")
  @SaturnColumn(description = "鱼种")
  @ApiModelProperty(name = "fishType", value = "鱼种", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private FishTypeEntity fishType;

  @Column(name = "divide_pond_time", columnDefinition = "datetime default current_timestamp comment'分塘时间'")
  @SaturnColumn(description = "分塘时间")
  @ApiModelProperty(value = "分塘时间", name = "dividePondTime")
  private Date dividePondTime;

  @Column(name = "supervisor", columnDefinition = "varchar(100) comment'监磅人'")
  @SaturnColumn(description = "监磅人")
  @ApiModelProperty(value = "监磅人", name = "supervisor")
  private String supervisor;

  @JoinColumn(name = "group_leader_id", columnDefinition = "varchar(100) comment'生产组长'")
  @SaturnColumn(description = "生产组长")
  @ManyToOne(fetch = FetchType.LAZY)
  @ApiModelProperty(value = "生产组长", name = "groupLeader")
  private UserEntity groupLeader;

  @SaturnColumn(description = "分塘明细表")
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "dividePond")
  @ApiModelProperty(value = "分塘明细", name = "dividePondItems")
  private Set<DividePondItemEntity> dividePondItems;

  @Column(name = "total_weight", columnDefinition = "double(10, 2) comment'总重量'")
  @SaturnColumn(description = "总重量")
  @ApiModelProperty(value = "总重量", name = "totalWeight")
  private Double totalWeight;

  @Column(name = "specs", columnDefinition = "varchar(100) comment'平均规格,斤/尾'")
  @SaturnColumn(description = "平均规格")
  @ApiModelProperty(value = "平均规格，斤/尾", name = "specs")
  private String specs;

  @Column(name = "price", columnDefinition = "double(10, 2) comment'单价，尾/元'")
  @SaturnColumn(description = "单价")
  @ApiModelProperty(value = "单价，尾/元", name = "price")
  private Double price;

  @Column(name = "amount", columnDefinition = "double(10, 2) comment'金额'")
  @SaturnColumn(description = "金额")
  @ApiModelProperty(value = "金额/元", name = "amount")
  private Double amount;

  @Column(name = "note", columnDefinition = "varchar(1000) comment'备注'")
  @SaturnColumn(description = "备注")
  @ApiModelProperty(value = "备注", name = "note")
  private String note;

  @Column(name = "picture", columnDefinition = "varchar(10000) comment'图片'")
  @SaturnColumn(description = "图片")
  @ApiModelProperty(value = "图片地址", name = "picture")
  private String picture;

  @JoinColumn(name = "work_order_id", columnDefinition = "varchar(255) comment'工单id'")
  @SaturnColumn(description = "工单")
  @ApiModelProperty(value = "工单", name = "workOrder")
  @ManyToOne(fetch = FetchType.LAZY)
  private WorkOrderEntity workOrder;

  public WorkOrderEntity getWorkOrder() {
    return workOrder;
  }

  public void setWorkOrder(WorkOrderEntity workOrder) {
    this.workOrder = workOrder;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public PondEntity getOutPond() {
    return outPond;
  }

  public void setOutPond(PondEntity outPond) {
    this.outPond = outPond;
  }

  public PondEntity getInPond() {
    return inPond;
  }

  public void setInPond(PondEntity inPond) {
    this.inPond = inPond;
  }

  public FishTypeEntity getFishType() {
    return fishType;
  }

  public void setFishType(FishTypeEntity fishType) {
    this.fishType = fishType;
  }

  public Date getDividePondTime() {
    return dividePondTime;
  }

  public void setDividePondTime(Date dividePondTime) {
    this.dividePondTime = dividePondTime;
  }

  public String getSupervisor() {
    return supervisor;
  }

  public void setSupervisor(String supervisor) {
    this.supervisor = supervisor;
  }

  public UserEntity getGroupLeader() {
    return groupLeader;
  }

  public void setGroupLeader(UserEntity groupLeader) {
    this.groupLeader = groupLeader;
  }

  public Set<DividePondItemEntity> getDividePondItems() {
    return dividePondItems;
  }

  public void setDividePondItems(Set<DividePondItemEntity> dividePondItems) {
    this.dividePondItems = dividePondItems;
  }

  public Double getTotalWeight() {
    return totalWeight;
  }

  public void setTotalWeight(Double totalWeight) {
    this.totalWeight = totalWeight;
  }

  public String getSpecs() {
    return specs;
  }

  public void setSpecs(String specs) {
    this.specs = specs;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }
}
