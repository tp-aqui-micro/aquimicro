package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.core.entity.UuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author 陈荣
 * @date 2019/11/14 9:51
 */
@Entity
@Table(name = "tw_divide_pond_item")
@ApiModel(description = "转/分塘明细", value = "DividePondItemEntity")
@SaturnDomain("process")
public class DividePondItemEntity extends UuidEntity {

  private static final long serialVersionUID = 5817338586355923429L;

  @Column(name = "count", columnDefinition = "int(10) comment'数量'")
  @SaturnColumn(description = "数量")
  @ApiModelProperty(value = "数量", name = "count")
  private Double count;

  @Column(name = "weight", columnDefinition = "double(10, 2) comment'重量'")
  @SaturnColumn(description = "重量")
  @ApiModelProperty(value = "重量", name = "weight")
  private Double weight;

  @JoinColumn(name = "divide_pond_id", columnDefinition = "varchar(255) comment'转/分塘id'")
  @SaturnColumn(description = "转/分塘")
  @ManyToOne(fetch = FetchType.LAZY)
  @ApiModelProperty(value = "转/分塘", name = "dividePond")
  private DividePondEntity dividePond;

  public Double getCount() {
    return count;
  }

  public void setCount(Double count) {
    this.count = count;
  }

  public Double getWeight() {
    return weight;
  }

  public void setWeight(Double weight) {
    this.weight = weight;
  }

  public DividePondEntity getDividePond() {
    return dividePond;
  }

  public void setDividePond(DividePondEntity dividePond) {
    this.dividePond = dividePond;
  }
}
