package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author 陈荣
 * @date 2019/11/13 17:39
 */
@Entity
@Table(name = "tw_fish_damage")
@SaturnDomain(value = "process")
@ApiModel(description = "损鱼记录", value = "FishDamageEntity")
public class FishDamageEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = -2137197559600034524L;

  @ApiModelProperty(value = "编码", name = "code")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment'编码'")
  @SaturnColumn(description = "编码")
  private String code;

  @JoinColumn(name = "pond_id", nullable = false, columnDefinition = "varchar(255) comment'塘口Id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @ApiModelProperty(value = "塘口", name = "pond", required = true)
  @SaturnColumn(description = "塘口")
  private PondEntity pond;

  @Column(name = "damage_time", columnDefinition = "datetime default current_timestamp comment'损鱼时间'")
  @SaturnColumn(description = "损鱼时间")
  @ApiModelProperty(value = "损鱼时间", name = "damageTime")
  private Date damageTime;

  @Column(name = "damage_reason", columnDefinition = "varchar(1000) comment'损鱼原因'")
  @SaturnColumn(description = "损鱼原因")
  @ApiModelProperty(value = "损鱼原因", name = "damageReason")
  private String damageReason;

  @JoinColumn(name = "fish_type", nullable = false, columnDefinition = "varchar(64) comment'鱼种'")
  @SaturnColumn(description = "鱼种")
  @ApiModelProperty(name = "fishType", value = "鱼种", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private FishTypeEntity fishType;

  @Column(name = "specs", columnDefinition = "double(10, 2) comment'规格'")
  @SaturnColumn(description = "规格")
  @ApiModelProperty(value = "规格", name = "specs")
  private Double specs;

  @Column(name = "count", columnDefinition = "double(10, 2) comment'数量'")
  @SaturnColumn(description = "数量")
  @ApiModelProperty(value = "数量", name = "count")
  private Double count;

  @Column(name = "weight", columnDefinition = "double(10, 2) comment'重量/斤'")
  @SaturnColumn(description = "重量")
  @ApiModelProperty(value = "重量/斤", name = "weight")
  private Double weight;

  @Column(name = "note", columnDefinition = "varchar(1000) comment'备注'")
  @SaturnColumn(description = "备注")
  @ApiModelProperty(value = "备注", name = "note")
  private String note;

  @Column(name = "picture", columnDefinition = "varchar(10000) comment'图片'")
  @SaturnColumn(description = "图片")
  @ApiModelProperty(value = "图片地址", name = "picture")
  private String picture;

  @JoinColumn(name = "work_order_id", columnDefinition = "varchar(255) comment'工单id'")
  @SaturnColumn(description = "工单")
  @ApiModelProperty(value = "工单", name = "workOrder")
  @ManyToOne(fetch = FetchType.LAZY)
  private WorkOrderEntity workOrder;

  @ApiModelProperty(value = "基地")
  @JoinColumn(name = "base_id", columnDefinition = "varchar(255) comment '基地id'")
  @SaturnColumn(description = "基地")
  @ManyToOne(fetch = FetchType.LAZY)
  private TwBaseEntity base;

  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public Date getDamageTime() {
    return damageTime;
  }

  public void setDamageTime(Date damageTime) {
    this.damageTime = damageTime;
  }

  public String getDamageReason() {
    return damageReason;
  }

  public void setDamageReason(String damageReason) {
    this.damageReason = damageReason;
  }

  public FishTypeEntity getFishType() {
    return fishType;
  }

  public void setFishType(FishTypeEntity fishType) {
    this.fishType = fishType;
  }

  public Double getCount() {
    return count;
  }

  public void setCount(Double count) {
    this.count = count;
  }

  public Double getWeight() {
    return weight;
  }

  public void setWeight(Double weight) {
    this.weight = weight;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public WorkOrderEntity getWorkOrder() {
    return workOrder;
  }

  public void setWorkOrder(WorkOrderEntity workOrder) {
    this.workOrder = workOrder;
  }

  public Double getSpecs() {
    return specs;
  }

  public void setSpecs(Double specs) {
    this.specs = specs;
  }
}
