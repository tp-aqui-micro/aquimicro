package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.core.entity.UuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author 陈荣
 * @date 2019/11/26 17:06
 */
@Entity
@Table(name = "tw_medicine_feedback")
@SaturnDomain(value = "process")
@ApiModel(value = "MedicineFeedBackEntity", description = "用药反馈")
public class MedicineFeedBackEntity extends UuidEntity {

  private static final long serialVersionUID = -6540403015174638235L;

  @Column(name = "content", columnDefinition = "varchar(5000) comment'用药反馈'")
  @SaturnColumn(description = "用药反馈")
  @ApiModelProperty(name = "content", value = "用药反馈", required = true)
  private String content;

  @JoinColumn(name = "parant_id", columnDefinition = "varchar(255) comment'上次用药反馈id'")
  @SaturnColumn(description = "上次用药反馈")
  @ApiModelProperty(name = "parent", value = "上次用药反馈")
  @ManyToOne(fetch = FetchType.LAZY)
  private MedicineFeedBackEntity parent;

  @JoinColumn(name = "safeguard_id", nullable = false, columnDefinition = "varchar(255) comment'动保id'")
  @SaturnColumn(description = "动保")
  @ApiModelProperty(name = "safeguard", value = "动保")
  @ManyToOne(fetch = FetchType.LAZY)
  private SafeguardEntity safeguard;

  @Column(name = "back_time")
  @SaturnColumn(description = "反馈时间")
  @ApiModelProperty(name = "backTime", value = "反馈时间")
  private Date backTime;

  @Column(name = "file", columnDefinition = "varchar(5000) comment'文件地址'")
  @SaturnColumn(description = "文件地址")
  @ApiModelProperty(name = "file", value = "文件地址")
  private String file;

  public String getFile() {
    return file;
  }

  public void setFile(String file) {
    this.file = file;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public MedicineFeedBackEntity getParent() {
    return parent;
  }

  public void setParent(MedicineFeedBackEntity parent) {
    this.parent = parent;
  }

  public SafeguardEntity getSafeguard() {
    return safeguard;
  }

  public void setSafeguard(SafeguardEntity safeguard) {
    this.safeguard = safeguard;
  }

  public Date getBackTime() {
    return backTime;
  }

  public void setBackTime(Date backTime) {
    this.backTime = backTime;
  }
}
