package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.core.entity.UuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.base.CustomerEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author 陈荣
 * @date 2019/11/29 14:40
 */
@Entity
@Table(name = "tw_out_apply_custom_state")
@ApiModel(description = "客户岀鱼是否中标维护", value = "OutApplyCustomStateEntity")
@SaturnDomain("process")
public class OutApplyCustomStateEntity extends UuidEntity {

  private static final long serialVersionUID = -8540207553560385569L;

  @JoinColumn(name = "out_apply_id", nullable = false, columnDefinition = "varchar(255) comment'岀鱼申请id'")
  @OneToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "岀鱼申请")
  @ApiModelProperty(value = "岀鱼申请", name = "outApplyId", required = true)
  private OutFishApplyEntity outFishApply;

  @JoinColumn(name = "custom_id", nullable = false, columnDefinition = "varchar(255) comment'客户id'")
  @OneToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "客户")
  @ApiModelProperty(value = "客户", name = "customId", required = true)
  private CustomerEntity customer;

  @Column(name = "state", nullable = false, columnDefinition = "int(11) comment'中标状态，1：中标，0：未中标'")
  @SaturnColumn(description = "中标状态")
  @ApiModelProperty(value = "中标状态", name = "state", required = true)
  private Integer state;

  @Column(name = "winning_mark", columnDefinition = "varchar(255) comment'中标标识'")
  @SaturnColumn(description = "中标标识")
  @ApiModelProperty(name = "winningMark", value = "中标标识")
  private String winningMark;

  @Column(name = "varieties_sales", columnDefinition = "varchar(255) comment'品种'")
  @SaturnColumn(description = "品种")
  @ApiModelProperty(name = "varietiesSales", value = "品种")
  private String varietiesSales;

  @Column(name = "quantity_num", columnDefinition = "varchar(255) comment'需求数量'")
  @SaturnColumn(description = "需求数量")
  @ApiModelProperty(name = "quantityNum", value = "需求数量")
  private String quantityNum;

  @Column(name = "require_specification", columnDefinition = "varchar(255) comment'需求规格'")
  @SaturnColumn(description = "需求规格")
  @ApiModelProperty(name = "requireSpecification", value = "需求规格")
  private String requireSpecification;

  @Column(name = "tangkouprice", columnDefinition = "varchar(255) comment'塘口价'")
  @SaturnColumn(description = "塘口价")
  @ApiModelProperty(name = "tangkouprice", value = "塘口价")
  private String tangkouprice;

  @Column(name = "freight", columnDefinition = "varchar(255) comment'运费'")
  @SaturnColumn(description = "运费")
  @ApiModelProperty(name = "freight", value = "运费")
  private String freight;

  @Column(name = "price_deadline", columnDefinition = "datetime comment'价格截止时间'")
  @SaturnColumn(description = "价格截止时间")
  @ApiModelProperty(name = "priceDeadline", value = "价格截止时间")
  private Date priceDeadline;

  @Column(name = "sales_specification", columnDefinition = "varchar(255) comment'销售规格'")
  @SaturnColumn(description = "销售规格")
  @ApiModelProperty(name = "salesSpecification", value = "销售规格")
  private String salesSpecification;

  @Column(name = "salesfreight", columnDefinition = "varchar(255) comment'数量'")
  @SaturnColumn(description = "数量")
  @ApiModelProperty(name = "salesfreight", value = "数量")
  private String salesfreight;

  @Column(name = "tangkou_pricecomfirm", columnDefinition = "varchar(255) comment'塘口价格'")
  @SaturnColumn(description = "塘口价格")
  @ApiModelProperty(name = "tangkouPricecomfirm", value = "塘口价格")
  private String tangkouPricecomfirm;

  @Column(name = "freightcomfirm", columnDefinition = "varchar(255) comment'运费'")
  @SaturnColumn(description = "运费")
  @ApiModelProperty(name = "freightcomfirm", value = "运费")
  private String freightcomfirm;

  @Column(name = "sales_remarks", columnDefinition = "varchar(255) comment'备注'")
  @SaturnColumn(description = "备注")
  @ApiModelProperty(name = "salesRemarks", value = "备注")
  private String salesRemarks;

  @Column(name = "base_name", columnDefinition = "varchar(255) comment'基地名称'")
  @SaturnColumn(description = "基地名称")
  @ApiModelProperty(name = "baseName", value = "基地名称")
  private String baseName;

  @Column(name = "pond_name", columnDefinition = "varchar(255) comment'塘口名称'")
  @SaturnColumn(description = "塘口名称")
  @ApiModelProperty(name = "pondName", value = "塘口名称")
  private String pondName;

  public OutFishApplyEntity getOutFishApply() {
    return outFishApply;
  }

  public void setOutFishApply(OutFishApplyEntity outFishApply) {
    this.outFishApply = outFishApply;
  }

  public CustomerEntity getCustomer() {
    return customer;
  }

  public void setCustomer(CustomerEntity customer) {
    this.customer = customer;
  }

  public Integer getState() {
    return state;
  }

  public void setState(Integer state) {
    this.state = state;
  }

  public String getWinningMark() {
    return winningMark;
  }

  public void setWinningMark(String winningMark) {
    this.winningMark = winningMark;
  }

  public String getVarietiesSales() {
    return varietiesSales;
  }

  public void setVarietiesSales(String varietiesSales) {
    this.varietiesSales = varietiesSales;
  }

  public String getQuantityNum() {
    return quantityNum;
  }

  public void setQuantityNum(String quantityNum) {
    this.quantityNum = quantityNum;
  }

  public String getRequireSpecification() {
    return requireSpecification;
  }

  public void setRequireSpecification(String requireSpecification) {
    this.requireSpecification = requireSpecification;
  }

  public String getTangkouprice() {
    return tangkouprice;
  }

  public void setTangkouprice(String tangkouprice) {
    this.tangkouprice = tangkouprice;
  }

  public String getFreight() {
    return freight;
  }

  public void setFreight(String freight) {
    this.freight = freight;
  }

  public Date getPriceDeadline() {
    return priceDeadline;
  }

  public void setPriceDeadline(Date priceDeadline) {
    this.priceDeadline = priceDeadline;
  }

  public String getSalesSpecification() {
    return salesSpecification;
  }

  public void setSalesSpecification(String salesSpecification) {
    this.salesSpecification = salesSpecification;
  }

  public String getSalesfreight() {
    return salesfreight;
  }

  public void setSalesfreight(String salesfreight) {
    this.salesfreight = salesfreight;
  }

  public String getTangkouPricecomfirm() {
    return tangkouPricecomfirm;
  }

  public void setTangkouPricecomfirm(String tangkouPricecomfirm) {
    this.tangkouPricecomfirm = tangkouPricecomfirm;
  }

  public String getFreightcomfirm() {
    return freightcomfirm;
  }

  public void setFreightcomfirm(String freightcomfirm) {
    this.freightcomfirm = freightcomfirm;
  }

  public String getSalesRemarks() {
    return salesRemarks;
  }

  public void setSalesRemarks(String salesRemarks) {
    this.salesRemarks = salesRemarks;
  }

  public String getBaseName() {
    return baseName;
  }

  public void setBaseName(String baseName) {
    this.baseName = baseName;
  }

  public String getPondName() {
    return pondName;
  }

  public void setPondName(String pondName) {
    this.pondName = pondName;
  }
}
