package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.Set;

/**
 * @author 陈荣
 * @date 2019/11/14 10:12
 */
@Entity
@Table(name = "tw_out_fish_apply")
@ApiModel(description = "出鱼申请", value = "OutFishApplyEntity")
@SaturnDomain("process")
public class OutFishApplyEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = -257733891868864867L;

  @ApiModelProperty(value = "编码", name = "code")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment'编码'")
  @SaturnColumn(description = "编码")
  private String code;

  @JoinColumn(name = "pond_id", nullable = false, columnDefinition = "varchar(255) comment'塘口id'")
  @SaturnColumn(description = "塘口")
  @ManyToOne(fetch = FetchType.LAZY)
  @ApiModelProperty(value = "塘口", name = "pond", required = true)
  private PondEntity pond;

  @Column(name = "apply_reason", columnDefinition = "varchar(1000) comment'申请理由'")
  @SaturnColumn(description = "申请理由")
  @ApiModelProperty(value = "申请理由", name = "applyReason")
  private String applyReason;

  @SaturnColumn(description = "存塘信息")
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "outFishApply",targetEntity = RemainInfoEntity.class)
  @ApiModelProperty(value = "存塘信息", name = "remainInfos")
  private Set<RemainInfoEntity> remainInfos;

  @SaturnColumn(description = "客户信息")
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "outFishApply", targetEntity = CustomerInfo.class)
  @ApiModelProperty(value = "客户信息", name = "customerInfos")
  private Set<CustomerInfo> customerInfos;

  @Column(name = "resure_state", columnDefinition = "int(11) comment'审批状态'")
  @SaturnColumn(description = "审批状态")
  @ApiModelProperty(name = "resureState", value = "审批状态")
  private int resureState;

  @Column(name = "apply_time", columnDefinition = "datetime default current_timestamp comment'申请时间'")
  @SaturnColumn(description = "申请时间")
  @ApiModelProperty(name = "applyTime", value = "申请时间")
  private Date applyTime;

  @Column(name = "predict_time", columnDefinition = "datetime default current_timestamp comment'预计出鱼时间'")
  @SaturnColumn(description = "预计出鱼时间")
  @ApiModelProperty(name = "predictTime", value = "预计出鱼时间")
  private Date predictTime;


  @JoinColumn(name = "work_order_id", columnDefinition = "varchar(255) comment'工单id'")
  @SaturnColumn(description = "工单")
  @ApiModelProperty(value = "工单", name = "workOrder")
  @ManyToOne(fetch = FetchType.LAZY)
  private WorkOrderEntity workOrder;

  @ApiModelProperty(value = "基地")
  @JoinColumn(name = "base_id", columnDefinition = "varchar(255) comment '基地id'")
  @SaturnColumn(description = "基地")
  @ManyToOne(fetch = FetchType.LAZY)
  private TwBaseEntity base;

  @Column(name = "req_no", columnDefinition = "varchar(255) comment'FBC的表单号-FBC流程的唯一标识值'")
  @SaturnColumn(description = "FBC的表单号-FBC流程的唯一标识值")
  @ApiModelProperty(name = "reqNo", value = "FBC的表单号-FBC流程的唯一标识值")
  private String reqNo;

  @Column(name = "instid", columnDefinition = "varchar(255) comment'FBC返回流程ID'")
  @SaturnColumn(description = "FBC返回流程ID")
  @ApiModelProperty(name = "instid", value = "FBC返回流程ID")
  private String instid;

  @Column(name = "resure_idea", columnDefinition = "varchar(1000) comment'审批意见'")
  @SaturnColumn(description = "审批意见")
  @ApiModelProperty(name = "resureIdea", value = "审批意见")
  private String resureIdea;

  public String getResureIdea() {
    return resureIdea;
  }

  public void setResureIdea(String resureIdea) {
    this.resureIdea = resureIdea;
  }

  public String getInstid() {
    return instid;
  }

  public void setInstid(String instid) {
    this.instid = instid;
  }

  public String getReqNo() {
    return reqNo;
  }

  public void setReqNo(String reqNo) {
    this.reqNo = reqNo;
  }
  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public String getApplyReason() {
    return applyReason;
  }

  public void setApplyReason(String applyReason) {
    this.applyReason = applyReason;
  }

  public Set<RemainInfoEntity> getRemainInfos() {
    return remainInfos;
  }

  public void setRemainInfos(Set<RemainInfoEntity> remainInfos) {
    this.remainInfos = remainInfos;
  }

  public Set<CustomerInfo> getCustomerInfos() {
    return customerInfos;
  }

  public void setCustomerInfos(Set<CustomerInfo> customerInfos) {
    this.customerInfos = customerInfos;
  }

  public int getResureState() {
    return resureState;
  }

  public void setResureState(int resureState) {
    this.resureState = resureState;
  }

  public WorkOrderEntity getWorkOrder() {
    return workOrder;
  }

  public void setWorkOrder(WorkOrderEntity workOrder) {
    this.workOrder = workOrder;
  }

  public Date getApplyTime() {
    return applyTime;
  }

  public void setApplyTime(Date applyTime) {
    this.applyTime = applyTime;
  }

  public Date getPredictTime() {
    return predictTime;
  }

  public void setPredictTime(Date predictTime) {
    this.predictTime = predictTime;
  }
}
