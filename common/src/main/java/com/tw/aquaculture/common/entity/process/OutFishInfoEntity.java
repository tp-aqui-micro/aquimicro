package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.core.entity.UuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author 陈荣
 * @date 2019/11/14 11:35
 */
@Entity
@Table(name = "tw_out_fish_info")
@ApiModel(description = "出鱼信息")
@SaturnDomain("process")
public class OutFishInfoEntity extends UuidEntity {

  private static final long serialVersionUID = -2356394988010804968L;

  @JoinColumn(name = "fish_type", nullable = false, columnDefinition = "varchar(64) comment'鱼种'")
  @SaturnColumn(description = "鱼种")
  @ApiModelProperty(name = "fishType", value = "鱼种", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private FishTypeEntity fishType;

  @Column(name = "specs", columnDefinition = "double(10,2) comment'规格'")
  @SaturnColumn(description = "规格")
  @ApiModelProperty(value = "规格", name = "specs")
  private Double specs;

  @Column(name = "total_weight", columnDefinition = "double(10, 2) comment'总重量'")
  @SaturnColumn(description = "总重量")
  @ApiModelProperty(value = "总重量", name = "totalWeight")
  private Double totalWeight;

  @Column(name = "price", columnDefinition = "double(10, 2) comment'塘口价格'")
  @SaturnColumn(description = "塘口价格")
  @ApiModelProperty(value = "塘口价格", name = "price")
  private String price;

  @Column(name = "transport_fee", columnDefinition = "double(10, 2) comment'运费'")
  @SaturnColumn(description = "运费")
  @ApiModelProperty(value = "运费", name = "transportFee")
  private String transportFee;

  @Column(name = "amount", columnDefinition = "double(10, 2) comment'金额'")
  @SaturnColumn(description = "金额")
  @ApiModelProperty(value = "金额", name = "amount")
  private String amount;

  @JoinColumn(name = "out_fish_regist_id", columnDefinition = "varchar(255) comment'出鱼登记id'")
  @SaturnColumn(description = "出鱼登记")
  @ManyToOne(fetch = FetchType.LAZY)
  @ApiModelProperty(name = "outFishRegist", value = "出于登记")
  private OutFishRegistEntity outFishRegist;

  public FishTypeEntity getFishType() {
    return fishType;
  }

  public void setFishType(FishTypeEntity fishType) {
    this.fishType = fishType;
  }

  public double getSpecs() {
    return specs;
  }

  public void setSpecs(double specs) {
    this.specs = specs;
  }

  public void setSpecs(Double specs) {
    this.specs = specs;
  }

  public Double getTotalWeight() {
    return totalWeight;
  }

  public void setTotalWeight(Double totalWeight) {
    this.totalWeight = totalWeight;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getTransportFee() {
    return transportFee;
  }

  public void setTransportFee(String transportFee) {
    this.transportFee = transportFee;
  }

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public OutFishRegistEntity getOutFishRegist() {
    return outFishRegist;
  }

  public void setOutFishRegist(OutFishRegistEntity outFishRegist) {
    this.outFishRegist = outFishRegist;
  }
}
