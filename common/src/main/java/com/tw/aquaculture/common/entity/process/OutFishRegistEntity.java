package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.CustomerEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.Set;

/**
 * @author 陈荣
 * @date 2019/11/14 11:08
 */
@Entity
@Table(name = "tw_out_fish_regist")
@ApiModel(description = "出鱼登记", value = "OutFishRegistEntity")
@SaturnDomain("process")
public class OutFishRegistEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = -7691880041250595661L;

  @ApiModelProperty(value = "编码", name = "code")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment'编码'")
  @SaturnColumn(description = "编码")
  private String code;

  @JoinColumn(name = "pond_id", nullable = false, columnDefinition = "varchar(255) comment'塘口id'")
  @SaturnColumn(description = "塘口")
  @ManyToOne(fetch = FetchType.LAZY)
  @ApiModelProperty(value = "塘口", name = "pond", required = true)
  private PondEntity pond;

  @JoinColumn(name = "customer_id", columnDefinition = "varchar(255) comment'客户id'")
  @SaturnColumn(description = "客户")
  @ManyToOne(fetch = FetchType.LAZY)
  @ApiModelProperty(value = "客户", name = "customer")
  private CustomerEntity customer;

  @Column(name = "supervisor", columnDefinition = "varchar(100) comment'监磅人'")
  @SaturnColumn(description = "监磅人")
  @ApiModelProperty(value = "监磅人", name = "supervisor")
  private String supervisor;

  @JoinColumn(name = "group_leader", columnDefinition = "varchar(64) comment'生产组长'")
  @SaturnColumn(description = "生产组长")
  @ApiModelProperty(value = "生产组长", name = "groupLeader")
  @ManyToOne(fetch = FetchType.LAZY)
  private UserEntity groupLeader;

  @Column(name = "regist_time", columnDefinition = "datetime default current_timestamp comment'登记时间'")
  @SaturnColumn(description = "登记时间")
  @ApiModelProperty(value = "登记时间", name = "registTime")
  private Date registTime;

  @SaturnColumn(description = "出鱼信息")
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "outFishRegist")
  @ApiModelProperty(value = "出鱼信息", name = "outFishInfos")
  private Set<OutFishInfoEntity> outFishInfos;

  @Column(name = "note", columnDefinition = "varchar(1000) comment'备注'")
  @SaturnColumn(description = "备注")
  @ApiModelProperty(value = "备注", name = "note")
  private String note;

  @Column(name = "picture", columnDefinition = "varchar(10000) comment'图片'")
  @SaturnColumn(description = "图片")
  @ApiModelProperty(value = "图片地址", name = "picture")
  private String picture;

  @Column(name = "will_clean_pond", columnDefinition = "int(2) comment'是否准备清塘,0:否，1：是'")
  @SaturnColumn(description = "是否清塘")
  @ApiModelProperty(value = "是否清塘", name = "willCleanPond")
  private Integer willCleanPond;

  @JoinColumn(name = "work_order_id", columnDefinition = "varchar(255) comment'工单id'")
  @SaturnColumn(description = "工单")
  @ApiModelProperty(value = "工单", name = "workOrder")
  @ManyToOne(fetch = FetchType.LAZY)
  private WorkOrderEntity workOrder;

  @JoinColumn(name = "out_fish_apply_id", nullable = false, columnDefinition = "varchar(255) comment'岀鱼申请id'")
  @SaturnColumn(description = "岀鱼申请")
  @ApiModelProperty(name = "outFishApply", value = "岀鱼申请", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private OutFishApplyEntity outFishApply;

  @ApiModelProperty(value = "基地")
  @JoinColumn(name = "base_id", columnDefinition = "varchar(255) comment '基地id'")
  @SaturnColumn(description = "基地")
  @ManyToOne(fetch = FetchType.LAZY)
  private TwBaseEntity base;

  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public CustomerEntity getCustomer() {
    return customer;
  }

  public void setCustomer(CustomerEntity customer) {
    this.customer = customer;
  }

  public String getSupervisor() {
    return supervisor;
  }

  public void setSupervisor(String supervisor) {
    this.supervisor = supervisor;
  }

  public UserEntity getGroupLeader() {
    return groupLeader;
  }

  public void setGroupLeader(UserEntity groupLeader) {
    this.groupLeader = groupLeader;
  }

  public Integer getWillCleanPond() {
    return willCleanPond;
  }

  public void setWillCleanPond(Integer willCleanPond) {
    this.willCleanPond = willCleanPond;
  }

  public Date getRegistTime() {
    return registTime;
  }

  public void setRegistTime(Date registTime) {
    this.registTime = registTime;
  }

  public Set<OutFishInfoEntity> getOutFishInfos() {
    return outFishInfos;
  }

  public void setOutFishInfos(Set<OutFishInfoEntity> outFishInfos) {
    this.outFishInfos = outFishInfos;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public WorkOrderEntity getWorkOrder() {
    return workOrder;
  }

  public void setWorkOrder(WorkOrderEntity workOrder) {
    this.workOrder = workOrder;
  }

  public OutFishApplyEntity getOutFishApply() {
    return outFishApply;
  }

  public void setOutFishApply(OutFishApplyEntity outFishApply) {
    this.outFishApply = outFishApply;
  }
}
