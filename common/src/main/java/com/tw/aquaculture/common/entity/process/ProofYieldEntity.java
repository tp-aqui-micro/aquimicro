package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.Set;

/**
 * @author 陈荣
 * @date 2019/11/13 18:05
 */

@Entity
@Table(name = "tw_proof_yield")
@ApiModel(description = "打样估产", value = "ProofYieldEntity")
@SaturnDomain("process")
public class ProofYieldEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = 2732439014624728253L;

  @ApiModelProperty(value = "编码", name = "code")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment'编码'")
  @SaturnColumn(description = "编码")
  private String code;

  @ApiModelProperty(value = "塘口", name= "pond", required = true)
  @JoinColumn(name = "pond_id", nullable = false, columnDefinition = "varchar(255) comment'塘口id'")
  @SaturnColumn(description = "塘口")
  @ManyToOne(fetch = FetchType.LAZY)
  private PondEntity pond;

  @Column(name = "proof_time", columnDefinition = "datetime default current_timestamp comment'打样时间'")
  @SaturnColumn(description = "打样时间")
  @ApiModelProperty(value = "打样时间", name = "proofTime")
  private Date proofTime;

  @JoinColumn(name = "fish_type", nullable = false, columnDefinition = "varchar(64) comment'鱼种'")
  @SaturnColumn(description = "鱼种")
  @ApiModelProperty(name = "fishType", value = "鱼种", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private FishTypeEntity fishType;

  @Column(name = "supervisor", columnDefinition = "varchar(100) comment'监磅人'")
  @SaturnColumn(description = "监磅人")
  @ApiModelProperty(value = "监磅人", name = "supervisor")
  private String supervisor;

  @JoinColumn(name = "group_leader", columnDefinition = "varchar(64) comment'生产组长'")
  @SaturnColumn(description = "生产组长")
  @ApiModelProperty(value = "生产组长", name = "groupLeader")
  @ManyToOne(fetch = FetchType.LAZY)
  private UserEntity groupLeader;

  @Column(name = "deviation_rate", columnDefinition = "double(10, 2) comment'偏差率'")
  @SaturnColumn(description = "偏差率")
  @ApiModelProperty(value = "偏差率", name = "deviationRate")
  private Double deviationRate;

  @SaturnColumn(description = "打样估产明细表")
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "proofYield", targetEntity = ProofYieldItemEntity.class)
  @ApiModelProperty(value = "打样估产明细表", name = "proofYieldItems")
  private Set<ProofYieldItemEntity> proofYieldItems;

  @Column(name = "specs", columnDefinition = "double(10,2) comment'打样规格,斤/尾'")
  @SaturnColumn(description = "打样规格")
  @ApiModelProperty(value = "打样规格，斤/尾", name = "specs")
  private Double specs;

  @Column(name = "exist_count", columnDefinition = "int(10) comment'本次存塘量/尾'")
  @SaturnColumn(description = "本次存塘量")
  @ApiModelProperty(value = "本次存塘量/尾", name = "existCount")
  private Double existCount;

  @Column(name = "exist_weight", columnDefinition = "double(10, 2) comment'本次存塘量/斤'")
  @SaturnColumn(description = "测产存塘量")
  @ApiModelProperty(value = "测产存塘量/斤", name = "existWeight")
  private Double existWeight;

  @Column(name = "predict_exist_weight", columnDefinition = "double(10, 2) comment'预估存塘量/斤'")
  @SaturnColumn(description = "预估存塘量")
  @ApiModelProperty(value = "预估存塘量/斤", name = "predictExistWeight")
  private Double predictExistWeight;

  @Column(name = "note", columnDefinition = "varchar(1000) comment'备注'")
  @SaturnColumn(description = "备注")
  @ApiModelProperty(value = "备注", name = "note")
  private String note;

  @Column(name = "picture", columnDefinition = "varchar(10000) comment'图片'")
  @SaturnColumn(description = "图片")
  @ApiModelProperty(value = "图片地址", name = "picture")
  private String picture;

  @Column(name = "the_push_count", columnDefinition = "double(10,2) comment'本期投鱼量'")
  @SaturnColumn(description = "本期投鱼量")
  @ApiModelProperty(value = "本期投鱼量", name = "thePushCount")
  private Double thePushCount;

  @Column(name = "the_out_count", columnDefinition = "double(10,2) comment'本期出鱼量'")
  @SaturnColumn(description = "本期出鱼量")
  @ApiModelProperty(value = "本期出鱼量", name = "theOutCount")
  private Double theOutCount;

  @Column(name = "the_damage_count", columnDefinition = "double(10,2) comment'本期损鱼量'")
  @SaturnColumn(description = "本期损鱼量")
  @ApiModelProperty(value = "本期损鱼量", name = "theDamageCount")
  private Double theDamageCount;

  @Column(name = "the_anatomy_count", columnDefinition = "double(10,2) comment'本期剖鱼量'")
  @SaturnColumn(description = "本期剖鱼量")
  @ApiModelProperty(value = "本期剖鱼量", name = "theAnatomyCount")
  private Double theAnatomyCount;

  @JoinColumn(name = "last_yield_id", columnDefinition = "varchar(255) comment'上次打样测产id'")
  @SaturnColumn(description = "上次打样测产")
  @ManyToOne(fetch = FetchType.LAZY)
  @ApiModelProperty(name = "lastYield", value = "上次打样测产")
  private ProofYieldEntity lastYield;

  @JoinColumn(name = "work_order_id", columnDefinition = "varchar(255) comment'工单id'")
  @SaturnColumn(description = "工单")
  @ApiModelProperty(value = "工单", name = "workOrder")
  @ManyToOne(fetch = FetchType.LAZY)
  private WorkOrderEntity workOrder;

  @ApiModelProperty(value = "基地")
  @JoinColumn(name = "base_id", columnDefinition = "varchar(255) comment '基地id'")
  @SaturnColumn(description = "基地")
  @ManyToOne(fetch = FetchType.LAZY)
  private TwBaseEntity base;

  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public Date getProofTime() {
    return proofTime;
  }

  public void setProofTime(Date proofTime) {
    this.proofTime = proofTime;
  }

  public FishTypeEntity getFishType() {
    return fishType;
  }

  public void setFishType(FishTypeEntity fishType) {
    this.fishType = fishType;
  }

  public String getSupervisor() {
    return supervisor;
  }

  public void setSupervisor(String supervisor) {
    this.supervisor = supervisor;
  }

  public UserEntity getGroupLeader() {
    return groupLeader;
  }

  public void setGroupLeader(UserEntity groupLeader) {
    this.groupLeader = groupLeader;
  }

  public Double getDeviationRate() {
    return deviationRate;
  }

  public void setDeviationRate(Double deviationRate) {
    this.deviationRate = deviationRate;
  }

  public Set<ProofYieldItemEntity> getProofYieldItems() {
    return proofYieldItems;
  }

  public void setProofYieldItems(Set<ProofYieldItemEntity> proofYieldItems) {
    this.proofYieldItems = proofYieldItems;
  }

  public Double getSpecs() {
    return specs;
  }

  public void setSpecs(Double specs) {
    this.specs = specs;
  }

  public Double getExistCount() {
    return existCount;
  }

  public void setExistCount(Double existCount) {
    this.existCount = existCount;
  }

  public Double getExistWeight() {
    return existWeight;
  }

  public void setExistWeight(Double existWeight) {
    this.existWeight = existWeight;
  }

  public Double getPredictExistWeight() {
    return predictExistWeight;
  }

  public void setPredictExistWeight(Double predictExistWeight) {
    this.predictExistWeight = predictExistWeight;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public Double getThePushCount() {
    return thePushCount;
  }

  public void setThePushCount(Double thePushCount) {
    this.thePushCount = thePushCount;
  }

  public Double getTheOutCount() {
    return theOutCount;
  }

  public void setTheOutCount(Double theOutCount) {
    this.theOutCount = theOutCount;
  }

  public Double getTheDamageCount() {
    return theDamageCount;
  }

  public void setTheDamageCount(Double theDamageCount) {
    this.theDamageCount = theDamageCount;
  }

  public Double getTheAnatomyCount() {
    return theAnatomyCount;
  }

  public void setTheAnatomyCount(Double theAnatomyCount) {
    this.theAnatomyCount = theAnatomyCount;
  }

  public ProofYieldEntity getLastYield() {
    return lastYield;
  }

  public void setLastYield(ProofYieldEntity lastYield) {
    this.lastYield = lastYield;
  }

  public WorkOrderEntity getWorkOrder() {
    return workOrder;
  }

  public void setWorkOrder(WorkOrderEntity workOrder) {
    this.workOrder = workOrder;
  }
}
