package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.core.entity.UuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author 陈荣
 * @date 2019/11/14 9:13
 */
@Entity
@Table(name = "tw_proof_yield_item")
@ApiModel(description = "打样估产明细", value = "ProofYieldItemEntity")
@SaturnDomain("process")
public class ProofYieldItemEntity extends UuidEntity {

  private static final long serialVersionUID = 2493739277209965680L;

  @Column(name = "count", columnDefinition = "int(10) comment'数量'")
  @SaturnColumn(description = "数量")
  @ApiModelProperty(value = "数量", name = "count")
  private Double count;

  @Column(name = "weight", columnDefinition = "double(10, 2) comment'重量'")
  @SaturnColumn(description = "重量")
  @ApiModelProperty(value = "重量", name = "weight")
  private Double weight;

  @JoinColumn(name = "proofYield_id", columnDefinition = "varchar(255) comment'打样估产id'")
  @SaturnColumn(description = "打样估产")
  @ManyToOne(fetch = FetchType.LAZY)
  @ApiModelProperty(value = "打样估产", name = "proofYield")
  private ProofYieldEntity proofYield;

  public Double getCount() {
    return count;
  }

  public void setCount(Double count) {
    this.count = count;
  }

  public Double getWeight() {
    return weight;
  }

  public void setWeight(Double weight) {
    this.weight = weight;
  }

  public ProofYieldEntity getProofYield() {
    return proofYield;
  }

  public void setProofYield(ProofYieldEntity proofYield) {
    this.proofYield = proofYield;
  }
}
