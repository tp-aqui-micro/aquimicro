package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.MatterEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.StoreEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author 陈荣
 * @date 2019/11/13 16:28
 */
@Entity
@Table(name = "tw_push_feed")
@SaturnDomain(value = "process")
@ApiModel(description = "饲料投喂", value = "PushFeedEntity")
public class PushFeedEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = 4974437262142432892L;

  @ApiModelProperty(value = "编码", name = "code")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(255) comment'编码'")
  @SaturnColumn(description = "编码")
  private String code;

  @JoinColumn(name = "pond_id", nullable = false, columnDefinition = "varchar(255) comment'塘口id'")
  @SaturnColumn(description = "塘口")
  @ApiModelProperty(value = "塘口", name = "pond", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private PondEntity pond;

  @JoinColumn(name = "store_id", columnDefinition = "varchar(255) comment'仓库id'")
  @SaturnColumn(description = "仓库")
  @ApiModelProperty(value = "仓库", name = "store")
  @ManyToOne(fetch = FetchType.LAZY)
  private StoreEntity store;

  @JoinColumn(name = "feed_id", columnDefinition = "varchar(255) comment'饲料'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "饲料")
  @ApiModelProperty(value = "饲料", name = "feed")
  private MatterEntity feed;

  @Column(name = "food_state", nullable = false, columnDefinition = "int(11) comment'吃食情况，1正常，0不正常'")
  @SaturnColumn(description = "吃食情况")
  @ApiModelProperty(value = "吃食情况，1正常，0不正常", name = "foodState", required = true)
  private int foodState;

  @Column(name = "specs", columnDefinition = "varchar(100) comment'规格，如：60斤/袋'")
  @SaturnColumn(description = "规格")
  @ApiModelProperty(value = "规格，如：60斤/袋", name = "specs")
  private String specs;

  @Column(name = "push_weight", columnDefinition = "double(10, 6) comment'投喂量/斤'")
  @SaturnColumn(description = "投喂量")
  @ApiModelProperty(value = "投喂量/斤", name = "pushWeight")
  private Double pushWeight;

  @Column(name = "unit", columnDefinition = "varchar(100) comment'单位'")
  @SaturnColumn(description = "单位")
  @ApiModelProperty(name = "unit", value = "单位")
  private String unit;

  @Column(name = "count", columnDefinition = "double(10,2) comment'数量'")
  @SaturnColumn(description = "数量")
  @ApiModelProperty(name = "count", value = "数量")
  private Double count;

  @Column(name = "push_time", columnDefinition = "datetime default current_timestamp comment'投喂时间'")
  @SaturnColumn(description = "投喂时间")
  @ApiModelProperty(value = "投喂时间", name = "pushTime")
  private Date pushTime;

  @Column(name = "note", columnDefinition = "varchar(1000) comment'备注'")
  @SaturnColumn(description = "备注")
  @ApiModelProperty(value = "备注", name = "note")
  private String note;

  @Column(name = "picture", columnDefinition = "varchar(10000) comment'图片'")
  @SaturnColumn(description = "图片")
  @ApiModelProperty(value = "图片地址", name = "picture")
  private String picture;

  @JoinColumn(name = "work_order_id", columnDefinition = "varchar(255) comment'工单id'")
  @SaturnColumn(description = "工单")
  @ApiModelProperty(value = "工单", name = "workOrder")
  @ManyToOne(fetch = FetchType.LAZY)
  private WorkOrderEntity workOrder;

  @ApiModelProperty(value = "基地")
  @JoinColumn(name = "base_id", columnDefinition = "varchar(255) comment '基地id'")
  @SaturnColumn(description = "基地")
  @ManyToOne(fetch = FetchType.LAZY)
  private TwBaseEntity base;

  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public MatterEntity getFeed() {
    return feed;
  }

  public void setFeed(MatterEntity feed) {
    this.feed = feed;
  }

  public int getFoodState() {
    return foodState;
  }

  public void setFoodState(int foodState) {
    this.foodState = foodState;
  }

  public String getSpecs() {
    return specs;
  }

  public void setSpecs(String specs) {
    this.specs = specs;
  }

  public Double getPushWeight() {
    return pushWeight;
  }

  public void setPushWeight(Double pushWeight) {
    this.pushWeight = pushWeight;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public Double getCount() {
    return count;
  }

  public void setCount(Double count) {
    this.count = count;
  }

  public Date getPushTime() {
    return pushTime;
  }

  public void setPushTime(Date pushTime) {
    this.pushTime = pushTime;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public WorkOrderEntity getWorkOrder() {
    return workOrder;
  }

  public void setWorkOrder(WorkOrderEntity workOrder) {
    this.workOrder = workOrder;
  }

  public StoreEntity getStore() {
    return store;
  }

  public void setStore(StoreEntity store) {
    this.store = store;
  }
}
