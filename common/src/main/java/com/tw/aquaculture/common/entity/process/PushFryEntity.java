package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * 苗种投苗
 * @author 陈荣
 * @date 2019/11/13 15:07
 */
@Entity
@Table(name = "tw_push_fry")
@SaturnDomain(value = "process")
@ApiModel(description = "苗种投苗")
public class PushFryEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = -1122937788019998550L;

  @ApiModelProperty(value = "编码", name = "code")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment'code'")
  @SaturnColumn(description = "编码")
  private String code;

  @ApiModelProperty(value = "塘口id", name = "pond", required = true)
  @JoinColumn(name = "pond_id", nullable = false, columnDefinition = "varchar(255) comment'塘口id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "塘口")
  private PondEntity pond;

  @JoinColumn(name = "fish_type", nullable = false, columnDefinition = "varchar(64) comment'鱼种'")
  @SaturnColumn(description = "鱼种")
  @ApiModelProperty(name = "fishType", value = "鱼种", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private FishTypeEntity fishType;

  @ApiModelProperty(value = "规格，如：0.2斤/条", name = "specs")
  @Column(name = "specs", columnDefinition = "varchar(100) comment'规格，如：0.2斤/条'")
  @SaturnColumn(description = "规格")
  private String specs;

  @ApiModelProperty(value = "数量/尾", name = "count")
  @Column(name = "count", columnDefinition = "double(10, 2) comment'数量/尾'")
  @SaturnColumn(description = "数量/尾")
  private Double count;

  @ApiModelProperty(value = "总量/斤", name = "summation")
  @Column(name = "summation", columnDefinition = "double(10, 2) comment'总量/斤'")
  @SaturnColumn(description = "总量/斤")
  private Double summation;

  @ApiModelProperty(value = "投苗时间", name = "pushTime")
  @Column(name = "push_time", columnDefinition = "datetime comment'投苗时间'")
  @SaturnColumn(description = "时间")
  private Date pushTime;

  @ApiModelProperty(value = "单价", name = "price")
  @Column(name = "price", columnDefinition = "double(10, 2) comment'单价'")
  @SaturnColumn(description = "单价")
  private Double price;

  @ApiModelProperty(value = "金额", name = "amount")
  @Column(name = "amount", columnDefinition = "double(10, 2) comment'金额'")
  @SaturnColumn(description = "金额")
  private Double amount;

  @ApiModelProperty(value = "运费", name = "transportFee")
  @Column(name = "transport_fee", columnDefinition = "double(10, 2) comment'运费'")
  @SaturnColumn(description = "运费")
  private Double transportFee;

  @ApiModelProperty(value = "承运方", name = "shipper")
  @Column(name = "shipper", columnDefinition = "varchar(100) comment'承运方'")
  @SaturnColumn(description = "承运方")
  private String shipper;

  @ApiModelProperty(value = "供应市场", name = "supplyMarket")
  @Column(name = "supply_market", columnDefinition = "varchar(100) comment'供应市场'")
  @SaturnColumn(description = "供应市场")
  private String supplyMarket;

  @ApiModelProperty(value = "供应客户", name = "supplyCustomer")
  @Column(name = "supply_customer", columnDefinition = "varchar(100) comment'供应客户'")
  @SaturnColumn(description = "供应客户")
  private String supplyCustomer;

  @ApiModelProperty(value = "客户电话")
  @Column(name = "customer_phone", columnDefinition = "varchar(11) comment'客户电话'")
  @SaturnColumn(description = "客户电话")
  private String customerPhone;

  @ApiModelProperty(value = "备注", name = "note")
  @Column(name = "note", columnDefinition = "varchar(1000) comment'备注'")
  @SaturnColumn(description = "备注")
  private String note;

  @ApiModelProperty(value = "图片", name = "picture")
  @Column(name = "picture", columnDefinition = "varchar(10000) comment'图片'")
  @SaturnColumn(description = "图片")
  private String picture;

  @JoinColumn(name = "work_order_id", columnDefinition = "varchar(255) comment'工单id'")
  @SaturnColumn(description = "工单")
  @ApiModelProperty(value = "工单", name = "workOrder")
  @ManyToOne(fetch = FetchType.LAZY)
  private WorkOrderEntity workOrder;

  @ApiModelProperty(value = "基地")
  @JoinColumn(name = "base_id", columnDefinition = "varchar(255) comment '基地id'")
  @SaturnColumn(description = "基地")
  @ManyToOne(fetch = FetchType.LAZY)
  private TwBaseEntity base;

  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public FishTypeEntity getFishType() {
    return fishType;
  }

  public void setFishType(FishTypeEntity fishType) {
    this.fishType = fishType;
  }

  public String getSpecs() {
    return specs;
  }

  public void setSpecs(String specs) {
    this.specs = specs;
  }

  public Double getCount() {
    return count;
  }

  public void setCount(Double count) {
    this.count = count;
  }

  public Double getSummation() {
    return summation;
  }

  public void setSummation(Double summation) {
    this.summation = summation;
  }

  public Date getPushTime() {
    return pushTime;
  }

  public void setPushTime(Date pushTime) {
    this.pushTime = pushTime;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Double getTransportFee() {
    return transportFee;
  }

  public void setTransportFee(Double transportFee) {
    this.transportFee = transportFee;
  }

  public String getShipper() {
    return shipper;
  }

  public void setShipper(String shipper) {
    this.shipper = shipper;
  }

  public String getSupplyMarket() {
    return supplyMarket;
  }

  public void setSupplyMarket(String supplyMarket) {
    this.supplyMarket = supplyMarket;
  }

  public String getSupplyCustomer() {
    return supplyCustomer;
  }

  public void setSupplyCustomer(String supplyCustomer) {
    this.supplyCustomer = supplyCustomer;
  }

  public String getCustomerPhone() {
    return customerPhone;
  }

  public void setCustomerPhone(String customerPhone) {
    this.customerPhone = customerPhone;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public WorkOrderEntity getWorkOrder() {
    return workOrder;
  }

  public void setWorkOrder(WorkOrderEntity workOrder) {
    this.workOrder = workOrder;
  }
}
