package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.core.entity.UuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author 陈荣
 * @date 2019/11/14 10:19
 */
@Entity
@Table(name = "tw_remain_info")
@ApiModel(description = "存塘信息", value = "RemainInfoEntity")
@SaturnDomain("process")
public class RemainInfoEntity extends UuidEntity {

  private static final long serialVersionUID = 707434636201004482L;

  @JoinColumn(name = "fish_type", nullable = false, columnDefinition = "varchar(64) comment'鱼种'")
  @SaturnColumn(description = "鱼种")
  @ApiModelProperty(name = "fishType", value = "鱼种", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  private FishTypeEntity fishType;

  @Column(name = "push_fry_time", columnDefinition = "datetime default current_timestamp comment'投苗时间'")
  @SaturnColumn(description = "投苗时间")
  @ApiModelProperty(value = "投苗时间", name = "pushFryTime")
  private Date pushFryTime;

  @Column(name = "low_price", columnDefinition = "double(10, 2) comment'低价'")
  @SaturnColumn(description = "低价")
  @ApiModelProperty(value = "低价", name = "lowPrice")
  private String lowPrice;

  @Column(name = "up_price", columnDefinition = "double(10, 2) comment'高价'")
  @SaturnColumn(description = "高价")
  @ApiModelProperty(value = "高价", name = "upPrice")
  private String upPrice;

  @Column(name = "specs", columnDefinition = "varchar(100) comment'规格'")
  @SaturnColumn(description = "规格")
  @ApiModelProperty(value = "规格", name = "specs")
  private String specs;

  @Column(name = "predict_out_time", columnDefinition = "datetime comment'预估出鱼时间'")
  @SaturnColumn(description = "预估出鱼时间")
  @ApiModelProperty(value = "预估出鱼时间", name = "predictOutTime")
  private Date predictOutTime;

  @Column(name = "weight", columnDefinition = "double(10, 2) comment'数量，实际为重量/斤'")
  @SaturnColumn(description = "数量")
  @ApiModelProperty(value = "数量，实际为重量/斤", name = "weight")
  private String weight;

  @Column(name = "out_idea", columnDefinition = "varchar(1000) comment'出鱼意见'")
  @SaturnColumn(description = "出鱼意见")
  @ApiModelProperty(value = "出鱼意见", name = "outIdea")
  private String outIdea;

  @JoinColumn(name = "out_fish_apply_id", columnDefinition = "varchar(255) comment'出鱼申请id'")
  @SaturnColumn(description = "出鱼申请")
  @ApiModelProperty(value = "出鱼申请", name = "outFishApply")
  @ManyToOne(fetch = FetchType.LAZY)
  private OutFishApplyEntity outFishApply;

  public FishTypeEntity getFishType() {
    return fishType;
  }

  public void setFishType(FishTypeEntity fishType) {
    this.fishType = fishType;
  }

  public Date getPushFryTime() {
    return pushFryTime;
  }

  public void setPushFryTime(Date pushFryTime) {
    this.pushFryTime = pushFryTime;
  }

  public String getLowPrice() {
    return lowPrice;
  }

  public void setLowPrice(String lowPrice) {
    this.lowPrice = lowPrice;
  }

  public String getUpPrice() {
    return upPrice;
  }

  public void setUpPrice(String upPrice) {
    this.upPrice = upPrice;
  }

  public Date getPredictOutTime() {
    return predictOutTime;
  }

  public void setPredictOutTime(Date predictOutTime) {
    this.predictOutTime = predictOutTime;
  }

  public String getWeight() {
    return weight;
  }

  public void setWeight(String weight) {
    this.weight = weight;
  }

  public String getOutIdea() {
    return outIdea;
  }

  public void setOutIdea(String outIdea) {
    this.outIdea = outIdea;
  }

  public OutFishApplyEntity getOutFishApply() {
    return outFishApply;
  }

  public void setOutFishApply(OutFishApplyEntity outFishApply) {
    this.outFishApply = outFishApply;
  }

  public String getSpecs() {
    return specs;
  }

  public void setSpecs(String specs) {
    this.specs = specs;
  }
}
