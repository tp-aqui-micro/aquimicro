package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.MatterEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.StoreEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.Set;

/**
 * @author 陈荣
 * @date 2019/11/13 16:58
 */
@Entity
@Table(name = "tw_safeguard")
@SaturnDomain(value = "process")
@ApiModel(description = "动保", value = "SafeguardEntity")
public class SafeguardEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = 1477127937708144499L;

  @ApiModelProperty(value = "编码", name = "code")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment'编码'")
  @SaturnColumn(description = "编码")
  private String code;

  @ApiModelProperty(value = "塘口", name = "pond", required = true)
  @JoinColumn(name = "pond_id", nullable = false, columnDefinition = "varchar(255) comment'塘口id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "塘口")
  private PondEntity pond;

  @Column(name = "water_depth", columnDefinition = "Double(10,2) comment'水深,如：25米'")
  @SaturnColumn(description = "水深")
  @ApiModelProperty(value = "水深，如:25米", name = "waterDepth")
  private Double waterDepth;

  @Column(name = "medicine_type", columnDefinition = "varchar(100) comment'用药类型'")
  @SaturnColumn(description = "用药类型")
  @ApiModelProperty(value = "用药类型", name = "medicineType")
  private String medicineType;

  @Column(name = "reason", columnDefinition = "varchar(100) comment'用药原因'")
  @SaturnColumn(description = "用药原因")
  @ApiModelProperty(value = "用药原因", name = "reason")
  private String reason;

  @JoinColumn(name = "medicine_id", columnDefinition = "varchar(255) comment'药品id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "药品")
  @ApiModelProperty(name = "medicine", value = "药品")
  private MatterEntity medicine;

  @Column(name = "medicine_time", columnDefinition = "datetime default current_timestamp comment'用药时间'")
  @SaturnColumn(description = "用药时间")
  @ApiModelProperty(value = "用药时间", name = "medicineTime")
  private Date medicineTime;

  @Column(name = "medicine_count", columnDefinition = "double(10, 2) comment'用药量'")
  @SaturnColumn(description = "用药量")
  @ApiModelProperty(value = "用药量", name = "medicineCount")
  private Double medicineCount;

  @Column(name = "unit", columnDefinition = "varchar(255) comment'单位'")
  @SaturnColumn(description = "单位")
  @ApiModelProperty(value = "单位", name = "unit")
  private String unit;

  @Column(name = "safe_state", nullable = false, columnDefinition = "int(2) comment'发病状态，1：发病，0：正常'")
  @SaturnColumn(description = "发病状态")
  @ApiModelProperty(value = "发病状态", name = "safeState")
  private Integer safeState;

  @Column(name = "note", columnDefinition = "varchar(1000) comment'备注'")
  @SaturnColumn(description = "备注")
  @ApiModelProperty(value = "备注", name = "note")
  private String note;

  @Column(name = "picture", columnDefinition = "varchar(10000) comment'图片'")
  @SaturnColumn(description = "图片")
  @ApiModelProperty(value = "图片", name = "picture")
  private String picture;

  @JoinColumn(name = "work_order_id", columnDefinition = "varchar(255) comment'工单id'")
  @SaturnColumn(description = "工单")
  @ApiModelProperty(value = "工单", name = "workOrder")
  @ManyToOne(fetch = FetchType.LAZY)
  private WorkOrderEntity workOrder;

  @SaturnColumn(description = "用药反馈")
  @ApiModelProperty(name = "medicineFeedBacks", value = "用药反馈")
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "safeguard")
  private Set<MedicineFeedBackEntity> medicineFeedBacks;

  @ApiModelProperty(value = "基地")
  @JoinColumn(name = "base_id", columnDefinition = "varchar(255) comment '基地id'")
  @SaturnColumn(description = "基地")
  @ManyToOne(fetch = FetchType.LAZY)
  private TwBaseEntity base;

  @JoinColumn(name = "store_id", columnDefinition = "varchar(255) comment'仓库id'")
  @SaturnColumn(description = "仓库")
  @ApiModelProperty(value = "仓库", name = "store")
  @ManyToOne(fetch = FetchType.LAZY)
  private StoreEntity store;

  public StoreEntity getStore() {
    return store;
  }

  public void setStore(StoreEntity store) {
    this.store = store;
  }

  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public Double getWaterDepth() {
    return waterDepth;
  }

  public Integer getSafeState() {
    return safeState;
  }

  public void setSafeState(Integer safeState) {
    this.safeState = safeState;
  }

  public void setWaterDepth(Double waterDepth) {
    this.waterDepth = waterDepth;
  }

  public String getMedicineType() {
    return medicineType;
  }

  public void setMedicineType(String medicineType) {
    this.medicineType = medicineType;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public MatterEntity getMedicine() {
    return medicine;
  }

  public void setMedicine(MatterEntity medicine) {
    this.medicine = medicine;
  }

  public Date getMedicineTime() {
    return medicineTime;
  }

  public void setMedicineTime(Date medicineTime) {
    this.medicineTime = medicineTime;
  }

  public Double getMedicineCount() {
    return medicineCount;
  }

  public void setMedicineCount(Double medicineCount) {
    this.medicineCount = medicineCount;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public WorkOrderEntity getWorkOrder() {
    return workOrder;
  }

  public void setWorkOrder(WorkOrderEntity workOrder) {
    this.workOrder = workOrder;
  }

  public Set<MedicineFeedBackEntity> getMedicineFeedBacks() {
    return medicineFeedBacks;
  }

  public void setMedicineFeedBacks(Set<MedicineFeedBackEntity> medicineFeedBacks) {
    this.medicineFeedBacks = medicineFeedBacks;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }
}
