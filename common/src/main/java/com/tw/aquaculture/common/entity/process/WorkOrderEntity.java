package com.tw.aquaculture.common.entity.process;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * 工单
 * @author 陈荣
 * @date 2019/11/13 14:54
 */
@Entity
@Table(name = "tw_work_order")
@SaturnDomain(value = "process")
@ApiModel(value = "WorkOrderEntity", description = "工单")
public class WorkOrderEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = 2266791825409554420L;

  @Column(name = "code", nullable = false, columnDefinition = "varchar(64) comment'工单编号'")
  @SaturnColumn(description = "工单编号")
  @ApiModelProperty(name = "code", value = "工单编号", required = true)
  private String code;

  @JoinColumn(name = "pond_id", nullable = false, columnDefinition = "varchar(255) comment'塘口id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "塘口")
  @ApiModelProperty(name = "pond", value = "塘口", required = true)
  private PondEntity pond;

  @Column(name = "enable_state", nullable = false, columnDefinition = "int(11) default 1 comment'生效状态，1：生效，0失效'")
  @SaturnColumn(description = "生效状态")
  @ApiModelProperty(name = "enableState", value = "生效状态", required = true)
  private Integer enableState;

  @Column(name = "finish_time", columnDefinition = "datetime comment'完成时间'")
  @SaturnColumn(description = "完成时间")
  @ApiModelProperty(name = "finishTime", value = "完成时间")
  private Date finishTime;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public PondEntity getPond() {
    return pond;
  }

  public void setPond(PondEntity pond) {
    this.pond = pond;
  }

  public Integer getEnableState() {
    return enableState;
  }

  public void setEnableState(Integer enableState) {
    this.enableState = enableState;
  }

  public Date getFinishTime() {
    return finishTime;
  }

  public void setFinishTime(Date finishTime) {
    this.finishTime = finishTime;
  }
}
