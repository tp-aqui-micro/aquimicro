package com.tw.aquaculture.common.entity.servicecentre;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.Set;

/**
 * @author 陈荣
 * @date 2019/11/26 13:34
 */
@SaturnDomain(value = "servicecentre")
@Entity
@Table(name = "tw_notice")
@ApiModel(value = "NoticeEntity", description = "公告")
public class NoticeEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = 32499688528191847L;

  @Column(name = "code", nullable = false, columnDefinition = "varchar(255) comment'编码'")
  @SaturnColumn(description = "编码")
  @ApiModelProperty(name = "code", value = "编码", required = true)
  private String code;

  @Column(name = "title", nullable = false, columnDefinition = "varchar(255) comment'标题'")
  @SaturnColumn(description = "标题")
  @ApiModelProperty(value = "标题", name = "title", required = true)
  private String title;

  @Column(name = "content", nullable = false, columnDefinition = "varchar(10000) comment'正文'")
  @SaturnColumn(description = "正文")
  @ApiModelProperty(value = "正文", name = "content", required = true)
  private String content;

  @Column(name = "start_time", nullable = false, columnDefinition = "datetime comment'开始时间'")
  @SaturnColumn(description = "开始时间")
  @ApiModelProperty(value = "开始时间", name = "startTime", required = true)
  private Date startTime;

  @Column(name = "end_time", nullable = false, columnDefinition = "datetime comment'结束时间'")
  @SaturnColumn(description = "结束时间")
  @ApiModelProperty(value = "结束时间", name = "endTime", required = true)
  private Date endTime;

  @Column(name = "enable_state", nullable = false, columnDefinition = "int(11) comment'生效状态（1生效，0失效）'")
  @SaturnColumn(description = "生效状态")
  @ApiModelProperty(value = "生效状态", name = "enableState", required = true)
  private int enableState;

  @SaturnColumn(description = "组织")
  @OneToMany(mappedBy = "notice", fetch = FetchType.LAZY)
  @ApiModelProperty(name = "orgs", value = "组织")
  private Set<NoticeOrgEntity> orgs;

  @SaturnColumn(description = "用户")
  @OneToMany(mappedBy = "notice", fetch = FetchType.LAZY)
  @ApiModelProperty(name = "users", value = "用户")
  private Set<NoticeUserEntity> users;

  @Column(name = "enclosure", columnDefinition = "varchar(1000) comment'附件'")
  @SaturnColumn(description = "附件")
  @ApiModelProperty(value = "附件", name = "title")
  private String enclosure;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public int getEnableState() {
    return enableState;
  }

  public void setEnableState(int enableState) {
    this.enableState = enableState;
  }

  public Set<NoticeOrgEntity> getOrgs() {
    return orgs;
  }

  public void setOrgs(Set<NoticeOrgEntity> orgs) {
    this.orgs = orgs;
  }

  public Set<NoticeUserEntity> getUsers() {
    return users;
  }

  public void setUsers(Set<NoticeUserEntity> users) {
    this.users = users;
  }

  public String getEnclosure() {
    return enclosure;
  }

  public void setEnclosure(String enclosure) {
    this.enclosure = enclosure;
  }
}
