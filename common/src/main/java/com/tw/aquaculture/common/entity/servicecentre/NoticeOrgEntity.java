package com.tw.aquaculture.common.entity.servicecentre;

import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.core.entity.UuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author 陈荣
 * @date 2019/11/26 13:45
 */
@ApiModel(value = "NoticeOrgEntity", description = "公告面向的组织")
@SaturnDomain(value = "servicecentre")
@Entity
@Table(name = "tw_notice_org")
public class NoticeOrgEntity extends UuidEntity {

  private static final long serialVersionUID = -4263660764301430526L;

  @JoinColumn(name = "org_id", columnDefinition = "varchar(255) comment'组织id'")
  @SaturnColumn(description = "组织")
  @ApiModelProperty(name = "org", value = "组织")
  @ManyToOne(fetch = FetchType.LAZY)
  private OrganizationEntity org;

  @JoinColumn(name = "notice_id", columnDefinition = "varchar(255) comment'公告'")
  @SaturnColumn(description = "公告")
  @ApiModelProperty(name = "notice", value = "公告")
  @ManyToOne(fetch = FetchType.LAZY)
  private NoticeEntity notice;

  public OrganizationEntity getOrg() {
    return org;
  }

  public void setOrg(OrganizationEntity org) {
    this.org = org;
  }

  public NoticeEntity getNotice() {
    return notice;
  }

  public void setNotice(NoticeEntity notice) {
    this.notice = notice;
  }
}
