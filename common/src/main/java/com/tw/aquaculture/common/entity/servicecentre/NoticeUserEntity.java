package com.tw.aquaculture.common.entity.servicecentre;

import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.core.entity.UuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author 陈荣
 * @date 2019/11/26 13:52
 */
@Entity
@Table(name = "tw_notice_user")
@SaturnDomain(value ="servicecentre")
@ApiModel(value = "NoticeUserEntity", description = "公告面向的用户")
public class NoticeUserEntity extends UuidEntity {

  private static final long serialVersionUID = -7937800212681256537L;

  @JoinColumn(name = "user_id", columnDefinition = "varchar(255) comment'用户id'")
  @SaturnColumn(description = "用户")
  @ApiModelProperty(name = "user", value = "用户")
  @ManyToOne(fetch = FetchType.LAZY)
  private UserEntity user;

  @JoinColumn(name = "notice_id", columnDefinition = "varchar(255) comment'公告'")
  @SaturnColumn(description = "公告")
  @ApiModelProperty(name = "notice", value = "公告")
  @ManyToOne(fetch = FetchType.LAZY)
  private NoticeEntity notice;

  public UserEntity getUser() {
    return user;
  }

  public void setUser(UserEntity user) {
    this.user = user;
  }

  public NoticeEntity getNotice() {
    return notice;
  }

  public void setNotice(NoticeEntity notice) {
    this.notice = notice;
  }
}
