package com.tw.aquaculture.common.entity.store;

import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.MatterEntity;
import com.tw.aquaculture.common.entity.base.StoreEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * 入库管理
 *
 * @author bo shi
 */
@ApiModel(value = "InStorageEntity", description = "入库管理")
@Entity
@Table(name = "tw_in_storage")
@SaturnDomain(value = "store")
public class InStorageEntity extends BaseFieldsEntity {

  /**
   * 
   */
  private static final long serialVersionUID = -4896573821256441095L;

  @ApiModelProperty(name = "code", value = "编码", required = true)
  @SaturnColumn(description = "编码")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(255) comment '仓库编码'")
  private String code;

  @ApiModelProperty(name = "twBase", value = "基地", required = true)
  @JoinColumn(name = "tw_base_id", nullable = false, columnDefinition = "varchar(255) comment'基地id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "基地id")
  private TwBaseEntity twBase;

  @ApiModelProperty(name = "store", value = "仓库id", required = true)
  @JoinColumn(name = "store_id", nullable = false, columnDefinition = "varchar(255) comment'仓库id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "仓库id")
  private StoreEntity store;

  @ApiModelProperty(name = "category", required = true, value = "品类(字典表编码)")
  @Column(name = "category", nullable = false, columnDefinition = "varchar(255) comment'品类(字典表编码)'")
  @SaturnColumn(description = "品类")
  private String category;

  @ApiModelProperty(name = "matter", required = true, value = "物料id")
  @JoinColumn(name = "matter_id", nullable = false, columnDefinition = "varchar(255) comment'物料id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "物料id")
  private MatterEntity matter;

  @ApiModelProperty(name = "spec", required = true, value = "规格")
  @Column(name = "spec", nullable = false, columnDefinition = "varchar(100) comment'规格，如：50kg/袋'")
  @SaturnColumn(description = "规格")
  private String spec;

  @ApiModelProperty(name = "unit", value = "单位")
  @Column(name = "unit", columnDefinition = "varchar(255) comment'单位'")
  @SaturnColumn(description = "单位")
  private String unit;

  @ApiModelProperty(name = "quantity", value = "数量", required = true)
  @SaturnColumn(description = "数量")
  @Column(name = "quantity", nullable = false, columnDefinition = "double(16,6) comment '数量'")
  private Double quantity;

  @ApiModelProperty(name = "inStorageDate", value = "入库时间", required = true)
  @Column(name = "in_storage_date", nullable = false, columnDefinition = "datetime COMMENT '入库时间'")
  @SaturnColumn(description = "入库时间")
  private Date inStorageDate;


  /** 创建人 */
  @ApiModelProperty(name = "createUser", value = "入库人", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "create_user", columnDefinition = "varchar(255) COMMENT '入库人'")
  @SaturnColumn(description = "入库人")
  private UserEntity createUser;


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public TwBaseEntity getTwBase() {
    return twBase;
  }

  public void setTwBase(TwBaseEntity twBase) {
    this.twBase = twBase;
  }

  public StoreEntity getStore() {
    return store;
  }

  public void setStore(StoreEntity store) {
    this.store = store;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public MatterEntity getMatter() {
    return matter;
  }

  public void setMatter(MatterEntity matter) {
    this.matter = matter;
  }

  public String getSpec() {
    return spec;
  }

  public void setSpec(String spec) {
    this.spec = spec;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public Double getQuantity() {
    return quantity;
  }

  public void setQuantity(Double quantity) {
    this.quantity = quantity;
  }

  public UserEntity getCreateUser() {
    return createUser;
  }

  public void setCreateUser(UserEntity createUser) {
    this.createUser = createUser;
  }

  public Date getInStorageDate() {
    return inStorageDate;
  }

  public void setInStorageDate(Date inStorageDate) {
    this.inStorageDate = inStorageDate;
  }
}
