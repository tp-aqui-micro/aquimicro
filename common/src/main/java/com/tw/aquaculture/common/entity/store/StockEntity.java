package com.tw.aquaculture.common.entity.store;

import com.bizunited.platform.core.entity.UuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.base.MatterEntity;
import com.tw.aquaculture.common.entity.base.StoreEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 库存
 * @author 陈荣
 * @date 2019/11/28 9:00
 */
@ApiModel(value = "StockEntity", description = "入库管理")
@Entity
@Table(name = "tw_stock")
@SaturnDomain(value = "store")
public class StockEntity extends UuidEntity {

  private static final long serialVersionUID = 4481698851872857209L;

  @JoinColumn(name = "base_id", nullable = false, columnDefinition = "varchar(255) comment'基地id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "基地")
  @ApiModelProperty(name = "base", value = "基地", required = true)
  private TwBaseEntity base;

  @JoinColumn(name = "store_id", nullable = false, columnDefinition = "varchar(255) comment'仓库id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "仓库")
  @ApiModelProperty(name = "store", value = "仓库", required = true)
  private StoreEntity store;

  @JoinColumn(name = "matter_id", nullable = false, columnDefinition = "varchar(255) comment'物料id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "物料")
  @ApiModelProperty(name = "matter", value = "物料", required = true)
  private MatterEntity matter;

  @Column(name = "specs", nullable = false, columnDefinition = "varchar(100) comment'规格'")
  @SaturnColumn(description = "规格")
  @ApiModelProperty(name = "specs", value = "规格", required = true)
  private String specs;

  @Column(name = "unit", columnDefinition = "varchar(100) comment'单位'")
  @SaturnColumn(description = "单位")
  @ApiModelProperty(name = "unit", value = "单位")
  private String unit;

  @Column(name = "storage", nullable = false, columnDefinition = "double(16, 6) comment'库存量'")
  @SaturnColumn(description = "库存量")
  @ApiModelProperty(name = "specs", value = "库存量", required = true)
  private Double storage;

  public TwBaseEntity getBase() {
    return base;
  }

  public void setBase(TwBaseEntity base) {
    this.base = base;
  }

  public StoreEntity getStore() {
    return store;
  }

  public void setStore(StoreEntity store) {
    this.store = store;
  }

  public MatterEntity getMatter() {
    return matter;
  }

  public void setMatter(MatterEntity matter) {
    this.matter = matter;
  }

  public String getSpecs() {
    return specs;
  }

  public void setSpecs(String specs) {
    this.specs = specs;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public Double getStorage() {
    return storage;
  }

  public void setStorage(Double storage) {
    this.storage = storage;
  }
}
