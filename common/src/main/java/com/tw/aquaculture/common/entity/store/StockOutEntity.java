package com.tw.aquaculture.common.entity.store;

import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.MatterEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.StoreEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * 出库管理
 *
 * @author bo shi
 */
@ApiModel(value = "StockOutEntity", description = "出库管理")
@Entity
@Table(name = "tw_stock_out")
@SaturnDomain(value = "store")
public class StockOutEntity extends BaseFieldsEntity {

  /**
   * 
   */
  private static final long serialVersionUID = -9009193700275415974L;

  @ApiModelProperty(name = "code", value = "编码", required = true)
  @SaturnColumn(description = "编码")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(255) comment '仓库编码'")
  private String code;

  @ApiModelProperty(name = "twBase", value = "基地", required = true)
  @JoinColumn(name = "tw_base_id", nullable = false, columnDefinition = "varchar(255) comment'基地id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "基地id")
  private TwBaseEntity twBase;

  @ApiModelProperty(name = "store", required = true, value = "仓库id")
  @JoinColumn(name = "store_id", nullable = false, columnDefinition = "varchar(255) comment'仓库id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "仓库id")
  private StoreEntity store;

  @ApiModelProperty(name = "category", required = true, value = "品类(字典表编码)")
  @Column(name = "category", nullable = false, columnDefinition = "varchar(255) comment'品类(字典表编码)'")
  @SaturnColumn(description = "品类")
  private String category;

  @ApiModelProperty(name = "matter", value = "物料id", required = true)
  @JoinColumn(name = "matter_id", nullable = false, columnDefinition = "varchar(255) comment'物料id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "物料id")
  private MatterEntity matter;

  @ApiModelProperty(name = "spec", value = "规格")
  @Column(name = "spec",columnDefinition = "varchar(100) comment'规格，如：50kg/袋'")
  @SaturnColumn(description = "规格")
  private String spec;

  @ApiModelProperty(name = "unit", value = "单位")
  @Column(name = "unit", columnDefinition = "varchar(255) comment'单位'")
  @SaturnColumn(description = "单位")
  private String unit;

  @ApiModelProperty(name = "pond", value = "塘口id")
  @JoinColumn(name = "pond_id", columnDefinition = "varchar(255) comment'塘口id'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "塘口id")
  private PondEntity pond;

  @ApiModelProperty(name = "quantity", value = "数量", required = true)
  @SaturnColumn(description = "数量")
  @Column(name = "quantity", nullable = false, columnDefinition = "double(16,6) comment '数量'")
  private Double quantity;

  @ApiModelProperty(name = "receiver", value = "领用人", required = true)
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "receiver", columnDefinition = "varchar(255) COMMENT '领用人'")
  @SaturnColumn(description = "领用人")
  private UserEntity receiver;

  @ApiModelProperty(name = "stockOutDate", required = true, value = "出库时间")
  @Column(name = "stock_out_date", nullable = false, columnDefinition = "datetime COMMENT '出库时间'")
  @SaturnColumn(description = "出库时间")
  private Date stockOutDate;


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public TwBaseEntity getTwBase() {
		return twBase;
	}

	public void setTwBase(TwBaseEntity twBase) {
		this.twBase = twBase;
	}

	public StoreEntity getStore() {
		return store;
	}

	public void setStore(StoreEntity store) {
		this.store = store;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public MatterEntity getMatter() {
		return matter;
	}

	public void setMatter(MatterEntity matter) {
		this.matter = matter;
	}

	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public PondEntity getPond() {
		return pond;
	}

	public void setPond(PondEntity pond) {
		this.pond = pond;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public UserEntity getReceiver() {
		return receiver;
	}

	public void setReceiver(UserEntity receiver) {
		this.receiver = receiver;
	}

	public Date getStockOutDate() {
		return stockOutDate;
	}

	public void setStockOutDate(Date stockOutDate) {
		this.stockOutDate = stockOutDate;
	}

}
