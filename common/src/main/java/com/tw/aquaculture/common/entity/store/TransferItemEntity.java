package com.tw.aquaculture.common.entity.store;

import com.bizunited.platform.core.entity.UuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.base.MatterEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author 陈荣
 * @date 2019/12/24 16:04
 */
@ApiModel(value = "TransferItemEntity", description = "转库明细")
@SaturnDomain(value = "store")
@Entity
@Table(name = "tw_transfer_item")
public class TransferItemEntity extends UuidEntity {

  private static final long serialVersionUID = 795639814570150864L;

  @ApiModelProperty(name = "matter", value = "物料", required = true)
  @SaturnColumn(description = "物料")
  @JoinColumn(name = "matter_id", columnDefinition = "varchar(255) comment'物料id'")
  @ManyToOne(fetch = FetchType.LAZY)
  private MatterEntity matter;

  @ApiModelProperty(name = "matterType", value = "物料品类")
  @SaturnColumn(description = "物料品类")
  @Column(name = "matter_type", columnDefinition = "varchar(255) comment'物料品类'")
  private String matterType;

  @ApiModelProperty(name = "count", value = "数量", required = true)
  @SaturnColumn(description = "数量")
  @Column(name = "count", nullable = false, columnDefinition = "double(10, 6) comment'数量'")
  private Double count;

  @ApiModelProperty(name = "unit", value = "单位", required = true)
  @SaturnColumn(description = "单位")
  @Column(name = "unit", nullable = false, columnDefinition = "varchar(255) comment'单位'")
  private String unit;

  @ApiModelProperty(name = "transferStore", value = "转库", required = true)
  @SaturnColumn(description = "转库")
  @JoinColumn(name = "transfer_store_id", nullable = false, columnDefinition = "varchar(255) comment'转库id'")
  @ManyToOne(fetch = FetchType.LAZY)
  private TransferStoreEntity transferStore;

  @Column(name = "matter_code")
  @SaturnColumn(description = "物料编码")
  private String matterCode;

  @Column(name = "matter_other_id")
  @SaturnColumn(description = "物料ID")
  private String matterOtherId;

  @Column(name = "matter_name")
  @SaturnColumn(description = "物料")
  private String matterName;

  public String getMatterCode() {
    return matterCode;
  }

  public void setMatterCode(String matterCode) {
    this.matterCode = matterCode;
  }

  public String getMatterOtherId() {
    return matterOtherId;
  }

  public void setMatterOtherId(String matterOtherId) {
    this.matterOtherId = matterOtherId;
  }

  public String getMatterName() {
    return matterName;
  }

  public void setMatterName(String matterName) {
    this.matterName = matterName;
  }

  public MatterEntity getMatter() {
    return matter;
  }

  public void setMatter(MatterEntity matter) {
    this.matter = matter;
  }

  public Double getCount() {
    return count;
  }

  public void setCount(Double count) {
    this.count = count;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public TransferStoreEntity getTransferStore() {
    return transferStore;
  }

  public void setTransferStore(TransferStoreEntity transferStore) {
    this.transferStore = transferStore;
  }

  public String getMatterType() {
    return matterType;
  }

  public void setMatterType(String matterType) {
    this.matterType = matterType;
  }
}
