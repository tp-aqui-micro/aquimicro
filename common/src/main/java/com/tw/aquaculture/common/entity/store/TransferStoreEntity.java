package com.tw.aquaculture.common.entity.store;

import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import com.tw.aquaculture.common.entity.BaseFieldsEntity;
import com.tw.aquaculture.common.entity.base.StoreEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Date;
import java.util.Set;

/**
 * @author 陈荣
 * @date 2019/12/24 15:42
 */
@ApiModel(value = "TransferStoreEntity", description = "转库")
@SaturnDomain(value = "store")
@Entity
@Table(name = "tw_transfer_store", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"code"})
})
public class TransferStoreEntity extends BaseFieldsEntity {

  private static final long serialVersionUID = 6115930220739345978L;

  @ApiModelProperty(name="code", value = "编码")
  @Column(name = "code", nullable = false, columnDefinition = "varchar(255) comment'编码'")
  @SaturnColumn(description = "编码")
  private String code;

  @ApiModelProperty(name="transferTime", required = true, value = "转库时间'")
  @Column(name = "transfer_time", nullable = false, columnDefinition = "datetime comment'转库时间'")
  @SaturnColumn(description = "转库时间")
  private Date transferTime;

  @ApiModelProperty(name="outStore", required = true, value = "转出仓库")
  @JoinColumn(name = "out_store_id", nullable = false, columnDefinition = "varchar(255) comment'转出仓库'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "转出仓库")
  private StoreEntity outStore;

  @ApiModelProperty(name="inStore", required = true, value = "转入仓库")
  @JoinColumn(name = "in_store_id", nullable = false, columnDefinition = "varchar(255) comment'转入仓库'")
  @ManyToOne(fetch = FetchType.LAZY)
  @SaturnColumn(description = "转入仓库")
  private StoreEntity inStore;

  @ApiModelProperty(name="transferItems", required = true, value = "出库明细")
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "transferStore", targetEntity = TransferItemEntity.class)
  @SaturnColumn(description = "转库明细")
  private Set<TransferItemEntity> transferItems;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Date getTransferTime() {
    return transferTime;
  }

  public void setTransferTime(Date transferTime) {
    this.transferTime = transferTime;
  }

  public StoreEntity getOutStore() {
    return outStore;
  }

  public void setOutStore(StoreEntity outStore) {
    this.outStore = outStore;
  }

  public StoreEntity getInStore() {
    return inStore;
  }

  public void setInStore(StoreEntity inStore) {
    this.inStore = inStore;
  }

  public Set<TransferItemEntity> getTransferItems() {
    return transferItems;
  }

  public void setTransferItems(Set<TransferItemEntity> transferItems) {
    this.transferItems = transferItems;
  }
}
