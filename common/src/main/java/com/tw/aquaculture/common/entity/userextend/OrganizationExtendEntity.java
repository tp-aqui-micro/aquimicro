package com.tw.aquaculture.common.entity.userextend;

import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.core.entity.UuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * 描述业务系统中的组织机构扩展信息
 * @author fangfu.luo
 * @date: 2019/12/12
 */
@ApiModel(value = "OrganizationExtendEntity", description = "组织机构扩展")
@Entity
@Table(name = "tw_organization_extend")
@SaturnDomain(value = "userextend")
public class OrganizationExtendEntity extends UuidEntity {

  private static final long serialVersionUID = -7899327347113427246L;

  @OneToOne(fetch = FetchType.LAZY, targetEntity = OrganizationEntity.class,cascade=CascadeType.PERSIST)
  @JoinColumn(name = "organization_id", columnDefinition = "varchar(255) COMMENT '组织id'")
  @SaturnColumn(description="组织id")
  private OrganizationEntity  organization;

  @Column(name = "hr_organization_id", columnDefinition = "varchar(255) comment'hr系统组织id'")
  @SaturnColumn(description = "hr系统组织id")
  @ApiModelProperty(name = "hrOrganizationId", value = "hr系统组织id", required = true)
  private String hrOrganizationId;

  @Column(name = "hr_effdt", columnDefinition = "varchar(255) comment'hr组织生效日期  yyyy-mm-dd'")
  @SaturnColumn(description = "hr组织生效日期")
  @ApiModelProperty(name = "effdt", value = "hr组织生效日期", required = true)
  private String effdt;

  @Column(name = "hr_descrshort", columnDefinition = "varchar(255) comment'hr系统组织简称'")
  @SaturnColumn(description = "hr系统组织简称")
  @ApiModelProperty(name = "descrshort", value = "hr系统组织简称", required = true)
  private String descrshort;

  @Column(name = "hr_descr", columnDefinition = "varchar(255) comment'hr系统组织名称'")
  @SaturnColumn(description = "hr系统组织名称")
  @ApiModelProperty(name = "descr", value = "hr系统组织名称", required = true)
  private String descr;

  @Column(name = "hr_tw_action", columnDefinition = "varchar(16) comment'hr系统twAction'")
  @SaturnColumn(description = "hr系统twAction")
  @ApiModelProperty(name = "twAction", value = "hr系统twAction", required = true)
  private String twAction;

  @Column(name = "hr_tw_org_type", columnDefinition = "varchar(16) comment'hr系统twOrgType'")
  @SaturnColumn(description = "hr系统twOrgType")
  @ApiModelProperty(name = "twOrgType", value = "hr系统twOrgType", required = true)
  private String twOrgType;

  @Column(name = "hr_tw_up_deptid", columnDefinition = "varchar(255) comment'hr系统上级部门'")
  @SaturnColumn(description = "hr系统上级部门Id")
  @ApiModelProperty(name = "twUpDeptId", value = "hr系统上级部门", required = true)
  private String twUpDeptId;

  @Column(name = "hr_tw_descr2", columnDefinition = "varchar(255) comment'hr系统twDescr2'")
  @SaturnColumn(description = "hr系统twDescr2")
  @ApiModelProperty(name = "twDescr2", value = "hr系统twDescr2", required = true)
  private String twDescr2;

  @Column(name = "hr_tw_itw_status", columnDefinition = "varchar(16) comment'hr系统twItwStatus'")
  @SaturnColumn(description = "hr系统twItwStatus")
  @ApiModelProperty(name = "twItwStatus", value = "hr系统twItwStatus", required = true)
  private String twItwStatus;

  @Column(name = "hr_company", columnDefinition = "varchar(255) comment'hr系统部门所属公司id'")
  @SaturnColumn(description = "hr系统部门所属公司id")
  @ApiModelProperty(name = "company", value = "hr系统部门所属公司id", required = true)
  private String company;

  @Column(name = "hr_tw_dept_type", columnDefinition = "varchar(255) comment'hr系统twDeptType'")
  @SaturnColumn(description = "hr系统twDeptType")
  @ApiModelProperty(name = "twDeptType", value = "hr系统twDeptType", required = true)
  private String twDeptType;

  @Column(name = "hr_tw_dept_property", columnDefinition = "varchar(255) comment'hr系统twDeptProperty'")
  @SaturnColumn(description = "hr系统twDeptProperty")
  @ApiModelProperty(name = "twDeptProperty", value = "hr系统twDeptProperty", required = true)
  private String twDeptProperty;

  public OrganizationEntity getOrganization() {
    return organization;
  }

  public void setOrganization(OrganizationEntity organization) {
    this.organization = organization;
  }

  public String getHrOrganizationId() {
    return hrOrganizationId;
  }

  public void setHrOrganizationId(String hrOrganizationId) {
    this.hrOrganizationId = hrOrganizationId;
  }

  public String getEffdt() {
    return effdt;
  }

  public void setEffdt(String effdt) {
    this.effdt = effdt;
  }

  public String getDescrshort() {
    return descrshort;
  }

  public void setDescrshort(String descrshort) {
    this.descrshort = descrshort;
  }

  public String getTwAction() {
    return twAction;
  }

  public void setTwAction(String twAction) {
    this.twAction = twAction;
  }

  public String getTwOrgType() {
    return twOrgType;
  }

  public void setTwOrgType(String twOrgType) {
    this.twOrgType = twOrgType;
  }

  public String getTwUpDeptId() {
    return twUpDeptId;
  }

  public void setTwUpDeptId(String twUpDeptId) {
    this.twUpDeptId = twUpDeptId;
  }

  public String getTwDescr2() {
    return twDescr2;
  }

  public void setTwDescr2(String twDescr2) {
    this.twDescr2 = twDescr2;
  }

  public String getTwItwStatus() {
    return twItwStatus;
  }

  public void setTwItwStatus(String twItwStatus) {
    this.twItwStatus = twItwStatus;
  }

  public String getCompany() {
    return company;
  }

  public void setCompany(String company) {
    this.company = company;
  }

  public String getTwDeptType() {
    return twDeptType;
  }

  public void setTwDeptType(String twDeptType) {
    this.twDeptType = twDeptType;
  }

  public String getTwDeptProperty() {
    return twDeptProperty;
  }

  public void setTwDeptProperty(String twDeptProperty) {
    this.twDeptProperty = twDeptProperty;
  }

  public String getDescr() {
    return descr;
  }

  public void setDescr(String descr) {
    this.descr = descr;
  }
}
