package com.tw.aquaculture.common.entity.userextend;

import com.bizunited.platform.core.entity.*;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 *  岗位扩展
 * @description:
 * @author: fangfu.luo
 * @date: 2019/12/12
 */
@ApiModel(value = "PositionExtendEntity", description = "岗位扩展")
@Entity
@Table(name = "tw_position_extend")
@SaturnDomain(value = "userextend")
public class PositionExtendEntity extends UuidEntity {

  private static final long serialVersionUID = 6180350993871864854L;

  @OneToOne(fetch = FetchType.LAZY, targetEntity = PositionEntity.class,cascade=CascadeType.PERSIST)
  @JoinColumn(name = "position_id", columnDefinition = "varchar(255) COMMENT '岗位id'")
  @SaturnColumn(description = "岗位id")
  private PositionEntity  position;

  @ManyToOne(fetch = FetchType.LAZY, targetEntity = PositionEntity.class,cascade=CascadeType.PERSIST)
  @JoinColumn(name = "parent_id",nullable = true,  columnDefinition = "varchar(255) COMMENT '父岗位id'")
  @SaturnColumn(description = "父岗位id")
  private PositionEntity  parent;

  @Column(name = "hr_position_id", nullable = false, columnDefinition = "varchar(255) comment'hr系统岗位id'")
  @SaturnColumn(description = "hr系统岗位id")
  @ApiModelProperty(name = "hrPositionId", value = "hr系统岗位id", required = true)
  private String hrPositionId;

  @Column(name = "hr_effdt", columnDefinition = "varchar(255) comment'hr生效日期  yyyy-mm-dd'")
  @SaturnColumn(description = "hr生效日期")
  @ApiModelProperty(name = "effdt", value = "hr生效日期", required = true)
  private String effdt;

  @Column(name = "hr_descrshort", columnDefinition = "varchar(255) comment'hr系统组织简称'")
  @SaturnColumn(description = "hr系统组织简称")
  @ApiModelProperty(name = "descrshort", value = "hr系统组织简称", required = true)
  private String descrshort;

    @Column(name = "hr_descr", columnDefinition = "varchar(255) comment'hr系统岗位名称'")
    @SaturnColumn(description = "hr系统岗位名称")
    @ApiModelProperty(name = "descr", value = "hr系统岗位名称", required = true)
    private String descr;

  @Column(name = "hr_job_function", columnDefinition = "varchar(16) comment'hr系统jobFunction'")
  @SaturnColumn(description = "hr系统jobFunction")
  @ApiModelProperty(name = "jobFunction", value = "hr系统jobFunction", required = true)
  private String jobFunction;

  @Column(name = "hr_job_sub_func", columnDefinition = "varchar(16) comment'hr系统jobSubFunc'")
  @SaturnColumn(description = "hr系统jobSubFunc")
  @ApiModelProperty(name = "jobSubFunc", value = "hr系统jobSubFunc", required = true)
  private String jobSubFunc;

  @Column(name = "hr_job_family", columnDefinition = "varchar(16) comment'hr系统jobFamily'")
  @SaturnColumn(description = "hr系统jobFamily")
  @ApiModelProperty(name = "jobFamily", value = "hr系统jobFamily", required = true)
  private String jobFamily;

  @Column(name = "hr_med_chkup_req", columnDefinition = "varchar(16) comment'hr系统medChkupReq'")
  @SaturnColumn(description = "hr系统medChkupReq")
  @ApiModelProperty(name = "medChkupReq", value = "hr系统medChkupReq", required = true)
  private String medChkupReq;

  @Column(name = "hr_tw_action", columnDefinition = "varchar(16) comment'hr系统twAction'")
  @SaturnColumn(description = "hr系统twAction")
  @ApiModelProperty(name = "twAction", value = "hr系统twAction", required = true)
  private String twAction;

  @Column(name = "hr_tw_org_type", columnDefinition = "varchar(16) comment'hr系统twOrgType'")
  @SaturnColumn(description = "hr系统twOrgType")
  @ApiModelProperty(name = "twOrgType", value = "hr系统twOrgType", required = true)
  private String twOrgType;

  public PositionEntity getPosition() {
    return position;
  }

  public void setPosition(PositionEntity position) {
    this.position = position;
  }

  public PositionEntity getParent() {
    return parent;
  }

  public void setParent(PositionEntity parent) {
    this.parent = parent;
  }

  public String getHrPositionId() {
    return hrPositionId;
  }

  public void setHrPositionId(String hrPositionId) {
    this.hrPositionId = hrPositionId;
  }

  public String getEffdt() {
    return effdt;
  }

  public void setEffdt(String effdt) {
    this.effdt = effdt;
  }

  public String getDescrshort() {
    return descrshort;
  }

  public void setDescrshort(String descrshort) {
    this.descrshort = descrshort;
  }

  public String getJobFunction() {
    return jobFunction;
  }

  public void setJobFunction(String jobFunction) {
    this.jobFunction = jobFunction;
  }

  public String getJobSubFunc() {
    return jobSubFunc;
  }

  public void setJobSubFunc(String jobSubFunc) {
    this.jobSubFunc = jobSubFunc;
  }

  public String getJobFamily() {
    return jobFamily;
  }

  public void setJobFamily(String jobFamily) {
    this.jobFamily = jobFamily;
  }

  public String getMedChkupReq() {
    return medChkupReq;
  }

  public void setMedChkupReq(String medChkupReq) {
    this.medChkupReq = medChkupReq;
  }

  public String getTwAction() {
    return twAction;
  }

  public void setTwAction(String twAction) {
    this.twAction = twAction;
  }

  public String getTwOrgType() {
    return twOrgType;
  }

  public void setTwOrgType(String twOrgType) {
    this.twOrgType = twOrgType;
  }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }
}
