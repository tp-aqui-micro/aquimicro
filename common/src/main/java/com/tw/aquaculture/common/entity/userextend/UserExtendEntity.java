package com.tw.aquaculture.common.entity.userextend;

import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.core.entity.UuidEntity;
import com.bizunited.platform.saturn.engine.annotation.SaturnColumn;
import com.bizunited.platform.saturn.engine.annotation.SaturnDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.Date;

/**
 *  用户扩展
 * @author fangfu.luo
 * @date 2019/12/12
 */
@ApiModel(value = "UserExtendEntity", description = "用户扩展")
@Entity
@Table(name = "tw_user_extend")
@SaturnDomain(value = "userextend")
public class UserExtendEntity extends UuidEntity {

  private static final long serialVersionUID = 4481698851872857209L;

  @JoinColumn(name = "user_id", columnDefinition = "varchar(255) comment'用户id'")
  @OneToOne(fetch = FetchType.LAZY,targetEntity = UserEntity.class ,cascade=CascadeType.PERSIST)
  @SaturnColumn(description = "用户")
  @ApiModelProperty(name = "user", value = "用户", required = true)
  private UserEntity user;

  @Column(name = "hr_user_id" , columnDefinition = "varchar(255) comment'hr系统用户id'")
  @SaturnColumn(description = "hr系统用户id")
  @ApiModelProperty(name = "hrUserId", value = "hr系统用户id", required = true)
  private String hrUserId;

  @Column(name = "hr_empl_rcd" , columnDefinition = "int(11) comment'hr系统用户雇佣记录'")
  @SaturnColumn(description = "hr系统用户雇佣记录")
  @ApiModelProperty(name = "hrEmplRcd", value = "hr系统用户雇佣记录", required = true)
  private Integer hrEmplRcd;

  @Column(name = "hr_effdt" , columnDefinition = "varchar(255) comment'hr系统用户生效日期'")
  @SaturnColumn(description = "hr系统用户生效日期")
  @ApiModelProperty(name = "hrEffdt", value = "hr系统用户生效日期", required = true)
  private String hrEffdt;

  @Column(name = "hr_effseq" , columnDefinition = "int(11) comment'hr系统用户生效序号'")
  @SaturnColumn(description = "hr系统用户生效序号")
  @ApiModelProperty(name = "hrEffseq", value = "hr系统用户生效序号", required = true)
  private Integer hrEffseq;

  @Column(name = "hr_job_indicator" , columnDefinition = "varchar(16) comment'hr系统用户主岗兼岗标识符(P-主岗，S-兼岗)'")
  @SaturnColumn(description = "hr系统用户主岗兼岗标识符(P-主岗，S-兼岗)")
  @ApiModelProperty(name = "hrJobIndicator", value = "hr系统用户主岗兼岗标识符(P-主岗，S-兼岗)", required = true)
  private String hrJobIndicator;

  @Column(name = "hr_name_ac" , columnDefinition = "varchar(255) comment'hr系统用户拼音姓名(名与姓用逗号隔开)'")
  @SaturnColumn(description = "hr系统用户拼音姓名(名与姓用逗号隔开)")
  @ApiModelProperty(name = "hrNameAc", value = "hr系统用户生拼音姓名(名与姓用逗号隔开)", required = true)
  private String hrNameAc;

  @Column(name = "hr_birthdate" , columnDefinition = "datetime comment'hr系统用户出生日期'")
  @SaturnColumn(description = "hr系统用户出生日期")
  @ApiModelProperty(name = "hrBirthdate", value = "hr系统用户出生日期", required = true)
  private Date hrBirthdate;

  @Column(name = "hr_mar_status" , columnDefinition = "varchar(2) comment'hr系统用户婚姻状况'")
  @SaturnColumn(description = "hr系统用户婚姻状况")
  @ApiModelProperty(name = "hrMarStatus", value = "hr系统用户婚姻状况", required = true)
  private String hrMarStatus;

  @Column(name = "hr_jpm_descr90" , columnDefinition = "varchar(32) comment'hr系统用户最高学历'")
  @SaturnColumn(description = "hr系统用户最高学历")
  @ApiModelProperty(name = "hrJpmDescr90", value = "hr系统用户最高学历", required = true)
  private String hrJpmDescr90;

  @Column(name = "hr_jpm_text254_1" , columnDefinition = "varchar(64) comment'hr系统用户最高学历专业'")
  @SaturnColumn(description = "hr系统用户最高学历专业")
  @ApiModelProperty(name = "hrJpmText2541", value = "hr系统用户最高学历专业", required = true)
  private String hrJpmText2541;

  @Column(name = "hr_national_id_type" , columnDefinition = "varchar(32) comment'hr系统用户证件类型'")
  @SaturnColumn(description = "hr系统用户证件类型")
  @ApiModelProperty(name = "hrNationalIdType", value = "hr系统用户证件类型", required = true)
  private String hrNationalIdType;

  @Column(name = "hr_national_id" , columnDefinition = "varchar(32) comment'hr系统用户证件号码'")
  @SaturnColumn(description = "hr系统用户证件号码")
  @ApiModelProperty(name = "hrMarStatus", value = "hr系统用户证件号码", required = true)
  private String hrNationalId;

  @Column(name = "hr_email_addr" , columnDefinition = "varchar(32) comment'hr系统用户公司邮箱'")
  @SaturnColumn(description = "hr系统用户公司邮箱")
  @ApiModelProperty(name = "hrEmailAddr", value = "hr系统用户公司邮箱", required = true)
  private String hrEmailAddr;

  @Column(name = "hr_phone" , columnDefinition = "varchar(32) comment'hr系统用户办公电话'")
  @SaturnColumn(description = "hr系统用户办公电话")
  @ApiModelProperty(name = "hrPhone", value = "hr系统用户办公电话", required = true)
  private String hrPhone;

  @Column(name = "hr_seniority_pay_dt" , columnDefinition = "datetime comment'hr系统用户加入本公司日期'")
  @SaturnColumn(description = "hr系统用户加入本公司日期")
  @ApiModelProperty(name = "hrSeniorityPayDt", value = "hr系统用户加入本公司日期", required = true)
  private Date hrSeniorityPayDt;

  @Column(name = "hr_company" , columnDefinition = "varchar(16) comment'hr系统用户所在公司id'")
  @SaturnColumn(description = "hr系统用户所在公司id")
  @ApiModelProperty(name = "hrCompany", value = "hr系统用户所在公司id", required = true)
  private String hrCompany;

  @Column(name = "hr_deptid" , columnDefinition = "varchar(32) comment'hr系统用户所在部门id'")
  @SaturnColumn(description = "hr系统用户所在部门id")
  @ApiModelProperty(name = "hrDeptid", value = "hr系统用户所在部门id", required = true)
  private String hrDeptid;

  @Column(name = "hr_tw_dept_line" , columnDefinition = "varchar(16) comment'hr系统用户条线ID'")
  @SaturnColumn(description = "hr系统用户条线ID")
  @ApiModelProperty(name = "hrTwDeptLine", value = "hr系统用户条线ID", required = true)
  private String hrTwDeptLine;

  @Column(name = "hr_descr" , columnDefinition = "varchar(255) comment'hr系统用户描述'")
  @SaturnColumn(description = "hr系统用户描述")
  @ApiModelProperty(name = "hrDescr", value = "hr系统用户描述", required = true)
  private String hrDescr;

  @Column(name = "hr_tw_dept_property" , columnDefinition = "varchar(16) comment'hr系统用户TwDeptProperty'")
  @SaturnColumn(description = "hr系统用户TwDeptProperty")
  @ApiModelProperty(name = "hrTwDeptProperty", value = "hr系统用户TwDeptProperty", required = true)
  private String hrTwDeptProperty;

  @Column(name = "hr_descr1" , columnDefinition = "varchar(255) comment'hr系统用户hrDescr1'")
  @SaturnColumn(description = "hr系统用户hrDescr1")
  @ApiModelProperty(name = "hrDescr1", value = "hr系统用户hrDescr1", required = true)
  private String hrDescr1;

  @Column(name = "hr_action" , columnDefinition = "varchar(16) comment'hr系统用户hrAction'")
  @SaturnColumn(description = "hr系统用户hrAction")
  @ApiModelProperty(name = "hrAction", value = "hr系统用户hrAction", required = true)
  private String hrAction;

  @Column(name = "hr_descr3" , columnDefinition = "varchar(255) comment'hr系统用户hrDescr3'")
  @SaturnColumn(description = "hr系统用户hrDescr3")
  @ApiModelProperty(name = "hrDescr3", value = "hr系统用户hrDescr3", required = true)
  private String hrDescr3;

  @Column(name = "hr_position_entry_dt" , columnDefinition = "varchar(32) comment'hr系统用户hrPositionEntryDt'")
  @SaturnColumn(description = "hr系统用户hrPositionEntryDt")
  @ApiModelProperty(name = "hrPositionEntryDt", value = "hr系统用户hrPositionEntryDt", required = true)
  private String hrPositionEntryDt;

  @Column(name = "hr_position_nbr" , columnDefinition = "varchar(32) comment'hr系统用户hrPositionNbr'")
  @SaturnColumn(description = "hr系统用户hrPositionNbr")
  @ApiModelProperty(name = "hrPositionNbr", value = "hr系统用户hrPositionNbr", required = true)
  private String hrPositionNbr;

  @Column(name = "hr_descr4" , columnDefinition = "varchar(255) comment'hr系统用户hrDescr4'")
  @SaturnColumn(description = "hr系统用户hrDescr4")
  @ApiModelProperty(name = "hrDescr4", value = "hr系统用户hrDescr4", required = true)
  private String hrDescr4;

  @Column(name = "hr_jobcode" , columnDefinition = "varchar(32) comment'hr系统用户hrJobcode'")
  @SaturnColumn(description = "hr系统用户hrJobcode")
  @ApiModelProperty(name = "hrJobcode", value = "hr系统用户hrJobcode", required = true)
  private String hrJobcode;

  @Column(name = "hr_descr5" , columnDefinition = "varchar(255) comment'hr系统用户hrDescr5'")
  @SaturnColumn(description = "hr系统用户hrDescr5")
  @ApiModelProperty(name = "hrDescr5", value = "hr系统用户hrDescr5", required = true)
  private String hrDescr5;

  @Column(name = "hr_location" , columnDefinition = "varchar(32) comment'hr系统用户hrLocation'")
  @SaturnColumn(description = "hr系统用户hrLocation")
  @ApiModelProperty(name = "hrLocation", value = "hr系统用户hrLocation", required = true)
  private String hrLocation;

  @Column(name = "hr_tw_job_level" , columnDefinition = "varchar(32) comment'hr系统用户hrTwJobLevel'")
  @SaturnColumn(description = "hr系统用户hrTwJobLevel")
  @ApiModelProperty(name = "hrTwJobLevel", value = "hr系统用户hrTwJobLevel", required = true)
  private String hrTwJobLevel;

  @Column(name = "hr_descr6" , columnDefinition = "varchar(255) comment'hr系统用户hrDescr6'")
  @SaturnColumn(description = "hr系统用户hrDescr6")
  @ApiModelProperty(name = "hrDescr6", value = "hr系统用户hrDescr6", required = true)
  private String hrDescr6;

  @Column(name = "hr_tw_pos_incharge" , columnDefinition = "varchar(32) comment'hr系统用户hrTwPosIncharge'")
  @SaturnColumn(description = "hr系统用户hrTwPosIncharge")
  @ApiModelProperty(name = "hrTwPosIncharge", value = "hr系统用户hrTwPosIncharge", required = true)
  private String hrTwPosIncharge;

  @Column(name = "hr_union_cd" , columnDefinition = "varchar(16) comment'hr系统用户hrUnionCd'")
  @SaturnColumn(description = "hr系统用户hrUnionCd")
  @ApiModelProperty(name = "hrUnionCd", value = "hr系统用户hrUnionCd", required = true)
  private String hrUnionCd;

  @Column(name = "hr_flag" , columnDefinition = "varchar(32) comment'hr系统用户hrFlag'")
  @SaturnColumn(description = "hr系统用户hrFlag")
  @ApiModelProperty(name = "hrFlag", value = "hr系统用户hrFlag", required = true)
  private String hrFlag;

  @Column(name = "hr_med_chkup_req" , columnDefinition = "varchar(32) comment'hr系统用户hrMedChkupReq'")
  @SaturnColumn(description = "hr系统用户hrMedChkupReq")
  @ApiModelProperty(name = "hrMedChkupReq", value = "hr系统用户hrMedChkupReq", required = true)
  private String hrMedChkupReq;

  @Column(name = "hr_job_function" , columnDefinition = "varchar(32) comment'hr系统用户hrJobFunction'")
  @SaturnColumn(description = "hr系统用户hrJobFunction")
  @ApiModelProperty(name = "hrJobFunction", value = "hr系统用户hrJobFunction", required = true)
  private String hrJobFunction;

  @Column(name = "hr_descr7" , columnDefinition = "varchar(255) comment'hr系统用户hrDescr7'")
  @SaturnColumn(description = "hr系统用户hrDescr7")
  @ApiModelProperty(name = "hrDescr7", value = "hr系统用户hrDescr7", required = true)
  private String hrDescr7;

  @Column(name = "hr_job_sub_func" , columnDefinition = "varchar(32) comment'hr系统用户hrJobSubFunc'")
  @SaturnColumn(description = "hr系统用户hrJobSubFunc")
  @ApiModelProperty(name = "hrJobSubFunc", value = "hr系统用户hrJobSubFunc", required = true)
  private String hrJobSubFunc;

  @Column(name = "hr_descr8" , columnDefinition = "varchar(32) comment'hr系统用户hrDescr8'")
  @SaturnColumn(description = "hr系统用户hrDescr8")
  @ApiModelProperty(name = "hrDescr8", value = "hr系统用户hrDescr8", required = true)
  private String hrDescr8;

  @Column(name = "hr_empl_class" , columnDefinition = "varchar(32) comment'hr系统用户hrEmplClass'")
  @SaturnColumn(description = "hr系统用户hrEmplClass")
  @ApiModelProperty(name = "hrEmplClass", value = "hr系统用户hrEmplClass", required = true)
  private String hrEmplClass;

  @Column(name = "hr_descr_2" , columnDefinition = "varchar(255) comment'hr系统用户hrDescr2'")
  @SaturnColumn(description = "hr系统用户hrDescr2")
  @ApiModelProperty(name = "hrDescr2", value = "hr系统用户hrDescr2", required = true)
  private String hrDescr2;

  @Column(name = "hr_supervisor_id" , columnDefinition = "varchar(32) comment'hr系统用户hrSupervisorId'")
  @SaturnColumn(description = "hr系统用户hrSupervisorId")
  @ApiModelProperty(name = "hrSupervisorId", value = "hr系统用户hrSupervisorId", required = true)
  private String hrSupervisorId;

  @Column(name = "hr_tw_action" , columnDefinition = "varchar(32) comment'hr系统用户hrTwAction'")
  @SaturnColumn(description = "hr系统用户hrTwAction")
  @ApiModelProperty(name = "hrTwAction", value = "hr系统用户hrTwAction", required = true)
  private String hrTwAction;

  @Column(name = "hr_tw_com_dept" , columnDefinition = "varchar(32) comment'hr系统用户hrTwComDept'")
  @SaturnColumn(description = "hr系统用户hrTwComDept")
  @ApiModelProperty(name = "hrTwComDept", value = "hr系统用户hrTwComDept", required = true)
  private String hrTwComDept;

  @Column(name = "hr_last_name" , columnDefinition = "varchar(32) comment'hr系统用户hrLastName'")
  @SaturnColumn(description = "hr系统用户hrLastName")
  @ApiModelProperty(name = "hrLastName", value = "hr系统用户hrLastName", required = true)
  private String hrLastName;


  @Column(name = "hr_first_name" , columnDefinition = "varchar(32) comment'hr系统用户hrFirstName'")
  @SaturnColumn(description = "hr系统用户hrFirstName")
  @ApiModelProperty(name = "hrFirstName", value = "hr系统用户hrFirstName", required = true)
  private String hrFirstName;


  @Column(name = "hr_tw_awe_line" , columnDefinition = "varchar(32) comment'hr系统用户hrTwAweLine'")
  @SaturnColumn(description = "hr系统用户hrTwAweLine")
  @ApiModelProperty(name = "hrTwAweLine", value = "hr系统用户hrTwAweLine", required = true)
  private String hrTwAweLine;


  @Column(name = "hr_tw_awe_line_descr" , columnDefinition = "varchar(255) comment'hr系统用户hrTwAweLineDescr'")
  @SaturnColumn(description = "hr系统用户hrTwAweLineDescr")
  @ApiModelProperty(name = "hrTwAweLineDescr", value = "hr系统用户hrTwAweLineDescr", required = true)
  private String hrTwAweLineDescr;


  @Column(name = "hr_tw_wx_id" , columnDefinition = "varchar(32) comment'hr系统用户hrTwWxId'")
  @SaturnColumn(description = "hr系统用户hrTwWxId")
  @ApiModelProperty(name = "hrTwWxId", value = "hr系统用户hrTwWxId", required = true)
  private String hrTwWxId;


  @Column(name = "hr_extension" , columnDefinition = "varchar(32) comment'hr系统用户hrExtension'")
  @SaturnColumn(description = "hr系统用户hrExtension")
  @ApiModelProperty(name = "hrExtension", value = "hr系统用户hrExtension", required = true)
  private String hrExtension;

  public UserEntity getUser() {
    return user;
  }

  public void setUser(UserEntity user) {
    this.user = user;
  }

  public String getHrUserId() {
    return hrUserId;
  }

  public void setHrUserId(String hrUserId) {
    this.hrUserId = hrUserId;
  }

  public Integer getHrEmplRcd() {
    return hrEmplRcd;
  }

  public void setHrEmplRcd(Integer hrEmplRcd) {
    this.hrEmplRcd = hrEmplRcd;
  }

  public String getHrEffdt() {
    return hrEffdt;
  }

  public void setHrEffdt(String hrEffdt) {
    this.hrEffdt = hrEffdt;
  }

  public Integer getHrEffseq() {
    return hrEffseq;
  }

  public void setHrEffseq(Integer hrEffseq) {
    this.hrEffseq = hrEffseq;
  }

  public String getHrJobIndicator() {
    return hrJobIndicator;
  }

  public void setHrJobIndicator(String hrJobIndicator) {
    this.hrJobIndicator = hrJobIndicator;
  }

  public String getHrNameAc() {
    return hrNameAc;
  }

  public void setHrNameAc(String hrNameAc) {
    this.hrNameAc = hrNameAc;
  }

  public Date getHrBirthdate() {
    return hrBirthdate;
  }

  public void setHrBirthdate(Date hrBirthdate) {
    this.hrBirthdate = hrBirthdate;
  }

  public String getHrMarStatus() {
    return hrMarStatus;
  }

  public void setHrMarStatus(String hrMarStatus) {
    this.hrMarStatus = hrMarStatus;
  }

  public String getHrJpmDescr90() {
    return hrJpmDescr90;
  }

  public void setHrJpmDescr90(String hrJpmDescr90) {
    this.hrJpmDescr90 = hrJpmDescr90;
  }

  public String getHrJpmText2541() {
    return hrJpmText2541;
  }

  public void setHrJpmText2541(String hrJpmText2541) {
    this.hrJpmText2541 = hrJpmText2541;
  }

  public String getHrNationalIdType() {
    return hrNationalIdType;
  }

  public void setHrNationalIdType(String hrNationalIdType) {
    this.hrNationalIdType = hrNationalIdType;
  }

  public String getHrNationalId() {
    return hrNationalId;
  }

  public void setHrNationalId(String hrNationalId) {
    this.hrNationalId = hrNationalId;
  }

  public String getHrEmailAddr() {
    return hrEmailAddr;
  }

  public void setHrEmailAddr(String hrEmailAddr) {
    this.hrEmailAddr = hrEmailAddr;
  }

  public String getHrPhone() {
    return hrPhone;
  }

  public void setHrPhone(String hrPhone) {
    this.hrPhone = hrPhone;
  }

  public Date getHrSeniorityPayDt() {
    return hrSeniorityPayDt;
  }

  public void setHrSeniorityPayDt(Date hrSeniorityPayDt) {
    this.hrSeniorityPayDt = hrSeniorityPayDt;
  }

  public String getHrCompany() {
    return hrCompany;
  }

  public void setHrCompany(String hrCompany) {
    this.hrCompany = hrCompany;
  }

  public String getHrDeptid() {
    return hrDeptid;
  }

  public void setHrDeptid(String hrDeptid) {
    this.hrDeptid = hrDeptid;
  }

  public String getHrTwDeptLine() {
    return hrTwDeptLine;
  }

  public void setHrTwDeptLine(String hrTwDeptLine) {
    this.hrTwDeptLine = hrTwDeptLine;
  }

  public String getHrDescr() {
    return hrDescr;
  }

  public void setHrDescr(String hrDescr) {
    this.hrDescr = hrDescr;
  }

  public String getHrTwDeptProperty() {
    return hrTwDeptProperty;
  }

  public void setHrTwDeptProperty(String hrTwDeptProperty) {
    this.hrTwDeptProperty = hrTwDeptProperty;
  }

  public String getHrDescr1() {
    return hrDescr1;
  }

  public void setHrDescr1(String hrDescr1) {
    this.hrDescr1 = hrDescr1;
  }

  public String getHrAction() {
    return hrAction;
  }

  public void setHrAction(String hrAction) {
    this.hrAction = hrAction;
  }

  public String getHrDescr3() {
    return hrDescr3;
  }

  public void setHrDescr3(String hrDescr3) {
    this.hrDescr3 = hrDescr3;
  }

  public String getHrPositionEntryDt() {
    return hrPositionEntryDt;
  }

  public void setHrPositionEntryDt(String hrPositionEntryDt) {
    this.hrPositionEntryDt = hrPositionEntryDt;
  }

  public String getHrPositionNbr() {
    return hrPositionNbr;
  }

  public void setHrPositionNbr(String hrPositionNbr) {
    this.hrPositionNbr = hrPositionNbr;
  }

  public String getHrDescr4() {
    return hrDescr4;
  }

  public void setHrDescr4(String hrDescr4) {
    this.hrDescr4 = hrDescr4;
  }

  public String getHrJobcode() {
    return hrJobcode;
  }

  public void setHrJobcode(String hrJobcode) {
    this.hrJobcode = hrJobcode;
  }

  public String getHrDescr5() {
    return hrDescr5;
  }

  public void setHrDescr5(String hrDescr5) {
    this.hrDescr5 = hrDescr5;
  }

  public String getHrLocation() {
    return hrLocation;
  }

  public void setHrLocation(String hrLocation) {
    this.hrLocation = hrLocation;
  }

  public String getHrTwJobLevel() {
    return hrTwJobLevel;
  }

  public void setHrTwJobLevel(String hrTwJobLevel) {
    this.hrTwJobLevel = hrTwJobLevel;
  }

  public String getHrDescr6() {
    return hrDescr6;
  }

  public void setHrDescr6(String hrDescr6) {
    this.hrDescr6 = hrDescr6;
  }

  public String getHrTwPosIncharge() {
    return hrTwPosIncharge;
  }

  public void setHrTwPosIncharge(String hrTwPosIncharge) {
    this.hrTwPosIncharge = hrTwPosIncharge;
  }

  public String getHrUnionCd() {
    return hrUnionCd;
  }

  public void setHrUnionCd(String hrUnionCd) {
    this.hrUnionCd = hrUnionCd;
  }

  public String getHrFlag() {
    return hrFlag;
  }

  public void setHrFlag(String hrFlag) {
    this.hrFlag = hrFlag;
  }

  public String getHrMedChkupReq() {
    return hrMedChkupReq;
  }

  public void setHrMedChkupReq(String hrMedChkupReq) {
    this.hrMedChkupReq = hrMedChkupReq;
  }

  public String getHrJobFunction() {
    return hrJobFunction;
  }

  public void setHrJobFunction(String hrJobFunction) {
    this.hrJobFunction = hrJobFunction;
  }

  public String getHrDescr7() {
    return hrDescr7;
  }

  public void setHrDescr7(String hrDescr7) {
    this.hrDescr7 = hrDescr7;
  }

  public String getHrJobSubFunc() {
    return hrJobSubFunc;
  }

  public void setHrJobSubFunc(String hrJobSubFunc) {
    this.hrJobSubFunc = hrJobSubFunc;
  }

  public String getHrDescr8() {
    return hrDescr8;
  }

  public void setHrDescr8(String hrDescr8) {
    this.hrDescr8 = hrDescr8;
  }

  public String getHrEmplClass() {
    return hrEmplClass;
  }

  public void setHrEmplClass(String hrEmplClass) {
    this.hrEmplClass = hrEmplClass;
  }

  public String getHrDescr2() {
    return hrDescr2;
  }

  public void setHrDescr2(String hrDescr2) {
    this.hrDescr2 = hrDescr2;
  }

  public String getHrSupervisorId() {
    return hrSupervisorId;
  }

  public void setHrSupervisorId(String hrSupervisorId) {
    this.hrSupervisorId = hrSupervisorId;
  }

  public String getHrTwAction() {
    return hrTwAction;
  }

  public void setHrTwAction(String hrTwAction) {
    this.hrTwAction = hrTwAction;
  }

  public String getHrTwComDept() {
    return hrTwComDept;
  }

  public void setHrTwComDept(String hrTwComDept) {
    this.hrTwComDept = hrTwComDept;
  }

  public String getHrLastName() {
    return hrLastName;
  }

  public void setHrLastName(String hrLastName) {
    this.hrLastName = hrLastName;
  }

  public String getHrFirstName() {
    return hrFirstName;
  }

  public void setHrFirstName(String hrFirstName) {
    this.hrFirstName = hrFirstName;
  }

  public String getHrTwAweLine() {
    return hrTwAweLine;
  }

  public void setHrTwAweLine(String hrTwAweLine) {
    this.hrTwAweLine = hrTwAweLine;
  }

  public String getHrTwAweLineDescr() {
    return hrTwAweLineDescr;
  }

  public void setHrTwAweLineDescr(String hrTwAweLineDescr) {
    this.hrTwAweLineDescr = hrTwAweLineDescr;
  }

  public String getHrTwWxId() {
    return hrTwWxId;
  }

  public void setHrTwWxId(String hrTwWxId) {
    this.hrTwWxId = hrTwWxId;
  }

  public String getHrExtension() {
    return hrExtension;
  }

  public void setHrExtension(String hrExtension) {
    this.hrExtension = hrExtension;
  }
}
