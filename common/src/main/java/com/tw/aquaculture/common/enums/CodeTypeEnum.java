package com.tw.aquaculture.common.enums;

import java.io.Serializable;

/**
 * code生成类型枚举
 *
 * @author 陈荣
 * @date 2019/11/22 13:40
 */
public enum CodeTypeEnum implements Serializable {

  ORGANIZATION_ENTITY("组织机构", "OrganizationEntity", "ZZ", 6),
  POSITION_ENTITY("岗位", "PositionEntity", "GW", 6),
  ROLE_ENTITY("角色", "RoleEntity", "JS", 6),
  /**
   * 基础模块数据编码
   */
  BASE_ENTITY("基地", "TwBaseEntity", "JD", 8),
  GROUP_ENTITY("养殖组", "TwGroupEntity", "YZ", 8),
  MATTER_ENTITY("物料", "MatterEntity", "WL", 8),
  POND_ENTITY("塘口", "PondEntity", "TK", 8),
  CUSTOMER_ENTITY("客户", "CustomerEntity", "KH", 8),
  FISH_TYPE_ENTITY("鱼种", "FishTypeEntity", "PZ", 8),
  TEST_INDEX_ENTITY("检测指标", "TestIndexEntity", "ZB", 8),
  SHARE_MODEL_ENTITY("模式管理", "ShareModelEntity", "MS", 8),
  STORE_ENTITY("仓库管理", "StoreEntity", "CK", 8),


  /**
   * 业务模块数据编码
   */
  YEARLY_PLAN_ENTITY("年度计划", "YearlyPlanEntity", "ND", 6),
  PUT_FRY_PLAN_ENTITY("投苗计划", "PutFryPlanEntity", "TM", 6),
  OUT_FISH_PLAN_ENTITY("出鱼计划", "OutFishPlanEntity", "CY", 6),

  WORK_ORDER_ENTITY("养殖工单", "WorkOrderEntity", "GD", 6),
  PUSH_FRY_ENTITY("鱼苗投放", "PushFryEntity", "TM", 6),
  PUSH_FEED_ENTITY("饲料投喂", "PushFeedEntity", "TL", 6),
  SAFEGUARD_ENTITY("动保", "SafegurdEntity", "DB", 6),
  FISH_DAMAGE_ENTITY("损鱼登记", "FishDamageEntity", "SY", 6),
  PROOF_YIELD_ENTITY("打样测产", "ProofYieldEntity", "DY", 6),
  DIVIDE_POND_ENTITY("转塘分塘", "DividePondEntity", "ZT", 6),
  OUT_FISH_APPLY_ENTITY("出鱼申请", "OutFishApplyEntity", "SQ", 6),
  OUT_FISH_REGIST_ENTITY("出鱼登记", "OutFishRegistEntity", "DJ", 6),
  CLEAN_POND_ENTITY("清塘", "CleanPondEntity", "QT", 6),
  BASK_POND_ENTITY("晒唐", "BaskPondEntity", "ST", 6),

  CHECK_POND_ENTITY("巡塘检测", "CheckPondEntity", "XT", 6),
  CHECK_WATER_QUALITY_ENTITY("水质检测", "CheckWaterQualityEntity", "JC", 6),
  CHECK_FISH_ENTITY("鱼体检测", "CheckFishEntity", "JC", 6),
  CHECK_TASK_ENTITY("临时任务", "CheckTaskEntity", "RW", 6),

  IN_STORAGE_ENITY("入库管理", "InStorageEntity", "RK", 6),
  STOCK_OUT_ENTITY("出库管理", "StockOutEntity", "CK", 6),
  TRANSFER_STORE_ENTITY("转库", "TransferStoreEntity", "ZK", 6),

  NOTICE_ENTITY("公告", "NoticeEntity", "GG", 6),

  ;


  private static final long serialVersionUID = 7485612775418416807L;

  private String memo;

  private String codeType;

  private String prefix;

  private int length;

  CodeTypeEnum(String memo, String codeType, String prefix, int length) {
    this.memo = memo;
    this.codeType = codeType;
    this.prefix = prefix;
    this.length = length;
  }

  public String getCodeType() {
    return codeType;
  }

  public String getPrefix() {
    return prefix;
  }

  public int getLength() {
    return length;
  }

  public String getMemo() {
    return memo;
  }
}
