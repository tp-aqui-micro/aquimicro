package com.tw.aquaculture.common.enums;

/**
 * @author 陈荣
 * @date 2019/11/12 11:03
 */
public enum CustomerTypeEnum {

  CUSTOMER_INSIDE("CT1001", "内部客户"),
  CUSTOMER_BIG("CT1002", "大客户"),
  CUSTOMER_FISH_INTERMEDIARY("CT1003", "鱼中介"),
  CUSTOMER_CASUAL("CT10004", "散户");

  private String code;

  private String name;

  CustomerTypeEnum(String code, String name) {
    this.code = code;
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public String getName() {
    return name;
  }
}
