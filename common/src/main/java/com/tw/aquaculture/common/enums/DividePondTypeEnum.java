package com.tw.aquaculture.common.enums;

/**
 * Created by chenrong on 2020/1/19
 */
public enum DividePondTypeEnum {

  OUT_POND(0, "转出"),
  IN_POND(1, "转入"),
  ALL(-1, "全部");

  private Integer state;

  private String memo;

  DividePondTypeEnum(Integer state, String memo) {
    this.state = state;
    this.memo = memo;
  }

  public Integer getState() {
    return state;
  }

  public String getMemo() {
    return memo;
  }
}
