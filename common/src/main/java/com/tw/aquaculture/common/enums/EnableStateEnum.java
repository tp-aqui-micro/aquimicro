package com.tw.aquaculture.common.enums;

/**
 * 生效/失效枚举
 * @author 陈荣
 * @date 2019/12/7 16:29
 */
public enum EnableStateEnum {

  DISABLE("失效/工单未完成/禁用", 0),
  ENABLE("生效/工单已完成/启用", 1),
  ALL("全部", -1);

  private String memo;

  private Integer state;

  EnableStateEnum(String memo, Integer state) {
    this.memo = memo;
    this.state = state;
  }

  public String getMemo() {
    return memo;
  }

  public Integer getState() {
    return state;
  }}
