package com.tw.aquaculture.common.enums;

/**
 * ebs 物料状态
 *
 * @author 陈荣
 * @date 2019/12/28 16:40
 */
public enum MatterEbsStateEnum {

  ACTIVE("有效","Active", 1),

  ACTIVE1("有效（禁止销售）","Active1", 1),

  ACTIVE2("有效（禁止采购）","Active2", 1),

  ERESREJECT("已拒绝","ERESReject", 0),

  INACTIVE("无效","Inactive", 0),

  NONE_SALE("淘汰产品","None_Sale", 0),

  NONE_STOCK("可销售但不可拥有库存","none_stock", 1),

  OPM("OPM","OPM", 0),

  PENDING("待定","Pending", 0);

  private String memo;

  private String code;

  private Integer state;

  MatterEbsStateEnum(String memo, String code, Integer state) {
    this.memo = memo;
    this.code = code;
    this.state = state;
  }

  public String getMemo() {
    return memo;
  }

  public String getCode() {
    return code;
  }

  public Integer getState() {
    return state;
  }}
