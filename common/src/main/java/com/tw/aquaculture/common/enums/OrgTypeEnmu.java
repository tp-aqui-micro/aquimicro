package com.tw.aquaculture.common.enums;

/**
 * @author 陈荣
 * @date 2019/11/1111:03
 */
public enum OrgTypeEnmu {

	//总部，养殖模式，总部部门，分公司，分公司部门，基地，养殖小组，其他部门

	HEAD_COMPANY("1", "总部"),
	AQUACULTURE_MODEL("2", "养殖模式"),
	HEAD_COMPANY_DEPARTMENT("3", "总部部门"),
	SON_COMPANY("4", "分公司"),
	SON_COMPANY_DEPARTMENT("5", "分公司部门"),
	BASE("6", "基地"),
	AQUACULTURE_GROUP("7", "养殖小组"),
	BASE_GROUP("8", "基地部门");

	private String code;

	private String memo;

	OrgTypeEnmu(String code, String memo) {
		this.code = code;
		this.memo = memo;
	}

	public String getCode() {
		return code;
	}

	public String getMemo() {
		return memo;
	}}
