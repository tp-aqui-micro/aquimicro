package com.tw.aquaculture.common.enums;
/**
 * @author 陈荣
 * @date 2019/12/19 9:23
 */
public enum  PondStateEnum {

  ZHENG_CHANG("正常", 0),
  FA_BING("发病", 1),
  XIAN_ZHI("闲置", 2),
  YUN_XING("运行", 3);

  private String memo;

  private Integer state;

  PondStateEnum(String memo, Integer state) {
    this.memo = memo;
    this.state = state;
  }

  public String getMemo() {
    return memo;
  }

  public Integer getState() {
    return state;
  }}
