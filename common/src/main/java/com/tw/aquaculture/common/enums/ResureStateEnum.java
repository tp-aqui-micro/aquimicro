package com.tw.aquaculture.common.enums;

/**
 * @author 陈荣
 * @date 2019/12/29 15:51
 */
public enum ResureStateEnum {

  UN_SUBMIT("未提交审批", -1),
  RESURING("审批中", 0),
  RESURED("审批通过", 1),
  KILL_BACK("驳回", 2),
  RETURN_BACK("退回", 3);

  private String memo;

  private Integer state;

  ResureStateEnum(String memo, Integer state) {
    this.memo = memo;
    this.state = state;
  }

  public String getMemo() {
    return memo;
  }

  public Integer getState() {
    return state;
  }}
