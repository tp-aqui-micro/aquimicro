package com.tw.aquaculture.common.mongo;


import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import java.util.ArrayList;
import java.util.List;

/**
 * @author: zengxingwang
 * @date: 2019/12/27 19:46
 * @description:mongoDB配置
 */
@Configuration
public class MongoConfig {
  // 注入配置实体
  @Autowired
  private MongoSettingsProperties mongoSettingsProperties;

  private MongoClient mongoClient = null;
  @Bean
  public MongoClient getMongoClient(){
    MongoClientOptions mongoClientOptions = null;
    if(mongoClient == null) {
      MongoClientOptions.Builder build = new MongoClientOptions.Builder();
      build.minConnectionsPerHost(mongoSettingsProperties.getMinConnectionsPerHost());
      build.connectionsPerHost(mongoSettingsProperties.getConnectionsPerHost());
      build.maxWaitTime(mongoSettingsProperties.getMaxWaitTime());
      build.connectTimeout(mongoSettingsProperties.getConnectTimeout());
      build.heartbeatFrequency(mongoSettingsProperties.getHeartbeatFrequency());
      build.heartbeatConnectTimeout(mongoSettingsProperties.getHeartbeatConnectTimeout());
      build.heartbeatSocketTimeout(mongoSettingsProperties.getHeartbeatSocketTimeout());
      mongoClientOptions = build.build();
    }
    //数据库连接实例
    // MongoDB地址列表
    List<ServerAddress> serverAddresses = new ArrayList<>();
    for (String host : mongoSettingsProperties.getHosts()) {
      Integer index = mongoSettingsProperties.getHosts().indexOf(host);
      Integer port = mongoSettingsProperties.getPorts().get(index);

      ServerAddress serverAddress = new ServerAddress(host, port);
      serverAddresses.add(serverAddress);
    }

    mongoClient = new MongoClient(serverAddresses, mongoClientOptions);
    return mongoClient;
  }

  public MongoDatabase getDatabase(String dataBaseName) {
    return mongoClient.getDatabase(dataBaseName);
  }

  public MongoCollection<Document> getCollection(String dataBaseName, String collectionName) {
    return mongoClient.getDatabase(dataBaseName).getCollection(collectionName);
  }

  @ConfigurationProperties(prefix = "spring.data.mongodb")
  MongoSettingsProperties mongoSettingsProperties() {
    return new MongoSettingsProperties();
  }
}
