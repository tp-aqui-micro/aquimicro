package com.tw.aquaculture.common.mongo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.List;

/**
 * @author: zengxingwang
 * @date: 2019/12/27 20:16
 * @description:
 */
@Component
@Validated
public class MongoSettingsProperties {

  /**
   * 数据库
   */
  private String database;

  /**
   * 主机
   */
  @Value("${spring.data.mongodb.hosts}")
  private List<String> hosts;

  /**
   * 端口
   */
  @Value("${spring.data.mongodb.ports}")
  private List<Integer> ports;

  private String replicaSet;
  private String username;
  private String password;
  private String authenticationDatabase;

  private Integer hreadsAllowedToBlockForConnectionMultiplier = 5;

  /**
   * 客户端最小连接数
   */
  private Integer minConnectionsPerHost = 10;

  /**
   *客户端最大连接数
   */
  private Integer connectionsPerHost = 100;

  /**
   * 一个线程等待连接可用的最大等待毫秒数，0表示不等待
   */
  private int maxWaitTime = 120000;

  /**
   * 连接超时时间，单位毫秒，0表示没有限制
   */
  private int connectTimeout = 10000;

  /**
   * 设置心跳频率。 这是驱动程序尝试确定群集中每个服务器的当前状态的频率，单位毫秒
   */
  private int heartbeatFrequency = 10000;

  /**
   * 心跳检测连接超时时间
   */
  private int heartbeatConnectTimeout = 20000;

  /**
   * 心跳检测Socket超时时间
   */
  private int heartbeatSocketTimeout = 20000;

  public MongoSettingsProperties() { }

  public Integer getHreadsAllowedToBlockForConnectionMultiplier() {
    return hreadsAllowedToBlockForConnectionMultiplier;
  }

  public void setHreadsAllowedToBlockForConnectionMultiplier(Integer hreadsAllowedToBlockForConnectionMultiplier) {
    this.hreadsAllowedToBlockForConnectionMultiplier = hreadsAllowedToBlockForConnectionMultiplier;
  }

  public int getMaxWaitTime() {
    return maxWaitTime;
  }

  public void setMaxWaitTime(int maxWaitTime) {
    this.maxWaitTime = maxWaitTime;
  }

  public int getConnectTimeout() {
    return connectTimeout;
  }

  public void setConnectTimeout(int connectTimeout) {
    this.connectTimeout = connectTimeout;
  }

  public int getHeartbeatFrequency() {
    return heartbeatFrequency;
  }

  public void setHeartbeatFrequency(int heartbeatFrequency) {
    this.heartbeatFrequency = heartbeatFrequency;
  }

  public int getHeartbeatConnectTimeout() {
    return heartbeatConnectTimeout;
  }

  public void setHeartbeatConnectTimeout(int heartbeatConnectTimeout) {
    this.heartbeatConnectTimeout = heartbeatConnectTimeout;
  }

  public int getHeartbeatSocketTimeout() {
    return heartbeatSocketTimeout;
  }

  public void setHeartbeatSocketTimeout(int heartbeatSocketTimeout) {
    this.heartbeatSocketTimeout = heartbeatSocketTimeout;
  }

  public String getDatabase() {
    return database;
  }

  public void setDatabase(String database) {
    this.database = database;
  }

  public List<String> getHosts() {
    return hosts;
  }

  public void setHosts(List<String> hosts) {
    this.hosts = hosts;
  }

  public List<Integer> getPorts() {
    return ports;
  }

  public void setPorts(List<Integer> ports) {
    this.ports = ports;
  }

  public String getReplicaSet() {
    return replicaSet;
  }

  public void setReplicaSet(String replicaSet) {
    this.replicaSet = replicaSet;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getAuthenticationDatabase() {
    return authenticationDatabase;
  }

  public void setAuthenticationDatabase(String authenticationDatabase) {
    this.authenticationDatabase = authenticationDatabase;
  }

  public Integer getMinConnectionsPerHost() {
    return minConnectionsPerHost;
  }

  public void setMinConnectionsPerHost(Integer minConnectionsPerHost) {
    this.minConnectionsPerHost = minConnectionsPerHost;
  }

  public Integer getConnectionsPerHost() {
    return connectionsPerHost;
  }

  public void setConnectionsPerHost(Integer connectionsPerHost) {
    this.connectionsPerHost = connectionsPerHost;
  }
}
