package com.tw.aquaculture.common.mongo;

import com.bizunited.platform.core.service.file.NebulaFileService;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.gridfs.GridFSFindIterable;
import com.mongodb.client.gridfs.model.GridFSFile;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Component
public class MongodbFileServiceImp implements NebulaFileService {

  @Autowired
  private GridFsTemplate gridFsTemplate;

  /**
   * 这里的保存逻辑是：先查询有没有这个路径的文件(原文件)，再新增一个文件(修改或新增的文件)，最后删除原文件
   * @param relativePath
   * @param fileName
   * @param fileRename
   * @param fileContext
   */
  @Override
  public void saveFile(String relativePath, String fileName, String fileRename, byte[] fileContext) {
    Validate.notBlank(relativePath,"relativePath路径不能为空");
    Validate.notBlank(fileName,"fileName名称不能为空");
    Validate.notBlank(fileRename,"fileRename名称不能为空");
    Document doc = new Document();
    doc.put("originalFileName",fileName);
    doc.put("relativePath",relativePath);
    doc.put("fileName",fileRename);

    //1======先查询是否有文件数据
    GridFSFindIterable oldFiles = gridFsTemplate.find(new Query(Criteria.where("filename").is(StringUtils.join(relativePath, "/",fileRename))));

    //3======最后再删除原有文件
    MongoCursor<GridFSFile> cursor = oldFiles.iterator();
    while(cursor.hasNext()){
      GridFSFile file = cursor.next();
      gridFsTemplate.delete(new Query(Criteria.where("_id").is(file.getObjectId())));
    }

    //2======再保存文件
    try(InputStream in = new ByteArrayInputStream(fileContext)){
        gridFsTemplate.store(in, StringUtils.join(relativePath, "/",fileRename),"image/jpeg/zip/docx/xlsx/txt/pdf/png",doc);
    }catch (Exception e){
        throw new IllegalArgumentException("文件保存出错：" + e.getMessage());
    }


  }

  @Override
  public void deleteFile(String relativePath, String fileName, String fileRename) {
    Validate.notBlank(relativePath,"relativePath路径不能为空");
    Validate.notBlank(fileName,"fileName名称不能为空");
    Validate.notBlank(fileRename,"fileRename名称不能为空");
    try{
      gridFsTemplate.delete(new Query(Criteria.where("filename").is(StringUtils.join(relativePath, File.separator,fileRename))));
    }catch (Exception e){
      throw new IllegalArgumentException("文件删除出错：" + e.getMessage());
    }
  }

  @Override
  public byte[] readFileContent(String relativePath, String fileRename) {
    Validate.notBlank(relativePath,"relativePath路径不能为空");
    Validate.notBlank(fileRename,"fileRename名称不能为空");
    try{
      GridFSFindIterable files =  gridFsTemplate.find(new Query(Criteria.where("filename").is(StringUtils.join(relativePath, "/",fileRename))));
      List<byte[]> bytesList = new ArrayList<>();
      MongoCursor<GridFSFile> cursor = files.iterator();
      while(cursor.hasNext()){
        GridFSFile file = cursor.next();
        GridFsResource resource = gridFsTemplate.getResource(file);
        byte[] f = this.getBytes(resource.getInputStream());
        bytesList.add(f);
      }
      if(CollectionUtils.isEmpty(bytesList)){
        return new byte[]{};
      }
      //由于mongo是分布式文件存储，所以需要将多个不同的byte[]数组，拼接成一个byte[]
      byte[] resultBytes = bytesList.get(0);
      for(int i = 1; i < bytesList.size(); i++){
        resultBytes = addBytes(resultBytes, bytesList.get(i));
      }
      return resultBytes;
    }catch (Exception e){
      throw new IllegalArgumentException("文件读取异常：" + e.getMessage());
    }
  }

  /**
   * 从流中读取数据
   * @param inputStream
   * @return
   */
  private byte[] getBytes(InputStream inputStream){
    try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
      int maxLen = 204800;
      byte[] readContext = new byte[maxLen];
      int realen;
      while((realen = inputStream.read(readContext, 0, maxLen)) != -1) {
        out.write(readContext, 0, realen);
      }
      return out.toByteArray();
    } catch (Exception e) {
      throw new IllegalArgumentException(e);
    }
  }

  /**
   * 对两个byte[]进行合并
   * @param data1
   * @param data2
   * @return
   */
  private byte[] addBytes(byte[] data1, byte[] data2) {
    byte[] newData = new byte[data1.length+data2.length];
    System.arraycopy(data1,0,newData,0,data1.length);
    System.arraycopy(data2,0,newData,data1.length,data2.length);
    return newData;
  }
}
