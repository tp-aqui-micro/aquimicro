package com.tw.aquaculture.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * @author 陈荣
 * @date 2019/11/21 18:20
 */
@Component
public class CodeGeneratorUtil {

  @Autowired
  private RedissonClient redissonClient;


  public static String toCodeString(long source, int size) {
    String sourceString = String.valueOf(source);
    if (sourceString.length() >= size) {
      return sourceString;
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      int whiteSpanSize = size - sourceString.length();

      for (int i = 0; i < whiteSpanSize; ++i) {
        stringBuilder.append(BigDecimal.ZERO);
      }

      stringBuilder.append(sourceString);
      return stringBuilder.toString();
    }
  }


  /**
   * 生成业务单据
   *
   * @param prefix         前辍  如MB
   * @param dateStr        日期字符串  如20190909
   * @param sequenceLength 流水长度
   * @return
   */
  public String generateBusinessNo(String prefix, String dateStr, int sequenceLength, String codeType) {
    if (StringUtils.isEmpty(prefix) || sequenceLength <= 0 || StringUtils.isBlank(codeType)) {
      return null;
    }
    String key = "aqui:generatecode:" + codeType + ":".concat(prefix);
    Long retLongValue;
    if (StringUtils.isEmpty(dateStr)) {
      retLongValue = redissonClient.getAtomicLong(key).incrementAndGet();
    } else {
      RMap<String, Long> map = redissonClient.getMap(key);
      retLongValue = map.addAndGet(dateStr, 1L);
    }
    return prefix.concat(StringUtils.trimToEmpty(dateStr)).concat(toCodeString(retLongValue, sequenceLength));
  }
}
