package com.tw.aquaculture.common.utils;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import java.util.concurrent.TimeUnit;

public final class DatePeriod {
    public static final long SECOND;
    public static final long MINUTE;
    public static final long HOUR;
    public static final long DAY;
    public static final long WEEK;

    public DatePeriod() {
    }

    static {
        SECOND = TimeUnit.SECONDS.toMillis(1L);
        MINUTE = TimeUnit.MINUTES.toMillis(1L);
        HOUR = TimeUnit.HOURS.toMillis(1L);
        DAY = TimeUnit.DAYS.toMillis(1L);
        WEEK = DAY * 7L;
    }
}
