package com.tw.aquaculture.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author 陈荣
 * @date 2019/11/22 14:50
 */
public class DateUtils {

  public final static String MONTH = "month";
  public final static String DYA = "day";

  public final static String FULL_TIME = "yyyy-MM-dd HH:mm:ss";
  public final static String TO_DAY_TIME = "yyyy-MM-dd";
  public final static String TO_YEAR_TIME = "yyyy";

  public static final String DEFAULT_DATE_SIMPLE_PATTERN = "yyyy-MM-dd";
  public static final String DEFAULT_DATETIME_24HOUR_PATTERN = "yyyy-MM-dd HH:mm:ss";
  private static final ThreadLocal<SimpleDateFormat> simpleDateFormatCache = new ThreadLocal<>();
  private static final ThreadLocal<Calendar> calendarCache = new ThreadLocal<>();
  public static final int MIN_HOUR = 0;
  public static final int MIN_MUNIT = 0;
  public static final int MIN_SECOND = 0;
  public static final int MAX_HOUR = 23;
  public static final int MAX_MUNIT = 59;
  public static final int MAX_SECOND = 59;

  /**
   * 生成业务编码使用的日期编码
   *
   * @return
   */
  public static String getCodeDate() {
    return new SimpleDateFormat("yyMMdd").format(new Date());
  }

  /**
   * 获取当月第一天
   *
   * @return
   */
  public static Date getMonthStart() {
    //获取前月的第一天
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.MONTH, -1);
    cal.set(Calendar.DAY_OF_MONTH, 1);
    return cal.getTime();
  }

  /**
   * 获取当天0时0分0秒
   *
   * @return
   */
  public static Date getDayStart() {
    Calendar cal = Calendar.getInstance();
    cal.setTime(new Date());
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    return cal.getTime();
  }

  /**
   * 判读date是否在两个时间范围内(模糊，实际以天为最小单位)
   *
   * @param date
   * @param startTime
   * @param endTime
   * @return
   */
  public static boolean between(Date date, Date startTime, Date endTime) {
    if (date == null || startTime == null || endTime == null) {
      return false;
    }
    if (startTime.getTime() > endTime.getTime()) {
      return false;
    }

    if (date.getTime() >= startTime.getTime() && date.getTime() <= endTime.getTime()) {
      return true;
    } else {
      Date now = new Date();
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(now);
      Calendar start = Calendar.getInstance();
      start.setTime(startTime);
      Calendar end = Calendar.getInstance();
      end.setTime(endTime);
      start.set(start.get(Calendar.YEAR), start.get(Calendar.MONTH), start.get(Calendar.DATE),
              MIN_HOUR, MIN_MUNIT, MIN_SECOND);
      end.set(end.get(Calendar.YEAR), end.get(Calendar.MONTH), end.get(Calendar.DATE),
              MAX_HOUR, MAX_MUNIT, MAX_SECOND);
      startTime = start.getTime();
      endTime = end.getTime();
      return date.getTime() >= startTime.getTime() && date.getTime() <= endTime.getTime();
    }
  }

  /**
   * 时间比较,是否max大于min
   *
   * @param max
   * @param min
   * @return
   */
  public static boolean bigThan(Date max, Date min) {
    if (max == null && min != null) {
      return false;
    } else if (max != null && min == null) {
      return true;
    } else if (max == null && min == null) {
      return false;
    }
    return max.getTime() > min.getTime();
  }

  /**
   * 比较date是否在startTime与endTime之间
   *
   * @param date
   * @param startTime
   * @param endTime
   * @return
   */
  public static boolean notBetween(Date date, Date startTime, Date endTime) {
    return !between(date, startTime, endTime);
  }

  /**
   * 根据时间格式转化时间成字符串
   *
   * @param date
   * @param type
   * @return
   */
  public static String dateFormat(Date date, String type) {
    if (date == null || type == null) {
      return null;
    }
    try {
      return new SimpleDateFormat(type).format(date);
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * 根据时间格式转化字符串为时间
   *
   * @param date
   * @param type
   * @return
   */
  public static Date dateParse(String date, String type) {
    if (date == null) {
      return null;
    }
    try {
      return new SimpleDateFormat(type).parse(date);
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * 比较两个时间
   *
   * @param small
   * @param big
   * @return
   */
  public static boolean compare(Date small, Date big) {
    if (small == null) {
      return true;
    }
    if (big == null) {
      return false;
    }
    return big.getTime() > small.getTime();
  }

  public DateUtils() {
  }

  /**
   * 获取字符串格式为yyyy-MM-dd HH:mm:ss的时间
   *
   * @param dateString
   * @return
   */
  public static Date getDateFromString(String dateString) {
    return getDateFromString(dateString, "yyyy-MM-dd HH:mm:ss");
  }

  /**
   * 日期求相差天数
   *
   * @param startDate
   * @param endDate
   * @return
   */
  public static int shortOfDay(Date startDate, Date endDate) {
    if (startDate == null || endDate == null) {
      return 0;
    }
    try {
      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
      long startDateTime = dateFormat.parse(dateFormat.format(startDate)).getTime();
      long endDateTime = dateFormat.parse(dateFormat.format(endDate)).getTime();
      return (int) ((endDateTime - startDateTime) / (1000 * 3600 * 24)) + 1;
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * 获取年初日期
   * @param y
   * @return
   */
  public static Date getYearStart(String y) {
    try{
      return new SimpleDateFormat(TO_YEAR_TIME).parse(y);
    }catch (Exception e){
      throw new RuntimeException(e);
    }
  }

  /**
   * 获取年末日期
   * @param y
   * @return
   */
  public static Date getYearEnd(String y) {
    Date date;
    try{
      Date time = new SimpleDateFormat(TO_YEAR_TIME).parse(y);
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(time);
      calendar.roll(Calendar.DAY_OF_YEAR, -1);
      date = getDayEnd(calendar.getTime());
    }catch (Exception e){
      throw new RuntimeException(e);
    }
    return date;
  }

  /**
   * 获取一天最后一毫秒秒
   * @param time
   * @return
   */
  public static Date getDayEnd(Date time) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(time);
    calendar.set(Calendar.HOUR, 23);
    calendar.set(Calendar.MINUTE, 59);
    calendar.set(Calendar.SECOND, 59);
    calendar.set(Calendar.MILLISECOND, 999);
    return calendar.getTime();
  }











  /**
   * 根据时间格式转化时间
   *
   * @param dateString
   * @param pattern
   * @return
   */
  public static Date getDateFromString(String dateString, String pattern) {
    try {
      SimpleDateFormat df = buildDateFormat(pattern);
      return df.parse(dateString);
    } catch (ParseException var3) {
      throw new RuntimeException(String.format("Could not parse %s with pattern %s.", dateString, pattern), var3);
    }
  }

  public static String getDateFormat(Date date, String pattern) {
    SimpleDateFormat simpleDateFormat = buildDateFormat(pattern);
    return simpleDateFormat.format(date);
  }

  public static String getDateFormat(String date, String datePattern, String formatPattern) {
    Date parsedDate = getDateFromString(date, datePattern);
    SimpleDateFormat simpleDateFormat = buildDateFormat(formatPattern);
    return simpleDateFormat.format(parsedDate);
  }

  public static Date getDateOfSecondsBack(int secondsBack, Date date) {
    return dateBack(13, secondsBack, date);
  }

  public static Date getDateOfMinutesBack(int minutesBack, Date date) {
    return dateBack(12, minutesBack, date);
  }

  public static Date getDateOfHoursBack(int hoursBack, Date date) {
    return dateBack(11, hoursBack, date);
  }

  public static Date getDateOfDaysBack(int daysBack, Date date) {
    return dateBack(5, daysBack, date);
  }

  public static Date getDateOfWeeksBack(int weeksBack, Date date) {
    return dateBack(4, weeksBack, date);
  }

  public static Date getDateOfMonthsBack(int monthsBack, Date date) {
    return dateBack(2, monthsBack, date);
  }

  public static Date getDateOfYearsBack(int yearsBack, Date date) {
    return dateBack(1, yearsBack, date);
  }

  public static int getSecondOfMinute(Date date) {
    return getNumberOfGranularity(13, date);
  }

  public static int getMinuteOfHour(Date date) {
    return getNumberOfGranularity(12, date);
  }

  public static int getHourOfDay(Date date) {
    return getNumberOfGranularity(11, date);
  }

  public static int getDayOfWeek(Date date) {
    return getNumberOfGranularity(7, date);
  }

  public static int getDayOfMonth(Date date) {
    return getNumberOfGranularity(5, date);
  }

  public static int getDayOfYear(Date date) {
    return getNumberOfGranularity(6, date);
  }

  public static int getWeekOfMonth(Date date) {
    return getNumberOfGranularity(4, date);
  }

  public static int getWeekOfYear(Date date) {
    return getNumberOfGranularity(3, date);
  }

  public static int getMonthOfYear(Date date) {
    return getNumberOfGranularity(2, date) + 1;
  }

  public static int getYear(Date date) {
    return getNumberOfGranularity(1, date);
  }

  public static boolean isFirstDayOfTheMonth(Date date) {
    return getDayOfMonth(date) == 1;
  }

  public static boolean isLastDayOfTheMonth(Date date) {
    Date dateOfMonthsBack = getDateOfMonthsBack(-1, date);
    int dayOfMonth = getDayOfMonth(dateOfMonthsBack);
    Date dateOfDaysBack = getDateOfDaysBack(dayOfMonth, dateOfMonthsBack);
    return dateOfDaysBack.equals(date);
  }

  public static Date getCurrentDate() {
    return new Date();
  }

  public static Timestamp currentTimestamp() {
    return new Timestamp(System.currentTimeMillis());
  }

  public static Date buildDate(long times) {
    return new Date(times);
  }

  public static long subSeconds(Date date1, Date date2) {
    return subTime(date1, date2, DatePeriod.SECOND);
  }

  public static long subMinutes(Date date1, Date date2) {
    return subTime(date1, date2, DatePeriod.MINUTE);
  }

  public static long subHours(Date date1, Date date2) {
    return subTime(date1, date2, DatePeriod.HOUR);
  }

  public static long subDays(String dateString1, String dateString2) {
    Date date1 = getDateFromString(dateString1, "yyyy-MM-dd");
    Date date2 = getDateFromString(dateString2, "yyyy-MM-dd");
    return subDays(date1, date2);
  }

  public static long subDays(Date date1, Date date2) {
    return subTime(date1, date2, DatePeriod.DAY);
  }

  public static long subMonths(String dateString1, String dateString2) {
    Date date1 = getDateFromString(dateString1, "yyyy-MM-dd");
    Date date2 = getDateFromString(dateString2, "yyyy-MM-dd");
    return subMonths(date1, date2);
  }

  public static long subMonths(Date date1, Date date2) {
    Calendar calendar1 = buildCalendar(date1);
    int monthOfDate1 = calendar1.get(2);
    int yearOfDate1 = calendar1.get(1);
    Calendar calendar2 = buildCalendar(date2);
    int monthOfDate2 = calendar2.get(2);
    int yearOfDate2 = calendar2.get(1);
    int subMonth = Math.abs(monthOfDate1 - monthOfDate2);
    int subYear = Math.abs(yearOfDate1 - yearOfDate2);
    return (long) (subYear * 12 + subMonth);
  }

  public static long subYears(String dateString1, String dateString2) {
    Date date1 = getDateFromString(dateString1, "yyyy-MM-dd");
    Date date2 = getDateFromString(dateString2, "yyyy-MM-dd");
    return subMonths(date1, date2);
  }

  public static long subYears(Date date1, Date date2) {
    return (long) Math.abs(getYear(date1) - getYear(date2));
  }

  public static Date formatToStartOfDay(Date date) {
    try {
      SimpleDateFormat dateFormat = buildDateFormat("yyyy-MM-dd");
      String formattedDate = dateFormat.format(date);
      return dateFormat.parse(formattedDate);
    } catch (ParseException var3) {
      throw new RuntimeException("Unparseable date specified.", var3);
    }
  }

  public static Date formatToEndOfDay(Date date) {
    return getDateOfSecondsBack(1, getDateOfDaysBack(-1, formatToStartOfDay(date)));
  }

  public static String format2String(LocalDateTime time) {
    return format2String(Timestamp.valueOf(time), (String) null);
  }

  public static String format2String(Timestamp ts, String fmt) {
    if (ts == null) {
      return null;
    } else {
      if (fmt == null) {
        fmt = "yyyy-MM-dd HH:mm:ss";
      }

      try {
        SimpleDateFormat sdf = new SimpleDateFormat(fmt);
        return sdf.format(ts);
      } catch (Exception var3) {
        return null;
      }
    }
  }

  private static SimpleDateFormat buildDateFormat(String pattern) {
    SimpleDateFormat simpleDateFormat = (SimpleDateFormat) simpleDateFormatCache.get();
    if (simpleDateFormat == null) {
      simpleDateFormat = new SimpleDateFormat();
      simpleDateFormatCache.set(simpleDateFormat);
    }

    simpleDateFormat.applyPattern(pattern);
    return simpleDateFormat;
  }

  private static Calendar buildCalendar() {
    Calendar calendar = (Calendar) calendarCache.get();
    if (calendar == null) {
      calendar = GregorianCalendar.getInstance();
      calendarCache.set(calendar);
    }

    return calendar;
  }

  private static Calendar buildCalendar(Date date) {
    Calendar calendar = buildCalendar();
    calendar.setTime(date);
    return calendar;
  }

  private static long subTime(Date date1, Date date2, long granularity) {
    long time1 = date1.getTime();
    long time = date2.getTime();
    long subTime = Math.abs(time1 - time);
    return subTime / granularity;
  }

  private static int getNumberOfGranularity(int granularity, Date date) {
    Calendar calendar = buildCalendar(date);
    return calendar.get(granularity);
  }

  private static long getTimeBackInMillis(int granularity, int numberToBack, Date date) {
    Calendar calendar = buildCalendar(date);
    calendar.add(granularity, -numberToBack);
    return calendar.getTimeInMillis();
  }

  private static Date dateBack(int granularity, int numberToBack, Date date) {
    long timeBackInMillis = getTimeBackInMillis(granularity, numberToBack, date);
    return buildDate(timeBackInMillis);
  }

  public static Timestamp getTimestamp(String dateStr) {
    return StringUtils.isNotBlank(dateStr) && dateStr.matches("\\d{4}-\\d{1,2}-\\d{1,2}") ? getTimestamp(dateStr, "yyyy-MM-dd") : getTimestamp(dateStr, "yyyy-MM-dd HH:mm:ss");
  }

  public static Timestamp getTimestamp(String dateStr, String dateFmt) {
    Timestamp timestamp = null;
    SimpleDateFormat f = new SimpleDateFormat(dateFmt);

    try {
      timestamp = new Timestamp(f.parse(dateStr).getTime());
    } catch (ParseException var5) {
      ;
    }

    return timestamp;
  }


}
