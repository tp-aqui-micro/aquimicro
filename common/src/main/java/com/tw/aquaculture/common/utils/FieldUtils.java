package com.tw.aquaculture.common.utils;

import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.process.WorkOrderEntity;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author 陈荣
 * @date 2019/12/16 17:58
 */
public class FieldUtils {

  public static void main(String[] args) {
    WorkOrderEntity workOrderEntity = new WorkOrderEntity();
    workOrderEntity.setEnableState(1);
    PondEntity pond = new PondEntity();
    pond.setCode("99");
    workOrderEntity.setPond(pond);
    WorkOrderEntity w = new WorkOrderEntity();
    w.setFormInstanceId("666");
    setFieldValueByFieldName(workOrderEntity, "formInstanceId", "888");
  }

  /**
   * 设置属性值
   * @param obj
   * @param fieldName
   * @param value
   */
  public static void setFieldValueByFieldName(Object obj, String fieldName, Object value) {
    try {
      Map<String, Field> fields = getFieldList(obj);
      Field f = fields.get(fieldName);
      if (f != null) {
        f.setAccessible(true);
        f.set(obj, value);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * 利用Java反射根据类的名称获取属性信息和父类的属性信息
   * @param obj
   * @return
   */
  public static Map<String, Field> getFieldList(Object obj) {
    Map<String, Field> fieldMap = new LinkedHashMap<>();
    Class<?> clazz = obj.getClass();
    Field[] fields = clazz.getDeclaredFields();
    for(Field field : fields){
      if (needFilterField(field)) {
        continue;
      }
      fieldMap.put(field.getName(), field);
    }
    getParentField(clazz, fieldMap);
    return fieldMap;
  }
  /**
   * 递归所有父类属性
   * @param clazz
   * @param fieldMap
   */
  private static void getParentField(Class<?> clazz, Map<String, Field> fieldMap){
    Class<?> superClazz = clazz.getSuperclass();
    if (superClazz != null) {
      Field[] superFields = superClazz.getDeclaredFields();
      for(Field field : superFields){
        if (needFilterField(field)) {
          continue;
        }
        fieldMap.put(field.getName(), field);
      }
      getParentField(superClazz, fieldMap);
    }
  }

  /**
   * 过滤不需要属性
   * @param field
   * @return
   */
  private static Boolean needFilterField(Field field){
    // 过滤静态属性
    if(Modifier.isStatic(field.getModifiers())){
      return true;
    }
    // 过滤transient 关键字修饰的属性
    if(Modifier.isTransient(field.getModifiers())){
      return true;
    }
    return false;
  }
}
