package com.tw.aquaculture.common.utils;

import com.alibaba.fastjson.JSONObject;
import com.bizunited.platform.core.controller.model.ResponseCode;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

/**
 * json转化工具类
 *
 * @author 陈荣
 * @date 2019/12/6 10:01
 */
public class JsonUtil {

  /**
   * 解析feign返回的ResponseModel类型的对象
   *
   * @param responseModel
   * @param typeReference
   * @param <T>
   * @return
   */
  public static <T> T parseResponseModel(ResponseModel responseModel, TypeReference<T> typeReference) {
    if (responseModel == null) {
      throw new RuntimeException(typeReference.getClass().getSimpleName()+"模型所在模块连接失败");
    }
    if(!ResponseCode.E0.equals(responseModel.getResponseCode())){
      throw new RuntimeException(responseModel.getErrorMsg());
    }
    Object map = responseModel.getData();
    if (map == null) {
      return null;
    }
    String json = JSONObject.toJSONString(map);
    T putFryPlanEntity = json2Obj(json, typeReference);
    return putFryPlanEntity;
  }

  /**
   * 解析feign返回的ResponseModel类型的对象（忽略异常）
   * @param responseModel
   * @param typeReference
   * @param <T>
   * @return
   */
  public static <T> T parseResponseModelWithOutException(ResponseModel responseModel, TypeReference<T> typeReference) {
    if (responseModel == null) {
      return null;
    }
    if(!ResponseCode.E0.equals(responseModel.getResponseCode())){
      return null;
    }
    Object map = responseModel.getData();
    if (map == null) {
      return null;
    }
    String json = JSONObject.toJSONString(map);
    T putFryPlanEntity = json2Obj(json, typeReference);
    return putFryPlanEntity;
  }

  /**
   * json转对象
   *
   * @param json
   * @param typeReference new TypeReference<List<ZentaoProjectVo>>(){}
   * @param <T>
   * @return
   */
  public static <T> T json2Obj(String json, TypeReference<T> typeReference) {
    if (StringUtils.isEmpty(json)) {
      return null;
    }
    T t = null;
    try {
      ObjectMapper mapper = new ObjectMapper();
      t = mapper.readValue(json, typeReference);
    } catch (IOException e) {

    }
    return t;
  }
}
