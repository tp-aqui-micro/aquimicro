package com.tw.aquaculture.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 陈荣
 * @date 2019/12/18 20:11
 */
public class MathUtil {

  public final static int INDEX_FIRST = 0;

  public final static int INDEX_LAST = -1;

  public static double round(Double a, int num) {
    if (a == null) {
      return 0;
    }
    double n = 1;
    for (int i = 0; i < num; i++) {
      n *= 10.0;
    }
    return Math.round(a * n) / n;
  }

  /**
   * 截取字符串中的第一个数字
   *
   * @param numStr
   * @return
   */
  public static Double subNum(String numStr, int index) {
    if (StringUtils.isBlank(numStr)) {
      return null;
    }
    String regx = "([1-9]\\d*\\.?\\d*)|(0\\.\\d*[1-9])";
    Pattern pattern = Pattern.compile(regx);
    Matcher matcher = pattern.matcher(numStr);
    Double num = null;
    int count = matcher.groupCount();
    if (index == INDEX_LAST || index >= count) {
      index = count - 1;
    }
    if (matcher.find()) {
      num = Double.parseDouble(matcher.group(index));
    }
    return num;
  }
}
