package com.tw.aquaculture.common.utils;

import com.tw.aquaculture.common.entity.base.MatterEntity;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

/**
 * 物料单位工具类
 *
 * @author 陈荣
 * @date 2019/12/21 14:38
 */
public class UnitUtil {

  public final static String UNIT_JIN = "斤";

  public final static double UNIT_JIN_LV = 2000.0;

  public final static String MATTER_TYPE_CODE_FEED = "11";

  /**
   * 单位换算（只支持辅单位换主单位）
   *
   * @param amount
   * @param matter
   * @return
   */
  /*public static Double compute(Double amount, MatterEntity matter) {

    return compute(null, amount, matter);
  }*/

  /**
   * 单位换算
   *
   * @param unit
   * @param amount
   * @param matter
   * @return
   */
  public static Double compute(String unit, Double amount, MatterEntity matter) {
    if (matter == null) {
      return amount;
    }
    if (amount == null) {
      return null;
    }
    if (matter.getMatterTypeCode() == null) {
      return amount;
    }
    if (MATTER_TYPE_CODE_FEED.equals(matter.getMatterTypeCode())) {
      if(UNIT_JIN.equals(unit)){
        return amount / UNIT_JIN_LV;
      }
    }
    
    Validate.notNull(matter.getScaleFactor(), "该物料换算率不正确，不可使用，请更换物料");
    if (Double.parseDouble(matter.getScaleFactor()) == 0) {
      return amount;
    }
    if(StringUtils.isBlank(unit)){
      if(StringUtils.isBlank(matter.getMatterHostUnitCode()) || StringUtils.isBlank(matter.getMatterHostUnitCode())){
        return amount;
      }
      return multiply(Double.parseDouble(matter.getScaleFactor()), amount);
    }

    if(unit.equals(matter.getMatterHostUnitCode()) || unit.equals(matter.getMatterHostUnit())){
      return amount;
    }
    return multiply(Double.parseDouble(matter.getScaleFactor()), amount);
  }

  /**
   * 辅单位换算成主单位
   *
   * @param scaleFactory
   * @param amount
   * @return
   */
  private static Double multiply(double scaleFactory, double amount) {

    return scaleFactory * amount;
  }


  /**
   * 主单位换算成辅单位
   * @param unit
   * @param amount
   * @param matter
   * @return
   */
  public static Double computeAssist(String unit, Double amount, MatterEntity matter) {
    if (amount == null) {
      return 0.0;
    }
    if (matter == null) {
      return amount;
    }
    if (matter.getMatterTypeCode() == null) {
      return amount;
    }

    Validate.notNull(matter.getScaleFactor(), "该物料换算率不正确，不可使用，请更换物料");
    if(Double.parseDouble(matter.getScaleFactor())==0){
      return amount;
    }
    if (Double.parseDouble(matter.getScaleFactor()) == 0) {
      return amount;
    }
    if(StringUtils.isBlank(unit)){
      return amount;
    }
    if(unit.equals(matter.getMatterAssistUnitCode()) || unit.equals(matter.getMatterAssistUnit())){
      return amount;
    }
    return amount/Double.parseDouble(matter.getScaleFactor());
  }

}
