package com.tw.aquaculture.common.utils.fileutils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.UUID;

/**
 * @author 陈荣
 * @date 2019/12/18 9:52
 */
public class FileHttpUtil {

  public static String FileUpload(File file, String fileName, String remoteUrl) {

    return uploadFile(file, fileName, remoteUrl, null, false);
  }

  public static String FileUpload(byte[] bytes, String fileName, String remoteUrl) {

    return uploadFile(null, fileName, remoteUrl, bytes, true);
  }

  private static String uploadFile(File file, String fileName, String remoteUrl, byte[] bytes, boolean flag){
    StringBuffer result = new StringBuffer();
    // 文件边界
    String Boundary = UUID.randomUUID().toString();
    try {
// 1.开启Http连接
      HttpURLConnection conn = (HttpURLConnection) new URL(remoteUrl).openConnection();
      conn.setConnectTimeout(10 * 1000);
      conn.setDoOutput(true);

// 2.Http请求行/头
      conn.setRequestMethod("POST");
      conn.setRequestProperty("Charset", "utf-8");
      conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + Boundary);

// 3.Http请求体
      DataOutputStream out = new DataOutputStream(conn.getOutputStream());
      out.writeUTF("--" + Boundary + "\r\n"
              + "Content-Disposition: form-data; name=\"file\"; filename=\""+fileName+"\"\r\n"
              + "Content-Type: application/octet-stream; charset=utf-8" + "\r\n\r\n");
      InputStream in = null;
      if(flag){
        out.write(bytes);
      }else{
        in = new FileInputStream(file);
        byte[] b = new byte[1024];
        int l = 0;
        while ((l = in.read(b)) != -1) {
          out.write(b, 0, l);
        }
      }
      out.writeUTF("\r\n--" + Boundary + "--\r\n");
      out.flush();
      out.close();
      if(in!=null){
        in.close();
      }

// 4.Http响应
      BufferedReader bf = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
      String line;
      while ((line = bf.readLine()) != null) {
        result.append(line);
      }
    } catch (Exception e) {

    }
    return result.toString();
  }

  public static void deleteFile(String remoteUrl){
    try {
      URL url=new URL(remoteUrl);
      System.out.println(url.toString());
      URLConnection connection=url.openConnection();
      connection.setReadTimeout(1000*60*60*3);
      connection.connect();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
