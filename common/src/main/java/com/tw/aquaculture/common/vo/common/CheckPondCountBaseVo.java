package com.tw.aquaculture.common.vo.common;

import java.io.Serializable;
import java.util.List;

/**
 * 巡塘次数，基地
 * @author 陈荣
 * @date 2019/12/5 10:48
 */
public class CheckPondCountBaseVo implements Serializable {

  private static final long serialVersionUID = 8897057593795785384L;
  /**
   * 基地id
   */
  private String baseId;

  /**
   * 基地组织id
   */
  private String baseOrgId;

  /**
   * 基地组织编码
   */
  private String baseOrgCode;
  /**
   * 基地名称
   */
  private String baseName;

  /**
   * 基地简称
   */
  private String simpleBaseName;

  /**
   * 基地编码
   */
  private String baseCode;
  /**
   * 塘口vo
   */
  private List<CheckPondCountPondVo> checkPondCountPondVoList;

  public String getBaseId() {
    return baseId;
  }

  public void setBaseId(String baseId) {
    this.baseId = baseId;
  }

  public String getBaseName() {
    return baseName;
  }

  public void setBaseName(String baseName) {
    this.baseName = baseName;
  }

  public String getBaseCode() {
    return baseCode;
  }

  public void setBaseCode(String baseCode) {
    this.baseCode = baseCode;
  }

  public List<CheckPondCountPondVo> getCheckPondCountPondVoList() {
    return checkPondCountPondVoList;
  }

  public void setCheckPondCountPondVoList(List<CheckPondCountPondVo> checkPondCountPondVoList) {
    this.checkPondCountPondVoList = checkPondCountPondVoList;
  }

  public String getBaseOrgId() {
    return baseOrgId;
  }

  public void setBaseOrgId(String baseOrgId) {
    this.baseOrgId = baseOrgId;
  }

  public String getBaseOrgCode() {
    return baseOrgCode;
  }

  public void setBaseOrgCode(String baseOrgCode) {
    this.baseOrgCode = baseOrgCode;
  }

  public String getSimpleBaseName() {
    return simpleBaseName;
  }

  public void setSimpleBaseName(String simpleBaseName) {
    this.simpleBaseName = simpleBaseName;
  }
}
