package com.tw.aquaculture.common.vo.common;

import java.io.Serializable;
import java.util.List;

/**
 * 巡塘次数，塘口
 * @author 陈荣
 * @date 2019/12/5 10:49
 */
public class CheckPondCountPondVo implements Serializable {

  private static final long serialVersionUID = 5072831719514203442L;
  /**
   * 塘口id
   */
  private String pondId;
  /**
   * 塘口编码
   */
  private String pondCode;
  /**
   * 塘口名称
   */
  private String pondName;
  /**
   * 当月巡塘次数
   */
  private int checkPondCountForMonth;
  /**
   * 当天巡塘次数
   */
  private int checkPondCountForDay;

  /**
   * 鱼种存塘量
   */
  private List<FishTypeSpecsVo> fishTypeSpecsVoList;

  /**
   *养殖工人
   */
  private String workerName;

  /**
   * 塘口状态
   * @return
   */
  private Integer pondState;

  public String getPondId() {
    return pondId;
  }

  public void setPondId(String pondId) {
    this.pondId = pondId;
  }

  public String getPondCode() {
    return pondCode;
  }

  public void setPondCode(String pondCode) {
    this.pondCode = pondCode;
  }

  public String getPondName() {
    return pondName;
  }

  public void setPondName(String pondName) {
    this.pondName = pondName;
  }

  public int getCheckPondCountForMonth() {
    return checkPondCountForMonth;
  }

  public void setCheckPondCountForMonth(int checkPondCountForMonth) {
    this.checkPondCountForMonth = checkPondCountForMonth;
  }

  public int getCheckPondCountForDay() {
    return checkPondCountForDay;
  }

  public void setCheckPondCountForDay(int checkPondCountForDay) {
    this.checkPondCountForDay = checkPondCountForDay;
  }

  public List<FishTypeSpecsVo> getFishTypeSpecsVoList() {
    return fishTypeSpecsVoList;
  }

  public void setFishTypeSpecsVoList(List<FishTypeSpecsVo> fishTypeSpecsVoList) {
    this.fishTypeSpecsVoList = fishTypeSpecsVoList;
  }

  public String getWorkerName() {
    return workerName;
  }

  public void setWorkerName(String workerName) {
    this.workerName = workerName;
  }

  public Integer getPondState() {
    return pondState;
  }

  public void setPondState(Integer pondState) {
    this.pondState = pondState;
  }
}
