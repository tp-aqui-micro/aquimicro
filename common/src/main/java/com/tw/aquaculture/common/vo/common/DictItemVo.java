package com.tw.aquaculture.common.vo.common;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author 陈荣
 * @date 2019/11/20 13:31
 */
public class DictItemVo implements Serializable {

  private static final long serialVersionUID = -8652253778483778192L;

  private String dictCode;

  private List<Map<String, Object>> dictItemEntities;

  public String getDictCode() {
    return dictCode;
  }

  public void setDictCode(String dictCode) {
    this.dictCode = dictCode;
  }

  public List<Map<String, Object>> getDictItemEntities() {
    return dictItemEntities;
  }

  public void setDictItemEntities(List<Map<String, Object>> dictItemEntities) {
    this.dictItemEntities = dictItemEntities;
  }
}
