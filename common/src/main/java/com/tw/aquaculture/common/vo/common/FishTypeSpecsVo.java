package com.tw.aquaculture.common.vo.common;

import java.io.Serializable;

/**
 * 鱼种和存塘量
 * @author 陈荣
 * @date 2019/12/5 10:50
 */
public class FishTypeSpecsVo implements Serializable {

  private static final long serialVersionUID = 7929606594862499363L;

  /**
   * 鱼种id
   */
  private String fishTypeId;

  /**
   * 鱼种编码
   */
  private String fishTypeCode;

  /**
   * 鱼种名称
   */
  private String fishTypeName;

  /**
   * 规格
   */
  private Double specs;

  /**
   * 存塘重量
   */
  private double exsitWeight;

  public String getFishTypeId() {
    return fishTypeId;
  }

  public void setFishTypeId(String fishTypeId) {
    this.fishTypeId = fishTypeId;
  }

  public String getFishTypeCode() {
    return fishTypeCode;
  }

  public void setFishTypeCode(String fishTypeCode) {
    this.fishTypeCode = fishTypeCode;
  }

  public String getFishTypeName() {
    return fishTypeName;
  }

  public void setFishTypeName(String fishTypeName) {
    this.fishTypeName = fishTypeName;
  }

  public Double getSpecs() {
    return specs;
  }

  public void setSpecs(Double specs) {
    this.specs = specs;
  }

  public double getExsitWeight() {
    return exsitWeight;
  }

  public void setExsitWeight(double exsitWeight) {
    this.exsitWeight = exsitWeight;
  }
}
