package com.tw.aquaculture.common.vo.common;

import java.io.Serializable;

/**
 * 物料消耗情况和库存量vo明细
 * @author 陈荣
 * @date 2019/12/2 17:38
 */
public class MatterBudgetItemVo implements Serializable {

  private static final long serialVersionUID = 680910375759743226L;

  //物料编码
  private String matterCode;

  //物料名称
  private String matterName;

  //消耗
  private double consume;

  //库存量
  private double stock;

  public String getMatterCode() {
    return matterCode;
  }

  public void setMatterCode(String matterCode) {
    this.matterCode = matterCode;
  }

  public String getMatterName() {
    return matterName;
  }

  public void setMatterName(String matterName) {
    this.matterName = matterName;
  }

  public double getConsume() {
    return consume;
  }

  public void setConsume(double consume) {
    this.consume = consume;
  }

  public double getStock() {
    return stock;
  }

  public void setStock(double stock) {
    this.stock = stock;
  }
}
