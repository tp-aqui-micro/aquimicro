package com.tw.aquaculture.common.vo.common;

import java.io.Serializable;
import java.util.List;

/**
 * 物料消耗情况和库存量vo
 * @author 陈荣
 * @date 2019/12/2 17:08
 */
public class MatterBudgetVo implements Serializable {

  private static final long serialVersionUID = -3998449051297983851L;

  //基地编码
  private String baseCode;

  //基地名称
  private String baseName;

  List<MatterBudgetItemVo> matterBudgetItemVoList;

  public String getBaseCode() {
    return baseCode;
  }

  public void setBaseCode(String baseCode) {
    this.baseCode = baseCode;
  }

  public String getBaseName() {
    return baseName;
  }

  public void setBaseName(String baseName) {
    this.baseName = baseName;
  }

  public List<MatterBudgetItemVo> getMatterBudgetItemVoList() {
    return matterBudgetItemVoList;
  }

  public void setMatterBudgetItemVoList(List<MatterBudgetItemVo> matterBudgetItemVoList) {
    this.matterBudgetItemVoList = matterBudgetItemVoList;
  }
}
