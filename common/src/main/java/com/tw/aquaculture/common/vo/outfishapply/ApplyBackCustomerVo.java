package com.tw.aquaculture.common.vo.outfishapply;

/**
 * @author 陈荣
 * @date 2020/1/1 19:30
 */
public class ApplyBackCustomerVo {

  private String customerId;

  private String customerName;

  private String fishTypeName;

  private String specs;

  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }

  public String getFishTypeName() {
    return fishTypeName;
  }

  public void setFishTypeName(String fishTypeName) {
    this.fishTypeName = fishTypeName;
  }

  public String getSpecs() {
    return specs;
  }

  public void setSpecs(String specs) {
    this.specs = specs;
  }
}
