package com.tw.aquaculture.common.vo.outfishapply;

import java.util.Date;
import java.util.List;

/**
 * @author 陈荣
 * @date 2020/1/1 19:29
 */
public class OutFishApplyVo {

  private String id;

  private String code;

  private String pondId;

  private String pondCode;

  private String pondName;

  private Date applyTime;

  private Integer resureState;

  private List<ApplyBackCustomerVo> customerVoList;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getPondId() {
    return pondId;
  }

  public void setPondId(String pondId) {
    this.pondId = pondId;
  }

  public String getPondCode() {
    return pondCode;
  }

  public void setPondCode(String pondCode) {
    this.pondCode = pondCode;
  }

  public String getPondName() {
    return pondName;
  }

  public void setPondName(String pondName) {
    this.pondName = pondName;
  }

  public Date getApplyTime() {
    return applyTime;
  }

  public void setApplyTime(Date applyTime) {
    this.applyTime = applyTime;
  }

  public Integer getResureState() {
    return resureState;
  }

  public void setResureState(Integer resureState) {
    this.resureState = resureState;
  }

  public List<ApplyBackCustomerVo> getCustomerVoList() {
    return customerVoList;
  }

  public void setCustomerVoList(List<ApplyBackCustomerVo> customerVoList) {
    this.customerVoList = customerVoList;
  }
}
