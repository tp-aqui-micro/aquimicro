package com.tw.aquaculture.common.vo.outfishapply;

/**
 * @author 陈荣
 * @date 2020/1/1 21:10
 */
public class PageParam {

  private int page;

  private int size;

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }
}
