package com.tw.aquaculture.common.vo.outfishapply;

/**
 * @author 陈荣
 * @date 2020/1/1 21:13
 */
public class PageResult<T> {

  private T content;

  private PageParam pageable;

  private long total;

  public PageResult(T content, PageParam pageable, long total) {
    this.content = content;
    this.pageable = pageable;
    this.total = total;
  }

  public T getContent() {
    return content;
  }

  public void setContent(T content) {
    this.content = content;
  }

  public PageParam getPageable() {
    return pageable;
  }

  public void setPageable(PageParam pageable) {
    this.pageable = pageable;
  }

  public long getTotal() {
    return total;
  }

  public void setTotal(long total) {
    this.total = total;
  }
}
