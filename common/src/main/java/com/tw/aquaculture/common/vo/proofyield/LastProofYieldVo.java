package com.tw.aquaculture.common.vo.proofyield;

import com.tw.aquaculture.common.entity.process.ProofYieldEntity;

import java.io.Serializable;

/**
 * 上次打样测产返回数据
 *
 * @author 陈荣
 * @date 2019/11/29 10:39
 */
public class LastProofYieldVo implements Serializable {

  private static final long serialVersionUID = 6757894459599572370L;
  /**
   * 上次打样测产
   */
  private ProofYieldEntity lastProofYield;

  /**
   * 当前存塘量
   */
  private double lastCount;

  public ProofYieldEntity getLastProofYield() {
    return lastProofYield;
  }

  public void setLastProofYield(ProofYieldEntity lastProofYield) {
    this.lastProofYield = lastProofYield;
  }

  public Double getLastCount() {
    return lastCount;
  }

  public void setLastCount(Double lastCount) {
    this.lastCount = lastCount;
  }
}
