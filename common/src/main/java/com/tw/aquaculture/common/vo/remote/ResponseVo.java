package com.tw.aquaculture.common.vo.remote;

/**
 * @author 陈荣
 * @date 2019/12/13 14:22
 */
public class ResponseVo<T extends Object> {

  private String transactionId;
  private String timestamp;
  private Integer status;
  private String message;
  private T data;

  public <C extends T> ResponseVo(String transactionId, String timestamp, Integer status, String message, T data) {
    this.transactionId = transactionId;
    this.timestamp = timestamp;
    this.status = status;
    this.message = message;
    this.data = data;
  }

  public static <T> ResponseVo<T> success(T data, String transactionId){
    return new ResponseVo<>(transactionId, Long.toString(System.currentTimeMillis()), 0, null, data);
  }

  public static <T> ResponseVo<T> fail(String transactionId, String message){
    return new ResponseVo<>(transactionId, Long.toString(System.currentTimeMillis()), 0, message, null);
  }

  public static <T> ResponseVo<T> error(){
    return new ResponseVo<>(null, Long.toString(System.currentTimeMillis()),0, "异常", null);
  }

  public static <T> ResponseVo<T> error(Throwable e){
    return new ResponseVo<>(null, Long.toString(System.currentTimeMillis()), 0, e.getMessage(), null);
  }

  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }
}
