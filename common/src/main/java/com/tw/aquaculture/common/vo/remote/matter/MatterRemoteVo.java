package com.tw.aquaculture.common.vo.remote.matter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 陈荣
 * @date 2019/11/20 14:47
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MatterRemoteVo implements Serializable {

  private static final long serialVersionUID = -2259968181458805638L;

  /**物料ID*/
  @JsonProperty("ITEM_ID")
  private String id;

  /**物料编码*/
  @JsonProperty("ITEM_CODE")
  private String code;

  /**物料名称*/
  @JsonProperty("ITEM_NAME")
  private String name;

  /**物料简称*/
  @JsonProperty("ITEM_JM")
  private String simpleName;

  /**主单位编码*/
  @JsonProperty("PRIMARY_UOM_CODE")
  private String hostUnitCode;

  /**主单位名称*/
  @JsonProperty("PRIMARY_UNIT_OF_MEASURE")
  private String hostUnitName;

  /**辅单位编码*/
  @JsonProperty("SECONDARY_UOM_CODE")
  private String assistUnitCode;

  /**辅单位名称*/
  @JsonProperty("SECONDARY_UNIT_OF_MEASURE")
  private String assistUnitName;

  /**规格*/
  @JsonProperty("ITEM_SPEC")
  private String specs;

  /**包装规格*/
  @JsonProperty("PKG_SPEC")
  private String packSpecs;

  /**物料大类编码, 只要11，12*/
  @JsonProperty("CATEGORY_FIRST")
  private String packBigClassCode;

  /**物料大类名称*/
  @JsonProperty("CATEGORY_FIRST_DESC")
  private String packBigClassName;

  /**物料中类编码*/
  @JsonProperty("CATEGORY_SEC")
  private String packMediumClassCode;

  /**物料中类名称*/
  @JsonProperty("CATEGORY_SEC_DESC")
  private String packMediumClassName;

  /**物料小类编码*/
  @JsonProperty("CATEGORY_THIRD")
  private String packSmallClassCode;

  /**物料小类名称*/
  @JsonProperty("CATEGORY_THIRD_DESC")
  private String packSmallClassName;

  /**最新更新时间*/
  @JsonProperty("LAST_UPDATE_DATE")
  private Date lastUpdateTime;

  /**状态，生效1、失效0, Active 有效
   Active1 有效（禁止销售）
   Active2 有效（禁止采购）
   ERESReject 已拒绝 ERES
   Inactive 无效
   None_Sale 淘汰产品
   none_stock 可销售但不可拥有库存
   OPM OPM
   Pending 待定*/
  @JsonProperty("INVENTORY_ITEM_STATUS_CODE")
  private String state;

  /**辅单位转换为主单位的系数*/
  @JsonProperty("CONVERT_FACTOR")
  private Double convertFactor;

  /**物料公司编码*/
  @JsonProperty("OU_CODE")
  private String companyCode;

  /**公司id*/
  @JsonProperty("OU_ID")
  private Integer companyId;

  /**公司名称*/
  @JsonProperty("OU_NAME")
  private String companyName;

  /**库存组织id*/
  @JsonProperty("ORGANIZATION_ID")
  private String orgId;

  /**库存组织编码*/
  @JsonProperty("ORGANIZATION_CODE")
  private String orgCode;

  /**库存组织名称*/
  @JsonProperty("ORGANIZATION_NAME")
  private String orgName;

  /**
   * 件重
   */
  @JsonProperty("PIECE_WEIGHT")
  private String pieceWeight;

  public String getCompanyCode() {
    return companyCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public Integer getCompanyId() {
    return companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSimpleName() {
    return simpleName;
  }

  public void setSimpleName(String simpleName) {
    this.simpleName = simpleName;
  }

  public String getHostUnitCode() {
    return hostUnitCode;
  }

  public void setHostUnitCode(String hostUnitCode) {
    this.hostUnitCode = hostUnitCode;
  }

  public String getHostUnitName() {
    return hostUnitName;
  }

  public void setHostUnitName(String hostUnitName) {
    this.hostUnitName = hostUnitName;
  }

  public String getAssistUnitCode() {
    return assistUnitCode;
  }

  public void setAssistUnitCode(String assistUnitCode) {
    this.assistUnitCode = assistUnitCode;
  }

  public String getAssistUnitName() {
    return assistUnitName;
  }

  public void setAssistUnitName(String assistUnitName) {
    this.assistUnitName = assistUnitName;
  }

  public String getSpecs() {
    return specs;
  }

  public void setSpecs(String specs) {
    this.specs = specs;
  }

  public String getPackSpecs() {
    return packSpecs;
  }

  public void setPackSpecs(String packSpecs) {
    this.packSpecs = packSpecs;
  }

  public String getPackBigClassCode() {
    return packBigClassCode;
  }

  public void setPackBigClassCode(String packBigClassCode) {
    this.packBigClassCode = packBigClassCode;
  }

  public String getPackBigClassName() {
    return packBigClassName;
  }

  public void setPackBigClassName(String packBigClassName) {
    this.packBigClassName = packBigClassName;
  }

  public String getPackMediumClassCode() {
    return packMediumClassCode;
  }

  public void setPackMediumClassCode(String packMediumClassCode) {
    this.packMediumClassCode = packMediumClassCode;
  }

  public String getPackMediumClassName() {
    return packMediumClassName;
  }

  public void setPackMediumClassName(String packMediumClassName) {
    this.packMediumClassName = packMediumClassName;
  }

  public String getPackSmallClassCode() {
    return packSmallClassCode;
  }

  public void setPackSmallClassCode(String packSmallClassCode) {
    this.packSmallClassCode = packSmallClassCode;
  }

  public String getPackSmallClassName() {
    return packSmallClassName;
  }

  public void setPackSmallClassName(String packSmallClassName) {
    this.packSmallClassName = packSmallClassName;
  }

  public Date getLastUpdateTime() {
    return lastUpdateTime;
  }

  public void setLastUpdateTime(Date lastUpdateTime) {
    this.lastUpdateTime = lastUpdateTime;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public Double getConvertFactor() {
    return convertFactor;
  }

  public void setConvertFactor(Double convertFactor) {
    this.convertFactor = convertFactor;
  }

  public String getPieceWeight() {
    return pieceWeight;
  }

  public void setPieceWeight(String pieceWeight) {
    this.pieceWeight = pieceWeight;
  }

  public String getOrgId() {
    return orgId;
  }

  public void setOrgId(String orgId) {
    this.orgId = orgId;
  }

  public String getOrgCode() {
    return orgCode;
  }

  public void setOrgCode(String orgCode) {
    this.orgCode = orgCode;
  }

  public String getOrgName() {
    return orgName;
  }

  public void setOrgName(String orgName) {
    this.orgName = orgName;
  }
}
