package com.tw.aquaculture.common.vo.remote.matter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author 陈荣
 * @date 2019/12/11 18:09
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MatterResponseBodyVo {

  @JsonProperty("P_ITEM_DATA_TAB_ITEM")
  List<MatterRemoteVo> matterRemoteVoList;

  public List<MatterRemoteVo> getMatterRemoteVoList() {
    return matterRemoteVoList;
  }

  public void setMatterRemoteVoList(List<MatterRemoteVo> matterRemoteVoList) {
    this.matterRemoteVoList = matterRemoteVoList;
  }

}
