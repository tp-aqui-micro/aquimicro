package com.tw.aquaculture.common.vo.remote.matter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author 陈荣
 * @date 2019/12/11 18:09
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MatterResponseHeaderVo {

  @JsonProperty("BIZTRANSACTIONID")
  private String uuid;

  @JsonProperty("COMMENTS")
  private String comments;

  @JsonProperty("ERRORCODE")
  private String errorCode;

  @JsonProperty("ERRORINFO")
  private String errorInfo;

  @JsonProperty("RESULT")
  private String result;

  @JsonProperty("SUCCESSCOUNT")
  private String successCount;

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getErrorInfo() {
    return errorInfo;
  }

  public void setErrorInfo(String errorInfo) {
    this.errorInfo = errorInfo;
  }

  public String getResult() {
    return result;
  }

  public void setResult(String result) {
    this.result = result;
  }

  public String getSuccessCount() {
    return successCount;
  }

  public void setSuccessCount(String successCount) {
    this.successCount = successCount;
  }
}
