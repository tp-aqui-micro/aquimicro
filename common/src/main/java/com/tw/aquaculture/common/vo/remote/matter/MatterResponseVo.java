package com.tw.aquaculture.common.vo.remote.matter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author 陈荣
 * @date 2019/12/11 18:08
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MatterResponseVo {

  @JsonProperty("RESPHEADER")
  private MatterResponseHeaderVo matterResponseHeaderVo;

  @JsonProperty("P_ITEM_DATA_TAB")
  private MatterResponseBodyVo matterResponseBodyVo;

  public MatterResponseHeaderVo getMatterResponseHeaderVo() {
    return matterResponseHeaderVo;
  }

  public void setMatterResponseHeaderVo(MatterResponseHeaderVo matterResponseHeaderVo) {
    this.matterResponseHeaderVo = matterResponseHeaderVo;
  }

  public MatterResponseBodyVo getMatterResponseBodyVo() {
    return matterResponseBodyVo;
  }

  public void setMatterResponseBodyVo(MatterResponseBodyVo matterResponseBodyVo) {
    this.matterResponseBodyVo = matterResponseBodyVo;
  }
}
