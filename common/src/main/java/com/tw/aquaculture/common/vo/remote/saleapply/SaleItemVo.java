package com.tw.aquaculture.common.vo.remote.saleapply;

/**
 * @author 陈荣
 * @date 2019/12/13 13:54
 */
public class SaleItemVo {

  /**中标标识*/
  private String winningMark;
  /**客户名称*/
  private String customerName;
  /**联系人*/
  private String contactsNAME;
  /**联系电话*/
  private String contactnumber;
  /**品种*/
  private String varietiesSales;
  /**需求数量*/
  private String quantityNum;
  /**需求规格*/
  private String requireSpecification;
  /**塘口价*/
  private String tangkouprice;
  /**运费*/
  private String freight;
  /**价格截至时间*/
  private String priceDeadline;
  /**销售规格*/
  private String salesSpecification;
  /**数量*/
  private String salesfreight;
  /**塘口价格*/
  private String tangkouPricecomfirm;
  /**运费*/
  private String freightcomfirm;
  /**备注*/
  private String salesRemarks;
  /**基地名*/
  private String baseName;
  /**堂口名*/
  private String pondName;

  public String getWinningMark() {
    return winningMark;
  }

  public void setWinningMark(String winningMark) {
    this.winningMark = winningMark;
  }

  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }

  public String getContactsNAME() {
    return contactsNAME;
  }

  public void setContactsNAME(String contactsNAME) {
    this.contactsNAME = contactsNAME;
  }

  public String getContactnumber() {
    return contactnumber;
  }

  public void setContactnumber(String contactnumber) {
    this.contactnumber = contactnumber;
  }

  public String getVarietiesSales() {
    return varietiesSales;
  }

  public void setVarietiesSales(String varietiesSales) {
    this.varietiesSales = varietiesSales;
  }

  public String getQuantityNum() {
    return quantityNum;
  }

  public void setQuantityNum(String quantityNum) {
    this.quantityNum = quantityNum;
  }

  public String getRequireSpecification() {
    return requireSpecification;
  }

  public void setRequireSpecification(String requireSpecification) {
    this.requireSpecification = requireSpecification;
  }

  public String getTangkouprice() {
    return tangkouprice;
  }

  public void setTangkouprice(String tangkouprice) {
    this.tangkouprice = tangkouprice;
  }

  public String getFreight() {
    return freight;
  }

  public void setFreight(String freight) {
    this.freight = freight;
  }

  public String getPriceDeadline() {
    return priceDeadline;
  }

  public void setPriceDeadline(String priceDeadline) {
    this.priceDeadline = priceDeadline;
  }

  public String getSalesSpecification() {
    return salesSpecification;
  }

  public void setSalesSpecification(String salesSpecification) {
    this.salesSpecification = salesSpecification;
  }

  public String getSalesfreight() {
    return salesfreight;
  }

  public void setSalesfreight(String salesfreight) {
    this.salesfreight = salesfreight;
  }

  public String getTangkouPricecomfirm() {
    return tangkouPricecomfirm;
  }

  public void setTangkouPricecomfirm(String tangkouPricecomfirm) {
    this.tangkouPricecomfirm = tangkouPricecomfirm;
  }

  public String getFreightcomfirm() {
    return freightcomfirm;
  }

  public void setFreightcomfirm(String freightcomfirm) {
    this.freightcomfirm = freightcomfirm;
  }

  public String getSalesRemarks() {
    return salesRemarks;
  }

  public void setSalesRemarks(String salesRemarks) {
    this.salesRemarks = salesRemarks;
  }

  public String getBaseName() {
    return baseName;
  }

  public void setBaseName(String baseName) {
    this.baseName = baseName;
  }

  public String getPondName() {
    return pondName;
  }

  public void setPondName(String pondName) {
    this.pondName = pondName;
  }
}
