package com.tw.aquaculture.common.vo.remote.saleapply;

/**
 * @author 陈荣
 * @date 2019/12/13 14:01
 */
public class SaleRequestVo {

  private String  transactionId;

  private SaleRequestBodyVo data;

  private String consummer;

  public String getConsummer() {
    return consummer;
  }

  public void setConsummer(String consummer) {
    this.consummer = consummer;
  }

  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public SaleRequestBodyVo getData() {
    return data;
  }

  public void setData(SaleRequestBodyVo data) {
    this.data = data;
  }
}
