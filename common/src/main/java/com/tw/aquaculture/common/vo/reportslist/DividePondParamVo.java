package com.tw.aquaculture.common.vo.reportslist;

import java.util.Date;

/**
 * Created by chenrong on 2020/1/19
 */
public class DividePondParamVo {

  /**公司id*/
  private String companyId;

  /**基地id*/
  private String baseId;

  /**塘口id*/
  private String pondId;

  /**品种*/
  private String fishType;

  /**类型,1:转出，2：转入*/
  private Integer divideType;

  /**开始时间*/
  private Date startTime;

  /**结束时间*/
  private Date endTime;

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getBaseId() {
    return baseId;
  }

  public void setBaseId(String baseId) {
    this.baseId = baseId;
  }

  public String getPondId() {
    return pondId;
  }

  public void setPondId(String pondId) {
    this.pondId = pondId;
  }

  public String getFishType() {
    return fishType;
  }

  public void setFishType(String fishType) {
    this.fishType = fishType;
  }

  public Integer getDivideType() {
    return divideType;
  }

  public void setDivideType(Integer divideType) {
    this.divideType = divideType;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }
}
