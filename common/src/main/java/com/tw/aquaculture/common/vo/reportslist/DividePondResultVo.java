package com.tw.aquaculture.common.vo.reportslist;

/**
 * Created by chenrong on 2020/1/19
 */
public class DividePondResultVo {

  /**公司id*/
  private String companyId;

  /**公司名称*/
  private String companyName;

  /**基地id*/
  private String baseId;

  /**基地名称*/
  private String baseName;

  /**塘口id*/
  private String pondId;

  /**塘口名称*/
  private String pondName;

  /**鱼种id*/
  private String fishTypeId;

  /**鱼种名称*/
  private String fishTypeName;

  /**查询类型，0：转出，1：转入，-1：全部*/
  private Integer divideTypeCode;

  /**转分塘类型名称*/
  private String divideTypeName;

  /**数量*/
  private double count;

  /**重量*/
  private double weight;

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getBaseId() {
    return baseId;
  }

  public void setBaseId(String baseId) {
    this.baseId = baseId;
  }

  public String getBaseName() {
    return baseName;
  }

  public void setBaseName(String baseName) {
    this.baseName = baseName;
  }

  public String getPondId() {
    return pondId;
  }

  public void setPondId(String pondId) {
    this.pondId = pondId;
  }

  public String getPondName() {
    return pondName;
  }

  public void setPondName(String pondName) {
    this.pondName = pondName;
  }

  public String getFishTypeId() {
    return fishTypeId;
  }

  public void setFishTypeId(String fishTypeId) {
    this.fishTypeId = fishTypeId;
  }

  public String getFishTypeName() {
    return fishTypeName;
  }

  public void setFishTypeName(String fishTypeName) {
    this.fishTypeName = fishTypeName;
  }

  public Integer getDivideTypeCode() {
    return divideTypeCode;
  }

  public void setDivideTypeCode(Integer divideTypeCode) {
    this.divideTypeCode = divideTypeCode;
  }

  public String getDivideTypeName() {
    return divideTypeName;
  }

  public void setDivideTypeName(String divideTypeName) {
    this.divideTypeName = divideTypeName;
  }

  public double getCount() {
    return count;
  }

  public void setCount(double count) {
    this.count = count;
  }

  public double getWeight() {
    return weight;
  }

  public void setWeight(double weight) {
    this.weight = weight;
  }
}
