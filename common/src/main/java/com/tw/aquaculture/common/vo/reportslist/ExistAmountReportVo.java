package com.tw.aquaculture.common.vo.reportslist;

import java.util.List;

/**
 * 存塘量报表
 * @author 陈荣
 * @date 2020/1/9 8:57
 */
public class ExistAmountReportVo {

  /**基地id*/
  private String baseId;

  /**塘口id*/
  private String pondId;

  /**公司id*/
  private String companyId;

  /**公司名称*/
  private String companyName;

  /**基地名称*/
  private String baseName;

  /**塘口名称*/
  private String pondName;

  /**数量*/
  private double count;

  /**重量*/
  private double weight;

  /**鱼种id*/
  private String fishTypeId;

  /**鱼种名称*/
  private String fishTypeName;

  /**规格*/
  private double specs;

  /**工单编号*/
  private String workOrderCode;

  /**工单id*/
  private String workOrderId;

  /**期初数量*/
  private double earlyCount;

  /**期初重量*/
  private double earlyWeight;

  /**本期投苗数量*/
  private double pushFryCount;

  /**本期投苗重量*/
  private double pushFryWeight;

  /**本期转入数量*/
  private double inCount;

  /**本期转入重量*/
  private double inWeight;

  /**本期转出数量*/
  private double outCount;

  /**本期转出重量*/
  private double outWeight;

  /**本期售鱼数量*/
  private double saleCount;

  /**本期售鱼重量*/
  private double saleWeight;

  /**本期损鱼数量*/
  private double damageCount;

  /**本期损鱼重量*/
  private double damageWeight;

  /**饲料投喂重量*/
  private double feedWeight;

  /**饲料单位*/
  private String feedUnit;

  /**本期日均投饵率*/
  private double averagePushFryRate;

  /**死亡率*/
  private double deadRate;

  /**预增长率*/
  private double upRate;

  /**塘口类型id*/
  private String pondTypeId;

  /**塘口类型名称*/
  private String pondTypeName;

  /**分类下塘口总面积*/
  private double totalArea;

  /**分类下塘口数量*/
  private int pondCount;

  /**塘口列表*/
  private List<String> pondIds;

  /**塘口名称列表*/
  private List<String> pondNames;

  public String getBaseId() {
    return baseId;
  }

  public void setBaseId(String baseId) {
    this.baseId = baseId;
  }

  public String getPondId() {
    return pondId;
  }

  public void setPondId(String pondId) {
    this.pondId = pondId;
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getBaseName() {
    return baseName;
  }

  public void setBaseName(String baseName) {
    this.baseName = baseName;
  }

  public String getPondName() {
    return pondName;
  }

  public void setPondName(String pondName) {
    this.pondName = pondName;
  }

  public double getCount() {
    return count;
  }

  public void setCount(double count) {
    this.count = count;
  }

  public double getWeight() {
    return weight;
  }

  public void setWeight(double weight) {
    this.weight = weight;
  }

  public String getFishTypeId() {
    return fishTypeId;
  }

  public void setFishTypeId(String fishTypeId) {
    this.fishTypeId = fishTypeId;
  }

  public String getFishTypeName() {
    return fishTypeName;
  }

  public void setFishTypeName(String fishTypeName) {
    this.fishTypeName = fishTypeName;
  }

  public double getSpecs() {
    return specs;
  }

  public void setSpecs(double specs) {
    this.specs = specs;
  }

  public String getWorkOrderCode() {
    return workOrderCode;
  }

  public void setWorkOrderCode(String workOrderCode) {
    this.workOrderCode = workOrderCode;
  }

  public String getWorkOrderId() {
    return workOrderId;
  }

  public void setWorkOrderId(String workOrderId) {
    this.workOrderId = workOrderId;
  }

  public double getEarlyCount() {
    return earlyCount;
  }

  public void setEarlyCount(double earlyCount) {
    this.earlyCount = earlyCount;
  }

  public double getEarlyWeight() {
    return earlyWeight;
  }

  public void setEarlyWeight(double earlyWeight) {
    this.earlyWeight = earlyWeight;
  }

  public double getPushFryCount() {
    return pushFryCount;
  }

  public void setPushFryCount(double pushFryCount) {
    this.pushFryCount = pushFryCount;
  }

  public double getPushFryWeight() {
    return pushFryWeight;
  }

  public void setPushFryWeight(double pushFryWeight) {
    this.pushFryWeight = pushFryWeight;
  }

  public double getInCount() {
    return inCount;
  }

  public void setInCount(double inCount) {
    this.inCount = inCount;
  }

  public double getInWeight() {
    return inWeight;
  }

  public void setInWeight(double inWeight) {
    this.inWeight = inWeight;
  }

  public double getOutCount() {
    return outCount;
  }

  public void setOutCount(double outCount) {
    this.outCount = outCount;
  }

  public double getOutWeight() {
    return outWeight;
  }

  public void setOutWeight(double outWeight) {
    this.outWeight = outWeight;
  }

  public double getSaleCount() {
    return saleCount;
  }

  public void setSaleCount(double saleCount) {
    this.saleCount = saleCount;
  }

  public double getSaleWeight() {
    return saleWeight;
  }

  public void setSaleWeight(double saleWeight) {
    this.saleWeight = saleWeight;
  }

  public double getDamageCount() {
    return damageCount;
  }

  public void setDamageCount(double damageCount) {
    this.damageCount = damageCount;
  }

  public double getDamageWeight() {
    return damageWeight;
  }

  public void setDamageWeight(double damageWeight) {
    this.damageWeight = damageWeight;
  }

  public double getFeedWeight() {
    return feedWeight;
  }

  public void setFeedWeight(double feedWeight) {
    this.feedWeight = feedWeight;
  }

  public String getFeedUnit() {
    return feedUnit;
  }

  public void setFeedUnit(String feedUnit) {
    this.feedUnit = feedUnit;
  }

  public double getAveragePushFryRate() {
    return averagePushFryRate;
  }

  public void setAveragePushFryRate(double averagePushFryRate) {
    this.averagePushFryRate = averagePushFryRate;
  }

  public double getDeadRate() {
    return deadRate;
  }

  public void setDeadRate(double deadRate) {
    this.deadRate = deadRate;
  }

  public double getUpRate() {
    return upRate;
  }

  public void setUpRate(double upRate) {
    this.upRate = upRate;
  }

  public String getPondTypeId() {
    return pondTypeId;
  }

  public void setPondTypeId(String pondTypeId) {
    this.pondTypeId = pondTypeId;
  }

  public String getPondTypeName() {
    return pondTypeName;
  }

  public void setPondTypeName(String pondTypeName) {
    this.pondTypeName = pondTypeName;
  }

  public double getTotalArea() {
    return totalArea;
  }

  public void setTotalArea(double totalArea) {
    this.totalArea = totalArea;
  }

  public int getPondCount() {
    return pondCount;
  }

  public void setPondCount(int pondCount) {
    this.pondCount = pondCount;
  }

  public List<String> getPondIds() {
    return pondIds;
  }

  public void setPondIds(List<String> pondIds) {
    this.pondIds = pondIds;
  }

  public List<String> getPondNames() {
    return pondNames;
  }

  public void setPondNames(List<String> pondNames) {
    this.pondNames = pondNames;
  }
}
