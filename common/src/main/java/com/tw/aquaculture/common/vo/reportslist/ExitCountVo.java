package com.tw.aquaculture.common.vo.reportslist;

/**
 * 存塘量计算字段
 * @author 陈荣
 * @date 2020/1/9 8:57
 */
public class ExitCountVo {

  /**投苗数量*/
  private double pushFryCount;

  /**转入数量*/
  private double dividInCount;

  /**转出数量*/
  private double dividOutCount;

  /**售鱼数量*/
  private double saleCount;

  /**损鱼数量*/
  private double damageCount;

  /**当前时间段内存塘量*/
  private double exitCount;

  /**出鱼量*/
  private double outCount;

  /**入鱼量*/
  private double inCount;

  public double getPushFryCount() {
    return pushFryCount;
  }

  public void setPushFryCount(double pushFryCount) {
    this.pushFryCount = pushFryCount;
  }

  public double getDividInCount() {
    return dividInCount;
  }

  public void setDividInCount(double dividInCount) {
    this.dividInCount = dividInCount;
  }

  public double getDividOutCount() {
    return dividOutCount;
  }

  public void setDividOutCount(double dividOutCount) {
    this.dividOutCount = dividOutCount;
  }

  public double getSaleCount() {
    return saleCount;
  }

  public void setSaleCount(double saleCount) {
    this.saleCount = saleCount;
  }

  public double getDamageCount() {
    return damageCount;
  }

  public void setDamageCount(double damageCount) {
    this.damageCount = damageCount;
  }

  public double getExitCount() {
    return exitCount;
  }

  public void setExitCount(double exitCount) {
    this.exitCount = exitCount;
  }

  public double getOutCount() {
    return outCount;
  }

  public void setOutCount(double outCount) {
    this.outCount = outCount;
  }

  public double getInCount() {
    return inCount;
  }

  public void setInCount(double inCount) {
    this.inCount = inCount;
  }
}
