package com.tw.aquaculture.common.vo.reportslist;

import java.util.Date;

/**
 * 损鱼报表vo
 * @author 陈荣
 * @date 2020/1/7 19:25
 */
public class FishDamageReportVo {

  private Date startTime;

  private Date endTime;

  /**基地id*/
  private String baseId;

  /**塘口id*/
  private String pondId;

  /**公司id*/
  private String companyId;

  /**公司名称*/
  private String companyName;

  /**基地名称*/
  private String baseName;

  /**塘口名称*/
  private String pondName;

  /**数量*/
  private Double count;

  /**重量*/
  private Double weight;

  /**死亡率*/
  private Double deadRate;

  /**存活率*/
  private Double liveRate;

  /**鱼种id*/
  private String fishTypeId;

  /**鱼种名称*/
  private String fishTypeName;

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public String getBaseId() {
    return baseId;
  }

  public void setBaseId(String baseId) {
    this.baseId = baseId;
  }

  public String getPondId() {
    return pondId;
  }

  public void setPondId(String pondId) {
    this.pondId = pondId;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getBaseName() {
    return baseName;
  }

  public void setBaseName(String baseName) {
    this.baseName = baseName;
  }

  public String getPondName() {
    return pondName;
  }

  public void setPondName(String pondName) {
    this.pondName = pondName;
  }

  public Double getCount() {
    return count;
  }

  public void setCount(Double count) {
    this.count = count;
  }

  public Double getWeight() {
    return weight;
  }

  public void setWeight(Double weight) {
    this.weight = weight;
  }

  public Double getDeadRate() {
    return deadRate;
  }

  public void setDeadRate(Double deadRate) {
    this.deadRate = deadRate;
  }

  public Double getLiveRate() {
    return liveRate;
  }

  public void setLiveRate(Double liveRate) {
    this.liveRate = liveRate;
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getFishTypeId() {
    return fishTypeId;
  }

  public void setFishTypeId(String fishTypeId) {
    this.fishTypeId = fishTypeId;
  }

  public String getFishTypeName() {
    return fishTypeName;
  }

  public void setFishTypeName(String fishTypeName) {
    this.fishTypeName = fishTypeName;
  }
}
