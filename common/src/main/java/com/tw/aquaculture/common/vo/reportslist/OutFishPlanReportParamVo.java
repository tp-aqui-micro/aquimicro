package com.tw.aquaculture.common.vo.reportslist;

/**
 * Created by chenrong on 2020/1/28
 */
public class OutFishPlanReportParamVo {

  /**公司id*/
  private String companyId;

  /**基地id*/
  private String baseId;

  /**塘口id*/
  private String pondId;

  /**品种*/
  private String fishType;

  /**年度*/
  private String yearly;

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getBaseId() {
    return baseId;
  }

  public void setBaseId(String baseId) {
    this.baseId = baseId;
  }

  public String getPondId() {
    return pondId;
  }

  public void setPondId(String pondId) {
    this.pondId = pondId;
  }

  public String getFishType() {
    return fishType;
  }

  public void setFishType(String fishType) {
    this.fishType = fishType;
  }

  public String getYearly() {
    return yearly;
  }

  public void setYearly(String yearly) {
    this.yearly = yearly;
  }
}
