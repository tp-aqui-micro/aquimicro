package com.tw.aquaculture.common.vo.reportslist;

/**
 * Created by chenrong on 2020/1/28
 */
public class OutFishPlanReportResultVo {

  /**年度*/
  private String yearly;

  /**月度*/
  private String monthly;

  /**公司id*/
  private String companyId;

  /**公司名称*/
  private String companyName;

  /**基地id*/
  private String baseId;

  /**基地名称*/
  private String baseName;

  /**塘口id*/
  private String pondId;

  /**塘口名称*/
  private String pondName;

  /**鱼种id*/
  private String fishTypeId;

  /**鱼种名称*/
  private String fishTypeName;

  /**计划出鱼数量*/
  private double planOutFishCount;

  /**计划出鱼重量*/
  private double planOutFishWeight;

  /**实际出鱼数量*/
  private double realOutFishCount;

  /**实际出鱼重量*/
  private double realOutFishWeight;

  /**出鱼进度*/
  private double outFishProcess;

  /**交通费*/
  private double transportFee;

  public double getTransportFee() {
    return transportFee;
  }

  public void setTransportFee(double transportFee) {
    this.transportFee = transportFee;
  }

  public String getYearly() {
    return yearly;
  }

  public void setYearly(String yearly) {
    this.yearly = yearly;
  }

  public String getMonthly() {
    return monthly;
  }

  public void setMonthly(String monthly) {
    this.monthly = monthly;
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getBaseId() {
    return baseId;
  }

  public void setBaseId(String baseId) {
    this.baseId = baseId;
  }

  public String getBaseName() {
    return baseName;
  }

  public void setBaseName(String baseName) {
    this.baseName = baseName;
  }

  public String getPondId() {
    return pondId;
  }

  public void setPondId(String pondId) {
    this.pondId = pondId;
  }

  public String getPondName() {
    return pondName;
  }

  public void setPondName(String pondName) {
    this.pondName = pondName;
  }

  public String getFishTypeId() {
    return fishTypeId;
  }

  public void setFishTypeId(String fishTypeId) {
    this.fishTypeId = fishTypeId;
  }

  public String getFishTypeName() {
    return fishTypeName;
  }

  public void setFishTypeName(String fishTypeName) {
    this.fishTypeName = fishTypeName;
  }

  public double getPlanOutFishCount() {
    return planOutFishCount;
  }

  public void setPlanOutFishCount(double planOutFishCount) {
    this.planOutFishCount = planOutFishCount;
  }

  public double getPlanOutFishWeight() {
    return planOutFishWeight;
  }

  public void setPlanOutFishWeight(double planOutFishWeight) {
    this.planOutFishWeight = planOutFishWeight;
  }

  public double getRealOutFishCount() {
    return realOutFishCount;
  }

  public void setRealOutFishCount(double realOutFishCount) {
    this.realOutFishCount = realOutFishCount;
  }

  public double getRealOutFishWeight() {
    return realOutFishWeight;
  }

  public void setRealOutFishWeight(double realOutFishWeight) {
    this.realOutFishWeight = realOutFishWeight;
  }

  public double getOutFishProcess() {
    return outFishProcess;
  }

  public void setOutFishProcess(double outFishProcess) {
    this.outFishProcess = outFishProcess;
  }
}
