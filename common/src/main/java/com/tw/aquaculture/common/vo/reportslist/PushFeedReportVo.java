package com.tw.aquaculture.common.vo.reportslist;

import java.util.Date;

/**
 * 饲料投喂报表vo
 * @author 陈荣
 * @date 2020/1/7 9:32
 */
public class PushFeedReportVo {

  /**公司id*/
  private String companyId;

  /**基地id*/
  private String baseId;

  /**塘口id*/
  private String pondId;

  /**饲料id*/
  private String feedId;

  /**公司名称*/
  private String companyName;

  /**基地名称*/
  private String baseName;

  /**塘口名称*/
  private String pondName;

  /**饲料名称*/
  private String feedName;

  /**投喂次数*/
  private Integer count;

  /**投喂量*/
  private Double amount;

  /**投喂量（吨）*/
  private Double amountT;

  /**开始时间*/
  private Date startTime;

  /**结束时间*/
  private Date endTime;

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getBaseId() {
    return baseId;
  }

  public void setBaseId(String baseId) {
    this.baseId = baseId;
  }

  public String getPondId() {
    return pondId;
  }

  public void setPondId(String pondId) {
    this.pondId = pondId;
  }

  public String getFeedId() {
    return feedId;
  }

  public void setFeedId(String feedId) {
    this.feedId = feedId;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getBaseName() {
    return baseName;
  }

  public void setBaseName(String baseName) {
    this.baseName = baseName;
  }

  public String getPondName() {
    return pondName;
  }

  public void setPondName(String pondName) {
    this.pondName = pondName;
  }

  public String getFeedName() {
    return feedName;
  }

  public void setFeedName(String feedName) {
    this.feedName = feedName;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Double getAmountT() {
    return amountT;
  }

  public void setAmountT(Double amountT) {
    this.amountT = amountT;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }
}
