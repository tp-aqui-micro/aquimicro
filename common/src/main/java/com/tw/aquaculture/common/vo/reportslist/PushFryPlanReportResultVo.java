package com.tw.aquaculture.common.vo.reportslist;

/**
 * Created by chenrong on 2020/1/28
 */
public class PushFryPlanReportResultVo {
  /**年度*/
  private String yearly;

  /**月度*/
  private String monthly;

  /**公司id*/
  private String companyId;

  /**公司名称*/
  private String companyName;

  /**基地id*/
  private String baseId;

  /**基地名称*/
  private String baseName;

  /**塘口id*/
  private String pondId;

  /**塘口名称*/
  private String pondName;

  /**鱼种id*/
  private String fishTypeId;

  /**鱼种名称*/
  private String fishTypeName;

  /**计划出鱼数量*/
  private double planPushFryCount;

  /**计划出鱼重量*/
  private double planPushFryWeight;

  /**实际出鱼数量*/
  private double realPushFryCount;

  /**实际出鱼重量*/
  private double realPushFryWeight;

  /**出鱼进度*/
  private double pushFryProcess;

  /**交通费*/
  private double transportFee;

  public double getTransportFee() {
    return transportFee;
  }

  public void setTransportFee(double transportFee) {
    this.transportFee = transportFee;
  }

  public String getYearly() {
    return yearly;
  }

  public void setYearly(String yearly) {
    this.yearly = yearly;
  }

  public String getMonthly() {
    return monthly;
  }

  public void setMonthly(String monthly) {
    this.monthly = monthly;
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getBaseId() {
    return baseId;
  }

  public void setBaseId(String baseId) {
    this.baseId = baseId;
  }

  public String getBaseName() {
    return baseName;
  }

  public void setBaseName(String baseName) {
    this.baseName = baseName;
  }

  public String getPondId() {
    return pondId;
  }

  public void setPondId(String pondId) {
    this.pondId = pondId;
  }

  public String getPondName() {
    return pondName;
  }

  public void setPondName(String pondName) {
    this.pondName = pondName;
  }

  public String getFishTypeId() {
    return fishTypeId;
  }

  public void setFishTypeId(String fishTypeId) {
    this.fishTypeId = fishTypeId;
  }

  public String getFishTypeName() {
    return fishTypeName;
  }

  public void setFishTypeName(String fishTypeName) {
    this.fishTypeName = fishTypeName;
  }

  public double getPlanPushFryCount() {
    return planPushFryCount;
  }

  public void setPlanPushFryCount(double planPushFryCount) {
    this.planPushFryCount = planPushFryCount;
  }

  public double getPlanPushFryWeight() {
    return planPushFryWeight;
  }

  public void setPlanPushFryWeight(double planPushFryWeight) {
    this.planPushFryWeight = planPushFryWeight;
  }

  public double getRealPushFryCount() {
    return realPushFryCount;
  }

  public void setRealPushFryCount(double realPushFryCount) {
    this.realPushFryCount = realPushFryCount;
  }

  public double getRealPushFryWeight() {
    return realPushFryWeight;
  }

  public void setRealPushFryWeight(double realPushFryWeight) {
    this.realPushFryWeight = realPushFryWeight;
  }

  public double getPushFryProcess() {
    return pushFryProcess;
  }

  public void setPushFryProcess(double pushFryProcess) {
    this.pushFryProcess = pushFryProcess;
  }
}
