package com.tw.aquaculture.common.vo.reportslist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * 报表查询条件
 * @author 陈荣
 * @date 2020/1/7 14:16
 */
@ApiModel(value = "报表查询条件")
public class ReportParamVo {

  /**开始时间*/
  @ApiModelProperty(name = "startTime", value = "开始时间")
  private Date startTime;

  /**结束时间*/
  @ApiModelProperty(name = "endTime", value = "结束时间")
  private Date endTime;

  /**鱼种id*/
  @ApiModelProperty(name = "fishTypeId", value = "鱼种id")
  private String fishTypeId;

  /**基地id*/
  @ApiModelProperty(name = "baseId", value = "接地id")
  private String baseId;

  /**塘口id*/
  @ApiModelProperty(name = "pondId", value = "塘口id")
  private String pondId;

  /**物料id*/
  @ApiModelProperty(name = "matterId", value = "物料id")
  private String matterId;

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public String getFishTypeId() {
    return fishTypeId;
  }

  public void setFishTypeId(String fishTypeId) {
    this.fishTypeId = fishTypeId;
  }

  public String getBaseId() {
    return baseId;
  }

  public void setBaseId(String baseId) {
    this.baseId = baseId;
  }

  public String getPondId() {
    return pondId;
  }

  public void setPondId(String pondId) {
    this.pondId = pondId;
  }

  public String getMatterId() {
    return matterId;
  }

  public void setMatterId(String matterId) {
    this.matterId = matterId;
  }
}
