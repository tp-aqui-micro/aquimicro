package com.tw.aquaculture.common.vo.reportslist;

import java.util.Date;

/**
 * 动保使用报表
 * @author 陈荣
 * @date 2020/1/7 16:05
 */
public class SafeReportVo {

  /**公司id*/
  private String companyId;

  /**基地id*/
  private String baseId;

  /**塘口id*/
  private String pondId;

  /**药品id*/
  private String medicineId;

  /**公司名称*/
  private String companyName;

  /**基地名称*/
  private String baseName;

  /**塘口名称*/
  private String pondName;

  /**药品名称*/
  private String medicineName;

  /**使用次数*/
  private Integer count;

  /**使用量*/
//  private Double amount;

  /**使用量（主单位）*/
  private Double amountT;

  /**主单位*/
  private String hostUnit;

  /**辅单位使用量*/
  private double amountA;

  /**辅单位*/
  private String assistUnit;

  /**单位*/
  private String unit;

  /**开始时间*/
  private Date startTime;

  /**结束时间*/
  private Date endTime;

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getBaseId() {
    return baseId;
  }

  public void setBaseId(String baseId) {
    this.baseId = baseId;
  }

  public String getPondId() {
    return pondId;
  }

  public void setPondId(String pondId) {
    this.pondId = pondId;
  }

  public String getMedicineId() {
    return medicineId;
  }

  public void setMedicineId(String medicineId) {
    this.medicineId = medicineId;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getBaseName() {
    return baseName;
  }

  public void setBaseName(String baseName) {
    this.baseName = baseName;
  }

  public String getPondName() {
    return pondName;
  }

  public void setPondName(String pondName) {
    this.pondName = pondName;
  }

  public String getMedicineName() {
    return medicineName;
  }

  public void setMedicineName(String medicineName) {
    this.medicineName = medicineName;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  /*public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }*/

  public Double getAmountT() {
    return amountT;
  }

  public void setAmountT(Double amountT) {
    this.amountT = amountT;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public String getHostUnit() {
    return hostUnit;
  }

  public void setHostUnit(String hostUnit) {
    this.hostUnit = hostUnit;
  }

  public double getAmountA() {
    return amountA;
  }

  public void setAmountA(double amountA) {
    this.amountA = amountA;
  }

  public String getAssistUnit() {
    return assistUnit;
  }

  public void setAssistUnit(String assistUnit) {
    this.assistUnit = assistUnit;
  }
}
