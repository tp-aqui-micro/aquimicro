package com.tw.aquaculture.common.vo.reportslist;

import java.util.Date;

/**
 * 库存报表查询条件
 * Created by chenrong on 2020/1/16
 */
public class StockReportParamVo {

  /**公司id*/
  private String companyId;

  /**基地id*/
  private String baseId;

  /**仓库id*/
  private String storeId;

  /**物料id*/
  private String matterId;

  /**物料类型id*/
  private String matterTypeCode;

  /**时间*/
  private Date time;

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getBaseId() {
    return baseId;
  }

  public void setBaseId(String baseId) {
    this.baseId = baseId;
  }

  public String getStoreId() {
    return storeId;
  }

  public void setStoreId(String storeId) {
    this.storeId = storeId;
  }

  public String getMatterId() {
    return matterId;
  }

  public void setMatterId(String matterId) {
    this.matterId = matterId;
  }

  public String getMatterTypeCode() {
    return matterTypeCode;
  }

  public void setMatterTypeCode(String matterTypeCode) {
    this.matterTypeCode = matterTypeCode;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }
}
