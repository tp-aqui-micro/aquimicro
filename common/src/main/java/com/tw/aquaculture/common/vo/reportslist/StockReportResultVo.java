package com.tw.aquaculture.common.vo.reportslist;

/**
 * 库存报表结果vo
 * Created by chenrong on 2020/1/16
 */
public class StockReportResultVo {

  //基地id
  private String baseId;
  //基地名称
  private String baseName;
  //物料类型编码
  private String matterTypeCode;
  //物料类型名称
  private String matterTypeName;
  //物料id
  private String matterId;
  //物料名称
  private String matterName;
  //主单位期初库存量
  private double hostEarlyStockAmount;
  //期初存唐主单位
  private String hostEarlyStockUnit;
  //辅单位期初库存量
  private double assistEarlyStockAmount;
  //期初库存量辅单位
  private String assistEarlyStockUnit;
  //主单位入库量
  private double hostInStoreAmount;
  //入库量主单位
  private String hostInStoreUnit;
  //辅单位入库量
  private String assistInStoreAmount;
  //入库量辅单位
  private String assistInStoreUnit;
  //主单位出库量
  private double hostOutStoreAmount;
  //出库量主单位
  private String hostOutStoreUnit;
  //辅单位出库量
  private double assistOutStoreAmount;
  //出库量辅单位
  private String assistOutStoreUnit;
  //主单位期末库存量
  private double hostStockAmount;
  //期末库存量主单位
  private String hostStockUnit;
  //辅单位期末库存量
  private double assistStockAmount;
  //期末库存量辅单位
  private String assistStockUnit;

  public String getBaseId() {
    return baseId;
  }

  public void setBaseId(String baseId) {
    this.baseId = baseId;
  }

  public String getBaseName() {
    return baseName;
  }

  public void setBaseName(String baseName) {
    this.baseName = baseName;
  }

  public String getMatterTypeCode() {
    return matterTypeCode;
  }

  public void setMatterTypeCode(String matterTypeCode) {
    this.matterTypeCode = matterTypeCode;
  }

  public String getMatterTypeName() {
    return matterTypeName;
  }

  public void setMatterTypeName(String matterTypeName) {
    this.matterTypeName = matterTypeName;
  }

  public String getMatterId() {
    return matterId;
  }

  public void setMatterId(String matterId) {
    this.matterId = matterId;
  }

  public String getMatterName() {
    return matterName;
  }

  public void setMatterName(String matterName) {
    this.matterName = matterName;
  }

  public double getHostEarlyStockAmount() {
    return hostEarlyStockAmount;
  }

  public void setHostEarlyStockAmount(double hostEarlyStockAmount) {
    this.hostEarlyStockAmount = hostEarlyStockAmount;
  }

  public String getHostEarlyStockUnit() {
    return hostEarlyStockUnit;
  }

  public void setHostEarlyStockUnit(String hostEarlyStockUnit) {
    this.hostEarlyStockUnit = hostEarlyStockUnit;
  }

  public double getAssistEarlyStockAmount() {
    return assistEarlyStockAmount;
  }

  public void setAssistEarlyStockAmount(double assistEarlyStockAmount) {
    this.assistEarlyStockAmount = assistEarlyStockAmount;
  }

  public String getAssistEarlyStockUnit() {
    return assistEarlyStockUnit;
  }

  public void setAssistEarlyStockUnit(String assistEarlyStockUnit) {
    this.assistEarlyStockUnit = assistEarlyStockUnit;
  }

  public double getHostInStoreAmount() {
    return hostInStoreAmount;
  }

  public void setHostInStoreAmount(double hostInStoreAmount) {
    this.hostInStoreAmount = hostInStoreAmount;
  }

  public String getHostInStoreUnit() {
    return hostInStoreUnit;
  }

  public void setHostInStoreUnit(String hostInStoreUnit) {
    this.hostInStoreUnit = hostInStoreUnit;
  }

  public String getAssistInStoreAmount() {
    return assistInStoreAmount;
  }

  public void setAssistInStoreAmount(String assistInStoreAmount) {
    this.assistInStoreAmount = assistInStoreAmount;
  }

  public String getAssistInStoreUnit() {
    return assistInStoreUnit;
  }

  public void setAssistInStoreUnit(String assistInStoreUnit) {
    this.assistInStoreUnit = assistInStoreUnit;
  }

  public double getHostOutStoreAmount() {
    return hostOutStoreAmount;
  }

  public void setHostOutStoreAmount(double hostOutStoreAmount) {
    this.hostOutStoreAmount = hostOutStoreAmount;
  }

  public String getHostOutStoreUnit() {
    return hostOutStoreUnit;
  }

  public void setHostOutStoreUnit(String hostOutStoreUnit) {
    this.hostOutStoreUnit = hostOutStoreUnit;
  }

  public double getAssistOutStoreAmount() {
    return assistOutStoreAmount;
  }

  public void setAssistOutStoreAmount(double assistOutStoreAmount) {
    this.assistOutStoreAmount = assistOutStoreAmount;
  }

  public String getAssistOutStoreUnit() {
    return assistOutStoreUnit;
  }

  public void setAssistOutStoreUnit(String assistOutStoreUnit) {
    this.assistOutStoreUnit = assistOutStoreUnit;
  }

  public double getHostStockAmount() {
    return hostStockAmount;
  }

  public void setHostStockAmount(double hostStockAmount) {
    this.hostStockAmount = hostStockAmount;
  }

  public String getHostStockUnit() {
    return hostStockUnit;
  }

  public void setHostStockUnit(String hostStockUnit) {
    this.hostStockUnit = hostStockUnit;
  }

  public double getAssistStockAmount() {
    return assistStockAmount;
  }

  public void setAssistStockAmount(double assistStockAmount) {
    this.assistStockAmount = assistStockAmount;
  }

  public String getAssistStockUnit() {
    return assistStockUnit;
  }

  public void setAssistStockUnit(String assistStockUnit) {
    this.assistStockUnit = assistStockUnit;
  }
}
