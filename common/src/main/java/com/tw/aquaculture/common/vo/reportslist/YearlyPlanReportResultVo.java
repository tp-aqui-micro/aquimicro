package com.tw.aquaculture.common.vo.reportslist;

import java.util.Date;

/**
 * Created by chenrong on 2020/1/20
 */
public class YearlyPlanReportResultVo {

  /**年度*/
  private String yearly;

  /**公司id*/
  private String companyId;

  /**公司名称*/
  private String companyName;

  /**基地id*/
  private String baseId;

  /**基地名称*/
  private String baseName;

  /**塘库id*/
  private String pondId;

  /**塘口名称*/
  private String pondName;

  /**品种id*/
  private String fishTypeId;

  /**品种名称*/
  private String getFishTypeName;

  /**计划投苗数量*/
  private double planPushFryCount;

  /**计划投苗重量*/
  private double planPushFryWeight;

  /**计划出鱼数量*/
  private double planOutFishCount;

  /**计划出鱼重量*/
  private double planOutFishWeight;

  /**实际投苗数量*/
  private double realPushFryCount;

  /**实际投苗重量*/
  private double realPushFryWeight;

  /**实际出鱼数量*/
  private double realOutFishCount;

  /**实际出鱼重量*/
  private double realOutFishWeight;

  /**投苗进度*/
  private double pushFryProgress;

  /**出鱼进度*/
  private double outFishProgress;

  public String getYearly() {
    return yearly;
  }

  public void setYearly(String yearly) {
    this.yearly = yearly;
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public String getBaseId() {
    return baseId;
  }

  public void setBaseId(String baseId) {
    this.baseId = baseId;
  }

  public String getBaseName() {
    return baseName;
  }

  public void setBaseName(String baseName) {
    this.baseName = baseName;
  }

  public String getPondId() {
    return pondId;
  }

  public void setPondId(String pondId) {
    this.pondId = pondId;
  }

  public String getPondName() {
    return pondName;
  }

  public void setPondName(String pondName) {
    this.pondName = pondName;
  }

  public String getFishTypeId() {
    return fishTypeId;
  }

  public void setFishTypeId(String fishTypeId) {
    this.fishTypeId = fishTypeId;
  }

  public String getGetFishTypeName() {
    return getFishTypeName;
  }

  public void setGetFishTypeName(String getFishTypeName) {
    this.getFishTypeName = getFishTypeName;
  }

  public double getPlanPushFryCount() {
    return planPushFryCount;
  }

  public void setPlanPushFryCount(double planPushFryCount) {
    this.planPushFryCount = planPushFryCount;
  }

  public double getPlanPushFryWeight() {
    return planPushFryWeight;
  }

  public void setPlanPushFryWeight(double planPushFryWeight) {
    this.planPushFryWeight = planPushFryWeight;
  }

  public double getPlanOutFishCount() {
    return planOutFishCount;
  }

  public void setPlanOutFishCount(double planOutFishCount) {
    this.planOutFishCount = planOutFishCount;
  }

  public double getPlanOutFishWeight() {
    return planOutFishWeight;
  }

  public void setPlanOutFishWeight(double planOutFishWeight) {
    this.planOutFishWeight = planOutFishWeight;
  }

  public double getRealPushFryCount() {
    return realPushFryCount;
  }

  public void setRealPushFryCount(double realPushFryCount) {
    this.realPushFryCount = realPushFryCount;
  }

  public double getRealPushFryWeight() {
    return realPushFryWeight;
  }

  public void setRealPushFryWeight(double realPushFryWeight) {
    this.realPushFryWeight = realPushFryWeight;
  }

  public double getRealOutFishCount() {
    return realOutFishCount;
  }

  public void setRealOutFishCount(double realOutFishCount) {
    this.realOutFishCount = realOutFishCount;
  }

  public double getRealOutFishWeight() {
    return realOutFishWeight;
  }

  public void setRealOutFishWeight(double realOutFishWeight) {
    this.realOutFishWeight = realOutFishWeight;
  }

  public double getPushFryProgress() {
    return pushFryProgress;
  }

  public void setPushFryProgress(double pushFryProgress) {
    this.pushFryProgress = pushFryProgress;
  }

  public double getOutFishProgress() {
    return outFishProgress;
  }

  public void setOutFishProgress(double outFishProgress) {
    this.outFishProgress = outFishProgress;
  }
}
