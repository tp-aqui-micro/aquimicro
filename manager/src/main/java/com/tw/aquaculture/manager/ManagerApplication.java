package com.tw.aquaculture.manager;

import com.bizunited.platform.kuiper.starter.annotations.EnableKuiperService;
import com.bizunited.platform.ui.annotations.EnableNebulaUI;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author chenr
 */
@SpringBootApplication(scanBasePackages = {"com.bizunited.platform.core","com.bizunited.platform.kuiper","com.tw.aquaculture.manager", "com.tw.aquaculture.common"})
@EnableKuiperService
@EnableFeignClients
//启动swagger配置
@EnableSwagger2
@EnableNebulaUI
@EnableJpaRepositories(basePackages = {"com.bizunited.platform.core.repository", "com.bizunited.platform.kuiper.starter.repository", "com.tw.aquaculture.manager.orgpondmap.repository","com.tw.aquaculture.manager.repository"})
@EntityScan(basePackages = {"com.bizunited.platform.core.entity", "com.bizunited.platform.kuiper.entity", "com.bizunited.platform.titan.entity", "com.bizunited.platform.proxima.entity", "com.tw.aquaculture.common.entity"})
public class ManagerApplication {
  public static void main(String[] args) {
    SpringApplication.run(ManagerApplication.class, args);
  }
}
