package com.tw.aquaculture.manager.configuration;

import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.ErrorPageRegistrar;
import org.springframework.boot.web.server.ErrorPageRegistry;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

@Configuration
public class ErrorPageConfig implements ErrorPageRegistrar {

    @Override
    public void registerErrorPages(ErrorPageRegistry registry) {
        //设定404页面
        ErrorPage errorPage = new ErrorPage(HttpStatus.NOT_FOUND,"/mobile/redirect.html");
        registry.addErrorPages(errorPage);
    }
}
