package com.tw.aquaculture.manager.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.EncodedResourceResolver;

import java.util.concurrent.TimeUnit;

@Configuration
@ComponentScan(
    basePackages = {"com.tw.aquaculture.manager"}
)
public class MobileAndReportUIConfig implements WebMvcConfigurer {
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    // 移动端h5页面支持
    this.registryPath(registry, "/mobile/**", "classpath:/META-INF/mobile/");
    this.registryPath(registry, "/mobile/css/**", "classpath:/META-INF/mobile/css/");
    this.registryPath(registry, "/mobile/fonts/**", "classpath:/META-INF/mobile/fonts/");
    this.registryPath(registry, "/mobile/images/**", "classpath:/META-INF/mobile/images/");
    this.registryPath(registry, "/mobile/resource/**", "classpath:/META-INF/mobile/resource/");
    this.registryPath(registry, "/mobile/resource/img/**", "classpath:/META-INF/mobile/resource/img/");
    this.registryPath(registry, "/mobile/scripts/**", "classpath:/META-INF/mobile/scripts/");

    // report报表页面支持
    this.registryPath(registry, "/report/**", "classpath:/META-INF/report/");
    this.registryPath(registry, "/report/css/**", "classpath:/META-INF/report/css/");
    this.registryPath(registry, "/report/resource/**", "classpath:/META-INF/report/resource/");
    this.registryPath(registry, "/report/resource/img/**", "classpath:/META-INF/report/resource/img/");
    this.registryPath(registry, "/report/scripts/**", "classpath:/META-INF/report/scripts/");
  }
  
  private void registryPath (ResourceHandlerRegistry registry , String handlerLocal , String locations) {
    registry.addResourceHandler(handlerLocal)
    .addResourceLocations(locations)
    // 开启 http304
    .setCacheControl(CacheControl.maxAge(30, TimeUnit.MINUTES).cachePrivate().cachePublic())
    .setCachePeriod(1800)
    .resourceChain(false)
    // 开启gzip
    .addResolver(new EncodedResourceResolver());
  }
}
