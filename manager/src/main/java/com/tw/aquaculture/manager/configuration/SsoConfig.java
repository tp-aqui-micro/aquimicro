package com.tw.aquaculture.manager.configuration;
import com.bizunited.platform.rbac.security.starter.service.security.CustomUserSecurityDetailsService;
import com.bizunited.platform.rbac.security.starter.service.security.SimpleAuthenticationDetailsSource;
import com.tw.aquaculture.manager.configuration.beans.LoginRequestHeaderAuthenticationFilter;
import com.tw.aquaculture.manager.configuration.beans.PreAuthAuthenticationManager;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;


import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import com.bizunited.platform.rbac.server.crypto.password.Aes2PasswordEncoder;
import com.bizunited.platform.rbac.server.service.UserService;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.CorsUtils;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.bizunited.platform.rbac.security.starter.handle.SimpleAuthenticationFailureHandler;
import com.bizunited.platform.rbac.security.starter.handle.SimpleAuthenticationSuccessHandler;
import com.bizunited.platform.rbac.security.starter.service.security.CustomAccessDecisionManager;
import com.bizunited.platform.rbac.security.starter.service.security.CustomFilterInvocationSecurityMetadataSource;
import com.bizunited.platform.rbac.security.starter.service.security.CustomFilterSecurityInterceptor;
import com.google.common.collect.Lists;
/**
 * 和访问权限有关的配置信息在这里
 * @author yinwenjie
 */
@Configuration
@EnableAutoConfiguration
@EnableWebSecurity
@Order(50)
@ComponentScan(basePackages= {"com.bizunited.platform.rbac","com.bizunited.platform.core"})
public class SsoConfig extends WebSecurityConfigurerAdapter {
  /**
   * 当单点认证信息在request header中时用该属性：身份信息key
   */
  @Value("${rbac.preAuthPrincipalRequestHeader:}")
  private String preAuthPrincipalRequestHeader;
  /**
   * 当单点认证信息在request header中时用该属性：凭证证明信息key
   */
  @Value("${rbac.preAuthCredentialsRequestHeader:}")
  private String preAuthCredentialsRequestHeader;
  
  /**
   * 忽略权限判断的url
   */
  @Value("${rbac.ignoreUrls}")
  private String[] ignoreUrls;
  /**
   * 登录操作地址
   */
  @Value("${rbac.loginUrl}")
  private String loginUrl;
  /**
   * 登录成功跳转页面
   */
  @Value("${rbac.loginPageUrl:}")
  private String loginPageUrl;
  /**
   * 登出操作地址
   */
  @Value("${rbac.logoutUrl}")
  private String logoutUrl;
  /**
   * 登出成功后跳转页面
   */
  @Value("${rbac.logoutSuccessRedirect:}")
  private String logoutSuccessRedirect;
  @Autowired
  private PreAuthAuthenticationManager preAuthAuthenticationManager;
  @Autowired
  private AuthenticationSuccessHandler authenticationSuccessHandler;
  @Autowired
  private AuthenticationFailureHandler authenticationFailureHandler;
  @Autowired
  private LogoutSuccessHandler logoutSuccessHandler;
  @Autowired
  private AuthenticationDetailsSource<HttpServletRequest, WebAuthenticationDetails> authenticationDetailsSource;
  @Autowired
  private AccessDecisionManager accessDecisionManager;
  @Autowired
  private AuthenticationProvider authenticationProvider;
  @Autowired
  private FilterInvocationSecurityMetadataSource securityMetadataSource;
  @Autowired
  private UserService uerService;
  /**
   * 那些系统默认的忽略权限判定的地址
   */
  public static final String[] DEFAULT_IGNOREURLS = new String[] {"/default/**","/static/**","/v1/kuiper/templatevisibilities/findDetailsByTemplateId" , "/v1/kuiper/templates/findByConditions" , "/v1/nebula/checkCodes" , "/v1/nebula/validateCodes"};

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    if(!StringUtils.isAnyBlank(preAuthPrincipalRequestHeader,preAuthCredentialsRequestHeader)){
      LoginRequestHeaderAuthenticationFilter filter = new LoginRequestHeaderAuthenticationFilter(uerService);
      filter.setAuthenticationManager(preAuthAuthenticationManager);
      filter.setAuthenticationDetailsSource(authenticationDetailsSource);
      filter.setPrincipalRequestHeader(preAuthPrincipalRequestHeader);
      filter.setCredentialsRequestHeader(preAuthCredentialsRequestHeader);
      http.addFilter(filter);
    }
    /*
     * 自定义的过滤器，这个过滤器的意思是：
     * 1、确定访问的资源具备什么样的角色访问权限(securityMetadataSource) ->
     * 2、根据当前用户具备的哪些角色进行比对，最终确定是否能够访问指定的资源(accessDecisionManager) ->
     * 3、authenticationManager是投票策略，spring security支持多种投票策略，有多数派、有全部通过，那么这里默认使用的投票器是“只做一次投票”
     * 4、关于ignoreUrls的信息，还要增加系统默认就不加入功能权限的几个路径，例如：验证码和校验码的路径
     * */
    CustomFilterSecurityInterceptor filterSecurityInterceptor = new CustomFilterSecurityInterceptor(securityMetadataSource, accessDecisionManager, super.authenticationManager());
    CorsConfigurationSource configurationSource = this.corsConfigurationSource();
//    // 无访问权限时的处理器
//    SimpleAccessDeniedHandler simpleAccessDeniedHandler = new SimpleAccessDeniedHandler(this);
    // 在忽略权限判断的设置中，增加几个系统默认的路径
    ArrayList<String> currentIgnoreUrls = new ArrayList<>();
    if(ignoreUrls != null && ignoreUrls.length > 0) {
      currentIgnoreUrls.addAll(Lists.newArrayList(ignoreUrls));
    }
    currentIgnoreUrls.addAll(Lists.newArrayList(DEFAULT_IGNOREURLS));
    if(StringUtils.isNotBlank(logoutSuccessRedirect)) {
      currentIgnoreUrls.add(logoutSuccessRedirect);
    }
    if(StringUtils.isNotBlank(loginPageUrl)) {
      currentIgnoreUrls.add(loginPageUrl);
    }
    http
      .addFilterAt(filterSecurityInterceptor,FilterSecurityInterceptor.class)
      // 允许登录操作时跨域
      .cors().configurationSource(configurationSource).and()
      // 允许iframe嵌入
      .headers().frameOptions().disable().and()
      // 设定显示jsession设定信息
      .sessionManagement()
      .enableSessionUrlRewriting(true).and()
      .authorizeRequests()
      //对preflight放行
      .requestMatchers(CorsUtils::isPreFlightRequest).permitAll()
      // 系统中的“登录页面”在被访问时，不进行权限控制
      .antMatchers(currentIgnoreUrls.toArray(new String[] {})).permitAll()
      // 其它访问都要验证权限
      .anyRequest().authenticated().and()
      // =================== 无权限访问资源或者用户登录信息过期时，会出发这个异常处理器
//      .exceptionHandling()
//      .authenticationEntryPoint(simpleAccessDeniedHandler)
//      .accessDeniedHandler(simpleAccessDeniedHandler).and()
      // ==================== 设定登录页的url地址，它不进行权限控制
      .formLogin()
      // 由于后端提供的都是restful接口，并没有直接跳转的页面
      // 所以只要访问的url没有通过权限认证，就跳到这个请求上，并直接排除权限异常
      .loginPage("/v1/rbac/loginFail")
      // 登录请求点
      .loginProcessingUrl(loginUrl)
      // 一旦权限验证成功，则执行这个处理器
      .successHandler(this.authenticationSuccessHandler)
      // 一旦权限验证过程出现错误，则执行这个处理器
      .failureHandler(this.authenticationFailureHandler)
      // 自定义的登录表单信息（默认的登录表单结构只有两个字段，用户名和输入的密码）
      .authenticationDetailsSource(this.authenticationDetailsSource)
      // 由于使用前后端分离时，设定了failureHandler，所以failureForwardUrl就不需要设定了
      // 由于使用前后端分离时，设定了successHandler，所以successForwardUrl就不需要设定了
      .permitAll().and()
      // ===================== 设定登出后的url地址
      .logout()
      // 登出页面
      .logoutUrl(this.logoutUrl)
      .logoutSuccessHandler(logoutSuccessHandler).permitAll().and()
      // 由于使用前后端分离时，设定了logoutSuccessHandler，所以logoutSuccessUrl就不需要设定了
      // 登录成功后
      // ===================== 关闭csrf
      .csrf()
      .disable()
      .rememberMe()
      // 持久化登录信息，登录时间为100天
      .tokenValiditySeconds(100 * 24 * 60 * 60)
      .rememberMeCookieName("persistence")
      .alwaysRemember(true);
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(authenticationProvider);
  }
  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    // 设置密码加密模式
    auth.userDetailsService(this.getUserDetailsService()).passwordEncoder(passwordEncoder());
  }

  @Bean("simpleAuthenticationSuccessHandler")
  public AuthenticationSuccessHandler getAuthenticationSuccessHandler() {
    return new SimpleAuthenticationSuccessHandler();
  }

  @Bean("simpleAuthenticationFailureHandler")
  public AuthenticationFailureHandler getAuthenticationFailureHandler() {
    return new SimpleAuthenticationFailureHandler();
  }

  /**
   * 自定义UserDetailsService，从数据库中读取用户信息
   * @return
   */
  @Bean(name="userDetailsService")
  public UserDetailsService getUserDetailsService() {
    return new CustomUserSecurityDetailsService();
  }
  /**
   * 用户密码的默认加密方式为PBKDF2加密
   * @return
   */
  @Bean(name="passwordEncoder")
  public PasswordEncoder passwordEncoder() {
    return new Pbkdf2PasswordEncoder();
  }

  /**
   * 用户密码采用AES-CBC方式加密
   * @return
   */
  @Bean(name = "aes2PasswordEncoder")
  public Aes2PasswordEncoder aes2PasswordEncoder() {
    return new Aes2PasswordEncoder();
  }

  @Bean(name="customAccessDecisionManager")
  public AccessDecisionManager getCustomAccessDecisionManager() {
    return new CustomAccessDecisionManager();
  }

  @Bean(name="customFilterInvocationSecurityMetadataSource")
  public FilterInvocationSecurityMetadataSource getCustomFilterInvocationSecurityMetadataSource() {
    return new CustomFilterInvocationSecurityMetadataSource();
  }
  /**
   * 该接口用来定义登录请求的数据获取方式
   * @return
   */
  @Bean(name="authenticationDetailsSource")
  public AuthenticationDetailsSource<HttpServletRequest, WebAuthenticationDetails> getAuthenticationDetailsSource() {
    return new SimpleAuthenticationDetailsSource();
  }

  @Bean
  public PreAuthAuthenticationManager getPreAuthAuthenticationManager(UserDetailsService userDetailsService){
    PreAuthenticatedAuthenticationProvider provider = new PreAuthenticatedAuthenticationProvider();
    provider.setPreAuthenticatedUserDetailsService(new UserDetailsByNameServiceWrapper<>(this.getUserDetailsService()));
    return new PreAuthAuthenticationManager(provider, userDetailsService);
  }

  /**
   * 获取登录页面地址
   * @return
   */
  public String getLoginPageUrl() {
    return loginPageUrl;
  }

  /**
   * 获取登出成功后的跳转地址
   * @return
   */
  public String getLogoutSuccessRedirect() {
    return logoutSuccessRedirect;
  }

  /**
   * 配置权限的跨域操作
   * @return
   */
  private CorsConfigurationSource corsConfigurationSource() {
    CorsConfiguration configuration = new CorsConfiguration();
    configuration.setAllowedOrigins(Arrays.asList("*"));
    configuration.setAllowedMethods(Arrays.asList("*"));
    configuration.setAllowedHeaders(Arrays.asList("*"));
    configuration.setAllowCredentials(true);
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", configuration);
    return source;
  }
}
