//package com.tw.aquaculture.manager.configuration;
//
//import com.bizunited.platform.core.controller.BaseController;
//import com.bizunited.platform.rbac.server.service.UserService;
//import com.bizunited.platform.rbac.server.vo.UserVo;
//import io.swagger.annotations.ApiOperation;
//import org.apache.commons.lang3.StringUtils;
//import org.apache.commons.lang3.Validate;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.web.savedrequest.DefaultSavedRequest;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import java.io.IOException;
//import java.util.Arrays;
//import java.util.Set;
//import java.util.stream.Collectors;
//
//@RestController
//@RequestMapping("/sign")
//public class WebSignController extends BaseController {
//  /**
//   * 当单点认证信息在request header中时用该属性：身份信息key
//   */
//  @Value("${rbac.preAuthPrincipalRequestHeader:}")
//  private String preAuthPrincipalRequestHeader;
//
//  @Value("${rbac.appWelcomeUrl:}")
//  private String appWelcomeUrl;
//
//  @Autowired
//  private UserService userService;
//
//  @Value("${server.servlet.context-path:}")
//  private String contextPath = "";
//
//  /**
//   * 日志
//   */
//  private static final Logger LOGGER = LoggerFactory.getLogger(WebSignController.class);
//
//  /**
//   * 当登陆成功后，默认跳转到这个URL，并且返回登录成功后的用户基本信息"
//   *       + "一旦登录成功，服务端将会向客户端返回两个重要属性：<br>"
//   *       + "1、SESSION属性：该属性可保证从最后一次服务器请求开始的30分钟内登录信息有效(前提是服务节点没有重启)<br>"
//   *       + "2、persistence属性：该属性可保证从最后一次登录操作开始的100天内登录信息有效(前提是服务节点没有重启)<br>"
//   *       + "客户端应该至少保证在进行HTTP请求时，向服务器传递persistence属性。但为了保证服务端业务处理性能，应该两个属性都进行传递。<br>"
//   *       + "<b>请注意：正常情况下SESSION属性可能发生变化，一旦变化客户端应该向服务端传递最新的SESSION属性</b>"
//   *       + "</p>"
//   *       + "一旦登录成功或者通过persistence自动重登录成功后，服务端将通过以下两种方式向客户端返回新的SESSION属性和persistence属性：<br>"
//   *       + "1、http response的header信息中，将携带。类似于：persistence =YWRtaW46MTUxNDUxNzA4MjYzNjplYzI0OTFlYWEyNDhkZmIyZWIyNjNjODc3YzM2M2Q0MA 和 SESSION =54fd02c7-4067-43c9-94f8-5f6e474cd858<br>"
//   *       + "2、http response的cookies信息中，将携带。（此种方式是推荐使用的方式）<br>"
//   *       + "<b>注意：以上描述只限于登录成功后的返回信息，并不是说每一次业务请求操作，服务端都会向客户端这样返回SESSION属性和persistence属性</b>"
//   *       + "</p>"
//   *       + "为了保证服务端能够正确识别到客户端已登录的用户权限信息，在正常的前端请求过程中，每一次客户端的请求都需要向服务端发送SESSION属性（非必要，但推荐）和persistence属性<br>"
//   *       + "客户端可以使用以下方式，向服务端发送SESSION属性和persistence属性：<br>"
//   *       + "1、直接使用http request的cookies发送。（此种方式是推荐使用的方式）<br>
//   * @return
//   */
//  @ApiOperation(value="单点登录跳转路径")
//  @RequestMapping(value="/test" , method={RequestMethod.GET , RequestMethod.POST})
//  public void loginSuccess(HttpServletRequest request , HttpServletResponse response) {
//    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//    Validate.notNull(authentication , "为获取到当前系统的登录人");
//    Object userPrincipalObject = authentication.getPrincipal();
//    Validate.notNull(userPrincipalObject, "错误的权限组件!!");
//
//    // 由于前段模块的要求，在用户登录成功，或者通过persistence自动重登录成功后，
//    // 后端服务都需要将cookies中重新设定的persistence和JSESSIONID以自定义属性的形式写入到head中。
//    Cookie[] cookies = request.getCookies();
//    String jsession = null;
//    if(cookies != null) {
//      Set<String> cookieNames = Arrays.stream(cookies).map(Cookie::getName).collect(Collectors.toSet());
//      Validate.isTrue(cookieNames.contains("OAMAuthnHintCookie"),"非法的访问");
//      response.setHeader("Access-Control-Expose-Headers", "JSESSIONID,persistence,Cookie");
//    }
//
//    String account = request.getHeader(preAuthPrincipalRequestHeader);
//    //查询本地数据库是否有该用户，若没有，则保存至本地
//    UserVo localUser = userService.findByAccount(account.toLowerCase());
//    //如果h5 未找请求头未找到携带的用户信息，则采用通威用户id来寻找数据是否存在；
//    if(null == localUser){
//      localUser =  userService.findByUserId(account);
//    }
////    if(null == localUser) {这里需要自己实现从第三方系统获取用户信息，如果第三方也获取不到该用户，则抛错
////      UserVo userVo = casUserService.findByThird(logUser.getName());
////      if(userVo!=null){
////        如果从第三方系统获取到了该用户信息，则在本地创建该用户
////        userService.create(userVo);
////      }
////    }
//    Validate.notNull(localUser,"用户不存在，请检查!!");
//
//    String url = request.getRequestURL().toString().replace("http://","");
//    String[] splitArr = url.split("/");
//    String defaultRedirectUrl = String.format("http://%s%s/%s",splitArr[0],request.getContextPath(),appWelcomeUrl);
//
//    // 确认前端最终跳转页面(优先从session中确认)
//    HttpSession session = request.getSession();
//    DefaultSavedRequest defaultSavedRequest = (DefaultSavedRequest)session.getAttribute("SPRING_SECURITY_SAVED_REQUEST");
//    String currentRedirectUrl = "";
//    if(defaultSavedRequest == null) {
//      currentRedirectUrl = defaultRedirectUrl;
//    } else {
//      String query = defaultSavedRequest.getQueryString();
//      String requestURI = defaultSavedRequest.getRequestURI();
//      if(StringUtils.isBlank(query)) {
//        currentRedirectUrl = requestURI;
//      } else {
//        currentRedirectUrl = StringUtils.join(requestURI , "?" , query);
//      }
//    }
//
//    Cookie redirectCookie = new Cookie("redirectUrl", currentRedirectUrl);
//    redirectCookie.setPath("/");
//    response.addCookie(redirectCookie);
//    // 确认当前用户账户名
//    Cookie accountCookie = new Cookie("account", account.toLowerCase());
//    accountCookie.setPath("/");
//    response.addCookie(accountCookie);
//
//    // 从spring session取出相关信息——如果Security没有取到
//    if(StringUtils.isBlank(jsession)) {
//      jsession = session.getId();
//      Cookie cookie = new Cookie("JSESSIONID", jsession);
//      cookie.setPath("/");
//      response.addCookie(cookie);
//      cookie = new Cookie("persistence", jsession);
//      cookie.setPath("/");
//      response.addCookie(cookie);
//    }
//    response.setHeader("JSESSIONID", jsession);
//
//    // 去取可能的Spring boot 2.x支持的context-path位置，以便在存在nginx代理的情况下，前端页面能够正确的请求。
//    Cookie contextPathCookie = new Cookie("context-path", contextPath);
//    contextPathCookie.setPath("/");
//    response.addCookie(contextPathCookie);
//
//    // 直接构造跳转页面内容
//    try {
//      response.setHeader("content-type", "text/html;charset=UTF-8");
//      response.getWriter().write(writeSsoPage().toString());
//    } catch (IOException e) {
//      LOGGER.error(e.getMessage() , e);
//      throw new RuntimeException(e);
//    }
//  }
//
//  private StringBuffer writeSsoPage() {
//    StringBuffer pageContext = new StringBuffer();
//
//    pageContext.append("<!DOCTYPE html>").append(System.lineSeparator());
//    pageContext.append("<html>").append(System.lineSeparator());
//    pageContext.append("<head>").append(System.lineSeparator());
//    pageContext.append("<title>鉴权跳转</title>").append(System.lineSeparator());
//    pageContext.append("</head>").append(System.lineSeparator());
//    pageContext.append("<body>").append(System.lineSeparator());
//    pageContext.append("<div id='authMsg'>正在鉴权,请等待...</div>").append(System.lineSeparator());
//    pageContext.append("<script>").append(System.lineSeparator());
//    pageContext.append("//设置消息").append(System.lineSeparator());
//    pageContext.append("function setAuthMsg(msg){").append(System.lineSeparator());
//    pageContext.append("document.getElementById(\"authMsg\").innerHTML=msg;").append(System.lineSeparator());
//    pageContext.append("}").append(System.lineSeparator());
//    pageContext.append("//设置storage").append(System.lineSeparator());
//    pageContext.append("function setStorage(session,persistence,account){").append(System.lineSeparator());
//    pageContext.append("var curStorage = window.localStorage;").append(System.lineSeparator());
//    pageContext.append("var formEngineData = {").append(System.lineSeparator());
//    pageContext.append("session:session,").append(System.lineSeparator());
//    pageContext.append("persistence:persistence").append(System.lineSeparator());
//    pageContext.append("}").append(System.lineSeparator());
//    pageContext.append("curStorage.setItem(\"formEngineData\", JSON.stringify(formEngineData));").append(System.lineSeparator());
//    pageContext.append("var loginInfo = {").append(System.lineSeparator());
//    pageContext.append("id:account,").append(System.lineSeparator());
//    pageContext.append("account:account").append(System.lineSeparator());
//    pageContext.append("}").append(System.lineSeparator());
//    pageContext.append("curStorage.setItem(\"formEngineLoginInfo\",JSON.stringify(loginInfo));").append(System.lineSeparator());
//    pageContext.append("//写入主机头到localStorage 供API调用").append(System.lineSeparator());
//    pageContext.append("var domain = getCookie(\"context-path\");").append(System.lineSeparator());
//    pageContext.append("if(domain){").append(System.lineSeparator());
//    pageContext.append("curStorage.setItem(\"formEngineDomain\",JSON.stringify({Domain:domain}));").append(System.lineSeparator());
//    pageContext.append("}").append(System.lineSeparator());
//    pageContext.append("}").append(System.lineSeparator());
//    pageContext.append("//localstorage注入表单引擎数据").append(System.lineSeparator());
//    pageContext.append("//读取当前cookie信息").append(System.lineSeparator());
//    pageContext.append("function getCookie(name){").append(System.lineSeparator());
//    pageContext.append("var arr,reg=new RegExp(\"(^| )\"+name+\"=([^;]*)(;|$)\"); //正则匹配").append(System.lineSeparator());
//    pageContext.append("if(arr=document.cookie.match(reg)){").append(System.lineSeparator());
//    pageContext.append("return unescape(arr[2]);").append(System.lineSeparator());
//    pageContext.append("}else{").append(System.lineSeparator());
//    pageContext.append("return \"\";").append(System.lineSeparator());
//    pageContext.append("}").append(System.lineSeparator());
//    pageContext.append("} ").append(System.lineSeparator());
//    pageContext.append("(function(){").append(System.lineSeparator());
//    pageContext.append("//读取 sessionid").append(System.lineSeparator());
//    pageContext.append("var sessionId = getCookie(\"JSESSIONID\")").append(System.lineSeparator());
//    pageContext.append("//读取persistence ").append(System.lineSeparator());
//    pageContext.append("var persistence = getCookie(\"persistence\");").append(System.lineSeparator());
//    pageContext.append("//读取 登录账户信息").append(System.lineSeparator());
//    pageContext.append("var account = getCookie(\"account\");").append(System.lineSeparator());
//    pageContext.append("if(!sessionId || !persistence ||!account){").append(System.lineSeparator());
//    pageContext.append("setAuthMsg('鉴权失败! 请<a href=\"javascript:back();\">返回重新登录</a>');").append(System.lineSeparator());
//    pageContext.append("return;").append(System.lineSeparator());
//    pageContext.append("}").append(System.lineSeparator());
//    pageContext.append("//写入当前数据到localStorage").append(System.lineSeparator());
//    pageContext.append("setStorage(sessionId,persistence,account);").append(System.lineSeparator());
//    pageContext.append("//获得跳转回的路径").append(System.lineSeparator());
//    pageContext.append("var redirectUrl = getCookie(\"redirectUrl\");").append(System.lineSeparator());
//    pageContext.append("if(!redirectUrl){").append(System.lineSeparator());
//    pageContext.append("setAuthMsg(\"鉴权成功! 但未指定需要跳转的页面。\");").append(System.lineSeparator());
//    pageContext.append("return;").append(System.lineSeparator());
//    pageContext.append("}").append(System.lineSeparator());
//    pageContext.append("//跳转回表单引擎或者指定页面").append(System.lineSeparator());
//    pageContext.append("window.location.href=redirectUrl;").append(System.lineSeparator());
//    pageContext.append("})();").append(System.lineSeparator());
//    pageContext.append("</script>").append(System.lineSeparator());
//    pageContext.append("</body>").append(System.lineSeparator());
//    pageContext.append("</html>").append(System.lineSeparator());
//
//    return pageContext;
//  }
//}
