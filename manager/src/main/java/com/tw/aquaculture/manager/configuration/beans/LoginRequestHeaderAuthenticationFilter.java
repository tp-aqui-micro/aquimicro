package com.tw.aquaculture.manager.configuration.beans;

import com.bizunited.platform.rbac.server.service.UserService;
import com.bizunited.platform.rbac.server.vo.UserVo;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;

import java.util.List;

/**
 * 由于Oracle商业套件提供的"单点登陆"不是标准协议的单点，所以实现Spring Security提供的AbstractPreAuthenticatedProcessingFilter
 * 解决从header头部信息中提取用户信息用于身份认证
 */
public class LoginRequestHeaderAuthenticationFilter extends AbstractPreAuthenticatedProcessingFilter {
  /**
   * 用户的身份信息
   */
  private String principalRequestHeader;
  private UserService uerService;

  public LoginRequestHeaderAuthenticationFilter(UserService uerService) {
    this.uerService = uerService;
  }

  /**
   * 业务系统实现的从request header中获取的用户信息
   * @param request
   * @return
   */
  @Override
  protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
    String account = request.getHeader(this.principalRequestHeader);
    UserVo currentUser = null;
    if(StringUtils.isBlank(account)){
       String userName = request.getParameter("source");
       if (StringUtils.isNotBlank(userName) ) {
         currentUser = this.h5PreAuthenticated(userName);
       }
    } else {
      currentUser = this.uerService.findByAccount(account);
    }
    
    if(currentUser == null) {
      return null;
    }
    return currentUser.getAccount();
  }

  private UserVo h5PreAuthenticated(String username) {
    List<UserVo> users = this.uerService.findByAccountLikeOrNameLike(username);
    Validate.notEmpty(users, "非法的访问");
    Validate.isTrue(users.size() == 1, "当前账号异常， 请联系水产管理员（发现多个重复用户名!!）");
    UserVo user = users.get(0);
    return user;
  }

  /**
   * 业务系统实现的从request header中获取的用户身份凭证证明信息
   * @param request
   * @return
   */
  @Override
  protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
    return this.getPreAuthenticatedPrincipal(request);
  }

  public void setPrincipalRequestHeader(String principalRequestHeader) {
    Assert.hasText(principalRequestHeader, "用户认证信息不能为空！");
    this.principalRequestHeader = principalRequestHeader;
  }

  public void setCredentialsRequestHeader(String credentialsRequestHeader) {
    Assert.hasText(credentialsRequestHeader, "用户凭证证明不能为空！");
  }
}
