package com.tw.aquaculture.manager.configuration.beans;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

/**
 * 提供的身份预验证的管理器，其中PreAuthenticatedAuthenticationProvider(身份验证实现)是Spring Security默认提供的
 */
public class PreAuthAuthenticationManager implements AuthenticationManager {

  private PreAuthenticatedAuthenticationProvider provider;
  
  private UserDetailsService userDetailsService;

  public PreAuthAuthenticationManager(PreAuthenticatedAuthenticationProvider provider , UserDetailsService userDetailsService){
    this.provider = provider;
    this.userDetailsService = userDetailsService;
  }

  /**
   * 用户身份认证
   * @param authentication
   * @return
   * @throws AuthenticationException
   */
  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    UserDetailsByNameServiceWrapper<PreAuthenticatedAuthenticationToken> udb = new UserDetailsByNameServiceWrapper<>();
    udb.setUserDetailsService(this.userDetailsService);
    provider.setPreAuthenticatedUserDetailsService(udb);
    
    return provider.authenticate(authentication);
  }  
}
