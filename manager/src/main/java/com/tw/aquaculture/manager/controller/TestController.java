package com.tw.aquaculture.manager.controller;

import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.manager.feign.plan.YearlyPlanFeign;
import com.tw.aquaculture.manager.orgpondmap.service.OrgPondMapService;
import com.tw.aquaculture.manager.service.remote.CodeGenerateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陈荣
 * @date 2019/12/4 20:25
 */
@RestController
@RequestMapping("/test")
public class TestController {

  @Autowired
  YearlyPlanFeign yearlyPlanFeign;
  @Autowired
  private OrgPondMapService orgPondMapService;

  @Autowired
  private CodeGenerateService codeGenerateService;


  @GetMapping("/test")
  public Object test() {
   String str = codeGenerateService.generateBusinessCode(CodeTypeEnum.POSITION_ENTITY);

   System.out.println(str);
    return yearlyPlanFeign.findById("e401a7ba-30f2-432a-9071-e0cb4fe8aaf3");
  }

  @GetMapping("/test2")
  public void test2() {
    orgPondMapService.flush();
  }
}
