package com.tw.aquaculture.manager.controller.base;

import com.alibaba.fastjson.JSONObject;
import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.kuiper.entity.TemplateEntity;
import com.bizunited.platform.kuiper.entity.TemplateItemEntity;
import com.bizunited.platform.kuiper.entity.TemplateRelationEntity;
import com.bizunited.platform.kuiper.starter.repository.TemplateItemRepository;
import com.bizunited.platform.kuiper.starter.repository.TemplateRelationRepository;
import com.bizunited.platform.kuiper.starter.repository.TemplateRepository;
import com.tw.aquaculture.manager.feign.plan.YearlyPlanFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 陈荣
 * @date 2019/12/3 15:10
 */
@RestController
@RequestMapping("/in")
public class InitController extends BaseController {
  
  private static final String ERROR = "error";
  private static final String SUCESS = "sucess";
  private static final String OLD_PATH = "tongwei.aquaculture.service";
  private static final String NEW_PATH = "tw.aquaculture.common";

  @Autowired
  private TemplateRepository templateRepository;

  @Autowired
  private TemplateItemRepository templateItemRepository;

  @Autowired
  private TemplateRelationRepository templateRelationRepository;

  @Autowired
  YearlyPlanFeign yearlyPlanFeign;
  
  @GetMapping("/changeTemplateScript")
  public Object changeTemplateScript(){
    
    List<TemplateEntity> templateEntityList = templateRepository.findAll();
    for (TemplateEntity t : templateEntityList) {
      String v = t.getCode();
      v = v.replace(OLD_PATH, NEW_PATH);
      String s = t.getPersistentClassName();
      s = s.replace(OLD_PATH, NEW_PATH);
      t.setCode(v);
      t.setPersistentClassName(s);
    }
    templateRepository.saveAll(templateEntityList);

    List<TemplateItemEntity> items = templateItemRepository.findAll();
    for (TemplateItemEntity i : items) {
      String a = i.getParentClassName();
      String b = i.getPkage();
      String c = i.getPropertyClassName();
      a = a.replace(OLD_PATH, NEW_PATH);
      b = b.replace(OLD_PATH, NEW_PATH);
      c = c.replace(OLD_PATH, NEW_PATH);
      i.setParentClassName(a);
      i.setPkage(b);
      i.setPropertyClassName(c);
    }
    templateItemRepository.saveAll(items);

    List<TemplateRelationEntity> rs = templateRelationRepository.findAll();
    for (TemplateRelationEntity r : rs) {
      String a = r.getPropertyClassName();
      a = a.replace(OLD_PATH, NEW_PATH);
      r.setPropertyClassName(a);
    }
    templateRelationRepository.saveAll(rs);

    //修改文件
    File fullPathFile = new File("D:/usr/appfile/layout");
    if (!fullPathFile.exists()) {
      return ERROR;
    }
    File[] files = fullPathFile.listFiles();
    for (File file : files) {
      Object content = readFile(file);
      String s = content.toString();
      if (s.startsWith("{")) {
        try {
          if(s.contains("com.tongwei.aquaculture.service")){
            System.out.println(file.getName());
          }
          if (s.contains("=")) {
            Map<Object, Object> obj = (Map<Object, Object>) content;
            JSONObject jsonObject = (JSONObject) JSONObject.toJSON(obj);
            String c = jsonObject.toJSONString();
            c = c.replace("com.tongwei.aquaculture.service", "com.tw.aquaculture.common");
            JSONObject w = JSONObject.parseObject(c);
            writeFile(w, file);
          } else {
            s = s.replace("com.tongwei.aquaculture.service", "com.tw.aquaculture.common");
            JSONObject m = JSONObject.parseObject(s);
            writeFile(m, file);
          }
        }catch (Exception e){
          System.out.println(content);
          System.out.println(file.getName());
          e.printStackTrace();
          return ERROR;
        }
      }
    }


    return SUCESS;
  }

  private static void writeFile(JSONObject content, File file) {
    byte[] fileContext = writeObjectToBytes(content);
    try (FileOutputStream fileOut = new FileOutputStream(file);
         ByteArrayInputStream byteIn = new ByteArrayInputStream(fileContext);) {
      int maxLen = 204800;
      byte[] readContext = new byte[maxLen];
      int realen;
      while ((realen = byteIn.read(readContext, 0, maxLen)) != -1) {
        fileOut.write(readContext, 0, realen);
      }
    } catch (IllegalStateException | IOException e) {
      throw new IllegalArgumentException(e.getMessage());
    }
  }

  private static byte[] writeObjectToBytes(JSONObject content) {
    byte[] bytes = null;
    try (ByteArrayOutputStream out = new ByteArrayOutputStream();
         ObjectOutputStream sOut = new ObjectOutputStream(out);) {
      sOut.writeObject(content);
      sOut.flush();
      bytes = out.toByteArray();
    } catch (IOException e) {
      return new byte[]{};
    }
    return bytes;
  }

  private static Object readFile(File file) {
    byte[] fileContents = null;
    try (FileInputStream ois = new FileInputStream(file);
         ByteArrayOutputStream out = new ByteArrayOutputStream()) {
      int maxLen = 204800;
      byte[] readContext = new byte[maxLen];
      int realen;
      while ((realen = ois.read(readContext, 0, maxLen)) != -1) {
        out.write(readContext, 0, realen);
      }
      fileContents = out.toByteArray();
      return readBytesToString(fileContents);
    } catch (Exception e) {
      throw new IllegalArgumentException(e);
    }
  }

  private static Object readBytesToString(byte[] bytes) {
    Object layoutObject = null;
    try (ByteArrayInputStream in = new ByteArrayInputStream(bytes);
         ObjectInputStream sIn = new ObjectInputStream(in);) {
      layoutObject = sIn.readObject();
    } catch (Exception e) {
      return null;
    }
    if (layoutObject == null) {
      return null;
    }

    return layoutObject;
  }
}
