package com.tw.aquaculture.manager.controller.base;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.bizunited.platform.rbac.server.service.PositionService;
import com.bizunited.platform.rbac.server.vo.PositionVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 岗位补充接口
 * @author 陈荣
 * @date 2020/1/10 10:13
 */
@Api(tags = "岗位信息补充接口")
@RestController
@RequestMapping(value = "v1/common/position")
public class PositionViewController extends BaseController {

  @Autowired
  PositionService positionService;

  @ApiOperation(value = "根据用户id查询当前登录人当前岗位")
  @GetMapping("/findMainPositionByUserId")
  public ResponseModel findMainPositionByUserId(@ApiParam("用户id") String userId){
    try {
      PositionVo result = this.positionService.findMainPositionByUserId(userId);
      return this.buildHttpResultW(result, new String[]{});
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }
}
