/*
package com.tw.aquaculture.manager.controller.base;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.base.ShareModelItemEntity;
import com.tw.aquaculture.manager.service.base.ShareModelItemEntityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

*/
/**
 * ShareModelItemEntity业务模型的MVC Controller层实现，基于HTTP Restful风格
 *
 * @author saturn
 * <p>
 * 日志
 * <p>
 * 相关的创建过程，http接口。请注意该创建过程除了可以创建shareModelItemEntity中的基本信息以外，还可以对shareModelItemEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（ShareModelItemEntity）模型的创建操作传入的shareModelItemEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
 * <p>
 * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（ShareModelItemEntity）的修改操作传入的shareModelItemEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
 * <p>
 * 按照ShareModelItemEntity实体中的（fishType）关联的 鱼种进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
 * @param fishType 关联的 鱼种
 * <p>
 * 按照ShareModelItemEntity实体中的（shareModel）关联的 分摊模式进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
 * @param shareModel 关联的 分摊模式
 * <p>
 * 按照ShareModelItemEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
 * @param id 主键
 *//*

@RestController
@RequestMapping("/v1/shareModelItemEntitys")
public class ShareModelItemEntityController extends BaseController {
  */
/**
 * 日志
 *//*

  private static final Logger LOGGER = LoggerFactory.getLogger(ShareModelItemEntityController.class);
  
  @Autowired
  private ShareModelItemEntityService shareModelItemEntityService;

  */
/**
 * 相关的创建过程，http接口。请注意该创建过程除了可以创建shareModelItemEntity中的基本信息以外，还可以对shareModelItemEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（ShareModelItemEntity）模型的创建操作传入的shareModelItemEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
 * *//*

  @ApiOperation(value = "相关的创建过程，http接口。请注意该创建过程除了可以创建shareModelItemEntity中的基本信息以外，还可以对shareModelItemEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（ShareModelItemEntity）模型的创建操作传入的shareModelItemEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value="")
  public ResponseModel create(@RequestBody @ApiParam(name="shareModelItemEntity" , value="相关的创建过程，http接口。请注意该创建过程除了可以创建shareModelItemEntity中的基本信息以外，还可以对shareModelItemEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（ShareModelItemEntity）模型的创建操作传入的shareModelItemEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") ShareModelItemEntity shareModelItemEntity) {
    try {
      ShareModelItemEntity current = this.shareModelItemEntityService.create(shareModelItemEntity);
      return this.buildHttpResultW(current, new String[]{});
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  */
/**
 * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（ShareModelItemEntity）的修改操作传入的shareModelItemEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
 * *//*

  @ApiOperation(value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（ShareModelItemEntity）的修改操作传入的shareModelItemEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PatchMapping(value="")
  public ResponseModel update(@RequestBody @ApiParam(name="shareModelItemEntity" , value="相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（ShareModelItemEntity）的修改操作传入的shareModelItemEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") ShareModelItemEntity shareModelItemEntity) {
    try {
      ShareModelItemEntity current = this.shareModelItemEntityService.update(shareModelItemEntity);
      return this.buildHttpResultW(current, new String[]{});
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  */
/**
 * 按照ShareModelItemEntity实体中的（fishType）关联的 鱼种进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
 * @param fishType 关联的 鱼种
 *//*

  @ApiOperation(value = "按照ShareModelItemEntity实体中的（fishType）关联的 鱼种进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @RequestMapping(value="/findDetailsByFishType" , method={RequestMethod.GET})
  public ResponseModel findDetailsByFishType(@RequestParam("fishType") @ApiParam("关联的 鱼种") String fishType) {
    try { 
      Set<ShareModelItemEntity> result = this.shareModelItemEntityService.findDetailsByFishType(fishType);
      return this.buildHttpResultW(result, new String[]{"shareModel","fishType"});
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    } 
  }  
  */
/**
 * 按照ShareModelItemEntity实体中的（shareModel）关联的 分摊模式进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
 * @param shareModel 关联的 分摊模式
 *//*

  @ApiOperation(value = "按照ShareModelItemEntity实体中的（shareModel）关联的 分摊模式进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @RequestMapping(value="/findDetailsByShareModel" , method={RequestMethod.GET})
  public ResponseModel findDetailsByShareModel(@RequestParam("shareModel") @ApiParam("关联的 分摊模式") String shareModel) {
    try { 
      Set<ShareModelItemEntity> result = this.shareModelItemEntityService.findDetailsByShareModel(shareModel);
      return this.buildHttpResultW(result, new String[]{"shareModel","fishType"});
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    } 
  }  
  */
/**
 * 按照ShareModelItemEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
 * @param id 主键
 *//*

  @ApiOperation(value = "按照ShareModelItemEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @RequestMapping(value="/findDetailsById" , method={RequestMethod.GET})
  public ResponseModel findDetailsById(@RequestParam("id") @ApiParam("主键") String id) {
    try { 
      ShareModelItemEntity result = this.shareModelItemEntityService.findDetailsById(id);
      return this.buildHttpResultW(result, new String[]{"shareModel","fishType"});
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    } 
  }  
} 
*/
