package com.tw.aquaculture.manager.controller.base;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.manager.service.remote.TwBaseEntityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * TwBaseEntity业务模型的MVC Controller层实现，基于HTTP Restful风格
 *
 * @author saturn
 */
@RestController
@RequestMapping("/v1/twBaseEntitys")
public class TwBaseEntityController extends BaseController {
  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(TwBaseEntityController.class);

  @Autowired
  private TwBaseEntityService twBaseEntityService;

  /**
   * 相关的创建过程，http接口。请注意该创建过程除了可以创建twBaseEntity中的基本信息以外，还可以对twBaseEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（TwBaseEntity）模型的创建操作传入的twBaseEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(value = "相关的创建过程，http接口。请注意该创建过程除了可以创建twBaseEntity中的基本信息以外，还可以对twBaseEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（TwBaseEntity）模型的创建操作传入的twBaseEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "")
  public ResponseModel create(@RequestBody @ApiParam(name = "twBaseEntity", value = "相关的创建过程，http接口。请注意该创建过程除了可以创建twBaseEntity中的基本信息以外，还可以对twBaseEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（TwBaseEntity）模型的创建操作传入的twBaseEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") TwBaseEntity twBaseEntity) {
    try {
      TwBaseEntity current = this.twBaseEntityService.create(twBaseEntity);
      return this.buildHttpResultW(current, new String[]{});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（TwBaseEntity）的修改操作传入的twBaseEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（TwBaseEntity）的修改操作传入的twBaseEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PatchMapping(value = "")
  public ResponseModel update(@RequestBody @ApiParam(name = "twBaseEntity", value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（TwBaseEntity）的修改操作传入的twBaseEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") TwBaseEntity twBaseEntity) {
    try {
      TwBaseEntity current = this.twBaseEntityService.update(twBaseEntity);
      return this.buildHttpResultW(current, new String[]{});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照TwBaseEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照TwBaseEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @RequestMapping(value = "/findDetailsById", method = {RequestMethod.GET})
  public ResponseModel findDetailsById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      TwBaseEntity result = this.twBaseEntityService.findDetailsById(id);
      return this.buildHttpResultW(result, new String[]{"leader", "baseOrg", "ponds", "ponds.technician", "ponds.tgroup", "ponds.groupLeader", "ponds.worker"});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照TwBaseEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param formInstanceId 表单实例编号
   */
  @ApiOperation(value = "按照TwBaseEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @RequestMapping(value = "/findDetailsByFormInstanceId", method = {RequestMethod.GET})
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") @ApiParam("表单实例编号") String formInstanceId) {
    try {
      TwBaseEntity result = this.twBaseEntityService.findDetailsByFormInstanceId(formInstanceId);
      return this.buildHttpResultW(result, new String[]{"leader", "baseOrg", "ponds", "ponds.technician", "ponds.tgroup", "ponds.groupLeader", "ponds.worker"});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照TwBaseEntity实体中的（formInstanceId）表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @ApiOperation(value = "按照TwBaseEntity实体中的（formInstanceId）表单实例编号进行查询")
  @RequestMapping(value = "/findByFormInstanceId", method = {RequestMethod.GET})
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") @ApiParam("表单实例编号") String formInstanceId) {
    try {
      TwBaseEntity result = this.twBaseEntityService.findByFormInstanceId(formInstanceId);
      return this.buildHttpResultW(result, new String[]{});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "查询所有")
  @RequestMapping(value = "/findAll", method = {RequestMethod.GET})
  public ResponseModel findAll() {
    try {
      List<TwBaseEntity> result = this.twBaseEntityService.findAll();
      return this.buildHttpResultW(result, new String[]{});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "查询所有")
  @RequestMapping(value = "/findDetailAll", method = {RequestMethod.GET})
  public ResponseModel findDetailAll() {
    try {
      List<TwBaseEntity> result = this.twBaseEntityService.findDetailAll();
      return this.buildHttpResultW(result, new String[]{"baseOrg", "leader", "ponds"});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据组织基地id查询详情")
  @RequestMapping(value = "/findDetailsByBaseOrgId", method = {RequestMethod.GET})
  public ResponseModel findDetailsByBaseOrgId(@ApiParam("组织基地id") String baseOrgId) {
    try {
      TwBaseEntity result = this.twBaseEntityService.findDetailsByBaseOrgId(baseOrgId);
      return this.buildHttpResultW(result, new String[]{"baseOrg", "leader", "ponds"});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据条件分页查询")
  @RequestMapping(value = "/findByConditions", method = {RequestMethod.GET})
  public ResponseModel findByConditions(@RequestParam("pageable") Pageable pageable, @RequestParam("params") Map<String, Object> params) {
    try {
      Page<TwBaseEntity> result = this.twBaseEntityService.findByConditions(pageable, params);
      return this.buildHttpResultW(result, new String[]{"baseOrg", "leader", "ponds"});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }
}