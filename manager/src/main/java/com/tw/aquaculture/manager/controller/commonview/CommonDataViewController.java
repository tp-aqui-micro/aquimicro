package com.tw.aquaculture.manager.controller.commonview;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.rbac.server.vo.PositionVo;
import com.google.common.collect.Maps;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.MatterEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.StoreEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.common.entity.check.CheckPondEntity;
import com.tw.aquaculture.common.entity.check.CheckWaterQualityEntity;
import com.tw.aquaculture.common.entity.process.OutApplyCustomStateEntity;
import com.tw.aquaculture.common.entity.process.OutFishRegistEntity;
import com.tw.aquaculture.common.entity.process.ProofYieldEntity;
import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import com.tw.aquaculture.common.vo.common.CheckPondCountBaseVo;
import com.tw.aquaculture.common.vo.common.DictItemVo;
import com.tw.aquaculture.common.vo.common.MatterBudgetVo;
import com.tw.aquaculture.manager.service.common.CommonDataViewService;
import com.tw.aquaculture.manager.service.process.WorkOrderEntityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author 陈荣
 * @date 2019/11/20 10:46
 */
@RestController
@RequestMapping(value = "/v1/common/data-view")
@Api(tags = "接口型数据视图", description = "提供给表单或者app选择manayToOne数据")
public class CommonDataViewController extends BaseController {

  @Autowired
  private CommonDataViewService commonDataViewService;

  @Autowired
  private WorkOrderEntityService workOrderEntityService;

  @ApiOperation(value = "获取字典")
  @GetMapping(value = "/getDicts")
  public ResponseModel getDicts(@RequestParam(name = "dictCodes") @ApiParam(name = "dictCodes", value = "字典编码，如果有多个，用逗号分隔") String dictCodes) {
    try {
      List<DictItemVo> current = this.commonDataViewService.findByCodes(dictCodes);
      return this.buildHttpResult(current);
    } catch (Exception e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "获取所有鱼种")
  @GetMapping(value = "/getFishTypes")
  public ResponseModel getFishTypes() {
    try {
      List<FishTypeEntity> current = this.commonDataViewService.findFishTypes();
      return this.buildHttpResult(current);
    } catch (Exception e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据物料类型编码分页查询物料")
  @GetMapping(value = "/getFishTypesByMatterTypeCode")
  public ResponseModel getFishTypesByMatterTypeCode(@ApiParam(name = "matterTypeCode", value = "物料类型编码") String matterTypeCode,
                                                    @ApiParam(name = "pageable", value = "分页参数") Pageable pageable) {
    try {
      Map<String, Object> params = Maps.newHashMap();
      params.put("matterTypeCode", matterTypeCode);
      Page<MatterEntity> current = this.commonDataViewService.findMattersByMatterConditions(params, pageable);
      return this.buildHttpResult(current);
    } catch (Exception e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据条件分页查询基地")
  @GetMapping(value = "/findBaseByConditions")
  public ResponseModel findBaseByConditions(@ApiParam(name = "userId", value = "用户id") String userId,
                                            @ApiParam(name = "pageable", value = "分页参数对象", required = false) Pageable pageable) {
    try {
      Map<String, Object> params = Maps.newHashMap();
      if (pageable == null) {
        pageable = PageRequest.of(0, 50);
      }
      Page<TwBaseEntity> current = this.commonDataViewService.findBaseByConditions(pageable, params);
      return this.buildHttpResultW(current, new String[]{"baseOrg", "ponds", "leader"});
    } catch (Exception e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据用户id查询基地(暂时未开通)")
  @GetMapping(value = "/findBaseByUserId")
  public ResponseModel findBaseByUserId(@ApiParam(name = "userId", value = "用户id") String userId) {
    try {
      List<TwBaseEntity> current = this.commonDataViewService.findBaseByUserId(userId);
      return this.buildHttpResultW(current, new String[]{"baseOrg", "ponds", "leader"});
    } catch (Exception e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value="根据岗位id查询基地")
  @GetMapping("/findBaseByPositionId")
  public ResponseModel findBaseByPositionId(@ApiParam(name = "positionId", value = "岗位id") String positionId) {
    try {
      List<TwBaseEntity> current = this.commonDataViewService.findBaseByPositionId(positionId);
      return this.buildHttpResultW(current, new String[]{"baseOrg", "ponds"});
    } catch (Exception e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据基地组织id查询下级所属人员")
  @GetMapping(value = "/findUserByBaseOrgId")
  public ResponseModel findUserByBaseOrgId(@ApiParam(name = "baseOrgId", value = "基地组织id") String baseOrgId) {
    try {
      List<UserEntity> current = this.commonDataViewService.findUserByBaseOrgId(baseOrgId);
      return this.buildHttpResultW(current, new String[]{"parent", "child", "position", "roles"});
    } catch (Exception e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据条件分页查询")
  @GetMapping(value = "/findWorkOrdersByConditions")
  public ResponseModel findByConditions(@ApiParam("塘口id") String pondId, @ApiParam("时间") Date time, Pageable pageable){
    try {
      Map<String, Object> params = org.gradle.internal.impldep.com.google.common.collect.Maps.newHashMap();
      params.put("pondId", pondId);
      params.put("time", time);
      Page<WorkOrderEntity> result = this.workOrderEntityService.findByConditions(pageable, params);
      return this.buildHttpResultW(result, new String[]{});
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "本周期内上次打样测产数据")
  @GetMapping("/findLastProofYield")
  public ResponseModel findLastProofYield(@ApiParam("塘口id") String pondId){
    try {
      ProofYieldEntity result = this.commonDataViewService.findLastProofYield(pondId);
      return this.buildHttpResultW(result, new String[]{"proofYieldItems"});
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "本周期内某鱼种上次打样测产数据")
  @GetMapping("/findLastProofYieldByFishType")
  public ResponseModel findLastProofYieldByFishType(@ApiParam("塘口id") String pondId, @ApiParam("鱼种id") String fishTypeId){
    try {
      ProofYieldEntity result = this.commonDataViewService.findLastProofYieldByFishType(pondId, fishTypeId);
      return this.buildHttpResultW(result, new String[]{});
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "查询当前存塘数量")
  @GetMapping("/findExitCount")
  public ResponseModel findExitCount(@ApiParam("塘口id") String pondId){
    try {
      double result = this.commonDataViewService.findExitCount(pondId);
      return this.buildHttpResult(result);
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据岀鱼申请查询客户")
  @GetMapping("/findCustomByOutFishApplyId")
  public ResponseModel findCustomByOutFishApplyId(@ApiParam("岀鱼申请id") String outFishApplyId, @ApiParam("-1：全部，0:未中标，1：中标") int state){
    try {
      List<OutApplyCustomStateEntity> result = this.commonDataViewService.findCustomByOutFishApplyId(outFishApplyId, state);
      return this.buildHttpResult(result);
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据用户id查询当前登录人所有岗位")
  @GetMapping("/findPositionByUserId")
  public ResponseModel findPositionByUserId(@ApiParam("用户id") String userId){
    try {
      List<PositionVo> result = this.commonDataViewService.findPositionByUserId(userId);
      return this.buildHttpResultW(result, new String[]{});
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据基地id查塘口")
  @GetMapping("/findPondsByBaseId")
  public ResponseModel findPondsByBaseId(@ApiParam("基地id") String baseId, @ApiParam("塘口状态，填-1时查所有") int enableState){
    try {
      List<PondEntity> result = this.commonDataViewService.findPondsByBaseId(baseId, enableState);
      return this.buildHttpResultW(result, new String[]{"base"});
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据塘口和人员姓名查询当月、当天巡塘次数")
  @GetMapping("/findCheckPondCount")
  public ResponseModel findCheckPondCount(@ApiParam("塘口id") String pondId, @ApiParam("username") String username){
    try {
      Map<String, Object> result = this.commonDataViewService.findCheckPondCountByPondAndUser(pondId, username);
      return this.buildHttpResult(result);
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据基地id查仓库")
  @GetMapping("/findStoresByBaseId")
  public ResponseModel findStoresByBaseId(@ApiParam("基地id") String baseId){
    try {
      List<StoreEntity> result = this.commonDataViewService.findStoresByBaseId(baseId);
      return this.buildHttpResultW(result, new String[]{"twBase", "twBase.baseOrg", "twBase.baseOrg.parent"});
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "查询上次水质检测")
  @GetMapping("/findLastCheckWaterByPondId")
  public ResponseModel findLastCheckWaterByPondId(@ApiParam("塘口id") String pondId){
    try {
      CheckWaterQualityEntity result = this.commonDataViewService.findLastCheckWaterByPondId(pondId);
      return this.buildHttpResultW(result, new String[]{"pond"});
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "查询上次巡塘检测")
  @GetMapping("/findLastCheckPondByPondId")
  public ResponseModel findLastCheckPondByPondId(@ApiParam("塘口id") String pondId){
    try {
      CheckPondEntity result = this.commonDataViewService.findLastCheckPondByPondId(pondId);
      return this.buildHttpResultW(result, new String[]{"pond"});
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "查询物料的本月消耗和库存量")
  @GetMapping("/findMatterBudgetByPondId")
  public ResponseModel findMatterBudget(){
    try {
      List<MatterBudgetVo> result = this.commonDataViewService.findMatterBudgetByPondId();
      return this.buildHttpResult(result);
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }
  @ApiOperation(value = "查询当前登陆人所有相关基地的所有塘口的当月、当天巡塘次数和所有鱼种规格、存塘重量")
  @GetMapping("/computeCheckPondCount")
  public ResponseModel computeCheckPondCount(@Param("当前登录人id") String userId){
    try {
      List<CheckPondCountBaseVo> result = this.commonDataViewService.computeCheckPondCount(userId);
      return this.buildHttpResult(result);
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "查询当前登陆人所有相关基地的所有塘口的当月、当天巡塘次数和所有鱼种规格、存塘重量")
  @GetMapping("/computeCheckPondCountByPositionId")
  public ResponseModel computeCheckPondCountByPositionId(@Param("当前登录人岗位") String positionId){
    try {
      List<CheckPondCountBaseVo> result = this.commonDataViewService.computeCheckPondCountByPositionId(positionId);
      return this.buildHttpResult(result);
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据岀鱼申请查询岀鱼登记")
  @GetMapping("/findOutFishRegistByOutFishApply")
  public ResponseModel findOutFishRegistByOutFishApply(@Param("岀鱼申请id") String outFishApplyId){
    try {
      List<OutFishRegistEntity> result = this.commonDataViewService.findOutFishRegistByOutFishApply(outFishApplyId);
      return this.buildHttpResultW(result, new String[]{"pond", "outFishInfos", "pond.base", "customer"});
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据基地查询组织基地")
  @GetMapping("/findBaseOrgByBaseId")
  public ResponseModel findBaseOrgByBaseId(@Param("基地id") String baseId){
    try {
      TwBaseEntity result = this.commonDataViewService.findBaseOrgByBaseId(baseId);
      return this.buildHttpResultW(result, new String[]{"baseOrg", "baseOrg.parent"});
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据塘口和鱼种查询本周期内存塘量")
  @GetMapping("/findExitCountByPondAndFishType")
  public ResponseModel findExitCountByPondAndFishType(@ApiParam("塘口id") String pondId, @ApiParam("鱼种id") String fishTypeId){
    try {
      double result = this.commonDataViewService.findExitCountByPondAndFishType(pondId, fishTypeId);
      return this.buildHttpResult(result);
    } catch(RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }


}
