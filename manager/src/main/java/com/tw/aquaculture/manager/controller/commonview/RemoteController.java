package com.tw.aquaculture.manager.controller.commonview;

import com.tw.aquaculture.common.vo.remote.ResponseVo;
import com.tw.aquaculture.common.vo.remote.saleapply.SaleRequestVo;
import com.tw.aquaculture.common.vo.remote.saleapply.SaleResponseDataVo;
import com.tw.aquaculture.manager.service.common.RemoteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;

/**
 * @author 陈荣
 * @date 2019/12/13 10:51
 */
@RestController
@RequestMapping("/v1/aquaculture")
@Api(tags = "提供给外部的接口")
public class RemoteController {

  @Autowired
  private RemoteService remoteService;

  /**
   * 岀鱼申请回调接口
   * @param saleRequestVo
   * @return
   */
  @PostMapping("/saleApplyCallback")
  @ApiModelProperty(value = "岀鱼申请回调")
  // TODO ResponseVo?
  public ResponseVo saleApplyCallback(@RequestBody(required = false) SaleRequestVo saleRequestVo) {

    try {
      if(saleRequestVo == null){
        return ResponseVo.fail(null, "没有获取到参数");
      }
      SaleResponseDataVo result = remoteService.saleApplyCallback(saleRequestVo);
      if (result != null) {
        return ResponseVo.success(result, saleRequestVo.getTransactionId());
      } else {
        return ResponseVo.fail(saleRequestVo.getTransactionId(), "业务处理失败");
      }
    }catch (RuntimeException e){
      return ResponseVo.error(e);
    } catch (ParseException e) {
      e.printStackTrace();
      return ResponseVo.error(e);
    }
  }

}
