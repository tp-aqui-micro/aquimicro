package com.tw.aquaculture.manager.controller.commonview;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.vo.reportslist.*;
import com.tw.aquaculture.manager.service.common.ReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 陈荣
 * @date 2020/1/7 10:02
 */
@Api(tags = "报表接口")
@RestController
@RequestMapping(value = "/v1/common/report")
public class ReportController extends BaseController {

  @Autowired
  private ReportService reportService;

  @ApiOperation(value = "饲料投喂报表")
  @PostMapping(value = "/pushFeedReport")
  public ResponseModel pushFeedReport(@RequestBody ReportParamVo reportParamVo) {
    try {
      String account = getPrincipalAccount();
      List<PushFeedReportVo> result = this.reportService.pushFeedReport(reportParamVo, account);
      return this.buildHttpResult(result);
    } catch (RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "动保使用报表")
  @PostMapping(value = "/safeReport")
  public ResponseModel safeReport(@RequestBody ReportParamVo reportParamVo) {
    try {
      String account = getPrincipalAccount();
      List<SafeReportVo> result = this.reportService.safeReport(reportParamVo, account);
      return this.buildHttpResult(result);
    } catch (RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "损鱼报表")
  @PostMapping(value = "/fishDamageReport")
  public ResponseModel fishDamageReport(@RequestBody ReportParamVo reportParamVo) {
    try {
      String account = getPrincipalAccount();
      List<FishDamageReportVo> result = this.reportService.fishDamageReport(reportParamVo, account);
      return this.buildHttpResult(result);
    } catch (RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "存塘量报表")
  @PostMapping(value = "/existAmountReport")
  public ResponseModel existAmountReport(@RequestBody ReportParamVo reportParamVo) {
    try {
      String account = getPrincipalAccount();
      List<ExistAmountReportVo> result = this.reportService.existAmountReport(reportParamVo, account);
      return this.buildHttpResult(result);
    } catch (RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "存塘量报表")
  @PostMapping(value = "/stockReport")
  public ResponseModel stockReport(@RequestBody StockReportParamVo stockReportParamVo) {
    try {
      String account = getPrincipalAccount();
      List<ExistAmountReportVo> result = this.reportService.stockReport(stockReportParamVo, account);
      return this.buildHttpResult(result);
    } catch (RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "转分塘报表")
  @PostMapping(value = "/dividePondReport")
  public ResponseModel dividePondReport(@RequestBody DividePondParamVo dividePondParamVo) {
    try {
      String account = getPrincipalAccount();
      List<DividePondResultVo> result = this.reportService.dividePondReport(dividePondParamVo, account);
      return this.buildHttpResult(result);
    } catch (RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "年度计划报表")
  @PostMapping(value = "/yearlyPlanReport")
  public ResponseModel yearlyPlanReport(@RequestBody YearlyPlanReportParamVo yearlyPlanReportParamVo) {
    try {
      String account = getPrincipalAccount();
      List<YearlyPlanReportResultVo> result = this.reportService.yearlyPlanReport(yearlyPlanReportParamVo, account);
      return this.buildHttpResult(result);
    } catch (RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "出鱼计划报表")
  @PostMapping(value = "/outFishPlanReport")
  public ResponseModel outFishPlanReport(@RequestBody OutFishPlanReportParamVo outFishPlanReportParamVo) {
    try {
      String account = getPrincipalAccount();
      List<OutFishPlanReportResultVo> result = this.reportService.outFishPlanReport(outFishPlanReportParamVo, account);
      return this.buildHttpResult(result);
    } catch (RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "投苗计划报表")
  @PostMapping(value = "/pushFryPlanReport")
  public ResponseModel pushFryPlanReport(@RequestBody PushFryPlanReportParamVo pushFryPlanReportParamVo) {
    try {
      String account = getPrincipalAccount();
      List<PushFryPlanReportResultVo> result = this.reportService.pushFryPlanReport(pushFryPlanReportParamVo, account);
      return this.buildHttpResult(result);
    } catch (RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

}
