package com.tw.aquaculture.manager.controller.process;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.process.MedicineFeedBackEntity;
import com.tw.aquaculture.manager.service.process.MedicineFeedBackEntityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * MedicineFeedBackEntity业务模型的MVC Controller层实现，基于HTTP Restful风格
 *
 * @author saturn
 */
@RestController
@RequestMapping("/v1/medicineFeedBackEntitys")
public class MedicineFeedBackEntityController extends BaseController {

  private String safeguard = "safeguard";
  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(MedicineFeedBackEntityController.class);

  @Autowired
  private MedicineFeedBackEntityService medicineFeedBackEntityService;

  /**
   * 相关的创建过程，http接口。请注意该创建过程除了可以创建medicineFeedBackEntity中的基本信息以外，还可以对medicineFeedBackEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（MedicineFeedBackEntity）模型的创建操作传入的medicineFeedBackEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(value = "相关的创建过程，http接口。请注意该创建过程除了可以创建medicineFeedBackEntity中的基本信息以外，还可以对medicineFeedBackEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（MedicineFeedBackEntity）模型的创建操作传入的medicineFeedBackEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "")
  public ResponseModel create(@RequestBody @ApiParam(name = "medicineFeedBackEntity", value = "相关的创建过程，http接口。请注意该创建过程除了可以创建medicineFeedBackEntity中的基本信息以外，还可以对medicineFeedBackEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（MedicineFeedBackEntity）模型的创建操作传入的medicineFeedBackEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") MedicineFeedBackEntity medicineFeedBackEntity) {
    try {
      MedicineFeedBackEntity current = this.medicineFeedBackEntityService.create(medicineFeedBackEntity);
      return this.buildHttpResultW(current, new String[]{});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（MedicineFeedBackEntity）的修改操作传入的medicineFeedBackEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（MedicineFeedBackEntity）的修改操作传入的medicineFeedBackEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PatchMapping(value = "")
  public ResponseModel update(@RequestBody @ApiParam(name = "medicineFeedBackEntity", value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（MedicineFeedBackEntity）的修改操作传入的medicineFeedBackEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") MedicineFeedBackEntity medicineFeedBackEntity) {
    try {
      MedicineFeedBackEntity current = this.medicineFeedBackEntityService.update(medicineFeedBackEntity);
      return this.buildHttpResultW(current, new String[]{});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照MedicineFeedBackEntity实体中的（parent）关联的 上次用药反馈进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param parent 关联的 上次用药反馈
   */
  @ApiOperation(value = "按照MedicineFeedBackEntity实体中的（parent）关联的 上次用药反馈进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @GetMapping(value = "/findDetailsByParent")
  public ResponseModel findDetailsByParent(@RequestParam("parent") @ApiParam("关联的 上次用药反馈") String parent) {
    try {
      Set<MedicineFeedBackEntity> result = this.medicineFeedBackEntityService.findDetailsByParent(parent);
      return this.buildHttpResultW(result, new String[]{safeguard});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照MedicineFeedBackEntity实体中的（safeguard）关联的 动保进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param safeguard 关联的 动保
   */
  @ApiOperation(value = "按照MedicineFeedBackEntity实体中的（safeguard）关联的 动保进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @RequestMapping(value = "/findDetailsBySafeguard", method = {RequestMethod.GET})
  public ResponseModel findDetailsBySafeguard(@RequestParam("safeguard") @ApiParam("关联的 动保") String safeguard) {
    try {
      Set<MedicineFeedBackEntity> result = this.medicineFeedBackEntityService.findDetailsBySafeguard(safeguard);
      return this.buildHttpResultW(result, new String[]{safeguard});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照MedicineFeedBackEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照MedicineFeedBackEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @RequestMapping(value = "/findDetailsById", method = {RequestMethod.GET})
  public ResponseModel findDetailsById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      MedicineFeedBackEntity result = this.medicineFeedBackEntityService.findDetailsById(id);
      return this.buildHttpResultW(result, new String[]{safeguard});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }
} 
