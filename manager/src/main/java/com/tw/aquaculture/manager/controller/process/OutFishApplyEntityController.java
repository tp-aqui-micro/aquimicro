package com.tw.aquaculture.manager.controller.process;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.process.OutApplyCustomStateEntity;
import com.tw.aquaculture.common.entity.process.OutFishApplyEntity;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.manager.service.process.OutFishApplyEntityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * OutFishApplyEntity业务模型的MVC Controller层实现，基于HTTP Restful风格
 *
 * @author saturn
 */
@RestController
@RequestMapping("/v1/outFishApplyEntitys")
public class OutFishApplyEntityController extends BaseController {
  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(OutFishApplyEntityController.class);

  @Autowired
  private OutFishApplyEntityService outFishApplyEntityService;

  /**
   * 相关的创建过程，http接口。请注意该创建过程除了可以创建outFishApplyEntity中的基本信息以外，还可以对outFishApplyEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（OutFishApplyEntity）模型的创建操作传入的outFishApplyEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(value = "相关的创建过程，http接口。请注意该创建过程除了可以创建outFishApplyEntity中的基本信息以外，还可以对outFishApplyEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（OutFishApplyEntity）模型的创建操作传入的outFishApplyEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "")
  public ResponseModel create(@RequestBody @ApiParam(name = "outFishApplyEntity", value = "相关的创建过程，http接口。请注意该创建过程除了可以创建outFishApplyEntity中的基本信息以外，还可以对outFishApplyEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（OutFishApplyEntity）模型的创建操作传入的outFishApplyEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") OutFishApplyEntity outFishApplyEntity) {
    try {
      OutFishApplyEntity current = this.outFishApplyEntityService.create(outFishApplyEntity);
      return this.buildHttpResultW(current, new String[]{});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（OutFishApplyEntity）的修改操作传入的outFishApplyEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（OutFishApplyEntity）的修改操作传入的outFishApplyEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PatchMapping(value = "")
  public ResponseModel update(@RequestBody @ApiParam(name = "outFishApplyEntity", value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（OutFishApplyEntity）的修改操作传入的outFishApplyEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") OutFishApplyEntity outFishApplyEntity) {
    try {
      OutFishApplyEntity current = this.outFishApplyEntityService.update(outFishApplyEntity);
      return this.buildHttpResultW(current, new String[]{});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照OutFishApplyEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照OutFishApplyEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @RequestMapping(value = "/findDetailsById", method = {RequestMethod.GET})
  public ResponseModel findDetailsById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      OutFishApplyEntity result = this.outFishApplyEntityService.findDetailsById(id);
      return this.buildHttpResultW(result, new String[]{"pond", "workOrder", "customerInfos", "customerInfos.fishType", "remainInfos", "remainInfos.fishType"});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照OutFishApplyEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param formInstanceId 表单实例编号
   */
  @ApiOperation(value = "按照OutFishApplyEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @RequestMapping(value = "/findDetailsByFormInstanceId", method = {RequestMethod.GET})
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") @ApiParam("表单实例编号") String formInstanceId) {
    try {
      OutFishApplyEntity result = this.outFishApplyEntityService.findDetailsByFormInstanceId(formInstanceId);
      return this.buildHttpResultW(result, new String[]{"pond", "workOrder", "customerInfos", "remainInfos.fishType", "remainInfos"});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照OutFishApplyEntity实体中的（formInstanceId）表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @ApiOperation(value = "按照OutFishApplyEntity实体中的（formInstanceId）表单实例编号进行查询")
  @RequestMapping(value = "/findByFormInstanceId", method = {RequestMethod.GET})
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") @ApiParam("表单实例编号") String formInstanceId) {
    try {
      OutFishApplyEntity result = this.outFishApplyEntityService.findByFormInstanceId(formInstanceId);
      return this.buildHttpResultW(result, new String[]{});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "重新发起岀鱼申请审批流程")
  @RequestMapping(value = "/reApply", method = {RequestMethod.GET})
  public ResponseModel reApply(@RequestParam("id") @ApiParam("岀鱼申请id") String id) {
    try {
      String result = this.outFishApplyEntityService.reApply(id);
      ResponseModel responseModel = this.buildHttpResult();
      responseModel.setMessage(result);
      return responseModel;
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 移动端根据基地名称、塘口名称、时间分页查询出鱼申请信息
   * @param pondId
   * @return
   */
  @ApiOperation(value = "移动端根据基地名称、塘口名称、时间分页查询出鱼申请信息")
  @GetMapping("findDetailByOrgBaseNameOrPondNameOrTiem")
  public ResponseModel findDetailByOrgBaseNameAndPondName(@ApiParam("塘口名称")String pondId, @ApiParam("开始时间")String start, @ApiParam("结束时间")String end, Pageable pageable){
    try {
      Date startTime = DateUtils.dateParse(start, DateUtils.TO_DAY_TIME);
      Date endTime = DateUtils.dateParse(end, DateUtils.TO_DAY_TIME);
      Map<String, Object> params = new HashMap<>();
      params.put("pondId", pondId);
      params.put("start", startTime);
      params.put("end", endTime);
      Page<OutApplyCustomStateEntity> result = this.outFishApplyEntityService.findDetailByOrgBaseNameOrPondNameOrTiem(params, pageable);
      return this.buildHttpResultW(result, "outFishApply", "outFishApply.pond", "customer");
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }
} 
