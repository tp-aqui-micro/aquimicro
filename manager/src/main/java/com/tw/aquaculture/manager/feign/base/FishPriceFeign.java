package com.tw.aquaculture.manager.feign.base;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.base.FishPriceEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 陈荣
 * @date 2019/12/9 11:38
 */
@FeignClient(name = "${aquiServiceName.process}", qualifier = "FishPriceFeign")
public interface FishPriceFeign {

  /**
   * 创建
   *
   * @param fishPriceEntity
   * @return
   */
  @PostMapping(value = "/v1/fishPriceEntitys")
  public ResponseModel create(@RequestBody FishPriceEntity fishPriceEntity);

  /**
   * 修改
   *
   * @param fishPriceEntity
   * @return
   */
  @PostMapping(value = "/v1/fishPriceEntitys/update")
  public ResponseModel update(@RequestBody FishPriceEntity fishPriceEntity);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/fishPriceEntitys/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/fishPriceEntitys/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 格局表单实例编号查详情
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/fishPriceEntitys/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据表单实例编号查询
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/fishPriceEntitys/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据id删除
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/fishPriceEntitys/deleteById")
  public ResponseModel deleteById(@RequestParam("id") String id);

}
