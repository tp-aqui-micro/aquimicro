package com.tw.aquaculture.manager.feign.base;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.base.FishUpRateEntity;
import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * Created by chenrong on 2020/1/15
 */
@FeignClient(value = "${aquiServiceName.process}", path = "/v1/fishUpRateEntitys")
public interface FishUpRateFeign {

  /**
   * 相关的创建过程，http接口。请注意该创建过程除了可以创建fishUpRateEntity中的基本信息以外，还可以对fishUpRateEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（FishUpRateEntity）模型的创建操作传入的fishUpRateEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   * */
  @PostMapping(value="")
  public ResponseModel create(@RequestBody FishUpRateEntity fishUpRateEntity);

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（FishUpRateEntity）的修改操作传入的fishUpRateEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   * */
  @PostMapping(value="/update")
  public ResponseModel update(@RequestBody FishUpRateEntity fishUpRateEntity);

  /**
   * 按照FishUpRateEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   * @param id 主键
   */
  @GetMapping(value="/findDetailsById" )
  public ResponseModel findDetailsById(@RequestParam("id") @ApiParam("主键") String id);
  /**
   * 按照FishUpRateEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   * @param formInstanceId 表单实例编号
   */
  @GetMapping(value="/findDetailsByFormInstanceId" )
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);
  /**
   * 按照FishUpRateEntity实体中的（formInstanceId）表单实例编号进行查询
   * @param formInstanceId 表单实例编号
   */
  @GetMapping(value="/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);
}
