package com.tw.aquaculture.manager.feign.base;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.base.PondEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 陈荣
 * @date 2019/12/5 19:17
 */
@FeignClient(qualifier = "PondFeign", name = "${aquiServiceName.process}")
public interface PondFeign {

  /**
   * 创建
   *
   * @param pondEntity
   * @return
   */
  @PostMapping(value = "/v1/pondEntitys")
  public ResponseModel create(@RequestBody PondEntity pondEntity);

  /**
   * 修改
   *
   * @param pondEntity
   * @return
   */
  @PostMapping(value = "/v1/pondEntitys/update")
  public ResponseModel update(@RequestBody PondEntity pondEntity);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/pondEntitys/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/pondEntitys/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 格局表单实例编号查详情
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/pondEntitys/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据表单实例编号查询
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/pondEntitys/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据id删除
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/pondEntitys/deleteById")
  public ResponseModel deleteById(@RequestParam("id") String id);

  /**
   * 根据基地查塘口
   *
   * @param baseId
   * @return
   */
  @GetMapping(value = "/v1/pondEntitys/findDetailsByBase")
  ResponseModel findDetailsByBase(@RequestParam("baseId") String baseId);

  /**
   * 根据养殖小组查塘口
   *
   * @param twGroupId
   * @return
   */
  @GetMapping(value = "/v1/pondEntitys/findByTwGroup")
  ResponseModel findByTwGroup(@RequestParam("twGroupId") String twGroupId);

  /**
   * 根据基地和状态查塘口
   *
   * @param baseId
   * @param enableState
   * @return
   */
  @GetMapping(value = "/v1/pondEntitys/findPondsByBaseId")
  ResponseModel findPondsByBaseId(@RequestParam("baseId") String baseId, @RequestParam("enableState") int enableState);
}
