package com.tw.aquaculture.manager.feign.base;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.base.ShareModelEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 陈荣
 * @date 2019/12/9 11:38
 */
@FeignClient(name = "${aquiServiceName.plan}", qualifier = "ShareModelFeign")
public interface ShareModelFeign {

  /**
   * 创建人
   *
   * @param shareModelEntity
   * @return
   */
  @PostMapping(value = "/v1/shareModelEntitys")
  public ResponseModel create(@RequestBody ShareModelEntity shareModelEntity);

  /**
   * 修改
   *
   * @param shareModelEntity
   * @return
   */
  @PostMapping(value = "/v1/shareModelEntitys/update")
  public ResponseModel update(@RequestBody ShareModelEntity shareModelEntity);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/shareModelEntitys/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/shareModelEntitys/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 格局表单实例编号查详情
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/shareModelEntitys/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据表单实例编号查询
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/shareModelEntitys/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据id删除
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/shareModelEntitys/deleteById")
  public ResponseModel deleteById(@RequestParam("id") String id);

}
