package com.tw.aquaculture.manager.feign.base;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.base.StoreEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 陈荣
 * @date 2019/12/9 11:38
 */
@FeignClient(name = "${aquiServiceName.store}", qualifier = "StoreFeign")
public interface StoreFeign {

  /**
   * 创建
   *
   * @return
   */
  @PostMapping(value = "/v1/storeEntitys")
  public ResponseModel create(@RequestBody StoreEntity storeEntity);

  /**
   * 修改
   *
   * @param storeEntity
   * @return
   */
  @PostMapping(value = "/v1/storeEntitys/update")
  public ResponseModel update(@RequestBody StoreEntity storeEntity);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/storeEntitys/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/storeEntitys/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 格局表单实例编号查详情
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/storeEntitys/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据表单实例编号查询
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/storeEntitys/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据id删除
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/storeEntitys/deleteById")
  public ResponseModel deleteById(@RequestParam("id") String id);

  /**
   * 根据基地查询仓库
   *
   * @param baseId
   * @return
   */
  @GetMapping(value = "/v1/storeEntitys/findStoresByBaseId")
  ResponseModel findStoresByBaseId(@RequestParam("baseId") String baseId);
}
