package com.tw.aquaculture.manager.feign.base;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @author 陈荣
 * @date 2019/12/5 19:14
 */
@FeignClient(qualifier = "TwBaseFeign", name = "${aquiServiceName.process}")
public interface TwBaseFeign {

  /**
   * 创建
   *
   * @param twBaseEntity
   * @return
   */
  @PostMapping(value = "/v1/twBaseEntitys")
  public ResponseModel create(@RequestBody TwBaseEntity twBaseEntity);

  /**
   * 修改
   *
   * @param twBaseEntity
   * @return
   */
  @PostMapping(value = "/v1/twBaseEntitys/update")
  public ResponseModel update(@RequestBody TwBaseEntity twBaseEntity);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/twBaseEntitys/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/twBaseEntitys/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 格局表单实例编号查详情
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/twBaseEntitys/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据表单实例编号查询
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/twBaseEntitys/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据id删除
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/twBaseEntitys/deleteById")
  public ResponseModel deleteById(@RequestParam("id") String id);

  /**
   * 查询所有
   *
   * @return
   */
  @GetMapping(value = "/v1/twBaseEntitys/findAll")
  ResponseModel findAll();

  /**
   * 查询所有（包括详情）
   *
   * @return
   */
  @GetMapping(value = "/v1/twBaseEntitys/findDetailAll")
  ResponseModel findDetailAll();

  /**
   * 根据组织基地id查询详情
   *
   * @param baseOrgId
   * @return
   */
  @GetMapping(value = "/v1/twBaseEntitys/findDetailsByBaseOrgId")
  ResponseModel findDetailsByBaseOrgId(@RequestParam("baseOrgId") String baseOrgId);

  /**
   * 条件分页查询
   *
   * @param pageable
   * @param params
   * @return
   */
  @GetMapping(value = "/v1/twBaseEntitys/findByConditions")
  ResponseModel findByConditions(@RequestParam("pageable") Pageable pageable, @RequestParam("params") Map<String, Object> params);
}
