package com.tw.aquaculture.manager.feign.check;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.check.CheckFishEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 陈荣
 * @date 2019/12/5 19:25
 */
@FeignClient(name = "${aquiServiceName.check}", qualifier = "CheckFishFeign")
public interface CheckFishFeign {

  /**
   * 创建
   *
   * @param checkFishEntity
   * @return
   */
  @PostMapping(value = "/v1/checkFishEntitys")
  public ResponseModel create(@RequestBody CheckFishEntity checkFishEntity);

  /**
   * 修改
   *
   * @param checkFishEntity
   * @return
   */
  @PostMapping(value = "/v1/checkFishEntitys/update")
  public ResponseModel update(@RequestBody CheckFishEntity checkFishEntity);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/checkFishEntitys/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/checkFishEntitys/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 格局表单实例编号查详情
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/checkFishEntitys/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据表单实例编号查询
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/checkFishEntitys/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据id删除
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/checkFishEntitys/deleteById")
  public ResponseModel deleteById(@RequestParam("id") String id);
}
