package com.tw.aquaculture.manager.feign.check;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.check.CheckPondEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 陈荣
 * @date 2019/12/5 19:25
 */
@FeignClient(name = "${aquiServiceName.check}", qualifier = "CheckPondFeign")
public interface CheckPondFeign {

  /**
   * 创建
   *
   * @param checkPondEntity
   * @return
   */
  @PostMapping(value = "/v1/checkPondEntitys")
  public ResponseModel create(@RequestBody CheckPondEntity checkPondEntity);

  /**
   * 修改
   *
   * @param checkPondEntity
   * @return
   */
  @PostMapping(value = "/v1/checkPondEntitys/update")
  public ResponseModel update(@RequestBody CheckPondEntity checkPondEntity);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/checkPondEntitys/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/checkPondEntitys/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 格局表单实例编号查详情
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/checkPondEntitys/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据表单实例编号查询
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/checkPondEntitys/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据id删除
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/checkPondEntitys/deleteById")
  public ResponseModel deleteById(@RequestParam("id") String id);

  /**
   * 根据塘口和用户统计
   *
   * @param pondId
   * @param username
   * @return
   */
  @GetMapping(value = "/v1/checkPondEntitys/findCountByPondAndUser")
  ResponseModel findCountByPondAndUser(@RequestParam("pondId") String pondId, @RequestParam("username") String username);

  /**
   * 巡塘检测统计
   *
   * @param pondId
   * @param state
   * @return
   */
  @GetMapping(value = "/v1/checkPondEntitys/checkPondCount")
  int checkPondCount(@RequestParam("pondId") String pondId, @RequestParam("state") String state);

  /**
   * 查询最后一次巡塘检测
   *
   * @param pondId
   * @return
   */
  @GetMapping(value = "/v1/checkPondEntitys/findLastCheckPondByPondId")
  ResponseModel findLastCheckPondByPondId(@RequestParam("pondId") String pondId);
}
