package com.tw.aquaculture.manager.feign.check;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.check.CheckWaterQualityEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 陈荣
 * @date 2019/12/5 19:25
 */
@FeignClient(name = "${aquiServiceName.check}", qualifier = "CheckWaterQualityFeign")
public interface CheckWaterQualityFeign {

  /**
   * 创建
   *
   * @param checkWaterQualityEntity
   * @return
   */
  @PostMapping(value = "/v1/checkWaterQualityEntitys")
  public ResponseModel create(@RequestBody CheckWaterQualityEntity checkWaterQualityEntity);

  /**
   * 修改
   *
   * @param checkWaterQualityEntity
   * @return
   */
  @PostMapping(value = "/v1/checkWaterQualityEntitys/update")
  public ResponseModel update(@RequestBody CheckWaterQualityEntity checkWaterQualityEntity);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/checkWaterQualityEntitys/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/checkWaterQualityEntitys/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 格局表单实例编号查详情
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/checkWaterQualityEntitys/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据表单实例编号查询
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/checkWaterQualityEntitys/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据id删除
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/checkWaterQualityEntitys/deleteById")
  public ResponseModel deleteById(@RequestParam("id") String id);

  /**
   * 查询最后一次水质检测
   *
   * @param pondId
   * @return
   */
  @GetMapping(value = "/v1/checkWaterQualityEntitys/findLastCheckWaterByPondId")
  ResponseModel findLastCheckWaterByPondId(@RequestParam("pondId") String pondId);
}
