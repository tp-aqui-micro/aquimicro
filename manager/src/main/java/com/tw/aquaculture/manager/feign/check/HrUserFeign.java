package com.tw.aquaculture.manager.feign.check;

import com.bizunited.platform.core.controller.model.ResponseModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author 陈荣
 * @date 2019/12/26 13:47
 */
@FeignClient(name = "${aquiServiceName.check}", qualifier = "HrUserFeign")
public interface HrUserFeign {

  /**
   * 拉取hr用户信息
   * @return
   */
  @GetMapping(value = "/v1/userHrRemote/pullAllMatters")
  ResponseModel pullAllMatters();
}
