package com.tw.aquaculture.manager.feign.plan;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.plan.OutFishPlanEntity;
import com.tw.aquaculture.common.vo.reportslist.OutFishPlanReportParamVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 陈荣
 * @date 2019/12/5 19:25
 */
@FeignClient(value = "${aquiServiceName.plan}", qualifier = "OutFishPlanFeign")
public interface OutFishPlanFeign {

  /**
   * 创建
   *
   * @return
   */
  @PostMapping(value = "/v1/outFishPlanEntitys")
  public ResponseModel create(@RequestBody OutFishPlanEntity outFishPlanEntity);

  /**
   * 修改
   *
   * @param outFishPlanEntity
   * @return
   */
  @PostMapping(value = "/v1/outFishPlanEntitys/update")
  public ResponseModel update(@RequestBody OutFishPlanEntity outFishPlanEntity);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/outFishPlanEntitys/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/outFishPlanEntitys/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 格局表单实例编号查详情
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/outFishPlanEntitys/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据表单实例编号查询
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/outFishPlanEntitys/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据id删除
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/outFishPlanEntitys/deleteById")
  public ResponseModel deleteById(@RequestParam("id") String id);

  /**
   * 出鱼计划报表
   * @param outFishPlanReportParamVo
   * @return
   */
  @PostMapping(value = "/v1/outFishPlanEntitys/outFishPlanReport")
  ResponseModel outFishPlanReport(@RequestBody OutFishPlanReportParamVo outFishPlanReportParamVo);
}
