package com.tw.aquaculture.manager.feign.plan;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.plan.PutFryPlanEntity;
import com.tw.aquaculture.common.vo.reportslist.PushFryPlanReportParamVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 陈荣
 * @date 2019/12/5 19:26
 */
@FeignClient(name = "${aquiServiceName.plan}", qualifier = "PutFryPlanFeign")
public interface PutFryPlanFeign {

  /**
   * 创建
   *
   * @return
   */
  @PostMapping(value = "/v1/putFryPlanEntitys")
  public ResponseModel create(@RequestBody PutFryPlanEntity putFryPlanEntity);

  /**
   * 修改年度计划
   *
   * @param putFryPlanEntity
   * @return
   */
  @PostMapping(value = "/v1/putFryPlanEntitys/update")
  public ResponseModel update(@RequestBody PutFryPlanEntity putFryPlanEntity);

  /**
   * 根据id查询年度计划详情
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/putFryPlanEntitys/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/putFryPlanEntitys/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 格局表单实例编号查详情
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/putFryPlanEntitys/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据表单实例编号查询
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/putFryPlanEntitys/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据id删除
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/putFryPlanEntitys/deleteById")
  public ResponseModel deleteById(@RequestParam("id") String id);

  /**
   * 投苗计划报表
   * @param pushFryPlanReportParamVo
   * @return
   */
  @GetMapping(value = "/v1/putFryPlanEntitys/pushFryPlanReport")
  ResponseModel pushFryPlanReport(@RequestBody PushFryPlanReportParamVo pushFryPlanReportParamVo);
}
