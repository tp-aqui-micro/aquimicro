package com.tw.aquaculture.manager.feign.plan;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.plan.YearlyPlanEntity;
import com.tw.aquaculture.common.vo.reportslist.YearlyPlanReportParamVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 陈荣
 * @date 2019/12/4 15:04
 */
@FeignClient(name = "${aquiServiceName.plan}", qualifier = "YearlyPlanFeign")
public interface YearlyPlanFeign {

  /**
   * 创建年度计划
   *
   * @return
   */
  @PostMapping(value = "/v1/yearlyPlanEntitys")
  public ResponseModel create(@RequestBody YearlyPlanEntity yearlyPlanEntity);

  /**
   * 修改年度计划
   *
   * @param yearlyPlanEntity
   * @return
   */
  @PostMapping(value = "/v1/yearlyPlanEntitys/update")
  public ResponseModel update(@RequestBody YearlyPlanEntity yearlyPlanEntity);

  /**
   * 根据id查询年度计划详情
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/yearlyPlanEntitys/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/yearlyPlanEntitys/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 格局表单实例编号查详情
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/yearlyPlanEntitys/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据表单实例编号查询
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/yearlyPlanEntitys/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据id删除
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/yearlyPlanEntitys/deleteById")
  public ResponseModel deleteById(@RequestParam("id") String id);

  /**
   * 年度计划报表
   * @param yearlyPlanReportParamVo
   * @return
   */
  @PostMapping(value = "/v1/yearlyPlanEntitys/yearlyPlanReport")
  ResponseModel yearlyPlanReport(@RequestBody YearlyPlanReportParamVo yearlyPlanReportParamVo);
}
