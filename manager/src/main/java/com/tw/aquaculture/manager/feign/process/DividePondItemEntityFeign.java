package com.tw.aquaculture.manager.feign.process;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.process.DividePondItemEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * DividePondItemEntity业务模型的MVC Controller层实现，基于HTTP Restful风格
 *
 * @author saturn
 */
@FeignClient(name = "${aquiServiceName.process}", qualifier = "dividePondEntityFeign",  path = "/v1/dividePondItemEntitys")
public interface DividePondItemEntityFeign {

  /**
   * 相关的创建过程，http接口。请注意该创建过程除了可以创建dividePondItemEntity中的基本信息以外，还可以对dividePondItemEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（DividePondItemEntity）模型的创建操作传入的dividePondItemEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @PostMapping(value = "")
  ResponseModel create(@RequestBody DividePondItemEntity dividePondItemEntity);

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（DividePondItemEntity）的修改操作传入的dividePondItemEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @PostMapping(value = "/update")
  ResponseModel update(@RequestBody DividePondItemEntity dividePondItemEntity);

  /**
   * 按照DividePondItemEntity实体中的（dividePond）关联的 转/分塘进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param dividePond 关联的 转/分塘
   */
  @GetMapping(value = "/findDetailsByDividePond")
  ResponseModel findDetailsByDividePond(@RequestParam("dividePond") String dividePond);

  /**
   * 按照DividePondItemEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param id 主键
   */
  @GetMapping(value = "/findDetailsById")
  ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 按照DividePondItemEntity的主键编号，查询指定的数据信息（不包括任何关联信息）
   *
   * @param id 主键
   */
  @GetMapping(value = "/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 按照主键进行信息的真删除
   *
   * @param id 主键
   */
  @GetMapping(value = "/deleteById")
  void deleteById(@RequestParam("id") String id);
} 
