package com.tw.aquaculture.manager.feign.process;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.process.OutApplyCustomStateEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * OutApplyCustomStateEntity业务模型的服务层接口定义
 * @author saturn
 */
@FeignClient(name = "${aquiServiceName.process}", qualifier = "outApplyCustomStateEntityFeign",path="/v1/outApplyCustomStateEntitys")
public interface OutApplyCustomStateEntityFeign {
  /**
   * 创建一个新的OutApplyCustomStateEntity模型对象（包括了可能的第三方系统调用、复杂逻辑处理等）
   */
  @PostMapping(value = "")
   ResponseModel create(@RequestBody OutApplyCustomStateEntity outApplyCustomStateEntity);
  /**
   * 更新一个已有的OutApplyCustomStateEntity模型对象，其主键属性必须有值(1.1.4-release版本调整)。
   * 这个方法实际上一共分为三个步骤（默认）：</br>
   * 1、调用updateValidation方法完成表单数据更新前的验证</br>
   * 2、调用updateForm方法完成表单数据的更新</br>
   * 3、完成开发人员自行在本update方法中书写的，进行第三方系统调用（或特殊处理过程）的执行。</br>
   * 这样做的目的，实际上是为了保证updateForm方法中纯粹是处理表单数据的，在数据恢复表单引擎默认调用updateForm方法时，不会影响任何第三方业务数据
   * （当然，如果系统有特别要求，可由开发人员自行完成代码逻辑调整）
   */
  @PostMapping(value="/update")
  ResponseModel update(@RequestBody OutApplyCustomStateEntity outApplyCustomStateEntity);
  /**
   * 按照关联的 岀鱼申请id进行详情查询（包括关联信息）
   * @param outFishApply 关联的 岀鱼申请id
   */
  @GetMapping(value = "/findDetailsByOutFishApply")
 ResponseModel findDetailsByOutFishApply(@RequestParam( "outFishApply") String outFishApply);
  /**
   * 按照关联的 客户进行详情查询（包括关联信息）
   * @param customer 关联的 客户
   */
  @GetMapping(value = "/findDetailsByCustomer")
  ResponseModel findDetailsByCustomer(@RequestParam("customer") String customer);
  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   */
  @GetMapping(value = "/findDetailsById")
  ResponseModel findDetailsById(@RequestParam("id")  String id);
  /**
   * 按照OutApplyCustomStateEntity的主键编号，查询指定的数据信息（不包括任何关联信息）
   * @param id 主键
   * */
  @GetMapping(value = "/findById")
  ResponseModel findById(@RequestParam("id") String id);
  /**
   *  按照主键进行信息的真删除 
   * @param id 主键 
   */
  @GetMapping(value = "/deleteById")
  void deleteById(@RequestParam("id") String id);

  /**
   * 根据岀鱼申请id查询
   * @param outFishApplyId
   * @param state
   * @return
   */
  @GetMapping(value = "/findByOutFishApplyIdAndState")
  ResponseModel findByOutFishApplyIdAndState(@RequestParam("outFishApplyId") String outFishApplyId,@RequestParam("state") int state);

  /**
   * 根据岀鱼申请id查询
   * @param outFishApplyId
   * @return
   */
  @GetMapping(value = "/findByOutFishApply")
  ResponseModel findByOutFishApply(@RequestParam("outFishApplyId") String outFishApplyId);
}