package com.tw.aquaculture.manager.feign.process;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.process.ProofYieldEntity;
import com.tw.aquaculture.common.vo.reportslist.ReportParamVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * ProofYieldEntity业务模型的MVC Controller层实现，基于HTTP Restful风格
 *
 * @author saturn
 */
@FeignClient(name = "${aquiServiceName.process}", qualifier = "proofYieldEntityFeign", path = "/v1/proofYieldEntitys")
public interface ProofYieldEntityFeign {
  /**
   * 相关的创建过程，http接口。请注意该创建过程除了可以创建proofYieldEntity中的基本信息以外，还可以对proofYieldEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（ProofYieldEntity）模型的创建操作传入的proofYieldEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @PostMapping(value = "")
  ResponseModel create(@RequestBody ProofYieldEntity proofYieldEntity);

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（ProofYieldEntity）的修改操作传入的proofYieldEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @PostMapping(value = "/update")
  ResponseModel update(@RequestBody ProofYieldEntity proofYieldEntity);

  /**
   * 按照ProofYieldEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param id 主键
   */
  @GetMapping(value = "/findDetailsById")
  ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 按照ProofYieldEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param formInstanceId 表单实例编号
   */
  @GetMapping(value = "/findDetailsByFormInstanceId")
  ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 按照ProofYieldEntity实体中的（formInstanceId）表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @GetMapping(value = "/findByFormInstanceId")
  ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 按照ProofYieldEntity的主键编号，查询指定的数据信息（不包括任何关联信息）
   *
   * @param id 主键
   */
  @GetMapping(value = "/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 按照主键进行信息的真删除
   *
   * @param id 主键
   */
  @GetMapping(value = "/deleteById")
  void deleteById(@RequestParam("id") String id);

  /**
   * 本周期内最后一次打样测产数据
   *
   * @param pondId 塘口id
   * @return
   */
  @GetMapping(value = "/findLastProofYield")
  ResponseModel findLastProofYield(@RequestParam("pondId") String pondId);

  /**
   * 查询上次存塘量
   *
   * @param pondId
   * @return
   */
  @GetMapping(value = "/computeLastExitCount")
  ResponseModel computeLastExitCount(@RequestParam("pondId") String pondId);

  /**
   * 本周期内某鱼种上次打样测产数据
   *
   * @param pondId
   * @param fishTypeId
   * @return
   */
  @GetMapping(value = "/findLastProofYieldByFishType")
  ResponseModel findLastProofYieldByFishType(@RequestParam("pondId") String pondId, @RequestParam("fishTypeId") String fishTypeId);

  /**
   * 根据塘口和工单号查询
   *
   * @param pondId
   * @param workOrderId
   * @return
   */
  @GetMapping(value = "/findDetailsByPondAndWorkOrder")
  ResponseModel findDetailsByPondAndWorkOrder(@RequestParam("pondId") String pondId, @RequestParam("workOrderId") String workOrderId);

  /**
   * 根据塘口和鱼种查询本周期内存塘量
   * @param pondId
   * @param fishTypeId
   * @return
   */
  @GetMapping(value = "/findExitCountByPondAndFishType")
  ResponseModel findExitCountByPondAndFishType(@RequestParam("pondId") String pondId, @RequestParam("fishTypeId") String fishTypeId);

  /**
   * 存塘量报表
   * @param reportParamVo
   * @return
   */
  @PostMapping(value = "/existAmountReport")
  ResponseModel existAmountReport(@RequestBody ReportParamVo reportParamVo);
}
