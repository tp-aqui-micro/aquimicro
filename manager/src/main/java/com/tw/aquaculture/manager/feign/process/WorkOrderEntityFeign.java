package com.tw.aquaculture.manager.feign.process;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;

/**
 * WorkOrderEntity业务模型的MVC Controller层实现，基于HTTP Restful风格
 *
 * @author saturn
 */
@FeignClient(name = "${aquiServiceName.process}", qualifier = "workOrderEntityFeign", path = "/v1/workOrderEntitys")
public interface WorkOrderEntityFeign {
  /**
   * 相关的创建过程，http接口。请注意该创建过程除了可以创建workOrderEntity中的基本信息以外，还可以对workOrderEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（WorkOrderEntity）模型的创建操作传入的workOrderEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @PostMapping(value = "")
  ResponseModel create(@RequestBody WorkOrderEntity workOrderEntity);

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（WorkOrderEntity）的修改操作传入的workOrderEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @PostMapping(value = "/update")
  ResponseModel update(@RequestBody WorkOrderEntity workOrderEntity);

  /**
   * 按照WorkOrderEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param id 主键
   */
  @GetMapping(value = "/findDetailsById")
  ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 按照WorkOrderEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param formInstanceId 表单实例编号
   */
  @GetMapping(value = "/findDetailsByFormInstanceId")
  ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 按照WorkOrderEntity实体中的（formInstanceId）表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @GetMapping(value = "/findByFormInstanceId")
  ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 按照WorkOrderEntity实体中的主键编号，查询指定的数据信息（不包括任何关联信息）
   *
   * @param id 主键
   */
  @GetMapping(value = "/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 按照主键进行信息的真删除
   *
   * @param id 主键
   */
  @GetMapping(value = "/deleteById")
  void deleteById(@RequestParam("id") String id);

  /**
   * 条件分页查询
   *
   * @param pageable
   * @param params
   * @return
   */
  @GetMapping(value = "/findByConditions")
  ResponseModel findByConditions(@RequestParam("pageable") Pageable pageable, @RequestParam("params") Map<String, Object> params);

  /**
   * 根据塘口和创建时间查询
   *
   * @param pondId
   * @param createTime
   * @return
   */
  @GetMapping(value = "/findByPondAndTime")
  ResponseModel findByPondAndTime(@RequestParam("pondId") String pondId, @RequestParam("createTime") Date createTime);

  /**
   * 保存
   *
   * @param w
   */
  @GetMapping(value = "/save")
  void save(@RequestBody WorkOrderEntity w);

  /**
   * 根据塘口和状态查询
   *
   * @param pondId
   * @param state
   * @return
   */
  @GetMapping(value = "/findByStateAndPond")
  ResponseModel findByStateAndPond(@RequestParam("pondId") String pondId, @RequestParam("state") int state);
}
