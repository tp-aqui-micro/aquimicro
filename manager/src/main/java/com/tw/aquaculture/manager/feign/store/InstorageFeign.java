package com.tw.aquaculture.manager.feign.store;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.store.InStorageEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 入库
 *
 * @author 陈荣
 * @date 2019/12/9 10:52
 */
@FeignClient(name = "${aquiServiceName.store}", qualifier = "InstorageFeign",path = "/v1/inStorageEntitys")
public interface InstorageFeign {

  /**
   * 创建
   *
   * @param inStorageEntity
   * @return
   */
  @PostMapping(value = "")
  public ResponseModel create(@RequestBody InStorageEntity inStorageEntity);

  /**
   * 修改
   */
  @PostMapping(value = "/update")
  public ResponseModel update(@RequestBody InStorageEntity inStorageEntity);

  /**
   * 按照InStorageEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param id 主键
   */
  @GetMapping(value = "/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 按照InStorageEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param formInstanceId 表单实例编号
   */
  @GetMapping(value = "/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 按照InStorageEntity实体中的（formInstanceId）表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @GetMapping(value = "/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);
}
