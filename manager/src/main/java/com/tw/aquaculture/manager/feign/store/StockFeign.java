package com.tw.aquaculture.manager.feign.store;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.store.StockEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 陈荣
 * @date 2019/12/11 11:00
 */
@FeignClient(name = "${aquiServiceName.store}", qualifier = "StockFeign",  path = "/v1/stockEntitys")
public interface StockFeign {

  /**
   * 创建
   *
   * @param stockEntity
   * @return
   */
  @PostMapping(value = "")
  public ResponseModel create(@RequestBody StockEntity stockEntity);

  /**
   * 修改
   */
  @PostMapping(value = "/update")
  public ResponseModel update(@RequestBody StockEntity stockEntity);

  /**
   * 按照InStorageEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param id 主键
   */
  @GetMapping(value = "/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 按照InStorageEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param formInstanceId 表单实例编号
   */
  @GetMapping(value = "/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 按照InStorageEntity实体中的（formInstanceId）表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @GetMapping(value = "/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);


  @GetMapping(value = "/findDetailsByBase")
  public ResponseModel findDetailsByBase(@RequestParam("base") String base);

  @GetMapping(value = "/findDetailsByStore")
  public ResponseModel findDetailsByStore(@RequestParam("store") String store);

  @GetMapping(value = "/findDetailsByMatter")
  public ResponseModel findDetailsByMatter(@RequestParam("matter") String matter);

  @GetMapping(value = "/findByMatterAndStore")
  public ResponseModel findByMatterAndStore(@RequestParam("storeId") String storeId, @RequestParam("matterId") String matterId);

  @GetMapping(value = "/findByBaseId")
  public ResponseModel findByBaseId(@RequestParam("baseId") String baseId);

}
