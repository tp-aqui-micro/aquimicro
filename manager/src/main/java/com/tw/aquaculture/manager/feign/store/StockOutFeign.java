package com.tw.aquaculture.manager.feign.store;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.store.StockOutEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

/**
 * 出库
 *
 * @author 陈荣
 * @date 2019/12/11 11:00
 */
@FeignClient(name = "${aquiServiceName.store}", qualifier = "StockOutFeign", path = "/v1/stockOutEntitys")
public interface StockOutFeign {

  /**
   * 创建
   *
   * @param stockOutEntity
   * @return
   */
  @PostMapping(value = "")
  public ResponseModel create(@RequestBody StockOutEntity stockOutEntity);

  /**
   * 修改
   */
  @PostMapping(value = "/update")
  public ResponseModel update(@RequestBody StockOutEntity stockOutEntity);

  /**
   * 按照InStorageEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param id 主键
   */
  @GetMapping(value = "/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 按照InStorageEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param formInstanceId 表单实例编号
   */
  @GetMapping(value = "/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 按照InStorageEntity实体中的（formInstanceId）表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @GetMapping(value = "/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  @GetMapping(value = "/findByBaseAndTimes")
  public ResponseModel findByBaseAndTimes(@RequestParam("baseId") String baseId, @RequestParam("start") Date start, @RequestParam("end") Date end);

  @GetMapping(value = "/sumConsumeByTimes")
  public Double sumConsumeByTimes(@RequestParam("baseId") String baseId, @RequestParam("matterId") String matterId, @RequestParam("start") Date start, @RequestParam("end") Date end);

}
