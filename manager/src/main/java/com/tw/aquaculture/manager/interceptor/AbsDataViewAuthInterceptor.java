package com.tw.aquaculture.manager.interceptor;

import com.bizunited.platform.core.common.PlatformContext;
import com.bizunited.platform.core.common.enums.SQLCorrelationEnum;
import com.bizunited.platform.core.entity.DataViewAuthHorizontalEntity;
import com.bizunited.platform.core.interceptor.DataViewAuthInterceptor;
import com.bizunited.platform.core.repository.dataview.analysis.SqlAnalysis;
import com.bizunited.platform.core.service.dataview.model.SQLParamModel;
import com.bizunited.platform.rbac.server.service.PositionService;
import com.bizunited.platform.rbac.server.service.RoleService;
import com.bizunited.platform.rbac.server.service.UserService;
import com.bizunited.platform.rbac.server.vo.PositionVo;
import com.bizunited.platform.rbac.server.vo.RoleVo;
import com.bizunited.platform.rbac.server.vo.UserVo;
import com.google.common.collect.Maps;
import com.tw.aquaculture.common.constant.RoleConstants;
import com.tw.aquaculture.manager.orgpondmap.service.OrgPondMapService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 公共类
 *
 * @author zizhou
 * @date 2019/12/28
 */
public abstract class AbsDataViewAuthInterceptor implements DataViewAuthInterceptor {

  protected static final Logger LOGGER = LoggerFactory.getLogger(AbsDataViewAuthInterceptor.class);

  protected PositionService positionService;
  protected UserService userService;
  protected OrgPondMapService orgPondMapService;
  protected ApplicationContext ctx;
  protected RoleService roleService;

  /**
   * 当前用户选择的岗位 可能为空
   */
  protected PositionVo selectPosition;
  protected UserVo userVo;
  protected List<RoleVo> roleVos;
  protected Map<String, RoleVo> roleMap;

  protected Map<SQLCorrelationEnum, Object> result;
  protected String originSql;
  protected int originCounter;
  protected Set<SQLParamModel> values;

  @Override
  public void intercept(SqlAnalysis sqlAnalysis) {
    initComponent();
    initCurrentUser();
    initSqlAnalysisData(sqlAnalysis);

    doIntercept(sqlAnalysis);
  }

  /**
   * 初始化依赖组件
   */
  protected void initComponent() {
    ctx = PlatformContext.getApplicationContext();
    userService = ctx.getBean(UserService.class);
    positionService = ctx.getBean(PositionService.class);
    orgPondMapService = ctx.getBean(OrgPondMapService.class);
    roleService = ctx.getBean(RoleService.class);
  }

  /**
   * 初始化当前用户相关信息
   */
  protected void initCurrentUser() {
    SecurityContext securityContext = SecurityContextHolder.getContext();
    Validate.notNull(securityContext, "未发现任何用户权限信息!!");
    Authentication authentication = securityContext.getAuthentication();
    Validate.notNull(authentication, "未发现任何用户登录信息!!");
    userVo = userService.findByAccount(authentication.getName());
    Validate.notNull(userVo, "用户不存在");

    // 拿到当前用户选择的岗位
    selectPosition = positionService.findMainPositionByUserId(userVo.getId());
    // 拿到当前用户所有直接关联的角色 和 间接关联的角色(岗位间接关联的角色只取当前选择的岗位关联的角色)
    roleVos = roleService.findAllByUserId(userVo.getId(), 0);
    if (CollectionUtils.isNotEmpty(roleVos)) {
      roleMap = roleVos.stream().collect(Collectors.toMap(RoleVo::getRoleName, vo -> vo, (vo1, vo2) -> vo1));
    } else {
      roleMap = Maps.newHashMap();
    }
  }

  /**
   * 从SqlAnalysis 中获取需要的数据
   */
  @SuppressWarnings("unchecked")
  protected void initSqlAnalysisData(SqlAnalysis sqlAnalysis) {
    result = sqlAnalysis.getResult();
    originSql = (String) result.get(SQLCorrelationEnum.RESULT_SQL);
    originCounter = (int) result.get(SQLCorrelationEnum.COUNTER);
    values = (LinkedHashSet<SQLParamModel>) result.get(SQLCorrelationEnum.AUTH_HORIZONTAL_VALUES);
  }


  public abstract void doIntercept(SqlAnalysis sqlAnalysis);

  /**
   * 拥有admin角色
   */
  protected boolean hasAdminRole() {
    return roleMap.containsKey("ADMIN");
  }

  /**
   * 用于更新sql
   */
  protected void updateSqlAnalysisResult(String sql, int counter) {
    result.put(SQLCorrelationEnum.COUNTER, counter);
    result.put(SQLCorrelationEnum.RESULT_SQL, sql);
  }

  /**
   * 添加一个参数
   */
  protected void addSQLParamModel(String value, int counter) {
    SQLParamModel spm = new SQLParamModel();
    spm.setParamType("java.lang.String");
    spm.setTransferType(2);
    spm.setIndex(counter);
    spm.setValue(value);
    spm.setSource(DataViewAuthHorizontalEntity.class);
    values.add(spm);
  }

  /**
   * select * from (${originSql}) originSql8848 where originSql8848.conditionColumn1 in (${values}) or originSql8848.conditionColumn2 in ((${values})..
   * 这种形式
   */
  protected void multipleCondition(List<String> conditionColumns, List<String> values) {
    checkSqlHaveColumns(conditionColumns.toArray(new String[]{}));
    int nowCounter = originCounter;
    StringBuilder builder = new StringBuilder("select * from (").append(originSql).append(") originSql8848 where ");
    if (CollectionUtils.isEmpty(values)) {
      // where一个恒不等式, mysql执行器将只会查询select的列 而不用再去筛选数据
      result.put(SQLCorrelationEnum.RESULT_SQL, builder.append(" 1=2"));
      return;
    }

    boolean isFirst = true;
    for (String conditionColumn : conditionColumns) {
      if (isFirst) {
        isFirst = false;
      } else {
        builder.append(" or ");
      }

      // 拼接占位符
      builder.append(" originSql8848.").append(conditionColumn).append(" in (");
      builder.append(StringUtils.join(values.stream().map(p1 -> "?").collect(Collectors.toList()), ",")).append(")");
      // 添加参数
      for (String value : values) {
        addSQLParamModel(value, nowCounter++);
      }
    }

    updateSqlAnalysisResult(builder.toString(), nowCounter);
  }

  /**
   * 改写为 select * from (${originSql}) originSql8848 where originSql8848.conditionColumn in (${values})这种形式
   */
  protected void simpleCondition(String conditionColumn, List<String> values) {
    checkSqlHaveColumns(conditionColumn);
    int nowCounter = originCounter;
    StringBuilder builder = new StringBuilder("select * from (").append(originSql).append(") originSql8848 ");
    if (CollectionUtils.isEmpty(values)) {
      // where一个恒不等式, mysql执行器将只会查询select的列 而不用再去筛选数据
      result.put(SQLCorrelationEnum.RESULT_SQL, builder.append("where 1=2"));
      return;
    }

    // 拼接in查询
    builder.append("where originSql8848.").append(conditionColumn).append(" in (");
    // 拼接占位符
    builder.append(StringUtils.join(values.stream().map(p1 -> "?").collect(Collectors.toList()), ",")).append(")");
    // 添加参数
    for (String value : values) {
      addSQLParamModel(value, nowCounter++);
    }
    // 更新sql和计数器
    updateSqlAnalysisResult(builder.toString(), nowCounter);
  }

  /**
   * 当前岗位可以查看的塘口id
   */
  protected List<String> findCanViewProdIds() {
    if (selectPosition == null) {
      return null;
    }
    // 拥有 UP_SELECT_ROLE 特殊角色的用户
    if (roleMap.containsKey(RoleConstants.UP_SELECT_ROLE)) {
      return orgPondMapService.findPondsByParentPos(selectPosition.getId());
    }

    return orgPondMapService.findPondsByPos(selectPosition.getId());
  }

  /**
   * 当前岗位可以查看的仓库id
   */
  protected List<String> findCanViewStoreIds() {
    if (selectPosition == null) {
      return null;
    }
    // 拥有 UP_SELECT_ROLE 特殊角色的用户
    if (roleMap.containsKey(RoleConstants.UP_SELECT_ROLE)) {
      return orgPondMapService.findStoresByParentPos(selectPosition.getId());
    }

    return orgPondMapService.findStoresByPos(selectPosition.getId());
  }

  /**
   * 查询当前岗位可以查看的组织id
   */
  protected List<String> findCanViewOrgIds() {
    if (selectPosition == null) {
      return null;
    }
    // 拥有 UP_SELECT_ROLE 特殊角色的用户
    if (roleMap.containsKey(RoleConstants.UP_SELECT_ROLE)) {
      return orgPondMapService.findOrgByParentPos(selectPosition.getId());
    }

    return orgPondMapService.findOrgByPos(selectPosition.getId());
  }

  /**
   * 检查数据视图sql查询结果列是否有指定的标识
   */
  protected void checkSqlHaveColumns(String... columns) {
    if (columns == null) {
      return;
    }
    for (String column : columns) {
      Validate.isTrue(originSql.contains(column), "当前配置的数据视图权限拦截器，需要原sql存在 【" + column + "】 这个列");
    }
  }
}
