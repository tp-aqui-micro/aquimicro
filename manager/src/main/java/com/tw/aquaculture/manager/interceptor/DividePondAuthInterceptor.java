package com.tw.aquaculture.manager.interceptor;

import com.bizunited.platform.core.repository.dataview.analysis.SqlAnalysis;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * 专用于转分塘数据视图
 *
 * @author zizhou
 * @date 2019/12/29
 */
public class DividePondAuthInterceptor extends AbsDataViewAuthInterceptor {

  @Override
  public void doIntercept(SqlAnalysis sqlAnalysis) {
    if (hasAdminRole()) {
      return;
    }
    // 当前用户能够看到的塘口信息
    List<String> prodIds = findCanViewProdIds();
    super.multipleCondition(Lists.newArrayList("sys_out_pond_id", "sys_in_pond_id"), prodIds);
  }
}
