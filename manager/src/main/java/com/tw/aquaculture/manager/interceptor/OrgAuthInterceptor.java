package com.tw.aquaculture.manager.interceptor;

import com.bizunited.platform.core.repository.dataview.analysis.SqlAnalysis;

import java.util.List;

/**
 * 组织机构id权限筛选拦截器
 * 原sql需要带 sys_org_id
 *
 * @author zizhou
 * @date 2019/12/30
 */
public class OrgAuthInterceptor extends AbsDataViewAuthInterceptor {
  @Override
  public void doIntercept(SqlAnalysis sqlAnalysis) {
    if (hasAdminRole()) {
      return;
    }

    // 当前岗位可以查看的组织机构id
    List<String> orgIds = super.findCanViewOrgIds();
    super.simpleCondition("sys_org_id", orgIds);
  }
}
