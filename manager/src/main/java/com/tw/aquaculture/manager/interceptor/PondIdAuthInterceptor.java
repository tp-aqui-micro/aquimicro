package com.tw.aquaculture.manager.interceptor;

import com.bizunited.platform.core.repository.dataview.analysis.SqlAnalysis;

import java.util.List;

/**
 * 塘口id权限拦截器
 * 要求当前数据视图 sql查询列必须带有 sys_pond_id
 *
 * @author zizhou
 * @date 2019/12/27
 */
public class PondIdAuthInterceptor extends AbsDataViewAuthInterceptor {

  @Override
  public void doIntercept(SqlAnalysis sqlAnalysis) {
    if (hasAdminRole()) {
      return;
    }
    // 当前用户能够看到的塘口信息
    List<String> prodIds = findCanViewProdIds();

    super.simpleCondition("sys_pond_id", prodIds);
  }
}