package com.tw.aquaculture.manager.interceptor;

import com.bizunited.platform.core.repository.dataview.analysis.SqlAnalysis;

import java.util.List;

/**
 * 仓库id权限拦截器
 * 要求当前数据视图 sql查询列必须带有 store_id
 *
 * @author zizhou
 * @date 2019/12/27
 */
public class StoreIdAuthInterceptor extends AbsDataViewAuthInterceptor {

  @Override
  public void doIntercept(SqlAnalysis sqlAnalysis) {
    if (hasAdminRole()) {
      return;
    }
    // 当前用户能够看到的塘口信息
    List<String> storeIds = findCanViewStoreIds();
    super.simpleCondition("sys_store_id", storeIds);
  }
}