package com.tw.aquaculture.manager.interceptor;

import com.bizunited.platform.core.repository.dataview.analysis.SqlAnalysis;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * 专用于转库数据视图的权限拦截器
 *
 * @author zizhou
 * @date 2019/12/29
 */
public class TransferStoreAuthInterceptor extends AbsDataViewAuthInterceptor {
  @Override
  public void doIntercept(SqlAnalysis sqlAnalysis) {
    if (hasAdminRole()) {
      return;
    }
    // 当前用户能够看到的塘口信息
    List<String> prodIds = findCanViewStoreIds();
    super.multipleCondition(Lists.newArrayList("sys_out_store_id", "sys_in_store_id"), prodIds);
  }
}
