package com.tw.aquaculture.manager.orgpondmap.repository;

import com.tw.aquaculture.common.entity.process.OutApplyCustomStateEntity;
import com.tw.aquaculture.manager.orgpondmap.repository.internal.OutApplyCustomStateEntityRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * OutFishApplyEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_OutApplyCustomStateEntityRepository")
public interface OutApplyCustomStateEntityRepository
    extends
      JpaRepository<OutApplyCustomStateEntity, String>
      ,JpaSpecificationExecutor<OutApplyCustomStateEntity>
      , OutApplyCustomStateEntityRepositoryCustom
  {

  }