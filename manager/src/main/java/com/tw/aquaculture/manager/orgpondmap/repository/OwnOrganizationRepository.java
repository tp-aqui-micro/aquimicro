package com.tw.aquaculture.manager.orgpondmap.repository;

import com.bizunited.platform.core.entity.OrganizationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * OwnRepository
 *
 * @description:
 * @author: yanwe
 * @date: 27/Dec/2019 16:55
 */
@Repository("OwnOrganizationRepository")
public interface OwnOrganizationRepository extends JpaRepository<OrganizationEntity, String>, JpaSpecificationExecutor<OrganizationEntity> {

  @Query("select distinct org from OrganizationEntity org where org.parent is null ")
  Set<OrganizationEntity> findTop();



  @Query("select distinct org from OrganizationEntity org left join fetch org.parent op  where op.id = :parentId ")
  Set<OrganizationEntity> findByParentId(@Param("parentId") String parentId);


}
