package com.tw.aquaculture.manager.orgpondmap.repository;

import com.bizunited.platform.core.entity.PositionEntity;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * OwnPositionRepository
 *
 * @description:
 * @author: yanwe
 * @date: 28/Dec/2019 11:42
 */
@Repository("OwnPositionRepository")
public interface OwnPositionRepository extends JpaRepository<PositionEntity,String>, JpaSpecificationExecutor<PositionEntity> {


  @Query("select distinct p from PositionEntity p left join fetch p.organization")
  Set<PositionEntity> findAllDetail();

  /**
   * 查询所有数据 只返回id和父级id
   *
   * 等于原生sql的 (select id,parent_id from engine_position)
   */
  @Query("SELECT new org.apache.commons.lang3.tuple.ImmutablePair(p.id, p.parent.id) FROM PositionEntity p")
  List<Pair<String, String>> findAllPositionIdAndParentId();
}
