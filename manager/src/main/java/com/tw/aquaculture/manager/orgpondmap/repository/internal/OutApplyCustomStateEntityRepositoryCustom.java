package com.tw.aquaculture.manager.orgpondmap.repository.internal;

import com.tw.aquaculture.common.entity.process.OutApplyCustomStateEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * @author: zengxingwang
 * @date: 2019/12/31 14:31
 * @description:
 */
public interface OutApplyCustomStateEntityRepositoryCustom {

  /**
   * 移动端根据基地名称、塘口名称、时间分页查询出鱼申请信息
   * @param params
   * @param pageable
   * @return
   */
  public Page<OutApplyCustomStateEntity> findDetailByOrgBaseNameOrPondNameOrTiem(Map<String, Object> params, Pageable pageable);
}
