package com.tw.aquaculture.manager.orgpondmap.repository.internal;

import com.tw.aquaculture.common.entity.process.OutApplyCustomStateEntity;
import com.tw.aquaculture.common.entity.process.OutFishApplyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author: zengxingwang
 * @date: 2019/12/31 14:31
 * @description:
 */
@Repository("OutApplyCustomStateEntityRepositoryCustom")
public class OutApplyCustomStateEntityRepositoryImpl implements OutApplyCustomStateEntityRepositoryCustom {

  @Autowired
  EntityManager entityManager;

  /**
   * 移动端根据基地名称、塘口名称、时间分页查询出鱼申请信息
   *
   * @param params
   * @param pageable
   * @return
   */
  @Override
  public Page<OutApplyCustomStateEntity> findDetailByOrgBaseNameOrPondNameOrTiem(Map<String, Object> params, Pageable pageable) {
    StringBuilder hql = new StringBuilder("select distinct out from OutApplyCustomStateEntity out");
    hql.append(" left join fetch out.outFishApply apply ");
    hql.append(" left join fetch apply.pond pond ");
    hql.append(" left join fetch out.customer ");
    hql.append(" where 1=1 ");
    StringBuilder countHql = new StringBuilder("select count(distinct out) from OutApplyCustomStateEntity out ");
    countHql.append(" left join out.outFishApply apply ");
    countHql.append(" left join apply.pond pond ");
    countHql.append(" left join out.customer");
    countHql.append(" where 1=1 ");
    StringBuilder condition = new StringBuilder();
    if (params != null) {
      if (params.get("pondId") != null) {
        condition.append(" and pond.id = :pondId");
      }
      if (params.get("start") != null) {
        condition.append(" and apply.applyTime > :start ");
      }
      if (params.get("end") != null) {
        condition.append(" and apply.applyTime < :end ");
      }
    }
    countHql.append(condition.toString());
    hql.append(condition.toString());
    Query query = entityManager.createQuery(hql.toString());
    Query countQuery = entityManager.createQuery(countHql.toString());
    if (params != null) {
      params.forEach((k, v) -> {
        if (v != null) {
          query.setParameter(k, v);
          countQuery.setParameter(k, v);
        }
      });
    }
    query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
    query.setMaxResults(pageable.getPageSize());
    List<OutFishApplyEntity> result = new ArrayList<>();
    long count = (long) countQuery.getResultList().get(0);
    if (count > 0) {
      result = query.getResultList();
    }
    return new PageImpl(result, pageable, count);
  }
}
