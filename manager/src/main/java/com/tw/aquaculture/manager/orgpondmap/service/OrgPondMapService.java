package com.tw.aquaculture.manager.orgpondmap.service;

import java.util.List;

/**
 * OrgPondMapService 组织机构-塘口从属关系处理
 *
 * @description:
 * @author: yanwe
 * @date: 28/Dec/2019 14:43
 */
public interface OrgPondMapService {

  /**
   * 定期执行更新，更新对应关系
   */
  void flush();

  /**
   * 根据岗位信息，查询当前岗位关联的组织机构下的所有组织机构ID
   * @param posId
   * @return
   */
  List<String> findOrgByPos(String posId);

  /**
   * 根据岗位信息，查询当前岗位关联的组织机构的父级组织下的所有组织机构ID
   * @param posId
   * @return
   */
  List<String> findOrgByParentPos(String posId);

  /**
   * 根据组织机构信息，查询当前组织机构下的塘口ID
   * @param orgId
   * @return
   */
  List<String> findPondsByOrg(String orgId);

  /**
   * 根据组织机构信息，查询当前组织机构的父级组织机构下的塘口ID
   * @param orgId
   * @return
   */
  List<String> findPondsByParentOrg(String orgId);

  /**
   * 根据岗位信息，查询当前岗位关联的组织机构下的塘口ID
   * @param posId
   * @return
   */
  List<String> findPondsByPos(String posId);

  /**
   * 根据岗位信息，查询当前岗位关联的组织机构的父级组织下的塘口ID
   * @param posId
   * @return
   */
  List<String> findPondsByParentPos(String posId);

  /**
   * 根据组织机构信息，查询当前组织机构下的仓库ID
   * @param orgId
   * @return
   */
  List<String> findStoresByOrg(String orgId);

  /**
   * 根据组织机构信息，查询当前组织机构的父级组织机构下的仓库ID
   * @param orgId
   * @return
   */
  List<String> findStoresByParentOrg(String orgId);

  /**
   * 根据岗位信息，查询当前岗位关联的组织机构下的仓库ID
   * @param posId
   * @return
   */
  List<String> findStoresByPos(String posId);

  /**
   * 根据岗位信息，查询当前岗位关联的组织机构的父级组织下的仓库ID
   * @param posId
   * @return
   */
  List<String> findStoresByParentPos(String posId);


}
