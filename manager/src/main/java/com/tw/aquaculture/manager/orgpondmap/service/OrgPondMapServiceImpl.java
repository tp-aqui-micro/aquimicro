package com.tw.aquaculture.manager.orgpondmap.service;

import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.core.entity.PositionEntity;
import com.bizunited.platform.rbac.server.service.OrganizationService;
import com.bizunited.platform.rbac.server.vo.OrganizationVo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.StoreEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.common.entity.base.TwGroupEntity;
import com.tw.aquaculture.manager.service.common.OwnOrganizationService;
import com.tw.aquaculture.manager.service.common.OwnPositionService;
import com.tw.aquaculture.manager.service.remote.PondEntityService;
import com.tw.aquaculture.manager.service.remote.StoreEntityService;
import com.tw.aquaculture.manager.service.remote.TwBaseEntityService;
import com.tw.aquaculture.manager.service.remote.TwGroupEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * OrgPondMap
 *
 * @description: 组织机构-塘口 关系表
 * @author: yanwe
 * @date: 27/Dec/2019 14:36
 */
@Service
public class OrgPondMapServiceImpl implements OrgPondMapService {

  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(OrgPondMapServiceImpl.class);
  /*
   * 整理逻辑：
   * 整理所有组织机构与该组织机构下塘口的ID对应MAP，存入Redis当中。
   * KV示例：(String-组织机构ID,List(String)-该组织下所有塘口ID)
   * 通威示例组织机构树： 通威集团-通威股份-XXX小组-XXX公司-|XXX部门|-XXX基地-|XXX养殖组| （其中部门与养殖组可能没有）
   * 其中，基地与养殖组含有额外实体信息（TwBaseEntity, TwGroupEntity）,代码上为一对多结构，但目前业务上为一对一结构
   * 塘口必然归属于某基地，可能关联某养殖组
   * 仓库必然归属于某基地
   */
  @Autowired private OwnOrganizationService ownOrganizationService;
  @Autowired private OrganizationService organizationService;
  @Autowired private PondEntityService pondEntityService;
  @Autowired private TwBaseEntityService twBaseEntityService;
  @Autowired private TwGroupEntityService twGroupEntityService;
  @Autowired private RedissonClient redissonClient;
  @Autowired private OwnPositionService ownPositionService;
  @Autowired private StoreEntityService storeEntityService;

  // 组织-组织
  private final static String ORG_ORG_MAP = "ORG_ORG_MAP";
  // 岗位-组织
  private final static String POS_ORG_MAP = "POS_ORG_MAP";
  // 组织-塘口
  private final static String ORG_POND_MAP = "ORG_POND_MAP";
  // 岗位-塘口
  private final static String POS_POND_MAP = "POS_POND_MAP";
  // 组织-仓库
  private final static String ORG_STORE_MAP = "ORG_STORE_MAP";
  // 岗位-仓库
  private final static String POS_STORE_MAP = "POS_STORE_MAP";

  /**
   * 定期执行更新，更新对应关系
   */
  @Override
  public void flush() {
    // 查询出的twGroups含有组织机构org信息
    List<TwGroupEntity> twGroups = twGroupEntityService.findAll();
    // 查询出的twBase含有组织机构org信息
    List<TwBaseEntity> twBases = twBaseEntityService.findDetailAll();
    // 更新组织机构-组织机构
    Map<String,List<String>> orgOrgMap = this.findOrgOrgMap(twGroups,twBases);
    RMap<String,List<String>> orgOrgRedisMap = redissonClient.getMap(ORG_ORG_MAP);
    orgOrgRedisMap.putAll(orgOrgMap);
    // 更新岗位-组织机构
    Map<String,List<String>> posOrgMap = this.findPosOrgMap(orgOrgMap);
    RMap<String,List<String>> posOrgRedisMap = redissonClient.getMap(POS_ORG_MAP);
    posOrgRedisMap.putAll(posOrgMap);
    //更新组织机构-塘口
    Map<String,List<String>> orgPondMap = this.findOrgPondMap(twGroups,twBases);
    RMap<String,List<String>> orgPondRedisMap = redissonClient.getMap(ORG_POND_MAP);
    orgPondRedisMap.putAll(orgPondMap);
    // 查询岗位 - 所有关联的岗位id map
    Map<String, Set<String>> positionAndSubPositionMap = findPositionAndSubPositionMap();
    // 更新岗位-塘口
    Map<String,List<String>> posPondMap = this.findPosPondMap(orgPondMap);
    Map<String, List<String>> allPosPondMap = this.transToAllPosMap(positionAndSubPositionMap, posPondMap);
    RMap<String,List<String>> posPondRedisMap = redissonClient.getMap(POS_POND_MAP);
    posPondRedisMap.putAll(allPosPondMap);
    // 更新组织机构-仓库
    Map<String,List<String>> orgStoreMap = this.findOrgStoreMap(twGroups,twBases);
    RMap<String,List<String>> orgStoreRedisMap = redissonClient.getMap(ORG_STORE_MAP);
    orgStoreRedisMap.putAll(orgStoreMap);
    // 更新岗位-仓库
    Map<String,List<String>> posStoreMap = this.findPosStoreMap(orgStoreMap);
    Map<String, List<String>> allPosStoreMap = this.transToAllPosMap(positionAndSubPositionMap, posStoreMap);
    RMap<String,List<String>> posStoreRedisMap = redissonClient.getMap(POS_STORE_MAP);
    posStoreRedisMap.putAll(allPosStoreMap);
    LOGGER.info("更新组织机构，岗位对应塘口，仓库的关系成功。");
  }

  @Override
  public List<String> findOrgByPos(String posId) {
    if(StringUtils.isBlank(posId)) return new ArrayList<>();
    RMap<String,List<String>> posOrgRedisMap = redissonClient.getMap(POS_ORG_MAP);
    List<String> orgIds = posOrgRedisMap.get(posId);
    if(CollectionUtils.isEmpty(orgIds)) return new ArrayList<>();
    return orgIds;
  }

  @Override
  public List<String> findOrgByParentPos(String posId) {
    if(StringUtils.isBlank(posId)) return new ArrayList<>();
    PositionEntity position = this.ownPositionService.findDetailsById(posId);
    if(null == position.getId() || null == position.getOrganization()) return new ArrayList<>();
    String orgId = position.getOrganization().getId();
    if(StringUtils.isBlank(orgId)) return new ArrayList<>();
    OrganizationVo parentOrg = this.organizationService.findByChild(orgId);
    // 如果该组织机构已经为顶节点，则查询他自身
    String parentId = null == parentOrg ? orgId : parentOrg.getId();
    RMap<String,List<String>> orgOrgRedisMap = redissonClient.getMap(ORG_ORG_MAP);
    List<String> orgIds = orgOrgRedisMap.get(parentId);
    if(CollectionUtils.isEmpty(orgIds)) return new ArrayList<>();
    return orgIds;
  }

  /**
   * 根据组织机构信息，查询当前组织机构下的塘口ID
   * @param orgId
   * @return
   */
  @Override
  public List<String> findPondsByOrg(String orgId){
    if(StringUtils.isBlank(orgId)) return new ArrayList<>();
    RMap<String,List<String>> orgPondRedisMap = redissonClient.getMap(ORG_POND_MAP);
    List<String> pondIds = orgPondRedisMap.get(orgId);
    if(CollectionUtils.isEmpty(pondIds)) return new ArrayList<>();
    return pondIds;
  }

  /**
   * 根据组织机构信息，查询当前组织机构的父级组织机构下的塘口ID
   * @param orgId
   * @return
   */
  @Override
  public List<String> findPondsByParentOrg(String orgId){
    if(StringUtils.isBlank(orgId)) return new ArrayList<>();
    OrganizationVo parentOrg = this.organizationService.findByChild(orgId);
    // 如果该组织机构已经为顶节点，则查询他自身
    String parentId = null == parentOrg ? orgId : parentOrg.getId();
    RMap<String,List<String>> orgPondRedisMap = redissonClient.getMap(ORG_POND_MAP);
    List<String> pondIds = orgPondRedisMap.get(parentId);
    if(CollectionUtils.isEmpty(pondIds)) return new ArrayList<>();
    return pondIds;
  }

  /**
   * 根据岗位信息，查询当前岗位关联的组织机构下的塘口ID
   * @param posId
   * @return
   */
  @Override
  public List<String> findPondsByPos(String posId){
    if(StringUtils.isBlank(posId)) return new ArrayList<>();
    RMap<String,List<String>> posPondRedisMap = redissonClient.getMap(POS_POND_MAP);
    List<String> pondIds = posPondRedisMap.get(posId);
    if(CollectionUtils.isEmpty(pondIds)) return new ArrayList<>();
    return pondIds;
  }

  /**
   * 根据岗位信息，查询当前岗位关联的组织机构的父级组织下的塘口ID
   * @param posId
   * @return
   */
  @Override
  public List<String> findPondsByParentPos(String posId){
    if(StringUtils.isBlank(posId)) return new ArrayList<>();
    PositionEntity position = this.ownPositionService.findDetailsById(posId);
    if(null == position.getId() || null == position.getOrganization()) return new ArrayList<>();
    String orgId = position.getOrganization().getId();
    if(StringUtils.isBlank(orgId)) return new ArrayList<>();
    OrganizationVo parentOrg = this.organizationService.findByChild(orgId);
    // 如果该组织机构已经为顶节点，则查询他自身
    String parentId = null == parentOrg ? orgId : parentOrg.getId();
    RMap<String,List<String>> orgPondRedisMap = redissonClient.getMap(ORG_POND_MAP);
    List<String> pondIds = orgPondRedisMap.get(parentId);
    if(CollectionUtils.isEmpty(pondIds)) return new ArrayList<>();
    return pondIds;
  }

  /**
   * 根据组织机构信息，查询当前组织机构下的仓库ID
   * @param orgId
   * @return
   */
  @Override
  public List<String> findStoresByOrg(String orgId) {
    if(StringUtils.isBlank(orgId)) return new ArrayList<>();
    RMap<String,List<String>> orgStoreRedisMap = redissonClient.getMap(ORG_STORE_MAP);
    List<String> storeIds = orgStoreRedisMap.get(orgId);
    if(CollectionUtils.isEmpty(storeIds)) return new ArrayList<>();
    return storeIds;
  }

  /**
   * 根据组织机构信息，查询当前组织机构的父级组织机构下的仓库ID
   * @param orgId
   * @return
   */
  @Override
  public List<String> findStoresByParentOrg(String orgId) {
    if(StringUtils.isBlank(orgId)) return new ArrayList<>();
    OrganizationVo parentOrg = this.organizationService.findByChild(orgId);
    // 如果该组织机构已经为顶节点，则查询他自身
    String parentId = null == parentOrg ? orgId : parentOrg.getId();
    RMap<String,List<String>> orgStoreRedisMap = redissonClient.getMap(ORG_STORE_MAP);
    List<String> storeIds = orgStoreRedisMap.get(parentId);
    if(CollectionUtils.isEmpty(storeIds)) return new ArrayList<>();
    return storeIds;
  }

  /**
   * 根据岗位信息，查询当前岗位关联的组织机构下的仓库ID
   * @param posId
   * @return
   */
  @Override
  public List<String> findStoresByPos(String posId) {
    if(StringUtils.isBlank(posId)) return new ArrayList<>();
    RMap<String,List<String>> posStoreRedisMap = redissonClient.getMap(POS_STORE_MAP);
    List<String> storeIds = posStoreRedisMap.get(posId);
    if(CollectionUtils.isEmpty(storeIds)) return new ArrayList<>();
    return storeIds;
  }

  /**
   * 根据岗位信息，查询当前岗位关联的组织机构的父级组织下的仓库ID
   * @param posId
   * @return
   */
  @Override
  public List<String> findStoresByParentPos(String posId) {
    if(StringUtils.isBlank(posId)) return new ArrayList<>();
    PositionEntity position = this.ownPositionService.findDetailsById(posId);
    if(null == position.getId() || null == position.getOrganization()) return new ArrayList<>();
    String orgId = position.getOrganization().getId();
    if(StringUtils.isBlank(orgId)) return new ArrayList<>();
    OrganizationVo parentOrg = this.organizationService.findByChild(orgId);
    // 如果该组织机构已经为顶节点，则查询他自身
    String parentId = null == parentOrg ? orgId : parentOrg.getId();
    RMap<String,List<String>> orgStoreRedisMap = redissonClient.getMap(ORG_STORE_MAP);
    List<String> storeIds = orgStoreRedisMap.get(parentId);
    if(CollectionUtils.isEmpty(storeIds)) return new ArrayList<>();
    return storeIds;
  }

  /**
   * 查询岗位-塘口信息
   * @param orgPondMap 组织机构-塘口信息
   * @return
   */
  private Map<String,List<String>> findPosPondMap(Map<String,List<String>> orgPondMap) {
    Map<String,List<String>> posPondMap = new HashMap<>();
    if(CollectionUtils.isEmpty(orgPondMap)) return posPondMap;
    Set<PositionEntity> positions = this.ownPositionService.findAllDetails();
    if(CollectionUtils.isEmpty(positions)) return posPondMap;
    for(PositionEntity position : positions){
      OrganizationEntity org = position.getOrganization();
      if(null == org) continue;
      if(StringUtils.isBlank(org.getId())) continue;
      List<String> orgPonds = orgPondMap.get(org.getId());
      if(CollectionUtils.isEmpty(orgPonds)) continue;
      posPondMap.put(position.getId(),orgPonds);
    }
    return posPondMap;
  }

  /**
   * 查询 岗位id - 该岗位和所有关联的子岗位的id map
   */
  private Map<String, Set<String>> findPositionAndSubPositionMap() {
    // 查找 岗位id和父岗位id数据
    List<Pair<String, String>> positions = ownPositionService.findAllPositionIdAndParentId();
    if (CollectionUtils.isEmpty(positions)) {
      return Maps.newHashMap();
    }
    // 构建为层级结构, 方便后续解析
    List<PositionItem> positionItemList = buildPositionRank(positions);
    // 转换为 k:岗位id v:当前岗位id和所有下级岗位id  的map
    return transToPositionAndSubPositionMap(positionItemList);
  }

  /**
   * 原有的为当前 岗位id - 当前岗位所能看到的资源id  map
   * 通过这个方法, 转换为 岗位id - 当前岗位和它的所有下级岗位能够看到的资源id  map
   *
   * @param posIdMap 原有的 岗位和该岗位能够看到的资源id map
   * @param positionAndSubPositionMap 这是一个 岗位id - 该岗位和所有子岗位的map
   */
  private Map<String, List<String>> transToAllPosMap(Map<String, Set<String>> positionAndSubPositionMap, Map<String, List<String>> posIdMap) {
    if (CollectionUtils.isEmpty(posIdMap) || CollectionUtils.isEmpty(positionAndSubPositionMap)) {
      return posIdMap;
    }

    Map<String, List<String>> result = Maps.newHashMapWithExpectedSize(posIdMap.size());
    positionAndSubPositionMap.forEach((k, v) -> {
      Set<String> pondIdSet = Sets.newHashSet();
      // 拿到岗位对应的id
      for (String positionId : v) {
        List<String> pondIds = posIdMap.get(positionId);
        if (!CollectionUtils.isEmpty(pondIds)) {
          pondIdSet.addAll(pondIds);
        }
      }
      if (!CollectionUtils.isEmpty(pondIdSet)) {
        result.put(k, Lists.newArrayList(pondIdSet));
      }
    });

    return result;
  }

  /**
   * 转换为 k:岗位id v:当前岗位id和所有下级岗位id  的map
   */
  private Map<String, Set<String>> transToPositionAndSubPositionMap(List<PositionItem> positionItemList) {
    Map<String, Set<String>> result = Maps.newHashMap();
    for (PositionItem parent : positionItemList) {
      getAllPositionId(parent, result);
    }
    return result;
  }

  /**
   * 获取所有的子岗位id，包括自身的id
   */
  private Set<String> getAllPositionId(PositionItem parent, Map<String, Set<String>> map) {
    // 自身的岗位id
    Set<String> result = Sets.newHashSet(parent.positionId);

    if (!CollectionUtils.isEmpty(parent.childPositionIds)) {
      // 子岗位关联的岗位id
      for (PositionItem child : parent.childPositionIds) {
        Set<String> childPositions = getAllPositionId(child, map);
        result.addAll(childPositions);
      }
    }
    map.put(parent.positionId, result);
    return result;
  }


  /**
   * 转换为层级结构
   */
  private List<PositionItem> buildPositionRank(List<Pair<String, String>> positions) {
    List<PositionItem> positionItemList = Lists.newArrayList();
    Map<String, PositionItem> index = Maps.newHashMap();
    Set<String> allPositionSet = positions.stream().map(Pair::getLeft).collect(Collectors.toSet());

    // 先找到 top 层级的数据
    positions.forEach(positionPair -> {
      if (StringUtils.isBlank(positionPair.getRight()) || !allPositionSet.contains(positionPair.getRight())) {
        PositionItem positionItem = new PositionItem(positionPair.getLeft());
        positionItemList.add(positionItem);
        index.put(positionItem.positionId, positionItem);
      }
    });
    // 移除已经添加的数据
    positions.removeIf(pair -> index.containsKey(pair.getLeft()));

    // 循环构建层级结构
    do {
      for (Pair<String, String> positionPair : positions) {
        PositionItem positionItem = index.get(positionPair.getRight());
        if (positionItem == null) {
          continue;
        }
        PositionItem child = positionItem.addChild(positionPair.getLeft());
        index.put(child.positionId, child);
      }

      positions.removeIf(pair ->  index.containsKey(pair.getLeft()));
    } while (!CollectionUtils.isEmpty(positions));

    return positionItemList;
  }

  /**
   * 辅助对象, 描述岗位层级关系的每个节点对象
   */
  static class PositionItem {
    String positionId;
    Set<PositionItem> childPositionIds;

    PositionItem(String positionId) {
      this.positionId = positionId;
    }

    PositionItem addChild(String childPositionId) {
      if (childPositionIds == null) {
        childPositionIds = Sets.newHashSet();
      }
      PositionItem child = new PositionItem(childPositionId);
      childPositionIds.add(child);
      return child;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      PositionItem that = (PositionItem) o;

      return Objects.equals(positionId, that.positionId);
    }

    @Override
    public int hashCode() {
      return positionId != null ? positionId.hashCode() : 0;
    }
  }


  /**
   * 查询岗位-组织机构信息
   * @param orgOrgMap 组织机构-塘口信息
   * @return
   */
  private Map<String,List<String>> findPosOrgMap(Map<String,List<String>> orgOrgMap){
    Map<String,List<String>> posOrgMap = new HashMap<>();
    if(CollectionUtils.isEmpty(orgOrgMap)) return posOrgMap;
    Set<PositionEntity> positions = this.ownPositionService.findAllDetails();
    if(CollectionUtils.isEmpty(positions)) return posOrgMap;
    for(PositionEntity position : positions){
      OrganizationEntity org = position.getOrganization();
      if(null == org) continue;
      if(StringUtils.isBlank(org.getId())) continue;
      List<String> orgOrgs = orgOrgMap.get(org.getId());
      if(CollectionUtils.isEmpty(orgOrgs)) continue;
      posOrgMap.put(position.getId(),orgOrgs);
    }
    return posOrgMap;
  }

  /**
   * 查询岗位-仓库信息
   * @param orgStoreMap 组织机构-仓库信息
   * @return
   */
  private Map<String,List<String>> findPosStoreMap(Map<String,List<String>> orgStoreMap){
    Map<String,List<String>> posStoreMap = new HashMap<>();
    if(CollectionUtils.isEmpty(orgStoreMap)) return posStoreMap;
    Set<PositionEntity> positions = this.ownPositionService.findAllDetails();
    if(CollectionUtils.isEmpty(positions)) return posStoreMap;
    for(PositionEntity position : positions){
      OrganizationEntity org = position.getOrganization();
      if(null == org) continue;
      if(StringUtils.isBlank(org.getId())) continue;
      List<String> orgStores = orgStoreMap.get(org.getId());
      if(CollectionUtils.isEmpty(orgStores)) continue;
      posStoreMap.put(position.getId(),orgStores);
    }
    return posStoreMap;
  }



  /**
   * 查询所有组织机构-塘口，对应关系
   * @return
   */
  private Map<String,List<String>> findOrgPondMap(List<TwGroupEntity> twGroups, List<TwBaseEntity> twBases){
    /*
     * 逻辑：
     * 1先获取所有基地，养殖组的塘口信息
     * 2以树顶点为起始，向下递归处理所有节点
     */
    Map<String,List<String>> map = new HashMap<>();
    // 查询养殖组-基地的关系
    map = this.findGroupOrgPondMap(twGroups,map);
    // 查询基地-塘口的关系
    map = this.findBaseOrgPondMap(twBases,map);
    //查询所有顶级组织机构
    Set<OrganizationEntity> topOrgs = this.ownOrganizationService.findTopOrgs();
    // 从顶节点开始递归查询，获取所有组织机构塘口信息
    if(CollectionUtils.isEmpty(topOrgs)) return map;
    for(OrganizationEntity org : topOrgs){
      map = this.findOrgPondMapByOrgId(org.getId(),map);
    }
    return map;
  }

  /**
   * 查询所有组织机构-组织机构，对应关系
   * @return
   */
  private Map<String,List<String>> findOrgOrgMap(List<TwGroupEntity> twGroups, List<TwBaseEntity> twBases){
    /*
     * 逻辑：
     * 1先处理底节点，TW基地，TW养殖组的对应关系
     * 2以树顶点为起始，向下递归处理所有节点
     */
    Map<String,List<String>> map;
    map = this.findBaseAndGroupMap(twGroups,twBases);
    //查询所有顶级组织机构
    Set<OrganizationEntity> topOrgs = this.ownOrganizationService.findTopOrgs();
    // 从顶节点开始递归查询，获取所有组织机构信息
    if(CollectionUtils.isEmpty(topOrgs)) return map;
    for(OrganizationEntity org : topOrgs){
      map = this.findOrgOrgMapByOrgId(org.getId(),map);
    }
    return map;
  }

  /**
   * 查询养殖组，基地的组织机构对应关系
   * @param twGroups
   * @param twBases
   * @return
   */
  private Map<String,List<String>> findBaseAndGroupMap(List<TwGroupEntity> twGroups, List<TwBaseEntity> twBases){
    Map<String,List<String>> map = new HashMap<>();
    //养殖组为最底层单位,做一一对应关系
    if(!CollectionUtils.isEmpty(twGroups)){
      for(TwGroupEntity twGroup : twGroups){
        OrganizationEntity org = twGroup.getGroupOrg();
        if(null == org || StringUtils.isBlank(org.getId())) continue;
        map.put(org.getId(), Lists.newArrayList(org.getId()));
        //养殖组上级基地
        TwBaseEntity twBase = twGroup.getBase();
        if(null == twBase) continue;
        twBase = this.twBaseEntityService.findDetailsById(twBase.getId());
        if(null == twBase) continue;
        OrganizationEntity twBaseOrg = twBase.getBaseOrg();
        if(null == twBaseOrg) continue;
        List<String> parentBaseOrg = map.get(twBaseOrg.getId());
        if(CollectionUtils.isEmpty(parentBaseOrg)) parentBaseOrg = new ArrayList<>();
        parentBaseOrg.add(org.getId());
        map.put(twBaseOrg.getId(),parentBaseOrg);
      }
    }
    //基地的组织对应
    if(!CollectionUtils.isEmpty(twBases)){
      for(TwBaseEntity twBase : twBases){
        OrganizationEntity baseOrg = twBase.getBaseOrg();
        if(null == baseOrg || StringUtils.isBlank(baseOrg.getId())) {
          continue;
        }
        List<String> baseOrgMap = map.get(baseOrg.getId());
        if(CollectionUtils.isEmpty(baseOrgMap)) baseOrgMap = new ArrayList<>();
        baseOrgMap.add(baseOrg.getId());
        map.put(baseOrg.getId(),baseOrgMap);
      }
    }
    return map;
  }

  /**
   * 递归遍历，自上往下查询 组织机构-塘口信息
   * @param orgId
   * @param map
   */
  private Map<String,List<String>> findOrgPondMapByOrgId(String orgId,Map<String,List<String>> map){
    /*
     *  递归逻辑：
     *  如果当前查询组织机构是否已在MAP中保存塘口信息
     *  --有：返回
     *  --无：查询出所有当前组织机构的下级组织，遍历所有子级机构，
     *     -- 所有子级机构都有塘口信息（或该组织机构无子节点），累加得到父级机构塘口信息，返回
     *     -- 执行本方法，查询子级组织机构的塘口信息
     */
    List<String> orgPondsId = map.get(orgId);
    // 如果该节点已有数据，则返回
    if(!CollectionUtils.isEmpty(orgPondsId)) return map;
    // 查询所有子级节点
    Set<OrganizationEntity> childOrgs = this.ownOrganizationService.findByParent(orgId);
    // 如果该节点无子节点，返回
    if(CollectionUtils.isEmpty(childOrgs)) return map;
    // 遍历所有子节点
    List<String> parentPonds = new ArrayList<>();
    for(OrganizationEntity childOrg : childOrgs){
      String childId = childOrg.getId();
      List<String> childPonds = map.get(childId);
      // 如果该节点已有数据，则累加至父节点
      if(!CollectionUtils.isEmpty(childPonds)) {
        parentPonds.addAll(childPonds);
        continue;
      }
      // 如果子节点无数据，则递归该子节点
      map = this.findOrgPondMapByOrgId(childId,map);
      // 重新获取子节点数据，累加
      childPonds = map.get(childId);
      if(CollectionUtils.isEmpty(childPonds)) continue;
      parentPonds.addAll(childPonds);
    }
    // 将存入当前节点数据
    map.put(orgId,parentPonds);
    return map;
  }

  /**
   * 递归遍历，自上往下查询 组织机构-组织机构信息
   * @param orgId
   * @param map
   */
  private Map<String,List<String>> findOrgOrgMapByOrgId(String orgId,Map<String,List<String>> map){
    /*
     *  递归逻辑：
     *  如果当前查询组织机构是否已在MAP中保存组织机构信息
     *  --有：返回
     *  --无：查询出所有当前组织机构的下级组织，遍历所有子级机构，
     *     -- 所有子级机构都有组织机构信息（或该组织机构无子节点），累加得到父级机构组织机构信息，返回
     *     -- 执行本方法，查询子级组织机构的组织机构信息
     */
    List<String> orgOrgsId = map.get(orgId);
    // 如果该节点已有数据，则返回
    if(!CollectionUtils.isEmpty(orgOrgsId)) return map;
    // 查询所有子级节点
    Set<OrganizationEntity> childOrgs = this.ownOrganizationService.findByParent(orgId);
    // 如果该节点无子节点，返回
    if(CollectionUtils.isEmpty(childOrgs)) return map;
    // 遍历所有子节点
    List<String> parentOrgs = new ArrayList<>();
    for(OrganizationEntity childOrg : childOrgs){
      String childId = childOrg.getId();
      List<String> childOrgIds = map.get(childId);
      // 如果该节点已有数据，则累加至父节点
      if(!CollectionUtils.isEmpty(childOrgIds)) {
        parentOrgs.addAll(childOrgIds);
        continue;
      }
      // 如果子节点无数据，则递归该子节点
      map = this.findOrgOrgMapByOrgId(childId,map);
      // 重新获取子节点数据，累加
      childOrgIds = map.get(childId);
      if(CollectionUtils.isEmpty(childOrgIds)) continue;
      parentOrgs.addAll(childOrgIds);
    }
    parentOrgs = parentOrgs.stream().distinct().collect(Collectors.toList());
    // 将存入当前节点数据
    map.put(orgId,parentOrgs);
    return map;
  }


  /**
   * 递归遍历，自上往下查询 组织机构-仓库信息
   * @param orgId
   * @param map
   */
  private Map<String,List<String>> findOrgStoreMapByOrgId(String orgId,Map<String,List<String>> map){
    /*
     *  递归逻辑：
     *  如果当前查询组织机构是否已在MAP中保存仓库信息
     *  --有：返回
     *  --无：查询出所有当前组织机构的下级组织，遍历所有子级机构，
     *     -- 所有子级机构都有仓库信息（或该组织机构无子节点），累加得到父级机构仓库信息，返回
     *     -- 执行本方法，查询子级组织机构的仓库信息
     */
    List<String> orgStoresId = map.get(orgId);
    // 如果该节点已有数据，则返回
    if(!CollectionUtils.isEmpty(orgStoresId)) return map;
    // 查询所有子级节点
    Set<OrganizationEntity> childOrgs = this.ownOrganizationService.findByParent(orgId);
    // 如果该节点无子节点，返回
    if(CollectionUtils.isEmpty(childOrgs)) return map;
    // 遍历所有子节点
    List<String> parentStores = new ArrayList<>();
    for(OrganizationEntity childOrg : childOrgs){
      String childId = childOrg.getId();
      List<String> childStores = map.get(childId);
      // 如果该节点已有数据，则累加至父节点
      if(!CollectionUtils.isEmpty(childStores)) {
        parentStores.addAll(childStores);
        continue;
      }
      // 如果子节点无数据，则递归该子节点
      map = this.findOrgStoreMapByOrgId(childId,map);
      // 重新获取子节点数据，累加
      childStores = map.get(childId);
      if(CollectionUtils.isEmpty(childStores)) continue;
      parentStores.addAll(childStores);
    }
    // 因为养殖组继承自基地的仓库信息，所以需要去重
    parentStores = parentStores.stream().distinct().collect(Collectors.toList());
    // 将存入当前节点数据
    map.put(orgId,parentStores);
    return map;
  }


  /**
   * 查询所有养殖组-塘口的对应关系，结果为 养殖组orgId - List塘口Id
   * @return
   */
  private Map<String,List<String>> findGroupOrgPondMap(List<TwGroupEntity> twGroups,Map<String,List<String>> map){
    /*
     *  代码逻辑：
     *  1查询出所有养殖组
     *  2遍历所有养殖组，查询养殖组下属的塘口
     *  3获取该养殖组的orgId，将对应关系更新
     */
    if(CollectionUtils.isEmpty(map)) map = new HashMap<>();
    if(CollectionUtils.isEmpty(twGroups)) return map;
    // 传入的养殖组信息已经含有Org信息
    for(TwGroupEntity twGroup : twGroups){
      if(null == twGroup) continue;
      OrganizationEntity twGroupOrg = twGroup.getGroupOrg();
      if(null == twGroupOrg || StringUtils.isBlank(twGroupOrg.getId())) continue;
      List<PondEntity> ponds = this.pondEntityService.findPondsByTwGroup(twGroup.getId());
      List<String> pondsIds = ponds.stream().map(o -> o.getId()).filter(o -> StringUtils.isNotBlank(o)).collect(Collectors.toList());
      map.put(twGroupOrg.getId(),pondsIds);
    }
    return map;
  }

  /**
   * 查询所有基地-塘口的对应关系，结果为 基地orgId - List塘口ID
   * @param twBases
   * @param map
   * @return
   */
  private Map<String,List<String>> findBaseOrgPondMap(List<TwBaseEntity> twBases,Map<String,List<String>> map){
    /*
     *  代码逻辑：
     *  1查询出所有基地
     *  2遍历所有基地，查询基地下属的塘口
     *  3获取该基地的orgId，将对应关系更新
     */
    if(CollectionUtils.isEmpty(map)) map = new HashMap<>();
    if(CollectionUtils.isEmpty(twBases)) return map;
    // 传入的基地信息已经含有Org,Ponds信息
    for(TwBaseEntity twBase : twBases){
      if(null == twBase) continue;
      OrganizationEntity baseOrg = twBase.getBaseOrg();
      if(null == baseOrg || StringUtils.isBlank(baseOrg.getId())) continue;
      Set<PondEntity> ponds = twBase.getPonds();
      List<String> pondsIds = ponds.stream().map(o -> o.getId()).filter(o -> StringUtils.isNotBlank(o)).collect(Collectors.toList());
      map.put(baseOrg.getId(),pondsIds);
    }
    return map;
  }

  /**
   * 查询TW基地-仓库的对应关系
   * @param map
   * @return
   */
  private Map<String,List<String>> findBaseOrgStoreMap(List<TwBaseEntity> twBases,Map<String,List<String>> map){
    /*
     *  代码逻辑：
     *  1查询出所有基地
     *  2遍历所有基地，查询基地下属的仓库
     *  3获取该基地的orgId，将对应关系更新
     */
    if(CollectionUtils.isEmpty(map)) map = new HashMap<>();
    if(CollectionUtils.isEmpty(twBases)) return map;
    // 传入的基地信息已经含有Org,Store信息
    for(TwBaseEntity twBase : twBases){
      if(null == twBase) continue;
      OrganizationEntity baseOrg = twBase.getBaseOrg();
      if(null == baseOrg || StringUtils.isBlank(baseOrg.getId())) continue;
      List<StoreEntity> stores = storeEntityService.findStoresByBaseId(twBase.getId());
      if(CollectionUtils.isEmpty(stores)) continue;
      List<String> storeIds = stores.stream().map(o -> o.getId()).filter(o -> StringUtils.isNotBlank(o)).collect(Collectors.toList());
      map.put(baseOrg.getId(),storeIds);
    }
    return map;
  }

  /**
   * 更新组织机构-仓库
   * @param twGroups
   * @param twBases
   * @return
   */
  private Map<String,List<String>> findOrgStoreMap(List<TwGroupEntity> twGroups, List<TwBaseEntity> twBases){
    /*
     * 逻辑：
     * 1先获取所有基地，养殖组的仓库信息
     * 2注意，因为所有仓库都绑定于基地，而非养殖组，所以养殖组的仓库信息都继承自基地。
     * 2以树顶点为起始，向下递归处理所有节点
     */
    Map<String,List<String>> map = new HashMap<>();
    // 查询基地-仓库的关系
    map = this.findBaseOrgStoreMap(twBases,map);
    // 查询养殖组-仓库的关系
    map = this.findGroupOrgStoreMap(map,twGroups);
    //查询所有顶级组织机构
    Set<OrganizationEntity> topOrgs = this.ownOrganizationService.findTopOrgs();
    // 从顶节点开始递归查询，获取所有组织机构塘口信息
    if(CollectionUtils.isEmpty(topOrgs)) return map;
    for(OrganizationEntity org : topOrgs){
      map = this.findOrgStoreMapByOrgId(org.getId(),map);
    }
    return map;
  }

  /**
   * 查询养殖组-仓库的对应关系，注意，因为所有仓库都绑定于基地，而非养殖组，所以养殖组的仓库信息都继承自基地。
   * @param map 已经含有基地-仓库的对应关系
   * @param twGroups
   * @return
   */
  private Map<String,List<String>> findGroupOrgStoreMap(Map<String,List<String>> map,List<TwGroupEntity> twGroups){
    if(CollectionUtils.isEmpty(map)) return map;
    if(CollectionUtils.isEmpty(twGroups)) return map;
    for(TwGroupEntity twGroup : twGroups){
      TwBaseEntity twBase = twGroup.getBase();
      if(null == twBase || StringUtils.isBlank(twBase.getId())) continue;
      List<String> baseStoreIds = map.get(twBase.getId());
      if(CollectionUtils.isEmpty(baseStoreIds)) continue;
      map.put(twGroup.getId(),baseStoreIds);
    }
    return map;
  }



}
