package com.tw.aquaculture.manager.repository;

import com.bizunited.platform.core.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 水产扩展的user repository
 */
@Repository
public interface AquiUserRepository extends JpaRepository<UserEntity, String> {

    List<UserEntity> findByUserName(String username);
}
