package com.tw.aquaculture.manager.service.check.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.check.CheckFishEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.check.CheckFishFeign;
import com.tw.aquaculture.manager.service.check.CheckFishEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * CheckFishEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("CheckFishEntityServiceImpl")
public class CheckFishEntityServiceImpl implements CheckFishEntityService {

  @Autowired
  private CheckFishFeign checkFishFeign;

  @Transactional
  @Override
  public CheckFishEntity create(CheckFishEntity checkFishEntity) {
    ResponseModel responseModel = checkFishFeign.create(checkFishEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckFishEntity>() {
    });
  }

  @Transactional
  @Override
  public CheckFishEntity createForm(CheckFishEntity checkFishEntity) {
    return null;
  }

  @Transactional
  @Override
  public CheckFishEntity update(CheckFishEntity checkFishEntity) {
    ResponseModel responseModel = checkFishFeign.update(checkFishEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckFishEntity>() {
    });
  }

  @Transactional
  @Override
  public CheckFishEntity updateForm(CheckFishEntity checkFishEntity) {
    return null;
  }

  @Override
  public CheckFishEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = checkFishFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckFishEntity>() {
    });
  }

  @Override
  public CheckFishEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = checkFishFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckFishEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    if (StringUtils.isBlank(id)) {
      return;
    }
    checkFishFeign.deleteById(id);
  }

  @Override
  public CheckFishEntity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = checkFishFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckFishEntity>() {
    });
  }

  @Override
  public CheckFishEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = checkFishFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckFishEntity>() {
    });
  }
} 
