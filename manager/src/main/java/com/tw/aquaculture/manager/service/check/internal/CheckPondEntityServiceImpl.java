package com.tw.aquaculture.manager.service.check.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.check.CheckPondEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.check.CheckPondFeign;
import com.tw.aquaculture.manager.service.check.CheckPondEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Map;

/**
 * CheckPondEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("CheckPondEntityServiceImpl")
public class CheckPondEntityServiceImpl implements CheckPondEntityService {
  @Autowired
  private CheckPondFeign checkPondFeign;

  @Transactional
  @Override
  public CheckPondEntity create(CheckPondEntity checkPondEntity) {
    ResponseModel responseModel = checkPondFeign.create(checkPondEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckPondEntity>() {
    });
  }

  @Transactional
  @Override
  public CheckPondEntity createForm(CheckPondEntity checkPondEntity) {
    return null;
  }

  @Transactional
  @Override
  public CheckPondEntity update(CheckPondEntity checkPondEntity) {
    ResponseModel responseModel = checkPondFeign.update(checkPondEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckPondEntity>() {
    });
  }

  @Transactional
  @Override
  public CheckPondEntity updateForm(CheckPondEntity checkPondEntity) {
    return null;
  }

  @Override
  public CheckPondEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = checkPondFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckPondEntity>() {
    });
  }

  @Override
  public CheckPondEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = checkPondFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckPondEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    if (StringUtils.isBlank(id)) {
      return;
    }
    checkPondFeign.deleteById(id);
  }

  @Override
  public CheckPondEntity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = checkPondFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckPondEntity>() {
    });
  }

  @Override
  public CheckPondEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = checkPondFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckPondEntity>() {
    });
  }

  @Override
  public Map<String, Object> findCountByPondAndUser(String pondId, String username) {
    if (StringUtils.isBlank(pondId) || StringUtils.isBlank(username)) {
      return null;
    }
    ResponseModel responseModel = checkPondFeign.findCountByPondAndUser(pondId, username);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Map<String, Object>>() {
    });
  }

  @Override
  public int checkPondCount(String pondId, String state) {
    if (StringUtils.isBlank(pondId) || StringUtils.isBlank(state)) {
      return 0;
    }
    return checkPondFeign.checkPondCount(pondId, state);
  }

  @Override
  public CheckPondEntity findLastCheckPondByPondId(String pondId) {
    if (StringUtils.isBlank(pondId)) {
      return null;
    }
    ResponseModel responseModel = checkPondFeign.findLastCheckPondByPondId(pondId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckPondEntity>() {
    });
  }
} 
