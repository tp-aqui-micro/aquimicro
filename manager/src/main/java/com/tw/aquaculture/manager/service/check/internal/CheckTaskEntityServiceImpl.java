package com.tw.aquaculture.manager.service.check.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.check.CheckTaskEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.check.CheckTaskFeign;
import com.tw.aquaculture.manager.service.check.CheckTaskEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * CheckTaskEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("CheckTaskEntityServiceImpl")
public class CheckTaskEntityServiceImpl implements CheckTaskEntityService {
  @Autowired
  private CheckTaskFeign checkTaskFeign;

  @Transactional
  @Override
  public CheckTaskEntity create(CheckTaskEntity checkTaskEntity) {
    ResponseModel responseModel = checkTaskFeign.create(checkTaskEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckTaskEntity>() {
    });
  }

  @Transactional
  @Override
  public CheckTaskEntity createForm(CheckTaskEntity checkTaskEntity) {
    return null;
  }

  @Transactional
  @Override
  public CheckTaskEntity update(CheckTaskEntity checkTaskEntity) {
    ResponseModel responseModel = checkTaskFeign.update(checkTaskEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckTaskEntity>() {
    });
  }

  @Transactional
  @Override
  public CheckTaskEntity updateForm(CheckTaskEntity checkTaskEntity) {
    return null;
  }

  @Override
  public CheckTaskEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = checkTaskFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckTaskEntity>() {
    });
  }

  @Override
  public CheckTaskEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = checkTaskFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckTaskEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    if (StringUtils.isBlank(id)) {
      return;
    }
    checkTaskFeign.deleteById(id);
  }

  @Override
  public CheckTaskEntity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = checkTaskFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckTaskEntity>() {
    });
  }

  @Override
  public CheckTaskEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = checkTaskFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckTaskEntity>() {
    });
  }
} 
