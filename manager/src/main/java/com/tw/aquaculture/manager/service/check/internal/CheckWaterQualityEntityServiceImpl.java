package com.tw.aquaculture.manager.service.check.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.check.CheckWaterQualityEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.check.CheckWaterQualityFeign;
import com.tw.aquaculture.manager.service.check.CheckWaterQualityEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * CheckWaterQualityEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("CheckWaterQualityEntityServiceImpl")
public class CheckWaterQualityEntityServiceImpl implements CheckWaterQualityEntityService {
  @Autowired
  private CheckWaterQualityFeign checkWaterQualityFeign;

  @Transactional
  @Override
  public CheckWaterQualityEntity create(CheckWaterQualityEntity checkWaterQualityEntity) {
    ResponseModel responseModel = checkWaterQualityFeign.create(checkWaterQualityEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckWaterQualityEntity>() {
    });
  }

  @Transactional
  @Override
  public CheckWaterQualityEntity createForm(CheckWaterQualityEntity checkWaterQualityEntity) {
    return null;
  }

  @Transactional
  @Override
  public CheckWaterQualityEntity update(CheckWaterQualityEntity checkWaterQualityEntity) {
    ResponseModel responseModel = checkWaterQualityFeign.update(checkWaterQualityEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckWaterQualityEntity>() {
    });
  }

  @Transactional
  @Override
  public CheckWaterQualityEntity updateForm(CheckWaterQualityEntity checkWaterQualityEntity) {
    return null;
  }

  @Override
  public CheckWaterQualityEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = checkWaterQualityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckWaterQualityEntity>() {
    });
  }

  @Override
  public CheckWaterQualityEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = checkWaterQualityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckWaterQualityEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    if (StringUtils.isBlank(id)) {
      return;
    }
    checkWaterQualityFeign.deleteById(id);
  }

  @Override
  public CheckWaterQualityEntity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = checkWaterQualityFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckWaterQualityEntity>() {
    });
  }

  @Override
  public CheckWaterQualityEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = checkWaterQualityFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckWaterQualityEntity>() {
    });
  }

  @Override
  public CheckWaterQualityEntity findLastCheckWaterByPondId(String pondId) {
    if (StringUtils.isBlank(pondId)) {
      return null;
    }
    ResponseModel responseModel = checkWaterQualityFeign.findLastCheckWaterByPondId(pondId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CheckWaterQualityEntity>() {
    });
  }
} 
