package com.tw.aquaculture.manager.service.common;

import com.bizunited.platform.core.annotations.NebulaService;
import com.bizunited.platform.core.annotations.NebulaServiceMethod;
import com.bizunited.platform.core.annotations.ServiceMethodParam;
import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.kuiper.entity.InstanceEntity;
import com.bizunited.platform.rbac.server.vo.PositionVo;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.MatterEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.StoreEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.common.entity.check.CheckPondEntity;
import com.tw.aquaculture.common.entity.check.CheckWaterQualityEntity;
import com.tw.aquaculture.common.entity.process.OutApplyCustomStateEntity;
import com.tw.aquaculture.common.entity.process.OutFishRegistEntity;
import com.tw.aquaculture.common.entity.process.ProofYieldEntity;
import com.tw.aquaculture.common.vo.common.CheckPondCountBaseVo;
import com.tw.aquaculture.common.vo.common.DictItemVo;
import com.tw.aquaculture.common.vo.common.MatterBudgetVo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.security.Principal;
import java.util.List;
import java.util.Map;

/**
 * 字典接口
 * @author 陈荣
 * @date 2019/11/20 13:27
 */
@NebulaService
public interface CommonDataViewService {

  /**
   * 根据字典编码查询字典
   * @param dictCodes
   * @return
   */
  @NebulaServiceMethod(name="DictItemService.findByCodes" , desc="根据字典编码查询字典" , returnPropertiesFilter="", scope= NebulaServiceMethod.ScopeType.READ)
  public List<DictItemVo> findByCodes(@ServiceMethodParam(name = "dictCodes") String dictCodes);

  /**
   * 查询所有鱼种
   * @return
   */
  List<FishTypeEntity> findFishTypes();

  /**
   * 根据条件分页查询物料
   * @param params
   * @param pageable
   * @return
   */
  Page<MatterEntity> findMattersByMatterConditions(Map<String, Object> params, Pageable pageable);

  /**
   * 根据用户id查询基地
   * @param userId
   * @return
   */
  List<TwBaseEntity> getBaseByUserId(String userId);

  /**
   * 根据条件分页查询基地
   * @param pageable
   * @param params
   * @return
   */
  Page<TwBaseEntity> findBaseByConditions(Pageable pageable, Map<String, Object> params);

  /**
   * 初始化表单实例
   * @param templateCode 模板编码
   * @param taskCode 任务编码
   * @param principal 当前登录信息
   * @return
   */
  InstanceEntity instances(String templateCode, String taskCode, Principal principal);

  /**
   * 根据用户id查询基地
   * @param userId
   * @return
   */
  List<TwBaseEntity> findBaseByUserId(String userId);

  /**
   * 根据基地组织id查询下级所有用户
   * @param baseOrgId
   * @return
   */
  List<UserEntity> findUserByBaseOrgId(String baseOrgId);

  /**
   * 本周期内最后一次打样测产数据
   * @param pondId 塘口id
   * @return
   */
  ProofYieldEntity findLastProofYield(String pondId);

  /**
   * 擦汗寻当前村塘量
   * @param pondId
   * @return
   */
  double findExitCount(String pondId);

  /**
   * 根据岀鱼申请id查询客户
   * @param outFishApplyId
   * @param state
   * @return
   */
  List<OutApplyCustomStateEntity> findCustomByOutFishApplyId(String outFishApplyId, int state);

  /**
   * 本周期内某鱼种上次打样测产数据
   * @param pondId
   * @param fishTypeId
   * @return
   */
  ProofYieldEntity findLastProofYieldByFishType(String pondId, String fishTypeId);

  /**
   * 根据用户id查询岗位
   * @param userId
   * @return
   */
  List<PositionVo> findPositionByUserId(String userId);

  /**
   * 根据基地id查询塘口
   * @param baseId
   * @param enableState
   * @return
   */
  List<PondEntity> findPondsByBaseId(String baseId, int enableState);

  /**
   * 根据塘口和用户查询巡塘次数
   * @param pondId
   * @param username
   * @return
   */
  Map<String, Object> findCheckPondCountByPondAndUser(String pondId, String username);

  /**
   * 根据岗位id查询基地
   * @param positionId
   * @return
   */
  List<TwBaseEntity> findBaseByPositionId(String positionId);

  /**
   * 根据基地id查询仓库
   * @param baseId
   * @return
   */
  List<StoreEntity> findStoresByBaseId(String baseId);

  /**
   * 根据塘口id查询上次水质检测
   * @param pondId
   * @return
   */
  CheckWaterQualityEntity findLastCheckWaterByPondId(String pondId);

  /**
   * 查询物料的本月消耗和库存量
   * @return
   */
  List<MatterBudgetVo> findMatterBudgetByPondId();

  /**
   * 查询当前登陆人所有相关基地的所有塘口的当月、当天巡塘次数和所有鱼种规格、存塘重量
   * @return
   */
  List<CheckPondCountBaseVo> computeCheckPondCount(String userId);

  /**
   * 查询上次巡塘检测
   * @param pondId
   * @return
   */
  CheckPondEntity findLastCheckPondByPondId(String pondId);

  /**
   * 根据岀鱼申请查询岀鱼登记
   * @param outFishApplyId
   * @return
   */
  List<OutFishRegistEntity> findOutFishRegistByOutFishApply(String outFishApplyId);

  /**
   * 根据基地查询组织基地
   * @param baseId
   * @return
   */
  TwBaseEntity findBaseOrgByBaseId(String baseId);

  /**
   * 根据塘口和鱼种查询本周期内存塘量
   * @param pondId
   * @param fishTypeId
   * @return
   */
  @NebulaServiceMethod(name="CommonDataViewService.findExitCountByPondAndFishType" , desc="根据塘口和鱼种查询本周期内存塘量" , returnPropertiesFilter="", scope= NebulaServiceMethod.ScopeType.READ)
  double findExitCountByPondAndFishType(@ServiceMethodParam(name = "pondId")String pondId, @ServiceMethodParam(name = "fishTypeId") String fishTypeId);

  /**
   * 查询当前登陆人所有相关基地的所有塘口的当月、当天巡塘次数和所有鱼种规格、存塘重量
   * @param positionId
   * @return
   */
  List<CheckPondCountBaseVo> computeCheckPondCountByPositionId(String positionId);

  /**
   * 根据用户id查询当前登录人当前岗位
   * @param userId
   * @return
   */
  PositionVo findMainPositionByUserId(String userId);
}
