package com.tw.aquaculture.manager.service.common;

import com.bizunited.platform.core.entity.OrganizationEntity;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * 组织
 *
 * @author 陈荣
 * @date 2019/11/27 11:40
 */
@Service
public interface OwnOrganizationService {

  /**
   * 根据id查详情
   *
   * @param baseOrgId
   * @return
   */
  OrganizationEntity findDetailsById(String baseOrgId);

  /**
   * 根据id查
   *
   * @param id
   * @return
   */
  OrganizationEntity findById(String id);


  /**
   * 创建组织
   *
   * @param organizationEntity
   * @return
   */
  OrganizationEntity create(OrganizationEntity organizationEntity);

  /**
   * 查询所有顶级组织结构
   * @return
   */
  Set<OrganizationEntity> findTopOrgs();


  /**
   * 查询某组织机构下的所有子级组织机构
   * @param parentId
   * @return
   */
  Set<OrganizationEntity> findByParent(String parentId);

}
