package com.tw.aquaculture.manager.service.common;

import com.bizunited.platform.core.entity.PositionEntity;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * 岗位
 *
 * @author 陈荣
 * @date 2019/11/27 11:40
 */
@Service
public interface OwnPositionService {

  /**
   * 根据id查详情
   *
   * @param id
   * @return
   */
  PositionEntity findDtailById(String id);

  /**
   * 根据id查询详情
   *
   * @param positionId
   * @return
   */
  PositionEntity findDetailsById(String positionId);

  /**
   * 查询所有岗位详情，包括岗位关联的组织机构信息
   * @return
   */
  Set<PositionEntity> findAllDetails();

  /**
   * 查询所有的岗位id和它的父岗位id
   */
  List<Pair<String, String>> findAllPositionIdAndParentId();
}
