package com.tw.aquaculture.manager.service.common;

import com.bizunited.platform.core.entity.UserEntity;
import org.springframework.stereotype.Service;

/**
 * 用户
 *
 * @author 陈荣
 * @date 2019/11/27 11:39
 */
@Service
public interface OwnUserService {

  /**
   * 根据id查询详情
   *
   * @param userId
   * @return
   */
  UserEntity findDetailsById(String userId);
}
