package com.tw.aquaculture.manager.service.common;

import com.tw.aquaculture.common.vo.remote.saleapply.SaleRequestVo;
import com.tw.aquaculture.common.vo.remote.saleapply.SaleResponseDataVo;

import java.text.ParseException;

/**
 * @author 陈荣
 * @date 2019/12/13 10:51
 */
public interface RemoteService {

  /**
   * 岀鱼申请回调
   * @param saleRequestVo
   * @return
   */
  SaleResponseDataVo saleApplyCallback(SaleRequestVo saleRequestVo) throws ParseException;

}
