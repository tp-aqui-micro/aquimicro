package com.tw.aquaculture.manager.service.common;

import com.tw.aquaculture.common.vo.reportslist.*;

import java.util.List;

/**
 * @author 陈荣
 * @date 2020/1/7 10:07
 */
public interface ReportService {

  /**
   * 饲料投喂报表
   * @param reportParamVo
   * @param acount
   * @return
   */
  List<PushFeedReportVo> pushFeedReport(ReportParamVo reportParamVo, String acount);

  /**
   * 动保使用报表
   * @param reportParamVo
   * @param acount
   * @return
   */
  List<SafeReportVo> safeReport(ReportParamVo reportParamVo, String acount);

  /**
   * 损鱼报表
   * @param reportParamVo
   * @param acount
   * @return
   */
  List<FishDamageReportVo> fishDamageReport(ReportParamVo reportParamVo, String acount);

  /**
   * 存塘量报表
   * @param reportParamVo
   * @param acount
   * @return
   */
  List<ExistAmountReportVo> existAmountReport(ReportParamVo reportParamVo, String acount);

  /**
   * 库存报表
   * @param stockReportParamVo
   * @param acount
   * @return
   */
  List<ExistAmountReportVo> stockReport(StockReportParamVo stockReportParamVo, String account);

  /**
   * 转分塘报表
   * @param dividePondParamVo
   * @param account
   * @return
   */
  List<DividePondResultVo> dividePondReport(DividePondParamVo dividePondParamVo, String account);

  /**
   * 年度计划报表
   * @param yearlyPlanReportParamVo
   * @param account
   * @return
   */
  List<YearlyPlanReportResultVo> yearlyPlanReport(YearlyPlanReportParamVo yearlyPlanReportParamVo, String account);

  /**
   * 出鱼计划报表
   * @param outFishPlanReportParamVo
   * @param account
   * @return
   */
  List<OutFishPlanReportResultVo> outFishPlanReport(OutFishPlanReportParamVo outFishPlanReportParamVo, String account);

  /**
   * 投苗计划报表
   * @param pushFryPlanReportParamVo
   * @param account
   * @return
   */
  List<PushFryPlanReportResultVo> pushFryPlanReport(PushFryPlanReportParamVo pushFryPlanReportParamVo, String account);
}
