package com.tw.aquaculture.manager.service.common.internal;

import com.bizunited.platform.core.entity.DictItemEntity;
import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.core.entity.PositionEntity;
import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.core.service.DictItemService;
import com.bizunited.platform.kuiper.entity.InstanceEntity;
import com.bizunited.platform.kuiper.service.InstanceService;
import com.bizunited.platform.rbac.server.service.PositionService;
import com.bizunited.platform.rbac.server.service.RoleService;
import com.bizunited.platform.rbac.server.service.UserService;
import com.bizunited.platform.rbac.server.vo.PositionVo;
import com.bizunited.platform.rbac.server.vo.RoleVo;
import com.bizunited.platform.rbac.server.vo.UserVo;
import com.google.common.collect.Sets;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.MatterEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.StoreEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.common.entity.check.CheckPondEntity;
import com.tw.aquaculture.common.entity.check.CheckWaterQualityEntity;
import com.tw.aquaculture.common.entity.process.OutApplyCustomStateEntity;
import com.tw.aquaculture.common.entity.process.OutFishRegistEntity;
import com.tw.aquaculture.common.entity.process.ProofYieldEntity;
import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import com.tw.aquaculture.common.entity.store.StockEntity;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.enums.OrgTypeEnmu;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.common.utils.MathUtil;
import com.tw.aquaculture.common.utils.UnitUtil;
import com.tw.aquaculture.common.vo.common.CheckPondCountBaseVo;
import com.tw.aquaculture.common.vo.common.CheckPondCountPondVo;
import com.tw.aquaculture.common.vo.common.DictItemVo;
import com.tw.aquaculture.common.vo.common.FishTypeSpecsVo;
import com.tw.aquaculture.common.vo.common.MatterBudgetItemVo;
import com.tw.aquaculture.common.vo.common.MatterBudgetVo;
import com.tw.aquaculture.manager.service.check.CheckPondEntityService;
import com.tw.aquaculture.manager.service.check.CheckWaterQualityEntityService;
import com.tw.aquaculture.manager.service.common.CommonDataViewService;
import com.tw.aquaculture.manager.service.common.OwnOrganizationService;
import com.tw.aquaculture.manager.service.common.OwnPositionService;
import com.tw.aquaculture.manager.service.common.OwnUserService;
import com.tw.aquaculture.manager.service.process.OutApplyCustomStateEntityService;
import com.tw.aquaculture.manager.service.process.OutFishRegistEntityService;
import com.tw.aquaculture.manager.service.process.ProofYieldEntityService;
import com.tw.aquaculture.manager.service.process.PushFeedEntityService;
import com.tw.aquaculture.manager.service.process.WorkOrderEntityService;
import com.tw.aquaculture.manager.service.remote.FishTypeEntityService;
import com.tw.aquaculture.manager.service.remote.MatterEntityService;
import com.tw.aquaculture.manager.service.remote.PondEntityService;
import com.tw.aquaculture.manager.service.remote.StoreEntityService;
import com.tw.aquaculture.manager.service.remote.TwBaseEntityService;
import com.tw.aquaculture.manager.service.store.StockEntityService;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.gradle.internal.impldep.com.google.common.collect.Maps;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author 陈荣
 * @date 2019/11/20 13:27
 */
@Service
public class CommonDataViewServiceImpl implements CommonDataViewService {

  private final static String UP_SELECT_ROLE = "UP_SELECT_ROLE";

  @Autowired
  private DictItemService dictItemService;
  @Autowired
  private FishTypeEntityService fishTypeEntityService;
  @Autowired
  private MatterEntityService matterEntityService;
  @Autowired
  private TwBaseEntityService twBaseEntityService;
  @Autowired
  private InstanceService instanceService;
  @Autowired
  private OwnUserService ownUserService;
  @Autowired
  private OwnPositionService ownPositionService;
  @Autowired
  private OwnOrganizationService ownOrganizationService;
  @Autowired
  private ProofYieldEntityService proofYieldEntityService;
  @Autowired
  private OutApplyCustomStateEntityService outApplyCustomStateEntityService;
  @Autowired
  private PositionService positionService;
  @Autowired
  private UserService userService;
  @Autowired
  private PondEntityService pondEntityService;
  @Autowired
  private CheckPondEntityService checkPondEntityService;
  @Autowired
  private StoreEntityService storeEntityService;
  @Autowired
  private CheckWaterQualityEntityService checkWaterQualityEntityService;
  @Autowired
  private StockEntityService stockEntityService;
  @Autowired
  private WorkOrderEntityService workOrderEntityService;
  @Autowired
  private OutFishRegistEntityService outFishRegistEntityService;
  @Autowired
  private PushFeedEntityService pushFeedEntityService;
  @Autowired
  private RoleService roleService;

  @Override
  public List<DictItemVo> findByCodes(String dictCodes) {
    if (StringUtils.isBlank(dictCodes)) {
      return null;
    }
    String[] ds = dictCodes.split(",");
    List<DictItemVo> dictItemVos = Lists.newArrayList();
    for (String d : ds) {
      if (StringUtils.isBlank(d)) {
        continue;
      }
      DictItemVo dictItemVo = new DictItemVo();
      dictItemVo.setDictCode(d);
      buildDictItemVo(dictItemVo, dictItemService.findItemsByCode(d));
      dictItemVos.add(dictItemVo);
    }
    return dictItemVos;
  }

  @Override
  public List<FishTypeEntity> findFishTypes() {
    return fishTypeEntityService.findAll();
  }

  @Override
  public Page<MatterEntity> findMattersByMatterConditions(Map<String, Object> params, Pageable pageable) {

    return matterEntityService.findByMatterConditions(params, pageable);
  }

  @Override
  public List<TwBaseEntity> getBaseByUserId(String userId) {
    if (StringUtils.isBlank(userId)) {
      return null;
    }
    /**
     * 暂时查出全部基地
     */
    List<TwBaseEntity> bases = twBaseEntityService.findDetailAll();
    return bases;
  }

  @Override
  public Page<TwBaseEntity> findBaseByConditions(Pageable pageable, Map<String, Object> params) {
    return twBaseEntityService.findByConditions(pageable, params);
  }

  @Override
  public InstanceEntity instances(String templateCode, String taskCode, Principal principal) {

    return this.instanceService.create(templateCode, taskCode, principal);
  }

  @Override
  public List<TwBaseEntity> findBaseByUserId(String userId) {
    if (StringUtils.isBlank(userId)) {
      return null;
    }
    List<TwBaseEntity> twBases = Lists.newArrayList();

    UserEntity user = ownUserService.findDetailsById(userId);
    if (user == null) {
      return null;
    }
    if (!CollectionUtils.isEmpty(user.getPositions())) {
      PositionEntity p = user.getPositions().iterator().next();
      if (p.getOrganization() != null && p.getOrganization().getId() != null) {
        String id = p.getOrganization().getId();
        OrganizationEntity o = ownOrganizationService.findDetailsById(id);
        p.setOrganization(o);
      } else {
        p.setOrganization(null);
      }
    }

    Set<PositionEntity> positionEntitySet = user.getPositions();
    if (CollectionUtils.isEmpty(positionEntitySet)) {
      return null;
    }
    //一人只有一岗
    OrganizationEntity organization = positionEntitySet.iterator().next().getOrganization();
    if (organization == null) {
      return null;
    }
    OrganizationEntity parentBaseOrg = findParentBaseOrg(organization);
    if (parentBaseOrg != null) {
      TwBaseEntity baseEntity = twBaseEntityService.findDetailsByBaseOrgId(parentBaseOrg.getId());
      if (baseEntity == null) {
        return null;
      }
      twBases.add(baseEntity);
    }
    //暂时不查询下级组织是基地的
    findChildrenBaseOrg(organization.getChild());
    return twBases;
  }

  @Override
  public List<UserEntity> findUserByBaseOrgId(String baseOrgId) {
    List<UserEntity> userEntities = Lists.newArrayList();
    if (StringUtils.isBlank(baseOrgId)) {
      return userEntities;
    }
    OrganizationEntity organization = ownOrganizationService.findDetailsById(baseOrgId);
    if (organization == null) {
      return userEntities;
    }
    if (CollectionUtils.isEmpty(organization.getPositions())) {
      return userEntities;
    }
    //查询当前组织下的用户
    findUser(organization, userEntities);
    if (!CollectionUtils.isEmpty(organization.getChild())) {
      //查询下级组织的用户
      findChildUser(organization.getChild(), userEntities);
    }
    return userEntities;
  }

  @Override
  public ProofYieldEntity findLastProofYield(String pondId) {
    if (StringUtils.isBlank(pondId)) {
      return null;
    }
    return proofYieldEntityService.findLastProofYield(pondId);
  }

  @Override
  public double findExitCount(String pondId) {
    if (StringUtils.isBlank(pondId)) {
      return 0;
    }
    return proofYieldEntityService.computeLastExitCount(pondId);
  }

  @Override
  public List<OutApplyCustomStateEntity> findCustomByOutFishApplyId(String outFishApplyId, int state) {
    if (StringUtils.isBlank(outFishApplyId)) {
      return null;
    }
    return this.outApplyCustomStateEntityService.findByOutFishApplyIdAndState(outFishApplyId, state);
  }

  @Override
  public ProofYieldEntity findLastProofYieldByFishType(String pondId, String fishTypeId) {
    if (StringUtils.isBlank(pondId) || StringUtils.isBlank(fishTypeId)) {
      return null;
    }
    return this.proofYieldEntityService.findLastProofYieldByFishType(pondId, fishTypeId);
  }

  @Override
  public List<PositionVo> findPositionByUserId(String userId) {
    if (StringUtils.isBlank(userId)) {
      return null;
    }
    /*PositionVo positionVo = positionService.findMainPositionByUserId(userId);
    List<PositionVo> positionVos = Lists.newArrayList();
    positionVos.add(positionVo);
    return positionVos;*/
    return positionService.findByUserId(userId);
  }

  @Override
  public List<PondEntity> findPondsByBaseId(String baseId, int enableState) {
    if (StringUtils.isBlank(baseId)) {
      return null;
    }
    return this.pondEntityService.findPondsByBaseId(baseId, enableState);
  }

  @Override
  public Map<String, Object> findCheckPondCountByPondAndUser(String pondId, String username) {
    if (StringUtils.isBlank(pondId) || StringUtils.isBlank(username)) {
      return null;
    }
    return checkPondEntityService.findCountByPondAndUser(pondId, username);
  }

  @Override
  @Transactional
  public List<TwBaseEntity> findBaseByPositionId(String positionId) {
    List<TwBaseEntity> twBaseEntities = Lists.newArrayList();
    if (StringUtils.isBlank(positionId)) {
      return twBaseEntities;
    }
    List<OrganizationEntity> organizationEntities = findBaseOrg(positionId);
    if (CollectionUtils.isEmpty(organizationEntities)) {
      return twBaseEntities;
    }

    for (OrganizationEntity organizationEntity : organizationEntities) {
      TwBaseEntity twBaseEntity = twBaseEntityService.findDetailsByBaseOrgId(organizationEntity.getId());
      if (twBaseEntity == null) {
        continue;
      }
      OrganizationEntity org = new OrganizationEntity();
      org.setId(twBaseEntity.getBaseOrg().getId());
      org.setCode(twBaseEntity.getBaseOrg().getCode());
      org.setOrgName(twBaseEntity.getBaseOrg().getOrgName());
      org.setTstatus(twBaseEntity.getBaseOrg().getTstatus());
      org.setType(twBaseEntity.getBaseOrg().getType());
      org.setDescription(twBaseEntity.getBaseOrg().getDescription());
      org.setCreateTime(twBaseEntity.getBaseOrg().getCreateTime());
      twBaseEntity.setBaseOrg(org);
      twBaseEntities.add(twBaseEntity);
    }
    return twBaseEntities;
  }

  @Override
  public List<StoreEntity> findStoresByBaseId(String baseId) {
    if (StringUtils.isBlank(baseId)) {
      return null;
    }
    return storeEntityService.findStoresByBaseId(baseId);
  }

  @Override
  public CheckWaterQualityEntity findLastCheckWaterByPondId(String pondId) {
    if (StringUtils.isBlank(pondId)) {
      return null;
    }
    return checkWaterQualityEntityService.findLastCheckWaterByPondId(pondId);
  }

  @Override
  public List<MatterBudgetVo> findMatterBudgetByPondId() {
    List<MatterBudgetVo> matterBudgetVos = Lists.newArrayList();
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    Validate.notNull(authentication, "未获取到当前系统的登录人");
    String account = authentication.getName();
    Validate.notBlank(account, "not found op user!");
    UserVo userVo = userService.findByAccount(account);
    Validate.notNull(userVo, "没有获取到当前用户信息");
    PositionVo positionVo = positionService.findMainPositionByUserId(userVo.getId());
    Validate.notNull(positionVo, "没有获取到当前登录岗位信息");
    //List<TwBaseEntity> twBaseEntities = twBaseEntityService.findDetailAll();
    List<TwBaseEntity> twBaseEntities = this.findBaseByPositionId(positionVo.getId());
    if (CollectionUtils.isEmpty(twBaseEntities)) {
      return null;
    }
    for (TwBaseEntity twBaseEntity : twBaseEntities) {
      MatterBudgetVo matterBudgetVo = computeConsumeAndStock(twBaseEntity);
      if (matterBudgetVo != null) {
        matterBudgetVos.add(matterBudgetVo);
      }
    }
    return matterBudgetVos;
  }

  @Override
  public List<CheckPondCountBaseVo> computeCheckPondCount(String userId) {
    List<TwBaseEntity> bases = this.findBaseByUserId(userId);
    if (CollectionUtils.isEmpty(bases)) {
      return null;
    }
    List<CheckPondCountBaseVo> checkPondCountBaseVos = buildCheckPondCountData(bases);

    return checkPondCountBaseVos;
  }

  /**
   * 根据基地列表计算存塘信息
   *
   * @param bases
   * @return
   */
  private List<CheckPondCountBaseVo> buildCheckPondCountData(List<TwBaseEntity> bases) {
    List<CheckPondCountBaseVo> checkPondCountBaseVos = Lists.newArrayList();
    for (TwBaseEntity base : bases) {
      CheckPondCountBaseVo checkPondCountBaseVo = new CheckPondCountBaseVo();
      checkPondCountBaseVo.setBaseCode(base.getCode());
      checkPondCountBaseVo.setBaseId(base.getId());
      checkPondCountBaseVo.setSimpleBaseName(base.getAbbreviation());
      checkPondCountBaseVo.setBaseOrgId(base.getBaseOrg().getId());
      checkPondCountBaseVo.setBaseOrgCode(base.getBaseOrg().getCode());
      checkPondCountBaseVo.setBaseName(base.getBaseOrg().getOrgName());
      List<CheckPondCountPondVo> checkPondCountPondVos = computeCheckPondCounts(base.getId());
      if (!CollectionUtils.isEmpty(checkPondCountPondVos)) {
        checkPondCountBaseVo.setCheckPondCountPondVoList(checkPondCountPondVos);
      }
      checkPondCountBaseVos.add(checkPondCountBaseVo);
    }
    return checkPondCountBaseVos;
  }

  @Override
  public CheckPondEntity findLastCheckPondByPondId(String pondId) {
    if (StringUtils.isBlank(pondId)) {
      return null;
    }
    return checkPondEntityService.findLastCheckPondByPondId(pondId);
  }

  @Override
  public List<OutFishRegistEntity> findOutFishRegistByOutFishApply(String outFishApplyId) {
    if (StringUtils.isBlank(outFishApplyId)) {
      return null;
    }
    return outFishRegistEntityService.findOutFishRegistByOutFishApply(outFishApplyId);
  }

  @Override
  public TwBaseEntity findBaseOrgByBaseId(String baseId) {
    if (StringUtils.isBlank(baseId)) {
      return null;
    }
    return twBaseEntityService.findDetailsById(baseId);
  }

  @Override
  public double findExitCountByPondAndFishType(String pondId, String fishTypeId) {

    if (StringUtils.isBlank(pondId) || StringUtils.isBlank(fishTypeId)) {
      return 0;
    }
    return proofYieldEntityService.findExitCountByPondAndFishType(pondId, fishTypeId);
  }

  @Override
  public List<CheckPondCountBaseVo> computeCheckPondCountByPositionId(String positionId) {
    List<TwBaseEntity> bases = this.findBaseByPositionId(positionId);
    if (CollectionUtils.isEmpty(bases)) {
      return null;
    }
    return buildCheckPondCountData(bases);
  }

  @Override
  public PositionVo findMainPositionByUserId(String userId) {
    if (StringUtils.isBlank(userId)) {
      return null;
    }
    return positionService.findMainPositionByUserId(userId);
  }

  private List<CheckPondCountPondVo> computeCheckPondCounts(String baseId) {
    List<PondEntity> pondEntities = pondEntityService.findPondsByBaseId(baseId, 1);
    if (CollectionUtils.isEmpty(pondEntities)) {
      return null;
    }
    List<CheckPondCountPondVo> checkPondCountPondVos = Lists.newArrayList();
    for (PondEntity pondEntity : pondEntities) {
      CheckPondCountPondVo checkPondCountPondVo = new CheckPondCountPondVo();
      checkPondCountPondVo.setPondId(pondEntity.getId());
      checkPondCountPondVo.setPondCode(pondEntity.getCode());
      checkPondCountPondVo.setPondName(pondEntity.getName());
      checkPondCountPondVo.setPondState(pondEntity.getState());
      checkPondCountPondVo.setWorkerName(pondEntity.getWorker() == null ? null : pondEntity.getWorker().getUserName());
      checkPondCountPondVo.setCheckPondCountForMonth(checkPondEntityService.checkPondCount(pondEntity.getId(), "month"));
      checkPondCountPondVo.setCheckPondCountForDay(checkPondEntityService.checkPondCount(pondEntity.getId(), "day"));
      List<FishTypeSpecsVo> fishTypeSpecsVos = computeFishSpecs(pondEntity.getId());
      if (!CollectionUtils.isEmpty(fishTypeSpecsVos)) {
        checkPondCountPondVo.setFishTypeSpecsVoList(fishTypeSpecsVos);
      }
      checkPondCountPondVos.add(checkPondCountPondVo);
    }
    return checkPondCountPondVos;
  }

  /**
   * @param pondId
   * @return
   */
  private List<FishTypeSpecsVo> computeFishSpecs(String pondId) {
    List<WorkOrderEntity> workOrderEntities = workOrderEntityService.findByStateAndPond(pondId, EnableStateEnum.DISABLE.getState());
    if (CollectionUtils.isEmpty(workOrderEntities)) {
      return null;
    }
    List<FishTypeSpecsVo> fishTypeSpecsVos = Lists.newArrayList();
    List<ProofYieldEntity> yields = proofYieldEntityService.findDetailsByPondAndWorkOrder(pondId, workOrderEntities.get(workOrderEntities.size() - 1).getId());
    if (CollectionUtils.isEmpty(yields)) {
      return null;
    }

    for (ProofYieldEntity proofYieldEntity : yields) {
      if (proofYieldEntity.getFishType() == null) {
        continue;
      }
      FishTypeSpecsVo fishTypeSpecsVo = new FishTypeSpecsVo();
      fishTypeSpecsVo.setFishTypeId(proofYieldEntity.getFishType().getId());
      fishTypeSpecsVo.setFishTypeCode(proofYieldEntity.getFishType().getCode());
      fishTypeSpecsVo.setFishTypeName(proofYieldEntity.getFishType().getName());
      fishTypeSpecsVo.setSpecs(proofYieldEntity.getSpecs());
      fishTypeSpecsVo.setExsitWeight(proofYieldEntity.getExistWeight());
      fishTypeSpecsVos.add(fishTypeSpecsVo);
    }
    return removeDuplicate(fishTypeSpecsVos);
  }

  /**
   * 合并相同鱼种的记录
   *
   * @param fishTypeSpecsVos
   * @return
   */
  private List<FishTypeSpecsVo> removeDuplicate(List<FishTypeSpecsVo> fishTypeSpecsVos) {
    if (CollectionUtils.isEmpty(fishTypeSpecsVos)) {
      return null;
    }
    List<FishTypeSpecsVo> fs = Lists.newArrayList();
    for (FishTypeSpecsVo f1 : fishTypeSpecsVos) {
      boolean flag = true;
      for (FishTypeSpecsVo f2 : fs) {
        if (f2.getFishTypeId().equals(f1.getFishTypeId())) {
          f2.setExsitWeight(f2.getExsitWeight() + f1.getExsitWeight());
          flag = false;
          break;
        }
      }
      if (flag) {
        FishTypeSpecsVo fishTypeSpecsVo = new FishTypeSpecsVo();
        BeanUtils.copyProperties(f1, fishTypeSpecsVo);
        fs.add(fishTypeSpecsVo);
      }
    }
    return fs;
  }

  /**
   * 计算消耗量和库存
   *
   * @param twBaseEntity
   * @return
   */
  private MatterBudgetVo computeConsumeAndStock(TwBaseEntity twBaseEntity) {
    MatterBudgetVo matterBudgetVo = new MatterBudgetVo();
    matterBudgetVo.setBaseCode(twBaseEntity.getBaseOrg().getCode());
    matterBudgetVo.setBaseName(twBaseEntity.getBaseOrg().getOrgName());
    List<StockEntity> matterEntities = stockEntityService.findByBaseId(twBaseEntity.getId());
    if (CollectionUtils.isEmpty(matterEntities)) {
      return null;
    }
    List<MatterBudgetItemVo> matterBudgetItemVos = Lists.newArrayList();
    for (StockEntity stockEntity : matterEntities) {
      if (!"11".equals(stockEntity.getMatter().getMatterTypeCode())) {
        continue;
      }
      //Double consume = stockOutEntityService.sumConsumeByTimes(twBaseEntity.getId(), stockEntity.getMatter().getId(), DateUtils.getMonthStart(), new Date());
      Double consume = pushFeedEntityService.sumConsumeByTimes(twBaseEntity.getId(), stockEntity.getMatter().getId(), DateUtils.getMonthStart(), new Date());
      Double stock = stockEntity == null ? 0 : stockEntity.getStorage();
      MatterBudgetItemVo matterBudgetItemVo = new MatterBudgetItemVo();
      matterBudgetItemVo.setMatterCode(stockEntity.getMatter().getCode());
      matterBudgetItemVo.setMatterName(stockEntity.getMatter().getName());
      if (consume == null || consume.equals(0.0)) {
        continue;
      }
      consume = MathUtil.round(consume / UnitUtil.UNIT_JIN_LV, 6);
      matterBudgetItemVo.setConsume(consume);
      matterBudgetItemVo.setStock(stock);
      matterBudgetItemVos.add(matterBudgetItemVo);
    }
    matterBudgetVo.setMatterBudgetItemVoList(CollectionUtils.isEmpty(matterBudgetItemVos) ? null : matterBudgetItemVos);
    return matterBudgetVo;
  }


  /**
   * 查找类型为基地的组织
   *
   * @param positionId
   * @return
   */
  private List<OrganizationEntity> findBaseOrg(String positionId) {
    List<OrganizationEntity> organizationEntities = Lists.newArrayList();
    PositionEntity positionEntity = ownPositionService.findDetailsById(positionId);
    if (positionEntity == null || positionEntity.getOrganization() == null || positionEntity.getOrganization().getId() == null) {
      return organizationEntities;
    }
    OrganizationEntity org = positionEntity.getOrganization();
    if (org == null || org.getId() == null || org.getType() == null || org.getTstatus() != 1) {
      return organizationEntities;
    }
    boolean up = false;
    Set<RoleVo> roles = Sets.newHashSet();
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if(authentication!=null){
      String account = authentication.getName();
      UserVo userVo = userService.findByAccount(account);
      if(userVo!=null){
        Set<RoleVo> roleVos  = roleService.findByUser(userVo.getId());
        if(!CollectionUtils.isEmpty(roleVos)){
          roles.addAll(roleVos);
        }
      }
    }
    PositionVo positionVo = positionService.findDetailsById(positionId);
    if(positionVo!=null && !CollectionUtils.isEmpty(positionVo.getRoles())){
      roles.addAll(positionVo.getRoles());
    }
    for(RoleVo r : roles){
      if(UP_SELECT_ROLE.equals(r.getRoleCode())){
        up = true;
        break;
      }
    }
    if (OrgTypeEnmu.BASE.getCode().equals(org.getType())) {
      //当前岗位所属组织为基地
      organizationEntities.add(org);
    } else if (compareOrgType(org.getType(), OrgTypeEnmu.BASE.getCode())) {
      //上级组织为基地
      organizationEntities.addAll(findParentOrg(org.getParent()));
    } else {
      //下级组织为基地
      List<OrganizationEntity> childOrg = Lists.newArrayList();
      findChildOrg(org.getChild(), childOrg);
      organizationEntities.addAll(childOrg);
    }
    if (up) {
      //当前岗位所属组织为分公司部门
      List<OrganizationEntity> baseOrgs = findBaseByCompany(org.getParent());
      organizationEntities.addAll(baseOrgs);
    }
    return organizationEntities;
  }

  /**
   * 查询所有子组织
   *
   * @param parent
   */
  private List<OrganizationEntity> findBaseByCompany(OrganizationEntity parent) {
    List<OrganizationEntity> organizationEntities = Lists.newArrayList();
    Set<OrganizationEntity> children = parent.getChild();
    if (CollectionUtils.isEmpty(children)) {
      return Lists.newArrayList();
    }
    for (OrganizationEntity org : children) {
      if (OrgTypeEnmu.BASE.getCode().equals(org.getType())) {
        organizationEntities.add(org);
      } else if (!compareOrgType(org.getType(), OrgTypeEnmu.BASE.getCode())) {
        //下级组织为基地
        List<OrganizationEntity> childOrg = Lists.newArrayList();
        findChildOrg(org.getChild(), childOrg);
        organizationEntities.addAll(childOrg);
      }

    }
    return organizationEntities;
  }

  /**
   * 比较组织级别，是b是a的上级返回true
   */
  private boolean compareOrgType(String a, String b) {
    int aNum = 0;
    int bNum = 0;
    OrgTypeEnmu[] orgTypeEnums = OrgTypeEnmu.values();
    for (int i = 0; i < orgTypeEnums.length; i++) {
      if (orgTypeEnums[i].getCode().equals(a)) {
        aNum = i;
      }
      if (orgTypeEnums[i].getCode().equals(b)) {
        bNum = i;
      }
    }
    return bNum < aNum;
  }

  /**
   * 查找下级组织为基地的
   *
   * @param children
   * @return
   */
  private void findChildOrg(Set<OrganizationEntity> children, List<OrganizationEntity> organizationEntities) {
    if (CollectionUtils.isEmpty(children)) {
      return;
    }
    for (OrganizationEntity child : children) {
      if (OrgTypeEnmu.BASE.getCode().equals(child.getType())) {
        organizationEntities.add(child);
      }
      if (!CollectionUtils.isEmpty(child.getChild())) {
        findChildOrg(child.getChild(), organizationEntities);
      }
    }
  }

  /**
   * 查询上级基地组织
   *
   * @param parent
   * @return
   */
  private List<OrganizationEntity> findParentOrg(OrganizationEntity parent) {
    List<OrganizationEntity> organizationEntities = Lists.newArrayList();
    if (parent == null || parent.getType() == null || parent.getTstatus() != 1) {
      return organizationEntities;
    }
    if (OrgTypeEnmu.BASE.getCode().equals(parent.getType())) {
      organizationEntities.add(parent);
      return organizationEntities;
    } else {
      return findParentOrg(parent.getParent());
    }
  }

  /**
   * 查询某组织下的用户
   *
   * @param organization
   * @param userEntities
   */
  private void findUser(OrganizationEntity organization, List<UserEntity> userEntities) {
    if (organization == null) {
      return;
    }
    if (CollectionUtils.isEmpty(organization.getPositions())) {
      return;
    }
    Iterator<PositionEntity> iterator = organization.getPositions().iterator();
    while (iterator.hasNext()) {
      PositionEntity position = iterator.next();
      if (!CollectionUtils.isEmpty(position.getUsers())) {
        userEntities.addAll(position.getUsers());
      }
    }
  }

  /**
   * 查找子级组织下的用户
   *
   * @param child
   * @param userEntities
   */
  private void findChildUser(Set<OrganizationEntity> child, List<UserEntity> userEntities) {
    if (CollectionUtils.isEmpty(child)) {
      return;
    }
    Iterator<OrganizationEntity> iterator = child.iterator();
    while (iterator.hasNext()) {
      OrganizationEntity organization = (OrganizationEntity) iterator.next();
      findUser(organization, userEntities);
      findChildUser(organization.getChild(), userEntities);
    }
  }

  /**
   * 查找上级组织是基地的组织
   *
   * @param organization
   * @return
   */
  private OrganizationEntity findParentBaseOrg(OrganizationEntity organization) {
    if (organization == null) {
      return null;
    }
    /* 该用户属于基地 */
    if (OrgTypeEnmu.BASE.getCode().equals(organization.getType())) {
      return organization;
    }
    /* 找上级是基地的组织 */
    OrganizationEntity parent = organization.getParent();
    if (OrgTypeEnmu.BASE.getCode().equals(parent.getType())) {
      return parent;
    }
    return findParentBaseOrg(parent);
  }

  /**
   * 查找属于基地的下级组织
   *
   * @param children
   * @return
   */
  private Set<OrganizationEntity> findChildrenBaseOrg(Set<OrganizationEntity> children) {
    Set<OrganizationEntity> set = Sets.newHashSet();
    if (CollectionUtils.isEmpty(children)) {
      return null;
    }
    for (OrganizationEntity organization : children) {
      /* 该用户属于基地 */
      if (OrgTypeEnmu.BASE.getCode().equals(organization.getType())) {
        set.add(organization);
      }
    }
    return null;
  }

  /**
   * 设置字典列表
   *
   * @param dictItemVo
   * @param items
   */
  private void buildDictItemVo(DictItemVo dictItemVo, List<DictItemEntity> items) {
    if (CollectionUtils.isEmpty(items)) {
      return;
    }
    List<Map<String, Object>> maps = Lists.newArrayList();
    for (DictItemEntity item : items) {
      Map<String, Object> map = Maps.newHashMap();
      map.put("value", item.getDictValue());
      map.put("name", item.getDictKey());
      map.put("state", item.getDictItemStatus());
      map.put("sort", item.getDictSort());
      maps.add(map);
    }
    dictItemVo.setDictItemEntities(maps);
  }
}
