package com.tw.aquaculture.manager.service.common.internal;

import com.alibaba.fastjson.JSONObject;
import com.bizunited.platform.core.entity.ServicableMethodEntity;
import com.bizunited.platform.core.service.invoke.InvokeProxyException;
import com.bizunited.platform.core.service.serviceable.ServicableMethodService;
import com.bizunited.platform.core.service.serviceable.model.InputParamsModel;
import com.bizunited.platform.kuiper.entity.InstanceEntity;
import com.bizunited.platform.kuiper.service.InstanceService;
import com.google.common.collect.Maps;
import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import com.tw.aquaculture.common.utils.FieldUtils;
import com.tw.aquaculture.manager.service.common.FormInstanceHandleService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author 陈荣
 * @date 2019/12/16 16:53
 */
@Service
public class FormInstanceHandleServiceImpl implements FormInstanceHandleService {

  private static final Logger LOGGER = LoggerFactory.getLogger(FormInstanceHandleServiceImpl.class);

  @Autowired
  private InstanceService instanceService;
  @Autowired
  private ServicableMethodService servicableMethodService;

  @Override
  public Object create(Object tEntity, String entityName, String serviceAndMethodName, boolean flag) {
    Object result;
    try {
      if(!flag){
        result = createForm(tEntity, entityName, serviceAndMethodName);
      }else{
        result = createRemoteForm(tEntity, entityName, serviceAndMethodName);
      }
    } catch (Exception e) {
      LOGGER.error(e.getMessage());
      throw new RuntimeException("创建实例失败:"+tEntity.getClass().getName()+"\n"+e.getMessage());
    }
    return result;
  }

  @Override
  public InstanceEntity createRemoteForm(Object tEntity, String entityName, String serviceAndMethodName) {

    String templateCode = (tEntity.getClass().getResource("").toString().substring(
            WorkOrderEntity.class.getResource("").toString().indexOf("com/")) + entityName)
            .replace("/", ".");
    Map<String , String> params = Maps.newHashMap();
    Principal principal = this.getPrincipal();
    Validate.notNull(principal, "没有获取到当前登录人信息");
    params.put("opType", serviceAndMethodName.substring(serviceAndMethodName.indexOf('.')+1));
    params.put("serviceName", serviceAndMethodName);
    return this.instanceService.create(templateCode, UUID.randomUUID().toString(), principal);
  }

  private Object createForm(Object tEntity, String entityName, String serviceAndMethodName) throws InvokeProxyException {
    Object result = null;
    //获取表单实例
    Principal principal = this.getPrincipal();
    Validate.notNull(principal, "没有获取到当前登录人信息");
    String templateCode = (tEntity.getClass().getResource("").toString().substring(
            WorkOrderEntity.class.getResource("").toString().indexOf("com/")) + entityName)
            .replace("/", ".");
    InstanceEntity currentInstance = this.instanceService.create(templateCode, null, principal);
    if (currentInstance != null) {
      FieldUtils.setFieldValueByFieldName(tEntity, "formInstanceId", currentInstance.getId());
      InputParamsModel model = new InputParamsModel();
      model.setInstanceActivityId(currentInstance.getActivities().iterator().next().getId());
      model.setServiceName(serviceAndMethodName);
      model.setFormData(JSONObject.parseObject(JSONObject.toJSON(tEntity).toString()));
      Validate.notNull(model, "未接收到任何数据，请检查!!");

      Validate.notBlank(model.getServiceName(), "服务源名称不能为空，请检查!!");
      if(model.getServiceName()==null){
        return null;
      }
      ServicableMethodEntity entity = servicableMethodService.findDetailsByName(model.getServiceName());
      Validate.notNull(entity, "根据服务名%s，没有查询到服务的相关信息，请检查!!", model.getServiceName());
      // 构建调用器，并进行调用
      Map<String, Object> params = new HashMap<>();
      params.put(InputParamsModel.class.getName(), model);
      if (StringUtils.equals(entity.getUsedScope(), "WRITE")) {
        result = this.servicableMethodService.writeInvoke(params);
      } else {
        result = this.servicableMethodService.readInvoke(params);
      }
    }
    return result;
  }

  /**
   * 获取登陆者账号对象
   *
   * @return
   */
  @Override
  public Principal getPrincipal() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    Validate.notNull(authentication, "未获取到当前系统的登录人");
    return authentication;
  }

}
