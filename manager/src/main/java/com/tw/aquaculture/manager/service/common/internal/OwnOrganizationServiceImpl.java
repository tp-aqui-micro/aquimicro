package com.tw.aquaculture.manager.service.common.internal;

import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.core.repository.OrganizationRepository;
import com.tw.aquaculture.manager.orgpondmap.repository.OwnOrganizationRepository;
import com.tw.aquaculture.manager.service.common.OwnOrganizationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * 组织实现类
 *
 * @author 陈荣
 * @date 2019/11/27 11:41
 */
@Service
public class OwnOrganizationServiceImpl implements OwnOrganizationService {

  @Autowired
  private OrganizationRepository organizationRepository;

  @Autowired
  private OwnOrganizationRepository ownOrganizationRepository;

  @Override
  public OrganizationEntity findDetailsById(String baseOrgId) {
    if (StringUtils.isBlank(baseOrgId)) {
      return null;
    }
    return organizationRepository.findDetailsById(baseOrgId);
  }

  @Override
  public OrganizationEntity findById(String baseOrgId) {
    if (StringUtils.isBlank(baseOrgId)) {
      return null;
    }
    return organizationRepository.findById(baseOrgId).orElse(null);
  }


  @Override
  public OrganizationEntity create(OrganizationEntity organizationEntity) {

    return organizationRepository.saveAndFlush(organizationEntity);
  }


  @Override
  public Set<OrganizationEntity> findTopOrgs() {
    return ownOrganizationRepository.findTop();
  }

  @Override
  public Set<OrganizationEntity> findByParent(String parentId) {
    if(StringUtils.isBlank(parentId))return new HashSet<>();
    return ownOrganizationRepository.findByParentId(parentId);
  }

}
