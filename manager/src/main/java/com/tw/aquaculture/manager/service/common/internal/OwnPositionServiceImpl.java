package com.tw.aquaculture.manager.service.common.internal;

import com.bizunited.platform.core.entity.PositionEntity;
import com.bizunited.platform.core.repository.PositionRepository;
import com.tw.aquaculture.manager.orgpondmap.repository.OwnPositionRepository;
import com.tw.aquaculture.manager.service.common.OwnPositionService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * 岗位实现类
 *
 * @author 陈荣
 * @date 2019/11/27 11:42
 */
@Service
public class OwnPositionServiceImpl implements OwnPositionService {

  @Autowired
  private PositionRepository positionRepository;
  @Autowired
  private OwnPositionRepository ownPositionRepository;

  @Override
  public PositionEntity findDtailById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    return positionRepository.findDetailsById(id);
  }

  @Override
  public PositionEntity findDetailsById(String positionId) {
    if (StringUtils.isBlank(positionId)) {
      return null;
    }
    return positionRepository.findDetailsById(positionId);
  }

  @Override
  public Set<PositionEntity> findAllDetails() {
    return this.ownPositionRepository.findAllDetail();
  }

  @Override
  public List<Pair<String, String>> findAllPositionIdAndParentId() {
    return ownPositionRepository.findAllPositionIdAndParentId();
  }
}
