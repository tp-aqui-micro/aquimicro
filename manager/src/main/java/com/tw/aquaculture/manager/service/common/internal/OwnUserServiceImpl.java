package com.tw.aquaculture.manager.service.common.internal;

import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.core.repository.UserRepository;
import com.tw.aquaculture.manager.service.common.OwnUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用户实现类
 *
 * @author 陈荣
 * @date 2019/11/27 11:41
 */
@Service
public class OwnUserServiceImpl implements OwnUserService {

  @Autowired
  private UserRepository userRepository;

  @Override
  public UserEntity findDetailsById(String userId) {
    if (StringUtils.isBlank(userId)) {
      return null;
    }
    return userRepository.findDetailsById(userId);
  }
}
