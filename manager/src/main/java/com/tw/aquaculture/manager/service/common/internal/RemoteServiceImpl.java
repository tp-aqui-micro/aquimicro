package com.tw.aquaculture.manager.service.common.internal;

import com.bizunited.platform.core.entity.OrganizationEntity;
import com.tw.aquaculture.common.entity.base.CustomerEntity;
import com.tw.aquaculture.common.entity.process.OutApplyCustomStateEntity;
import com.tw.aquaculture.common.entity.process.OutFishApplyEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.vo.remote.saleapply.SaleItemVo;
import com.tw.aquaculture.common.vo.remote.saleapply.SaleRequestBodyVo;
import com.tw.aquaculture.common.vo.remote.saleapply.SaleRequestVo;
import com.tw.aquaculture.common.vo.remote.saleapply.SaleResponseDataVo;
import com.tw.aquaculture.manager.service.common.RemoteService;
import com.tw.aquaculture.manager.service.process.OutApplyCustomStateEntityService;
import com.tw.aquaculture.manager.service.process.OutFishApplyEntityService;
import com.tw.aquaculture.manager.service.remote.CodeGenerateService;
import com.tw.aquaculture.manager.service.remote.CustomerEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author 陈荣
 * @date 2019/12/13 10:51
 */
@Service
public class RemoteServiceImpl implements RemoteService {
  @Autowired
  private CustomerEntityService customerEntityService;
  @Autowired
  private OutFishApplyEntityService outFishApplyEntityService;
  @Autowired
  private OutApplyCustomStateEntityService outApplyCustomStateEntityService;
  @Autowired
  private CodeGenerateService codeGenerateService;

  @Override
  @Transactional
  public SaleResponseDataVo saleApplyCallback(SaleRequestVo saleRequestVo) throws ParseException {
    SaleRequestBodyVo saleRequestBodyVo = saleRequestVo.getData();
    if(saleRequestBodyVo == null){
      return null;
    }
    List<SaleItemVo> saleItemVos = saleRequestBodyVo.getSaleItems();
    if(CollectionUtils.isEmpty(saleItemVos)){
      return null;
    }
    String outFishApplyId = saleRequestVo.getTransactionId();
    OutFishApplyEntity outFishApplyEntity = outFishApplyEntityService.findDetailsById(outFishApplyId);
    if(outFishApplyEntity == null){
      outFishApplyId = saleRequestBodyVo.getBussinessId();
      outFishApplyEntity = outFishApplyEntityService.findDetailsById(outFishApplyId);
    }
    Validate.notNull(outFishApplyEntity, "没有获取到岀鱼申请，请检查事务ID和业务表单ID");
    outFishApplyEntity.setResureState(saleRequestBodyVo.getStatus());
    outFishApplyEntity.setResureIdea(saleRequestBodyVo.getMessage());
    outFishApplyEntityService.updateResureState(outFishApplyId, saleRequestBodyVo.getStatus(), saleRequestBodyVo.getMessage());
    List<OutApplyCustomStateEntity> outApplyCustomStateEntities = outApplyCustomStateEntityService.findByOutFishApply(outFishApplyId);
    for(SaleItemVo saleItemVo : saleItemVos){
      CustomerEntity customerEntity = new CustomerEntity();
      customerEntity.setCustomerType(null);
      if(outFishApplyEntity.getBase()!=null && outFishApplyEntity.getBase().getBaseOrg()!=null && outFishApplyEntity.getBase().getBaseOrg()!=null){
        OrganizationEntity company = new OrganizationEntity();
        company.setId(outFishApplyEntity.getBase().getBaseOrg().getParent().getId());
        customerEntity.setCompany(company);
      }
      customerEntity.setTwLinkUser(null);
      customerEntity.setEbsCode(null);
      customerEntity.setLinkName(saleItemVo.getContactsNAME());
      customerEntity.setName(saleItemVo.getCustomerName());
      customerEntity.setPhone(saleItemVo.getContactnumber());
      customerEntity.setEnableState(EnableStateEnum.ENABLE.getState());
      List<CustomerEntity> oldCustomers = customerEntityService.findByNameAndPhone(customerEntity.getName(), customerEntity.getPhone());
      /*保存客户*/
      CustomerEntity customerResult;
      if(CollectionUtils.isEmpty(oldCustomers)){
        customerEntity.setCode(codeGenerateService.generateCode(CodeTypeEnum.CUSTOMER_ENTITY));
        customerEntity.setCreatePosition("接口传入");
        customerEntity.setCreateName("接口传入");
        customerEntity.setCreateTime(new Date());
        customerEntity.setFormInstanceId(UUID.randomUUID().toString());
        customerResult = customerEntityService.create(customerEntity);
        /*InstanceEntity instanceEntity = (InstanceEntity) *///customerResult = (CustomerEntity) formInstanceHandleService.create(customerEntity, "CustomerEntity", "CustomerEntityService.create", false);
        /*String formInstanceId = instanceEntity.getId();
        customerEntity.setFormInstanceId(formInstanceId);
        customerResult = customerEntityService.create(customerEntity);*/
      }else{
        customerEntity.setId(oldCustomers.get(0).getId());
        customerEntity.setCode(oldCustomers.get(0).getCode());
        customerEntity.setFormInstanceId(oldCustomers.get(0).getFormInstanceId());
        customerEntity.setCompany(oldCustomers.get(0).getCompany());
        customerEntity.setModifyTime(new Date());
        customerEntity.setModifyName("接口传入");
        customerEntity.setModifyPosition("接口传入");
        customerEntity.setCreatePosition(oldCustomers.get(0).getCreatePosition());
        customerEntity.setCreateName(oldCustomers.get(0).getCreateName());
        customerResult = customerEntityService.update(customerEntity);
      }

      if(customerResult == null){
        continue;
      }
      OutApplyCustomStateEntity outApplyCustomStateEntity = new OutApplyCustomStateEntity();
      outApplyCustomStateEntity.setBaseName(saleItemVo.getBaseName());
      outApplyCustomStateEntity.setFreight(saleItemVo.getFreight());
      outApplyCustomStateEntity.setFreightcomfirm(saleItemVo.getFreightcomfirm());
      outApplyCustomStateEntity.setPondName(saleItemVo.getPondName());
      String time = saleItemVo.getPriceDeadline();
      if(StringUtils.isNotBlank(time)){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        time = time.substring(0, 19);
        outApplyCustomStateEntity.setPriceDeadline(sdf.parse(time));
      }

      outApplyCustomStateEntity.setPondName(saleItemVo.getPondName());
      outApplyCustomStateEntity.setQuantityNum(saleItemVo.getQuantityNum());
      outApplyCustomStateEntity.setRequireSpecification(saleItemVo.getRequireSpecification());
      outApplyCustomStateEntity.setSalesfreight(saleItemVo.getSalesfreight());
      outApplyCustomStateEntity.setTangkouprice(saleItemVo.getTangkouprice());
      //outApplyCustomStateEntity.setState();
      outApplyCustomStateEntity.setWinningMark(saleItemVo.getWinningMark());
      outApplyCustomStateEntity.setVarietiesSales(saleItemVo.getVarietiesSales());
      outApplyCustomStateEntity.setTangkouPricecomfirm(saleItemVo.getTangkouPricecomfirm());
      outApplyCustomStateEntity.setSalesRemarks(saleItemVo.getSalesRemarks());
      outApplyCustomStateEntity.setCustomer(customerResult);
      outApplyCustomStateEntity.setOutFishApply(outFishApplyEntity);
      /*保存岀鱼申请审批信息*/
      saveOutApplyCustomStateEntity(outApplyCustomStateEntity, outApplyCustomStateEntities);
    }
    return new SaleResponseDataVo(saleRequestBodyVo.getBussinessId(), null);
  }

  private OutApplyCustomStateEntity saveOutApplyCustomStateEntity(@NotNull OutApplyCustomStateEntity outApplyCustomStateEntity,
                                                                  List<OutApplyCustomStateEntity> outApplyCustomStateEntities) {
    if(CollectionUtils.isEmpty(outApplyCustomStateEntities)){
      return outApplyCustomStateEntityService.create(outApplyCustomStateEntity);
    }else{
      for(OutApplyCustomStateEntity out : outApplyCustomStateEntities){
        if(out.getCustomer().getId().equals(outApplyCustomStateEntity.getId())){
          BeanUtils.copyProperties(outApplyCustomStateEntity, out);
          return outApplyCustomStateEntityService.update(out);
        }
      }
      return outApplyCustomStateEntityService.create(outApplyCustomStateEntity);
    }
  }
}
