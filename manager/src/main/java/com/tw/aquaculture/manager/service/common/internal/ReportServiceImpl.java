package com.tw.aquaculture.manager.service.common.internal;

import com.bizunited.platform.rbac.server.service.PositionService;
import com.bizunited.platform.rbac.server.service.UserService;
import com.bizunited.platform.rbac.server.vo.PositionVo;
import com.bizunited.platform.rbac.server.vo.UserVo;
import com.google.common.collect.Lists;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.common.vo.reportslist.*;
import com.tw.aquaculture.manager.service.common.CommonDataViewService;
import com.tw.aquaculture.manager.service.common.ReportService;
import com.tw.aquaculture.manager.service.plan.OutFishPlanEntityService;
import com.tw.aquaculture.manager.service.plan.PutFryPlanEntityService;
import com.tw.aquaculture.manager.service.plan.YearlyPlanEntityService;
import com.tw.aquaculture.manager.service.process.*;
import com.tw.aquaculture.manager.service.remote.TwBaseEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.util.List;

/**
 * @author 陈荣
 * @date 2020/1/7 10:07
 */
@Service
public class ReportServiceImpl implements ReportService {

  private final static String ADMIN = "admin";

  @Autowired
  private PushFeedEntityService pushFeedEntityService;
  @Autowired
  private SafeguardEntityService safeguardEntityService;
  @Autowired
  private FishDamageEntityService fishDamageEntityService;
  @Autowired
  private PositionService positionService;
  @Autowired
  private UserService userService;
  @Autowired
  private CommonDataViewService commonDataViewService;
  @Autowired
  private ProofYieldEntityService proofYieldEntityService;
  @Autowired
  private TwBaseEntityService twBaseEntityService;
  @Autowired
  private DividePondEntityService dividePondEntityService;
  @Autowired
  private YearlyPlanEntityService yearlyPlanEntityService;
  @Autowired
  private OutFishPlanEntityService outFishPlanEntityService;
  @Autowired
  private PutFryPlanEntityService putFryPlanEntityService;

  @Override
  public List<PushFeedReportVo> pushFeedReport(ReportParamVo reportParamVo, String account) {
    buildParam(reportParamVo, account);
    return pushFeedEntityService.pushFeedReport(reportParamVo);
  }

  /**
   * 构建初始化权限参数（到基地）
   *
   * @param reportParamVo
   * @param account
   */
  private void buildParam(Object reportParamVo, String account) {
    List<TwBaseEntity> bases = null;
    String baseId = null;
    Class cls = reportParamVo.getClass();
    try {
      Field field = cls.getDeclaredField("baseId");
      field.setAccessible(true);
      Object baseIdObj = field.get(reportParamVo);
      baseId = baseIdObj == null ? null : baseIdObj.toString();
      if (!ADMIN.equals(account) && StringUtils.isBlank(baseId)) {
        Validate.notNull(account, "没有获取到当前登录用户, 请重新登录");
        UserVo userVo = userService.findByAccount(account);
        Validate.notNull(userVo, "没有获取到当前登录用户, 请重新登录");
        PositionVo positionVo = positionService.findMainPositionByUserId(userVo.getId());
        Validate.notNull(positionVo, "没有获取到岗位信息，请先分配岗位");
        bases = commonDataViewService.findBaseByPositionId(positionVo.getId());
        Validate.notEmpty(bases, "没有获取到基地");
      } else if (ADMIN.equals(account) && StringUtils.isBlank(baseId)) {
        bases = twBaseEntityService.findAll();
        Validate.notEmpty(bases, "没有获取到基地");
      }
      if (!CollectionUtils.isEmpty(bases)) {
        StringBuffer bs = new StringBuffer();
        for (TwBaseEntity base : bases) {
          bs.append(base.getId());
          bs.append(",");
        }
        //reportParamVo.setBaseId(bs.toString().substring(0, bs.toString().lastIndexOf(",")));
        field.set(reportParamVo, bs.toString().substring(0, bs.toString().lastIndexOf(",")));
      }
    } catch (NoSuchFieldException | IllegalAccessException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public List<SafeReportVo> safeReport(ReportParamVo reportParamVo, String account) {
    buildParam(reportParamVo, account);
    return safeguardEntityService.safeReport(reportParamVo);
  }

  @Override
  public List<FishDamageReportVo> fishDamageReport(ReportParamVo reportParamVo, String account) {
    buildParam(reportParamVo, account);
    return this.fishDamageEntityService.fishDamageReport(reportParamVo);
  }

  @Override
  public List<ExistAmountReportVo> existAmountReport(ReportParamVo reportParamVo, String acount) {
    buildParam(reportParamVo, acount);
    List<ExistAmountReportVo> result = Lists.newArrayList();
    String[] bases = reportParamVo.getBaseId().split(",");
    /*if (bases.length > 2 && StringUtils.isBlank(reportParamVo.getPondId())) {
      for (String baseId : bases) {
        reportParamVo.setBaseId(baseId);
        List<ExistAmountReportVo> existAmountReportVos = this.proofYieldEntityService.existAmountReport(reportParamVo);
        if (!CollectionUtils.isEmpty(existAmountReportVos)) {
          result.addAll(existAmountReportVos);
        }
      }
    } else {
      result = this.proofYieldEntityService.existAmountReport(reportParamVo);
    }*/
    List<ExistAmountReportVo> existAmountReportVos = this.proofYieldEntityService.existAmountReport(reportParamVo);
    if (!CollectionUtils.isEmpty(existAmountReportVos)) {
      result.addAll(existAmountReportVos);
    }
    return result;
  }

  @Override
  public List<ExistAmountReportVo> stockReport(StockReportParamVo stockReportParamVo, String acount) {
    buildParam(stockReportParamVo, acount);
    return null;
  }

  @Override
  public List<DividePondResultVo> dividePondReport(DividePondParamVo dividePondParamVo, String account) {
    buildParam(dividePondParamVo, account);
    return dividePondEntityService.dividePondReport(dividePondParamVo);
  }

  @Override
  public List<YearlyPlanReportResultVo> yearlyPlanReport(YearlyPlanReportParamVo yearlyPlanReportParamVo, String account) {
    buildParam(yearlyPlanReportParamVo, account);
    return yearlyPlanEntityService.yearlyPlanReport(yearlyPlanReportParamVo);
  }

  @Override
  public List<OutFishPlanReportResultVo> outFishPlanReport(OutFishPlanReportParamVo outFishPlanReportParamVo, String account) {
    buildParam(outFishPlanReportParamVo, account);
    return outFishPlanEntityService.outFishPlanReport(outFishPlanReportParamVo);
  }

  @Override
  public List<PushFryPlanReportResultVo> pushFryPlanReport(PushFryPlanReportParamVo pushFryPlanReportParamVo, String account) {
    buildParam(pushFryPlanReportParamVo, account);
    return this.putFryPlanEntityService.pushFryPlanReport(pushFryPlanReportParamVo);
  }


}
