package com.tw.aquaculture.manager.service.init;

import com.bizunited.platform.core.entity.RoleEntity;
import com.bizunited.platform.core.repository.RoleRepository;
import com.bizunited.platform.core.service.init.InitProcessService;
import com.tw.aquaculture.common.constant.RoleConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 水产系统存在一些特殊角色, 一些业务场景对这种角色会有特殊的处理
 * <p>
 * 这个初始化类专门用来做这特殊角色的初始化
 *
 * @author zizhou
 * @date 2019/12/28
 */
@Component("initRoleService")
public class InitRoleService implements InitProcessService {

  @Autowired
  private RoleRepository roleRepository;

  @Override
  public boolean doProcess() {
    return true;
  }

  @Override
  public boolean stopOnException() {
    return true;
  }

  @Override
  public void init() {
    if (roleRepository.findByRoleName(RoleConstants.UP_SELECT_ROLE) == null) {
      initUpSelectRole();
    }
  }


  /**
   * 初始化 UP_SELECT_ROLE 这个特殊角色
   */
  private void initUpSelectRole() {
    RoleEntity role = new RoleEntity();
    role.setComment("拥有此角色的用户数据视图查询时能够查询到它父级和父级下所有组织的数据");
    role.setCreateDate(new Date());
    role.setRoleName(RoleConstants.UP_SELECT_ROLE);
    role.setRoleCode(RoleConstants.UP_SELECT_ROLE);
    role.setTstatus(1);
    role.setIsDeny(true);
    this.roleRepository.saveAndFlush(role);
  }
}
