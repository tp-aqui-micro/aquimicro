package com.tw.aquaculture.manager.service.plan.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.servicecentre.NoticeEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.plan.NoticeFeign;
import com.tw.aquaculture.manager.service.plan.NoticeEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * NoticeEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("NoticeEntityServiceImpl")
public class NoticeEntityServiceImpl implements NoticeEntityService {
  @Autowired
  private NoticeFeign noticeFeign;

  @Transactional
  @Override
  public NoticeEntity create(NoticeEntity noticeEntity) {
    ResponseModel responseModel = noticeFeign.create(noticeEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<NoticeEntity>() {
    });
  }

  @Transactional
  @Override
  public NoticeEntity createForm(NoticeEntity noticeEntity) {
    return null;
  }


  @Transactional
  @Override
  public NoticeEntity update(NoticeEntity noticeEntity) {
    ResponseModel responseModel = noticeFeign.update(noticeEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<NoticeEntity>() {
    });
  }

  @Override
  public NoticeEntity updateForm(NoticeEntity noticeEntity) {
    return null;
  }

  @Override
  public NoticeEntity findDetailsById(String id) {
    ResponseModel responseModel = noticeFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<NoticeEntity>() {
    });
  }

  @Override
  public NoticeEntity findById(String id) {
    ResponseModel responseModel = noticeFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<NoticeEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {

    noticeFeign.deleteById(id);
  }

  @Override
  public NoticeEntity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = noticeFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<NoticeEntity>() {
    });
  }

  @Override
  public NoticeEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = noticeFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<NoticeEntity>() {
    });
  }

  @Override
  public void checkState() {
    noticeFeign.checkState();
  }

} 
