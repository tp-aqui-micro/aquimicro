package com.tw.aquaculture.manager.service.plan.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.plan.OutFishPlanEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.common.vo.reportslist.OutFishPlanReportParamVo;
import com.tw.aquaculture.common.vo.reportslist.OutFishPlanReportResultVo;
import com.tw.aquaculture.manager.feign.plan.OutFishPlanFeign;
import com.tw.aquaculture.manager.service.plan.OutFishPlanEntityService;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * OutFishPlanEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("OutFishPlanEntityServiceImpl")
public class OutFishPlanEntityServiceImpl implements OutFishPlanEntityService {
  @Autowired
  private OutFishPlanFeign outFishPlanFeign;

  @Transactional
  @Override
  public OutFishPlanEntity create(OutFishPlanEntity outFishPlanEntity) {

    ResponseModel responseModel = outFishPlanFeign.create(outFishPlanEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishPlanEntity>() {
    });
  }

  @Transactional
  @Override
  public OutFishPlanEntity createForm(OutFishPlanEntity outFishPlanEntity) {
    return null;
  }

  /**
   * 在创建一个新的OutFishPlanEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  @Transactional
  @Override
  public OutFishPlanEntity update(OutFishPlanEntity outFishPlanEntity) {

    ResponseModel responseModel = outFishPlanFeign.update(outFishPlanEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishPlanEntity>() {
    });
  }

  @Transactional
  @Override
  public OutFishPlanEntity updateForm(OutFishPlanEntity outFishPlanEntity) {
    return null;
  }

  @Override
  public OutFishPlanEntity findDetailsById(String id) {

    ResponseModel responseModel = outFishPlanFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishPlanEntity>() {
    });
  }

  @Override
  public OutFishPlanEntity findById(String id) {

    ResponseModel responseModel = outFishPlanFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishPlanEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    OutFishPlanEntity current = this.findById(id);
    Validate.notNull(current , "删除时未找到指定的主键对应的出鱼计划信息!!");
    outFishPlanFeign.deleteById(id);
  }

  @Override
  public OutFishPlanEntity findDetailsByFormInstanceId(String formInstanceId) {

    ResponseModel responseModel = outFishPlanFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishPlanEntity>() {
    });
  }

  @Override
  public OutFishPlanEntity findByFormInstanceId(String formInstanceId) {

    ResponseModel responseModel = outFishPlanFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishPlanEntity>() {
    });
  }

  @Override
  public List<OutFishPlanReportResultVo> outFishPlanReport(OutFishPlanReportParamVo outFishPlanReportParamVo) {
    ResponseModel responseModel = outFishPlanFeign.outFishPlanReport(outFishPlanReportParamVo);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<OutFishPlanReportResultVo>>() {
    });
  }
} 
