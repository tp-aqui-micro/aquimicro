package com.tw.aquaculture.manager.service.plan.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.plan.PutFryPlanEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.common.vo.reportslist.PushFryPlanReportParamVo;
import com.tw.aquaculture.common.vo.reportslist.PushFryPlanReportResultVo;
import com.tw.aquaculture.manager.feign.plan.PutFryPlanFeign;
import com.tw.aquaculture.manager.service.plan.PutFryPlanEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;

/**
 * PutFryPlanEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("PutFryPlanEntityServiceImpl")
public class PutFryPlanEntityServiceImpl implements PutFryPlanEntityService {
  @Autowired
  private PutFryPlanFeign putFryPlanFeign;

  @Transactional
  @Override
  public PutFryPlanEntity create(PutFryPlanEntity putFryPlanEntity) {

    ResponseModel responseModel = putFryPlanFeign.create(putFryPlanEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PutFryPlanEntity>() {
    });
  }

  @Transactional
  @Override
  public PutFryPlanEntity createForm(PutFryPlanEntity putFryPlanEntity) {
    return null;
  }

  /**
   * 在创建一个新的PutFryPlanEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  @Transactional
  @Override
  public PutFryPlanEntity update(PutFryPlanEntity putFryPlanEntity) {

    ResponseModel responseModel = putFryPlanFeign.update(putFryPlanEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PutFryPlanEntity>() {
    });
  }

  @Transactional
  @Override
  public PutFryPlanEntity updateForm(PutFryPlanEntity putFryPlanEntity) {

    return null;
  }

  /**
   * 在更新一个已有的PutFryPlanEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  @Override
  public PutFryPlanEntity findDetailsById(String id) throws IOException {
    ResponseModel result = putFryPlanFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(result, new TypeReference<PutFryPlanEntity>() {
    });
  }


  @Override
  public PutFryPlanEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = putFryPlanFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PutFryPlanEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    PutFryPlanEntity current = this.findById(id);
    Validate.notNull(current , "删除时未找到指定的投苗计划信息!!");
    putFryPlanFeign.deleteById(id);
  }

  @Override
  public PutFryPlanEntity findDetailsByFormInstanceId(String formInstanceId) {

    ResponseModel responseModel = putFryPlanFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PutFryPlanEntity>() {
    });
  }

  @Override
  public PutFryPlanEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = putFryPlanFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PutFryPlanEntity>() {
    });
  }

  @Override
  public List<PushFryPlanReportResultVo> pushFryPlanReport(PushFryPlanReportParamVo pushFryPlanReportParamVo) {
    ResponseModel responseModel = putFryPlanFeign.pushFryPlanReport(pushFryPlanReportParamVo);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<PushFryPlanReportResultVo>>() {
    });
  }
} 
