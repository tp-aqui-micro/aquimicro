package com.tw.aquaculture.manager.service.plan.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.plan.YearlyPlanEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.common.vo.reportslist.YearlyPlanReportParamVo;
import com.tw.aquaculture.common.vo.reportslist.YearlyPlanReportResultVo;
import com.tw.aquaculture.manager.feign.plan.YearlyPlanFeign;
import com.tw.aquaculture.manager.service.plan.YearlyPlanEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * YearlyPlanEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("YearlyPlanEntityServiceImpl")
public class YearlyPlanEntityServiceImpl implements YearlyPlanEntityService {
  @Autowired
  private YearlyPlanFeign yearlyPlanFeign;

  @Transactional
  @Override
  public YearlyPlanEntity create(YearlyPlanEntity yearlyPlanEntity) {

    ResponseModel responseModel = yearlyPlanFeign.create(yearlyPlanEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<YearlyPlanEntity>() {
    });
  }

  @Transactional
  @Override
  public YearlyPlanEntity createForm(YearlyPlanEntity yearlyPlanEntity) {

    return null;
  }

  @Transactional
  @Override
  public YearlyPlanEntity update(YearlyPlanEntity yearlyPlanEntity) {

    ResponseModel responseModel = yearlyPlanFeign.update(yearlyPlanEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<YearlyPlanEntity>() {
    });
  }

  @Transactional
  @Override
  public YearlyPlanEntity updateForm(YearlyPlanEntity yearlyPlanEntity) {

    return null;
  }

  /**
   * 在更新一个已有的YearlyPlanEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  @Override
  public YearlyPlanEntity findDetailsById(String id) {

    ResponseModel responseModel = yearlyPlanFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<YearlyPlanEntity>() {
    });
  }

  @Override
  public YearlyPlanEntity findById(String id) {

    ResponseModel responseModel = yearlyPlanFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<YearlyPlanEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    YearlyPlanEntity current = this.findById(id);
    if (current != null) {
      this.yearlyPlanFeign.deleteById(id);
    }
  }

  @Override
  public YearlyPlanEntity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = yearlyPlanFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<YearlyPlanEntity>() {
    });
  }

  @Override
  public YearlyPlanEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = yearlyPlanFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<YearlyPlanEntity>() {
    });
  }

  @Override
  public List<YearlyPlanReportResultVo> yearlyPlanReport(YearlyPlanReportParamVo yearlyPlanReportParamVo) {
    ResponseModel responseModel = yearlyPlanFeign.yearlyPlanReport(yearlyPlanReportParamVo);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<YearlyPlanReportResultVo>>() {
    });
  }
} 
