package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.BaskPondEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.process.BaskPondEntityFeign;
import com.tw.aquaculture.manager.service.process.BaskPondEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * BaskPondEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("BaskPondEntityServiceImpl")
public class BaskPondEntityServiceImpl implements BaskPondEntityService {

  @Autowired
  private BaskPondEntityFeign baskPondEntityFeign;

  @Transactional
  @Override
  public BaskPondEntity create(BaskPondEntity baskPondEntity) {
    ResponseModel responseModel = baskPondEntityFeign.create(baskPondEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<BaskPondEntity>() {
    });
  }

  @Transactional
  @Override
  public BaskPondEntity createForm(BaskPondEntity baskPondEntity) {
    return null;
  }

  @Transactional
  @Override
  public BaskPondEntity update(BaskPondEntity baskPondEntity) {
    ResponseModel responseModel = baskPondEntityFeign.update(baskPondEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<BaskPondEntity>() {
    });
  }

  @Transactional
  @Override
  public BaskPondEntity updateForm(BaskPondEntity baskPondEntity) {
    return null;
  }

  @Override
  public BaskPondEntity findDetailsById(String id) {
    ResponseModel responseModel = baskPondEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<BaskPondEntity>() {
    });
  }

  @Override
  public BaskPondEntity findById(String id) {
    ResponseModel responseModel = baskPondEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<BaskPondEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    baskPondEntityFeign.deleteById(id);
  }

  @Override
  public BaskPondEntity findDetailsByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = baskPondEntityFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<BaskPondEntity>() {
    });
  }

  @Override
  public BaskPondEntity findByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = baskPondEntityFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<BaskPondEntity>() {
    });
  }
} 
