package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.CleanPondEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.process.CleanPondEntityFeign;
import com.tw.aquaculture.manager.service.process.CleanPondEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * CleanPondEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("CleanPondEntityServiceImpl")
public class CleanPondEntityServiceImpl implements CleanPondEntityService {
  @Autowired
  private CleanPondEntityFeign cleanPondEntityFeign;

  @Transactional
  @Override
  public CleanPondEntity create(CleanPondEntity cleanPondEntity) {
    ResponseModel responseModel = cleanPondEntityFeign.create(cleanPondEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CleanPondEntity>() {
    });
  }

  @Transactional
  @Override
  public CleanPondEntity createForm(CleanPondEntity cleanPondEntity) {
    return null;
  }

  @Transactional
  @Override
  public CleanPondEntity update(CleanPondEntity cleanPondEntity) {
    ResponseModel responseModel = cleanPondEntityFeign.update(cleanPondEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CleanPondEntity>() {
    });
  }

  @Transactional
  @Override
  public CleanPondEntity updateForm(CleanPondEntity cleanPondEntity) {
    return null;
  }

  @Override
  public CleanPondEntity findDetailsById(String id) {
    ResponseModel responseModel = cleanPondEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CleanPondEntity>() {
    });
  }

  @Override
  public CleanPondEntity findById(String id) {
    ResponseModel responseModel = cleanPondEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CleanPondEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    cleanPondEntityFeign.deleteById(id);
  }

  @Override
  public CleanPondEntity findDetailsByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = cleanPondEntityFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CleanPondEntity>() {
    });
  }

  @Override
  public CleanPondEntity findByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = cleanPondEntityFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CleanPondEntity>() {
    });
  }
} 
