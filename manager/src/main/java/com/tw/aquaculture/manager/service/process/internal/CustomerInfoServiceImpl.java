package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.CustomerInfo;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.process.CustomerInfoFeign;
import com.tw.aquaculture.manager.service.process.CustomerInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;

/**
 * CustomerInfo业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("CustomerInfoServiceImpl")
public class CustomerInfoServiceImpl implements CustomerInfoService {

  @Autowired
  private CustomerInfoFeign customerInfoFeign;

  @Transactional
  @Override
  public CustomerInfo create(CustomerInfo customerInfo) {
    ResponseModel responseModel = customerInfoFeign.create(customerInfo);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CustomerInfo>() {
    });
  }

  @Transactional
  @Override
  public CustomerInfo createForm(CustomerInfo customerInfo) {
    return null;
  }

  @Transactional
  @Override
  public CustomerInfo update(CustomerInfo customerInfo) {
    ResponseModel responseModel = customerInfoFeign.update(customerInfo);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CustomerInfo>() {
    });
  }

  @Transactional
  @Override
  public CustomerInfo updateForm(CustomerInfo customerInfo) {
    return null;
  }

  @Override
  public Set<CustomerInfo> findDetailsByFishType(String fishType) {
    ResponseModel responseModel = customerInfoFeign.findDetailsByFishType(fishType);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Set<CustomerInfo>>() {
    });
  }

  @Override
  public Set<CustomerInfo> findDetailsByOutFishApply(String outFishApply) {
    ResponseModel responseModel = customerInfoFeign.findDetailsByOutFishApply(outFishApply);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Set<CustomerInfo>>() {
    });
  }

  @Override
  public CustomerInfo findDetailsById(String id) {
    ResponseModel responseModel = customerInfoFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CustomerInfo>() {
    });
  }

  @Override
  public CustomerInfo findById(String id) {
    ResponseModel responseModel = customerInfoFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CustomerInfo>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    customerInfoFeign.deleteById(id);
  }
} 
