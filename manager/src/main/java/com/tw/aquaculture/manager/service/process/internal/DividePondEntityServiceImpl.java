package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.DividePondEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.common.vo.reportslist.DividePondResultVo;
import com.tw.aquaculture.common.vo.reportslist.DividePondParamVo;
import com.tw.aquaculture.manager.feign.process.DividePondEntityFeign;
import com.tw.aquaculture.manager.service.process.DividePondEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

/**
 * DividePondEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("DividePondEntityServiceImpl")
public class DividePondEntityServiceImpl implements DividePondEntityService {
  @Autowired
  private DividePondEntityFeign dividePondEntityFeign;


  @Transactional
  @Override
  public DividePondEntity create(DividePondEntity dividePondEntity) {
    ResponseModel responseModel = dividePondEntityFeign.create(dividePondEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<DividePondEntity>() {
    });
  }

  @Transactional
  @Override
  public DividePondEntity createForm(DividePondEntity dividePondEntity) {
    return null;
  }

  @Transactional
  @Override
  public DividePondEntity update(DividePondEntity dividePondEntity) {
    ResponseModel responseModel = dividePondEntityFeign.update(dividePondEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<DividePondEntity>() {
    });
  }

  @Transactional
  @Override
  public DividePondEntity updateForm(DividePondEntity dividePondEntity) {
    return null;
  }

  @Override
  public DividePondEntity findDetailsById(String id) {
    ResponseModel responseModel = dividePondEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<DividePondEntity>() {
    });
  }

  @Override
  public DividePondEntity findById(String id) {
    ResponseModel responseModel = dividePondEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<DividePondEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    dividePondEntityFeign.deleteById(id);
  }

  @Override
  public DividePondEntity findDetailsByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = dividePondEntityFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<DividePondEntity>() {
    });
  }

  @Override
  public DividePondEntity findByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = dividePondEntityFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<DividePondEntity>() {
    });
  }

  @Override
  public List<DividePondEntity> findByInPondAndTimes(String pondId, Date startTime, Date endTime) {
    ResponseModel responseModel = dividePondEntityFeign.findByInPondAndTimes(pondId, startTime, endTime);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<DividePondEntity>>() {
    });
  }

  @Override
  public List<DividePondEntity> findByOutPondAndTimes(String pondId, Date startTime, Date endTime) {
    ResponseModel responseModel = dividePondEntityFeign.findByOutPondAndTimes(pondId, startTime, endTime);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<DividePondEntity>>() {
    });
  }

  @Override
  public List<DividePondResultVo> dividePondReport(DividePondParamVo dividePondParamVo) {
    ResponseModel responseModel = dividePondEntityFeign.dividePondReport(dividePondParamVo);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<DividePondResultVo>>() {
    });
  }
} 
