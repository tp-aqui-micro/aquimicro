package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.DividePondItemEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.process.DividePondItemEntityFeign;
import com.tw.aquaculture.manager.service.process.DividePondItemEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;

/**
 * DividePondItemEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("DividePondItemEntityServiceImpl")
public class DividePondItemEntityServiceImpl implements DividePondItemEntityService {

  @Autowired
  private DividePondItemEntityFeign dividePondItemEntityFeign;

  @Transactional
  @Override
  public DividePondItemEntity create(DividePondItemEntity dividePondItemEntity) {
    ResponseModel responseModel = dividePondItemEntityFeign.create(dividePondItemEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<DividePondItemEntity>() {
    });
  }

  @Transactional
  @Override
  public DividePondItemEntity createForm(DividePondItemEntity dividePondItemEntity) {
    return null;
  }

  @Transactional
  @Override
  public DividePondItemEntity update(DividePondItemEntity dividePondItemEntity) {
    ResponseModel responseModel = dividePondItemEntityFeign.update(dividePondItemEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<DividePondItemEntity>() {
    });
  }

  @Transactional
  @Override
  public DividePondItemEntity updateForm(DividePondItemEntity dividePondItemEntity) {
    return null;
  }

  @Override
  public Set<DividePondItemEntity> findDetailsByDividePond(String dividePond) {
    ResponseModel responseModel = dividePondItemEntityFeign.findDetailsByDividePond(dividePond);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Set<DividePondItemEntity>>() {
    });
  }

  @Override
  public DividePondItemEntity findDetailsById(String id) {
    ResponseModel responseModel = dividePondItemEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<DividePondItemEntity>() {
    });
  }

  @Override
  public DividePondItemEntity findById(String id) {
    ResponseModel responseModel = dividePondItemEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<DividePondItemEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    dividePondItemEntityFeign.deleteById(id);
  }
} 
