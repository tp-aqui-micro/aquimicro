package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.FishDamageEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.common.vo.reportslist.FishDamageReportVo;
import com.tw.aquaculture.common.vo.reportslist.ReportParamVo;
import com.tw.aquaculture.manager.feign.process.FishDamageEntityFeign;
import com.tw.aquaculture.manager.service.process.FishDamageEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * FishDamageEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("FishDamageEntityServiceImpl")
public class FishDamageEntityServiceImpl implements FishDamageEntityService {

  @Autowired
  private FishDamageEntityFeign fishDamageEntityFeign;

  @Transactional
  @Override
  public FishDamageEntity create(FishDamageEntity fishDamageEntity) {
    ResponseModel responseModel = fishDamageEntityFeign.create(fishDamageEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishDamageEntity>() {
    });
  }

  @Transactional
  @Override
  public FishDamageEntity createForm(FishDamageEntity fishDamageEntity) {
    return null;
  }

  @Transactional
  @Override
  public FishDamageEntity update(FishDamageEntity fishDamageEntity) {
    ResponseModel responseModel = fishDamageEntityFeign.update(fishDamageEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishDamageEntity>() {
    });
  }

  @Transactional
  @Override
  public FishDamageEntity updateForm(FishDamageEntity fishDamageEntity) {
    return null;
  }

  @Override
  public FishDamageEntity findDetailsById(String id) {
    ResponseModel responseModel = fishDamageEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishDamageEntity>() {
    });
  }

  @Override
  public FishDamageEntity findById(String id) {
    ResponseModel responseModel = fishDamageEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishDamageEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    fishDamageEntityFeign.deleteById(id);
  }

  @Override
  public FishDamageEntity findDetailsByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = fishDamageEntityFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishDamageEntity>() {
    });
  }

  @Override
  public FishDamageEntity findByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = fishDamageEntityFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishDamageEntity>() {
    });
  }

  @Override
  public List<FishDamageEntity> findByPondAndTimes(String pondId, Date startTime, Date endTime) {
    ResponseModel responseModel = fishDamageEntityFeign.findByPondAndTimes(pondId, startTime, endTime);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<FishDamageEntity>>() {
    });
  }

  @Override
  public List<FishDamageReportVo> fishDamageReport(ReportParamVo reportParamVo) {
    ResponseModel responseModel = fishDamageEntityFeign.fishDamageReport(reportParamVo);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<FishDamageReportVo>>() {
    });
  }
} 
