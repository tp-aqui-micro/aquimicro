package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.base.FishUpRateEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.base.FishUpRateFeign;
import com.tw.aquaculture.manager.service.process.FishUpRateEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * FishUpRateEntity业务模型的服务层接口实现
 * @author saturn
 */
@Service("FishUpRateEntityServiceImpl")
public class FishUpRateEntityServiceImpl implements FishUpRateEntityService {
  @Autowired
  private FishUpRateFeign fishUpRateFeign;

  @Override
  public FishUpRateEntity create(FishUpRateEntity fishUpRateEntity) {
    return this.createForm(fishUpRateEntity);
  } 
  @Transactional
  @Override
  public FishUpRateEntity createForm(FishUpRateEntity fishUpRateEntity) {
    ResponseModel responseModel = fishUpRateFeign.create(fishUpRateEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishUpRateEntity>() {
    });
  }

  @Override
  public FishUpRateEntity update(FishUpRateEntity fishUpRateEntity) { 
    FishUpRateEntity current = this.updateForm(fishUpRateEntity);
    //====================================================
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  } 

  @Override
  public FishUpRateEntity updateForm(FishUpRateEntity fishUpRateEntity) {
    ResponseModel responseModel = fishUpRateFeign.update(fishUpRateEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishUpRateEntity>() {
    });
  }

  @Override
  public FishUpRateEntity findDetailsById(String id) {
    ResponseModel responseModel = fishUpRateFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishUpRateEntity>() {
    });
  }
  @Override
  public FishUpRateEntity findById(String id) {
    ResponseModel responseModel = fishUpRateFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishUpRateEntity>() {
    });
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    throw new RuntimeException("暂未开放此接口");
  }
  @Override
  public FishUpRateEntity findDetailsByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = fishUpRateFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishUpRateEntity>() {
    });
  }
  @Override
  public FishUpRateEntity findByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = fishUpRateFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishUpRateEntity>() {
    });
  } 
} 
