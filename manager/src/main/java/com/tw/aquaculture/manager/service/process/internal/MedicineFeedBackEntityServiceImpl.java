package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.MedicineFeedBackEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.process.MedicineFeedBackEntityFeign;
import com.tw.aquaculture.manager.service.process.MedicineFeedBackEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;

/**
 * MedicineFeedBackEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("MedicineFeedBackEntityServiceImpl")
public class MedicineFeedBackEntityServiceImpl implements MedicineFeedBackEntityService {


  @Autowired
  private MedicineFeedBackEntityFeign medicineFeedBackEntityFeign;

  @Transactional
  @Override
  public MedicineFeedBackEntity create(MedicineFeedBackEntity medicineFeedBackEntity) {
    ResponseModel responseModel = medicineFeedBackEntityFeign.create(medicineFeedBackEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<MedicineFeedBackEntity>() {
    });
  }

  @Transactional
  @Override
  public MedicineFeedBackEntity createForm(MedicineFeedBackEntity medicineFeedBackEntity) {
    return null;
  }

  @Transactional
  @Override
  public MedicineFeedBackEntity update(MedicineFeedBackEntity medicineFeedBackEntity) {
    ResponseModel responseModel = medicineFeedBackEntityFeign.update(medicineFeedBackEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<MedicineFeedBackEntity>() {
    });
  }

  @Transactional
  @Override
  public MedicineFeedBackEntity updateForm(MedicineFeedBackEntity medicineFeedBackEntity) {
    return null;
  }

  @Override
  public Set<MedicineFeedBackEntity> findDetailsByParent(String parent) {
    ResponseModel responseModel = medicineFeedBackEntityFeign.findDetailsByParent(parent);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Set<MedicineFeedBackEntity>>() {
    });
  }

  @Override
  public Set<MedicineFeedBackEntity> findDetailsBySafeguard(String safeguard) {
    ResponseModel responseModel = medicineFeedBackEntityFeign.findDetailsBySafeguard(safeguard);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Set<MedicineFeedBackEntity>>() {
    });
  }

  @Override
  public MedicineFeedBackEntity findDetailsById(String id) {
    ResponseModel responseModel = medicineFeedBackEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<MedicineFeedBackEntity>() {
    });
  }

  @Override
  public MedicineFeedBackEntity findById(String id) {
    ResponseModel responseModel = medicineFeedBackEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<MedicineFeedBackEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    medicineFeedBackEntityFeign.deleteById(id);
  }
} 
