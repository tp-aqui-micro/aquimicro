package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Lists;
import com.tw.aquaculture.common.entity.process.OutApplyCustomStateEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.process.OutApplyCustomStateEntityFeign;
import com.tw.aquaculture.manager.service.process.OutApplyCustomStateEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * OutApplyCustomStateEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("OutApplyCustomStateEntityServiceImpl")
public class OutApplyCustomStateEntityServiceImpl implements OutApplyCustomStateEntityService {
  @Autowired
  private OutApplyCustomStateEntityFeign outApplyCustomStateEntityFeign;

  @Transactional
  @Override
  public OutApplyCustomStateEntity create(OutApplyCustomStateEntity outApplyCustomStateEntity) {
    ResponseModel responseModel = outApplyCustomStateEntityFeign.create(outApplyCustomStateEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutApplyCustomStateEntity>() {
    });
  }

  @Transactional
  @Override
  public OutApplyCustomStateEntity createForm(OutApplyCustomStateEntity outApplyCustomStateEntity) {
    return null;
  }

  @Transactional
  @Override
  public OutApplyCustomStateEntity update(OutApplyCustomStateEntity outApplyCustomStateEntity) {
    ResponseModel responseModel = outApplyCustomStateEntityFeign.update(outApplyCustomStateEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutApplyCustomStateEntity>() {
    });
  }

  @Transactional
  @Override
  public OutApplyCustomStateEntity updateForm(OutApplyCustomStateEntity outApplyCustomStateEntity) {
    return null;
  }

  @Override
  public OutApplyCustomStateEntity findDetailsByOutFishApply(String outFishApply) {
    ResponseModel responseModel = outApplyCustomStateEntityFeign.findDetailsByOutFishApply(outFishApply);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutApplyCustomStateEntity>() {
    });
  }

  @Override
  public OutApplyCustomStateEntity findDetailsByCustomer(String customer) {
    ResponseModel responseModel = outApplyCustomStateEntityFeign.findDetailsByCustomer(customer);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutApplyCustomStateEntity>() {
    });
  }

  @Override
  public OutApplyCustomStateEntity findDetailsById(String id) {
    ResponseModel responseModel = outApplyCustomStateEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutApplyCustomStateEntity>() {
    });
  }

  @Override
  public OutApplyCustomStateEntity findById(String id) {
    ResponseModel responseModel = outApplyCustomStateEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutApplyCustomStateEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    outApplyCustomStateEntityFeign.deleteById(id);
  }

  @Override
  public List<OutApplyCustomStateEntity> findByOutFishApplyIdAndState(String outFishApplyId, int state) {
    ResponseModel responseModel = outApplyCustomStateEntityFeign.findByOutFishApplyIdAndState(outFishApplyId, state);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<OutApplyCustomStateEntity>>() {
    });
  }

  @Override
  public List<OutApplyCustomStateEntity> findByOutFishApply(String outFishApplyId) {
    if(StringUtils.isBlank(outFishApplyId)){
      return Lists.newArrayList();
    }
    ResponseModel responseModel = outApplyCustomStateEntityFeign.findByOutFishApply(outFishApplyId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<OutApplyCustomStateEntity>>() {
    });
  }
} 
