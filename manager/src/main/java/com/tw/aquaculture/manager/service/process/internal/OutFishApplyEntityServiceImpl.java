package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.OutApplyCustomStateEntity;
import com.tw.aquaculture.common.entity.process.OutFishApplyEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.process.OutFishApplyEntityFeign;
import com.tw.aquaculture.manager.orgpondmap.repository.OutApplyCustomStateEntityRepository;
import com.tw.aquaculture.manager.service.process.OutFishApplyEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Map;

/**
 * OutFishApplyEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("OutFishApplyEntityServiceImpl")
public class OutFishApplyEntityServiceImpl implements OutFishApplyEntityService {

  @Autowired
  private OutFishApplyEntityFeign outFishApplyEntityFeign;
  @Autowired
  private OutApplyCustomStateEntityRepository outApplyCustomStateEntityRepository;

  @Transactional
  @Override
  public OutFishApplyEntity create(OutFishApplyEntity outFishApplyEntity) {
    ResponseModel responseModel = outFishApplyEntityFeign.create(outFishApplyEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishApplyEntity>() {
    });
  }

  @Transactional
  @Override
  public OutFishApplyEntity createForm(OutFishApplyEntity outFishApplyEntity) {
    return null;
  }

  @Transactional
  @Override
  public OutFishApplyEntity update(OutFishApplyEntity outFishApplyEntity) {
    ResponseModel responseModel = outFishApplyEntityFeign.update(outFishApplyEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishApplyEntity>() {
    });
  }

  @Transactional
  @Override
  public OutFishApplyEntity updateForm(OutFishApplyEntity outFishApplyEntity) {
    return null;
  }

  @Override
  public OutFishApplyEntity findDetailsById(String id) {
    ResponseModel responseModel = outFishApplyEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishApplyEntity>() {
    });
  }

  @Override
  public OutFishApplyEntity findById(String id) {
    ResponseModel responseModel = outFishApplyEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishApplyEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    outFishApplyEntityFeign.deleteById(id);
  }

  @Override
  public OutFishApplyEntity findDetailsByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = outFishApplyEntityFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishApplyEntity>() {
    });
  }

  @Override
  public OutFishApplyEntity findByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = outFishApplyEntityFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishApplyEntity>() {
    });
  }

  @Override
  public void updateResureState(String outFishApplyId, Integer status, String message) {
    ResponseModel responseModel = outFishApplyEntityFeign.updateResureState(outFishApplyId, status, message);
    JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishApplyEntity>() {
    });
  }

  @Override
  public String reApply(String id) {
    if(StringUtils.isBlank(id)){
      return null;
    }
    ResponseModel responseModel = outFishApplyEntityFeign.reApply(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<String>() {
    });
  }

  @Override
  public Page<OutApplyCustomStateEntity> findDetailByOrgBaseNameOrPondNameOrTiem(Map<String, Object> params, Pageable pageable) {
    Page<OutApplyCustomStateEntity> outFishApplyEntity = outApplyCustomStateEntityRepository.findDetailByOrgBaseNameOrPondNameOrTiem(params, pageable);
    return outFishApplyEntity;
  }
} 
