package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.OutFishInfoEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.process.OutFishInfoEntityFeign;
import com.tw.aquaculture.manager.service.process.OutFishInfoEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;

/**
 * OutFishInfoEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("OutFishInfoEntityServiceImpl")
public class OutFishInfoEntityServiceImpl implements OutFishInfoEntityService {

  @Autowired
  private OutFishInfoEntityFeign outFishInfoEntityFeign;

  @Transactional
  @Override
  public OutFishInfoEntity create(OutFishInfoEntity outFishInfoEntity) {
    ResponseModel responseModel = outFishInfoEntityFeign.create(outFishInfoEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishInfoEntity>() {
    });
  }

  @Transactional
  @Override
  public OutFishInfoEntity createForm(OutFishInfoEntity outFishInfoEntity) {
    return null;
  }

  @Transactional
  @Override
  public OutFishInfoEntity update(OutFishInfoEntity outFishInfoEntity) {
    ResponseModel responseModel = outFishInfoEntityFeign.update(outFishInfoEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishInfoEntity>() {
    });
  }

  @Transactional
  @Override
  public OutFishInfoEntity updateForm(OutFishInfoEntity outFishInfoEntity) {
    return null;
  }

  @Override
  public Set<OutFishInfoEntity> findDetailsByFishType(String fishType) {
    ResponseModel responseModel = outFishInfoEntityFeign.findDetailsByFishType(fishType);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Set<OutFishInfoEntity>>() {
    });
  }

  @Override
  public Set<OutFishInfoEntity> findDetailsByOutFishRegist(String outFishRegist) {
    ResponseModel responseModel = outFishInfoEntityFeign.findDetailsByOutFishRegist(outFishRegist);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Set<OutFishInfoEntity>>() {
    });
  }

  @Override
  public OutFishInfoEntity findDetailsById(String id) {
    ResponseModel responseModel = outFishInfoEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishInfoEntity>() {
    });
  }

  @Override
  public OutFishInfoEntity findById(String id) {
    ResponseModel responseModel = outFishInfoEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishInfoEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    outFishInfoEntityFeign.deleteById(id);
  }
} 
