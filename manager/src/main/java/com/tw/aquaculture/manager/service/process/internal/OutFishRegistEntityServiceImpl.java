package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.OutFishRegistEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.process.OutFishRegistEntityFeign;
import com.tw.aquaculture.manager.service.process.OutFishRegistEntityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

/**
 * OutFishRegistEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("OutFishRegistEntityServiceImpl")
public class OutFishRegistEntityServiceImpl implements OutFishRegistEntityService {

  @Autowired
  private OutFishRegistEntityFeign outFishRegistEntityFeign;

  @Transactional
  @Override
  public OutFishRegistEntity create(OutFishRegistEntity outFishRegistEntity) {
    ResponseModel responseModel = outFishRegistEntityFeign.create(outFishRegistEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishRegistEntity>() {
    });
  }

  @Transactional
  @Override
  public OutFishRegistEntity createForm(OutFishRegistEntity outFishRegistEntity) {
    return null;
  }

  @Transactional
  @Override
  public OutFishRegistEntity update(OutFishRegistEntity outFishRegistEntity) {
    ResponseModel responseModel = outFishRegistEntityFeign.update(outFishRegistEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishRegistEntity>() {
    });
  }

  @Transactional
  @Override
  public OutFishRegistEntity updateForm(OutFishRegistEntity outFishRegistEntity) {
    return null;
  }

  @Override
  public OutFishRegistEntity findDetailsById(String id) {
    ResponseModel responseModel = outFishRegistEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishRegistEntity>() {
    });
  }

  @Override
  public OutFishRegistEntity findById(String id) {
    ResponseModel responseModel = outFishRegistEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishRegistEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    outFishRegistEntityFeign.deleteById(id);
  }

  @Override
  public OutFishRegistEntity findDetailsByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = outFishRegistEntityFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishRegistEntity>() {
    });
  }

  @Override
  public OutFishRegistEntity findByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = outFishRegistEntityFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<OutFishRegistEntity>() {
    });
  }

  @Override
  public List<OutFishRegistEntity> findByPondAndTimes(String pondId, Date startTime, Date endTime) {
    ResponseModel responseModel = outFishRegistEntityFeign.findByPondAndTimes(pondId, startTime, endTime);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<OutFishRegistEntity>>() {
    });
  }

  @Override
  public List<OutFishRegistEntity> findOutFishRegistByOutFishApply(String outFishApplyId) {
    ResponseModel responseModel = outFishRegistEntityFeign.findOutFishRegistByOutFishApply(outFishApplyId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<OutFishRegistEntity>>() {
    });
  }
} 
