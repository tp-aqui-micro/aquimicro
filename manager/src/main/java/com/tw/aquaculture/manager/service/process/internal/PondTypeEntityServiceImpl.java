package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.base.PondTypeEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.base.PondTypeFeign;
import com.tw.aquaculture.manager.service.process.PondTypeEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * PondTypeEntity业务模型的服务层接口实现
 * @author saturn
 */
@Service("PondTypeEntityServiceImpl")
public class PondTypeEntityServiceImpl implements PondTypeEntityService {
  @Autowired
  private PondTypeFeign pondTypeFeign;

  @Override
  public PondTypeEntity create(PondTypeEntity pondTypeEntity) {
    PondTypeEntity current = this.createForm(pondTypeEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  } 
  @Override
  public PondTypeEntity createForm(PondTypeEntity pondTypeEntity) {
    ResponseModel responseModel = pondTypeFeign.create(pondTypeEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PondTypeEntity>() {
    });
  }

  @Override
  public PondTypeEntity update(PondTypeEntity pondTypeEntity) { 
    PondTypeEntity current = this.updateForm(pondTypeEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  } 
  @Override
  public PondTypeEntity updateForm(PondTypeEntity pondTypeEntity) {
    ResponseModel responseModel = pondTypeFeign.update(pondTypeEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PondTypeEntity>() {
    });
  }

  @Override
  public PondTypeEntity findDetailsById(String id) {
    ResponseModel responseModel = pondTypeFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PondTypeEntity>() {
    });
  }
  @Override
  public PondTypeEntity findById(String id) { 
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    ResponseModel responseModel = pondTypeFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PondTypeEntity>() {
    });
  }
  @Override
  public void deleteById(String id) {
    throw new RuntimeException("暂未开放此接口");
  }
  @Override
  public PondTypeEntity findDetailsByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = pondTypeFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PondTypeEntity>() {
    });
  }
  @Override
  public PondTypeEntity findByFormInstanceId(String formInstanceId) {
    if(StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = pondTypeFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PondTypeEntity>() {
    });
  }
} 
