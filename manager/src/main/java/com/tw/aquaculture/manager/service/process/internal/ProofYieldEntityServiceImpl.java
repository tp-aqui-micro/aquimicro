package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.*;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.common.vo.reportslist.ExistAmountReportVo;
import com.tw.aquaculture.common.vo.reportslist.ReportParamVo;
import com.tw.aquaculture.manager.feign.process.ProofYieldEntityFeign;
import com.tw.aquaculture.manager.service.process.ProofYieldEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

/**
 * ProofYieldEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("ProofYieldEntityServiceImpl")
public class ProofYieldEntityServiceImpl implements ProofYieldEntityService {
  @Autowired
  private ProofYieldEntityFeign proofYieldEntityFeign;

  @Transactional
  @Override
  public ProofYieldEntity create(ProofYieldEntity proofYieldEntity) {
    ResponseModel responseModel = proofYieldEntityFeign.create(proofYieldEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ProofYieldEntity>() {
    });
  }

  @Transactional
  @Override
  public ProofYieldEntity createForm(ProofYieldEntity proofYieldEntity) {
    return null;
  }

  @Transactional
  @Override
  public ProofYieldEntity update(ProofYieldEntity proofYieldEntity) {
    ResponseModel responseModel = proofYieldEntityFeign.update(proofYieldEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ProofYieldEntity>() {
    });
  }

  @Transactional
  @Override
  public ProofYieldEntity updateForm(ProofYieldEntity proofYieldEntity) {
    return null;
  }

  @Override
  public ProofYieldEntity findDetailsById(String id) {
    ResponseModel responseModel = proofYieldEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ProofYieldEntity>() {
    });
  }

  @Override
  public ProofYieldEntity findById(String id) {
    ResponseModel responseModel = proofYieldEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ProofYieldEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    this.proofYieldEntityFeign.deleteById(id);
  }

  @Override
  public ProofYieldEntity findDetailsByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = proofYieldEntityFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ProofYieldEntity>() {
    });
  }

  @Override
  public ProofYieldEntity findByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = proofYieldEntityFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ProofYieldEntity>() {
    });
  }

  @Override
  public ProofYieldEntity findLastProofYield(String pondId) {
    ResponseModel responseModel = proofYieldEntityFeign.findLastProofYield(pondId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ProofYieldEntity>() {
    });
  }

  @Override
  public double computeLastExitCount(String pondId) {
    ResponseModel responseModel = proofYieldEntityFeign.computeLastExitCount(pondId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Double>() {
    });
  }

  @Override
  public ProofYieldEntity findLastProofYieldByFishType(String pondId, String fishTypeId) {
    ResponseModel responseModel = proofYieldEntityFeign.findLastProofYieldByFishType(pondId, fishTypeId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ProofYieldEntity>() {
    });
  }

  @Override
  public List<ProofYieldEntity> findDetailsByPondAndWorkOrder(String pondId, String workOrderId) {
    ResponseModel responseModel = proofYieldEntityFeign.findDetailsByPondAndWorkOrder(pondId, workOrderId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<ProofYieldEntity>>() {
    });
  }

  @Override
  public double findExitCountByPondAndFishType(String pondId, String fishTypeId) {
    if(StringUtils.isBlank(pondId) || StringUtils.isBlank(fishTypeId)){
      return 0;
    }
    ResponseModel responseModel = proofYieldEntityFeign.findExitCountByPondAndFishType(pondId, fishTypeId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Double>() {
    });
  }

  @Override
  public List<ExistAmountReportVo> existAmountReport(ReportParamVo reportParamVo) {
    ResponseModel responseModel = proofYieldEntityFeign.existAmountReport(reportParamVo);
    return JsonUtil.parseResponseModelWithOutException(responseModel, new TypeReference<List<ExistAmountReportVo>>() {
    });
  }
} 
