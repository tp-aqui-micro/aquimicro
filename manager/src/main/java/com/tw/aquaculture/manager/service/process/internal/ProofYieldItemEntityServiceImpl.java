package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.ProofYieldItemEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.process.ProofYieldItemEntityFeign;
import com.tw.aquaculture.manager.service.process.ProofYieldItemEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;

/**
 * ProofYieldItemEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("ProofYieldItemEntityServiceImpl")
public class ProofYieldItemEntityServiceImpl implements ProofYieldItemEntityService {

  @Autowired
  private ProofYieldItemEntityFeign proofYieldItemEntityFeign;

  @Transactional
  @Override
  public ProofYieldItemEntity create(ProofYieldItemEntity proofYieldItemEntity) {
    ResponseModel responseModel = proofYieldItemEntityFeign.create(proofYieldItemEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ProofYieldItemEntity>() {
    });
  }

  @Transactional
  @Override
  public ProofYieldItemEntity createForm(ProofYieldItemEntity proofYieldItemEntity) {
    return null;
  }

  @Transactional
  @Override
  public ProofYieldItemEntity update(ProofYieldItemEntity proofYieldItemEntity) {
    ResponseModel responseModel = proofYieldItemEntityFeign.update(proofYieldItemEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ProofYieldItemEntity>() {
    });
  }

  @Transactional
  @Override
  public ProofYieldItemEntity updateForm(ProofYieldItemEntity proofYieldItemEntity) {
    return null;
  }

  @Override
  public Set<ProofYieldItemEntity> findDetailsByProofYield(String proofYield) {
    ResponseModel responseModel = proofYieldItemEntityFeign.findDetailsByProofYield(proofYield);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Set<ProofYieldItemEntity>>() {
    });
  }

  @Override
  public ProofYieldItemEntity findDetailsById(String id) {
    ResponseModel responseModel = proofYieldItemEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ProofYieldItemEntity>() {
    });
  }

  @Override
  public ProofYieldItemEntity findById(String id) {
    ResponseModel responseModel = proofYieldItemEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<ProofYieldItemEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    proofYieldItemEntityFeign.deleteById(id);
  }
} 
