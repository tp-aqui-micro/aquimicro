package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.PushFeedEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.common.vo.reportslist.PushFeedReportVo;
import com.tw.aquaculture.common.vo.reportslist.ReportParamVo;
import com.tw.aquaculture.manager.feign.process.PushFeedEntityFeign;
import com.tw.aquaculture.manager.service.process.PushFeedEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * PushFeedEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("PushFeedEntityServiceImpl")
public class PushFeedEntityServiceImpl implements PushFeedEntityService {

  @Autowired
  private PushFeedEntityFeign pushFeedEntityFeign;

  @Transactional
  @Override
  public PushFeedEntity create(PushFeedEntity pushFeedEntity) {
    ResponseModel responseModel = pushFeedEntityFeign.create(pushFeedEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PushFeedEntity>() {
    });
  }

  @Transactional
  @Override
  public PushFeedEntity createForm(PushFeedEntity pushFeedEntity) {
    return null;
  }

  @Transactional
  @Override
  public PushFeedEntity update(PushFeedEntity pushFeedEntity) {
    ResponseModel responseModel = pushFeedEntityFeign.update(pushFeedEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PushFeedEntity>() {
    });
  }

  @Transactional
  @Override
  public PushFeedEntity updateForm(PushFeedEntity pushFeedEntity) {
    return null;
  }

  @Override
  public PushFeedEntity findDetailsById(String id) {
    ResponseModel responseModel = pushFeedEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PushFeedEntity>() {
    });
  }

  @Override
  public PushFeedEntity findById(String id) {
    ResponseModel responseModel = pushFeedEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PushFeedEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    pushFeedEntityFeign.deleteById(id);
  }

  @Override
  public PushFeedEntity findDetailsByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = pushFeedEntityFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PushFeedEntity>() {
    });
  }

  @Override
  public PushFeedEntity findByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = pushFeedEntityFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PushFeedEntity>() {
    });
  }

  @Override
  public Double sumConsumeByTimes(String baseId, String matterId, Date start, Date end) {
    if(StringUtils.isBlank(baseId) || StringUtils.isBlank(matterId) || start==null || end==null){
      return null;
    }
    ResponseModel responseModel = pushFeedEntityFeign.sumConsumeByTimes(baseId, matterId, start, end);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Double>() {
    });
  }

  @Override
  public List<PushFeedReportVo> pushFeedReport(ReportParamVo reportParamVo) {

    ResponseModel responseModel = pushFeedEntityFeign.pushFeedReport(reportParamVo);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<PushFeedReportVo>>() {
    });
  }
} 
