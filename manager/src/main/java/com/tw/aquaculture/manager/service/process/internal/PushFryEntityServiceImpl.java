package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.bizunited.platform.core.service.invoke.InvokeProxyException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.PushFryEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.process.PushFryEntityFeign;
import com.tw.aquaculture.manager.service.process.PushFryEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

/**
 * PushFryEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("PushFryEntityServiceImpl")
public class PushFryEntityServiceImpl implements PushFryEntityService {

  @Autowired
  private PushFryEntityFeign pushFryEntityFeign;


  @Transactional
  @Override
  public PushFryEntity create(PushFryEntity pushFryEntity) throws InvokeProxyException {
    ResponseModel responseModel = pushFryEntityFeign.create(pushFryEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PushFryEntity>() {
    });
  }

  @Transactional
  @Override
  public PushFryEntity createForm(PushFryEntity pushFryEntity) throws InvokeProxyException {
    return null;
  }


  @Transactional
  @Override
  public PushFryEntity update(PushFryEntity pushFryEntity) {
    ResponseModel responseModel = pushFryEntityFeign.update(pushFryEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PushFryEntity>() {
    });
  }

  @Transactional
  @Override
  public PushFryEntity updateForm(PushFryEntity pushFryEntity) {
    return null;
  }

  @Override
  public PushFryEntity findDetailsById(String id) {
    ResponseModel responseModel = pushFryEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PushFryEntity>() {
    });
  }

  @Override
  public PushFryEntity findById(String id) {
    ResponseModel responseModel = pushFryEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PushFryEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    pushFryEntityFeign.deleteById(id);
  }

  @Override
  public PushFryEntity findDetailsByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = pushFryEntityFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PushFryEntity>() {
    });
  }

  @Override
  public PushFryEntity findByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = pushFryEntityFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PushFryEntity>() {
    });
  }

  @Override
  public List<PushFryEntity> findByPondAndTimes(String pondId, Date startTime, Date endTime) {
    ResponseModel responseModel = pushFryEntityFeign.findByPondAndTimes(pondId, startTime, endTime);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<PushFryEntity>>() {
    });
  }
} 
