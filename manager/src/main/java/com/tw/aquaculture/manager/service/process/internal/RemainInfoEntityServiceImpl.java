package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.RemainInfoEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.process.RemainInfoEntityFeign;
import com.tw.aquaculture.manager.service.process.RemainInfoEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;

/**
 * RemainInfoEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("RemainInfoEntityServiceImpl")
public class RemainInfoEntityServiceImpl implements RemainInfoEntityService {

  @Autowired
  private RemainInfoEntityFeign remainInfoEntityFeign;

  @Transactional
  @Override
  public RemainInfoEntity create(RemainInfoEntity remainInfoEntity) {
    ResponseModel responseModel = remainInfoEntityFeign.create(remainInfoEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<RemainInfoEntity>() {
    });
  }

  @Transactional
  @Override
  public RemainInfoEntity createForm(RemainInfoEntity remainInfoEntity) {
    return null;
  }

  @Transactional
  @Override
  public RemainInfoEntity update(RemainInfoEntity remainInfoEntity) {
    ResponseModel responseModel = remainInfoEntityFeign.update(remainInfoEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<RemainInfoEntity>() {
    });
  }

  @Transactional
  @Override
  public RemainInfoEntity updateForm(RemainInfoEntity remainInfoEntity) {
    return null;
  }

  @Override
  public Set<RemainInfoEntity> findDetailsByFishType(String fishType) {
    ResponseModel responseModel = remainInfoEntityFeign.findDetailsByFishType(fishType);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Set<RemainInfoEntity>>() {
    });
  }

  @Override
  public Set<RemainInfoEntity> findDetailsByOutFishApply(String outFishApply) {
    ResponseModel responseModel = remainInfoEntityFeign.findDetailsByOutFishApply(outFishApply);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Set<RemainInfoEntity>>() {
    });
  }

  @Override
  public RemainInfoEntity findDetailsById(String id) {
    ResponseModel responseModel = remainInfoEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<RemainInfoEntity>() {
    });
  }

  @Override
  public RemainInfoEntity findById(String id) {
    ResponseModel responseModel = remainInfoEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<RemainInfoEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    remainInfoEntityFeign.deleteById(id);
  }
} 
