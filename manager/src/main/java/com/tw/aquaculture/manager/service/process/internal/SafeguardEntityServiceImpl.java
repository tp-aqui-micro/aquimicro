package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.SafeguardEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.common.vo.reportslist.ReportParamVo;
import com.tw.aquaculture.common.vo.reportslist.SafeReportVo;
import com.tw.aquaculture.manager.feign.process.SafeguardEntityFeign;
import com.tw.aquaculture.manager.service.process.SafeguardEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * SafeguardEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("SafeguardEntityServiceImpl")
public class SafeguardEntityServiceImpl implements SafeguardEntityService {

  @Autowired
  private SafeguardEntityFeign safeguardEntityFeign;

  @Transactional
  @Override
  public SafeguardEntity create(SafeguardEntity safeguardEntity) {
    ResponseModel responseModel = safeguardEntityFeign.create(safeguardEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<SafeguardEntity>() {
    });
  }

  @Transactional
  @Override
  public SafeguardEntity createForm(SafeguardEntity safeguardEntity) {
    return null;
  }

  @Transactional
  @Override
  public SafeguardEntity update(SafeguardEntity safeguardEntity) {
    ResponseModel responseModel = safeguardEntityFeign.update(safeguardEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<SafeguardEntity>() {
    });
  }

  @Transactional
  @Override
  public SafeguardEntity updateForm(SafeguardEntity safeguardEntity) {
    return null;
  }

  @Override
  public SafeguardEntity findDetailsById(String id) {
    ResponseModel responseModel = safeguardEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<SafeguardEntity>() {
    });
  }

  @Override
  public SafeguardEntity findById(String id) {
    ResponseModel responseModel = safeguardEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<SafeguardEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    safeguardEntityFeign.deleteById(id);
  }

  @Override
  public SafeguardEntity findDetailsByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = safeguardEntityFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<SafeguardEntity>() {
    });
  }

  @Override
  public SafeguardEntity findByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = safeguardEntityFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<SafeguardEntity>() {
    });
  }

  @Override
  public List<SafeReportVo> safeReport(ReportParamVo reportParamVo) {
    ResponseModel responseModel = safeguardEntityFeign.safeReport(reportParamVo);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<SafeReportVo>>() {
    });
  }
} 
