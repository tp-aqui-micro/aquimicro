package com.tw.aquaculture.manager.service.process.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.process.WorkOrderEntityFeign;
import com.tw.aquaculture.manager.service.process.WorkOrderEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * WorkOrderEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("WorkOrderEntityServiceImpl")
public class WorkOrderEntityServiceImpl implements WorkOrderEntityService {
  @Autowired
  private WorkOrderEntityFeign workOrderEntityFeign;

  @Transactional
  @Override
  public WorkOrderEntity create(WorkOrderEntity workOrderEntity) {
    ResponseModel responseModel = workOrderEntityFeign.create(workOrderEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<WorkOrderEntity>() {
    });
  }

  @Transactional
  @Override
  public WorkOrderEntity createForm(WorkOrderEntity workOrderEntity) {
    return null;
  }

  @Transactional
  @Override
  public WorkOrderEntity update(WorkOrderEntity workOrderEntity) {
    ResponseModel responseModel = workOrderEntityFeign.update(workOrderEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<WorkOrderEntity>() {
    });
  }

  @Transactional
  @Override
  public WorkOrderEntity updateForm(WorkOrderEntity workOrderEntity) {
    return null;
  }

  @Override
  public WorkOrderEntity findDetailsById(String id) {
    ResponseModel responseModel = workOrderEntityFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<WorkOrderEntity>() {
    });
  }

  @Override
  public WorkOrderEntity findById(String id) {
    ResponseModel responseModel = workOrderEntityFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<WorkOrderEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    workOrderEntityFeign.deleteById(id);
  }

  @Override
  public WorkOrderEntity findDetailsByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = workOrderEntityFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<WorkOrderEntity>() {
    });
  }

  @Override
  public WorkOrderEntity findByFormInstanceId(String formInstanceId) {
    ResponseModel responseModel = workOrderEntityFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<WorkOrderEntity>() {
    });
  }

  @Override
  public Page<WorkOrderEntity> findByConditions(Pageable pageable, Map<String, Object> params) {
    ResponseModel responseModel = workOrderEntityFeign.findByConditions(pageable, params);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Page<WorkOrderEntity>>() {
    });
  }

  @Override
  public List<WorkOrderEntity> findByPondAndTime(String pondId, Date createTime) {
    ResponseModel responseModel = workOrderEntityFeign.findByPondAndTime(pondId, createTime);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<WorkOrderEntity>>() {
    });
  }

  @Override
  public void save(WorkOrderEntity w) {
    workOrderEntityFeign.save(w);
  }

  @Override
  public List<WorkOrderEntity> findByStateAndPond(String pondId, int state) {
    ResponseModel responseModel = workOrderEntityFeign.findByStateAndPond(pondId, state);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<WorkOrderEntity>>() {
    });
  }
} 
