package com.tw.aquaculture.manager.service.remote;

import com.bizunited.platform.core.annotations.NebulaService;
import com.bizunited.platform.core.annotations.NebulaServiceMethod;
import com.bizunited.platform.core.annotations.NebulaServiceMethod.ScopeType;
import com.bizunited.platform.core.annotations.ServiceMethodParam;
import com.tw.aquaculture.common.entity.base.ShareModelItemEntity;

import java.util.Set;

/**
 * ShareModelItemEntity业务模型的服务层接口定义
 *
 * @author saturn
 */
@NebulaService
public interface ShareModelItemEntityService {
  /**
   * 创建一个新的ShareModelItemEntity模型对象（包括了可能的第三方系统调用、复杂逻辑处理等）
   */
  @NebulaServiceMethod(name = "ShareModelItemEntityService.create", desc = "创建一个新的ShareModelItemEntity模型对象（包括了可能的第三方系统调用、复杂逻辑处理等）", returnPropertiesFilter = "", scope = ScopeType.WRITE)
  public ShareModelItemEntity create(ShareModelItemEntity shareModelItemEntity);

  /**
   * 创建一个新的StudentEntity模型对象
   * 该代码由satrun骨架生成，默认不包括任何可能第三方系统调用、任何复杂逻辑处理等，主要应用场景为前端表单数据的暂存功能</br>
   * 该方法与本接口中的updateFrom方法呼应
   */
  @NebulaServiceMethod(name = "ShareModelItemEntityService.createForm", desc = "创建一个新的StudentEntity模型对象（默认不包括任何可能第三方系统调用、任何复杂逻辑处理等）", returnPropertiesFilter = "", scope = ScopeType.WRITE)
  public ShareModelItemEntity createForm(ShareModelItemEntity shareModelItemEntity);

  /**
   * 更新一个已有的ShareModelItemEntity模型对象，其主键属性必须有值(1.1.4-release版本调整)。
   * 这个方法实际上一共分为三个步骤（默认）：</br>
   * 1、调用updateValidation方法完成表单数据更新前的验证</br>
   * 2、调用updateForm方法完成表单数据的更新</br>
   * 3、完成开发人员自行在本update方法中书写的，进行第三方系统调用（或特殊处理过程）的执行。</br>
   * 这样做的目的，实际上是为了保证updateForm方法中纯粹是处理表单数据的，在数据恢复表单引擎默认调用updateForm方法时，不会影响任何第三方业务数据
   * （当然，如果系统有特别要求，可由开发人员自行完成代码逻辑调整）
   */
  @NebulaServiceMethod(name = "ShareModelItemEntityService.update", desc = "更新一个已有的ShareModelItemEntity模型对象，其主键属性必须有值(1.1.4-release版本调整)。", returnPropertiesFilter = "", scope = ScopeType.WRITE)
  public ShareModelItemEntity update(ShareModelItemEntity shareModelItemEntity);

  /**
   * 该方法只用于处理业务表单信息，包括了主业务模型、其下的关联模型、分组信息和明细细信息等
   * 该方法非常重要，因为如果进行静态表单的数据恢复，那么表单引擎将默认调用主业务模型（服务层）的这个方法。</br>
   * 这样一来保证了数据恢复时，不会涉及任何第三方系统的调用（当然，如果开发人员需要涉及的，可以自行进行修改）
   */
  @NebulaServiceMethod(name = "ShareModelItemEntityService.updateForm", desc = "该方法只用于处理业务表单信息，包括了主业务模型、其下的关联模型、分组信息和明细细信息等", returnPropertiesFilter = "", scope = ScopeType.WRITE)
  public ShareModelItemEntity updateForm(ShareModelItemEntity shareModelItemEntity);

  /**
   * 按照关联的 鱼种进行详情查询（包括关联信息）
   *
   * @param fishType 关联的 鱼种
   */
  @NebulaServiceMethod(name = "ShareModelItemEntityService.findDetailsByFishType", desc = "按照关联的 鱼种进行详情查询（包括关联信息）", returnPropertiesFilter = "shareModel,fishType", scope = ScopeType.READ)
  public Set<ShareModelItemEntity> findDetailsByFishType(@ServiceMethodParam(name = "fishType") String fishType);

  /**
   * 按照关联的 分摊模式进行详情查询（包括关联信息）
   *
   * @param shareModel 关联的 分摊模式
   */
  @NebulaServiceMethod(name = "ShareModelItemEntityService.findDetailsByShareModel", desc = "按照关联的 分摊模式进行详情查询（包括关联信息）", returnPropertiesFilter = "shareModel,fishType", scope = ScopeType.READ)
  public Set<ShareModelItemEntity> findDetailsByShareModel(@ServiceMethodParam(name = "shareModel") String shareModel);

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @NebulaServiceMethod(name = "ShareModelItemEntityService.findDetailsById", desc = "按照主键进行详情查询（包括关联信息）", returnPropertiesFilter = "shareModel,fishType", scope = ScopeType.READ)
  public ShareModelItemEntity findDetailsById(@ServiceMethodParam(name = "id") String id);

  /**
   * 按照ShareModelItemEntity的主键编号，查询指定的数据信息（不包括任何关联信息）
   *
   * @param id 主键
   */
  public ShareModelItemEntity findById(String id);

  /**
   * 按照主键进行信息的真删除
   *
   * @param id 主键
   */
  public void deleteById(String id);
}