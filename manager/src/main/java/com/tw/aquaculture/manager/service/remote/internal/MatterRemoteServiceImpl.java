package com.tw.aquaculture.manager.service.remote.internal;

import com.tw.aquaculture.manager.feign.base.MatterFeign;
import com.tw.aquaculture.manager.service.remote.MatterRemoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 物料相关定时任务
 *
 * @author 陈荣
 * @date 2019/11/20 14:13
 */
@Service("MatterRemoteService")
public class MatterRemoteServiceImpl implements MatterRemoteService {

  @Autowired
  private MatterFeign matterFeign;

  @Override
  public void pullAMatters() {

    matterFeign.pullMatters();
  }
}
