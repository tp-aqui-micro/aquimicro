package com.tw.aquaculture.manager.service.remote.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.base.StoreEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.base.StoreFeign;
import com.tw.aquaculture.manager.service.remote.StoreEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * StoreEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("StoreEntityServiceImpl")
public class StoreEntityServiceImpl implements StoreEntityService {
  @Autowired
  private StoreFeign storeFeign;

  @Transactional
  @Override
  public StoreEntity create(StoreEntity storeEntity) {
    ResponseModel responseModel = storeFeign.create(storeEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StoreEntity>() {
    });
  }

  @Transactional
  @Override
  public StoreEntity createForm(StoreEntity storeEntity) {
    return null;
  }

  @Transactional
  @Override
  public StoreEntity update(StoreEntity storeEntity) {
    ResponseModel responseModel = storeFeign.update(storeEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StoreEntity>() {
    });
  }

  @Transactional
  @Override
  public StoreEntity updateForm(StoreEntity storeEntity) {
    return null;
  }

  @Override
  public StoreEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = storeFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StoreEntity>() {
    });
  }

  @Override
  public StoreEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = storeFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StoreEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    if (StringUtils.isBlank(id)) {
      return;
    }
    storeFeign.deleteById(id);
  }

  @Override
  public StoreEntity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = storeFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StoreEntity>() {
    });
  }

  @Override
  public StoreEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = storeFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StoreEntity>() {
    });
  }

  @Override
  public List<StoreEntity> findStoresByBaseId(String baseId) {
    if (StringUtils.isBlank(baseId)) {
      return null;
    }
    ResponseModel responseModel = storeFeign.findStoresByBaseId(baseId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<StoreEntity>>() {
    });
  }
} 
