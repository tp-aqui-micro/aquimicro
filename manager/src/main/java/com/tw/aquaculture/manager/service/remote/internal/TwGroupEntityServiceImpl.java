package com.tw.aquaculture.manager.service.remote.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.base.TwGroupEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.base.TwGroupFeign;
import com.tw.aquaculture.manager.service.remote.TwGroupEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * TwGroupEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("TwGroupEntityServiceImpl")
public class TwGroupEntityServiceImpl implements TwGroupEntityService {
  @Autowired
  private TwGroupFeign twGroupFeign;

  @Transactional
  @Override
  public TwGroupEntity create(TwGroupEntity twGroupEntity) {
    ResponseModel responseModel = twGroupFeign.create(twGroupEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TwGroupEntity>() {
    });
  }

  @Transactional
  @Override
  public TwGroupEntity createForm(TwGroupEntity twGroupEntity) {
    return null;
  }

  @Transactional
  @Override
  public TwGroupEntity update(TwGroupEntity twGroupEntity) {
    ResponseModel responseModel = twGroupFeign.update(twGroupEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TwGroupEntity>() {
    });
  }

  @Transactional
  @Override
  public TwGroupEntity updateForm(TwGroupEntity twGroupEntity) {
    return null;
  }

  @Override
  public TwGroupEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = twGroupFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TwGroupEntity>() {
    });
  }

  @Override
  public TwGroupEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = twGroupFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TwGroupEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    if (StringUtils.isBlank(id)) {
      return;
    }
    twGroupFeign.deleteById(id);
  }

  @Override
  public TwGroupEntity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = twGroupFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TwGroupEntity>() {
    });
  }

  @Override
  public TwGroupEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = twGroupFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TwGroupEntity>() {
    });
  }

  @Override
  public List<TwGroupEntity> findAll() {
    ResponseModel responseModel = twGroupFeign.findAll();
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<TwGroupEntity>>() {});
  }
} 
