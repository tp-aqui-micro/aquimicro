package com.tw.aquaculture.manager.service.servicesource;

import com.bizunited.platform.core.annotations.NebulaService;
import com.bizunited.platform.core.annotations.NebulaServiceMethod;
import com.bizunited.platform.core.annotations.ServiceMethodParam;
import com.bizunited.platform.rbac.server.vo.OrganizationVo;
import com.bizunited.platform.rbac.server.vo.UserVo;

/**
 * 基础信息服务源
 * @author 陈荣
 * @date 2019/11/16 17:39
 */
@NebulaService
public interface BaseSourceService {

  /**
   * 按照组织id查询组织详情
   * @param orgId
   * @return
   */
  @NebulaServiceMethod(name="BaseSourceService.findOrgDetailById" , desc="按照组织id查询组织详情" , returnPropertiesFilter="parent,parent.parent", scope= NebulaServiceMethod.ScopeType.READ)
  public OrganizationVo findOrgDetailById(@ServiceMethodParam(name = "orgId") String orgId);

  /**
   * 按照用户id查询用户详情
   * @param userId
   * @return
   */
  @NebulaServiceMethod(name="BaseSourceService.findUserDetailById" , desc="按照用户id查询用户详情" , returnPropertiesFilter="orgs,roles,positions,groups", scope= NebulaServiceMethod.ScopeType.READ)
  public UserVo findUserDetailById(@ServiceMethodParam(name = "userId") String userId);


}
