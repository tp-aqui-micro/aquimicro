package com.tw.aquaculture.manager.service.servicesource.internal;

import com.bizunited.platform.rbac.server.service.OrganizationService;
import com.bizunited.platform.rbac.server.service.UserService;
import com.bizunited.platform.rbac.server.vo.OrganizationVo;
import com.bizunited.platform.rbac.server.vo.UserVo;
import com.tw.aquaculture.manager.service.servicesource.BaseSourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 基础数据服务源实现类
 * @author 陈荣
 * @date 2019/11/16 17:40
 */
@Service("BaseSourceServiceImpl")
public class BaseSourceServiceImpl implements BaseSourceService {

  @Autowired
  private OrganizationService organizationService;

  @Autowired
  private UserService userService;

  @Override
  public OrganizationVo findOrgDetailById(String orgId) {
    return organizationService.findDetailsById(orgId);
  }

  @Override
  public UserVo findUserDetailById(String userId) {
    return userService.findDetailsById(userId);
  }
}
