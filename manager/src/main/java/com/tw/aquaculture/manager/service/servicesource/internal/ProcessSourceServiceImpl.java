package com.tw.aquaculture.manager.service.servicesource.internal;

import com.tw.aquaculture.manager.service.servicesource.ProcessSourceService;
import org.springframework.stereotype.Service;

/**
 * 塘口信息服务源实现类
 * @author 陈荣
 * @date 2019/11/16 17:40
 */
@Service("PondSourceServiceImpl")
public class ProcessSourceServiceImpl implements ProcessSourceService {

}
