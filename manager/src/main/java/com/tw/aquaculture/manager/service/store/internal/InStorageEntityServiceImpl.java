package com.tw.aquaculture.manager.service.store.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.store.InStorageEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.store.InstorageFeign;
import com.tw.aquaculture.manager.service.store.InStorageEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * InStorageEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("InStorageEntityServiceImpl")
public class InStorageEntityServiceImpl implements InStorageEntityService {
  @Autowired
  private InstorageFeign instorageFeign;

  @Transactional
  @Override
  public InStorageEntity create(InStorageEntity inStorageEntity) {
    return createForm(inStorageEntity);
  }

  @Transactional
  @Override
  public InStorageEntity createForm(InStorageEntity inStorageEntity) {
    ResponseModel responseModel = instorageFeign.create(inStorageEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<InStorageEntity>() {
    });
  }

  @Transactional
  @Override
  public InStorageEntity update(InStorageEntity inStorageEntity) {
    return updateForm(inStorageEntity);
  }

  @Transactional
  @Override
  public InStorageEntity updateForm(InStorageEntity inStorageEntity) {
    ResponseModel responseModel = instorageFeign.update(inStorageEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<InStorageEntity>() {
    });
  }

  @Override
  public InStorageEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = instorageFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<InStorageEntity>() {
    });
  }

  @Override
  public InStorageEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    throw new RuntimeException("暂未开放此接口");
  }

  @Override
  @Transactional
  public void deleteById(String id) {

    throw new RuntimeException("暂未开放此接口");
  }

  @Override
  public InStorageEntity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = instorageFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<InStorageEntity>() {
    });
  }

  @Override
  public InStorageEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = instorageFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<InStorageEntity>() {
    });
  }
} 
