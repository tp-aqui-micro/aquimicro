package com.tw.aquaculture.manager.service.store.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.store.StockEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.store.StockFeign;
import com.tw.aquaculture.manager.service.store.StockEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

/**
 * StockEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("StockEntityServiceImpl")
public class StockEntityServiceImpl implements StockEntityService {
  @Autowired
  private StockFeign stockFeign;

  @Transactional
  @Override
  public StockEntity create(StockEntity stockEntity) {
    return createForm(stockEntity);
  }

  @Transactional
  @Override
  public StockEntity createForm(StockEntity stockEntity) {
    ResponseModel responseModel = stockFeign.create(stockEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StockEntity>() {
    });
  }

  @Transactional
  @Override
  public StockEntity update(StockEntity stockEntity) {
    return updateForm(stockEntity);
  }

  @Transactional
  @Override
  public StockEntity updateForm(StockEntity stockEntity) {
    ResponseModel responseModel = stockFeign.update(stockEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StockEntity>() {
    });
  }

  @Override
  public Set<StockEntity> findDetailsByBase(String base) {
    if (StringUtils.isBlank(base)) {
      return null;
    }
    ResponseModel responseModel = stockFeign.findDetailsByBase(base);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Set<StockEntity>>() {
    });
  }

  @Override
  public Set<StockEntity> findDetailsByStore(String store) {
    if (StringUtils.isBlank(store)) {
      return null;
    }
    ResponseModel responseModel = stockFeign.findDetailsByStore(store);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Set<StockEntity>>() {
    });
  }

  @Override
  public Set<StockEntity> findDetailsByMatter(String matter) {
    if (StringUtils.isBlank(matter)) {
      return null;
    }
    ResponseModel responseModel = stockFeign.findDetailsByStore(matter);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Set<StockEntity>>() {
    });
  }

  @Override
  public StockEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = stockFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StockEntity>() {
    });
  }

  @Override
  public StockEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    return null;
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    return;
  }

  @Override
  public StockEntity findByStoreAndMatterAndSpecs(String storeId, String matterId, String specs) {
    if (StringUtils.isBlank(specs) || StringUtils.isBlank(storeId) || StringUtils.isBlank(matterId)) {
      return null;
    }
    ResponseModel responseModel = stockFeign.findByMatterAndStore(storeId, matterId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StockEntity>() {
    });
  }

  @Override
  public List<StockEntity> findByBaseId(String baseId) {
    if (StringUtils.isBlank(baseId)) {
      return null;
    }
    ResponseModel responseModel = stockFeign.findByBaseId(baseId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<StockEntity>>() {
    });
  }

} 
