package com.tw.aquaculture.manager.service.store.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.store.StockOutEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.store.StockOutFeign;
import com.tw.aquaculture.manager.service.store.StockOutEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * StockOutEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("StockOutEntityServiceImpl")
public class StockOutEntityServiceImpl implements StockOutEntityService {
  @Autowired
  private StockOutFeign stockOutFeign;

  @Transactional
  @Override
  public StockOutEntity create(StockOutEntity stockOutEntity) {
    return createForm(stockOutEntity);
  }

  @Transactional
  @Override
  public StockOutEntity createForm(StockOutEntity stockOutEntity) {
    ResponseModel responseModel = stockOutFeign.create(stockOutEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StockOutEntity>() {
    });
  }

  @Transactional
  @Override
  public StockOutEntity update(StockOutEntity stockOutEntity) {
    return updateForm(stockOutEntity);
  }

  @Transactional
  @Override
  public StockOutEntity updateForm(StockOutEntity stockOutEntity) {
    ResponseModel responseModel = stockOutFeign.update(stockOutEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StockOutEntity>() {
    });
  }

  @Override
  public StockOutEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = stockOutFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StockOutEntity>() {
    });
  }

  @Override
  public StockOutEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    return null;
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    return;
  }

  @Override
  public StockOutEntity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = stockOutFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StockOutEntity>() {
    });
  }

  @Override
  public StockOutEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = stockOutFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StockOutEntity>() {
    });
  }

  @Override
  public List<StockOutEntity> findByBaseAndTimes(String baseId, Date start, Date end) {
    if (StringUtils.isBlank(baseId) || start == null || end == null) {
      return null;
    }
    ResponseModel responseModel = stockOutFeign.findByBaseAndTimes(baseId, start, end);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<StockOutEntity>>() {
    });
  }

  @Override
  public Double sumConsumeByTimes(String baseId, String matterId, Date start, Date end) {

    return stockOutFeign.sumConsumeByTimes(baseId, matterId, start, end);
  }
} 
