package com.tw.aquaculture.manager.service.store.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.store.TransferStoreEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.manager.feign.store.TransferStoreFeign;
import com.tw.aquaculture.manager.service.store.TransferStoreEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * TransferStoreEntity业务模型的服务层接口实现
 * @author saturn
 */
@Service("TransferStoreEntityServiceImpl")
public class TransferStoreEntityServiceImpl implements TransferStoreEntityService {
  @Autowired
  private TransferStoreFeign transferStoreFeign;

  @Transactional
  @Override
  public TransferStoreEntity create(TransferStoreEntity transferStoreEntity) {
    TransferStoreEntity current = this.createForm(transferStoreEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  } 
  @Transactional
  @Override
  public TransferStoreEntity createForm(TransferStoreEntity transferStoreEntity) {
    ResponseModel responseModel = transferStoreFeign.create(transferStoreEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TransferStoreEntity>() {
    });
  }

  @Transactional
  @Override
  public TransferStoreEntity update(TransferStoreEntity transferStoreEntity) {
    TransferStoreEntity current = this.updateForm(transferStoreEntity);
    //====================================================
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  } 
  @Transactional
  @Override
  public TransferStoreEntity updateForm(TransferStoreEntity transferStoreEntity) {
    ResponseModel responseModel = transferStoreFeign.update(transferStoreEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TransferStoreEntity>() {
    });
  }

  @Override
  public TransferStoreEntity findDetailsById(String id) {
    if(StringUtils.isBlank(id)) {
      return null; 
    }
    ResponseModel responseModel = transferStoreFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TransferStoreEntity>() {
    });
  }
  @Override
  public TransferStoreEntity findById(String id) {
    if(StringUtils.isBlank(id)) {
      return null;
    }
    return findDetailsById(id);
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id , "进行删除时，必须给定主键信息!!");
    TransferStoreEntity current = this.findById(id);
    /*if(current != null) {
      this.transferStoreEntityRepository.delete(current);
    }*/
  }
  @Override
  public TransferStoreEntity findDetailsByFormInstanceId(String formInstanceId) {
    if(StringUtils.isBlank(formInstanceId)) {
      return null; 
    }
    ResponseModel responseModel = transferStoreFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TransferStoreEntity>() {
    });
  }
  @Override
  public TransferStoreEntity findByFormInstanceId(String formInstanceId) {
    if(StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    return findDetailsByFormInstanceId(formInstanceId);
  } 
} 
