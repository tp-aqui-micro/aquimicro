package com.tw.aquaculture.manager.task;

import com.bizunited.platform.core.annotations.DynamicTaskService;
import com.tw.aquaculture.manager.feign.check.HrUserFeign;
import com.tw.aquaculture.manager.orgpondmap.service.OrgPondMapService;
import com.tw.aquaculture.manager.service.plan.NoticeEntityService;
import com.tw.aquaculture.manager.service.remote.MatterRemoteService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 陈荣
 * @date 2019/12/21 14:28
 */
@Component
public class Tasks {
  @Autowired
  private NoticeEntityService noticeEntityService;
  @Autowired
  private MatterRemoteService matterRemoteService;
  @Autowired
  private HrUserFeign hrUserFeign;
  @Autowired
  private OrgPondMapService orgPondMapService;
  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(Tasks.class);
  
  /**
   * 定时检测公告状态(每30分钟执行一次)
   */
  @DynamicTaskService(cornExpression = "0 0/30 * * * ?" , taskDesc = "定时检测公告状态(每30分钟执行一次)")
  public void checkNoticeTask(){
    LOGGER.info("scheduler task : com.tw.aquaculture.manager.task.Tasks.checkNoticeTask() ========= ");
    noticeEntityService.checkState();
  }

  /**
   * 定时拉取物料信息(每天1点执行)
   */
  @DynamicTaskService(cornExpression = "0 0 1 * * ? ", taskDesc = "定时拉取物料信息(每天1点执行)")
  public void pullMatter(){
    LOGGER.info("scheduler task : com.tw.aquaculture.manager.task.Tasks.pullMatter() ========= ");
    matterRemoteService.pullAMatters();
  }
  /**
   * 定时同步组织机构用户岗位角色信息(每天2点执行)
   */
  @DynamicTaskService(cornExpression = "0 0 2 * * ? " , taskDesc = "定时同步组织机构用户岗位角色信息")
  public void pullAllMatters() {
    LOGGER.info("scheduler task : com.tw.aquaculture.manager.task.Tasks.pullAllMatters() ========= ");
    hrUserFeign.pullAllMatters();
  }
  
  /**
   * 定期执行更新，更新对应关系(每30秒执行一次)
   */
  @DynamicTaskService(cornExpression = "0/30 * * * * ? " , taskDesc = "定期执行更新，更新对应关系(每30秒执行一次)")
  public void flushOrgPondMap() {
    this.orgPondMapService.flush();
  }
}
