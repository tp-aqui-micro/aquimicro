package com.tw.aquaculture.plan.configaration.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

/**
 * 登录操作错误将执行这个handle<br><br>
 * 
 * 由于后端提供的都是restful接口，并没有直接跳转的页面<br>
 * 所以只要访问的url没有通过权限认证，就跳到这个请求上，并直接排除权限异常
 * @author yinwenjie
 */
@Component("simpleAuthenticationFailureHandler")
public class SimpleAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
  
  @Override
  public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
    
  }
}
