package com.tw.aquaculture.plan.configaration.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;

/**
 * 鉴权成功（无论哪种鉴权模式）
 * 由于各个业务中心不需要登录鉴权，用户信息都是依靠spring session获取的，所以这里无需做任务处理
 * @author yinwenjie
 */
@Component("simpleAuthenticationSuccessHandler")
public class SimpleAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
  
  @Override
  @Transactional
  public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
    
  }
}
