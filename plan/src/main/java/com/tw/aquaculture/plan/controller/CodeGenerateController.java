package com.tw.aquaculture.plan.controller;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.CodeGenerateEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.plan.service.CodeGenerateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 陈荣
 * @date 2019/11/21 18:38
 */
@RestController
@RequestMapping(value = "/generateCode")
@Api(value = "CodeGenerateController", tags = "编码生成")
public class CodeGenerateController extends BaseController {

  private final static int CODELENGTH = 6;

  @Autowired
  CodeGenerateService codeGenerateService;

  @GetMapping(value = "generateCodeByPrefix")
  @ApiModelProperty(value = "根据模块生成编码")
  public String generateCodeByName(@ApiParam(value = "模块名") String name, @ApiParam(value = "前缀") String prefix) {
    return codeGenerateService.generateCode(name, prefix, CODELENGTH);
  }

  @GetMapping(value = "generateBusinessCodeByPrefix")
  @ApiModelProperty(value = "根据模块生成业务编码")
  public String generateBusinessCodeByPrefix(@ApiParam(value = "模块名") String name, @ApiParam(value = "前缀") String prefix) {
    return codeGenerateService.generateBusinessCode(name, prefix, CODELENGTH);
  }

  @GetMapping(value = "generateCode")
  @ApiModelProperty(value = "根据模块生成编码")
  public String generateCode(@ApiParam(value = "编码类型枚举") CodeTypeEnum codeTypeEnum) {
    return codeGenerateService.generateCode(codeTypeEnum);
  }

  @GetMapping(value = "generateBusinessCode")
  @ApiModelProperty(value = "根据模块生成业务编码")
  public String generateBusinessCode(@ApiParam(value = "编码类型枚举") CodeTypeEnum codeTypeEnum) {
    return codeGenerateService.generateBusinessCode(codeTypeEnum);
  }

  @ApiOperation(value = "根据编码类型和前缀查询")
  @RequestMapping(value = "/findByCodeTypeAndPrefix", method = {RequestMethod.GET})
  public ResponseModel findByCodeTypeAndPrefix(@ApiParam("编码类型") String codeType, @ApiParam("编码前缀") String prefix) {
    try {
      CodeGenerateEntity result = this.codeGenerateService.findByCodeTypeAndPrefix(codeType, prefix);
      return this.buildHttpResultW(result, new String[]{});
    } catch (Exception e) {
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "保存")
  @RequestMapping(value = "/saveAndFlush", method = {RequestMethod.POST})
  public ResponseModel saveAndFlush(@RequestBody CodeGenerateEntity entity) {
    try {
      CodeGenerateEntity result = this.codeGenerateService.saveAndFlush(entity);
      return this.buildHttpResultW(result, new String[]{});
    } catch (Exception e) {
      return this.buildHttpResultForException(e);
    }
  }


}
