package com.tw.aquaculture.plan.feign.check;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.base.TestIndexEentity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 陈荣
 * @date 2019/12/9 11:38
 */
@FeignClient(name = "${aquiServiceName.check}", qualifier = "TestIndexFeign", url = "${aquiPath.check}")
public interface TestIndexFeign {

  /**
   * 创建
   *
   * @param testIndexEntity
   * @return
   */
  @PostMapping(value = "/v1/testIndexEentitys")
  public ResponseModel create(@RequestBody TestIndexEentity testIndexEntity);

  /**
   * 修改
   *
   * @param noticeEntity
   * @return
   */
  @PostMapping(value = "/v1/testIndexEentitys/update")
  public ResponseModel update(@RequestBody TestIndexEentity noticeEntity);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/testIndexEentitys/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/testIndexEentitys/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 格局表单实例编号查详情
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/testIndexEentitys/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据表单实例编号查询
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/testIndexEentitys/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据id删除
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/testIndexEentitys/deleteById")
  public ResponseModel deleteById(@RequestParam("id") String id);

}
