package com.tw.aquaculture.plan.feign.process;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.vo.reportslist.ReportParamVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Created by chenrong on 2020/1/20
 */
@FeignClient(name = "${aquiServiceName.process}", qualifier = "OutFishFeign", url = "${aquiPath.process}", path = "/v1/outFishInfoEntitys")
public interface OutFishFeign {

  @PostMapping("/findByPondAndFishTypeAndTimes")
  ResponseModel findByPondAndFishTypeAndTimes(@RequestBody ReportParamVo reportParamVo);
}
