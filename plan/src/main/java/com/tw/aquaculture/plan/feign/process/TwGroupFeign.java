package com.tw.aquaculture.plan.feign.process;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.base.TwGroupEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 陈荣
 * @date 2019/12/9 11:38
 */
@FeignClient(name = "${aquiServiceName.process}", qualifier = "CodeGenerateFeign", url = "${aquiPath.process}")
public interface TwGroupFeign {

  /**
   * 创建
   *
   * @param twGroupEntity
   * @return
   */
  @PostMapping(value = "/v1/twGroupEntitys")
  public ResponseModel create(@RequestBody TwGroupEntity twGroupEntity);

  /**
   * 修改
   *
   * @param twGroupEntity
   * @return
   */
  @PostMapping(value = "/v1/twGroupEntitys/update")
  public ResponseModel update(@RequestBody TwGroupEntity twGroupEntity);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/twGroupEntitys/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/twGroupEntitys/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 格局表单实例编号查详情
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/twGroupEntitys/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据表单实例编号查询
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/twGroupEntitys/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据id删除
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/twGroupEntitys/deleteById")
  public ResponseModel deleteById(@RequestParam("id") String id);

}
