package com.tw.aquaculture.plan.feign.store;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.base.MatterEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @author 陈荣
 * @date 2019/12/9 11:38
 */
@FeignClient(name = "${aquiServiceName.store}", qualifier = "MatterFeign", url = "${aquiPath.store}")
public interface MatterFeign {

  /**
   * 创建
   *
   * @param matterEntity
   * @return
   */
  @PostMapping(value = "/v1/matterEntitys")
  public ResponseModel create(@RequestBody MatterEntity matterEntity);

  /**
   * 修改
   *
   * @param matterEntity
   * @return
   */
  @PostMapping(value = "/v1/matterEntitys/update")
  public ResponseModel update(@RequestBody MatterEntity matterEntity);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/matterEntitys/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/matterEntitys/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 格局表单实例编号查详情
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/matterEntitys/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据表单实例编号查询
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/matterEntitys/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据id删除
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/matterEntitys/deleteById")
  public ResponseModel deleteById(@RequestParam("id") String id);

  /**
   * 条件分页查询
   *
   * @param params
   * @param pageable
   * @return
   */
  @GetMapping(value = "/v1/matterEntitys/findByMatterConditions")
  ResponseModel findByMatterConditions(@RequestParam("params") Map<String, Object> params, @RequestParam("pageable") Pageable pageable);
}
