package com.tw.aquaculture.plan.repository;

import com.tw.aquaculture.common.entity.CodeGenerateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/** @author saturn */
@Repository("CodeGeneraterRepository")
public interface CodeGeneraterRepository
    extends JpaRepository<CodeGenerateEntity, String>,
        JpaSpecificationExecutor<CodeGenerateEntity> {

  CodeGenerateEntity findByCodeTypeAndPrefix(String codeType, String prefix);
}
