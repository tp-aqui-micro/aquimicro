package com.tw.aquaculture.plan.repository;

import com.tw.aquaculture.common.entity.servicecentre.NoticeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * NoticeEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_NoticeEntityRepository")
public interface NoticeEntityRepository
    extends
      JpaRepository<NoticeEntity, String>
      ,JpaSpecificationExecutor<NoticeEntity>
  {

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct noticeEntity from NoticeEntity noticeEntity "
      + " where noticeEntity.id=:id ")
  public NoticeEntity findDetailsById(@Param("id") String id);
  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   * @param formInstanceId 表单实例编号
   * */
  @Query("select distinct noticeEntity from NoticeEntity noticeEntity "
      + " where noticeEntity.formInstanceId=:formInstanceId ")
  public NoticeEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);
  /**
   * 按照表单实例编号进行查询
   * @param formInstanceId 表单实例编号
   * */
  @Query(" from NoticeEntity f where f.formInstanceId = :formInstanceId")
  public NoticeEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);



}