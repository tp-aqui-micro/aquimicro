package com.tw.aquaculture.plan.repository;

import com.tw.aquaculture.common.entity.servicecentre.NoticeOrgEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * NoticeOrgEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_NoticeOrgEntityRepository")
public interface NoticeOrgEntityRepository
    extends
      JpaRepository<NoticeOrgEntity, String>
      ,JpaSpecificationExecutor<NoticeOrgEntity>
  {
  /**
   * 按照组织进行详情查询（包括关联信息）
   * @param org 组织
   * */
  @Query("select distinct noticeOrgEntity from NoticeOrgEntity noticeOrgEntity "
      + " left join fetch noticeOrgEntity.org noticeOrgEntity_org "
      + " left join fetch noticeOrgEntity.notice noticeOrgEntity_notice "
       + " where noticeOrgEntity_org.id = :id")
  public Set<NoticeOrgEntity> findDetailsByOrg(@Param("id") String id);
  /**
   * 按照公告进行详情查询（包括关联信息）
   * @param notice 公告
   * */
  @Query("select distinct noticeOrgEntity from NoticeOrgEntity noticeOrgEntity "
      + " left join fetch noticeOrgEntity.org noticeOrgEntity_org "
      + " left join fetch noticeOrgEntity.notice noticeOrgEntity_notice "
       + " where noticeOrgEntity_notice.id = :id")
  public Set<NoticeOrgEntity> findDetailsByNotice(@Param("id") String id);

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct noticeOrgEntity from NoticeOrgEntity noticeOrgEntity "
      + " left join fetch noticeOrgEntity.org noticeOrgEntity_org "
      + " left join fetch noticeOrgEntity.notice noticeOrgEntity_notice "
      + " where noticeOrgEntity.id=:id ")
  public NoticeOrgEntity findDetailsById(@Param("id") String id);



}