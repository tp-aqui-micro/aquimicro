package com.tw.aquaculture.plan.repository;

import com.tw.aquaculture.common.entity.servicecentre.NoticeUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * NoticeUserEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_NoticeUserEntityRepository")
public interface NoticeUserEntityRepository
    extends
      JpaRepository<NoticeUserEntity, String>
      ,JpaSpecificationExecutor<NoticeUserEntity>
  {
  /**
   * 按照用户进行详情查询（包括关联信息）
   * @param user 用户
   * */
  @Query("select distinct noticeUserEntity from NoticeUserEntity noticeUserEntity "
      + " left join fetch noticeUserEntity.user noticeUserEntity_user "
      + " left join fetch noticeUserEntity.notice noticeUserEntity_notice "
       + " where noticeUserEntity_user.id = :id")
  public Set<NoticeUserEntity> findDetailsByUser(@Param("id") String id);
  /**
   * 按照公告进行详情查询（包括关联信息）
   * @param notice 公告
   * */
  @Query("select distinct noticeUserEntity from NoticeUserEntity noticeUserEntity "
      + " left join fetch noticeUserEntity.user noticeUserEntity_user "
      + " left join fetch noticeUserEntity.notice noticeUserEntity_notice "
       + " where noticeUserEntity_notice.id = :id")
  public Set<NoticeUserEntity> findDetailsByNotice(@Param("id") String id);

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct noticeUserEntity from NoticeUserEntity noticeUserEntity "
      + " left join fetch noticeUserEntity.user noticeUserEntity_user "
      + " left join fetch noticeUserEntity.notice noticeUserEntity_notice "
      + " where noticeUserEntity.id=:id ")
  public NoticeUserEntity findDetailsById(@Param("id") String id);



}