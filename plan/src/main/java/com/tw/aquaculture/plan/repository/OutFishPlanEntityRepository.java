package com.tw.aquaculture.plan.repository;

import com.tw.aquaculture.common.entity.plan.OutFishPlanEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * OutFishPlanEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_OutFishPlanEntityRepository")
public interface OutFishPlanEntityRepository
    extends JpaRepository<OutFishPlanEntity, String>, JpaSpecificationExecutor<OutFishPlanEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query(
      "select distinct outFishPlanEntity from OutFishPlanEntity outFishPlanEntity "
          + " left join fetch outFishPlanEntity.company outFishPlanEntity_company "
          + " left join fetch outFishPlanEntity.twBase outFishPlanEntity_twBase "
          + " left join fetch outFishPlanEntity.pond outFishPlanEntity_pond "
          + " left join fetch outFishPlanEntity.fishType outFishPlanEntity_fishType "
          + " where outFishPlanEntity.id=:id ")
  public OutFishPlanEntity findDetailsById(@Param("id") String id);
  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(
      "select distinct outFishPlanEntity from OutFishPlanEntity outFishPlanEntity "
          + " left join fetch outFishPlanEntity.company outFishPlanEntity_company "
          + " left join fetch outFishPlanEntity.twBase outFishPlanEntity_twBase "
          + " left join fetch outFishPlanEntity.pond outFishPlanEntity_pond "
          + " left join fetch outFishPlanEntity.fishType outFishPlanEntity_fishType "
          + " where outFishPlanEntity.formInstanceId=:formInstanceId ")
  public OutFishPlanEntity findDetailsByFormInstanceId(
      @Param("formInstanceId") String formInstanceId);
  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from OutFishPlanEntity f where f.formInstanceId = :formInstanceId")
  public OutFishPlanEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据塘口id查询
   * @param pondId
   * @return
   */
  @Query("select distinct outFishPlanEntity from OutFishPlanEntity outFishPlanEntity "
                  + " left join fetch outFishPlanEntity.pond pond "
                  + " where pond.id=:pondId ")
  List<OutFishPlanEntity> findByPond(@Param("pondId") String pondId);

  /**
   * 根据塘口和鱼种查询
   * @param pondId
   * @param fishTypeId
   * @return
   */
  @Query("select distinct outFishPlanEntity from OutFishPlanEntity outFishPlanEntity "
                  + " left join fetch outFishPlanEntity.pond pond "
                  + " left join fetch outFishPlanEntity.fishType fishType "
                  + " where pond.id=:pondId and fishType=:fishTypeId ")
  List<OutFishPlanEntity> findByPondAndFishType(@Param("pondId") String pondId, @Param("fishTypeId") String fishTypeId);

  /**
   * 根据塘口、鱼种和年度查询
   * @param pondId
   * @param fishTypeId
   * @param year
   * @return
   */
  @Query("select distinct outFishPlanEntity from OutFishPlanEntity outFishPlanEntity "
          + " left join fetch outFishPlanEntity.pond pond "
          + " left join fetch outFishPlanEntity.fishType fishType "
          + " where pond.id=:pondId and fishType=:fishTypeId and outFishPlanEntity.year=:year ")
  List<OutFishPlanEntity> findByPondAndFishTypeAndYear(@Param("pondId") String pondId, @Param("fishTypeId") String fishTypeId, @Param("year") String year);
}
