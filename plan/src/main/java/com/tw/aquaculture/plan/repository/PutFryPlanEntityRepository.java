package com.tw.aquaculture.plan.repository;

import com.tw.aquaculture.common.entity.plan.PutFryPlanEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * PutFryPlanEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_PutFryPlanEntityRepository")
public interface PutFryPlanEntityRepository
    extends JpaRepository<PutFryPlanEntity, String>, JpaSpecificationExecutor<PutFryPlanEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query(
      "select distinct putFryPlanEntity from PutFryPlanEntity putFryPlanEntity "
          + " left join fetch putFryPlanEntity.company putFryPlanEntity_company "
          + " left join fetch putFryPlanEntity.twBase putFryPlanEntity_twBase "
          + " left join fetch putFryPlanEntity.pond putFryPlanEntity_pond "
          + " left join fetch putFryPlanEntity.fishType putFryPlanEntity_fishType "
          + " where putFryPlanEntity.id=:id ")
  public PutFryPlanEntity findDetailsById(@Param("id") String id);
  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(
      "select distinct putFryPlanEntity from PutFryPlanEntity putFryPlanEntity "
          + " left join fetch putFryPlanEntity.company putFryPlanEntity_company "
          + " left join fetch putFryPlanEntity.twBase putFryPlanEntity_twBase "
          + " left join fetch putFryPlanEntity.pond putFryPlanEntity_pond "
          + " left join fetch putFryPlanEntity.fishType putFryPlanEntity_fishType "
          + " where putFryPlanEntity.formInstanceId=:formInstanceId ")
  public PutFryPlanEntity findDetailsByFormInstanceId(
      @Param("formInstanceId") String formInstanceId);
  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from PutFryPlanEntity f where f.formInstanceId = :formInstanceId")
  public PutFryPlanEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据塘口查询
   * @param pondId
   * @return
   */
  @Query(
          "select distinct putFryPlanEntity from PutFryPlanEntity putFryPlanEntity "
                  + " left join fetch putFryPlanEntity.pond pond "
                  + " where pond.id=:pondId ")
  List<PutFryPlanEntity> findByPond(@Param("pondId") String pondId);

  /**
   * 根据塘口和鱼种查询
   * @param pondId
   * @param fishTypeId
   * @return
   */
  @Query(
          "select distinct putFryPlanEntity from PutFryPlanEntity putFryPlanEntity "
                  + " left join fetch putFryPlanEntity.pond pond "
                  + " left join fetch putFryPlanEntity.fishType fishType "
                  + " where pond.id=:pondId and fishType.id=:fishTypeId ")
  List<PutFryPlanEntity> findByPondAndFishType(@Param("pondId") String pondId, @Param("fishTypeId") String fishTypeId);

  /**
   * 根据塘口、鱼种和年度查询
   * @param pondId
   * @param fishTypeId
   * @param year
   * @return
   */
  @Query(
          "select distinct putFryPlanEntity from PutFryPlanEntity putFryPlanEntity "
                  + " left join fetch putFryPlanEntity.pond pond "
                  + " left join fetch putFryPlanEntity.fishType fishType "
                  + " where pond.id=:pondId and fishType.id=:fishTypeId and putFryPlanEntity.year=:year ")
  List<PutFryPlanEntity> findByPondAndFishTypeAndYear(@Param("pondId") String pondId, @Param("fishTypeId") String fishTypeId, @Param("year") String year);
}
