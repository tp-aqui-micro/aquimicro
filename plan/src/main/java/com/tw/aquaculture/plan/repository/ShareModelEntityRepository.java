package com.tw.aquaculture.plan.repository;

import com.tw.aquaculture.common.entity.base.ShareModelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * ShareModelEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_ShareModelEntityRepository")
public interface ShareModelEntityRepository
    extends JpaRepository<ShareModelEntity, String>, JpaSpecificationExecutor<ShareModelEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query(
      "select distinct shareModelEntity from ShareModelEntity shareModelEntity "
          + " where shareModelEntity.id=:id ")
  public ShareModelEntity findDetailsById(@Param("id") String id);
  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(
      "select distinct shareModelEntity from ShareModelEntity shareModelEntity "
          + " where shareModelEntity.formInstanceId=:formInstanceId ")
  public ShareModelEntity findDetailsByFormInstanceId(
      @Param("formInstanceId") String formInstanceId);
  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from ShareModelEntity f where f.formInstanceId = :formInstanceId")
  public ShareModelEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);
}
