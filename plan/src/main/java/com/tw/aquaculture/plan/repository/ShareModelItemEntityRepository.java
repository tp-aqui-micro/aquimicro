package com.tw.aquaculture.plan.repository;

import com.tw.aquaculture.common.entity.base.ShareModelItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * ShareModelItemEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_ShareModelItemEntityRepository")
public interface ShareModelItemEntityRepository
    extends JpaRepository<ShareModelItemEntity, String>,
        JpaSpecificationExecutor<ShareModelItemEntity> {
  /**
   * 按照鱼种进行详情查询（包括关联信息）
   *
   * @param fishType 鱼种
   */
  @Query(
      "select distinct shareModelItemEntity from ShareModelItemEntity shareModelItemEntity "
          + " left join fetch shareModelItemEntity.fishType shareModelItemEntity_fishType "
          + " left join fetch shareModelItemEntity.shareModel shareModelItemEntity_shareModel "
          + " where shareModelItemEntity_fishType.id = :id")
  public Set<ShareModelItemEntity> findDetailsByFishType(@Param("id") String id);
  /**
   * 按照分摊模式进行详情查询（包括关联信息）
   *
   * @param shareModel 分摊模式
   */
  @Query(
      "select distinct shareModelItemEntity from ShareModelItemEntity shareModelItemEntity "
          + " left join fetch shareModelItemEntity.fishType shareModelItemEntity_fishType "
          + " left join fetch shareModelItemEntity.shareModel shareModelItemEntity_shareModel "
          + " where shareModelItemEntity_shareModel.id = :id")
  public Set<ShareModelItemEntity> findDetailsByShareModel(@Param("id") String id);

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query(
      "select distinct shareModelItemEntity from ShareModelItemEntity shareModelItemEntity "
          + " left join fetch shareModelItemEntity.fishType shareModelItemEntity_fishType "
          + " left join fetch shareModelItemEntity.shareModel shareModelItemEntity_shareModel "
          + " where shareModelItemEntity.id=:id ")
  public ShareModelItemEntity findDetailsById(@Param("id") String id);
}
