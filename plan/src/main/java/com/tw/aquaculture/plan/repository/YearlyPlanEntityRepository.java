package com.tw.aquaculture.plan.repository;

import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.plan.YearlyPlanEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * YearlyPlanEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_YearlyPlanEntityRepository")
public interface YearlyPlanEntityRepository
    extends JpaRepository<YearlyPlanEntity, String>, JpaSpecificationExecutor<YearlyPlanEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query(
      "select distinct yearlyPlanEntity from YearlyPlanEntity yearlyPlanEntity "
          + " left join fetch yearlyPlanEntity.company yearlyPlanEntity_company "
          + " left join fetch yearlyPlanEntity.twBase yearlyPlanEntity_twBase "
          + " left join fetch yearlyPlanEntity.pond yearlyPlanEntity_pond "
          + " left join fetch yearlyPlanEntity.fishType yearlyPlanEntity_fishType "
          + " where yearlyPlanEntity.id=:id ")
  public YearlyPlanEntity findDetailsById(@Param("id") String id);
  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(
      "select distinct yearlyPlanEntity from YearlyPlanEntity yearlyPlanEntity "
          + " left join fetch yearlyPlanEntity.company yearlyPlanEntity_company "
          + " left join fetch yearlyPlanEntity.twBase yearlyPlanEntity_twBase "
          + " left join fetch yearlyPlanEntity.pond yearlyPlanEntity_pond "
          + " left join fetch yearlyPlanEntity.fishType yearlyPlanEntity_fishType "
          + " where yearlyPlanEntity.formInstanceId=:formInstanceId ")
  public YearlyPlanEntity findDetailsByFormInstanceId(
      @Param("formInstanceId") String formInstanceId);
  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from YearlyPlanEntity f where f.formInstanceId = :formInstanceId")
  public YearlyPlanEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据塘口查询
   * @param pondId
   * @return
   */
  @Query(
          "select distinct yearlyPlanEntity from YearlyPlanEntity yearlyPlanEntity "
                  + " left join fetch yearlyPlanEntity.pond pond "
                  + " where pond.id=:pondId ")
  List<YearlyPlanEntity> findByPond(@Param("pondId") String pondId);

  /**
   * 根据塘口id和年度查询
   * @param pondId
   * @param year
   * @return
   */
  @Query(" select distinct yearlyPlanEntity from YearlyPlanEntity yearlyPlanEntity "
                  + " left join fetch yearlyPlanEntity.pond pond "
                  + " where pond.id=:pondId and yearlyPlanEntity.year=:year ")
  List<YearlyPlanEntity> findByPondAndYearly(@Param("pondId") String pondId, @Param("year") String year);

  /**
   * 根据塘口id、年度、鱼种查询
   * @param pondId
   * @param yearly
   * @param fishTypeId
   * @return
   */
  @Query(" select distinct yearlyPlanEntity from YearlyPlanEntity yearlyPlanEntity "
                  + " left join fetch yearlyPlanEntity.pond yearlyPlanEntity_pond "
                  + " left join fetch yearlyPlanEntity.fishType yearlyPlanEntity_fishType "
                  + " where yearlyPlanEntity.year=:yearly and yearlyPlanEntity_pond.id=:pondId and yearlyPlanEntity_fishType.id=:fishTypeId ")
  List<YearlyPlanEntity> findByPondAndYearlyFishType(@Param("pondId") String pondId, @Param("yearly") String yearly, @Param("fishTypeId") String fishTypeId);
}
