package com.tw.aquaculture.plan.service.internal;

import com.tw.aquaculture.common.entity.CodeGenerateEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.utils.CodeGeneratorUtil;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.plan.repository.CodeGeneraterRepository;
import com.tw.aquaculture.plan.service.CodeGenerateService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author 陈荣
 * @date 2019/11/22 11:22
 */
@Service
public class CodeGenerateServiceImpl implements CodeGenerateService {

  @Autowired
  CodeGeneraterRepository codeGeneraterRepository;

  @Autowired
  CodeGeneratorUtil codeGeneratorUtil;

  @Override
  @Transactional
  public String generateCode(String codeType, String prefix, int length) {
    String code = null;
    CodeGenerateEntity codeGenerateEntity = codeGeneraterRepository.findByCodeTypeAndPrefix(codeType, prefix);
    if (codeGenerateEntity == null) {
      CodeGenerateEntity entity = new CodeGenerateEntity();
      entity.setCodeType(codeType);
      entity.setPrefix(prefix);
      entity.setLength(length);
      entity.setValue(1L);
      CodeGenerateEntity result = codeGeneraterRepository.saveAndFlush(entity);
      if (result != null) {
        code = prefix + CodeGeneratorUtil.toCodeString(result.getValue(), length);
      }
    } else {
      code = codeGenerateEntity.getPrefix() + CodeGeneratorUtil.toCodeString(codeGenerateEntity.getValue() + 1L, codeGenerateEntity.getLength());
      codeGenerateEntity.setValue(codeGenerateEntity.getValue() + 1L);
      codeGeneraterRepository.saveAndFlush(codeGenerateEntity);
    }
    return code;
  }

  @Override
  public String generateCode(CodeTypeEnum codeTypeEnum) {
    if (codeTypeEnum == null) {
      return null;
    }
    return generateCode(codeTypeEnum.getCodeType(), codeTypeEnum.getPrefix(), codeTypeEnum.getLength());
  }

  @Override
  public String generateBusinessCode(CodeTypeEnum codeTypeEnum) {
    if (codeTypeEnum == null) {
      return null;
    }
    return codeGeneratorUtil.generateBusinessNo(codeTypeEnum.getPrefix(), DateUtils.getCodeDate(),
            codeTypeEnum.getLength(), codeTypeEnum.getCodeType());
  }

  @Override
  public String generateBusinessCode(String name, String prefix, int i) {
    return codeGeneratorUtil.generateBusinessNo(prefix, DateUtils.getCodeDate(),
            i, name);
  }

  @Override
  public CodeGenerateEntity findByCodeTypeAndPrefix(String codeType, String prefix) {
    if (StringUtils.isBlank(prefix) || StringUtils.isBlank(codeType)) {
      return null;
    }
    return codeGeneraterRepository.findByCodeTypeAndPrefix(codeType, prefix);
  }

  @Override
  public CodeGenerateEntity saveAndFlush(CodeGenerateEntity entity) {
    if (entity == null) {
      return null;
    }
    return codeGeneraterRepository.saveAndFlush(entity);
  }
}
