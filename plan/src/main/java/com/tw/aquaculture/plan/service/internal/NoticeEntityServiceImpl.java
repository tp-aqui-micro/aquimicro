package com.tw.aquaculture.plan.service.internal;

import com.bizunited.platform.kuiper.starter.service.KuiperToolkitService;
import com.google.common.collect.Sets;
import com.tw.aquaculture.common.entity.servicecentre.NoticeEntity;
import com.tw.aquaculture.common.entity.servicecentre.NoticeOrgEntity;
import com.tw.aquaculture.common.entity.servicecentre.NoticeUserEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.plan.repository.NoticeEntityRepository;
import com.tw.aquaculture.plan.service.CodeGenerateService;
import com.tw.aquaculture.plan.service.NoticeEntityService;
import com.tw.aquaculture.plan.service.NoticeOrgEntityService;
import com.tw.aquaculture.plan.service.NoticeUserEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * NoticeEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("NoticeEntityServiceImpl")
public class NoticeEntityServiceImpl implements NoticeEntityService {
  @Autowired
  private NoticeOrgEntityService noticeOrgEntityService;
  @Autowired
  private NoticeUserEntityService noticeUserEntityService;
  @Autowired
  private NoticeEntityRepository noticeEntityRepository;
  @Autowired
  private CodeGenerateService codeGenerateService;
  /**
   * Kuiper表单引擎用于减少编码工作量的工具包
   */
  @Autowired
  private KuiperToolkitService kuiperToolkitService;

  @Transactional
  @Override
  public NoticeEntity create(NoticeEntity noticeEntity) {
    NoticeEntity current = this.createForm(noticeEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  }

  @Transactional
  @Override
  public NoticeEntity createForm(NoticeEntity noticeEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *   2.3.2、以及验证每个分组的OneToMany明细信息
     * */

    noticeEntity.setCode(this.codeGenerateService.generateBusinessCode(CodeTypeEnum.NOTICE_ENTITY));
    this.createValidation(noticeEntity);
    if(noticeEntity.getEndTime().getTime()<noticeEntity.getStartTime().getTime()){
      throw new RuntimeException("开始时间必须小于结束时间");
    }

    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================

    this.noticeEntityRepository.saveAndFlush(noticeEntity);
    // 处理明细信息：组织
    Set<NoticeOrgEntity> noticeOrgEntityItems = noticeEntity.getOrgs();
    if (noticeOrgEntityItems != null) {
      for (NoticeOrgEntity noticeOrgEntityItem : noticeOrgEntityItems) {
        noticeOrgEntityItem.setNotice(noticeEntity);
        this.noticeOrgEntityService.create(noticeOrgEntityItem);
      }
    }
    // 处理明细信息：用户
    Set<NoticeUserEntity> noticeUserEntityItems = noticeEntity.getUsers();
    if (noticeUserEntityItems != null) {
      for (NoticeUserEntity noticeUserEntityItem : noticeUserEntityItems) {
        noticeUserEntityItem.setNotice(noticeEntity);
        this.noticeUserEntityService.create(noticeUserEntityItem);
      }
    }

    this.noticeEntityRepository.flush();
    // 返回最终处理的结果，里面带有详细的关联信息
    return noticeEntity;
  }

  /**
   * 在创建一个新的NoticeEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(NoticeEntity noticeEntity) {
    Validate.notNull(noticeEntity, "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(noticeEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    noticeEntity.setId(null);
    Validate.notBlank(noticeEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(noticeEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(noticeEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(noticeEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notBlank(noticeEntity.getCode(), "添加信息时，编码不能为空！");
    Validate.notBlank(noticeEntity.getTitle(), "添加信息时，标题不能为空！");
    Validate.notBlank(noticeEntity.getContent(), "添加信息时，正文不能为空！");
    Validate.notNull(noticeEntity.getStartTime(), "添加信息时，开始时间不能为空！");
    Validate.notNull(noticeEntity.getEndTime(), "添加信息时，结束时间不能为空！");
    Validate.notNull(noticeEntity.getEnableState(), "添加信息时，生效状态不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况）
    Validate.isTrue(noticeEntity.getFormInstanceId() == null || noticeEntity.getFormInstanceId().length() < 255, "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(noticeEntity.getCreateName() == null || noticeEntity.getCreateName().length() < 255, "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(noticeEntity.getCreatePosition() == null || noticeEntity.getCreatePosition().length() < 255, "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(noticeEntity.getModifyName() == null || noticeEntity.getModifyName().length() < 255, "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(noticeEntity.getModifyPosition() == null || noticeEntity.getModifyPosition().length() < 255, "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(noticeEntity.getCode() == null || noticeEntity.getCode().length() < 255, "编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(noticeEntity.getTitle() == null || noticeEntity.getTitle().length() < 255, "标题,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(noticeEntity.getContent() == null || noticeEntity.getContent().length() < 10000, "正文,在进行添加时填入值超过了限定长度(10000)，请检查!");
    Validate.isTrue(noticeEntity.getEnclosure() == null || noticeEntity.getEnclosure().length() < 1000, "附件,在进行添加时填入值超过了限定长度(1000)，请检查!");
    NoticeEntity currentNoticeEntity = this.findByFormInstanceId(noticeEntity.getFormInstanceId());
    Validate.isTrue(currentNoticeEntity == null, "表单实例编号已存在,请检查");
  }

  @Transactional
  @Override
  public NoticeEntity update(NoticeEntity noticeEntity) {
    NoticeEntity current = this.updateForm(noticeEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  }

  @Transactional
  @Override
  public NoticeEntity updateForm(NoticeEntity noticeEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     *
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */

    this.updateValidation(noticeEntity);
    // ===================基本信息
    String currentId = noticeEntity.getId();
    Optional<NoticeEntity> op_currentNoticeEntity = this.noticeEntityRepository.findById(currentId);
    NoticeEntity currentNoticeEntity = op_currentNoticeEntity.orElse(null);
    currentNoticeEntity = Validate.notNull(currentNoticeEntity, "未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentNoticeEntity.setFormInstanceId(noticeEntity.getFormInstanceId());
    currentNoticeEntity.setCreateTime(noticeEntity.getCreateTime());
    currentNoticeEntity.setCreateName(noticeEntity.getCreateName());
    currentNoticeEntity.setCreatePosition(noticeEntity.getCreatePosition());
    currentNoticeEntity.setModifyTime(noticeEntity.getModifyTime());
    currentNoticeEntity.setModifyName(noticeEntity.getModifyName());
    currentNoticeEntity.setModifyPosition(noticeEntity.getModifyPosition());
    currentNoticeEntity.setCode(noticeEntity.getCode());
    currentNoticeEntity.setTitle(noticeEntity.getTitle());
    currentNoticeEntity.setContent(noticeEntity.getContent());
    currentNoticeEntity.setStartTime(noticeEntity.getStartTime());
    currentNoticeEntity.setEndTime(noticeEntity.getEndTime());
    currentNoticeEntity.setEnableState(noticeEntity.getEnableState());
    currentNoticeEntity.setEnclosure(noticeEntity.getEnclosure());

    this.noticeEntityRepository.saveAndFlush(currentNoticeEntity);
    // ==========准备处理OneToMany关系的详情orgs
    // 清理出那些需要删除的orgs信息
    Set<NoticeOrgEntity> noticeOrgEntitys = null;
    if (noticeEntity.getOrgs() != null) {
      noticeOrgEntitys = Sets.newLinkedHashSet(noticeEntity.getOrgs());
    } else {
      noticeOrgEntitys = Sets.newLinkedHashSet();
    }
    Set<NoticeOrgEntity> dbNoticeOrgEntitys = this.noticeOrgEntityService.findDetailsByNotice(noticeEntity.getId());
    if (dbNoticeOrgEntitys == null) {
      dbNoticeOrgEntitys = Sets.newLinkedHashSet();
    }
    // 求得的差集既是需要删除的数据 
    Set<String> mustDeleteOrgsPks = kuiperToolkitService.collectionDiffent(dbNoticeOrgEntitys, noticeOrgEntitys, NoticeOrgEntity::getId);
    if (mustDeleteOrgsPks != null && !mustDeleteOrgsPks.isEmpty()) {
      for (String mustDeleteOrgsPk : mustDeleteOrgsPks) {
        this.noticeOrgEntityService.deleteById(mustDeleteOrgsPk);
      }
    }
    // 对传来的记录进行添加或者修改操作 
    for (NoticeOrgEntity noticeOrgEntityItem : noticeOrgEntitys) {
      noticeOrgEntityItem.setNotice(noticeEntity);
      if (StringUtils.isBlank(noticeOrgEntityItem.getId())) {
        noticeOrgEntityService.create(noticeOrgEntityItem);
      } else {
        noticeOrgEntityService.update(noticeOrgEntityItem);
      }
    }

    // ==========准备处理OneToMany关系的详情users
    // 清理出那些需要删除的users信息
    Set<NoticeUserEntity> noticeUserEntitys = null;
    if (noticeEntity.getUsers() != null) {
      noticeUserEntitys = Sets.newLinkedHashSet(noticeEntity.getUsers());
    } else {
      noticeUserEntitys = Sets.newLinkedHashSet();
    }
    Set<NoticeUserEntity> dbNoticeUserEntitys = this.noticeUserEntityService.findDetailsByNotice(noticeEntity.getId());
    if (dbNoticeUserEntitys == null) {
      dbNoticeUserEntitys = Sets.newLinkedHashSet();
    }
    // 求得的差集既是需要删除的数据 
    Set<String> mustDeleteUsersPks = kuiperToolkitService.collectionDiffent(dbNoticeUserEntitys, noticeUserEntitys, NoticeUserEntity::getId);
    if (mustDeleteUsersPks != null && !mustDeleteUsersPks.isEmpty()) {
      for (String mustDeleteUsersPk : mustDeleteUsersPks) {
        this.noticeUserEntityService.deleteById(mustDeleteUsersPk);
      }
    }
    // 对传来的记录进行添加或者修改操作 
    for (NoticeUserEntity noticeUserEntityItem : noticeUserEntitys) {
      noticeUserEntityItem.setNotice(noticeEntity);
      if (StringUtils.isBlank(noticeUserEntityItem.getId())) {
        noticeUserEntityService.create(noticeUserEntityItem);
      } else {
        noticeUserEntityService.update(noticeUserEntityItem);
      }
    }

    return currentNoticeEntity;
  }

  /**
   * 在更新一个已有的NoticeEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(NoticeEntity noticeEntity) {
    Validate.isTrue(!StringUtils.isBlank(noticeEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");

    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(noticeEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(noticeEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(noticeEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(noticeEntity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    Validate.notBlank(noticeEntity.getCode(), "修改信息时，编码不能为空！");
    Validate.notBlank(noticeEntity.getTitle(), "修改信息时，标题不能为空！");
    Validate.notBlank(noticeEntity.getContent(), "修改信息时，正文不能为空！");
    Validate.notNull(noticeEntity.getStartTime(), "修改信息时，开始时间不能为空！");
    Validate.notNull(noticeEntity.getEndTime(), "修改信息时，结束时间不能为空！");
    Validate.notNull(noticeEntity.getEnableState(), "修改信息时，生效状态不能为空！");

    // 重复性判断，基本属性，需要满足unique = true
    NoticeEntity currentForFormInstanceId = this.findByFormInstanceId(noticeEntity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId(), noticeEntity.getId()), "表单实例编号已存在,请检查");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(noticeEntity.getFormInstanceId() == null || noticeEntity.getFormInstanceId().length() < 255, "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(noticeEntity.getCreateName() == null || noticeEntity.getCreateName().length() < 255, "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(noticeEntity.getCreatePosition() == null || noticeEntity.getCreatePosition().length() < 255, "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(noticeEntity.getModifyName() == null || noticeEntity.getModifyName().length() < 255, "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(noticeEntity.getModifyPosition() == null || noticeEntity.getModifyPosition().length() < 255, "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(noticeEntity.getCode() == null || noticeEntity.getCode().length() < 255, "编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(noticeEntity.getTitle() == null || noticeEntity.getTitle().length() < 255, "标题,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(noticeEntity.getContent() == null || noticeEntity.getContent().length() < 10000, "正文,在进行修改时填入值超过了限定长度(10000)，请检查!");
    Validate.isTrue(noticeEntity.getEnclosure() == null || noticeEntity.getEnclosure().length() < 10000, "附件,在进行修改时填入值超过了限定长度(255)，请检查!");
  }

  @Override
  public NoticeEntity findDetailsById(String id) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(id)) {
      return null;
    }
    NoticeEntity current = this.noticeEntityRepository.findDetailsById(id);
    if (current == null) {
      return null;
    }
    String currentPkValue = current.getId();
    // 2、======
    // 组织 的明细信息查询
    Collection<NoticeOrgEntity> oneToManyOrgsDetails = this.noticeOrgEntityService.findDetailsByNotice(currentPkValue);
    current.setOrgs(Sets.newLinkedHashSet(oneToManyOrgsDetails));
    // 用户 的明细信息查询
    Collection<NoticeUserEntity> oneToManyUsersDetails = this.noticeUserEntityService.findDetailsByNotice(currentPkValue);
    current.setUsers(Sets.newLinkedHashSet(oneToManyUsersDetails));

    return current;
  }

  @Override
  public NoticeEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }

    Optional<NoticeEntity> op = noticeEntityRepository.findById(id);
    return op.orElse(null);
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    NoticeEntity current = this.findById(id);
    if (current != null) {
      this.noticeEntityRepository.delete(current);
    }
  }

  @Override
  public NoticeEntity findDetailsByFormInstanceId(String formInstanceId) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    NoticeEntity current = this.noticeEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if (current == null) {
      return null;
    }
    String currentPkValue = current.getId();
    // 2、======
    // 组织 的明细信息查询
    Collection<NoticeOrgEntity> oneToManyOrgsDetails = this.noticeOrgEntityService.findDetailsByNotice(currentPkValue);
    current.setOrgs(Sets.newLinkedHashSet(oneToManyOrgsDetails));
    // 用户 的明细信息查询
    Collection<NoticeUserEntity> oneToManyUsersDetails = this.noticeUserEntityService.findDetailsByNotice(currentPkValue);
    current.setUsers(Sets.newLinkedHashSet(oneToManyUsersDetails));

    return current;
  }

  @Override
  public NoticeEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    return this.noticeEntityRepository.findByFormInstanceId(formInstanceId);
  }

  @Override
  //@DynamicTaskService(cornExpression = "0 0/30 * * * ?", taskDesc = "检测公告定时任务, 每小时的30分执行一次")
  public void checkState() {
    List<NoticeEntity> noticeEntities = noticeEntityRepository.findAll();
    if(CollectionUtils.isEmpty(noticeEntities)){
      return;
    }
    Date date = new Date();
    for(NoticeEntity noticeEntity : noticeEntities){
      checkState(noticeEntity, date);
    }
  }

  /**
   * 检测单个公告状态
   * @param noticeEntity
   */
  private void checkState(NoticeEntity noticeEntity, Date date){
    switch (noticeEntity.getEnableState()){
      case 1:
        if(DateUtils.notBetween(date, noticeEntity.getStartTime(), noticeEntity.getEndTime())){
          noticeEntity.setEnableState(0);
          noticeEntityRepository.saveAndFlush(noticeEntity);
        }
      case 0:
        if(DateUtils.between(date, noticeEntity.getStartTime(), noticeEntity.getEndTime())){
          noticeEntity.setEnableState(1);
          noticeEntityRepository.saveAndFlush(noticeEntity);
        }
    }
  }
} 
