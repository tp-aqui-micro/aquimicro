package com.tw.aquaculture.plan.service.internal;

import com.bizunited.platform.rbac.server.service.OrganizationService;
import com.bizunited.platform.rbac.server.vo.OrganizationVo;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.common.entity.plan.OutFishPlanEntity;
import com.tw.aquaculture.common.entity.plan.PutFryPlanEntity;
import com.tw.aquaculture.common.entity.process.OutFishInfoEntity;
import com.tw.aquaculture.common.entity.process.PushFryEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.common.vo.reportslist.OutFishPlanReportResultVo;
import com.tw.aquaculture.common.vo.reportslist.PushFryPlanReportParamVo;
import com.tw.aquaculture.common.vo.reportslist.PushFryPlanReportResultVo;
import com.tw.aquaculture.plan.repository.PutFryPlanEntityRepository;
import com.tw.aquaculture.plan.service.CodeGenerateService;
import com.tw.aquaculture.plan.service.PutFryPlanEntityService;
import com.tw.aquaculture.plan.service.remote.FishTypeEntityService;
import com.tw.aquaculture.plan.service.remote.PondEntityService;
import com.tw.aquaculture.plan.service.remote.PushFryEntityService;
import com.tw.aquaculture.plan.service.remote.TwBaseEntityService;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.*;

/**
 * PutFryPlanEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("PutFryPlanEntityServiceImpl")
public class PutFryPlanEntityServiceImpl implements PutFryPlanEntityService {

  @Autowired private FishTypeEntityService fishTypeEntityService;
  @Autowired private PutFryPlanEntityRepository putFryPlanEntityRepository;
  @Autowired private CodeGenerateService codeGenerateService;
  @Autowired private TwBaseEntityService twBaseEntityService;
  @Autowired private OrganizationService organizationService;
  @Autowired private PushFryEntityService pushFryEntityService;
  @Autowired private PondEntityService pondEntityService;

  @Transactional
  @Override
  public PutFryPlanEntity create(PutFryPlanEntity putFryPlanEntity) {
    PutFryPlanEntity current = this.createForm(putFryPlanEntity);
    // ====================================================
    //    这里可以处理第三方系统调用（或特殊处理过程）
    // ====================================================
    return current;
  }

  @Transactional
  @Override
  public PutFryPlanEntity createForm(PutFryPlanEntity putFryPlanEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *   2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    putFryPlanEntity.setCode(
        codeGenerateService.generateBusinessCode(CodeTypeEnum.PUT_FRY_PLAN_ENTITY));
    this.createValidation(putFryPlanEntity);

    // ===============================
    //  和业务有关的验证填写在这个区域
    // ===============================

    this.putFryPlanEntityRepository.saveAndFlush(putFryPlanEntity);

    // 返回最终处理的结果，里面带有详细的关联信息
    return putFryPlanEntity;
  }
  /** 在创建一个新的PutFryPlanEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值 */
  private void createValidation(PutFryPlanEntity putFryPlanEntity) {
    Validate.notNull(putFryPlanEntity, "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(putFryPlanEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    putFryPlanEntity.setId(null);
    Validate.notBlank(putFryPlanEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(putFryPlanEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(putFryPlanEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(putFryPlanEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notBlank(putFryPlanEntity.getCode(), "添加信息时，计划编码不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况）
    Validate.isTrue(
        putFryPlanEntity.getFormInstanceId() == null
            || putFryPlanEntity.getFormInstanceId().length() < 255,
        "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
        putFryPlanEntity.getCreateName() == null || putFryPlanEntity.getCreateName().length() < 255,
        "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
        putFryPlanEntity.getCreatePosition() == null
            || putFryPlanEntity.getCreatePosition().length() < 255,
        "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
        putFryPlanEntity.getModifyName() == null || putFryPlanEntity.getModifyName().length() < 255,
        "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
        putFryPlanEntity.getModifyPosition() == null
            || putFryPlanEntity.getModifyPosition().length() < 255,
        "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
        putFryPlanEntity.getCode() == null || putFryPlanEntity.getCode().length() < 255,
        "计划编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
        putFryPlanEntity.getYear() == null || putFryPlanEntity.getYear().length() < 255,
        "年度,在进行添加时填入值超过了限定长度(255)，请检查!");
    PutFryPlanEntity currentPutFryPlanEntity =
        this.findByFormInstanceId(putFryPlanEntity.getFormInstanceId());
    Validate.isTrue(currentPutFryPlanEntity == null, "表单实例编号已存在,请检查");
    // 验证ManyToOne关联：鱼种绑定值的正确性
    FishTypeEntity currentFishType = putFryPlanEntity.getFishType();
    Validate.notNull(currentFishType, "鱼种信息必须传入，请检查!!");
    String currentPkFishType = currentFishType.getId();
    Validate.notBlank(currentPkFishType, "创建操作时，当前鱼种信息必须关联！");
    Validate.notNull(this.fishTypeEntityService.findById(currentPkFishType), "鱼种关联信息未找到，请检查!!");
  }

  @Transactional
  @Override
  public PutFryPlanEntity update(PutFryPlanEntity putFryPlanEntity) {
    PutFryPlanEntity current = this.updateForm(putFryPlanEntity);
    // ====================================================
    //    这里可以处理第三方系统调用（或特殊处理过程）
    // ====================================================
    return current;
  }

  @Transactional
  @Override
  public PutFryPlanEntity updateForm(PutFryPlanEntity putFryPlanEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     *
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */

    this.updateValidation(putFryPlanEntity);
    // ===================基本信息
    String currentId = putFryPlanEntity.getId();
    Optional<PutFryPlanEntity> op_currentPutFryPlanEntity =
        this.putFryPlanEntityRepository.findById(currentId);
    PutFryPlanEntity currentPutFryPlanEntity = op_currentPutFryPlanEntity.orElse(null);
    currentPutFryPlanEntity = Validate.notNull(currentPutFryPlanEntity, "未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentPutFryPlanEntity.setFormInstanceId(putFryPlanEntity.getFormInstanceId());
    currentPutFryPlanEntity.setCreateTime(putFryPlanEntity.getCreateTime());
    currentPutFryPlanEntity.setCreateName(putFryPlanEntity.getCreateName());
    currentPutFryPlanEntity.setCreatePosition(putFryPlanEntity.getCreatePosition());
    currentPutFryPlanEntity.setModifyTime(putFryPlanEntity.getModifyTime());
    currentPutFryPlanEntity.setModifyName(putFryPlanEntity.getModifyName());
    currentPutFryPlanEntity.setModifyPosition(putFryPlanEntity.getModifyPosition());
    currentPutFryPlanEntity.setCode(putFryPlanEntity.getCode());
    currentPutFryPlanEntity.setYear(putFryPlanEntity.getYear());
    currentPutFryPlanEntity.setPutFryTime(putFryPlanEntity.getPutFryTime());
    currentPutFryPlanEntity.setPutFryQuantity(putFryPlanEntity.getPutFryQuantity());
    currentPutFryPlanEntity.setPutFryWeight(putFryPlanEntity.getPutFryWeight());
    currentPutFryPlanEntity.setFishFryPrice(putFryPlanEntity.getFishFryPrice());
    currentPutFryPlanEntity.setFishFryTotalPrice(putFryPlanEntity.getFishFryTotalPrice());
    currentPutFryPlanEntity.setCompany(putFryPlanEntity.getCompany());
    currentPutFryPlanEntity.setTwBase(putFryPlanEntity.getTwBase());
    currentPutFryPlanEntity.setPond(putFryPlanEntity.getPond());
    currentPutFryPlanEntity.setFishType(putFryPlanEntity.getFishType());

    this.putFryPlanEntityRepository.saveAndFlush(currentPutFryPlanEntity);
    return currentPutFryPlanEntity;
  }
  /** 在更新一个已有的PutFryPlanEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值 */
  private void updateValidation(PutFryPlanEntity putFryPlanEntity) {
    Validate.isTrue(!StringUtils.isBlank(putFryPlanEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");

    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(putFryPlanEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(putFryPlanEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(putFryPlanEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(putFryPlanEntity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    Validate.notBlank(putFryPlanEntity.getCode(), "修改信息时，计划编码不能为空！");

    // 重复性判断，基本属性，需要满足unique = true
    PutFryPlanEntity currentForFormInstanceId =
        this.findByFormInstanceId(putFryPlanEntity.getFormInstanceId());
    Validate.isTrue(
        currentForFormInstanceId == null
            || StringUtils.equals(currentForFormInstanceId.getId(), putFryPlanEntity.getId()),
        "表单实例编号已存在,请检查");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(
        putFryPlanEntity.getFormInstanceId() == null
            || putFryPlanEntity.getFormInstanceId().length() < 255,
        "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
        putFryPlanEntity.getCreateName() == null || putFryPlanEntity.getCreateName().length() < 255,
        "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
        putFryPlanEntity.getCreatePosition() == null
            || putFryPlanEntity.getCreatePosition().length() < 255,
        "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
        putFryPlanEntity.getModifyName() == null || putFryPlanEntity.getModifyName().length() < 255,
        "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
        putFryPlanEntity.getModifyPosition() == null
            || putFryPlanEntity.getModifyPosition().length() < 255,
        "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
        putFryPlanEntity.getCode() == null || putFryPlanEntity.getCode().length() < 255,
        "计划编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
        putFryPlanEntity.getYear() == null || putFryPlanEntity.getYear().length() < 255,
        "年度,在进行修改时填入值超过了限定长度(255)，请检查!");

    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
    FishTypeEntity currentForFishType = putFryPlanEntity.getFishType();
    Validate.notNull(currentForFishType, "修改信息时，鱼种必须传入，请检查!!");
  }

  @Override
  public PutFryPlanEntity findDetailsById(String id) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(id)) {
      return null;
    }
    PutFryPlanEntity current = this.putFryPlanEntityRepository.findDetailsById(id);
    if (current == null) {
      return null;
    }
    return current;
  }

  @Override
  public PutFryPlanEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }

    Optional<PutFryPlanEntity> op = putFryPlanEntityRepository.findById(id);
    return op.orElse(null);
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    PutFryPlanEntity current = this.findById(id);
    if (current != null) {
      this.putFryPlanEntityRepository.delete(current);
    }
  }

  @Override
  public PutFryPlanEntity findDetailsByFormInstanceId(String formInstanceId) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    PutFryPlanEntity current =
        this.putFryPlanEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if (current == null) {
      return null;
    }
    return current;
  }

  @Override
  public PutFryPlanEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    return this.putFryPlanEntityRepository.findByFormInstanceId(formInstanceId);
  }

  @Override
  public List<PushFryPlanReportResultVo> pushFryPlanReport(PushFryPlanReportParamVo pushFryPlanReportParamVo) {
    List<PushFryPlanReportResultVo> result = Lists.newArrayList();
    String baseId = pushFryPlanReportParamVo.getBaseId();
    String pondId = pushFryPlanReportParamVo.getPondId();
    String fishTypeId = pushFryPlanReportParamVo.getFishType();
    String yearly = pushFryPlanReportParamVo.getYearly();
    List<PondEntity> ponds = buildPonds(baseId, pondId);
    List<FishTypeEntity> fishTypes = buildFishTypes(fishTypeId);
    Validate.notEmpty(ponds, "没有获取到塘口信息");
    List<String> years = Lists.newArrayList();
    if (StringUtils.isNotBlank(yearly)) {
      years.addAll(Arrays.asList(yearly.split(",")));
    }
    ponds.forEach(pond -> {
      outFishPlanReportByPond(pond, fishTypes, years, result);
    });
    return result;
  }

  /**
   * 根据塘口查询出鱼计划报表
   *
   * @param pond
   * @param fishTypes
   * @param years
   * @param result
   */
  private void outFishPlanReportByPond(PondEntity pond, List<FishTypeEntity> fishTypes, List<String> years, List<PushFryPlanReportResultVo> result) {
    if (pond == null) {
      return;
    }
    List<FishTypeEntity> fishTypeEntities = Lists.newArrayList();
    if (CollectionUtils.isEmpty(fishTypes)) {
      List<PutFryPlanEntity> pushFryPlanEntities = this.putFryPlanEntityRepository.findByPond(pond.getId());
      if (CollectionUtils.isEmpty(pushFryPlanEntities)) {
        return;
      }
      Map<String, FishTypeEntity> fishTypeEntityMap = Maps.newTreeMap();
      pushFryPlanEntities.forEach(o -> {
        if (o.getFishType() != null) {
          fishTypeEntityMap.put(o.getFishType().getId(), o.getFishType());
        }
      });
      fishTypeEntityMap.forEach((k, v) -> {
        fishTypeEntities.add(v);
      });
    } else {
      fishTypeEntities.addAll(fishTypes);
    }
    if (CollectionUtils.isEmpty(fishTypeEntities)) {
      return;
    }
    fishTypeEntities.forEach(fishType -> {
      outFishReportByFishType(pond, fishType, years, result);
    });
  }

  /**
   * 根据鱼种查询出鱼计划报表
   *
   * @param pond
   * @param fishType
   * @param years
   * @param result
   */
  private void outFishReportByFishType(PondEntity pond, FishTypeEntity fishType, List<String> years, List<PushFryPlanReportResultVo> result) {
    if (pond == null || fishType == null) {
      return;
    }
    List<String> yearlies = Lists.newArrayList();
    if (CollectionUtils.isEmpty(years)) {
      List<PutFryPlanEntity> putFryPlanEntities = this.putFryPlanEntityRepository.findByPondAndFishType(pond.getId(), fishType.getId());
      if (CollectionUtils.isEmpty(putFryPlanEntities)) {
        return;
      }
      Set<String> yearSet = Sets.newHashSet();
      putFryPlanEntities.forEach(o -> {
        yearSet.add(o.getYear());
      });
    } else {
      yearlies.addAll(years);
    }
    if (CollectionUtils.isEmpty(yearlies)) {
      return;
    }
    yearlies.forEach(year -> {
      outFishPlanReportByYearly(pond, fishType, year, result);
    });

  }

  /**
   * 根据年度查询出鱼计划报表
   *
   * @param pond
   * @param fishType
   * @param year
   * @param result
   */
  private void outFishPlanReportByYearly(PondEntity pond, FishTypeEntity fishType, String year, List<PushFryPlanReportResultVo> result) {
    if (pond == null || fishType == null || StringUtils.isBlank(year)) {
      return;
    }
    PushFryPlanReportResultVo pushFryPlanReportResultVo = new PushFryPlanReportResultVo();
    pushFryPlanReportResultVo.setYearly(year);
    pushFryPlanReportResultVo.setPondId(pond.getId());
    pushFryPlanReportResultVo.setPondName(pond.getName());
    if (pond.getBase() != null) {
      pushFryPlanReportResultVo.setBaseId(pond.getBase().getId());
      TwBaseEntity twBaseEntity = twBaseEntityService.findDetailsById(pond.getBase().getId());
      if (twBaseEntity != null && twBaseEntity.getBaseOrg() != null) {
        OrganizationVo baseOrg = organizationService.findDetailsById(twBaseEntity.getBaseOrg().getId());
        if (baseOrg != null) {
          pushFryPlanReportResultVo.setBaseName(baseOrg.getOrgName());
        }
        if (baseOrg != null && baseOrg.getParent() != null) {
          pushFryPlanReportResultVo.setCompanyId(baseOrg.getParent().getId());
          pushFryPlanReportResultVo.setCompanyName(baseOrg.getParent().getOrgName());
        }
      }
    }
    double planCount = 0;
    double planWeight = 0;
    double realCount = 0;
    double realWeight = 0;
    double transportFee = 0;
    double planProcess = 0;
    List<PutFryPlanEntity> putFryPlanEntities = this.putFryPlanEntityRepository.findByPondAndFishTypeAndYear(pond.getId(), fishType.getId(), year);
    if (CollectionUtils.isEmpty(putFryPlanEntities)) {
      return;
    }
    for (PutFryPlanEntity putFryPlanEntity : putFryPlanEntities) {
      planWeight += putFryPlanEntity.getPutFryWeight() == null ? 0 : putFryPlanEntity.getPutFryWeight().doubleValue();
      planCount += putFryPlanEntity.getPutFryQuantity();
    }
    pushFryPlanReportResultVo.setPlanPushFryCount(planCount);
    pushFryPlanReportResultVo.setPlanPushFryWeight(planWeight);
    Date start = DateUtils.getYearStart(year);
    Date end = DateUtils.getYearEnd(year);
    List<PushFryEntity> pushFryEntities = pushFryEntityService.findByPondAndFishTypeAndTimes(pond.getId(), fishType.getId(), start, end);
    for (PushFryEntity pushFryEntity : pushFryEntities) {
      realCount += pushFryEntity.getCount()==null?0:pushFryEntity.getCount();
      realWeight += pushFryEntity.getSummation()==null?0:pushFryEntity.getSummation();
      transportFee += pushFryEntity.getTransportFee() == null ? 0 : pushFryEntity.getTransportFee();
    }
    pushFryPlanReportResultVo.setRealPushFryCount(realCount);
    pushFryPlanReportResultVo.setRealPushFryWeight(realWeight);
    pushFryPlanReportResultVo.setTransportFee(transportFee);
    if (planCount != 0) {
      planProcess = realCount / planCount;
    }
    pushFryPlanReportResultVo.setPushFryProcess(planProcess);
    result.add(pushFryPlanReportResultVo);
  }

  /**
   * 构建鱼种实体
   *
   * @param fishTypeId
   * @return
   */
  private List<FishTypeEntity> buildFishTypes(String fishTypeId) {
    List<FishTypeEntity> fishTypeEntities = Lists.newArrayList();
    if (StringUtils.isBlank(fishTypeId)) {
      return fishTypeEntities;
    }
    for (String id : fishTypeId.split(",")) {
      FishTypeEntity fishTypeEntity = fishTypeEntityService.findDetailsById(id);
      if (fishTypeEntity == null) continue;
      fishTypeEntities.add(fishTypeEntity);
    }
    return fishTypeEntities;
  }

  /**
   * 构建塘口列表
   *
   * @param baseId
   * @param pondId
   * @return
   */
  private List<PondEntity> buildPonds(String baseId, String pondId) {
    List<PondEntity> pondEntities = Lists.newArrayList();
    if (StringUtils.isNotBlank(pondId)) {
      for (String p : pondId.split(",")) {
        PondEntity pondEntity = pondEntityService.findDetailsById(p);
        if (pondEntity == null) continue;
        pondEntities.add(pondEntity);
      }
    } else if (StringUtils.isNotBlank(baseId)) {
      for (String b : baseId.split(",")) {
        List<PondEntity> pondEntityList = pondEntityService.findPondsByBaseId(b, EnableStateEnum.ALL.getState());
        if (CollectionUtils.isEmpty(pondEntityList)) continue;
        pondEntities.addAll(pondEntityList);
      }
    } else {
      throw new RuntimeException("没有获取到基地或塘口");
    }
    return pondEntities;
  }
}
