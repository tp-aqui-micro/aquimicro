package com.tw.aquaculture.plan.service.internal;

import com.alibaba.druid.sql.dialect.oracle.ast.stmt.OracleCreateTableStatement;
import com.bizunited.platform.core.repository.OrganizationRepository;
import com.bizunited.platform.rbac.server.service.OrganizationService;
import com.bizunited.platform.rbac.server.vo.OrganizationVo;
import com.google.common.collect.Sets;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.common.entity.plan.YearlyPlanEntity;
import com.tw.aquaculture.common.entity.process.OutFishInfoEntity;
import com.tw.aquaculture.common.entity.process.OutFishRegistEntity;
import com.tw.aquaculture.common.entity.process.PushFryEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.common.vo.reportslist.YearlyPlanReportParamVo;
import com.tw.aquaculture.common.vo.reportslist.YearlyPlanReportResultVo;
import com.tw.aquaculture.plan.repository.YearlyPlanEntityRepository;
import com.tw.aquaculture.plan.service.CodeGenerateService;
import com.tw.aquaculture.plan.service.YearlyPlanEntityService;
import com.tw.aquaculture.plan.service.remote.*;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.*;

/**
 * YearlyPlanEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("YearlyPlanEntityServiceImpl")
public class YearlyPlanEntityServiceImpl implements YearlyPlanEntityService {

  @Autowired
  private FishTypeEntityService fishTypeEntityService;
  @Autowired
  private YearlyPlanEntityRepository yearlyPlanEntityRepository;
  @Autowired
  private CodeGenerateService codeGenerateService;
  @Autowired
  private PondEntityService pondEntityService;
  @Autowired
  private TwBaseEntityService twBaseEntityService;
  @Autowired
  private OrganizationService organizationService;
  @Autowired
  private PushFryEntityService pushFryEntityService;
  @Autowired
  private OutFishEntityService outFishEntityService;


  @Transactional
  @Override
  public YearlyPlanEntity create(YearlyPlanEntity yearlyPlanEntity) {
    YearlyPlanEntity current = this.createForm(yearlyPlanEntity);
    // ====================================================
    //    这里可以处理第三方系统调用（或特殊处理过程）
    // ====================================================
    return current;
  }

  @Transactional
  @Override
  public YearlyPlanEntity createForm(YearlyPlanEntity yearlyPlanEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *   2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    yearlyPlanEntity.setCode(
            codeGenerateService.generateBusinessCode(CodeTypeEnum.YEARLY_PLAN_ENTITY));
    this.createValidation(yearlyPlanEntity);

    // ===============================
    //  和业务有关的验证填写在这个区域
    // ===============================

    this.yearlyPlanEntityRepository.saveAndFlush(yearlyPlanEntity);

    // 返回最终处理的结果，里面带有详细的关联信息
    return yearlyPlanEntity;
  }

  /**
   * 在创建一个新的YearlyPlanEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(YearlyPlanEntity yearlyPlanEntity) {
    Validate.notNull(yearlyPlanEntity, "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(yearlyPlanEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    yearlyPlanEntity.setId(null);
    Validate.notBlank(yearlyPlanEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(yearlyPlanEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(yearlyPlanEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(yearlyPlanEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notBlank(yearlyPlanEntity.getCode(), "添加信息时，规划编码不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况）
    Validate.isTrue(
            yearlyPlanEntity.getFormInstanceId() == null
                    || yearlyPlanEntity.getFormInstanceId().length() < 255,
            "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
            yearlyPlanEntity.getCreateName() == null || yearlyPlanEntity.getCreateName().length() < 255,
            "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
            yearlyPlanEntity.getCreatePosition() == null
                    || yearlyPlanEntity.getCreatePosition().length() < 255,
            "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
            yearlyPlanEntity.getModifyName() == null || yearlyPlanEntity.getModifyName().length() < 255,
            "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
            yearlyPlanEntity.getModifyPosition() == null
                    || yearlyPlanEntity.getModifyPosition().length() < 255,
            "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
            yearlyPlanEntity.getCode() == null || yearlyPlanEntity.getCode().length() < 255,
            "规划编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
            yearlyPlanEntity.getYear() == null || yearlyPlanEntity.getYear().length() < 255,
            "年度,在进行添加时填入值超过了限定长度(255)，请检查!");
    YearlyPlanEntity currentYearlyPlanEntity =
            this.findByFormInstanceId(yearlyPlanEntity.getFormInstanceId());
    Validate.isTrue(currentYearlyPlanEntity == null, "表单实例编号已存在,请检查");
    // 验证ManyToOne关联：鱼种绑定值的正确性
    FishTypeEntity currentFishType = yearlyPlanEntity.getFishType();
    Validate.notNull(currentFishType, "鱼种信息必须传入，请检查!!");
    String currentPkFishType = currentFishType.getId();
    Validate.notBlank(currentPkFishType, "创建操作时，当前鱼种信息必须关联！");
    Validate.notNull(this.fishTypeEntityService.findById(currentPkFishType), "鱼种关联信息未找到，请检查!!");
  }

  @Transactional
  @Override
  public YearlyPlanEntity update(YearlyPlanEntity yearlyPlanEntity) {
    YearlyPlanEntity current = this.updateForm(yearlyPlanEntity);
    // ====================================================
    //    这里可以处理第三方系统调用（或特殊处理过程）
    // ====================================================
    return current;
  }

  @Transactional
  @Override
  public YearlyPlanEntity updateForm(YearlyPlanEntity yearlyPlanEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     *
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */

    this.updateValidation(yearlyPlanEntity);
    // ===================基本信息
    String currentId = yearlyPlanEntity.getId();
    Optional<YearlyPlanEntity> op_currentYearlyPlanEntity =
            this.yearlyPlanEntityRepository.findById(currentId);
    YearlyPlanEntity currentYearlyPlanEntity = op_currentYearlyPlanEntity.orElse(null);
    currentYearlyPlanEntity = Validate.notNull(currentYearlyPlanEntity, "未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentYearlyPlanEntity.setFormInstanceId(yearlyPlanEntity.getFormInstanceId());
    currentYearlyPlanEntity.setCreateTime(yearlyPlanEntity.getCreateTime());
    currentYearlyPlanEntity.setCreateName(yearlyPlanEntity.getCreateName());
    currentYearlyPlanEntity.setCreatePosition(yearlyPlanEntity.getCreatePosition());
    currentYearlyPlanEntity.setModifyTime(yearlyPlanEntity.getModifyTime());
    currentYearlyPlanEntity.setModifyName(yearlyPlanEntity.getModifyName());
    currentYearlyPlanEntity.setModifyPosition(yearlyPlanEntity.getModifyPosition());
    currentYearlyPlanEntity.setCode(yearlyPlanEntity.getCode());
    currentYearlyPlanEntity.setYear(yearlyPlanEntity.getYear());
    currentYearlyPlanEntity.setPutFryQuantity(yearlyPlanEntity.getPutFryQuantity());
    currentYearlyPlanEntity.setPutFryAmount(yearlyPlanEntity.getPutFryAmount());
    currentYearlyPlanEntity.setPutFryWeight(yearlyPlanEntity.getPutFryWeight());
    currentYearlyPlanEntity.setOutFishWeight(yearlyPlanEntity.getOutFishWeight());
    currentYearlyPlanEntity.setOutFishQuantity(yearlyPlanEntity.getOutFishQuantity());
    currentYearlyPlanEntity.setOutFishAmount(yearlyPlanEntity.getOutFishAmount());
    currentYearlyPlanEntity.setCompany(yearlyPlanEntity.getCompany());
    currentYearlyPlanEntity.setTwBase(yearlyPlanEntity.getTwBase());
    currentYearlyPlanEntity.setPond(yearlyPlanEntity.getPond());
    currentYearlyPlanEntity.setFishType(yearlyPlanEntity.getFishType());

    this.yearlyPlanEntityRepository.saveAndFlush(currentYearlyPlanEntity);
    return currentYearlyPlanEntity;
  }

  /**
   * 在更新一个已有的YearlyPlanEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(YearlyPlanEntity yearlyPlanEntity) {
    Validate.isTrue(!StringUtils.isBlank(yearlyPlanEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");

    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(yearlyPlanEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(yearlyPlanEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(yearlyPlanEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(yearlyPlanEntity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    Validate.notBlank(yearlyPlanEntity.getCode(), "修改信息时，规划编码不能为空！");

    // 重复性判断，基本属性，需要满足unique = true
    YearlyPlanEntity currentForFormInstanceId =
            this.findByFormInstanceId(yearlyPlanEntity.getFormInstanceId());
    Validate.isTrue(
            currentForFormInstanceId == null
                    || StringUtils.equals(currentForFormInstanceId.getId(), yearlyPlanEntity.getId()),
            "表单实例编号已存在,请检查");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(
            yearlyPlanEntity.getFormInstanceId() == null
                    || yearlyPlanEntity.getFormInstanceId().length() < 255,
            "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
            yearlyPlanEntity.getCreateName() == null || yearlyPlanEntity.getCreateName().length() < 255,
            "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
            yearlyPlanEntity.getCreatePosition() == null
                    || yearlyPlanEntity.getCreatePosition().length() < 255,
            "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
            yearlyPlanEntity.getModifyName() == null || yearlyPlanEntity.getModifyName().length() < 255,
            "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
            yearlyPlanEntity.getModifyPosition() == null
                    || yearlyPlanEntity.getModifyPosition().length() < 255,
            "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
            yearlyPlanEntity.getCode() == null || yearlyPlanEntity.getCode().length() < 255,
            "规划编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
            yearlyPlanEntity.getYear() == null || yearlyPlanEntity.getYear().length() < 255,
            "年度,在进行修改时填入值超过了限定长度(255)，请检查!");

    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
    FishTypeEntity currentForFishType = yearlyPlanEntity.getFishType();
    Validate.notNull(currentForFishType, "修改信息时，鱼种必须传入，请检查!!");
  }

  @Override
  public YearlyPlanEntity findDetailsById(String id) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(id)) {
      return null;
    }
    YearlyPlanEntity current = this.yearlyPlanEntityRepository.findDetailsById(id);
    if (current == null) {
      return null;
    }
    return current;
  }

  @Override
  public YearlyPlanEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }

    Optional<YearlyPlanEntity> op = yearlyPlanEntityRepository.findById(id);
    return op.orElse(null);
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    YearlyPlanEntity current = this.findById(id);
    if (current != null) {
      this.yearlyPlanEntityRepository.delete(current);
    }
  }

  @Override
  public YearlyPlanEntity findDetailsByFormInstanceId(String formInstanceId) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    YearlyPlanEntity current =
            this.yearlyPlanEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if (current == null) {
      return null;
    }
    return current;
  }

  @Override
  public YearlyPlanEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    return this.yearlyPlanEntityRepository.findByFormInstanceId(formInstanceId);
  }

  @Override
  public List<YearlyPlanReportResultVo> yearlyPlanReport(YearlyPlanReportParamVo yearlyPlanReportParamVo) {
    String baseId = yearlyPlanReportParamVo.getBaseId();
    String pondId = yearlyPlanReportParamVo.getPondId();
    String fishTypeId = yearlyPlanReportParamVo.getFishType();
    String yearly = yearlyPlanReportParamVo.getYearly();
    List<PondEntity> ponds = Lists.newArrayList();
    List<String> yearlies = Lists.newArrayList();
    if (StringUtils.isNotBlank(yearly)) {
      yearlies.addAll(Arrays.asList(yearly.split(",")));
    }
    List<FishTypeEntity> fishTypes = Lists.newArrayList();
    if (StringUtils.isNotBlank(pondId)) {
      this.buildPondsByPondIds(pondId, ponds);
    } else if (StringUtils.isNotBlank(baseId)) {
      this.buildPondsByBaseIds(baseId, ponds);
    } else {
      throw new RuntimeException("没有获取到基地信息");
    }
    buildFishType(fishTypeId, fishTypes);
    Validate.notEmpty(ponds, "没有获取到塘口信息");
    List<YearlyPlanReportResultVo> result = Lists.newArrayList();
    ponds.forEach(p -> {
      yearlyPlanReportByPond(p, yearlies, fishTypes, result);
    });
    return result;
  }

  /**
   * 根据塘口查询年度计划报表
   *
   * @param p
   * @param yearlies
   * @param fishTypes
   * @param result
   */
  private void yearlyPlanReportByPond(PondEntity p, List<String> yearlies, List<FishTypeEntity> fishTypes, List<YearlyPlanReportResultVo> result) {
    if (p == null) {
      return;
    }
    List<String> ys = Lists.newArrayList();
    if (CollectionUtils.isEmpty(yearlies)) {
      List<YearlyPlanEntity> yearlyPlanEntities = yearlyPlanEntityRepository.findByPond(p.getId());
      if (CollectionUtils.isEmpty(yearlyPlanEntities)) {
        return;
      }
      Set<String> sets = Sets.newTreeSet();
      yearlyPlanEntities.forEach(y -> {
        sets.add(y.getYear());
      });
      ys.addAll(sets);
    } else {
      ys.addAll(yearlies);
    }
    ys.forEach(y -> {
      yearlyPlanReportByYearly(p, y, fishTypes, result);
    });

  }

  /**
   * 根据年度查询年度计划报表
   *
   * @param p
   * @param y
   * @param fishTypes
   * @param result
   */
  private void yearlyPlanReportByYearly(PondEntity p, String y, List<FishTypeEntity> fishTypes, List<YearlyPlanReportResultVo> result) {
    List<FishTypeEntity> fishTypeEntities = Lists.newArrayList();
    if (CollectionUtils.isEmpty(fishTypes)) {
      List<YearlyPlanEntity> yearlyPlanEntities = this.yearlyPlanEntityRepository.findByPondAndYearly(p.getId(), y);
      if (CollectionUtils.isEmpty(yearlyPlanEntities)) {
        return;
      }
      Set<String> fishTypeIds = Sets.newTreeSet();
      yearlyPlanEntities.forEach(yp -> {
        if (yp.getFishType() != null) {
          fishTypeIds.add(yp.getFishType().getId());
        }
      });
      fishTypeIds.forEach(fi -> {
        FishTypeEntity fishTypeEntity = fishTypeEntityService.findDetailsById(fi);
        if (fishTypeEntity != null) {
          fishTypeEntities.add(fishTypeEntity);
        }
      });
    } else {
      fishTypeEntities.addAll(fishTypes);
    }
    fishTypeEntities.forEach(f -> {
      yearlyPlanReportByFishType(p, y, f, result);
    });

  }

  /**
   * 根据鱼种查询报表
   *
   * @param p
   * @param y
   * @param f
   * @param result
   */
  private void yearlyPlanReportByFishType(PondEntity p, String y, FishTypeEntity f, List<YearlyPlanReportResultVo> result) {
    if (p == null || StringUtils.isBlank(y) || f == null || result == null) {
      return;
    }
    Date startTime = DateUtils.getYearStart(y);
    Date endTime = DateUtils.getYearEnd(y);
    List<YearlyPlanEntity> yearlyPlanEntities = yearlyPlanEntityRepository.findByPondAndYearlyFishType(p.getId(), y, f.getId());
    YearlyPlanReportResultVo rel = ComputeEarlyPlanReport(yearlyPlanEntities, p);
    if (rel == null) {
      return;
    }
    List<PushFryEntity> pushFryEntities = pushFryEntityService.findByPondAndFishTypeAndTimes(p.getId(), f.getId(), startTime, endTime);
    List<OutFishInfoEntity> outFishInfoEntities = outFishEntityService.findByPondAndFishTypeAndTimes(p.getId(), f.getId(), startTime, endTime);
    if (CollectionUtils.isEmpty(pushFryEntities)) {
      rel.setRealPushFryCount(0);
      rel.setRealPushFryWeight(0);
      rel.setPushFryProgress(0);
    } else {
      double pushFryCount = 0;
      double pushFryWeight = 0;
      for (PushFryEntity pushFryEntity : pushFryEntities) {
        pushFryCount += (pushFryEntity.getCount() == null ? 0 : pushFryEntity.getCount());
        pushFryWeight += (pushFryEntity.getSummation() == null ? 0 : pushFryEntity.getSummation());
      }
      rel.setRealPushFryCount(pushFryCount);
      rel.setRealPushFryWeight(pushFryWeight);
      rel.setPushFryProgress(rel.getPlanPushFryCount() == 0 ? 0 : pushFryCount / rel.getPlanPushFryCount());
    }
    if (CollectionUtils.isEmpty(outFishInfoEntities)) {
      rel.setRealOutFishCount(0);
      rel.setRealOutFishWeight(0);
      rel.setOutFishProgress(0);
    } else {
      double outFishCount = 0;
      double outFishWeight = 0;
      for (OutFishInfoEntity outFishInfoEntity : outFishInfoEntities) {
        outFishCount += outFishInfoEntity.getSpecs() == 0 ? 0 : (outFishInfoEntity.getTotalWeight() / outFishInfoEntity.getSpecs());
        outFishWeight += outFishInfoEntity.getTotalWeight();
        rel.setRealOutFishCount(outFishCount);
        rel.setRealOutFishWeight(outFishWeight);
        rel.setOutFishProgress(rel.getPlanOutFishWeight() == 0 ? 0 : outFishWeight / rel.getPlanOutFishWeight());
      }
    }
    result.add(rel);
  }

  /**
   * 计算报表
   *
   * @param yearlyPlanEntities
   * @param p
   */
  private YearlyPlanReportResultVo ComputeEarlyPlanReport(List<YearlyPlanEntity> yearlyPlanEntities, PondEntity p) {
    if (CollectionUtils.isEmpty(yearlyPlanEntities) || p == null) {
      return null;
    }
    String year = null;
    double pushFryCount = 0;
    double pushFryWeight = 0;
    double outFishCount = 0;
    double outFishWeight = 0;

    for (YearlyPlanEntity y : yearlyPlanEntities) {
      year = y.getYear();
      pushFryCount += y.getPutFryQuantity();
      pushFryWeight += (y.getPutFryWeight() == null ? 0.0 : y.getPutFryWeight().doubleValue());
      outFishCount += y.getOutFishQuantity();
      outFishWeight += (y.getOutFishWeight() == null ? 0.0 : y.getOutFishWeight().doubleValue());
    }
    YearlyPlanReportResultVo rel = new YearlyPlanReportResultVo();
    rel.setPlanPushFryCount(pushFryCount);
    rel.setPlanPushFryWeight(pushFryWeight);
    rel.setPlanOutFishCount(outFishCount);
    rel.setPlanOutFishWeight(outFishWeight);
    rel.setPondId(p.getId());
    rel.setPondName(p.getName());
    if (p.getBase() != null) {
      TwBaseEntity baseEntity = twBaseEntityService.findDetailsById(p.getBase().getId());
      rel.setBaseId(baseEntity.getId());
      if (baseEntity.getBaseOrg() != null) {
        rel.setBaseName(baseEntity.getBaseOrg().getOrgName());
        OrganizationVo organizationVo = organizationService.findDetailsById(baseEntity.getBaseOrg().getId());
        if (organizationVo != null && organizationVo.getParent() != null) {
          rel.setCompanyId(organizationVo.getParent().getId());
          rel.setCompanyName(organizationVo.getParent().getOrgName());
        }
      }
    }
    rel.setYearly(year);
    return rel;
  }

  /**
   * 根据鱼种id生成鱼种实体
   *
   * @param fishTypeId
   * @param fishTypes
   */
  private void buildFishType(String fishTypeId, List<FishTypeEntity> fishTypes) {
    if (StringUtils.isBlank(fishTypeId)) {
      return;
    }
    String[] fishTypeIds = fishTypeId.split(",");
    for (String id : fishTypeIds) {
      FishTypeEntity fishTypeEntity = fishTypeEntityService.findDetailsById(id);
      if (fishTypeEntity == null) {
        continue;
      }
      fishTypes.add(fishTypeEntity);
    }
  }

  /**
   * 根据基地id生成塘口实体
   *
   * @param baseId
   * @param ponds
   */
  private void buildPondsByBaseIds(String baseId, List<PondEntity> ponds) {
    if (StringUtils.isBlank(baseId)) {
      return;
    }
    String[] baseIds = baseId.split(",");
    for (String id : baseIds) {
      List<PondEntity> pondEntities = pondEntityService.findPondsByBaseId(id, EnableStateEnum.ALL.getState());
      if (!CollectionUtils.isEmpty(pondEntities)) {
        ponds.addAll(pondEntities);
      }
    }
  }

  /**
   * 根据塘口id生成塘口实体
   *
   * @param pondId
   * @param ponds
   */
  private void buildPondsByPondIds(String pondId, List<PondEntity> ponds) {
    if (StringUtils.isBlank(pondId)) {
      return;
    }
    String[] pondIds = pondId.split(",");
    for (String id : pondIds) {
      PondEntity pondEntity = pondEntityService.findDetailsById(id);
      if (pondEntity != null) {
        ponds.add(pondEntity);
      }
    }
  }
}
