package com.tw.aquaculture.plan.service.remote;

import com.tw.aquaculture.common.entity.process.OutFishInfoEntity;

import java.util.Date;
import java.util.List;

/**
 * Created by chenrong on 2020/1/20
 */
public interface OutFishEntityService {

  /**
   * 根据塘口id、鱼种id和时间区间查询出鱼
   * @param pondId
   * @param fishTypeId
   * @param startTime
   * @param endTime
   * @return
   */
  List<OutFishInfoEntity> findByPondAndFishTypeAndTimes(String pondId, String fishTypeId, Date startTime, Date endTime);
}
