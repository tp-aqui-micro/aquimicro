package com.tw.aquaculture.plan.service.remote;

import com.tw.aquaculture.common.entity.process.PushFryEntity;

import java.util.Date;
import java.util.List;

/**
 * Created by chenrong on 2020/1/20
 */
public interface PushFryEntityService {

  /**
   * 根据塘口id、鱼种id和时间区间查询投苗
   * @param pondId
   * @param fishTypeId
   * @param startTime
   * @param endTime
   * @return
   */
  List<PushFryEntity> findByPondAndFishTypeAndTimes(String pondId, String fishTypeId, Date startTime, Date endTime);
}
