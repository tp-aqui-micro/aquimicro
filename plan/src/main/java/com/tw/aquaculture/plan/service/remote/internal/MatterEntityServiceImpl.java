package com.tw.aquaculture.plan.service.remote.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.base.MatterEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.plan.feign.store.MatterFeign;
import com.tw.aquaculture.plan.service.remote.MatterEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Map;

/**
 * MatterEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("MatterEntityServiceImpl")
public class MatterEntityServiceImpl implements MatterEntityService {
  @Autowired
  private MatterFeign matterFeign;

  @Transactional
  @Override
  public MatterEntity create(MatterEntity matterEntity) {
    ResponseModel responseModel = matterFeign.create(matterEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<MatterEntity>() {
    });
  }

  @Transactional
  @Override
  public MatterEntity createForm(MatterEntity matterEntity) {
    return null;
  }

  @Transactional
  @Override
  public MatterEntity update(MatterEntity matterEntity) {
    ResponseModel responseModel = matterFeign.update(matterEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<MatterEntity>() {
    });
  }

  @Transactional
  @Override
  public MatterEntity updateForm(MatterEntity matterEntity) {
    return null;
  }

  @Override
  public MatterEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = matterFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<MatterEntity>() {
    });
  }

  @Override
  public MatterEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = matterFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<MatterEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    if (StringUtils.isBlank(id)) {
      return;
    }
    matterFeign.deleteById(id);
  }

  @Override
  public MatterEntity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = matterFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<MatterEntity>() {
    });
  }

  @Override
  public MatterEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = matterFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<MatterEntity>() {
    });
  }

  @Override
  public Page<MatterEntity> findByMatterConditions(Map<String, Object> params, Pageable pageable) {

    ResponseModel responseModel = matterFeign.findByMatterConditions(params, pageable);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Page<MatterEntity>>() {
    });
  }
} 
