package com.tw.aquaculture.plan.service.remote.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.process.PushFryEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.common.vo.reportslist.ReportParamVo;
import com.tw.aquaculture.plan.feign.process.PushFryFeign;
import com.tw.aquaculture.plan.service.remote.PushFryEntityService;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by chenrong on 2020/1/20
 */
@Service
public class PushFryEntityServiceImpl implements PushFryEntityService {

  @Autowired
  private PushFryFeign pushFryFeign;

  @Override
  public List<PushFryEntity> findByPondAndFishTypeAndTimes(String pondId, String fishTypeId, Date startTime, Date endTime) {
    if (StringUtils.isBlank(pondId) || StringUtils.isBlank(fishTypeId) || startTime == null || endTime == null) {
      return Lists.newArrayList();
    }
    ReportParamVo reportParamVo = new ReportParamVo();
    reportParamVo.setPondId(pondId);
    reportParamVo.setFishTypeId(fishTypeId);
    reportParamVo.setStartTime(startTime);
    reportParamVo.setEndTime(endTime);
    ResponseModel responseModel = pushFryFeign.findByPondAndFishTypeAndTimes(reportParamVo);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<PushFryEntity>>() {
    });
  }

}
