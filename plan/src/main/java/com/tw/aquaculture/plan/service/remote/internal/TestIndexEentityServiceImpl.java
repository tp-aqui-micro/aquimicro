package com.tw.aquaculture.plan.service.remote.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.base.TestIndexEentity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.plan.feign.check.TestIndexFeign;
import com.tw.aquaculture.plan.service.remote.TestIndexEentityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * TestIndexEentity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("TestIndexEentityServiceImpl")
public class TestIndexEentityServiceImpl implements TestIndexEentityService {
  @Autowired
  private TestIndexFeign testIndexFeign;

  @Transactional
  @Override
  public TestIndexEentity create(TestIndexEentity testIndexEentity) {
    ResponseModel responseModel = testIndexFeign.create(testIndexEentity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TestIndexEentity>() {
    });
  }

  @Transactional
  @Override
  public TestIndexEentity createForm(TestIndexEentity testIndexEentity) {
    return null;
  }

  @Transactional
  @Override
  public TestIndexEentity update(TestIndexEentity testIndexEentity) {
    ResponseModel responseModel = testIndexFeign.update(testIndexEentity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TestIndexEentity>() {
    });
  }

  @Transactional
  @Override
  public TestIndexEentity updateForm(TestIndexEentity testIndexEentity) {
    return null;
  }

  @Override
  public TestIndexEentity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = testIndexFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TestIndexEentity>() {
    });
  }

  @Override
  public TestIndexEentity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = testIndexFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TestIndexEentity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    if (StringUtils.isBlank(id)) {
      return;
    }
    testIndexFeign.deleteById(id);
  }

  @Override
  public TestIndexEentity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = testIndexFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TestIndexEentity>() {
    });
  }

  @Override
  public TestIndexEentity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = testIndexFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TestIndexEentity>() {
    });
  }
} 
