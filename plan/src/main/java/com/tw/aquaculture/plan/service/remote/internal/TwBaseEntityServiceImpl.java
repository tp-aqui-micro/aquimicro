package com.tw.aquaculture.plan.service.remote.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.plan.feign.process.TwBaseFeign;
import com.tw.aquaculture.plan.service.remote.TwBaseEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

/**
 * TwBaseEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("TwBaseEntityServiceImpl")
public class TwBaseEntityServiceImpl implements TwBaseEntityService {
  @Autowired
  private TwBaseFeign twBaseFeign;

  @Transactional
  @Override
  public TwBaseEntity create(TwBaseEntity twBaseEntity) {
    ResponseModel responseModel = twBaseFeign.create(twBaseEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TwBaseEntity>() {
    });
  }

  @Transactional
  @Override
  public TwBaseEntity createForm(TwBaseEntity twBaseEntity) {
    return null;
  }

  @Transactional
  @Override
  public TwBaseEntity update(TwBaseEntity twBaseEntity) {
    ResponseModel responseModel = twBaseFeign.update(twBaseEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TwBaseEntity>() {
    });
  }

  @Transactional
  @Override
  public TwBaseEntity updateForm(TwBaseEntity twBaseEntity) {
    return null;
  }

  @Override
  public TwBaseEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = twBaseFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TwBaseEntity>() {
    });
  }

  @Override
  public TwBaseEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = twBaseFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TwBaseEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    if (StringUtils.isBlank(id)) {
      return;
    }
    twBaseFeign.deleteById(id);
  }

  @Override
  public TwBaseEntity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = twBaseFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TwBaseEntity>() {
    });
  }

  @Override
  public TwBaseEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = twBaseFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TwBaseEntity>() {
    });
  }

  @Override
  public List<TwBaseEntity> findDetailAll() {

    ResponseModel responseModel = twBaseFeign.findDetailAll();
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<TwBaseEntity>>() {
    });
  }

  @Override
  public TwBaseEntity findDetailsByBaseOrgId(String baseOrgId) {
    if (StringUtils.isBlank(baseOrgId)) {
      return null;
    }
    ResponseModel responseModel = twBaseFeign.findDetailsByBaseOrgId(baseOrgId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<TwBaseEntity>() {
    });
  }

  @Override
  public Page<TwBaseEntity> findByConditions(Pageable pageable, Map<String, Object> params) {
    ResponseModel responseModel = twBaseFeign.findByConditions(pageable, params);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Page<TwBaseEntity>>() {
    });
  }

  @Override
  public List<TwBaseEntity> findAll() {

    ResponseModel responseModel = twBaseFeign.findAll();
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<TwBaseEntity>>() {
    });
  }
} 
