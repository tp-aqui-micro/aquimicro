package com.tw.aquaculture.process.controller;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.process.DividePondEntity;
import com.tw.aquaculture.common.vo.reportslist.DividePondResultVo;
import com.tw.aquaculture.common.vo.reportslist.DividePondParamVo;
import com.tw.aquaculture.process.service.DividePondEntityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * DividePondEntity业务模型的MVC Controller层实现，基于HTTP Restful风格
 *
 * @author saturn
 */
@RestController
@RequestMapping("/v1/dividePondEntitys")
public class DividePondEntityController extends BaseController {
  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(DividePondEntityController.class);
  //通用白名单通过
  private static final String [] PROPERTIES = new String[]{"outPond", "inPond", "fishType", "groupLeader", "workOrder","dividePondItems"};
  @Autowired
  private DividePondEntityService dividePondEntityService;

  /**
   * 相关的创建过程，http接口。请注意该创建过程除了可以创建dividePondEntity中的基本信息以外，还可以对dividePondEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（DividePondEntity）模型的创建操作传入的dividePondEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(value = "相关的创建过程，http接口。请注意该创建过程除了可以创建dividePondEntity中的基本信息以外，还可以对dividePondEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（DividePondEntity）模型的创建操作传入的dividePondEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "")
  public ResponseModel create(@RequestBody @ApiParam(name = "dividePondEntity", value = "相关的创建过程，http接口。请注意该创建过程除了可以创建dividePondEntity中的基本信息以外，还可以对dividePondEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（DividePondEntity）模型的创建操作传入的dividePondEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") DividePondEntity dividePondEntity) {
    try {
      DividePondEntity current = this.dividePondEntityService.create(dividePondEntity);
      return this.buildHttpResultW(current);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（DividePondEntity）的修改操作传入的dividePondEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（DividePondEntity）的修改操作传入的dividePondEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "/update")
  public ResponseModel update(@RequestBody @ApiParam(name = "dividePondEntity", value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（DividePondEntity）的修改操作传入的dividePondEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") DividePondEntity dividePondEntity) {
    try {
      DividePondEntity current = this.dividePondEntityService.update(dividePondEntity);
      return this.buildHttpResultW(current);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照DividePondEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照DividePondEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @GetMapping(value = "/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      DividePondEntity result = this.dividePondEntityService.findDetailsById(id);
      return this.buildHttpResultW(result, PROPERTIES);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照DividePondEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param formInstanceId 表单实例编号
   */
  @ApiOperation(value = "按照DividePondEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @GetMapping(value = "/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") @ApiParam("表单实例编号") String formInstanceId) {
    try {
      DividePondEntity result = this.dividePondEntityService.findDetailsByFormInstanceId(formInstanceId);
      return this.buildHttpResultW(result, PROPERTIES);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照DividePondEntity实体中的（formInstanceId）表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @ApiOperation(value = "按照DividePondEntity实体中的（formInstanceId）表单实例编号进行查询")
  @GetMapping(value = "/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") @ApiParam("表单实例编号") String formInstanceId) {
    try {
      DividePondEntity result = this.dividePondEntityService.findByFormInstanceId(formInstanceId);
      return this.buildHttpResultW(result);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照DividePondEntity的主键编号，查询指定的数据信息（不包括任何关联信息）
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照DividePondEntity的主键编号，查询指定的数据信息（不包括任何关联信息）。")
  @GetMapping(value = "/findById")
  public ResponseModel findById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      DividePondEntity result = this.dividePondEntityService.findById(id);
      return this.buildHttpResultW(result);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照主键进行信息的真删除
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照主键进行信息的真删除。")
  @GetMapping(value = "/deleteById")
  public void deleteById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      this.dividePondEntityService.deleteById(id);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
    }
  }

  /**
   * 根据塘口和时间区间查询转入塘口的分塘记录
   *
   * @param pondId    塘口主键
   * @param startTime 转入塘口开始时间
   * @param endTime   转入塘口结束时间
   * @return
   */
  @ApiOperation(value = "根据塘口和时间区间查询转入塘口的分塘记录")
  @GetMapping(value = "/findByInPondAndTimes")
  public ResponseModel findByInPondAndTimes(@RequestParam("pondId") @ApiParam("塘口主键") String pondId,
                                             @ApiParam("转入塘口开始时间") Date startTime,
                                              @ApiParam("转入塘口结束时间") Date endTime) {
    try {
      List<DividePondEntity> result = this.dividePondEntityService.findByInPondAndTimes(pondId, startTime, endTime);
      String [] properties = new String[]{"inPond"};
      return this.buildHttpResultW(result, properties);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 根据塘口和时间区间查询转出塘口的分塘记录
   *
   * @param pondId    塘口主键
   * @param startTime 转出塘口开始时间
   * @param endTime   转出塘口开始时间
   * @return
   */
  @ApiOperation(value = "根据塘口和时间区间查询转出塘口的分塘记录")
  @GetMapping(value = "/findByOutPondAndTimes")
  public ResponseModel findByOutPondAndTimes(@RequestParam("pondId") @ApiParam("塘口主键") String pondId,
                                             @ApiParam("转出塘口开始时间") Date startTime,
                                             @ApiParam("转出塘口开始时间") Date endTime) {
    try {
      List<DividePondEntity> result = this.dividePondEntityService.findByOutPondAndTimes(pondId, startTime, endTime);
      String [] properties = new String[]{"outPond"};
      return this.buildHttpResultW(result, properties);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "转分塘报表")
  @PostMapping(value = "/dividePondReport")
  public ResponseModel dividePondReport(@RequestBody DividePondParamVo dividePondParamVo) {
    try {
      List<DividePondResultVo> result = this.dividePondEntityService.dividePondReport(dividePondParamVo);
      return this.buildHttpResult(result);
    } catch (RuntimeException e) {
      LOGGER.error("", e);
      return this.buildHttpResultForException(e);
    }
  }

}
