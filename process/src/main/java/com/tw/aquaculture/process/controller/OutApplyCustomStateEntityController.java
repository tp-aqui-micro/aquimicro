package com.tw.aquaculture.process.controller;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.process.OutApplyCustomStateEntity;
import com.tw.aquaculture.process.service.OutApplyCustomStateEntityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * OutApplyCustomStateEntity 业务模型的MVC Controller层实现，基于HTTP Restful风格
 * @author fangfu.luo
 * @date 2019/12/10
 */
@RestController
@RequestMapping("/v1/outApplyCustomStateEntitys")
public class OutApplyCustomStateEntityController extends BaseController {
  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(OutApplyCustomStateEntityController.class);
  /**
   * 白名单配置列
   */
  private static final String OUT_FISH_APPLY = "outFishApply";
  private static final String CUSTOMER = "customer";
  @Autowired
  private OutApplyCustomStateEntityService outApplyCustomStateEntityService;
  /**
   * 创建一个新的OutApplyCustomStateEntity模型对象（包括了可能的第三方系统调用、复杂逻辑处理等）
   */
  @ApiOperation(value = "创建一个新的OutApplyCustomStateEntity模型对象（包括了可能的第三方系统调用、复杂逻辑处理等）")
  @PostMapping(value="")
  public ResponseModel create(@RequestBody @ApiParam(name="outApplyCustomStateEntity" , value="相关的创建过程，http接口。请注意该更新过程只会创建在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其创建过程注意：基于模型（outApplyCustomStateEntity）的修改操作传入的outApplyCustomStateEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")  OutApplyCustomStateEntity outApplyCustomStateEntity){
    try {
      OutApplyCustomStateEntity current = this.outApplyCustomStateEntityService.create(outApplyCustomStateEntity);
      return this.buildHttpResultW(current);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（OutApplyCustomStateEntity）的修改操作传入的OutApplyCustomStateEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   * */
  @ApiOperation(value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（outApplyCustomStateEntity）的修改操作传入的outApplyCustomStateEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value="/update")
  public ResponseModel update(@RequestBody @ApiParam(name="outApplyCustomStateEntity" , value="相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（outApplyCustomStateEntity）的修改操作传入的outApplyCustomStateEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") OutApplyCustomStateEntity outApplyCustomStateEntity) {
    try {
      OutApplyCustomStateEntity current = this.outApplyCustomStateEntityService.update(outApplyCustomStateEntity);
      return this.buildHttpResultW(current);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照关联的 岀鱼申请id进行详情查询（包括关联信息）
   * @param outFishApply 关联的 岀鱼申请id
   */
  @ApiOperation(value = "按照关联的 岀鱼申请id进行详情查询（包括关联信息）")
  @GetMapping(value="/findDetailsByOutFishApply")
  public ResponseModel findDetailsByOutFishApply(@RequestParam("outFishApply") @ApiParam("关联的 岀鱼申请id") String outFishApply){
    try {
      OutApplyCustomStateEntity result = this.outApplyCustomStateEntityService.findDetailsByOutFishApply(outFishApply);
      String [] properties = new String[]{OUT_FISH_APPLY};
      return this.buildHttpResultW(result,properties );
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }
  /**
   * 按照关联的 客户进行详情查询（包括关联信息）
   * @param customer 关联的 客户
   */
  @ApiOperation(value = "按照关联的 客户进行详情查询（包括关联信息）")
  @GetMapping(value="/findDetailsByCustomer")
  public ResponseModel findDetailsByCustomer(@RequestParam(name = "customer")  @ApiParam("关联的 客户") String customer){
    try {
      OutApplyCustomStateEntity result = this.outApplyCustomStateEntityService.findDetailsByCustomer(customer);
      String [] properties = new String[]{CUSTOMER};
      return this.buildHttpResultW(result,properties);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }
  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   */
  @ApiOperation(value = "按照关联的 客户进行详情查询（包括关联信息）")
  @GetMapping(value="/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam(name = "id") @ApiParam("主键")  String id){
    try {
      OutApplyCustomStateEntity result = this.outApplyCustomStateEntityService.findDetailsById(id);
      return this.buildHttpResultW(result);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }
  /**
   * 按照OutApplyCustomStateEntity的主键编号，查询指定的数据信息（不包括任何关联信息）
   * @param id 主键
   * */
  @ApiOperation(value = "按照OutApplyCustomStateEntity的主键编号，查询指定的数据信息（不包括任何关联信息）")
  @GetMapping(value="/findById")
  public ResponseModel findById(@RequestParam(name = "id") @ApiParam("主键")  String id){
    try {
      OutApplyCustomStateEntity result = this.outApplyCustomStateEntityService.findById(id);
      return this.buildHttpResultW(result);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }
  /**
   *  按照主键进行信息的真删除 
   * @param id 主键 
   */
  @ApiOperation(value = "按照主键进行信息的真删除")
  @GetMapping(value="/deleteById")
  public void deleteById(@RequestParam(name = "id") @ApiParam("主键")  String id){
    try {
      this.outApplyCustomStateEntityService.deleteById(id);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
    }
  }

  /**
   * 根据岀鱼申请id查询
   * @param outFishApplyId 出鱼主键
   * @param state 状态
   * @return
   */
  @ApiOperation(value = "根据岀鱼申请id查询")
  @GetMapping(value="/findByOutFishApplyIdAndState")
  public ResponseModel findByOutFishApplyIdAndState(  @RequestParam("outFishApplyId") @ApiParam("出鱼主键") String outFishApplyId,
                                                      @ApiParam("状态") int state){
    try {
      List<OutApplyCustomStateEntity> result = this.outApplyCustomStateEntityService.findByOutFishApplyIdAndState(outFishApplyId,state);
      String [] properties = new String[]{OUT_FISH_APPLY};
      return this.buildHttpResultW(result,properties);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 根据岀鱼申请id查询
   * @param outFishApplyId
   * @return
   */
  @ApiOperation(value = "根据岀鱼申请id查询")
  @GetMapping(value="/findByOutFishApply")
  public ResponseModel findByOutFishApply(  @RequestParam("outFishApplyId") @ApiParam("出鱼主键") String outFishApplyId){
    try{
      List<OutApplyCustomStateEntity> result =  this.outApplyCustomStateEntityService.findByOutFishApply(outFishApplyId);
      String [] properties = new String[]{OUT_FISH_APPLY,CUSTOMER};
      return this.buildHttpResultW(result, properties);
    }catch (RuntimeException e){
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

}