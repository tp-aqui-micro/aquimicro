package com.tw.aquaculture.process.controller;

import com.alibaba.fastjson.JSONObject;
import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.process.OutFishApplyEntity;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.common.vo.outfishapply.OutFishApplyVo;
import com.tw.aquaculture.common.vo.outfishapply.PageParam;
import com.tw.aquaculture.common.vo.outfishapply.PageResult;
import com.tw.aquaculture.process.service.OutFishApplyEntityService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * OutFishApplyEntity业务模型的MVC Controller层实现，基于HTTP Restful风格
 *
 * @author saturn
 */
@RestController
@RequestMapping("/v1/outFishApplyEntitys")
@ApiModel("出鱼申请")
public class OutFishApplyEntityController extends BaseController {
  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(OutFishApplyEntityController.class);

  @Autowired
  private OutFishApplyEntityService outFishApplyEntityService;

  /**
   * 相关的创建过程，http接口。请注意该创建过程除了可以创建outFishApplyEntity中的基本信息以外，还可以对outFishApplyEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（OutFishApplyEntity）模型的创建操作传入的outFishApplyEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(value = "相关的创建过程，http接口。请注意该创建过程除了可以创建outFishApplyEntity中的基本信息以外，还可以对outFishApplyEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（OutFishApplyEntity）模型的创建操作传入的outFishApplyEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "")
  public ResponseModel create(@RequestBody @ApiParam(name = "outFishApplyEntity", value = "相关的创建过程，http接口。请注意该创建过程除了可以创建outFishApplyEntity中的基本信息以外，还可以对outFishApplyEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（OutFishApplyEntity）模型的创建操作传入的outFishApplyEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") OutFishApplyEntity outFishApplyEntity) {
    try {
      OutFishApplyEntity current = this.outFishApplyEntityService.create(outFishApplyEntity);
      return this.buildHttpResultW(current);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（OutFishApplyEntity）的修改操作传入的outFishApplyEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（OutFishApplyEntity）的修改操作传入的outFishApplyEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "/update")
  public ResponseModel update(@RequestBody @ApiParam(name = "outFishApplyEntity", value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（OutFishApplyEntity）的修改操作传入的outFishApplyEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") OutFishApplyEntity outFishApplyEntity) {
    try {
      OutFishApplyEntity current = this.outFishApplyEntityService.update(outFishApplyEntity);
      return this.buildHttpResultW(current);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照OutFishApplyEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照OutFishApplyEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @GetMapping(value = "/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      OutFishApplyEntity result = this.outFishApplyEntityService.findDetailsById(id);
      String [] properties = new String[]{"pond", "pond.base", "workOrder", "remainInfos", "customerInfos", "remainInfos.fishType"};
      return this.buildHttpResultW(result,properties );
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照OutFishApplyEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param formInstanceId 表单实例编号
   */
  @ApiOperation(value = "按照OutFishApplyEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @GetMapping(value = "/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") @ApiParam("表单实例编号") String formInstanceId) {
    try {
      OutFishApplyEntity result = this.outFishApplyEntityService.findDetailsByFormInstanceId(formInstanceId);
      String [] properties =  new String[]{"pond", "pond.base", "workOrder", "remainInfos", "customerInfos", "remainInfos.fishType"};
      return this.buildHttpResultW(result,properties);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照OutFishApplyEntity实体中的（formInstanceId）表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @ApiOperation(value = "按照OutFishApplyEntity实体中的（formInstanceId）表单实例编号进行查询")
  @GetMapping(value = "/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") @ApiParam("表单实例编号") String formInstanceId) {
    try {
      OutFishApplyEntity result = this.outFishApplyEntityService.findByFormInstanceId(formInstanceId);
      return this.buildHttpResultW(result);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照OutFishApplyEntity的主键编号，查询指定的数据信息（不包括任何关联信息）
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照OutFishApplyEntity的主键编号，查询指定的数据信息（不包括任何关联信息）。")
  @GetMapping(value = "/findById")
  public ResponseModel findById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      OutFishApplyEntity result = this.outFishApplyEntityService.findById(id);
      return this.buildHttpResultW(result);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照主键进行信息的真删除
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照主键进行信息的真删除。")
  @GetMapping(value = "/deleteById")
  public void deleteById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      this.outFishApplyEntityService.deleteById(id);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
    }
  }

  @ApiOperation(value = "修改审批状态")
  @GetMapping(value = "/updateResureState")
  public ResponseModel updateResureState(String id, Integer resureState, String message) {
    try {
      this.outFishApplyEntityService.updateResureState(id, resureState, message);
      return this.buildHttpResult();
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "重新发起岀鱼申请审批")
  @GetMapping(value = "/reApply")
  public ResponseModel reApply(@RequestParam("id") String id) {
    try {
      String result = this.outFishApplyEntityService.reApply(id);
      return this.buildHttpResult(result);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 移动端根据基地名称、塘口名称、时间分页查询出鱼申请信息
   *
   * @return
   */
  @ApiOperation(value = "移动端根据基地名称、塘口名称、时间分页查询出鱼申请信息")
  @GetMapping("/findDetailByOrgBaseNameOrPondNameOrTiem")
  public String findDetailByOrgBaseNameAndPondName(String pondId, String timeBeginStr, String endBeginStr, PageParam pageable) {
    try {
      Date timeBegin = DateUtils.dateParse(timeBeginStr, DateUtils.TO_DAY_TIME);
      Date endBegin = DateUtils.dateParse(endBeginStr, DateUtils.TO_DAY_TIME);
      Map<String, Object> params = new HashMap<>();
      params.put("pondId", pondId);
      params.put("timeBegin", timeBegin);
      params.put("endBegin", endBegin);
      PageResult<List<OutFishApplyVo>> result = this.outFishApplyEntityService.findDetailByOrgBaseNameOrPondNameOrTiem(params, pageable);
      return JSONObject.toJSONString(result);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return null;
    }
  }


} 
