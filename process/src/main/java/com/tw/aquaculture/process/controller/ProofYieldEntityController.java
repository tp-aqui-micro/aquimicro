package com.tw.aquaculture.process.controller;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.process.ProofYieldEntity;
import com.tw.aquaculture.common.vo.reportslist.ExistAmountReportVo;
import com.tw.aquaculture.common.vo.reportslist.ReportParamVo;
import com.tw.aquaculture.process.service.ProofYieldEntityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ProofYieldEntity业务模型的MVC Controller层实现，基于HTTP Restful风格
 * @author saturn
 */
@RestController
@RequestMapping("/v1/proofYieldEntitys")
public class ProofYieldEntityController extends BaseController { 
  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ProofYieldEntityController.class);
  /**
   * 查询通用白名单
   */
  private static final String WORK_ORDER = "workOrder";
  private static final String [] PROPERTIES = new String[]{"pond","pond.base","fishType","groupLeader","lastYield","proofYieldItems",WORK_ORDER};
  private static final String [] PROPERTIES2 = new String[]{"pond",WORK_ORDER,"fishType"};
  private static final String [] PROPERTIES3 = new String[]{"pond",WORK_ORDER};
  @Autowired
  private ProofYieldEntityService proofYieldEntityService;

  /**
   * 相关的创建过程，http接口。请注意该创建过程除了可以创建proofYieldEntity中的基本信息以外，还可以对proofYieldEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（ProofYieldEntity）模型的创建操作传入的proofYieldEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   * */
  @ApiOperation(value = "相关的创建过程，http接口。请注意该创建过程除了可以创建proofYieldEntity中的基本信息以外，还可以对proofYieldEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（ProofYieldEntity）模型的创建操作传入的proofYieldEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value="")
  public ResponseModel create(@RequestBody @ApiParam(name="proofYieldEntity" , value="相关的创建过程，http接口。请注意该创建过程除了可以创建proofYieldEntity中的基本信息以外，还可以对proofYieldEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（ProofYieldEntity）模型的创建操作传入的proofYieldEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") ProofYieldEntity proofYieldEntity) {
    try {
      ProofYieldEntity current = this.proofYieldEntityService.create(proofYieldEntity);
      return this.buildHttpResultW(current);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（ProofYieldEntity）的修改操作传入的proofYieldEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   * */
  @ApiOperation(value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（ProofYieldEntity）的修改操作传入的proofYieldEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value="/update")
  public ResponseModel update(@RequestBody @ApiParam(name="proofYieldEntity" , value="相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（ProofYieldEntity）的修改操作传入的proofYieldEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") ProofYieldEntity proofYieldEntity) {
    try {
      ProofYieldEntity current = this.proofYieldEntityService.update(proofYieldEntity);
      return this.buildHttpResultW(current);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照ProofYieldEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   * @param id 主键
   */
  @ApiOperation(value = "按照ProofYieldEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @GetMapping(value="/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") @ApiParam("主键") String id) {
    try { 
      ProofYieldEntity result = this.proofYieldEntityService.findDetailsById(id);
      return this.buildHttpResultW(result,PROPERTIES );
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    } 
  }  
  /**
   * 按照ProofYieldEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   * @param formInstanceId 表单实例编号
   */
  @ApiOperation(value = "按照ProofYieldEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @GetMapping(value="/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") @ApiParam("表单实例编号") String formInstanceId) {
    try { 
      ProofYieldEntity result = this.proofYieldEntityService.findDetailsByFormInstanceId(formInstanceId);
      return this.buildHttpResultW(result,PROPERTIES);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    } 
  }  
  /**
   * 按照ProofYieldEntity实体中的（formInstanceId）表单实例编号进行查询
   * @param formInstanceId 表单实例编号
   */
  @ApiOperation(value = "按照ProofYieldEntity实体中的（formInstanceId）表单实例编号进行查询")
  @GetMapping(value="/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") @ApiParam("表单实例编号") String formInstanceId) {
    try { 
      ProofYieldEntity result = this.proofYieldEntityService.findByFormInstanceId(formInstanceId);
      return this.buildHttpResultW(result);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    } 
  }
  /**
   * 按照ProofYieldEntity的主键编号，查询指定的数据信息（不包括任何关联信息）
   * @param id 主键
   * */
  @ApiOperation(value = "按照ProofYieldEntity的主键编号，查询指定的数据信息（不包括任何关联信息）。")
  @GetMapping(value="/findById")
  public ResponseModel findById(@RequestParam("id") @ApiParam("主键") String id){
    try {
      ProofYieldEntity result = this.proofYieldEntityService.findById(id);
      return this.buildHttpResultW(result);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }
  /**
   *  按照主键进行信息的真删除
   * @param id 主键
   */
  @ApiOperation(value = "按照主键进行信息的真删除。")
  @GetMapping(value="/deleteById")
  public void deleteById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      this.proofYieldEntityService.deleteById(id);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
    }
  }
  /**
   * 本周期内最后一次打样测产数据
   * @param pondId 塘口id
   * @return
   */
  @ApiOperation(value = "本周期内最后一次打样测产数据。")
  @GetMapping(value="/findLastProofYield")
  public ResponseModel findLastProofYield(@RequestParam("pondId") @ApiParam("塘口id") String pondId){
    try {
      ProofYieldEntity result = this.proofYieldEntityService.findLastProofYield(pondId);
      return this.buildHttpResultW(result,PROPERTIES3 );
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 查询上次存塘量
   * @param pondId
   * @return
   */
  @ApiOperation(value = "查询上次存塘量。")
  @GetMapping(value="/computeLastExitCount")
  public ResponseModel computeLastExitCount(@RequestParam("pondId") @ApiParam("塘口id") String pondId){
    try {
      double result = this.proofYieldEntityService.computeLastExitCount(pondId);
      return this.buildHttpResult(result);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 本周期内某鱼种上次打样测产数据
   * @param pondId 塘口ID
   * @param fishTypeId 鱼类型
   * @return
   */
  @ApiOperation(value = "本周期内某鱼种上次打样测产数据。")
  @GetMapping(value="/findLastProofYieldByFishType")
  public ResponseModel findLastProofYieldByFishType(@RequestParam("pondId") @ApiParam("塘口id") String pondId,
                                                    @RequestParam("fishTypeId") @ApiParam("鱼类") String fishTypeId){
    try {
      ProofYieldEntity result = this.proofYieldEntityService.findLastProofYieldByFishType(pondId,fishTypeId);
      return this.buildHttpResultW(result, PROPERTIES2);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 根据塘口和工单号查询
   * @param pondId 塘口主键
   * @param workOrderId 工单主键
   * @return
   */
  @ApiOperation(value = "本周期内某鱼种上次打样测产数据。")
  @GetMapping(value="/findDetailsByPondAndWorkOrder" )
  public ResponseModel findDetailsByPondAndWorkOrder(@RequestParam("pondId") @ApiParam("塘口id") String pondId,
                                                     @RequestParam("workOrderId") @ApiParam("工单主键") String workOrderId){
    try {
      List<ProofYieldEntity> result = this.proofYieldEntityService.findDetailsByPondAndWorkOrder(pondId,workOrderId);
      return this.buildHttpResultW(result, PROPERTIES2);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据塘口和鱼种查询本周期内存塘量。")
  @GetMapping(value="/findExitCountByPondAndFishType")
  public ResponseModel findExitCountByPondAndFishType(@RequestParam("pondId") String pondId, @RequestParam("fishTypeId") String fishTypeId){
    try {
      double result = this.proofYieldEntityService.findExitCountByPondAndFishType(pondId, fishTypeId);
      return this.buildHttpResult(result);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "存塘量报表")
  @PostMapping(value = "/existAmountReport")
  public ResponseModel existAmountReport(@RequestBody ReportParamVo reportParamVo) {
    try {
      List<ExistAmountReportVo> result = this.proofYieldEntityService.existAmountReport(reportParamVo);
      return this.buildHttpResult(result);
    } catch (RuntimeException e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

} 
