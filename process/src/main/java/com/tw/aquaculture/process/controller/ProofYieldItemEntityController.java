package com.tw.aquaculture.process.controller;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.process.ProofYieldItemEntity;
import com.tw.aquaculture.process.service.ProofYieldItemEntityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * ProofYieldItemEntity业务模型的MVC Controller层实现，基于HTTP Restful风格
 *
 * @author saturn
 */
@RestController
@RequestMapping("/v1/proofYieldItemEntitys")
public class ProofYieldItemEntityController extends BaseController {
  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ProofYieldItemEntityController.class);
  private static final String [] PROPERTIES = new String[]{"proofYield"};
  @Autowired
  private ProofYieldItemEntityService proofYieldItemEntityService;

  /**
   * 相关的创建过程，http接口。请注意该创建过程除了可以创建proofYieldItemEntity中的基本信息以外，还可以对proofYieldItemEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（ProofYieldItemEntity）模型的创建操作传入的proofYieldItemEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(value = "相关的创建过程，http接口。请注意该创建过程除了可以创建proofYieldItemEntity中的基本信息以外，还可以对proofYieldItemEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（ProofYieldItemEntity）模型的创建操作传入的proofYieldItemEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "")
  public ResponseModel create(@RequestBody @ApiParam(name = "proofYieldItemEntity", value = "相关的创建过程，http接口。请注意该创建过程除了可以创建proofYieldItemEntity中的基本信息以外，还可以对proofYieldItemEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（ProofYieldItemEntity）模型的创建操作传入的proofYieldItemEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") ProofYieldItemEntity proofYieldItemEntity) {
    try {
      ProofYieldItemEntity current = this.proofYieldItemEntityService.create(proofYieldItemEntity);
      return this.buildHttpResultW(current);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（ProofYieldItemEntity）的修改操作传入的proofYieldItemEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（ProofYieldItemEntity）的修改操作传入的proofYieldItemEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "/update")
  public ResponseModel update(@RequestBody @ApiParam(name = "proofYieldItemEntity", value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（ProofYieldItemEntity）的修改操作传入的proofYieldItemEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") ProofYieldItemEntity proofYieldItemEntity) {
    try {
      ProofYieldItemEntity current = this.proofYieldItemEntityService.update(proofYieldItemEntity);
      return this.buildHttpResultW(current);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照ProofYieldItemEntity实体中的（proofYield）关联的 打样估产进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param proofYield 关联的 打样估产
   */
  @ApiOperation(value = "按照ProofYieldItemEntity实体中的（proofYield）关联的 打样估产进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @GetMapping(value = "/findDetailsByProofYield")
  public ResponseModel findDetailsByProofYield(@RequestParam("proofYield") @ApiParam("关联的 打样估产") String proofYield) {
    try {
      Set<ProofYieldItemEntity> result = this.proofYieldItemEntityService.findDetailsByProofYield(proofYield);
      return this.buildHttpResultW(result, PROPERTIES);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照ProofYieldItemEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照ProofYieldItemEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @GetMapping(value = "/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      ProofYieldItemEntity result = this.proofYieldItemEntityService.findDetailsById(id);
      return this.buildHttpResultW(result,PROPERTIES );
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照ProofYieldItemEntity的主键编号，查询指定的数据信息（不包括任何关联信息）
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照ProofYieldItemEntity的主键编号，查询指定的数据信息（不包括任何关联信息）。")
  @GetMapping(value = "/findById")
  public ResponseModel findById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      ProofYieldItemEntity result = this.proofYieldItemEntityService.findById(id);
      return this.buildHttpResultW(result);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照主键进行信息的真删除
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照主键进行信息的真删除。")
  @GetMapping(value = "/deleteById")
  public void deleteById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      this.proofYieldItemEntityService.deleteById(id);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
    }
  }
} 
