package com.tw.aquaculture.process.controller;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.process.PushFeedEntity;
import com.tw.aquaculture.common.vo.reportslist.PushFeedReportVo;
import com.tw.aquaculture.common.vo.reportslist.ReportParamVo;
import com.tw.aquaculture.process.service.PushFeedEntityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * PushFeedEntity业务模型的MVC Controller层实现，基于HTTP Restful风格
 * @author saturn
 */
@RestController
@RequestMapping("/v1/pushFeedEntitys")
public class PushFeedEntityController extends BaseController { 
  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(PushFeedEntityController.class);
  
  @Autowired
  private PushFeedEntityService pushFeedEntityService;

  /**
   * 相关的创建过程，http接口。请注意该创建过程除了可以创建pushFeedEntity中的基本信息以外，还可以对pushFeedEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（PushFeedEntity）模型的创建操作传入的pushFeedEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   * */
  @ApiOperation(value = "相关的创建过程，http接口。请注意该创建过程除了可以创建pushFeedEntity中的基本信息以外，还可以对pushFeedEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（PushFeedEntity）模型的创建操作传入的pushFeedEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value="")
  public ResponseModel create(@RequestBody @ApiParam(name="pushFeedEntity" , value="相关的创建过程，http接口。请注意该创建过程除了可以创建pushFeedEntity中的基本信息以外，还可以对pushFeedEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（PushFeedEntity）模型的创建操作传入的pushFeedEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") PushFeedEntity pushFeedEntity) {
    try {
      PushFeedEntity current = this.pushFeedEntityService.create(pushFeedEntity);
      return this.buildHttpResultW(current);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（PushFeedEntity）的修改操作传入的pushFeedEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   * */
  @ApiOperation(value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（PushFeedEntity）的修改操作传入的pushFeedEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value="/update")
  public ResponseModel update(@RequestBody @ApiParam(name="pushFeedEntity" , value="相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（PushFeedEntity）的修改操作传入的pushFeedEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") PushFeedEntity pushFeedEntity) {
    try {
      PushFeedEntity current = this.pushFeedEntityService.update(pushFeedEntity);
      return this.buildHttpResultW(current);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照PushFeedEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   * @param id 主键
   */
  @ApiOperation(value = "按照PushFeedEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @GetMapping(value="/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") @ApiParam("主键") String id) {
    try { 
      PushFeedEntity result = this.pushFeedEntityService.findDetailsById(id);
      String [] properties = new String[]{"workOrder","pond","pond.base"};
      return this.buildHttpResultW(result,properties);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    } 
  }  
  /**
   * 按照PushFeedEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   * @param formInstanceId 表单实例编号
   */
  @ApiOperation(value = "按照PushFeedEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @GetMapping(value="/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") @ApiParam("表单实例编号") String formInstanceId) {
    try { 
      PushFeedEntity result = this.pushFeedEntityService.findDetailsByFormInstanceId(formInstanceId);
      String [] properties = new String[]{"workOrder","pond","pond.base", "store", "feed"};
      return this.buildHttpResultW(result, properties);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    } 
  }  
  /**
   * 按照PushFeedEntity实体中的（formInstanceId）表单实例编号进行查询
   * @param formInstanceId 表单实例编号
   */
  @ApiOperation(value = "按照PushFeedEntity实体中的（formInstanceId）表单实例编号进行查询")
  @GetMapping(value="/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") @ApiParam("表单实例编号") String formInstanceId) {
    try { 
      PushFeedEntity result = this.pushFeedEntityService.findByFormInstanceId(formInstanceId);
      return this.buildHttpResultW(result);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    } 
  }
  /**
   * 按照CleanPondEntity的主键编号，查询指定的数据信息（不包括任何关联信息）
   * @param id 主键
   * */
  @ApiOperation(value = "按照PushFeedEntity的主键编号，查询指定的数据信息（不包括任何关联信息）。")
  @GetMapping(value="/findById")
  public ResponseModel findById(@RequestParam("id") @ApiParam("主键") String id){
    try {
      PushFeedEntity result = this.pushFeedEntityService.findById(id);
      return this.buildHttpResultW(result);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }
  /**
   *  按照主键进行信息的真删除
   * @param id 主键
   */
  @ApiOperation(value = "按照主键进行信息的真删除。")
  @GetMapping(value="/deleteById")
  public void deleteById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      this.pushFeedEntityService.deleteById(id);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
    }
  }
  /**
   * 统计相同饲料在某时间段内消耗量
   * @param baseId
   * @param matterId
   * @param start
   * @param end
   * @return
   */
  @ApiOperation(value = "统计相同饲料在某时间段内消耗量")
  @GetMapping(value="/sumConsumeByTimes")
  public ResponseModel sumConsumeByTimes(@RequestParam("baseId") String baseId, @RequestParam("matterId") String matterId, @RequestParam("start") Date start, @RequestParam("end") Date end){
    try {
      Double result = this.pushFeedEntityService.sumConsumeByTimes(baseId, matterId, start, end);
      return this.buildHttpResult(result);
    } catch(Exception e) {
      LOGGER.error(e.getMessage() , e);
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "饲料投喂报表")
  @PostMapping(value = "/pushFeedReport")
  public ResponseModel pushFeedReport(@RequestBody ReportParamVo reportParamVo) {
    try {
      List<PushFeedReportVo> result = this.pushFeedEntityService.pushFeedReport(reportParamVo);
      return this.buildHttpResult(result);
    } catch (RuntimeException e) {
      return this.buildHttpResultForException(e);
    }
  }

}
