package com.tw.aquaculture.process.controller;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.process.RemainInfoEntity;
import com.tw.aquaculture.process.service.RemainInfoEntityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * RemainInfoEntity业务模型的MVC Controller层实现，基于HTTP Restful风格
 *
 * @author saturn
 */
@RestController
@RequestMapping("/v1/remainInfoEntitys")
public class RemainInfoEntityController extends BaseController {
  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(RemainInfoEntityController.class);
  private static final String [] PROPERTIES =  new String[]{"fishType", "outFishApply"};
  @Autowired
  private RemainInfoEntityService remainInfoEntityService;

  /**
   * 相关的创建过程，http接口。请注意该创建过程除了可以创建remainInfoEntity中的基本信息以外，还可以对remainInfoEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（RemainInfoEntity）模型的创建操作传入的remainInfoEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(value = "相关的创建过程，http接口。请注意该创建过程除了可以创建remainInfoEntity中的基本信息以外，还可以对remainInfoEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（RemainInfoEntity）模型的创建操作传入的remainInfoEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "")
  public ResponseModel create(@RequestBody @ApiParam(name = "remainInfoEntity", value = "相关的创建过程，http接口。请注意该创建过程除了可以创建remainInfoEntity中的基本信息以外，还可以对remainInfoEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（RemainInfoEntity）模型的创建操作传入的remainInfoEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") RemainInfoEntity remainInfoEntity) {
    try {
      RemainInfoEntity current = this.remainInfoEntityService.create(remainInfoEntity);
      return this.buildHttpResultW(current);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（RemainInfoEntity）的修改操作传入的remainInfoEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（RemainInfoEntity）的修改操作传入的remainInfoEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "/update")
  public ResponseModel update(@RequestBody @ApiParam(name = "remainInfoEntity", value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（RemainInfoEntity）的修改操作传入的remainInfoEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") RemainInfoEntity remainInfoEntity) {
    try {
      RemainInfoEntity current = this.remainInfoEntityService.update(remainInfoEntity);
      return this.buildHttpResultW(current);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照RemainInfoEntity实体中的（fishType）关联的 鱼种进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param fishType 关联的 鱼种
   */
  @ApiOperation(value = "按照RemainInfoEntity实体中的（fishType）关联的 鱼种进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @GetMapping(value = "/findDetailsByFishType")
  public ResponseModel findDetailsByFishType(@RequestParam("fishType") @ApiParam("关联的 鱼种") String fishType) {
    try {
      Set<RemainInfoEntity> result = this.remainInfoEntityService.findDetailsByFishType(fishType);
      return this.buildHttpResultW(result,PROPERTIES);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照RemainInfoEntity实体中的（outFishApply）关联的 出鱼申请进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param outFishApply 关联的 出鱼申请
   */
  @ApiOperation(value = "按照RemainInfoEntity实体中的（outFishApply）关联的 出鱼申请进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @GetMapping(value = "/findDetailsByOutFishApply")
  public ResponseModel findDetailsByOutFishApply(@RequestParam("outFishApply") @ApiParam("关联的 出鱼申请") String outFishApply) {
    try {
      Set<RemainInfoEntity> result = this.remainInfoEntityService.findDetailsByOutFishApply(outFishApply);
      return this.buildHttpResultW(result, PROPERTIES);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照RemainInfoEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照RemainInfoEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @GetMapping(value = "/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      RemainInfoEntity result = this.remainInfoEntityService.findDetailsById(id);
      return this.buildHttpResultW(result,PROPERTIES);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照RemainInfoEntity的主键编号，查询指定的数据信息（不包括任何关联信息）
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照RemainInfoEntity的主键编号，查询指定的数据信息（不包括任何关联信息）。")
  @GetMapping(value = "/findById")
  public ResponseModel findById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      RemainInfoEntity result = this.remainInfoEntityService.findById(id);
      return this.buildHttpResultW(result);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照主键进行信息的真删除
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照主键进行信息的真删除。")
  @GetMapping(value = "/deleteById")
  public void deleteById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      this.remainInfoEntityService.deleteById(id);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
    }
  }
} 
