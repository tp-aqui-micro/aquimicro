package com.tw.aquaculture.process.controller;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import com.tw.aquaculture.process.service.WorkOrderEntityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * WorkOrderEntity业务模型的MVC Controller层实现，基于HTTP Restful风格
 *
 * @author saturn
 */
@RestController
@RequestMapping("/v1/workOrderEntitys")
public class WorkOrderEntityController extends BaseController {
  /**
   * 日志
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(WorkOrderEntityController.class);
  /**
   * 通用白名单配置
   */
  private static final String [] PROPERTIES = new String[]{"pond","pond.base"};
  private static final String [] PROPERTIES2 = new String[]{"pond"};
  @Autowired
  private WorkOrderEntityService workOrderEntityService;

  /**
   * 相关的创建过程，http接口。请注意该创建过程除了可以创建workOrderEntity中的基本信息以外，还可以对workOrderEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（WorkOrderEntity）模型的创建操作传入的workOrderEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(value = "相关的创建过程，http接口。请注意该创建过程除了可以创建workOrderEntity中的基本信息以外，还可以对workOrderEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（WorkOrderEntity）模型的创建操作传入的workOrderEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "")
  public ResponseModel create(@RequestBody @ApiParam(name = "workOrderEntity", value = "相关的创建过程，http接口。请注意该创建过程除了可以创建workOrderEntity中的基本信息以外，还可以对workOrderEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（WorkOrderEntity）模型的创建操作传入的workOrderEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") WorkOrderEntity workOrderEntity) {
    try {
      WorkOrderEntity current = this.workOrderEntityService.create(workOrderEntity);
      return this.buildHttpResultW(current);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（WorkOrderEntity）的修改操作传入的workOrderEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（WorkOrderEntity）的修改操作传入的workOrderEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "/update")
  public ResponseModel update(@RequestBody @ApiParam(name = "workOrderEntity", value = "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（WorkOrderEntity）的修改操作传入的workOrderEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）") WorkOrderEntity workOrderEntity) {
    try {
      WorkOrderEntity current = this.workOrderEntityService.update(workOrderEntity);
      return this.buildHttpResultW(current);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照WorkOrderEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照WorkOrderEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @GetMapping(value = "/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      WorkOrderEntity result = this.workOrderEntityService.findDetailsById(id);
      return this.buildHttpResultW(result, PROPERTIES);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照WorkOrderEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param formInstanceId 表单实例编号
   */
  @ApiOperation(value = "按照WorkOrderEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @GetMapping(value = "/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") @ApiParam("表单实例编号") String formInstanceId) {
    try {
      WorkOrderEntity result = this.workOrderEntityService.findDetailsByFormInstanceId(formInstanceId);
      return this.buildHttpResultW(result, PROPERTIES);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照WorkOrderEntity实体中的（formInstanceId）表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @ApiOperation(value = "按照WorkOrderEntity实体中的（formInstanceId）表单实例编号进行查询")
  @GetMapping(value = "/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") @ApiParam("表单实例编号") String formInstanceId) {
    try {
      WorkOrderEntity result = this.workOrderEntityService.findByFormInstanceId(formInstanceId);
      return this.buildHttpResultW(result);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照WorkOrderEntity的主键编号，查询指定的数据信息（不包括任何关联信息）
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照WorkOrderEntity的主键编号，查询指定的数据信息（不包括任何关联信息）。")
  @GetMapping(value = "/findById")
  public ResponseModel findById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      WorkOrderEntity result = this.workOrderEntityService.findById(id);
      return this.buildHttpResultW(result);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照主键进行信息的真删除
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照主键进行信息的真删除。")
  @GetMapping(value = "/deleteById")
  public void deleteById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      this.workOrderEntityService.deleteById(id);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
    }
  }

  /**
   * 条件分页查询
   *
   * @param pageable
   * @param params
   * @return
   */
  @ApiOperation(value = "条件分页查询。")
  @GetMapping(value = "/findByConditions")
  public ResponseModel findByConditions(@RequestBody @ApiParam("分页信息") Pageable pageable,
                                       @ApiParam("查询条件") Map<String, Object> params) {
    try {
      Page<WorkOrderEntity> result = this.workOrderEntityService.findByConditions(pageable, params);
      return this.buildHttpResultW(result, PROPERTIES2);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 根据塘口和创建时间查询
   *
   * @param pondId     塘口主键
   * @param createTime 创建时间
   * @return
   */
  @ApiOperation(value = "根据塘口和创建时间查询。")
  @GetMapping(value = "/findByPondAndTime")
  public ResponseModel findByPondAndTime(@RequestParam("pondId") @ApiParam("塘口主键") String pondId,@RequestParam("createTime")  @ApiParam("创建时间") Date createTime) {
    try {
      List<WorkOrderEntity> result = this.workOrderEntityService.findByPondAndTime(pondId, createTime);
      return this.buildHttpResultW(result, PROPERTIES2);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 保存
   *
   * @param w
   */
  @ApiOperation(value = "保存。")
  @PostMapping(value = "/findByPondAndTime")
  public void save(@RequestBody @ApiParam("WorkOrderEntity信息") WorkOrderEntity w) {
    try {
      this.workOrderEntityService.save(w);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
    }
  }

  /**
   * 根据塘口和状态查询
   *
   * @param pondId 塘口主键
   * @param state  状态
   * @return
   */
  @ApiOperation(value = "根据塘口和状态查询。")
  @GetMapping(value = "/findByStateAndPond")
  public ResponseModel findByStateAndPond(@RequestParam("pondId") @ApiParam("塘口主键") String pondId,
                                          @RequestParam("state") @ApiParam("状态") int state) {
    try {
      List<WorkOrderEntity> result = this.workOrderEntityService.findByStateAndPond(pondId, state);
      return this.buildHttpResultW(result, PROPERTIES2);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }
}
