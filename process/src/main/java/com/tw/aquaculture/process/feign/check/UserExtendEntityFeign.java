package com.tw.aquaculture.process.feign.check;

import com.bizunited.platform.core.controller.model.ResponseModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 用户拓展信息
 * @author fangfu.luo
 * @date 2019/12/27 11:38
 */
@FeignClient(name = "${aquiServiceName.check}", qualifier = "userExtendEntityFeign")
public interface UserExtendEntityFeign {

  /**
   * 按照UserEntity的主键编号，查询指定的数据信息
   * @param userId 用户主键
   * @return
   */
  @GetMapping(value="/v1/userExtendEntitys/findByUserId")
  ResponseModel findByUserId(@RequestParam("userId") String userId);

}
