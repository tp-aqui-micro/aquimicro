package com.tw.aquaculture.process.feign.plan;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.CodeGenerateEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 编码生成
 * @author 陈荣
 * @date 2019/12/4 14:50
 */
@FeignClient(name = "${aquiServiceName.plan}", qualifier = "CodeGenerateFeign")
public interface CodeGenerateFeign {

  /**
   * 生成基础数据编码
   *
   * @param codeTypeEnum
   * @return
   */
  @GetMapping(value = "/generateCode/generateBusinessCode")
  public String generateCode(@RequestParam("codeTypeEnum") CodeTypeEnum codeTypeEnum);

  /**
   * 生成业务数据编码
   *
   * @param codeTypeEnum
   * @return
   */
  @GetMapping(value = "/generateCode/generateBusinessCode")
  public String generateBusinessCode(@RequestParam("codeTypeEnum") CodeTypeEnum codeTypeEnum);

  /**
   * 根据编码类型和前缀查询
   *
   * @param codeType
   * @param prefix
   * @return
   */
  @GetMapping(value = "/generateCode/findByCodeTypeAndPrefix")
  ResponseModel findByCodeTypeAndPrefix(@RequestParam("codeType") String codeType, @RequestParam("prefix") String prefix);

  /**
   * 保存编码
   *
   * @param entity
   * @return
   */
  @PostMapping(value = "/generateCode/saveAndFlush")
  ResponseModel saveAndFlush(@RequestBody CodeGenerateEntity entity);
}
