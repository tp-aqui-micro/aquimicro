package com.tw.aquaculture.process.feign.store;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.store.StockOutEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 出库
 * @author 陈荣
 * @date 2019/12/23 11:18
 */
@FeignClient(name = "${aquiServiceName.store}", qualifier = "StockOutFeign")
public interface StockOutFeign {

  /**
   * 创建
   *
   * @return stockOutEntity
   */
  @PostMapping(value = "/v1/stockOutEntitys")
  public ResponseModel create(@RequestBody StockOutEntity stockOutEntity);

  /**
   * 修改
   *
   * @param stockOutEntity
   * @return
   */
  @PostMapping(value = "/v1/stockOutEntitys/update")
  public ResponseModel update(@RequestBody StockOutEntity stockOutEntity);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/stockOutEntitys/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/stockOutEntitys/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 格局表单实例编号查详情
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/stockOutEntitys/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据表单实例编号查询
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/stockOutEntitys/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据id删除
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/stockOutEntitys/deleteById")
  public ResponseModel deleteById(@RequestParam("id") String id);
}
