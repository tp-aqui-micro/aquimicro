package com.tw.aquaculture.process.remote.saleapply;

import com.tw.aquaculture.common.vo.remote.saleapply.SaleRequestVo;
import com.tw.aquaculture.common.vo.remote.saleapply.SaleResponseDataVo;

/**
 * @author 陈荣
 * @date 2019/12/13 10:51
 */
public interface RemoteService {

  /**
   * 岀鱼申请回调
   * @param saleRequestVo
   * @return
   */
  SaleResponseDataVo saleApplyCallback(SaleRequestVo saleRequestVo);

}
