package com.tw.aquaculture.process.remote.saleapply;

import com.tw.aquaculture.common.entity.process.OutFishApplyEntity;
import com.tw.aquaculture.process.remote.saleapply.dto.SaleApplyRequestDTO;
import com.tw.aquaculture.process.remote.saleapply.dto.SaleApplyResponseDTO;
import com.tw.aquaculture.process.remote.saleapply.vo.OSBResponse;

/**
 * 水产品销售：水产系统对接外部接口Service
 *
 * @author bo shi
 */
public interface SaleRemoteService {

  /**
   * 《水产品销售申请》发起流程接口
   * @param outFishApplyEntity
   * @return
   */
  OSBResponse<SaleApplyResponseDTO> saleApply(OutFishApplyEntity outFishApplyEntity, String account);

  /**
   * 《水产品销售申请》发起流程接口
   *
   * @param data 申请数据主体
   * @return {@link OSBResponse<SaleApplyResponseDTO>}
   */
  OSBResponse<SaleApplyResponseDTO> saleApply(SaleApplyRequestDTO data, String transactionId);
}
