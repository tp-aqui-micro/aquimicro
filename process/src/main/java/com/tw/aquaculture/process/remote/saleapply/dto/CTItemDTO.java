package com.tw.aquaculture.process.remote.saleapply.dto;

import java.io.Serializable;

/**
 * 水产品销售申请：存塘信息列表
 *
 * @author bo shi
 */
public class CTItemDTO implements Serializable {

  private static final long serialVersionUID = -6053549379695860040L;
  /** 品种 */
  private String varieties;
  /** 规格 */
  private String specifications;
  /** 投苗时间。格式：yyyy-MM-dd */
  private String tmDate;
  /** 最高价 */
  private String maxPrice;
  /** 最低价 */
  private String minPrice;
  /** 数量 */
  private String ctcount;
  /** 预计销售时间。格式：yyyy-MM-dd */
  private String estimatedsalestime;
  /** 销售意见 */
  private String salesOpinions;
  /** 备注 */
  private String ctRemarks;

  public String getVarieties() {
    return varieties;
  }

  public void setVarieties(String varieties) {
    this.varieties = varieties;
  }

  public String getSpecifications() {
    return specifications;
  }

  public void setSpecifications(String specifications) {
    this.specifications = specifications;
  }

  public String getTmDate() {
    return tmDate;
  }

  public void setTmDate(String tmDate) {
    this.tmDate = tmDate;
  }

  public String getMaxPrice() {
    return maxPrice;
  }

  public void setMaxPrice(String maxPrice) {
    this.maxPrice = maxPrice;
  }

  public String getMinPrice() {
    return minPrice;
  }

  public void setMinPrice(String minPrice) {
    this.minPrice = minPrice;
  }

  public String getCtcount() {
    return ctcount;
  }

  public void setCtcount(String ctcount) {
    this.ctcount = ctcount;
  }

  public String getEstimatedsalestime() {
    return estimatedsalestime;
  }

  public void setEstimatedsalestime(String estimatedsalestime) {
    this.estimatedsalestime = estimatedsalestime;
  }

  public String getSalesOpinions() {
    return salesOpinions;
  }

  public void setSalesOpinions(String salesOpinions) {
    this.salesOpinions = salesOpinions;
  }

  public String getCtRemarks() {
    return ctRemarks;
  }

  public void setCtRemarks(String ctRemarks) {
    this.ctRemarks = ctRemarks;
  }
}
