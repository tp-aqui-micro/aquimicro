package com.tw.aquaculture.process.remote.saleapply.dto;

import com.tw.aquaculture.process.remote.saleapply.vo.RemoteCommonConstants;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 水产品销售申请 ： 附件列表
 *
 * @author bo shi
 */
public class FilesDTO implements Serializable {

  private static final long serialVersionUID = 6530029503719699709L;

  /** 唯一标识name */
  private String dDocName;
  /** 唯一标识id。上传fbc后返回的id */
  private String dID;
  /** 文件名。必填 */
  @NotBlank(message = "附件名称不能为空")
  private String filename;
  /** 文件大小。必填 */
  @NotBlank(message = "附件大小不能为空")
  private String size;
  /** */
  private String oldid;
  /** 系统标识。固定值：{@link RemoteCommonConstants#TO_FBC_SYSTEM_FLAG} */
  private String system = RemoteCommonConstants.TO_FBC_SYSTEM_FLAG;
  /** 附件下载地址 */
  private String url;

  public String getdDocName() {
    return dDocName;
  }

  public void setdDocName(String dDocName) {
    this.dDocName = dDocName;
  }

  public String getdID() {
    return dID;
  }

  public void setdID(String dID) {
    this.dID = dID;
  }

  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public String getSize() {
    return size;
  }

  public void setSize(String size) {
    this.size = size;
  }

  public String getOldid() {
    return oldid;
  }

  public void setOldid(String oldid) {
    this.oldid = oldid;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getSystem() {
    return system;
  }
}
