package com.tw.aquaculture.process.remote.saleapply.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * 水产品销售申请 ： 请求数据主体
 *
 * @author bo shi
 */
public class SaleApplyRequestDTO implements Serializable {
  private static final long serialVersionUID = 536138010468889507L;

  /**固定值-SCYZ*/
  private String systemCode="SCYZ";
  /** 组织机构 */
  private String baseOrgName;
  /** 组织机构id */
  private String baseOrgId;
  /** 申请人员 */
  private String applyName;
  /** 申请人员Id。必填 */
  @NotBlank(message = "申请人员ID不能为空")
  private String applyId;
  /** 填报时间 */
  private String creationDate;
  /** FBC流程ID */
  private String processId;
  /** 业务表单ID。必填。外围系统唯一业务标示id，FBC用以确实流程是否有重发。如果有统一的bussinessId在在审批中的流程则返回报错信息 */
  @NotBlank(message = "业务表单ID不能为空")
  private String bussinessId;
  /** 岗位Id。必填。HR系统8位编码 */
  @NotBlank(message = "HR系统岗位ID不能为空")
  private String stationId;
  /** 岗位名称。必填。HR系统岗位名称 */
  @NotBlank(message = "HR系统岗位名称不能为空")
  private String stationName;
  /** 主题。必填 */
  @NotBlank(message = "主题不能为空")
  private String title;
  /** 流程表单号。如有值，则FBC视为退回后发起的流程 */
  private String reqNo;
  /** 流程回写关键字。响应时FBC返回的数据调用方的预留字段 */
  private String bussinessType;
  /** 基地。必填 */
  @NotBlank(message = "基地不能为空")
  private String basename;
  /** 塘口 */
  private String tangkou;
  /** 申请理由 */
  private String reasons;
  /** 附件信息列表 */
  @Valid
  private List<FilesDTO> files;
  /** 存塘信息列表 */
  private List<CTItemDTO> ctItems;
  /** 销售信息列表 */
  private List<SCItemDTO> scItems;

  public String getBaseOrgName() {
    return baseOrgName;
  }

  public void setBaseOrgName(String baseOrgName) {
    this.baseOrgName = baseOrgName;
  }

  public String getBaseOrgId() {
    return baseOrgId;
  }

  public void setBaseOrgId(String baseOrgId) {
    this.baseOrgId = baseOrgId;
  }

  public String getApplyName() {
    return applyName;
  }

  public void setApplyName(String applyName) {
    this.applyName = applyName;
  }

  public String getApplyId() {
    return applyId;
  }

  public void setApplyId(String applyId) {
    this.applyId = applyId;
  }

  public String getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(String creationDate) {
    this.creationDate = creationDate;
  }

  public String getProcessId() {
    return processId;
  }

  public void setProcessId(String processId) {
    this.processId = processId;
  }

  public String getBussinessId() {
    return bussinessId;
  }

  public void setBussinessId(String bussinessId) {
    this.bussinessId = bussinessId;
  }

  public String getStationId() {
    return stationId;
  }

  public void setStationId(String stationId) {
    this.stationId = stationId;
  }

  public String getStationName() {
    return stationName;
  }

  public void setStationName(String stationName) {
    this.stationName = stationName;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getReqNo() {
    return reqNo;
  }

  public void setReqNo(String reqNo) {
    this.reqNo = reqNo;
  }

  public String getBussinessType() {
    return bussinessType;
  }

  public void setBussinessType(String bussinessType) {
    this.bussinessType = bussinessType;
  }

  public String getBasename() {
    return basename;
  }

  public void setBasename(String basename) {
    this.basename = basename;
  }

  public String getTangkou() {
    return tangkou;
  }

  public void setTangkou(String tangkou) {
    this.tangkou = tangkou;
  }

  public String getReasons() {
    return reasons;
  }

  public void setReasons(String reasons) {
    this.reasons = reasons;
  }

  public List<FilesDTO> getFiles() {
    return files;
  }

  public void setFiles(List<FilesDTO> files) {
    this.files = files;
  }

  public List<CTItemDTO> getCtItems() {
    return ctItems;
  }

  public void setCtItems(List<CTItemDTO> ctItems) {
    this.ctItems = ctItems;
  }

  public List<SCItemDTO> getScItems() {
    return scItems;
  }

  public void setScItems(List<SCItemDTO> scItems) {
    this.scItems = scItems;
  }

  public String getSystemCode() {
    return systemCode;
  }

  public void setSystemCode(String systemCode) {
    this.systemCode = systemCode;
  }
}
