package com.tw.aquaculture.process.remote.saleapply.dto;

import java.io.Serializable;

/**
 * 水产品销售申请 ： 响应数据
 *
 * @author bo shi
 */
public class SaleApplyResponseDTO implements Serializable {
  private static final long serialVersionUID = -4926807036076083262L;

  /** 其他系统传过去的业务id */
  private String bussinessId;
  /** 其他系统传过去的业务类型(如：SRM_WF_001) */
  private String bussinessType;
  /** 流程ID */
  private String instid;
  /** 流程编号 FBC的表单号-FBC流程的唯一标识值 */
  private String reqNo;
  /** 扩展字段1，便于特殊情况填充 */
  private String attribute1;
  /** 扩展字段2，便于特殊情况填充 */
  private String attribute2;

  public String getBussinessId() {
    return bussinessId;
  }

  public void setBussinessId(String bussinessId) {
    this.bussinessId = bussinessId;
  }

  public String getBussinessType() {
    return bussinessType;
  }

  public void setBussinessType(String bussinessType) {
    this.bussinessType = bussinessType;
  }

  public String getInstid() {
    return instid;
  }

  public void setInstid(String instid) {
    this.instid = instid;
  }

  public String getReqNo() {
    return reqNo;
  }

  public void setReqNo(String reqNo) {
    this.reqNo = reqNo;
  }

  public String getAttribute1() {
    return attribute1;
  }

  public void setAttribute1(String attribute1) {
    this.attribute1 = attribute1;
  }

  public String getAttribute2() {
    return attribute2;
  }

  public void setAttribute2(String attribute2) {
    this.attribute2 = attribute2;
  }
}
