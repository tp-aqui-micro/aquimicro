package com.tw.aquaculture.process.remote.saleapply.internal;

import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.kuiper.entity.InstanceEntity;
import com.tw.aquaculture.common.entity.base.CustomerEntity;
import com.tw.aquaculture.common.entity.process.OutApplyCustomStateEntity;
import com.tw.aquaculture.common.entity.process.OutFishApplyEntity;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.vo.remote.saleapply.SaleItemVo;
import com.tw.aquaculture.common.vo.remote.saleapply.SaleRequestBodyVo;
import com.tw.aquaculture.common.vo.remote.saleapply.SaleRequestVo;
import com.tw.aquaculture.common.vo.remote.saleapply.SaleResponseDataVo;
import com.tw.aquaculture.process.remote.saleapply.RemoteService;
import com.tw.aquaculture.process.service.CustomerEntityService;
import com.tw.aquaculture.process.service.FormInstanceHandleService;
import com.tw.aquaculture.process.service.OutApplyCustomStateEntityService;
import com.tw.aquaculture.process.service.OutFishApplyEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * @author 陈荣
 * @date 2019/12/13 10:51
 */
@Service
public class RemoteServiceImpl implements RemoteService {

  private static final Logger LOGGER = LoggerFactory.getLogger(RemoteServiceImpl.class);

  @Autowired
  private CustomerEntityService customerEntityService;

  @Autowired
  private OutFishApplyEntityService outFishApplyEntityService;

  @Autowired
  private OutApplyCustomStateEntityService outApplyCustomStateEntityService;

  @Autowired
  private FormInstanceHandleService formInstanceHandleService;

  @Override
  @Transactional
  public SaleResponseDataVo saleApplyCallback(SaleRequestVo saleRequestVo) {
    SaleRequestBodyVo saleRequestBodyVo = saleRequestVo.getData();
    if(saleRequestBodyVo == null){
      return null;
    }
    List<SaleItemVo> saleItemVos = saleRequestBodyVo.getSaleItems();
    if(CollectionUtils.isEmpty(saleItemVos)){
      return null;
    }
    LOGGER.info("岀鱼申请回调参数：{}", saleRequestVo);
    String outFishApplyId = saleRequestBodyVo.getBussinessId();
    OutFishApplyEntity outFishApplyEntity = outFishApplyEntityService.findDetailsById(outFishApplyId);
    Validate.notNull(outFishApplyEntity, "未找到该出鱼申请，请检查！");
    outFishApplyEntity.setResureState(saleRequestBodyVo.getStatus());
    outFishApplyEntity.setResureIdea(saleRequestBodyVo.getMessage());
    outFishApplyEntity.setModifyTime(new Date());
    outFishApplyEntityService.update(outFishApplyEntity);
    List<OutApplyCustomStateEntity> outApplyCustomStateEntities = outApplyCustomStateEntityService.findByOutFishApply(outFishApplyId);
    for(SaleItemVo saleItemVo : saleItemVos){
      if(StringUtils.isBlank(saleItemVo.getCustomerName())){
        continue;
      }
      CustomerEntity customerEntity = new CustomerEntity();
      customerEntity.setCustomerType(null);
      if(outFishApplyEntity.getBase()!=null && outFishApplyEntity.getBase().getBaseOrg()!=null){
        OrganizationEntity company = new OrganizationEntity();
        company.setId(outFishApplyEntity.getBase().getBaseOrg().getParent().getId());
        customerEntity.setCompany(company);
      }
      customerEntity.setTwLinkUser(null);
      customerEntity.setEbsCode(null);
      customerEntity.setLinkName(saleItemVo.getContactsNAME());
      customerEntity.setName(saleItemVo.getCustomerName());
      customerEntity.setPhone(saleItemVo.getContactnumber());
      customerEntity.setEnableState(EnableStateEnum.ENABLE.getState());
      List<CustomerEntity> oldCustomers = customerEntityService.findByNameAndPhone(customerEntity.getName(), customerEntity.getPhone());
      /*保存客户*/
      CustomerEntity customerResult;
      if(CollectionUtils.isEmpty(oldCustomers)){
        customerEntity.setCreatePosition("接口传入");
        customerEntity.setCreateName("接口传入");
        customerEntity.setCreateTime(new Date());
        InstanceEntity instanceEntity = (InstanceEntity) formInstanceHandleService.create(customerEntity, "CustomerEntity", "CustomerEntityService.create", true);
        String formInstanceId = instanceEntity.getId();
        customerEntity.setFormInstanceId(formInstanceId);
        customerResult = customerEntityService.create(customerEntity);
      }else{
        customerEntity.setModifyTime(new Date());
        customerEntity.setModifyName("接口传入");
        customerEntity.setModifyPosition("接口传入");
        customerEntity.setCreatePosition(oldCustomers.get(0).getCreatePosition());
        customerEntity.setCreateName(oldCustomers.get(0).getCreateName());
        customerEntity.setFormInstanceId(oldCustomers.get(0).getFormInstanceId());
        customerEntity.setId(oldCustomers.get(0).getId());
        customerResult = customerEntityService.update(customerEntity);
      }
      if(customerResult != null) {
        OutApplyCustomStateEntity outApplyCustomStateEntity = new OutApplyCustomStateEntity();
        outApplyCustomStateEntity.setBaseName(saleItemVo.getBaseName());
        outApplyCustomStateEntity.setFreight(saleItemVo.getFreight());
        outApplyCustomStateEntity.setFreightcomfirm(saleItemVo.getFreightcomfirm());
        outApplyCustomStateEntity.setPondName(saleItemVo.getPondName());
        outApplyCustomStateEntity.setPondName(saleItemVo.getPondName());
        outApplyCustomStateEntity.setQuantityNum(saleItemVo.getQuantityNum());
        outApplyCustomStateEntity.setRequireSpecification(saleItemVo.getRequireSpecification());
        outApplyCustomStateEntity.setSalesfreight(saleItemVo.getSalesfreight());
        outApplyCustomStateEntity.setTangkouprice(saleItemVo.getTangkouprice());
        outApplyCustomStateEntity.setState(saleRequestBodyVo.getStatus());
        outApplyCustomStateEntity.setVarietiesSales(saleItemVo.getVarietiesSales());
        outApplyCustomStateEntity.setTangkouPricecomfirm(saleItemVo.getTangkouPricecomfirm());
        outApplyCustomStateEntity.setSalesRemarks(saleRequestBodyVo.getMessage());
        outApplyCustomStateEntity.setCustomer(customerResult);
        outApplyCustomStateEntity.setOutFishApply(outFishApplyEntity);
        /*保存岀鱼申请审批信息*/
        saveOutApplyCustomStateEntity(outApplyCustomStateEntity, outApplyCustomStateEntities);
      }
    }
    return new SaleResponseDataVo(saleRequestBodyVo.getBussinessId(), null);
  }

  private OutApplyCustomStateEntity saveOutApplyCustomStateEntity(@NotNull OutApplyCustomStateEntity outApplyCustomStateEntity,
                                                                  List<OutApplyCustomStateEntity> outApplyCustomStateEntities) {
    if(CollectionUtils.isEmpty(outApplyCustomStateEntities)){
      return outApplyCustomStateEntityService.create(outApplyCustomStateEntity);
    }else{
      for(OutApplyCustomStateEntity out : outApplyCustomStateEntities){
        if(out.getCustomer().getId().equals(outApplyCustomStateEntity.getId())){
          BeanUtils.copyProperties(outApplyCustomStateEntity, out);
          return outApplyCustomStateEntityService.update(out);
        }
      }
      return outApplyCustomStateEntityService.create(outApplyCustomStateEntity);
    }
  }
}
