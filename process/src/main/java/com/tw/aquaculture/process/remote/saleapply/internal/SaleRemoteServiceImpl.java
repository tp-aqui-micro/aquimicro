package com.tw.aquaculture.process.remote.saleapply.internal;

import com.bizunited.platform.rbac.server.service.UserService;
import com.bizunited.platform.rbac.server.vo.UserVo;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.process.OutFishApplyEntity;
import com.tw.aquaculture.common.entity.process.RemainInfoEntity;
import com.tw.aquaculture.common.entity.userextend.UserExtendEntity;
import com.tw.aquaculture.common.enums.ResureStateEnum;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.process.remote.saleapply.SaleRemoteService;
import com.tw.aquaculture.process.remote.saleapply.dto.CTItemDTO;
import com.tw.aquaculture.process.remote.saleapply.dto.SaleApplyRequestDTO;
import com.tw.aquaculture.process.remote.saleapply.dto.SaleApplyResponseDTO;
import com.tw.aquaculture.process.remote.saleapply.vo.OSBRequest;
import com.tw.aquaculture.process.remote.saleapply.vo.OSBResponse;
import com.tw.aquaculture.process.service.OutFishApplyEntityService;
import com.tw.aquaculture.process.service.PondEntityService;
import com.tw.aquaculture.process.service.base.UserExtendEntityService;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/** @author bo shi */
@Service
public class SaleRemoteServiceImpl implements SaleRemoteService {

  private static final Logger LOGGER = LoggerFactory.getLogger(SaleRemoteServiceImpl.class);

  private static final String FAIL_DESC = "提交审批失败";

  @Autowired private RestTemplate restTemplate;

  @Autowired private LocalValidatorFactoryBean validator;

  @Autowired private PondEntityService pondEntityService;

  @Autowired private UserExtendEntityService userExtendEntityService;

  @Autowired private UserService userService;

  @Autowired private OutFishApplyEntityService outFishApplyEntityService;

  /**申请地址*/
  @Value("${saleApply.url}")
  private String url;

  @Override
  @Async("myAsync")
  public OSBResponse<SaleApplyResponseDTO> saleApply(OutFishApplyEntity outFishApplyEntity, String account){
    outFishApplyEntity = outFishApplyEntityService.findDetailsById(outFishApplyEntity.getId());
    PondEntity pond = pondEntityService.findDetailsById(outFishApplyEntity.getPond().getId());
    Validate.notNull(pond, "未找到塘口信息，请检查");
    Validate.notNull(pond.getBase(), "未获取到塘口关联的基地信息，请检查");
    Validate.notNull(pond.getBase().getBaseOrg(), "未获取到所属基地组织信息，请检查");
    SaleApplyRequestDTO saleApplyRequestDTO = new SaleApplyRequestDTO();
    saleApplyRequestDTO.setApplyName(outFishApplyEntity.getCreateName());
    String baseName = pond.getBase().getBaseOrg().getOrgName();
    saleApplyRequestDTO.setBasename(baseName);
    saleApplyRequestDTO.setBaseOrgId(pond.getBase().getBaseOrg().getId());
    saleApplyRequestDTO.setBussinessId(outFishApplyEntity.getId());
    UserVo userVo = userService.findByAccount(account);
    Validate.notNull(userVo, "未找到用户信息，请检查");
    saleApplyRequestDTO.setApplyId(userVo.getAccount());
    UserExtendEntity userExtendEntity = userExtendEntityService.findByUserId(userVo.getId());
    Validate.notNull(userExtendEntity, "未找到hr系统对应的用户信息，请检查");
    saleApplyRequestDTO.setStationName(userExtendEntity.getHrDescr4());
    saleApplyRequestDTO.setStationId(userExtendEntity.getHrPositionNbr());
    saleApplyRequestDTO.setBussinessType("scxs");
    saleApplyRequestDTO.setCreationDate(DateUtils.dateFormat(outFishApplyEntity.getCreateTime(), DateUtils.FULL_TIME));
    saleApplyRequestDTO.setProcessId(UUID.randomUUID().toString());
    saleApplyRequestDTO.setReqNo(outFishApplyEntity.getReqNo());
    saleApplyRequestDTO.setTitle(baseName+"水产品销售申请["+DateUtils.dateFormat(new Date(), DateUtils.FULL_TIME)+"]");
    Validate.notNull(outFishApplyEntity.getPond(), "塘口不能为空");
    Validate.notEmpty(outFishApplyEntity.getRemainInfos(), "存塘信息不能为空");
    saleApplyRequestDTO.setReasons(outFishApplyEntity.getApplyReason());
    saleApplyRequestDTO.setFiles(Lists.newArrayList());
    List<CTItemDTO> ctItemDTOList = Lists.newArrayList();
    for(RemainInfoEntity remainInfoEntity : outFishApplyEntity.getRemainInfos()){
      CTItemDTO ctItemDTO = new CTItemDTO();
      ctItemDTO.setVarieties(remainInfoEntity.getFishType().getName());
      ctItemDTO.setSpecifications(remainInfoEntity.getSpecs());
      ctItemDTO.setTmDate(DateUtils.dateFormat(remainInfoEntity.getPushFryTime(), DateUtils.TO_DAY_TIME));
      ctItemDTO.setMaxPrice(remainInfoEntity.getUpPrice());
      ctItemDTO.setMinPrice(remainInfoEntity.getLowPrice());
      ctItemDTO.setCtcount(remainInfoEntity.getWeight());
      ctItemDTO.setEstimatedsalestime(DateUtils.dateFormat(remainInfoEntity.getPredictOutTime(), DateUtils.TO_DAY_TIME));
      ctItemDTO.setCtRemarks(remainInfoEntity.getOutIdea());
      ctItemDTO.setSalesOpinions(remainInfoEntity.getOutIdea());
      ctItemDTOList.add(ctItemDTO);
    }
    saleApplyRequestDTO.setCtItems(ctItemDTOList);
    saleApplyRequestDTO.setTangkou(outFishApplyEntity.getPond().getName());
    saleApplyRequestDTO.setScItems(Lists.newArrayList());
    OSBResponse<SaleApplyResponseDTO> osbResponse = saleApply(saleApplyRequestDTO, outFishApplyEntity.getId());
    Validate.notNull(osbResponse,FAIL_DESC);
    Validate.notNull(osbResponse.getHead(), FAIL_DESC);
    Validate.notNull(osbResponse.getData(), FAIL_DESC);
    String reqNo = osbResponse.getData().getReqNo();
    String instid = osbResponse.getData().getInstid();
    outFishApplyEntity.setReqNo(reqNo);
    outFishApplyEntity.setInstid(instid);
    outFishApplyEntity.setResureState(ResureStateEnum.RESURING.getState());
    this.outFishApplyEntityService.update(outFishApplyEntity);

    String errorCode = osbResponse.getHead().getErrorcode();
    if(!ResureStateEnum.RESURING.getState().toString().equals(errorCode)){
      throw new IllegalArgumentException(osbResponse.getHead().getErrorinfo());
    }
    return osbResponse;
  }

  @Override
  public OSBResponse<SaleApplyResponseDTO> saleApply(SaleApplyRequestDTO data, String transactionId) {
    Errors errors = new DirectFieldBindingResult(data, "SaleApplyBodyDTO");
    data.setSystemCode("SCYZ");
    validator.validate(data, errors);
    if (errors.hasFieldErrors()) {
      throw new IllegalArgumentException(
              errors.getFieldErrors().stream()
                      .map(err -> String.format("[%s]-%s", err.getField(), err.getDefaultMessage()))
                      .collect(Collectors.joining(",")));
    }
    OSBRequest osbRequest =
            OSBRequest.builder().biztransactionid(transactionId).data(data).build();

    Gson gson = new GsonBuilder().create();

    OSBResponse<SaleApplyResponseDTO> osbResponse;
    ResponseEntity<String> rawResponse = null;
    try {
      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
      String json = gson.toJson(OSBRequest.wrap(osbRequest));
      HttpEntity<String> httpEntity =
              new HttpEntity<>(json, headers);
      rawResponse = restTemplate.postForEntity(url, httpEntity, String.class);
      LOGGER.info("岀鱼申请请求参数：{}", httpEntity);
      osbResponse =
              extractResponseFromJson(
                      rawResponse, new TypeToken<OSBResponse<SaleApplyResponseDTO>>() {}.getType());
    } catch (RestClientException e) {
      throw new IllegalArgumentException(
              String.format("销售申请提交失败，原因：%s,原始返回信息：%s", e.getLocalizedMessage(), rawResponse), e);
    }
    return osbResponse;
  }

  /**
   * 解析岀鱼申请返回json结果
   * @param response
   * @param type
   * @param <T>
   * @return
   */
  private <T> OSBResponse<T> extractResponseFromJson(ResponseEntity<String> response, Type type) {
    Gson gson = new GsonBuilder().create();
    JsonObject jsonObject = gson.fromJson(response.getBody(), JsonObject.class);
    JsonObject asJsonObject = jsonObject.getAsJsonObject("Response");
    return gson.fromJson(asJsonObject, type);
  }
}