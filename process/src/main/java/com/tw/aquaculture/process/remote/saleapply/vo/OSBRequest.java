package com.tw.aquaculture.process.remote.saleapply.vo;

import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * osb统一请求格式
 *
 * <p>根据客户方反馈，水产养殖系统对接的大部分接口是此格式，暂时定义一个通用格式
 *
 * @author bo shi
 */
public class OSBRequest<T> implements Serializable {

  private static final long serialVersionUID = 5711916228656450470L;

  @SerializedName("Head")
  private Head head;

  @SerializedName(value = "Data")
  private T data;

  private static class Head {
    /** 事务ID 必填，业务唯一id */
    private String biztransactionid;
    /** 数据数量 */
    private String count;
    /** 调用系统简称 固定值“SCYZ” */
    private String consumer = RemoteCommonConstants.TO_FBC_SYSTEM_FLAG;
    /** 固定值1 固定值“1” */
    private String srvlevel = "1";

    private String account;
    private String password;

    public String getBiztransactionid() {
      return biztransactionid;
    }

    public void setBiztransactionid(String biztransactionid) {
      this.biztransactionid = biztransactionid;
    }

    public String getCount() {
      return count;
    }

    public void setCount(String count) {
      this.count = count;
    }

    public String getConsumer() {
      return consumer;
    }

    public void setConsumer(String consumer) {
      this.consumer = consumer;
    }

    public String getSrvlevel() {
      return srvlevel;
    }

    public void setSrvlevel(String srvlevel) {
      this.srvlevel = srvlevel;
    }

    public String getAccount() {
      return account;
    }

    public void setAccount(String account) {
      this.account = account;
    }

    public String getPassword() {
      return password;
    }

    public void setPassword(String password) {
      this.password = password;
    }
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    private Head head;
    private OSBRequest osbRequest;

    public Builder() {
      this.head = new OSBRequest.Head();
      this.osbRequest = new OSBRequest();
      this.osbRequest.head = this.head;
    }

    public Builder data(Object data) {
      this.osbRequest.data = data;
      return this;
    }

    public Builder biztransactionid(String transactionId) {
      this.head.setBiztransactionid(transactionId);
      return this;
    }

    public Builder dataCount(String count) {
      this.head.setCount(count);
      return this;
    }

    public Builder account(String count) {
      this.head.setAccount(count);
      return this;
    }

    public Builder password(String password) {
      this.head.setPassword(password);
      return this;
    }

    public OSBRequest build() {
      if (StringUtils.isBlank(this.head.biztransactionid)) {
        throw new IllegalArgumentException("biztransactionid不能为空");
      }
      return this.osbRequest;
    }
  }

  public static OSBRequestWrapper wrap(OSBRequest request){
    return new OSBRequestWrapper(request);
  }

  public static class OSBRequestWrapper implements Serializable{

    private static final long serialVersionUID = -981410027185382808L;

    @SerializedName(value = "Request")
    private OSBRequest request;

    OSBRequestWrapper(OSBRequest request) {
      this.request = request;
    }
  }


}
