package com.tw.aquaculture.process.remote.saleapply.vo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * osb统一响应格式
 *
 * @author bo shi
 */
public class OSBResponse<T> implements Serializable {

  private static final long serialVersionUID = -359125052385818586L;

  @SerializedName(value = "head", alternate = "Head")
  private Head head;

  @SerializedName(value = "data", alternate = "Data")
  private T data;

  public class Head {

    /** 事务ID 和请求报文一致 */
    @SerializedName(value = "biztransactionid",alternate = "BIZTRANSACTIONID")
    private String biztransactionid;
    /** OSB的重做标志 0:不需重做 1：需重做 */
    @SerializedName(value = "result",alternate = "RESULT")
    private String result;
    /** 业务响应编码 0=成功，1=失败，10=bussinessId重复 */
    @SerializedName(value = "errorcode",alternate = "ERRORCODE")
    private String errorcode;
    /** 业务响应说明 */
    @SerializedName(value = "errorinfo",alternate = "ERRORINFO")
    private String errorinfo;
    /** 备注 */
    @SerializedName(value = "comments",alternate = "COMMENTS")
    private String comments;

    @SerializedName(value = "successcount",alternate = "SUCCESSCOUNT")
    private String successcount;

    public String getBiztransactionid() {
      return biztransactionid;
    }

    public String getResult() {
      return result;
    }

    public String getErrorcode() {
      return errorcode;
    }

    public String getErrorinfo() {
      return errorinfo;
    }

    public String getComments() {
      return comments;
    }

    public String getSuccesscount() {
      return successcount;
    }
  }


	public Head getHead() {
		return head;
	}

	public T getData() {
		return data;
	}
}
