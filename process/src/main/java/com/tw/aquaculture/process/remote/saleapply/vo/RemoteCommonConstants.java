package com.tw.aquaculture.process.remote.saleapply.vo;

/**
 * 接口对接公共常量
 *
 * @author bo shi
 */
public abstract class RemoteCommonConstants {
  /**
   * 意思是util类里面都是静态方法，不会去初始化这个类，所以不应该暴露一个public构造函数
   */
  private RemoteCommonConstants(){}
  /** 系统默认标识 */
  public static final String DEFAULT_SYSTEM_FLAG = "SCYZ";

  /** 对接fbc系统时，本系统标识 */
  public static final String TO_FBC_SYSTEM_FLAG = DEFAULT_SYSTEM_FLAG;
}
