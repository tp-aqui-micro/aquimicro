package com.tw.aquaculture.process.remote.saleapply.vo;

import com.tw.aquaculture.common.vo.remote.saleapply.SaleItemVo;

import java.util.List;

/**
 * @author 陈荣
 * @date 2019/12/13 13:56
 */
public class SaleRequestBodyVo {

  /**标题*/
  private String title;
  /**业务流程id*/
  private String bussinessId;
  /**审批状态*/
  private Integer status;
  /**审批意见*/
  private String message;
  /**销售明细*/
  private List<SaleItemVo> saleItems;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getBussinessId() {
    return bussinessId;
  }

  public void setBussinessId(String bussinessId) {
    this.bussinessId = bussinessId;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public List<SaleItemVo> getSaleItems() {
    return saleItems;
  }

  public void setSaleItems(List<SaleItemVo> saleItems) {
    this.saleItems = saleItems;
  }
}
