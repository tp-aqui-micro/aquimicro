package com.tw.aquaculture.process.remote.saleapply.vo;

/**
 * @author 陈荣
 * @date 2019/12/13 14:08
 */
public class SaleResponseDataVo {

  private String bussinessId;

  private String bussinessType;

  public SaleResponseDataVo(String bussinessId, String bussinessType) {
    this.bussinessId = bussinessId;
    this.bussinessType = bussinessType;
  }

  public String getBussinessId() {
    return bussinessId;
  }

  public void setBussinessId(String bussinessId) {
    this.bussinessId = bussinessId;
  }

  public String getBussinessType() {
    return bussinessType;
  }

  public void setBussinessType(String bussinessType) {
    this.bussinessType = bussinessType;
  }
}
