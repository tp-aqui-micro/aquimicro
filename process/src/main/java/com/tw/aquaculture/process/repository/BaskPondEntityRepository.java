package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.BaskPondEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * BaskPondEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_BaskPondEntityRepository")
public interface BaskPondEntityRepository
        extends
        JpaRepository<BaskPondEntity, String>
        , JpaSpecificationExecutor<BaskPondEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct baskPondEntity from BaskPondEntity baskPondEntity "
          + " left join fetch baskPondEntity.pond baskPondEntity_pond "
          + " left join fetch baskPondEntity_pond.base base "
          + " left join fetch baskPondEntity.workOrder baskPondEntity_workOrder "
          + " where baskPondEntity.id=:id ")
  public BaskPondEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct baskPondEntity from BaskPondEntity baskPondEntity "
          + " left join fetch baskPondEntity.pond baskPondEntity_pond "
          + " left join fetch baskPondEntity_pond.base base "
          + " left join fetch baskPondEntity.workOrder baskPondEntity_workOrder "
          + " where baskPondEntity.formInstanceId=:formInstanceId ")
  public BaskPondEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from BaskPondEntity f where f.formInstanceId = :formInstanceId")
  public BaskPondEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据工单id查询
   * @param workOrderId
   * @return
   */
  @Query("select distinct baskPondEntity from BaskPondEntity baskPondEntity "
          + " left join fetch baskPondEntity.pond baskPondEntity_pond "
          + " left join fetch baskPondEntity_pond.base base "
          + " left join fetch baskPondEntity.workOrder baskPondEntity_workOrder "
          + " where baskPondEntity_workOrder.id=:workOrderId ")
  List<BaskPondEntity> findByWorkOrderId(@Param("workOrderId") String workOrderId);

  /**
   * 查询没有绑定工单的晒唐记录
   * @return
   */
  @Query("select distinct baskPondEntity from BaskPondEntity baskPondEntity "
          + " left join fetch baskPondEntity.pond baskPondEntity_pond "
          + " left join fetch baskPondEntity_pond.base base "
          + " left join fetch baskPondEntity.workOrder baskPondEntity_workOrder "
          + " where baskPondEntity_pond.id=:pondId and baskPondEntity.workOrder is null order by baskPondEntity.createTime ")
  List<BaskPondEntity> findNoneWorkOrderByPond(@Param("pondId") String pondId);
}