package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.CleanPondEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * CleanPondEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_CleanPondEntityRepository")
public interface CleanPondEntityRepository
    extends
      JpaRepository<CleanPondEntity, String>
      ,JpaSpecificationExecutor<CleanPondEntity>
  {

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct cleanPondEntity from CleanPondEntity cleanPondEntity "
      + " left join fetch cleanPondEntity.pond cleanPondEntity_pond "
          + " left join fetch cleanPondEntity_pond.base base "
      + " left join fetch cleanPondEntity.workOrder cleanPondEntity_workOrder "
      + " where cleanPondEntity.id=:id ")
  public CleanPondEntity findDetailsById(@Param("id") String id);
  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   * @param formInstanceId 表单实例编号
   * */
  @Query("select distinct cleanPondEntity from CleanPondEntity cleanPondEntity "
      + " left join fetch cleanPondEntity.pond cleanPondEntity_pond "
          + " left join fetch cleanPondEntity_pond.base base "
      + " left join fetch cleanPondEntity.workOrder cleanPondEntity_workOrder "
      + " where cleanPondEntity.formInstanceId=:formInstanceId ")
  public CleanPondEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);
  /**
   * 按照表单实例编号进行查询
   * @param formInstanceId 表单实例编号
   * */
  @Query(" from CleanPondEntity f where f.formInstanceId = :formInstanceId")
  public CleanPondEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);



}