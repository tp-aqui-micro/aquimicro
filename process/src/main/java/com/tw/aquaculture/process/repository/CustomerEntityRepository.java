package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.base.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * CustomerEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_CustomerEntityRepository")
public interface CustomerEntityRepository
        extends
        JpaRepository<CustomerEntity, String>
        , JpaSpecificationExecutor<CustomerEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct customerEntity from CustomerEntity customerEntity "
          + " left join fetch customerEntity.company customerEntity_company "
          + " left join fetch customerEntity.twLinkUser customerEntity_twLinkUser "
          + " where customerEntity.id=:id ")
  public CustomerEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct customerEntity from CustomerEntity customerEntity "
          + " left join fetch customerEntity.company customerEntity_company "
          + " left join fetch customerEntity.twLinkUser customerEntity_twLinkUser "
          + " where customerEntity.formInstanceId=:formInstanceId ")
  public CustomerEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from CustomerEntity f where f.formInstanceId = :formInstanceId")
  public CustomerEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据客户名称和电话查询
   * @param name
   * @param phone
   * @return
   */
  List<CustomerEntity> findByNameAndPhone(String name, String phone);
}