package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.CustomerInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * CustomerInfo业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_CustomerInfoRepository")
public interface CustomerInfoRepository
    extends
      JpaRepository<CustomerInfo, String>
      ,JpaSpecificationExecutor<CustomerInfo>
  {
  /**
   * 按照鱼种进行详情查询（包括关联信息）
   * @param fishType 鱼种
   * */
  @Query("select distinct customerInfo from CustomerInfo customerInfo "
      + " left join fetch customerInfo.fishType customerInfo_fishType "
      + " left join fetch customerInfo.outFishApply customerInfo_outFishApply "
       + " where customerInfo_fishType.id = :id")
  public Set<CustomerInfo> findDetailsByFishType(@Param("id") String id);
  /**
   * 按照出鱼申请进行详情查询（包括关联信息）
   * @param outFishApply 出鱼申请
   * */
  @Query("select distinct customerInfo from CustomerInfo customerInfo "
      + " left join fetch customerInfo.fishType customerInfo_fishType "
      + " left join fetch customerInfo.outFishApply customerInfo_outFishApply "
       + " where customerInfo_outFishApply.id = :id")
  public Set<CustomerInfo> findDetailsByOutFishApply(@Param("id") String id);

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct customerInfo from CustomerInfo customerInfo "
      + " left join fetch customerInfo.fishType customerInfo_fishType "
      + " left join fetch customerInfo.outFishApply customerInfo_outFishApply "
      + " where customerInfo.id=:id ")
  public CustomerInfo findDetailsById(@Param("id") String id);



}