package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.DividePondEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * DividePondEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_DividePondEntityRepository")
public interface DividePondEntityRepository
        extends
        JpaRepository<DividePondEntity, String>
        , JpaSpecificationExecutor<DividePondEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct dividePondEntity from DividePondEntity dividePondEntity "
          + " left join fetch dividePondEntity.outPond dividePondEntity_outPond "
          + " left join fetch dividePondEntity.inPond dividePondEntity_inPond "
          + " left join fetch dividePondEntity.fishType dividePondEntity_fishType "
          + " left join fetch dividePondEntity.groupLeader dividePondEntity_groupLeader "
          + " left join fetch dividePondEntity.workOrder dividePondEntity_workOrder "
          + " left join fetch dividePondEntity.dividePondItems dividePondItems "
          + " where dividePondEntity.id=:id ")
  public DividePondEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct dividePondEntity from DividePondEntity dividePondEntity "
          + " left join fetch dividePondEntity.outPond dividePondEntity_outPond "
          + " left join fetch dividePondEntity.inPond dividePondEntity_inPond "
          + " left join fetch dividePondEntity.fishType dividePondEntity_fishType "
          + " left join fetch dividePondEntity.groupLeader dividePondEntity_groupLeader "
          + " left join fetch dividePondEntity.workOrder dividePondEntity_workOrder "
          + " left join fetch dividePondEntity.dividePondItems dividePondItems "
          + " where dividePondEntity.formInstanceId=:formInstanceId ")
  public DividePondEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from DividePondEntity f where f.formInstanceId = :formInstanceId")
  public DividePondEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据转入塘口和时间区间查询
   *
   * @param pondId
   * @param startTime
   * @param endTime
   * @return
   */
  @Query(" select distinct dp from DividePondEntity dp " +
          " left join fetch dp.inPond pond " +
          " where pond.id=:pondId and dp.dividePondTime>=:startTime and dp.dividePondTime<:endTime ")
  List<DividePondEntity> findByInPondAndTime(@Param("pondId") String pondId, @Param("startTime") Date startTime, @Param("endTime") Date endTime);

  /**
   * 根据转出塘口和时间区间查询
   *
   * @param pondId
   * @param startTime
   * @param endTime
   * @return
   */
  @Query(" select distinct dp from DividePondEntity dp " +
          " left join fetch dp.outPond pond " +
          " where pond.id=:pondId and dp.dividePondTime>=:startTime and dp.dividePondTime<:endTime ")
  List<DividePondEntity> findByOutPondAndTime(@Param("pondId") String pondId, @Param("startTime") Date startTime, @Param("endTime") Date endTime);

  /**
   * 按鱼种统计、转入塘口查询养殖周期内记录
   * @param pondId
   * @param startTime
   * @param endTime
   * @param fishTypeId
   * @return
   */
  @Query(" select distinct dp from DividePondEntity dp " +
          " left join fetch dp.inPond pond " +
          " left join fetch dp.fishType fishType " +
          " where pond.id=:pondId and dp.dividePondTime>=:startTime and dp.dividePondTime<:endTime and fishType.id=:fishTypeId ")
  List<DividePondEntity> findByInPondAndTimesAndFishType(@Param("pondId") String pondId, @Param("startTime") Date startTime,
                                                         @Param("endTime") Date endTime, @Param("fishTypeId") String fishTypeId);

  /**
   * 按鱼种统计、转入塘口查询养殖周期内记录
   * @param pondId
   * @param startTime
   * @param endTime
   * @param fishTypeId
   * @return
   */
  @Query(" select distinct dp from DividePondEntity dp " +
          " left join fetch dp.outPond pond " +
          " left join fetch dp.fishType fishType " +
          " where pond.id=:pondId and dp.dividePondTime>=:startTime and dp.dividePondTime<:endTime and fishType.id=:fishTypeId ")
  List<DividePondEntity> findByOutPondAndTimesAndFishType(@Param("pondId") String pondId, @Param("startTime") Date startTime,
                                                          @Param("endTime") Date endTime, @Param("fishTypeId") String fishTypeId);

  /**
   * 根据工单编号、转入塘口、鱼种查询
   * @param pondId
   * @param workOrderId
   * @param fishTypeId
   * @return
   */
  @Query(" select distinct dp from DividePondEntity dp " +
          " left join fetch dp.inPond pond " +
          " left join fetch dp.workOrder workOrder " +
          " left join fetch dp.fishType fishType " +
          " where pond.id=:pondId and workOrder.id=:workOrderId and fishType.id=:fishTypeId ")
  List<DividePondEntity> findByInPondAndWorkOrderIdAndFishType(@Param("pondId") String pondId, @Param("workOrderId") String workOrderId, @Param("fishTypeId") String fishTypeId);

  /**
   * 根据工单编号、转出塘口、鱼种查询
   * @param pondId
   * @param workOrderId
   * @param fishTypeId
   * @return
   */
  @Query(" select distinct dp from DividePondEntity dp " +
          " left join fetch dp.outPond pond " +
          " left join fetch dp.workOrder workOrder " +
          " left join fetch dp.fishType fishType " +
          " where pond.id=:pondId and workOrder.id=:workOrderId and fishType.id=:fishTypeId ")
  List<DividePondEntity> findByOutPondAndWorkOrderIdAndFishType(@Param("pondId") String pondId, @Param("workOrderId") String workOrderId, @Param("fishTypeId") String fishTypeId);
}