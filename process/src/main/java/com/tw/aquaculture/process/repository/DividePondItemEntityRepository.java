package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.DividePondItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * DividePondItemEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_DividePondItemEntityRepository")
public interface DividePondItemEntityRepository
    extends
      JpaRepository<DividePondItemEntity, String>
      ,JpaSpecificationExecutor<DividePondItemEntity>
  {
  /**
   * 按照转/分塘进行详情查询（包括关联信息）
   * @param dividePond 转/分塘
   * */
  @Query("select distinct dividePondItemEntity from DividePondItemEntity dividePondItemEntity "
      + " left join fetch dividePondItemEntity.dividePond dividePondItemEntity_dividePond "
       + " where dividePondItemEntity_dividePond.id = :id")
  public Set<DividePondItemEntity> findDetailsByDividePond(@Param("id") String id);

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct dividePondItemEntity from DividePondItemEntity dividePondItemEntity "
      + " left join fetch dividePondItemEntity.dividePond dividePondItemEntity_dividePond "
      + " where dividePondItemEntity.id=:id ")
  public DividePondItemEntity findDetailsById(@Param("id") String id);



}