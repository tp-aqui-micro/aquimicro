package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.FishDamageEntity;
import com.tw.aquaculture.process.repository.internal.FishDamageEntityRepositoryCusm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * FishDamageEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_FishDamageEntityRepository")
public interface FishDamageEntityRepository
        extends
        JpaRepository<FishDamageEntity, String>
        , JpaSpecificationExecutor<FishDamageEntity>,
        FishDamageEntityRepositoryCusm {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct fishDamageEntity from FishDamageEntity fishDamageEntity "
          + " left join fetch fishDamageEntity.pond fishDamageEntity_pond "
          + " left join fetch fishDamageEntity_pond.base base "
          + " left join fetch fishDamageEntity.fishType fishDamageEntity_fishType "
          + " left join fetch fishDamageEntity.workOrder fishDamageEntity_workOrder "
          + " where fishDamageEntity.id=:id ")
  public FishDamageEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct fishDamageEntity from FishDamageEntity fishDamageEntity "
          + " left join fetch fishDamageEntity.pond fishDamageEntity_pond "
          + " left join fetch fishDamageEntity_pond.base base "
          + " left join fetch fishDamageEntity.fishType fishDamageEntity_fishType "
          + " left join fetch fishDamageEntity.workOrder fishDamageEntity_workOrder "
          + " where fishDamageEntity.formInstanceId=:formInstanceId ")
  public FishDamageEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from FishDamageEntity f where f.formInstanceId = :formInstanceId")
  public FishDamageEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据塘口和时间区间查询
   * @param pondId
   * @param startTime
   * @param endTime
   * @return
   */
  @Query(" select distinct f from FishDamageEntity f " +
          " left join fetch f.pond pond " +
          " where pond.id=:pondId and f.damageTime>=:startTime and f.damageTime<:endTime")
  List<FishDamageEntity> findByPondAndTimes(@Param("pondId") String pondId, @Param("startTime") Date startTime, @Param("endTime") Date endTime);

  /**
   * 按鱼种统计、塘口查询养殖周期内记录
   * @param pondId
   * @param startTime
   * @param endTime
   * @param fishTypeId
   * @return
   */
  @Query(" select distinct f from FishDamageEntity f " +
          " left join fetch f.pond pond " +
          " left join fetch f.fishType fishType " +
          " where pond.id=:pondId and f.damageTime>=:startTime and f.damageTime<:endTime and fishType.id=:fishTypeId ")
  List<FishDamageEntity> findByPondAndTimesAndFishType(@Param("pondId") String pondId, @Param("startTime") Date startTime,
                                                       @Param("endTime") Date endTime, @Param("fishTypeId") String fishTypeId);

  /**
   * 根据工单编号、塘口、鱼种查询
   * @param pondId
   * @param workOrderId
   * @param fishTypeId
   * @return
   */
  @Query(" select distinct f from FishDamageEntity f " +
          " left join fetch f.pond pond " +
          " left join fetch f.fishType fishType " +
          " left join fetch f.workOrder workOrder " +
          " where pond.id=:pondId and workOrder.id=:workOrderId and fishType.id=:fishTypeId ")
  List<FishDamageEntity> findByPondAndWorkOrderIdAndFishType(@Param("pondId") String pondId, @Param("workOrderId") String workOrderId, @Param("fishTypeId") String fishTypeId);
}