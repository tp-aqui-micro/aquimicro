package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.base.FishPriceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * FishPriceEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_FishPriceEntityRepository")
public interface FishPriceEntityRepository
    extends
      JpaRepository<FishPriceEntity, String>
      ,JpaSpecificationExecutor<FishPriceEntity>
  {

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct fishPriceEntity from FishPriceEntity fishPriceEntity "
      + " left join fetch fishPriceEntity.base fishPriceEntity_base "
      + " left join fetch fishPriceEntity.fishType fishPriceEntity_fishType "
      + " where fishPriceEntity.id=:id ")
  public FishPriceEntity findDetailsById(@Param("id") String id);
  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   * @param formInstanceId 表单实例编号
   * */
  @Query("select distinct fishPriceEntity from FishPriceEntity fishPriceEntity "
      + " left join fetch fishPriceEntity.base fishPriceEntity_base "
      + " left join fetch fishPriceEntity.fishType fishPriceEntity_fishType "
      + " where fishPriceEntity.formInstanceId=:formInstanceId ")
  public FishPriceEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);
  /**
   * 按照表单实例编号进行查询
   * @param formInstanceId 表单实例编号
   * */
  @Query(" from FishPriceEntity f where f.formInstanceId = :formInstanceId")
  public FishPriceEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);



}