package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * FishTypeEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_FishTypeEntityRepository")
public interface FishTypeEntityRepository
        extends
        JpaRepository<FishTypeEntity, String>
        , JpaSpecificationExecutor<FishTypeEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct fishTypeEntity from FishTypeEntity fishTypeEntity "
          + " where fishTypeEntity.id=:id ")
  public FishTypeEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct fishTypeEntity from FishTypeEntity fishTypeEntity "
          + " where fishTypeEntity.formInstanceId=:formInstanceId ")
  public FishTypeEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from FishTypeEntity f where f.formInstanceId = :formInstanceId")
  public FishTypeEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据生效状态查询
   * @param state
   * @return
   */
  List<FishTypeEntity> findByEnableState(Integer state);
}