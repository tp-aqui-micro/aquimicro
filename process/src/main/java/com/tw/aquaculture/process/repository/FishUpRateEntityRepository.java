package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.base.FishUpRateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * FishUpRateEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_FishUpRateEntityRepository")
public interface FishUpRateEntityRepository
        extends
        JpaRepository<FishUpRateEntity, String>
        , JpaSpecificationExecutor<FishUpRateEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct fishUpRateEntity from FishUpRateEntity fishUpRateEntity "
          + " left join fetch fishUpRateEntity.pondType fishUpRateEntity_pondType "
          + " left join fetch fishUpRateEntity.fishType fishUpRateEntity_fishType "
          + " where fishUpRateEntity.id=:id ")
  public FishUpRateEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct fishUpRateEntity from FishUpRateEntity fishUpRateEntity "
          + " left join fetch fishUpRateEntity.pondType fishUpRateEntity_pondType "
          + " left join fetch fishUpRateEntity.fishType fishUpRateEntity_fishType "
          + " where fishUpRateEntity.formInstanceId=:formInstanceId ")
  public FishUpRateEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from FishUpRateEntity f where f.formInstanceId = :formInstanceId")
  public FishUpRateEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据塘口类型和鱼种查询
   * @param pondTypeId
   * @param fishTypeId
   * @return
   */
  @Query("select distinct fishUpRateEntity from FishUpRateEntity fishUpRateEntity "
          + " left join fetch fishUpRateEntity.pondType fishUpRateEntity_pondType "
          + " left join fetch fishUpRateEntity.fishType fishUpRateEntity_fishType "
          + " where fishUpRateEntity_pondType.id=:pondTypeId and fishUpRateEntity_fishType.id=:fishTypeId ")
  FishUpRateEntity findByPondTypeAndFishType(@Param("pondTypeId") String pondTypeId, @Param("fishTypeId") String fishTypeId);
}