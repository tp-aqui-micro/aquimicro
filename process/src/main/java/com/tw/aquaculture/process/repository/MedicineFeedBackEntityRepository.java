package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.MedicineFeedBackEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * MedicineFeedBackEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_MedicineFeedBackEntityRepository")
public interface MedicineFeedBackEntityRepository
    extends
      JpaRepository<MedicineFeedBackEntity, String>
      ,JpaSpecificationExecutor<MedicineFeedBackEntity>
  {
  /**
   * 按照上次用药反馈进行详情查询（包括关联信息）
   * @param parent 上次用药反馈
   * */
  @Query("select distinct medicineFeedBackEntity from MedicineFeedBackEntity medicineFeedBackEntity "
      + " left join fetch medicineFeedBackEntity.parent medicineFeedBackEntity_parent "
      + " left join fetch medicineFeedBackEntity.safeguard medicineFeedBackEntity_safeguard "
       + " where medicineFeedBackEntity_parent.id = :id")
  public Set<MedicineFeedBackEntity> findDetailsByParent(@Param("id") String id);
  /**
   * 按照动保进行详情查询（包括关联信息）
   * @param safeguard 动保
   * */
  @Query("select distinct medicineFeedBackEntity from MedicineFeedBackEntity medicineFeedBackEntity "
      + " left join fetch medicineFeedBackEntity.parent medicineFeedBackEntity_parent "
      + " left join fetch medicineFeedBackEntity.safeguard medicineFeedBackEntity_safeguard "
       + " where medicineFeedBackEntity_safeguard.id = :id")
  public Set<MedicineFeedBackEntity> findDetailsBySafeguard(@Param("id") String id);

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct medicineFeedBackEntity from MedicineFeedBackEntity medicineFeedBackEntity "
      + " left join fetch medicineFeedBackEntity.parent medicineFeedBackEntity_parent "
      + " left join fetch medicineFeedBackEntity.safeguard medicineFeedBackEntity_safeguard "
      + " where medicineFeedBackEntity.id=:id ")
  public MedicineFeedBackEntity findDetailsById(@Param("id") String id);



}