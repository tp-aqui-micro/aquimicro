package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.OutApplyCustomStateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * OutApplyCustomStateEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_OutApplyCustomStateEntityRepository")
public interface OutApplyCustomStateEntityRepository
        extends
        JpaRepository<OutApplyCustomStateEntity, String>
        , JpaSpecificationExecutor<OutApplyCustomStateEntity> {
  /**
   * 按照岀鱼申请id进行详情查询（包括关联信息）
   *
   * @param outFishApply 岀鱼申请id
   */
  @Query("select distinct outApplyCustomStateEntity from OutApplyCustomStateEntity outApplyCustomStateEntity "
          + " left join fetch outApplyCustomStateEntity.outFishApply outApplyCustomStateEntity_outFishApply "
          + " where outApplyCustomStateEntity_outFishApply.id = :id")
  public OutApplyCustomStateEntity findDetailsByOutFishApply(@Param("id") String id);

  /**
   * 按照客户进行详情查询（包括关联信息）
   *
   * @param customer 客户
   */
  @Query("select distinct outApplyCustomStateEntity from OutApplyCustomStateEntity outApplyCustomStateEntity "
          + " left join fetch outApplyCustomStateEntity.customer outApplyCustomStateEntity_customer "
          + " where outApplyCustomStateEntity_customer.id = :id")
  public OutApplyCustomStateEntity findDetailsByCustomer(@Param("id") String id);

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct outApplyCustomStateEntity from OutApplyCustomStateEntity outApplyCustomStateEntity "
          + " where outApplyCustomStateEntity.id=:id ")
  public OutApplyCustomStateEntity findDetailsById(@Param("id") String id);

  /**
   * 根据岀鱼申请id查询
   * @param outFishApplyId
   * @return
   */
  @Query("select distinct outApplyCustomStateEntity from OutApplyCustomStateEntity outApplyCustomStateEntity "
          + " left join fetch outApplyCustomStateEntity.outFishApply outApplyCustomStateEntity_outFishApply "
          + " left join fetch outApplyCustomStateEntity.customer customer "
          + " where outApplyCustomStateEntity_outFishApply.id = :outFishApplyId")
  List<OutApplyCustomStateEntity> findByOutFishApplyId(@Param("outFishApplyId") String outFishApplyId);

  /**
   * 根据岀鱼申请id和中标状态查询
   * @param outFishApplyId
   * @param state
   * @return
   */
  @Query("select distinct outApplyCustomStateEntity from OutApplyCustomStateEntity outApplyCustomStateEntity "
          + " left join outApplyCustomStateEntity.outFishApply outApplyCustomStateEntity_outFishApply "
          + " where outApplyCustomStateEntity_outFishApply.id = :outFishApplyId and outApplyCustomStateEntity.state=:state")
  List<OutApplyCustomStateEntity> findByOutFishApplyIdAndState(@Param("outFishApplyId") String outFishApplyId, @Param("state") int state);
}