package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.OutFishApplyEntity;
import com.tw.aquaculture.process.repository.internal.OutFishApplyEntityRepositoryParams;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * OutFishApplyEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_OutFishApplyEntityRepository")
public interface OutFishApplyEntityRepository
        extends
        JpaRepository<OutFishApplyEntity, String>
        , JpaSpecificationExecutor<OutFishApplyEntity>
        , OutFishApplyEntityRepositoryParams {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct outFishApplyEntity from OutFishApplyEntity outFishApplyEntity "
          + " left join fetch outFishApplyEntity.pond outFishApplyEntity_pond "
          + " left join fetch outFishApplyEntity_pond.base base "
          + " left join fetch base.baseOrg baseOrg "
          + " left join fetch outFishApplyEntity.workOrder outFishApplyEntity_workOrder "
          + " left join fetch outFishApplyEntity.remainInfos remainInfos "
          + " left join fetch remainInfos.fishType fishType "
          + " where outFishApplyEntity.id=:id ")
  public OutFishApplyEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct outFishApplyEntity from OutFishApplyEntity outFishApplyEntity "
          + " left join fetch outFishApplyEntity.pond outFishApplyEntity_pond "
          + " left join fetch outFishApplyEntity_pond.base base "
          + " left join fetch base.baseOrg baseOrg "
          + " left join fetch outFishApplyEntity.workOrder outFishApplyEntity_workOrder "
          + " where outFishApplyEntity.formInstanceId=:formInstanceId ")
  public OutFishApplyEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from OutFishApplyEntity f where f.formInstanceId = :formInstanceId")
  public OutFishApplyEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);


}