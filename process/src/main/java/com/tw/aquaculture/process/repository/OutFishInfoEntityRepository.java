package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.OutFishInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * OutFishInfoEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_OutFishInfoEntityRepository")
public interface OutFishInfoEntityRepository
        extends
        JpaRepository<OutFishInfoEntity, String>
        , JpaSpecificationExecutor<OutFishInfoEntity> {
  /**
   * 按照鱼种进行详情查询（包括关联信息）
   *
   * @param fishType 鱼种
   */
  @Query("select distinct outFishInfoEntity from OutFishInfoEntity outFishInfoEntity "
          + " left join fetch outFishInfoEntity.fishType outFishInfoEntity_fishType "
          + " left join fetch outFishInfoEntity.outFishRegist outFishInfoEntity_outFishRegist "
          + " where outFishInfoEntity_fishType.id = :id")
  public Set<OutFishInfoEntity> findDetailsByFishType(@Param("id") String id);

  /**
   * 按照出鱼登记进行详情查询（包括关联信息）
   *
   * @param outFishRegist 出鱼登记
   */
  @Query("select distinct outFishInfoEntity from OutFishInfoEntity outFishInfoEntity "
          + " left join fetch outFishInfoEntity.fishType outFishInfoEntity_fishType "
          + " left join fetch outFishInfoEntity.outFishRegist outFishInfoEntity_outFishRegist "
          + " where outFishInfoEntity_outFishRegist.id = :id")
  public Set<OutFishInfoEntity> findDetailsByOutFishRegist(@Param("id") String id);

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct outFishInfoEntity from OutFishInfoEntity outFishInfoEntity "
          + " left join fetch outFishInfoEntity.fishType outFishInfoEntity_fishType "
          + " left join fetch outFishInfoEntity.outFishRegist outFishInfoEntity_outFishRegist "
          + " where outFishInfoEntity.id=:id ")
  public OutFishInfoEntity findDetailsById(@Param("id") String id);

  /**
   * 根据塘口、鱼种和时间区间查询
   * @param pondId
   * @param fishTypeId
   * @param startTime
   * @param endTime
   * @return
   */
  @Query("select distinct outFishInfoEntity from OutFishInfoEntity outFishInfoEntity "
          + " left join fetch outFishInfoEntity.fishType fishType "
          + " left join fetch outFishInfoEntity.outFishRegist outFishRegist "
          + " left join fetch outFishRegist.pond pond "
          + " where pond.id=:pondId and fishType.id=:fishTypeId and outFishRegist.registTime>=:startTime and outFishRegist.registTime<:endTime ")
  List<OutFishInfoEntity> findByPondAndFishTypeAndTimes(@Param("pondId") String pondId, @Param("fishTypeId") String fishTypeId, @Param("startTime") Date startTime, @Param("endTime") Date endTime);
}