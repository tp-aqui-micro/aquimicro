package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.OutFishRegistEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * OutFishRegistEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_OutFishRegistEntityRepository")
public interface OutFishRegistEntityRepository
        extends
        JpaRepository<OutFishRegistEntity, String>
        , JpaSpecificationExecutor<OutFishRegistEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct outFishRegistEntity from OutFishRegistEntity outFishRegistEntity "
          + " left join fetch outFishRegistEntity.pond outFishRegistEntity_pond "
          + " left join fetch outFishRegistEntity_pond.base base "
          + " left join fetch outFishRegistEntity.customer outFishRegistEntity_customer "
          + " left join fetch outFishRegistEntity.groupLeader outFishRegistEntity_groupLeader "
          + " left join fetch outFishRegistEntity.workOrder outFishRegistEntity_workOrder "
          + " left join fetch outFishRegistEntity.outFishApply outFishRegistEntity_apply "
          + " where outFishRegistEntity.id=:id ")
  public OutFishRegistEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct outFishRegistEntity from OutFishRegistEntity outFishRegistEntity "
          + " left join fetch outFishRegistEntity.pond outFishRegistEntity_pond "
          + " left join fetch outFishRegistEntity_pond.base base "
          + " left join fetch outFishRegistEntity.customer outFishRegistEntity_customer "
          + " left join fetch outFishRegistEntity.groupLeader outFishRegistEntity_groupLeader "
          + " left join fetch outFishRegistEntity.workOrder outFishRegistEntity_workOrder "
          + " left join fetch outFishRegistEntity.outFishApply outFishRegistEntity_apply "
          + " where outFishRegistEntity.formInstanceId=:formInstanceId ")
  public OutFishRegistEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from OutFishRegistEntity f where f.formInstanceId = :formInstanceId")
  public OutFishRegistEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据塘口和时间区间查询
   *
   * @param pondId
   * @param startTime
   * @param endTime
   * @return
   */
  @Query(" select distinct regist from OutFishRegistEntity regist " +
          " left join fetch regist.pond pond " +
          " left join fetch regist.outFishInfos outFishInfos " +
          " where pond.id=:pondId and regist.registTime>=:startTime and regist.registTime<:endTime ")
  List<OutFishRegistEntity> findByPondAndTimes(@Param("pondId") String pondId, @Param("startTime") Date startTime, @Param("endTime") Date endTime);

  /**
   * 根据岀鱼申请查询岀鱼登记
   * @param outFishApplyId
   * @return
   */
  @Query("select distinct outFishRegistEntity from OutFishRegistEntity outFishRegistEntity "
          + " left join fetch outFishRegistEntity.pond outFishRegistEntity_pond "
          + " left join fetch outFishRegistEntity_pond.base base "
          + " left join fetch outFishRegistEntity.customer outFishRegistEntity_customer "
          + " left join fetch outFishRegistEntity.groupLeader outFishRegistEntity_groupLeader "
          + " left join fetch outFishRegistEntity.workOrder outFishRegistEntity_workOrder "
          + " left join outFishRegistEntity.outFishApply outFishRegistEntity_apply "
          + " where outFishRegistEntity_apply.id=:outFishApplyId ")
  List<OutFishRegistEntity> findOutFishRegistByOutFishApply(@Param("outFishApplyId") String outFishApplyId);

  /**
   * 根据工单编号、塘口查询
   * @param pondId
   * @param workOrderId
   * @return
   */
  @Query(" select distinct regist from OutFishRegistEntity regist " +
          " left join fetch regist.pond pond " +
          " left join fetch regist.workOrder workOrder " +
          " left join fetch regist.outFishInfos outFishInfos " +
          " where pond.id=:pondId and workOrder.id=:workOrderId ")
  List<OutFishRegistEntity> findByPondAndWorkOrderId(@Param("pondId") String pondId, @Param("workOrderId") String workOrderId);
}