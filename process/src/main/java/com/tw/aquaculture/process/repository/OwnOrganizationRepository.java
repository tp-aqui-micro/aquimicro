package com.tw.aquaculture.process.repository;

import com.bizunited.platform.core.entity.OrganizationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author 陈荣
 * @date 2019/11/30 11:42
 */
@Repository("OwnOrganizationRepository")
public interface OwnOrganizationRepository extends JpaRepository<OrganizationEntity, String>, JpaSpecificationExecutor<OrganizationEntity> {


  @Query("select distinct org from OrganizationEntity org "
          + "left join fetch org.parent "
          + "where org.id = :id")
  OrganizationEntity findDetailsById(@Param("id") String id);
}
