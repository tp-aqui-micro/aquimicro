package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.base.PondEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * PondEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_PondEntityRepository")
public interface PondEntityRepository
        extends
        JpaRepository<PondEntity, String>
        , JpaSpecificationExecutor<PondEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct pondEntity from PondEntity pondEntity "
          + " left join fetch pondEntity.base pondEntity_base "
          + " left join fetch pondEntity.pondType pondType "
          + " left join fetch pondEntity_base.baseOrg baseOrg "
          + " left join fetch pondEntity.stores stores "
          + " left join fetch pondEntity.tgroup pondEntity_tgroup "
          + " left join fetch pondEntity.groupLeader pondEntity_groupLeader "
          + " left join fetch pondEntity.worker pondEntity_worker "
          + " left join fetch pondEntity.technician pondEntity_technician "
          + " where pondEntity.id=:id ")
  public PondEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct pondEntity from PondEntity pondEntity "
          + " left join fetch pondEntity.base pondEntity_base "
          + " left join fetch pondEntity.pondType pondType "
          + " left join fetch pondEntity_base.baseOrg baseOrg "
          + " left join fetch pondEntity.stores stores "
          + " left join fetch pondEntity.tgroup pondEntity_tgroup "
          + " left join fetch pondEntity.groupLeader pondEntity_groupLeader "
          + " left join fetch pondEntity.worker pondEntity_worker "
          + " left join fetch pondEntity.technician pondEntity_technician "
          + " where pondEntity.formInstanceId=:formInstanceId ")
  public PondEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from PondEntity f where f.formInstanceId = :formInstanceId")
  public PondEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据基地id查询列表
   * @param baseId
   * @return
   */
  @Query("select distinct pondEntity from PondEntity pondEntity "
          + " left join fetch pondEntity.base pondEntity_base "
          + " left join fetch pondEntity.pondType pondType "
          + " left join fetch pondEntity_base.baseOrg baseOrg "
          + " left join fetch pondEntity.tgroup pondEntity_tgroup "
          + " left join fetch pondEntity.groupLeader pondEntity_groupLeader "
          + " left join fetch pondEntity.worker pondEntity_worker "
          + " left join fetch pondEntity.technician pondEntity_technician "
          + " where pondEntity_base.id=:baseId ")
  public Set<PondEntity> findDetailsByBase(@Param("baseId") String baseId);

  /**
   * 根据基地查询
   * @param baseId
   * @return
   */
  @Query("select distinct pondEntity from PondEntity pondEntity "
          + " left join fetch pondEntity.base pondEntity_base "
          + " left join fetch pondEntity_base.baseOrg baseOrg "
          + " left join fetch pondEntity.pondType pondType "
          + " left join fetch pondEntity.tgroup pondEntity_tgroup "
          + " left join fetch pondEntity.groupLeader pondEntity_groupLeader "
          + " left join fetch pondEntity.worker pondEntity_worker "
          + " left join fetch pondEntity.technician pondEntity_technician "
          + " where pondEntity_base.id=:baseId ")
  List<PondEntity> findByBaseId(@Param("baseId") String baseId);

  /**
   * 根据养殖小组查塘口
   *
   * @param twGroupId
   * @return
   */
  @Query("select distinct pondEntity from PondEntity pondEntity "
          + " left join fetch pondEntity.pondType pondType "
      + " left join fetch pondEntity.tgroup pondEntity_tgroup "
      + " where pondEntity_tgroup.id=:twGroupId ")
  Set<PondEntity> findByTwGroup(@Param("twGroupId") String twGroupId);

  /**
   * 根据基地和塘口状态查询
   * @param baseId
   * @param enableState
   * @return
   */
  @Query("select distinct pondEntity from PondEntity pondEntity "
          + " left join fetch pondEntity.base pondEntity_base "
          + " left join fetch pondEntity.pondType pondType "
          + " left join fetch pondEntity_base.baseOrg baseOrg "
          + " left join fetch pondEntity.tgroup pondEntity_tgroup "
          + " left join fetch pondEntity.groupLeader pondEntity_groupLeader "
          + " left join fetch pondEntity.worker pondEntity_worker "
          + " left join fetch pondEntity.technician pondEntity_technician "
          + " where pondEntity_base.id=:baseId and pondEntity.enableState=:enableState ")
  List<PondEntity> findPondsByBaseIdAndEnableState(@Param("baseId") String baseId, @Param("enableState") int enableState);

  /**
   * 根据塘口分类查询
   * @param pondTypeId
   * @return
   */
  @Query("select distinct pondEntity from PondEntity pondEntity "
          + " left join fetch pondEntity.pondType pondType "
          + " where pondType.id=:pondTypeId ")
  List<PondEntity> findByPondType(@Param("pondTypeId") String pondTypeId);
}