package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.base.PondTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * PondTypeEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_PondTypeEntityRepository")
public interface PondTypeEntityRepository
    extends
      JpaRepository<PondTypeEntity, String>
      ,JpaSpecificationExecutor<PondTypeEntity>
  {

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct pondTypeEntity from PondTypeEntity pondTypeEntity "
      + " left join fetch pondTypeEntity.parent pondTypeEntity_parent "
      + " where pondTypeEntity.id=:id ")
  public PondTypeEntity findDetailsById(@Param("id") String id);
  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   * @param formInstanceId 表单实例编号
   * */
  @Query("select distinct pondTypeEntity from PondTypeEntity pondTypeEntity "
      + " left join fetch pondTypeEntity.parent pondTypeEntity_parent "
      + " where pondTypeEntity.formInstanceId=:formInstanceId ")
  public PondTypeEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);
  /**
   * 按照表单实例编号进行查询
   * @param formInstanceId 表单实例编号
   * */
  @Query(" from PondTypeEntity f where f.formInstanceId = :formInstanceId")
  public PondTypeEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);



}