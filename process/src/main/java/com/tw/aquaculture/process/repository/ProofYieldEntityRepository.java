package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.ProofYieldEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * ProofYieldEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_ProofYieldEntityRepository")
public interface ProofYieldEntityRepository
        extends
        JpaRepository<ProofYieldEntity, String>
        , JpaSpecificationExecutor<ProofYieldEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct proofYieldEntity from ProofYieldEntity proofYieldEntity "
          + " left join fetch proofYieldEntity.pond proofYieldEntity_pond "
          + " left join fetch proofYieldEntity.proofYieldItems items "
          + " left join fetch proofYieldEntity_pond.base base "
          + " left join fetch proofYieldEntity.fishType proofYieldEntity_fishType "
          + " left join fetch proofYieldEntity.groupLeader proofYieldEntity_groupLeader "
          + " left join fetch proofYieldEntity.lastYield proofYieldEntity_lastYield "
          + " left join fetch proofYieldEntity.workOrder proofYieldEntity_workOrder "
          + " where proofYieldEntity.id=:id ")
  public ProofYieldEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct proofYieldEntity from ProofYieldEntity proofYieldEntity "
          + " left join fetch proofYieldEntity.pond proofYieldEntity_pond "
          + " left join fetch proofYieldEntity_pond.base base "
          + " left join fetch proofYieldEntity.fishType proofYieldEntity_fishType "
          + " left join fetch proofYieldEntity.groupLeader proofYieldEntity_groupLeader "
          + " left join fetch proofYieldEntity.lastYield proofYieldEntity_lastYield "
          + " left join fetch proofYieldEntity.workOrder proofYieldEntity_workOrder "
          + " where proofYieldEntity.formInstanceId=:formInstanceId ")
  public ProofYieldEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from ProofYieldEntity f where f.formInstanceId = :formInstanceId")
  public ProofYieldEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);


  /**
   * 根据工单查询
   *
   * @param workOrderId
   * @return
   */
  @Query("select distinct proofYieldEntity from ProofYieldEntity proofYieldEntity "
          + " left join fetch proofYieldEntity.workOrder workOrder "
          + " where workOrder.id=:workOrderId order by proofYieldEntity.proofTime desc")
  public List<ProofYieldEntity> findByWorkOrderDesc(@Param("workOrderId") String workOrderId);

  /**
   * 根据工单和鱼种id查询
   *
   * @param workOrderId
   * @return
   */
  @Query("select distinct proofYieldEntity from ProofYieldEntity proofYieldEntity "
          + " left join fetch proofYieldEntity.workOrder workOrder "
          + " left join fetch proofYieldEntity.fishType fishType "
          + " where workOrder.id=:workOrderId and fishType.id=:fishTypeId " +
          " order by proofYieldEntity.createTime desc ")
  List<ProofYieldEntity> findByWorkOrderAndFishTyeIdDesc(@Param("workOrderId") String workOrderId, @Param("fishTypeId") String fishTypeId);

  /**
   * 根据塘口和工单查询详情
   *
   * @param workOrderId
   * @return
   */
  @Query("select distinct proofYieldEntity from ProofYieldEntity proofYieldEntity "
          + " left join fetch proofYieldEntity.pond pond "
          + " left join fetch proofYieldEntity.workOrder workOrder "
          + " left join fetch proofYieldEntity.fishType fishType "
          + " where workOrder.id=:workOrderId and pond.id=:pondId " +
          " order by proofYieldEntity.createTime desc ")
  List<ProofYieldEntity> findDetailsByPondAndWorkOrder(@Param("pondId") String pondId, @Param("workOrderId") String workOrderId);

  /**
   * 根据塘口和时间区间查询
   *
   * @param pondId
   * @param startTime
   * @param endTime
   * @return
   */
  @Query("select distinct proofYieldEntity from ProofYieldEntity proofYieldEntity "
          + " left join fetch proofYieldEntity.pond proofYieldEntity_pond "
          + " left join fetch proofYieldEntity_pond.base base "
          + " left join fetch proofYieldEntity.fishType proofYieldEntity_fishType "
          + " left join fetch proofYieldEntity.groupLeader proofYieldEntity_groupLeader "
          + " left join fetch proofYieldEntity.lastYield proofYieldEntity_lastYield "
          + " left join fetch proofYieldEntity.workOrder proofYieldEntity_workOrder "
          + " where proofYieldEntity_pond.id=:pondId and proofYieldEntity.proofTime>=:startTime and proofYieldEntity.proofTime<:endTime order by proofYieldEntity.proofTime ")
  List<ProofYieldEntity> findByPondAndTimes(@Param("pondId") String pondId, @Param("startTime") Date startTime, @Param("endTime") Date endTime);

  /**
   * 根据塘口和时间区间查询
   * @param pondId
   * @param endTime
   * @return
   */
  @Query("select distinct proofYieldEntity from ProofYieldEntity proofYieldEntity "
          + " left join fetch proofYieldEntity.pond proofYieldEntity_pond "
          + " left join fetch proofYieldEntity_pond.base base "
          + " left join fetch proofYieldEntity.fishType proofYieldEntity_fishType "
          + " left join fetch proofYieldEntity.groupLeader proofYieldEntity_groupLeader "
          + " left join fetch proofYieldEntity.lastYield proofYieldEntity_lastYield "
          + " left join fetch proofYieldEntity.workOrder proofYieldEntity_workOrder "
          + " where proofYieldEntity_pond.id=:pondId and proofYieldEntity.proofTime<:endTime order by proofYieldEntity.proofTime ")
  List<ProofYieldEntity> findByPondAndEndTime(@Param("pondId") String pondId, @Param("endTime") Date endTime);

  /**
   * 根据塘口查询
   * @param pondId
   * @return
   */
  @Query("select distinct proofYieldEntity from ProofYieldEntity proofYieldEntity "
          + " left join fetch proofYieldEntity.pond proofYieldEntity_pond "
          + " left join fetch proofYieldEntity_pond.base base "
          + " left join fetch proofYieldEntity.fishType proofYieldEntity_fishType "
          + " left join fetch proofYieldEntity.groupLeader proofYieldEntity_groupLeader "
          + " left join fetch proofYieldEntity.lastYield proofYieldEntity_lastYield "
          + " left join fetch proofYieldEntity.workOrder proofYieldEntity_workOrder "
          + " where proofYieldEntity_pond.id=:pondId order by proofYieldEntity.proofTime ")
  List<ProofYieldEntity> findByPond(@Param("pondId") String pondId);
}