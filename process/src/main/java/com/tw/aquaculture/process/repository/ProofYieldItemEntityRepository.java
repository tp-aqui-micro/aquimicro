package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.ProofYieldItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * ProofYieldItemEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_ProofYieldItemEntityRepository")
public interface ProofYieldItemEntityRepository
    extends
      JpaRepository<ProofYieldItemEntity, String>
      ,JpaSpecificationExecutor<ProofYieldItemEntity>
  {
  /**
   * 按照打样估产进行详情查询（包括关联信息）
   * @param proofYield 打样估产
   * */
  @Query("select distinct proofYieldItemEntity from ProofYieldItemEntity proofYieldItemEntity "
      + " left join fetch proofYieldItemEntity.proofYield proofYieldItemEntity_proofYield "
       + " where proofYieldItemEntity_proofYield.id = :id")
  public Set<ProofYieldItemEntity> findDetailsByProofYield(@Param("id") String id);

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct proofYieldItemEntity from ProofYieldItemEntity proofYieldItemEntity "
      + " left join fetch proofYieldItemEntity.proofYield proofYieldItemEntity_proofYield "
      + " where proofYieldItemEntity.id=:id ")
  public ProofYieldItemEntity findDetailsById(@Param("id") String id);



}