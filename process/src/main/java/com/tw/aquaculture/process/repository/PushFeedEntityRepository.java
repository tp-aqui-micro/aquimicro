package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.PushFeedEntity;
import com.tw.aquaculture.process.repository.internal.PushFeedEntityRepositoryCusm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * PushFeedEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_PushFeedEntityRepository")
public interface PushFeedEntityRepository
        extends
        JpaRepository<PushFeedEntity, String>
        , JpaSpecificationExecutor<PushFeedEntity>,
        PushFeedEntityRepositoryCusm {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct pushFeedEntity from PushFeedEntity pushFeedEntity "
          + " left join fetch pushFeedEntity.pond pushFeedEntity_pond "
          + " left join fetch pushFeedEntity.store store "
          + " left join fetch pushFeedEntity_pond.base base "
          + " left join fetch pushFeedEntity.workOrder pushFeedEntity_workOrder "
          + " where pushFeedEntity.id=:id ")
  public PushFeedEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct pushFeedEntity from PushFeedEntity pushFeedEntity "
          + " left join fetch pushFeedEntity.pond pushFeedEntity_pond "
          + " left join fetch pushFeedEntity.store store "
          + " left join fetch pushFeedEntity_pond.base base "
          + " left join fetch pushFeedEntity.feed feed "
          + " left join fetch pushFeedEntity.workOrder pushFeedEntity_workOrder "
          + " where pushFeedEntity.formInstanceId=:formInstanceId ")
  public PushFeedEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from PushFeedEntity f where f.formInstanceId = :formInstanceId")
  public PushFeedEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);


  /**
   * 统计相同物料在某时间段内消耗量
   *
   * @param baseId
   * @param matterId
   * @param start
   * @param end
   * @return
   */
  @Query("select sum(pushFeedEntity.pushWeight) from PushFeedEntity pushFeedEntity "
          + " left join pushFeedEntity.pond pushFeedEntity_pond "
          + " left join pushFeedEntity_pond.base base "
          + " left join pushFeedEntity.feed feed "
          + " where base.id=:baseId and feed.id=:matterId and pushFeedEntity.createTime>=:start and pushFeedEntity.createTime<:end  ")
  Double sumConsumeByTimes(@Param("baseId") String baseId, @Param("matterId") String matterId, @Param("start") Date start, @Param("end") Date end);

  /**
   * 根据塘口和时间区间查询
   * @param pondId
   * @param startTime
   * @param endTime
   * @return
   */
  @Query("select pushFeedEntity from PushFeedEntity pushFeedEntity "
          + " left join fetch pushFeedEntity.pond pushFeedEntity_pond "
          + " left join fetch pushFeedEntity.feed feed "
          + " where pushFeedEntity_pond.id=:pondId and pushFeedEntity.pushTime>=:startTime and pushFeedEntity.pushTime<:endTime  ")
  List<PushFeedEntity> findByPondAndTimes(@Param("pondId") String pondId, @Param("startTime") Date startTime, @Param("endTime") Date endTime);
}