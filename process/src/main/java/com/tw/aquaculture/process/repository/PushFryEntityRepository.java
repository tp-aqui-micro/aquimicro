package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.PushFryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * PushFryEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_PushFryEntityRepository")
public interface PushFryEntityRepository
        extends
        JpaRepository<PushFryEntity, String>
        , JpaSpecificationExecutor<PushFryEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct pushFryEntity from PushFryEntity pushFryEntity "
          + " left join fetch pushFryEntity.pond pushFryEntity_pond "
          + " left join fetch pushFryEntity_pond.base base "
          + " left join fetch pushFryEntity.fishType pushFryEntity_fishType "
          + " left join fetch pushFryEntity.workOrder pushFryEntity_workOrder "
          + " where pushFryEntity.id=:id ")
  public PushFryEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct pushFryEntity from PushFryEntity pushFryEntity "
          + " left join fetch pushFryEntity.pond pushFryEntity_pond "
          + " left join fetch pushFryEntity_pond.base base "
          + " left join fetch pushFryEntity.fishType pushFryEntity_fishType "
          + " left join fetch pushFryEntity.workOrder pushFryEntity_workOrder "
          + " where pushFryEntity.formInstanceId=:formInstanceId ")
  public PushFryEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from PushFryEntity f where f.formInstanceId = :formInstanceId")
  public PushFryEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据投苗时间和塘口查询
   *
   * @param pondId
   * @param startTime
   * @param endTime
   * @return
   */
  @Query(" select distinct p from PushFryEntity p " +
          " left join fetch p.pond pond " +
          " where pond.id=:pondId and p.pushTime>=:startTime and p.pushTime<:endTime")
  List<PushFryEntity> findByPondAndTime(@Param("pondId") String pondId, @Param("startTime") Date startTime, @Param("endTime") Date endTime);

  /**
   * 按鱼种统计、塘口查询养殖周期内记录
   * @param pondId
   * @param startTime
   * @param endTime
   * @param fishTypeId
   * @return
   */
  @Query(" select distinct p from PushFryEntity p " +
          " left join fetch p.pond pond " +
          " left join fetch p.fishType fishType " +
          " where pond.id=:pondId and p.pushTime>=:startTime and p.pushTime<:endTime and fishType.id=:fishTypeId")
  List<PushFryEntity> findByPondAndTimesAndFishType(@Param("pondId") String pondId, @Param("startTime") Date startTime, @Param("endTime") Date endTime, @Param("fishTypeId") String fishTypeId);

  /**
   * 根据工单编号、塘口、鱼种查询
   * @param pondId
   * @param workOrderId
   * @param fishTypeId
   * @return
   */
  @Query(" select distinct p from PushFryEntity p " +
          " left join fetch p.pond pond " +
          " left join fetch p.workOrder workOrder " +
          " left join fetch p.fishType fishType " +
          " where pond.id=:pondId and workOrder.id=:workOrderId and fishType.id=:fishTypeId")
  List<PushFryEntity> findByPondAndWorkOrderIdAndFishType(@Param ("pondId") String pondId, @Param("workOrderId") String workOrderId, @Param("fishTypeId") String fishTypeId);

  /**
   * 根据工单编号、塘口、鱼种查询
   * @param pondId
   * @param workOrderId
   * @return
   */
  @Query(" select distinct p from PushFryEntity p " +
          " left join p.pond pond " +
          " left join p.workOrder workOrder " +
          " where pond.id=:pondId and workOrder.id=:workOrderId order by p.pushTime desc")
  List<PushFryEntity> findByPondAndWorkOrderId(@Param("pondId") String pondId, @Param("workOrderId") String workOrderId);

  /**
   * 根据投苗时间和塘口查询
   *
   * @param pondId
   * @param startTime
   * @param endTime
   * @return
   */
  @Query(" select distinct p from PushFryEntity p " +
          " left join fetch p.pond pond " +
          " left join fetch p.fishType fishType " +
          " where pond.id=:pondId and p.pushTime>=:startTime and p.pushTime<:endTime")
  List<PushFryEntity> findSimpleByPondAndTimes(@Param("pondId") String pondId, @Param("startTime") Date startTime, @Param("endTime") Date endTime);

  /**
   * 根据塘口id查询
   * @param pondId
   * @return
   */
  @Query(" select distinct p from PushFryEntity p " +
          " left join p.pond pond " +
          " left join p.fishType fishType " +
          " where pond.id=:pondId ")
  List<PushFryEntity> findByPond(@Param("pondId") String pondId);

  /**
   * 根据塘口、鱼种和时间区间查询
   * @param pondId
   * @param fishTypeId
   * @param startTime
   * @param endTime
   * @return
   */
  @Query(" select distinct p from PushFryEntity p " +
          " left join p.pond pond " +
          " left join p.fishType fishType " +
          " where pond.id=:pondId and fishType.id=:fishTypeId and p.pushTime>=:startTime and p.pushTime<:endTime ")
  List<PushFryEntity> findByPondAndFishTypeAndTimes(@Param("pondId") String pondId, @Param("fishTypeId") String fishTypeId, @Param("startTime") Date startTime, @Param("endTime") Date endTime);
}