package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.RemainInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * RemainInfoEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_RemainInfoEntityRepository")
public interface RemainInfoEntityRepository
    extends
      JpaRepository<RemainInfoEntity, String>
      ,JpaSpecificationExecutor<RemainInfoEntity>
  {
  /**
   * 按照鱼种进行详情查询（包括关联信息）
   * @param fishType 鱼种
   * */
  @Query("select distinct remainInfoEntity from RemainInfoEntity remainInfoEntity "
      + " left join fetch remainInfoEntity.fishType remainInfoEntity_fishType "
      + " left join fetch remainInfoEntity.outFishApply remainInfoEntity_outFishApply "
       + " where remainInfoEntity_fishType.id = :id")
  public Set<RemainInfoEntity> findDetailsByFishType(@Param("id") String id);
  /**
   * 按照出鱼申请进行详情查询（包括关联信息）
   * @param outFishApply 出鱼申请
   * */
  @Query("select distinct remainInfoEntity from RemainInfoEntity remainInfoEntity "
      + " left join fetch remainInfoEntity.fishType remainInfoEntity_fishType "
      + " left join fetch remainInfoEntity.outFishApply remainInfoEntity_outFishApply "
       + " where remainInfoEntity_outFishApply.id = :id")
  public Set<RemainInfoEntity> findDetailsByOutFishApply(@Param("id") String id);

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct remainInfoEntity from RemainInfoEntity remainInfoEntity "
      + " left join fetch remainInfoEntity.fishType remainInfoEntity_fishType "
      + " left join fetch remainInfoEntity.outFishApply remainInfoEntity_outFishApply "
      + " where remainInfoEntity.id=:id ")
  public RemainInfoEntity findDetailsById(@Param("id") String id);



}