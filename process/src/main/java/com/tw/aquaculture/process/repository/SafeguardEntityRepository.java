package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.SafeguardEntity;
import com.tw.aquaculture.process.repository.internal.SafeguardEntityRepositoryCusm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * SafeguardEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_SafeguardEntityRepository")
public interface SafeguardEntityRepository
        extends
        JpaRepository<SafeguardEntity, String>
        , JpaSpecificationExecutor<SafeguardEntity>
        , SafeguardEntityRepositoryCusm {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct safeguardEntity from SafeguardEntity safeguardEntity "
          + " left join fetch safeguardEntity.pond safeguardEntity_pond "
          + " left join fetch safeguardEntity.medicine medicine "
          + " left join fetch safeguardEntity.medicineFeedBacks medicineFeedBacks "
          + " left join fetch safeguardEntity.store store "
          + " left join fetch safeguardEntity_pond.base base "
          + " left join fetch safeguardEntity.workOrder safeguardEntity_workOrder "
          + " where safeguardEntity.id=:id ")
  public SafeguardEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct safeguardEntity from SafeguardEntity safeguardEntity "
          + " left join fetch safeguardEntity.pond safeguardEntity_pond "
          + " left join fetch safeguardEntity.medicine medicine "
          + " left join fetch safeguardEntity.medicineFeedBacks medicineFeedBacks "
          + " left join fetch safeguardEntity.store store "
          + " left join fetch safeguardEntity_pond.base base "
          + " left join fetch safeguardEntity.workOrder safeguardEntity_workOrder "
          + " where safeguardEntity.formInstanceId=:formInstanceId ")
  public SafeguardEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from SafeguardEntity f where f.formInstanceId = :formInstanceId")
  public SafeguardEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 查询没有绑定工单的动保
   * @param pondId
   * @return
   */
  @Query("select distinct safeguardEntity from SafeguardEntity safeguardEntity "
          + " left join fetch safeguardEntity.pond safeguardEntity_pond "
          + " left join fetch safeguardEntity.medicine medicine "
          + " left join fetch safeguardEntity.medicineFeedBacks medicineFeedBacks "
          + " left join fetch safeguardEntity.store store "
          + " left join fetch safeguardEntity_pond.base base "
          + " left join fetch safeguardEntity.workOrder safeguardEntity_workOrder "
          + " where safeguardEntity_pond.id=:pondId and safeguardEntity.workOrder is null order by safeguardEntity.createTime ")
  List<SafeguardEntity> findNoneWorkOrderByPond(@Param("pondId") String pondId);
}