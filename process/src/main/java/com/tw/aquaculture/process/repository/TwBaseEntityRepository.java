package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.process.repository.internal.TwBaseEntityRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * TwBaseEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_TwBaseEntityRepository")
public interface TwBaseEntityRepository
        extends
        JpaRepository<TwBaseEntity, String>
        , JpaSpecificationExecutor<TwBaseEntity>
        , TwBaseEntityRepositoryCustom {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct twBaseEntity from TwBaseEntity twBaseEntity "
          + " left join fetch twBaseEntity.baseOrg twBaseEntity_baseOrg "
          + " left join fetch twBaseEntity_baseOrg.parent parent "
          + " left join fetch twBaseEntity.leader twBaseEntity_leader "
          + " where twBaseEntity.id=:id ")
  public TwBaseEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct twBaseEntity from TwBaseEntity twBaseEntity "
          + " left join fetch twBaseEntity.baseOrg twBaseEntity_baseOrg "
          + " left join fetch twBaseEntity_baseOrg.parent parent "
          + " left join fetch twBaseEntity.leader twBaseEntity_leader "
          + " where twBaseEntity.formInstanceId=:formInstanceId ")
  public TwBaseEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from TwBaseEntity f where f.formInstanceId = :formInstanceId")
  public TwBaseEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 查询全部基地
   *
   * @return
   */
  @Query("select distinct twBaseEntity from TwBaseEntity twBaseEntity "
          + " left join fetch twBaseEntity.baseOrg twBaseEntity_baseOrg "
          + " left join fetch twBaseEntity.leader twBaseEntity_leader "
          + " left join fetch twBaseEntity.ponds twBaseEntity_ponds")
  public List<TwBaseEntity> findDetailAll();

  /**
   * 根据组织基地查询
   * @param baseOrgId
   * @return
   */
  @Query("select distinct twBaseEntity from TwBaseEntity twBaseEntity "
          + " left join fetch twBaseEntity.baseOrg twBaseEntity_baseOrg "
          + " left join fetch twBaseEntity.leader twBaseEntity_leader "
          + " left join fetch twBaseEntity.ponds twBaseEntity_ponds " +
          " where twBaseEntity_baseOrg.id=:baseOrgId ")
  List<TwBaseEntity> findDetailsByBaseOrgId(@Param("baseOrgId") String baseOrgId);
}