package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.base.TwGroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * TwGroupEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_TwGroupEntityRepository")
public interface TwGroupEntityRepository
    extends
      JpaRepository<TwGroupEntity, String>
      ,JpaSpecificationExecutor<TwGroupEntity>
  {

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct twGroupEntity from TwGroupEntity twGroupEntity "
      + " left join fetch twGroupEntity.groupLeader twGroupEntity_groupLeader "
      + " left join fetch twGroupEntity.base twGroupEntity_base "
      + " left join fetch twGroupEntity.groupOrg twGroupEntity_group_org "
      + " where twGroupEntity.id=:id ")
  public TwGroupEntity findDetailsById(@Param("id") String id);
  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   * @param formInstanceId 表单实例编号
   * */
  @Query("select distinct twGroupEntity from TwGroupEntity twGroupEntity "
      + " left join fetch twGroupEntity.groupLeader twGroupEntity_groupLeader "
      + " left join fetch twGroupEntity.base twGroupEntity_base "
      + " left join fetch twGroupEntity.groupOrg twGroupEntity_group_org "
      + " where twGroupEntity.formInstanceId=:formInstanceId ")
  public TwGroupEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);
  /**
   * 按照表单实例编号进行查询
   * @param formInstanceId 表单实例编号
   * */
  @Query(" from TwGroupEntity f where f.formInstanceId = :formInstanceId")
  public TwGroupEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);


  @Query("select distinct tg from TwGroupEntity tg left join fetch tg.base left join fetch tg.groupOrg ")
  List<TwGroupEntity> findAllDetail();

}