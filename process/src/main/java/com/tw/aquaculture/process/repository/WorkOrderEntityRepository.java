package com.tw.aquaculture.process.repository;

import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import com.tw.aquaculture.process.repository.internal.WorkOrderEntityRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * WorkOrderEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_WorkOrderEntityRepository")
public interface WorkOrderEntityRepository
        extends
        JpaRepository<WorkOrderEntity, String>
        , JpaSpecificationExecutor<WorkOrderEntity> ,
        WorkOrderEntityRepositoryCustom {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct workOrderEntity from WorkOrderEntity workOrderEntity "
          + " left join fetch workOrderEntity.pond workOrderEntity_pond "
          + " left join fetch workOrderEntity_pond.base base "
          + " where workOrderEntity.id=:id ")
  public WorkOrderEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct workOrderEntity from WorkOrderEntity workOrderEntity "
          + " left join fetch workOrderEntity.pond workOrderEntity_pond "
          + " left join fetch workOrderEntity_pond.base base "
          + " where workOrderEntity.formInstanceId=:formInstanceId ")
  public WorkOrderEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from WorkOrderEntity f where f.formInstanceId = :formInstanceId")
  public WorkOrderEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据塘口和状态查询
   *
   * @param pondId
   * @param state
   * @return
   */
  @Query(" select distinct w from WorkOrderEntity w " +
          " left join fetch w.pond p " +
          " where p.id=:pondId and w.enableState=:state order by w.createTime")
  public List<WorkOrderEntity> findByStateAndPond(@Param("pondId") String pondId, @Param("state") int state);

  /**
   * 根据塘口和创建时间两个条件约束查询工单
   * @param pondId
   * @param finishTime
   * @return
   */
  @Query(" select distinct w from WorkOrderEntity w " +
          " left join fetch w.pond p " +
          " where p.id=:pondId and w.createTime<:finishTime order by w.createTime")
  public List<WorkOrderEntity> findByPondAndTime(@Param("pondId") String pondId, @Param("finishTime") Date finishTime);

  /**
   * 根据时间、堂口和状态查询
   * @param pondId
   * @param endTime
   * @param state
   * @return
   */
  @Query(" select distinct w from WorkOrderEntity w " +
          " left join fetch w.pond p " +
          " where p.id=:pondId and w.createTime<:endTime and w.enableState=:state order by w.createTime")
  List<WorkOrderEntity> findByPondAndTimeAndState(@Param("pondId") String pondId, @Param("endTime") Date endTime, @Param("state") Integer state);
}