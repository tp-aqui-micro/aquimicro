package com.tw.aquaculture.process.repository.internal;

import com.tw.aquaculture.common.entity.process.FishDamageEntity;

import java.util.Date;
import java.util.List;

/**
 * @author 陈荣
 * @date 2020/1/7 19:38
 */
public interface FishDamageEntityRepositoryCusm {

  List<FishDamageEntity> findByConditions(Date startTime, Date endTime, String baseId, String pondId, String fishTypeId);
}
