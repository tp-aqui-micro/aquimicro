package com.tw.aquaculture.process.repository.internal;

import com.tw.aquaculture.common.entity.process.FishDamageEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author 陈荣
 * @date 2020/1/7 19:38
 */
public class FishDamageEntityRepositoryImpl implements FishDamageEntityRepositoryCusm{

  @Autowired
  EntityManager entityManager;

  @Override
  public List<FishDamageEntity> findByConditions(Date startTime, Date endTime, String baseId, String pondId, String fishTypeId) {
    if (StringUtils.isBlank(baseId)) {
      return null;
    }
    StringBuilder hql = new StringBuilder(" select distinct fishDamageEntity from FishDamageEntity fishDamageEntity ");
    hql.append(" left join fetch fishDamageEntity.pond pond ");
    hql.append(" left join fetch pond.base base ");
    hql.append(" left join fetch fishDamageEntity.fishType fishType ");
    hql.append(" left join fetch fishDamageEntity.workOrder fishDamageEntity_workOrder ");
    hql.append(" where 1=1 ");
    StringBuilder condition = new StringBuilder();
    List<String> baseIds = null;
    List<String> pondIds = null;
    List<String> fishTypeIds = null;
    if (startTime != null) {
      condition.append(" and fishDamageEntity.damageTime>=:startTime ");
    }
    if (endTime != null) {
      condition.append(" and fishDamageEntity.damageTime<:endTime ");
    }
    if (StringUtils.isNotBlank(baseId)) {
      baseIds = Arrays.asList(baseId.split(","));
      condition.append(" and base.id in (:baseIds) ");
    }
    if (StringUtils.isNotBlank(pondId)) {
      pondIds = Arrays.asList(pondId.split(","));
      condition.append(" and pond.id in (:pondIds) ");
    }
    if (StringUtils.isNotBlank(fishTypeId)) {
      fishTypeIds = Arrays.asList(fishTypeId.split(","));
      condition.append(" and fishType.id in (:fishTypeIds) ");
    }
    hql.append(condition.toString());
    hql.append(" order by fishDamageEntity.damageTime ");
    Query query = entityManager.createQuery(hql.toString());
    if (startTime != null) {
      query.setParameter("startTime", startTime);
    }
    if (endTime != null) {
      query.setParameter("endTime", endTime);
    }
    if (StringUtils.isNotBlank(baseId)) {
      query.setParameter("baseIds", baseIds);
    }
    if (StringUtils.isNotBlank(pondId)) {
      query.setParameter("pondIds", pondIds);
    }
    if (StringUtils.isNotBlank(fishTypeId)) {
      query.setParameter("fishTypeIds", fishTypeIds);
    }
    List<FishDamageEntity> result = query.getResultList();
    return result;
  }
}
