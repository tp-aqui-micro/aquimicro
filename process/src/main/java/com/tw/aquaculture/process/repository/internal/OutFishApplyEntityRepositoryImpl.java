package com.tw.aquaculture.process.repository.internal;

import com.tw.aquaculture.common.entity.process.OutFishApplyEntity;
import com.tw.aquaculture.common.vo.outfishapply.PageParam;
import com.tw.aquaculture.common.vo.outfishapply.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author: zengxingwang
 * @date: 2019/12/31 14:31
 * @description:
 */
@Repository("OutFishApplyEntityRepositoryParams")
public class OutFishApplyEntityRepositoryImpl implements OutFishApplyEntityRepositoryParams {

  @Autowired
  EntityManager entityManager;

  /**
   * 移动端根据基地名称、塘口名称、时间分页查询出鱼申请信息
   *
   * @param params
   * @param pageable
   * @return
   */
  @Override
  public PageResult<List<OutFishApplyEntity>> findDetailByOrgBaseNameOrPondNameOrTiem(Map<String, Object> params, PageParam pageable) {
    StringBuilder hql = new StringBuilder("select distinct outFishApplyEntity from OutFishApplyEntity outFishApplyEntity");
    hql.append(" left join fetch outFishApplyEntity.pond pond ");
    hql.append(" left join fetch outFishApplyEntity.customerInfos outFishApplyEntity_customerInfos ");
    hql.append(" where 1=1 ");
    StringBuilder countHql = new StringBuilder("select count(distinct outFishApplyEntity) from OutFishApplyEntity outFishApplyEntity ");
    countHql.append(" left join outFishApplyEntity.pond pond ");
    countHql.append(" left join outFishApplyEntity.customerInfos outFishApplyEntity_customerInfos ");
    countHql.append(" where 1=1 ");
    StringBuilder condition = new StringBuilder();
    if (params != null) {
      if (params.get("pondId") != null) {
        condition.append(" and pond.id = :pondId");
      }
      if (params.get("timeBegin") != null) {
        condition.append(" and outFishApplyEntity.applyTime > :timeBegin ");
      }
      if (params.get("endBegin") != null) {
        condition.append(" and outFishApplyEntity.applyTime < :endBegin ");
      }
    }
    countHql.append(condition.toString());
    hql.append(condition.toString());
    Query query = entityManager.createQuery(hql.toString());
    Query countQuery = entityManager.createQuery(countHql.toString());
    if (params != null) {
      params.forEach((k, v) -> {
        if (v != null) {
          query.setParameter(k, v);
          countQuery.setParameter(k, v);
        }
      });
    }
    query.setFirstResult(pageable.getPage() * pageable.getSize());
    query.setMaxResults(pageable.getSize());
    List<OutFishApplyEntity> result = new ArrayList<>();
    long count = (long) countQuery.getResultList().get(0);
    if (count > 0) {
      result = query.getResultList();
    }
    return new PageResult(result, pageable, count);
  }
}
