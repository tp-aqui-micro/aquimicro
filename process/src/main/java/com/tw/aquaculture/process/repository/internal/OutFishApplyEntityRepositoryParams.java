package com.tw.aquaculture.process.repository.internal;

import com.tw.aquaculture.common.entity.process.OutFishApplyEntity;
import com.tw.aquaculture.common.vo.outfishapply.PageParam;
import com.tw.aquaculture.common.vo.outfishapply.PageResult;

import java.util.List;
import java.util.Map;

/**
 * @author: zengxingwang
 * @date: 2019/12/31 14:31
 * @description:
 */
public interface OutFishApplyEntityRepositoryParams {

  /**
   * 移动端根据基地名称、塘口名称、时间分页查询出鱼申请信息
   * @param params
   * @param pageable
   * @return
   */
  public PageResult<List<OutFishApplyEntity>> findDetailByOrgBaseNameOrPondNameOrTiem(Map<String, Object> params, PageParam pageable);
}
