package com.tw.aquaculture.process.repository.internal;

import com.tw.aquaculture.common.entity.process.PushFeedEntity;

import java.util.Date;
import java.util.List;

/**
 * 饲料投喂自定义sql
 * @author 陈荣
 * @date 2020/1/7 10:32
 */
public interface PushFeedEntityRepositoryCusm {

  /**
   * 条件查询饲料投喂
   * @param startTime
   * @param endTime
   * @param baseId
   * @param pondId
   * @param matterId
   * @return
   */
  List<PushFeedEntity> findByConditions(Date startTime, Date endTime, String baseId, String pondId, String matterId);

}
