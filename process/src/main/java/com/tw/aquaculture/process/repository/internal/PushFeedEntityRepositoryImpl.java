package com.tw.aquaculture.process.repository.internal;

import com.tw.aquaculture.common.entity.process.PushFeedEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author 陈荣
 * @date 2020/1/7 10:33
 */
public class PushFeedEntityRepositoryImpl implements PushFeedEntityRepositoryCusm{

  @Autowired
  EntityManager entityManager;

  @Override
  public List<PushFeedEntity> findByConditions(Date startTime, Date endTime, String baseId, String pondId, String matterId) {
    if (StringUtils.isBlank(baseId)) {
      return null;
    }
    StringBuilder hql = new StringBuilder(" select distinct pushFeedEntity from PushFeedEntity pushFeedEntity ");
    hql.append(" left join fetch pushFeedEntity.pond pond ");
    hql.append(" left join fetch pushFeedEntity.store store ");
    hql.append(" left join fetch pond.base base ");
    hql.append(" left join fetch pushFeedEntity.feed feed ");
    hql.append(" left join fetch pushFeedEntity.workOrder workOrder ");
    hql.append(" where 1=1 ");
    StringBuilder condition = new StringBuilder();
    List<String> baseIds = null;
    List<String> pondIds = null;
    List<String> matterIds = null;
    if(startTime!=null){
      condition.append(" and pushFeedEntity.pushTime>=:startTime ");
    }
    if(endTime!=null){
      condition.append(" and pushFeedEntity.pushTime<:endTime ");
    }
    if(StringUtils.isNotBlank(baseId)){
      baseIds = Arrays.asList(baseId.split(","));
      condition.append(" and base.id in (:baseIds) ");
    }
    if(StringUtils.isNotBlank(pondId)){
      pondIds = Arrays.asList(pondId.split(","));
      condition.append(" and pond.id in (:pondIds) ");
    }
    if(StringUtils.isNotBlank(matterId)){
      matterIds = Arrays.asList(matterId.split(","));
      condition.append(" and feed.id in (:matterIds) ");
    }
    hql.append(condition.toString());
    hql.append(" order by pushFeedEntity.pushTime ");
    Query query = entityManager.createQuery(hql.toString());
    if(startTime!=null){
      query.setParameter("startTime", startTime);
    }
    if(endTime!=null){
      query.setParameter("endTime", endTime);
    }
    if(StringUtils.isNotBlank(baseId)){
      query.setParameter("baseIds", baseIds);
    }
    if(StringUtils.isNotBlank(pondId)){
      query.setParameter("pondIds", pondIds);
    }
    if(StringUtils.isNotBlank(matterId)){
      query.setParameter("matterIds", matterIds);
    }
    List<PushFeedEntity> result = query.getResultList();
    return result;
  }
}
