package com.tw.aquaculture.process.repository.internal;

import com.tw.aquaculture.common.entity.process.SafeguardEntity;

import java.util.Date;
import java.util.List;

/**
 * @author 陈荣
 * @date 2020/1/7 16:35
 */
public interface SafeguardEntityRepositoryCusm {

  /**
   * 条件查询动保
   * @param startTime
   * @param endTime
   * @param baseId
   * @param pondId
   * @param matterId
   * @return
   */
  List<SafeguardEntity> findByConditions(Date startTime, Date endTime, String baseId, String pondId, String matterId);
}
