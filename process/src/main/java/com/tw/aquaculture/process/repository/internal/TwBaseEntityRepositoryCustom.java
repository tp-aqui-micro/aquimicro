package com.tw.aquaculture.process.repository.internal;

import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * 基地信息自定义查询
 *
 * @author 陈荣
 * @date 2019/11/21 9:00
 */
public interface TwBaseEntityRepositoryCustom {
  /**
   * 条件分页查询基地
   *
   * @param pageable
   * @param params
   * @return
   */
  public Page<TwBaseEntity> findByConditions(Pageable pageable, Map<String, Object> params);
}
