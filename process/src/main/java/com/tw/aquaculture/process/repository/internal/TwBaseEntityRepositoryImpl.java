package com.tw.aquaculture.process.repository.internal;

import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author 陈荣
 * @date 2019/11/21 9:05
 */
@Repository("TwBaseEntityRepositoryImpl")
public class TwBaseEntityRepositoryImpl implements TwBaseEntityRepositoryCustom {

  @Autowired
  private EntityManager entityManager;

  @SuppressWarnings("unchecked")
  @Override
  public Page<TwBaseEntity> findByConditions(Pageable pageable, Map<String, Object> params) {
    StringBuilder hql = new StringBuilder("select distinct twBaseEntity from TwBaseEntity twBaseEntity ");
    hql.append(" left join fetch twBaseEntity.baseOrg twBaseEntity_baseOrg ");
    hql.append(" left join fetch twBaseEntity.leader twBaseEntity_leader ");
    hql.append(" left join fetch twBaseEntity.ponds twBaseEntity_ponds ");
    hql.append(" where 1=1 ");
    StringBuilder countHql = new StringBuilder("select count(distinct twBaseEntity) from TwBaseEntity twBaseEntity ");
    countHql.append(" left join twBaseEntity.baseOrg twBaseEntity_baseOrg ");
    countHql.append(" left join twBaseEntity.leader twBaseEntity_leader ");
    countHql.append(" left join twBaseEntity.ponds twBaseEntity_ponds ");
    countHql.append(" where 1=1 ");

    StringBuilder condition = new StringBuilder();
    condition.append("");
    countHql.append(condition.toString());
    Query query = entityManager.createQuery(hql.toString());
    Query countQuery = entityManager.createQuery(countHql.toString());
    if (params != null) {
      params.forEach((k, v) -> {
        query.setParameter(k, v);
        countQuery.setParameter(k, v);
      });
    }
    query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
    query.setMaxResults(pageable.getPageSize());
    List<TwBaseEntity> result = new ArrayList<>();
    long count = (long) countQuery.getResultList().get(0);
    if (count > 0) {
      result = query.getResultList();
    }
    return new PageImpl<>(result, pageable, count);
  }
}
