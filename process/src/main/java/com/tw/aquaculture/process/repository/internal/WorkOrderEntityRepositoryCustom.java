package com.tw.aquaculture.process.repository.internal;

import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;


/**
 * @author 陈荣
 * @date 2019/11/22 10:34
 */
public interface WorkOrderEntityRepositoryCustom {

  /**
   * 条件分页查询
   *
   * @param pageable
   * @param params
   * @return
   */
  Page<WorkOrderEntity> findByConditions(Pageable pageable, Map<String, Object> params);
}
