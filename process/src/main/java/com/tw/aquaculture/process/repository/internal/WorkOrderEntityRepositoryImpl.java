package com.tw.aquaculture.process.repository.internal;

import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author 陈荣
 * @date 2019/11/22 10:34
 */
@Repository("WorkOrderEntityRepositoryImpl")
public class WorkOrderEntityRepositoryImpl implements WorkOrderEntityRepositoryCustom {

  @Autowired
  EntityManager entityManager;

  @SuppressWarnings("unchecked")
  @Override
  public Page<WorkOrderEntity> findByConditions(Pageable pageable, Map<String, Object> params) {
    StringBuilder hql = new StringBuilder("select distinct w from WorkOrderEntity w ");
    hql.append(" left join w.pond p ");
    hql.append(" where w.enableState=1 ");
    StringBuilder countHql = new StringBuilder("select count(distinct w) from WorkOrderEntity w ");
    countHql.append(" left join w.pond p ");
    countHql.append(" where w.enableState=1 ");

    StringBuilder condition = new StringBuilder();
    if (params != null) {
      if (params.get("pondId") != null) {
        condition.append(" and p.id = :pondId ");
      }
      if (params.get("time") != null) {
        condition.append(" and w.createTime<:time ");
      }
    }
    countHql.append(condition.toString());
    Query query = entityManager.createQuery(hql.toString());
    Query countQuery = entityManager.createQuery(countHql.toString());
    if (params != null) {
      params.forEach((k, v) -> {
        query.setParameter(k, v);
        countQuery.setParameter(k, v);
      });
    }
    query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
    query.setMaxResults(pageable.getPageSize());
    List<WorkOrderEntity> result = new ArrayList<>();
    long count = (long) countQuery.getResultList().get(0);
    if (count > 0) {
      result = query.getResultList();
    }
    return new PageImpl<>(result, pageable, count);
  }
}
