package com.tw.aquaculture.process.service;

import com.bizunited.platform.kuiper.entity.InstanceEntity;

import java.security.Principal;

/**
 * @author 陈荣
 * @date 2019/12/16 16:52
 */
public interface FormInstanceHandleService {

  /**
   * 创建表单实例
   * @param entity
   * @param entityName
   * @param serviceAndMethodName
   * @return
   */
  Object create(Object entity, String entityName, String serviceAndMethodName, boolean flag);

  /**
   *
   * @param entity
   * @param entityName
   * @param serviceAndMethodName
   * @return
   */
  InstanceEntity createRemoteForm(Object entity, String entityName, String serviceAndMethodName);

  /**
   * 获取当前登录信息
   * @return
   */
  Principal getPrincipal();

  /**
   * 创建表单实例(获取formInstanceId)
   * @param entity
   * @param entityName
   * @param serviceAndMethodName
   * @return
   */
  String create(Object entity, String entityName, String serviceAndMethodName);
}
