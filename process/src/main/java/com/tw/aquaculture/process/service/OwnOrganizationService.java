package com.tw.aquaculture.process.service;

import com.bizunited.platform.core.entity.OrganizationEntity;
import org.springframework.stereotype.Service;

/**
 * 组织
 *
 * @author 陈荣
 * @date 2019/11/27 11:40
 */
@Service
public interface OwnOrganizationService {

  /**
   * 根据id查详情
   *
   * @param baseOrgId
   * @return
   */
  OrganizationEntity findDetailsById(String baseOrgId);

  /**
   * 根据id查
   *
   * @param id
   * @return
   */
  OrganizationEntity findById(String id);

  /**
   * 根据id查询详情
   *
   * @param id
   * @return
   */
  OrganizationEntity findOwnDetailsById(String id);

  /**
   * 创建组织
   *
   * @param organizationEntity
   * @return
   */
  OrganizationEntity create(OrganizationEntity organizationEntity);
}
