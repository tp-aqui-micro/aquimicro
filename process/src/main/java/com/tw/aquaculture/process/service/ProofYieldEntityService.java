package com.tw.aquaculture.process.service;

import com.bizunited.platform.core.annotations.ServiceMethodParam;
import com.tw.aquaculture.common.entity.process.ProofYieldEntity;
import com.tw.aquaculture.common.vo.reportslist.ExistAmountReportVo;
import com.tw.aquaculture.common.vo.reportslist.ReportParamVo;

import java.util.Date;
import java.util.List;

/**
 * ProofYieldEntity业务模型的服务层接口定义
 *
 * @author saturn
 */
//@NebulaService
public interface ProofYieldEntityService {
  /**
   * 创建一个新的ProofYieldEntity模型对象（包括了可能的第三方系统调用、复杂逻辑处理等）
   */
  // @NebulaServiceMethod(name="ProofYieldEntityService.create" , desc="创建一个新的ProofYieldEntity模型对象（包括了可能的第三方系统调用、复杂逻辑处理等）" , returnPropertiesFilter="" , scope=ScopeType.WRITE , recordQuery="ProofYieldEntityService.findDetailsByFormInstanceId" , recoveryService="ProofYieldEntityService.updateForm")
  public ProofYieldEntity create(ProofYieldEntity proofYieldEntity);

  /**
   * 创建一个新的StudentEntity模型对象
   * 该代码由satrun骨架生成，默认不包括任何可能第三方系统调用、任何复杂逻辑处理等，主要应用场景为前端表单数据的暂存功能</br>
   * 该方法与本接口中的updateFrom方法呼应
   */
  //@NebulaServiceMethod(name="ProofYieldEntityService.createForm" , desc="创建一个新的StudentEntity模型对象（默认不包括任何可能第三方系统调用、任何复杂逻辑处理等）" , returnPropertiesFilter="" , scope=ScopeType.WRITE , recordQuery="ProofYieldEntityService.findDetailsByFormInstanceId" , recoveryService="ProofYieldEntityService.updateForm")
  public ProofYieldEntity createForm(ProofYieldEntity proofYieldEntity);

  /**
   * 更新一个已有的ProofYieldEntity模型对象，其主键属性必须有值(1.1.4-release版本调整)。
   * 这个方法实际上一共分为三个步骤（默认）：</br>
   * 1、调用updateValidation方法完成表单数据更新前的验证</br>
   * 2、调用updateForm方法完成表单数据的更新</br>
   * 3、完成开发人员自行在本update方法中书写的，进行第三方系统调用（或特殊处理过程）的执行。</br>
   * 这样做的目的，实际上是为了保证updateForm方法中纯粹是处理表单数据的，在数据恢复表单引擎默认调用updateForm方法时，不会影响任何第三方业务数据
   * （当然，如果系统有特别要求，可由开发人员自行完成代码逻辑调整）
   */
  //@NebulaServiceMethod(name="ProofYieldEntityService.update" , desc="更新一个已有的ProofYieldEntity模型对象，其主键属性必须有值(1.1.4-release版本调整)。" , returnPropertiesFilter="", scope=ScopeType.WRITE , recordQuery="ProofYieldEntityService.findDetailsByFormInstanceId" , recoveryService="ProofYieldEntityService.updateForm")
  public ProofYieldEntity update(ProofYieldEntity proofYieldEntity);

  /**
   * 该方法只用于处理业务表单信息，包括了主业务模型、其下的关联模型、分组信息和明细细信息等
   * 该方法非常重要，因为如果进行静态表单的数据恢复，那么表单引擎将默认调用主业务模型（服务层）的这个方法。</br>
   * 这样一来保证了数据恢复时，不会涉及任何第三方系统的调用（当然，如果开发人员需要涉及的，可以自行进行修改）
   */
  //@NebulaServiceMethod(name="ProofYieldEntityService.updateForm" , desc="该方法只用于处理业务表单信息，包括了主业务模型、其下的关联模型、分组信息和明细细信息等" , returnPropertiesFilter="", scope=ScopeType.WRITE, recordQuery="ProofYieldEntityService.findDetailsByFormInstanceId" , recoveryService="ProofYieldEntityService.updateForm")
  public ProofYieldEntity updateForm(ProofYieldEntity proofYieldEntity);

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  //@NebulaServiceMethod(name="ProofYieldEntityService.findDetailsById" , desc="按照主键进行详情查询（包括关联信息）" , returnPropertiesFilter="groupLeader,pond,fishType,workOrder,proofYieldItems,pond.base" , scope=ScopeType.READ)
  public ProofYieldEntity findDetailsById(@ServiceMethodParam(name = "id") String id);

  /**
   * 按照ProofYieldEntity的主键编号，查询指定的数据信息（不包括任何关联信息）
   *
   * @param id 主键
   */
  public ProofYieldEntity findById(String id);

  /**
   * 按照主键进行信息的真删除
   *
   * @param id 主键
   */
  public void deleteById(String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  //@NebulaServiceMethod(name="ProofYieldEntityService.findDetailsByFormInstanceId" , desc="按照表单实例编号进行详情查询（包括关联信息）" , returnPropertiesFilter="groupLeader,pond,fishType,workOrder,proofYieldItems,pond.base" , scope=ScopeType.READ)
  public ProofYieldEntity findDetailsByFormInstanceId(@ServiceMethodParam(name = "formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  //@NebulaServiceMethod(name="ProofYieldEntityService.findByFormInstanceId" , desc="按照表单实例编号进行查询" , returnPropertiesFilter="", scope=ScopeType.READ)
  public ProofYieldEntity findByFormInstanceId(@ServiceMethodParam(name = "formInstanceId") String formInstanceId);

  /**
   * 本周期内最后一次打样测产数据
   *
   * @param pondId 塘口id
   * @return
   */
  ProofYieldEntity findLastProofYield(String pondId);

  /**
   * 查询上次存塘量
   *
   * @param pondId
   * @return
   */
  double computeLastExitCount(String pondId);

  /**
   * 本周期内某鱼种上次打样测产数据
   *
   * @param pondId
   * @param fishTypeId
   * @return
   */
  ProofYieldEntity findLastProofYieldByFishType(String pondId, String fishTypeId);

  /**
   * 根据塘口和工单号查询
   *
   * @param pondId
   * @param workOrderId
   * @return
   */
  List<ProofYieldEntity> findDetailsByPondAndWorkOrder(String pondId, String workOrderId);

  /**
   * 根据塘口和鱼种查询本周期内存塘量
   * @param pondId
   * @param fishTypeId
   * @return
   */
  double findExitCountByPondAndFishType(String pondId, String fishTypeId);

  /**
   * 根据时间和塘口id查询存塘量
   * @param pondId
   * @param startTime
   * @param endTime
   * @return
   */
  double computeLastExitCount(String pondId, Date startTime, Date endTime, String fishTypeId);

  /**
   * 存塘量报表
   * @param reportParamVo
   * @return
   */
  List<ExistAmountReportVo> existAmountReport(ReportParamVo reportParamVo);

  /**
   * 根据塘口和时间区间查询
   * @param pondId
   * @param startTime
   * @param endTime
   * @return
   */
  List<ProofYieldEntity> findByPondAndTimes(String pondId, Date startTime, Date endTime);

  /**
   * 根据塘口和截至时间查询
   * @param pid
   * @param endTime
   * @return
   */
  List<ProofYieldEntity> findByPondAndEndTime(String pid, Date endTime);

  /**
   * 根据塘口查询
   * @param pid
   * @return
   */
  List<ProofYieldEntity> findByPond(String pid);

  /**
   * 根据时间查询相应养殖周期内的存塘量
   * @param pid
   * @param endTime
   * @param fishTypeId
   * @return
   */
  Double computeReportExitCount(String pid, Date endTime, String fishTypeId);
}