package com.tw.aquaculture.process.service;

import com.bizunited.platform.core.annotations.ServiceMethodParam;
import com.tw.aquaculture.common.entity.base.TwGroupEntity;

import java.util.List;

/**
 * TwGroupEntity业务模型的服务层接口定义
 *
 * @author saturn
 */
//@NebulaService
public interface TwGroupEntityService {
  /**
   * 创建一个新的TwGroupEntity模型对象（包括了可能的第三方系统调用、复杂逻辑处理等）
   */
  //@NebulaServiceMethod(name="TwGroupEntityService.create" , desc="创建一个新的TwGroupEntity模型对象（包括了可能的第三方系统调用、复杂逻辑处理等）" , returnPropertiesFilter="" , scope= ScopeType.WRITE , recordQuery="TwGroupEntityService.findDetailsByFormInstanceId" , recoveryService="TwGroupEntityService.updateForm")
  public TwGroupEntity create(TwGroupEntity twGroupEntity);

  /**
   * 创建一个新的StudentEntity模型对象
   * 该代码由satrun骨架生成，默认不包括任何可能第三方系统调用、任何复杂逻辑处理等，主要应用场景为前端表单数据的暂存功能</br>
   * 该方法与本接口中的updateFrom方法呼应
   */
  // @NebulaServiceMethod(name="TwGroupEntityService.createForm" , desc="创建一个新的StudentEntity模型对象（默认不包括任何可能第三方系统调用、任何复杂逻辑处理等）" , returnPropertiesFilter="" , scope= ScopeType.WRITE , recordQuery="TwGroupEntityService.findDetailsByFormInstanceId" , recoveryService="TwGroupEntityService.updateForm")
  public TwGroupEntity createForm(TwGroupEntity twGroupEntity);

  /**
   * 更新一个已有的TwGroupEntity模型对象，其主键属性必须有值(1.1.4-release版本调整)。
   * 这个方法实际上一共分为三个步骤（默认）：</br>
   * 1、调用updateValidation方法完成表单数据更新前的验证</br>
   * 2、调用updateForm方法完成表单数据的更新</br>
   * 3、完成开发人员自行在本update方法中书写的，进行第三方系统调用（或特殊处理过程）的执行。</br>
   * 这样做的目的，实际上是为了保证updateForm方法中纯粹是处理表单数据的，在数据恢复表单引擎默认调用updateForm方法时，不会影响任何第三方业务数据
   * （当然，如果系统有特别要求，可由开发人员自行完成代码逻辑调整）
   */
  // @NebulaServiceMethod(name="TwGroupEntityService.update" , desc="更新一个已有的TwGroupEntity模型对象，其主键属性必须有值(1.1.4-release版本调整)。" , returnPropertiesFilter="", scope= ScopeType.WRITE , recordQuery="TwGroupEntityService.findDetailsByFormInstanceId" , recoveryService="TwGroupEntityService.updateForm")
  public TwGroupEntity update(TwGroupEntity twGroupEntity);

  /**
   * 该方法只用于处理业务表单信息，包括了主业务模型、其下的关联模型、分组信息和明细细信息等
   * 该方法非常重要，因为如果进行静态表单的数据恢复，那么表单引擎将默认调用主业务模型（服务层）的这个方法。</br>
   * 这样一来保证了数据恢复时，不会涉及任何第三方系统的调用（当然，如果开发人员需要涉及的，可以自行进行修改）
   */
  //@NebulaServiceMethod(name="TwGroupEntityService.updateForm" , desc="该方法只用于处理业务表单信息，包括了主业务模型、其下的关联模型、分组信息和明细细信息等" , returnPropertiesFilter="", scope= ScopeType.WRITE, recordQuery="TwGroupEntityService.findDetailsByFormInstanceId" , recoveryService="TwGroupEntityService.updateForm")
  public TwGroupEntity updateForm(TwGroupEntity twGroupEntity);

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  // @NebulaServiceMethod(name="TwGroupEntityService.findDetailsById" , desc="按照主键进行详情查询（包括关联信息）" , returnPropertiesFilter="groupLeader,base" , scope= ScopeType.READ)
  public TwGroupEntity findDetailsById(@ServiceMethodParam(name = "id") String id);

  /**
   * 按照TwGroupEntity的主键编号，查询指定的数据信息（不包括任何关联信息）
   *
   * @param id 主键
   */
  public TwGroupEntity findById(String id);

  /**
   * 按照主键进行信息的真删除
   *
   * @param id 主键
   */
  public void deleteById(String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  //@NebulaServiceMethod(name="TwGroupEntityService.findDetailsByFormInstanceId" , desc="按照表单实例编号进行详情查询（包括关联信息）" , returnPropertiesFilter="groupLeader,base" , scope= ScopeType.READ)
  public TwGroupEntity findDetailsByFormInstanceId(@ServiceMethodParam(name = "formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  //@NebulaServiceMethod(name="TwGroupEntityService.findByFormInstanceId" , desc="按照表单实例编号进行查询" , returnPropertiesFilter="", scope= ScopeType.READ)
  public TwGroupEntity findByFormInstanceId(@ServiceMethodParam(name = "formInstanceId") String formInstanceId);


  /**
   * 查询所有（包括组织信息及基地信息）
   * @return
   */
  List<TwGroupEntity> findAllDetail();
}