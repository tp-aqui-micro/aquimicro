package com.tw.aquaculture.process.service.base;

import com.bizunited.platform.core.annotations.NebulaService;
import com.tw.aquaculture.common.entity.userextend.UserExtendEntity;

/**
 * 组织结构扩展信息
 * @author fangfu.luo
 * @data 2019/12/27
 */
@NebulaService
public interface UserExtendEntityService {
  /**
   * 按照UserEntity的主键编号，查询指定的数据信息
   * @param id UserEntity的主键编号
   * */
  UserExtendEntity findByUserId(String id);
}