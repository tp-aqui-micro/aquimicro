package com.tw.aquaculture.process.service.base.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.store.StockOutEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.process.feign.store.StockOutFeign;
import com.tw.aquaculture.process.service.base.StockOutEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * StockOutEntity业务模型的服务层接口实现
 * @author saturn
 */
@Service("StockOutEntityServiceImpl")
public class StockOutEntityServiceImpl implements StockOutEntityService {
  @Autowired
  private StockOutFeign stockOutFeign;
  @Override
  public StockOutEntity create(StockOutEntity stockOutEntity) {
    return this.createForm(stockOutEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  }
  @Override
  public StockOutEntity createForm(StockOutEntity stockOutEntity) {
    if(stockOutEntity == null){
      return null;
    }
    try{
      ResponseModel responseModel = stockOutFeign.create(stockOutEntity);
      return JsonUtil.parseResponseModel(responseModel, new TypeReference<StockOutEntity>() {
      });
    }catch (Exception e){
      throw new IllegalArgumentException(e.getMessage());
    }
  }

  @Override
  public StockOutEntity update(StockOutEntity stockOutEntity) {
    return this.updateForm(stockOutEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  }
  @Override
  public StockOutEntity updateForm(StockOutEntity stockOutEntity) {
    if(stockOutEntity == null){
      return null;
    }
    ResponseModel responseModel = stockOutFeign.update(stockOutEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StockOutEntity>() {
    });
  }

  @Override
  public StockOutEntity findDetailsById(String id) {
    if(StringUtils.isBlank(id)){
      return null;
    }
    ResponseModel responseModel = stockOutFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StockOutEntity>() {
    });
  }
  @Override
  public StockOutEntity findById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    ResponseModel responseModel = stockOutFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StockOutEntity>() {
    });
  }
  @Override
  public void deleteById(String id) {
    if(StringUtils.isBlank(id)){
      return;
    }
    stockOutFeign.deleteById(id);
  }
  @Override
  public StockOutEntity findDetailsByFormInstanceId(String formInstanceId) {
    if(StringUtils.isBlank(formInstanceId)){
      return null;
    }
    ResponseModel responseModel = stockOutFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StockOutEntity>() {
    });
  }
  @Override
  public StockOutEntity findByFormInstanceId(String formInstanceId) {
    if(StringUtils.isBlank(formInstanceId)) { 
      return null;
    }
    ResponseModel responseModel = stockOutFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<StockOutEntity>() {
    });
  }

} 
