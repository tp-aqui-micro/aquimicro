package com.tw.aquaculture.process.service.base.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.userextend.UserExtendEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.process.feign.check.UserExtendEntityFeign;
import com.tw.aquaculture.process.service.base.UserExtendEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * UserExtendEntityService组织机构扩展属性业务实现层
 * @author fangfu.luo
 * @data 2019/12/16
 */
@Service("userExtendEntityServiceImpl")
public class UserExtendEntityServiceImpl implements UserExtendEntityService {

  @Autowired
  private UserExtendEntityFeign userExtendEntityFeign;

  @Override
  public UserExtendEntity findByUserId(String id) {
    ResponseModel responseModel = userExtendEntityFeign.findByUserId(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<UserExtendEntity>() {
    });
  }
}
