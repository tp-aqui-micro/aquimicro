package com.tw.aquaculture.process.service.internal;

import com.google.common.collect.Sets;
import com.tw.aquaculture.common.entity.process.CustomerInfo;
import com.tw.aquaculture.process.repository.CustomerInfoRepository;
import com.tw.aquaculture.process.service.CustomerInfoService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;

/**
 * CustomerInfo业务模型的服务层接口实现
 * @author saturn
 */
@Service("CustomerInfoServiceImpl")
public class CustomerInfoServiceImpl implements CustomerInfoService {
  @Autowired
  private CustomerInfoRepository customerInfoRepository;
  @Transactional
  @Override
  public CustomerInfo create(CustomerInfo customerInfo) {
    return this.createForm(customerInfo);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public CustomerInfo createForm(CustomerInfo customerInfo) {
   /* 
    * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
    * 1、如果当前模型对象不是主模型
    * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
    * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
    * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
    * 2、如果当前模型对象是主业务模型
    *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
    *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
    *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
    * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
    *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
    *   2.3.2、以及验证每个分组的OneToMany明细信息
    * */
    this.createValidation(customerInfo);
    
    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    
    this.customerInfoRepository.save(customerInfo);
    
    // 返回最终处理的结果，里面带有详细的关联信息
    return customerInfo;
  }
  /**
   * 在创建一个新的CustomerInfo模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(CustomerInfo customerInfo) {
    Validate.notNull(customerInfo , "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(customerInfo.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    customerInfo.setId(null);
    Validate.notBlank(customerInfo.getName(), "添加信息时，客户名称不能为空！");
    Validate.notBlank(customerInfo.getLinkMan(), "添加信息时，联系人不能为空！");
    Validate.notBlank(customerInfo.getLinkPhone(), "添加信息时，联系电话不能为空！");
    Validate.notNull(customerInfo.getPriceByTime(), "添加信息时，价格截至时间不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(customerInfo.getName() == null || customerInfo.getName().length() < 255 , "客户名称,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(customerInfo.getLinkMan() == null || customerInfo.getLinkMan().length() < 255 , "联系人,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(customerInfo.getLinkPhone() == null || customerInfo.getLinkPhone().length() < 255 , "联系电话,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(customerInfo.getNeedSpecs() == null || customerInfo.getNeedSpecs().length() < 255 , "需求规格,在进行添加时填入值超过了限定长度(255)，请检查!");
  }
  @Transactional
  @Override
  public CustomerInfo update(CustomerInfo customerInfo) {
    return this.updateForm(customerInfo);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public CustomerInfo updateForm(CustomerInfo customerInfo) {
    /* 
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     * 
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    
    this.updateValidation(customerInfo);
    // ===================基本信息
    String currentId = customerInfo.getId();
    Optional<CustomerInfo> opCurrentCustomerInfo = this.customerInfoRepository.findById(currentId);
    CustomerInfo currentCustomerInfo = opCurrentCustomerInfo.orElse(null);
    Validate.notNull(currentCustomerInfo ,"未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentCustomerInfo.setName(customerInfo.getName());
    currentCustomerInfo.setLinkMan(customerInfo.getLinkMan());
    currentCustomerInfo.setLinkPhone(customerInfo.getLinkPhone());
    currentCustomerInfo.setNeedSpecs(customerInfo.getNeedSpecs());
    currentCustomerInfo.setNeedWeight(customerInfo.getNeedWeight());
    currentCustomerInfo.setPrice(customerInfo.getPrice());
    currentCustomerInfo.setTransportMoney(customerInfo.getTransportMoney());
    currentCustomerInfo.setPriceByTime(customerInfo.getPriceByTime());
    currentCustomerInfo.setFishType(customerInfo.getFishType());
    currentCustomerInfo.setOutFishApply(customerInfo.getOutFishApply());
    
    this.customerInfoRepository.saveAndFlush(currentCustomerInfo);
    return currentCustomerInfo;
  }
  /**
   * 在更新一个已有的CustomerInfo模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(CustomerInfo customerInfo) {
    Validate.isTrue(!StringUtils.isBlank(customerInfo.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
    
    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(customerInfo.getName(), "修改信息时，客户名称不能为空！");
    Validate.notBlank(customerInfo.getLinkMan(), "修改信息时，联系人不能为空！");
    Validate.notBlank(customerInfo.getLinkPhone(), "修改信息时，联系电话不能为空！");
    Validate.notNull(customerInfo.getPriceByTime(), "修改信息时，价格截至时间不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(customerInfo.getName() == null || customerInfo.getName().length() < 255 , "客户名称,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(customerInfo.getLinkMan() == null || customerInfo.getLinkMan().length() < 255 , "联系人,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(customerInfo.getLinkPhone() == null || customerInfo.getLinkPhone().length() < 255 , "联系电话,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(customerInfo.getNeedSpecs() == null || customerInfo.getNeedSpecs().length() < 255 , "需求规格,在进行修改时填入值超过了限定长度(255)，请检查!");
    
    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
  } 
  @Override
  public Set<CustomerInfo> findDetailsByFishType(String fishType) {
    if(StringUtils.isBlank(fishType)) { 
      return Sets.newHashSet();
    }
    return this.customerInfoRepository.findDetailsByFishType(fishType);
  }
  @Override
  public Set<CustomerInfo> findDetailsByOutFishApply(String outFishApply) {
    if(StringUtils.isBlank(outFishApply)) { 
      return Sets.newHashSet();
    }
    return this.customerInfoRepository.findDetailsByOutFishApply(outFishApply);
  }
  @Override
  public CustomerInfo findDetailsById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    return this.customerInfoRepository.findDetailsById(id);
  }
  @Override
  public CustomerInfo findById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    
    Optional<CustomerInfo> op = customerInfoRepository.findById(id);
    return op.orElse(null); 
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id , "进行删除时，必须给定主键信息!!");
    CustomerInfo current = this.findById(id);
    if(current != null) { 
      this.customerInfoRepository.delete(current);
    }
  }
} 
