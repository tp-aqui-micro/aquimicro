package com.tw.aquaculture.process.service.internal;

import com.bizunited.platform.kuiper.starter.service.KuiperToolkitService;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.common.entity.process.DividePondEntity;
import com.tw.aquaculture.common.entity.process.DividePondItemEntity;
import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.enums.DividePondTypeEnum;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.common.vo.reportslist.DividePondResultVo;
import com.tw.aquaculture.common.vo.reportslist.DividePondParamVo;
import com.tw.aquaculture.process.repository.DividePondEntityRepository;
import com.tw.aquaculture.process.service.*;
import com.tw.aquaculture.process.service.base.CodeGenerateService;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.*;

/**
 * DividePondEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("DividePondEntityServiceImpl")
public class DividePondEntityServiceImpl implements DividePondEntityService {
  @Autowired
  private PondEntityService pondEntityService;
  @Autowired
  private FishTypeEntityService fishTypeEntityService;
  @Autowired
  private DividePondItemEntityService dividePondItemEntityService;
  @Autowired
  private WorkOrderEntityService workOrderEntityService;
  @Autowired
  private DividePondEntityRepository dividePondEntityRepository;
  @Autowired
  private CodeGenerateService codeGenerateService;
  @Autowired
  private TwBaseEntityService twBaseEntityService;
  /**
   * Kuiper表单引擎用于减少编码工作量的工具包
   */
  @Autowired
  private KuiperToolkitService kuiperToolkitService;

  @Transactional
  @Override
  public DividePondEntity create(DividePondEntity dividePondEntity) {
    return this.createForm(dividePondEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  }

  @Transactional
  @Override
  public DividePondEntity createForm(DividePondEntity dividePondEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *   2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    dividePondEntity.setCode(codeGenerateService.generateBusinessCode(CodeTypeEnum.DIVIDE_POND_ENTITY));
    List<WorkOrderEntity> workOrderEntities = workOrderEntityService.findByStateAndPond(dividePondEntity.getOutPond().getId(), EnableStateEnum.DISABLE.getState());
    if (!CollectionUtils.isEmpty(workOrderEntities)) {
      List<WorkOrderEntity> inWorkOrderEntities = workOrderEntityService.findByStateAndPond(dividePondEntity.getOutPond().getId(), EnableStateEnum.DISABLE.getState());
      if (CollectionUtils.isEmpty(inWorkOrderEntities)) {
        throw new IllegalArgumentException("当前转入塘口没有开启养殖周期，请先对转入塘口注水或者投苗");
      }
      dividePondEntity.setWorkOrder(workOrderEntities.get(workOrderEntities.size() - 1));
    } else {
      throw new IllegalArgumentException("当前转出塘口没有开启养殖周期，请先对转出塘口注水或者投苗");
    }
    if (!DateUtils.between(dividePondEntity.getDividePondTime(), dividePondEntity.getWorkOrder().getCreateTime(), new Date())) {
      throw new IllegalArgumentException("填写时间不在养殖周期内，请重新填写");
    }
    this.createValidation(dividePondEntity);

    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================

    this.dividePondEntityRepository.saveAndFlush(dividePondEntity);
    // 处理明细信息：分塘明细表
    Set<DividePondItemEntity> dividePondItemEntityItems = dividePondEntity.getDividePondItems();
    if (dividePondItemEntityItems != null) {
      for (DividePondItemEntity dividePondItemEntityItem : dividePondItemEntityItems) {
        dividePondItemEntityItem.setDividePond(dividePondEntity);
        this.dividePondItemEntityService.create(dividePondItemEntityItem);
      }
    }

    // 返回最终处理的结果，里面带有详细的关联信息
    return dividePondEntity;
  }

  /**
   * 在创建一个新的DividePondEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(DividePondEntity dividePondEntity) {
    Validate.notNull(dividePondEntity, "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(dividePondEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    dividePondEntity.setId(null);
    Validate.notBlank(dividePondEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(dividePondEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(dividePondEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(dividePondEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notBlank(dividePondEntity.getCode(), "添加信息时，编码不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(dividePondEntity.getFormInstanceId() == null || dividePondEntity.getFormInstanceId().length() < 255, "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(dividePondEntity.getCreateName() == null || dividePondEntity.getCreateName().length() < 255, "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(dividePondEntity.getCreatePosition() == null || dividePondEntity.getCreatePosition().length() < 255, "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(dividePondEntity.getModifyName() == null || dividePondEntity.getModifyName().length() < 255, "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(dividePondEntity.getModifyPosition() == null || dividePondEntity.getModifyPosition().length() < 255, "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(dividePondEntity.getCode() == null || dividePondEntity.getCode().length() < 255, "编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(dividePondEntity.getSupervisor() == null || dividePondEntity.getSupervisor().length() < 255, "监磅人,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(dividePondEntity.getSpecs() == null || dividePondEntity.getSpecs().length() < 255, "平均规格,在进行添加时填入值超过了限定长度(255)，请检查!");
    DividePondEntity currentDividePondEntity = this.findByFormInstanceId(dividePondEntity.getFormInstanceId());
    Validate.isTrue(currentDividePondEntity == null, "表单实例编号已存在,请检查");
    // 验证ManyToOne关联：转出塘口绑定值的正确性
    PondEntity currentOutPond = dividePondEntity.getOutPond();
    Validate.notNull(currentOutPond, "转出塘口信息必须传入，请检查!!");
    String currentPkOutPond = currentOutPond.getId();
    Validate.notBlank(currentPkOutPond, "创建操作时，当前转出塘口信息必须关联！");
    Validate.notNull(this.pondEntityService.findById(currentPkOutPond), "转出塘口关联信息未找到，请检查!!");
    // 验证ManyToOne关联：转入塘口绑定值的正确性
    PondEntity currentInPond = dividePondEntity.getInPond();
    Validate.notNull(currentInPond, "转入塘口信息必须传入，请检查!!");
    String currentPkInPond = currentInPond.getId();
    Validate.notBlank(currentPkInPond, "创建操作时，当前转入塘口信息必须关联！");
    Validate.notNull(this.pondEntityService.findById(currentPkInPond), "转入塘口关联信息未找到，请检查!!");
    // 验证ManyToOne关联：鱼种绑定值的正确性
    FishTypeEntity currentFishType = dividePondEntity.getFishType();
    Validate.notNull(currentFishType, "鱼种信息必须传入，请检查!!");
    String currentPkFishType = currentFishType.getId();
    Validate.notBlank(currentPkFishType, "创建操作时，当前鱼种信息必须关联！");
    Validate.notNull(this.fishTypeEntityService.findById(currentPkFishType), "鱼种关联信息未找到，请检查!!");
  }

  @Transactional
  @Override
  public DividePondEntity update(DividePondEntity dividePondEntity) {
    return this.updateForm(dividePondEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  }

  @Transactional
  @Override
  public DividePondEntity updateForm(DividePondEntity dividePondEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     *
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */

    this.updateValidation(dividePondEntity);
    // ===================基本信息
    String currentId = dividePondEntity.getId();
    Optional<DividePondEntity> opCurrentDividePondEntity = this.dividePondEntityRepository.findById(currentId);
    DividePondEntity currentDividePondEntity = opCurrentDividePondEntity.orElse(null);
    Validate.notNull(currentDividePondEntity, "未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentDividePondEntity.setFormInstanceId(dividePondEntity.getFormInstanceId());
    currentDividePondEntity.setCreateTime(dividePondEntity.getCreateTime());
    currentDividePondEntity.setCreateName(dividePondEntity.getCreateName());
    currentDividePondEntity.setCreatePosition(dividePondEntity.getCreatePosition());
    currentDividePondEntity.setModifyTime(dividePondEntity.getModifyTime());
    currentDividePondEntity.setModifyName(dividePondEntity.getModifyName());
    currentDividePondEntity.setModifyPosition(dividePondEntity.getModifyPosition());
    currentDividePondEntity.setCode(dividePondEntity.getCode());
    currentDividePondEntity.setDividePondTime(dividePondEntity.getDividePondTime());
    currentDividePondEntity.setSupervisor(dividePondEntity.getSupervisor());
    currentDividePondEntity.setTotalWeight(dividePondEntity.getTotalWeight());
    currentDividePondEntity.setSpecs(dividePondEntity.getSpecs());
    currentDividePondEntity.setPrice(dividePondEntity.getPrice());
    currentDividePondEntity.setAmount(dividePondEntity.getAmount());
    currentDividePondEntity.setNote(dividePondEntity.getNote());
    currentDividePondEntity.setPicture(dividePondEntity.getPicture());
    currentDividePondEntity.setOutPond(dividePondEntity.getOutPond());
    currentDividePondEntity.setInPond(dividePondEntity.getInPond());
    currentDividePondEntity.setFishType(dividePondEntity.getFishType());
    currentDividePondEntity.setGroupLeader(dividePondEntity.getGroupLeader());
    currentDividePondEntity.setWorkOrder(dividePondEntity.getWorkOrder());
    if (!DateUtils.between(dividePondEntity.getDividePondTime(), dividePondEntity.getWorkOrder().getCreateTime(), new Date())) {
      throw new IllegalArgumentException("填写时间不在养殖周期内，请重新填写");
    }
    this.dividePondEntityRepository.saveAndFlush(currentDividePondEntity);
    // ==========准备处理OneToMany关系的详情dividePondItems
    // 清理出那些需要删除的dividePondItems信息
    Set<DividePondItemEntity> dividePondItemEntitys = null;
    if (dividePondEntity.getDividePondItems() != null) {
      dividePondItemEntitys = Sets.newLinkedHashSet(dividePondEntity.getDividePondItems());
    } else {
      dividePondItemEntitys = Sets.newLinkedHashSet();
    }
    Set<DividePondItemEntity> dbDividePondItemEntitys = this.dividePondItemEntityService.findDetailsByDividePond(dividePondEntity.getId());
    if (dbDividePondItemEntitys == null) {
      dbDividePondItemEntitys = Sets.newLinkedHashSet();
    }
    // 求得的差集既是需要删除的数据 
    Set<String> mustDeleteDividePondItemsPks = kuiperToolkitService.collectionDiffent(dbDividePondItemEntitys, dividePondItemEntitys, DividePondItemEntity::getId);
    if (mustDeleteDividePondItemsPks != null && !mustDeleteDividePondItemsPks.isEmpty()) {
      for (String mustDeleteDividePondItemsPk : mustDeleteDividePondItemsPks) {
        this.dividePondItemEntityService.deleteById(mustDeleteDividePondItemsPk);
      }
    }
    // 对传来的记录进行添加或者修改操作 
    for (DividePondItemEntity dividePondItemEntityItem : dividePondItemEntitys) {
      dividePondItemEntityItem.setDividePond(dividePondEntity);
      if (StringUtils.isBlank(dividePondItemEntityItem.getId())) {
        dividePondItemEntityService.create(dividePondItemEntityItem);
      } else {
        dividePondItemEntityService.update(dividePondItemEntityItem);
      }
    }

    return currentDividePondEntity;
  }

  /**
   * 在更新一个已有的DividePondEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(DividePondEntity dividePondEntity) {
    Validate.isTrue(!StringUtils.isBlank(dividePondEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");

    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(dividePondEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(dividePondEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(dividePondEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(dividePondEntity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    Validate.notBlank(dividePondEntity.getCode(), "修改信息时，编码不能为空！");

    // 重复性判断，基本属性，需要满足unique = true
    DividePondEntity currentForFormInstanceId = this.findByFormInstanceId(dividePondEntity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId(), dividePondEntity.getId()), "表单实例编号已存在,请检查");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(dividePondEntity.getFormInstanceId() == null || dividePondEntity.getFormInstanceId().length() < 255, "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(dividePondEntity.getCreateName() == null || dividePondEntity.getCreateName().length() < 255, "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(dividePondEntity.getCreatePosition() == null || dividePondEntity.getCreatePosition().length() < 255, "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(dividePondEntity.getModifyName() == null || dividePondEntity.getModifyName().length() < 255, "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(dividePondEntity.getModifyPosition() == null || dividePondEntity.getModifyPosition().length() < 255, "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(dividePondEntity.getCode() == null || dividePondEntity.getCode().length() < 255, "编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(dividePondEntity.getSupervisor() == null || dividePondEntity.getSupervisor().length() < 255, "监磅人,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(dividePondEntity.getSpecs() == null || dividePondEntity.getSpecs().length() < 255, "平均规格,在进行修改时填入值超过了限定长度(255)，请检查!");

    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
    PondEntity currentForOutPond = dividePondEntity.getOutPond();
    Validate.notNull(currentForOutPond, "修改信息时，转出塘口必须传入，请检查!!");
    PondEntity currentForInPond = dividePondEntity.getInPond();
    Validate.notNull(currentForInPond, "修改信息时，转入塘口必须传入，请检查!!");
    FishTypeEntity currentForFishType = dividePondEntity.getFishType();
    Validate.notNull(currentForFishType, "修改信息时，鱼种必须传入，请检查!!");
  }

  @Override
  public DividePondEntity findDetailsById(String id) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(id)) {
      return null;
    }
    DividePondEntity current = this.dividePondEntityRepository.findDetailsById(id);
    if (current == null) {
      return null;
    }
    String currentPkValue = current.getId();
    // 2、======
    // 分塘明细表 的明细信息查询
    Collection<DividePondItemEntity> oneToManyDividePondItemsDetails = this.dividePondItemEntityService.findDetailsByDividePond(currentPkValue);
    current.setDividePondItems(Sets.newLinkedHashSet(oneToManyDividePondItemsDetails));

    return current;
  }

  @Override
  public DividePondEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }

    Optional<DividePondEntity> op = dividePondEntityRepository.findById(id);
    return op.orElse(null);
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    DividePondEntity current = this.findById(id);
    if (current != null) {
      this.dividePondEntityRepository.delete(current);
    }
  }

  @Override
  public DividePondEntity findDetailsByFormInstanceId(String formInstanceId) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    DividePondEntity current = this.dividePondEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if (current == null) {
      return null;
    }
    String currentPkValue = current.getId();
    // 2、======
    // 分塘明细表 的明细信息查询
    Collection<DividePondItemEntity> oneToManyDividePondItemsDetails = this.dividePondItemEntityService.findDetailsByDividePond(currentPkValue);
    current.setDividePondItems(Sets.newLinkedHashSet(oneToManyDividePondItemsDetails));

    return current;
  }

  @Override
  public DividePondEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    return this.dividePondEntityRepository.findByFormInstanceId(formInstanceId);
  }

  @Override
  public List<DividePondEntity> findByInPondAndTimes(String pondId, Date startTime, Date endTime) {
    if (StringUtils.isBlank(pondId) || startTime == null || endTime == null) {
      return Collections.emptyList();
    }
    return dividePondEntityRepository.findByInPondAndTime(pondId, startTime, endTime);
  }

  @Override
  public List<DividePondEntity> findByOutPondAndTimes(String pondId, Date startTime, Date endTime) {
    if (StringUtils.isBlank(pondId) || startTime == null || endTime == null) {
      return Collections.emptyList();
    }
    return dividePondEntityRepository.findByOutPondAndTime(pondId, startTime, endTime);
  }

  @Override
  public List<DividePondEntity> findByInPondAndTimesAndFishType(String pondId, Date startTime, Date endTime, String fishTypeId) {
    if (StringUtils.isBlank(pondId) || startTime == null || endTime == null || fishTypeId == null) {
      return Collections.emptyList();
    }
    return dividePondEntityRepository.findByInPondAndTimesAndFishType(pondId, startTime, endTime, fishTypeId);
  }

  @Override
  public List<DividePondEntity> findByOutPondAndTimesAndFishType(String pondId, Date startTime, Date endTime, String fishTypeId) {
    if (StringUtils.isBlank(pondId) || startTime == null || endTime == null || fishTypeId == null) {
      return Collections.emptyList();
    }
    return dividePondEntityRepository.findByOutPondAndTimesAndFishType(pondId, startTime, endTime, fishTypeId);
  }

  @Override
  public List<DividePondEntity> findByInPondAndWorkOrderIdAndFishType(String pondId, String workOrderId, String fishTypeId) {
    if (StringUtils.isBlank(pondId) || StringUtils.isBlank(workOrderId) || StringUtils.isBlank(fishTypeId)) {
      return Collections.emptyList();
    }
    return dividePondEntityRepository.findByInPondAndWorkOrderIdAndFishType(pondId, workOrderId, fishTypeId);
  }

  @Override
  public List<DividePondEntity> findByOutPondAndWorkOrderIdAndFishType(String pondId, String workOrderId, String fishTypeId) {
    if (StringUtils.isBlank(pondId) || StringUtils.isBlank(workOrderId) || StringUtils.isBlank(fishTypeId)) {
      return Collections.emptyList();
    }
    return dividePondEntityRepository.findByOutPondAndWorkOrderIdAndFishType(pondId, workOrderId, fishTypeId);
  }

  @Override
  public List<DividePondResultVo> dividePondReport(DividePondParamVo dividePondParamVo) {
    if(dividePondParamVo!=null){
      return Lists.newArrayList();
    }
    List<DividePondResultVo> result = Lists.newArrayList();
    Date startTime = dividePondParamVo.getStartTime();
    Date endTime = dividePondParamVo.getEndTime();
    String baseId = dividePondParamVo.getBaseId();
    String pondId = dividePondParamVo.getPondId();
    String fishTypeId = dividePondParamVo.getFishType();
    Integer divideType = dividePondParamVo.getDivideType();
    if (startTime == null) {
      startTime = DateUtils.getMonthStart();
    }
    if (endTime == null) {
      endTime = new Date();
    }
    List<TwBaseEntity> baseEntities = Lists.newArrayList();
    List<PondEntity> pondEntities = Lists.newArrayList();
    List<FishTypeEntity> fishTypeEntities = Lists.newArrayList();
    if (StringUtils.isBlank(baseId) && StringUtils.isBlank(pondId)) {
      baseEntities = twBaseEntityService.findAll();
    } else if (StringUtils.isNotBlank(baseId) && StringUtils.isBlank(pondId)) {
      baseEntities = buildBases(Arrays.asList(baseId.split(",")));
    } else if (StringUtils.isNotBlank(pondId)) {
      pondEntities = buildPonds(Arrays.asList(pondId.split(",")));
    }
    if (StringUtils.isNotBlank(fishTypeId)) {
      fishTypeEntities = buildFishTypes(Arrays.asList(fishTypeId.split(",")));
    }
    if (StringUtils.isBlank(pondId)) {
      this.dividePondReportWithBases(result, baseEntities, fishTypeEntities, startTime, endTime, divideType);
    } else {
      this.dividePondReportWithPonds(result, pondEntities, fishTypeEntities, startTime, endTime, divideType);
    }
    return result;
  }

  /**
   * 根据基地查询分唐报表
   *
   * @param result
   * @param baseEntities
   * @param fishTypeEntities
   * @param startTime
   * @param endTime
   * @param divideType
   */
  private void dividePondReportWithBases(List<DividePondResultVo> result, List<TwBaseEntity> baseEntities, List<FishTypeEntity> fishTypeEntities, Date startTime, Date endTime, Integer divideType) {
    if (CollectionUtils.isEmpty(baseEntities)) {
      return;
    }
    baseEntities.forEach(b -> {
      List<PondEntity> pondEntities = pondEntityService.findPondsByBaseId(b.getId(), EnableStateEnum.ALL.getState());
      this.dividePondReportWithPonds(result, pondEntities, fishTypeEntities, startTime, endTime, divideType);
    });
  }

  /**
   * 根据塘口查询分搪报表
   *
   * @param result
   * @param pondEntities
   * @param fishTypeEntities
   * @param startTime
   * @param endTime
   * @param divideType
   */
  private void dividePondReportWithPonds(List<DividePondResultVo> result, List<PondEntity> pondEntities, List<FishTypeEntity> fishTypeEntities, Date startTime, Date endTime, Integer divideType) {
    if (CollectionUtils.isEmpty(pondEntities)) {
      return;
    }
    pondEntities.forEach(p -> {
      dividePondReportWithFishTypes(result, p, fishTypeEntities, startTime, endTime, divideType);
    });
  }

  /**
   * 根据鱼种查询分搪报表
   *
   * @param result
   * @param p
   * @param fishTypeEntities
   * @param startTime
   * @param endTime
   * @param divideType
   */
  private void dividePondReportWithFishTypes(List<DividePondResultVo> result, PondEntity p, List<FishTypeEntity> fishTypeEntities, Date startTime, Date endTime, Integer divideType) {
    if (p == null) {
      return;
    }
    if (DividePondTypeEnum.OUT_POND.getState().equals(divideType)) {
      dividePondReport(result, p, fishTypeEntities, startTime, endTime, DividePondTypeEnum.OUT_POND);
    } else if (DividePondTypeEnum.IN_POND.getState().equals(divideType)) {
      dividePondReport(result, p, fishTypeEntities, startTime, endTime, DividePondTypeEnum.IN_POND);
    } else {
      dividePondReport(result, p, fishTypeEntities, startTime, endTime, DividePondTypeEnum.OUT_POND);
      dividePondReport(result, p, fishTypeEntities, startTime, endTime, DividePondTypeEnum.IN_POND);
    }
  }

  /**
   * 转出/转入报表
   *
   * @param result
   * @param p
   * @param fishTypeEntities
   * @param startTime
   * @param endTime
   * @param outPond
   */
  private void dividePondReport(List<DividePondResultVo> result, PondEntity p, List<FishTypeEntity> fishTypeEntities, Date startTime, Date endTime, DividePondTypeEnum outPond) {
    if (p == null) {
      return;
    }
    if (CollectionUtils.isEmpty(fishTypeEntities)) {
      List<DividePondEntity> dividePondEntities;
      if (DividePondTypeEnum.OUT_POND.equals(outPond)) {
        dividePondEntities = dividePondEntityRepository.findByOutPondAndTime(p.getId(), startTime, endTime);
      } else if (DividePondTypeEnum.IN_POND.equals(outPond)) {
        dividePondEntities = dividePondEntityRepository.findByInPondAndTime(p.getId(), startTime, endTime);
      } else {
        dividePondEntities = Lists.newArrayList();
      }
      fishTypeEntities = Lists.newArrayList();
      buildFishTypesByDividePonds(dividePondEntities, fishTypeEntities);
    }
    if (CollectionUtils.isEmpty(fishTypeEntities)) {
      return;
    }
    for (FishTypeEntity fishType : fishTypeEntities) {
      List<DividePondEntity> divides;
      if(DividePondTypeEnum.OUT_POND.equals(outPond)){
        divides = this.dividePondEntityRepository.findByOutPondAndTimesAndFishType(p.getId(), startTime, endTime, fishType.getId());
      }else{
        divides = this.dividePondEntityRepository.findByInPondAndTimesAndFishType(p.getId(), startTime, endTime, fishType.getId());
      }
      DividePondResultVo dividePondResultVo = new DividePondResultVo();
      dividePondResultVo.setPondId(p.getId());
      dividePondResultVo.setPondName(p.getName());
      dividePondResultVo.setBaseId(p.getBase() == null ? null : p.getBase().getId());
      if (p.getBase() != null && p.getBase().getBaseOrg() != null) {
        dividePondResultVo.setBaseName(p.getBase().getBaseOrg().getOrgName());
      }
      if (p.getBase() != null && p.getBase().getBaseOrg() != null && p.getBase().getBaseOrg().getParent() != null) {
        dividePondResultVo.setCompanyId(p.getBase().getBaseOrg().getParent().getId());
        dividePondResultVo.setCompanyName(p.getBase().getBaseOrg().getParent().getOrgName());
      }
      dividePondResultVo.setDivideTypeCode(outPond.getState());
      dividePondResultVo.setDivideTypeName(outPond.getMemo());
      dividePondResultVo.setFishTypeId(fishType.getId());
      dividePondResultVo.setFishTypeName(fishType.getName());
      computeCountAndWeightReport(dividePondResultVo, divides);
      result.add(dividePondResultVo);
    }
  }

  /**
   * 计算数量和重量
   *
   * @param dividePondResultVo
   * @param divides
   */
  private void computeCountAndWeightReport(DividePondResultVo dividePondResultVo, List<DividePondEntity> divides) {
    if (CollectionUtils.isEmpty(divides)) {
      return;
    }
    double count = 0;
    double weight = 0;
    for (DividePondEntity divide : divides) {
      Set<DividePondItemEntity> items = divide.getDividePondItems();
      if (CollectionUtils.isEmpty(items)) {
        continue;
      }
      for (DividePondItemEntity item : items) {
        count += (item.getCount() == null ? 0 : item.getCount());
        weight += (item.getWeight() == null ? 0 : item.getWeight());
      }
    }
    dividePondResultVo.setCount(count);
    dividePondResultVo.setWeight(weight);
  }

  /**
   * 根据转分塘记录初始化鱼种列表
   *
   * @param dividePondEntities
   * @param fishTypeEntities
   * @return
   */
  private void buildFishTypesByDividePonds(List<DividePondEntity> dividePondEntities, List<FishTypeEntity> fishTypeEntities) {
    if (CollectionUtils.isEmpty(dividePondEntities)) {
      return;
    }
    Map<String, FishTypeEntity> fishTypeEntityMap = Maps.newHashMap();
    dividePondEntities.forEach(d -> {
      if (d.getFishType() != null) {
        fishTypeEntityMap.put(d.getFishType().getId(), d.getFishType());
      }
    });
    fishTypeEntityMap.forEach((k, v) -> {
      fishTypeEntities.add(v);
    });
  }

  /**
   * 根据鱼种id，获取鱼种对象
   *
   * @param fishTypeIds
   * @return
   */
  private List<FishTypeEntity> buildFishTypes(List<String> fishTypeIds) {
    if (CollectionUtils.isEmpty(fishTypeIds)) {
      return Lists.newArrayList();
    }
    List<FishTypeEntity> fishTypes = Lists.newArrayList();
    fishTypeIds.forEach(f -> {
      FishTypeEntity fishType = fishTypeEntityService.findById(f);
      if (fishType != null) {
        fishTypes.add(fishType);
      }
    });
    return fishTypes;
  }

  /**
   * 根据塘口id，获取塘口对象
   *
   * @param pondIds
   * @return
   */
  private List<PondEntity> buildPonds(List<String> pondIds) {
    if (CollectionUtils.isEmpty(pondIds)) {
      return Lists.newArrayList();
    }
    List<PondEntity> ponds = Lists.newArrayList();
    for (String pondId : pondIds) {
      PondEntity pond = pondEntityService.findById(pondId);
      if (pond != null) {
        ponds.add(pond);
      }
    }
    return ponds;
  }

  /**
   * 根据基地id，获取基地对象
   *
   * @param baseIds
   * @return
   */
  private List<TwBaseEntity> buildBases(List<String> baseIds) {
    if (CollectionUtils.isEmpty(baseIds)) {
      return Lists.newArrayList();
    }
    List<TwBaseEntity> bases = Lists.newArrayList();
    for (String baseId : baseIds) {
      TwBaseEntity base = twBaseEntityService.findById(baseId);
      if (base != null) {
        bases.add(base);
      }
    }
    return bases;
  }
} 
