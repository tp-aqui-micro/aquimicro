package com.tw.aquaculture.process.service.internal;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.common.entity.process.FishDamageEntity;
import com.tw.aquaculture.common.entity.process.ProofYieldEntity;
import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.common.utils.MathUtil;
import com.tw.aquaculture.common.vo.reportslist.FishDamageReportVo;
import com.tw.aquaculture.common.vo.reportslist.ReportParamVo;
import com.tw.aquaculture.process.repository.FishDamageEntityRepository;
import com.tw.aquaculture.process.service.FishDamageEntityService;
import com.tw.aquaculture.process.service.FishTypeEntityService;
import com.tw.aquaculture.process.service.PondEntityService;
import com.tw.aquaculture.process.service.ProofYieldEntityService;
import com.tw.aquaculture.process.service.WorkOrderEntityService;
import com.tw.aquaculture.process.service.base.CodeGenerateService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

/**
 * FishDamageEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("FishDamageEntityServiceImpl")
public class FishDamageEntityServiceImpl implements FishDamageEntityService {
  @Autowired
  private PondEntityService pondEntityService;
  @Autowired
  private FishTypeEntityService fishTypeEntityService;
  @Autowired
  private WorkOrderEntityService workOrderEntityService;
  @Autowired
  private FishDamageEntityRepository fishDamageEntityRepository;
  @Autowired
  private CodeGenerateService codeGenerateService;
  @Autowired
  private ProofYieldEntityService proofYieldEntityService;

  @Transactional
  @Override
  public FishDamageEntity create(FishDamageEntity fishDamageEntity) {
    FishDamageEntity current = this.createForm(fishDamageEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  }

  @Transactional
  @Override
  public FishDamageEntity createForm(FishDamageEntity fishDamageEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *   2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    fishDamageEntity.setCode(codeGenerateService.generateBusinessCode(CodeTypeEnum.FISH_DAMAGE_ENTITY));
    List<WorkOrderEntity> workOrderEntities = workOrderEntityService.findByStateAndPond(fishDamageEntity.getPond().getId(), EnableStateEnum.DISABLE.getState());
    if (!CollectionUtils.isEmpty(workOrderEntities) && StringUtils.isNotBlank(workOrderEntities.get(0).getId())) {
      fishDamageEntity.setWorkOrder(workOrderEntities.get(workOrderEntities.size() - 1));
    } else {
      throw new RuntimeException("请先对该塘口注水");
    }
    if (!DateUtils.between(fishDamageEntity.getDamageTime(), fishDamageEntity.getWorkOrder().getCreateTime(), new Date())) {
      throw new RuntimeException("填写时间不在养殖周期内，请重新填写");
    }
    this.createValidation(fishDamageEntity);

    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================

    this.fishDamageEntityRepository.saveAndFlush(fishDamageEntity);

    // 返回最终处理的结果，里面带有详细的关联信息
    return fishDamageEntity;
  }

  /**
   * 在创建一个新的FishDamageEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(FishDamageEntity fishDamageEntity) {
    Validate.notNull(fishDamageEntity, "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(fishDamageEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    fishDamageEntity.setId(null);
    Validate.notBlank(fishDamageEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(fishDamageEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(fishDamageEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(fishDamageEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notBlank(fishDamageEntity.getCode(), "添加信息时，编码不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(fishDamageEntity.getFormInstanceId() == null || fishDamageEntity.getFormInstanceId().length() < 255, "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishDamageEntity.getCreateName() == null || fishDamageEntity.getCreateName().length() < 255, "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishDamageEntity.getCreatePosition() == null || fishDamageEntity.getCreatePosition().length() < 255, "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishDamageEntity.getModifyName() == null || fishDamageEntity.getModifyName().length() < 255, "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishDamageEntity.getModifyPosition() == null || fishDamageEntity.getModifyPosition().length() < 255, "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishDamageEntity.getCode() == null || fishDamageEntity.getCode().length() < 255, "编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishDamageEntity.getDamageReason() == null || fishDamageEntity.getDamageReason().length() < 255, "损鱼原因,在进行添加时填入值超过了限定长度(255)，请检查!");
    FishDamageEntity currentFishDamageEntity = this.findByFormInstanceId(fishDamageEntity.getFormInstanceId());
    Validate.isTrue(currentFishDamageEntity == null, "表单实例编号已存在,请检查");
    // 验证ManyToOne关联：塘口绑定值的正确性
    PondEntity currentPond = fishDamageEntity.getPond();
    Validate.notNull(currentPond, "塘口信息必须传入，请检查!!");
    String currentPkPond = currentPond.getId();
    Validate.notBlank(currentPkPond, "创建操作时，当前塘口信息必须关联！");
    Validate.notNull(this.pondEntityService.findById(currentPkPond), "塘口关联信息未找到，请检查!!");
    // 验证ManyToOne关联：鱼种绑定值的正确性
    FishTypeEntity currentFishType = fishDamageEntity.getFishType();
    Validate.notNull(currentFishType, "鱼种信息必须传入，请检查!!");
    String currentPkFishType = currentFishType.getId();
    Validate.notBlank(currentPkFishType, "创建操作时，当前鱼种信息必须关联！");
    Validate.notNull(this.fishTypeEntityService.findById(currentPkFishType), "鱼种关联信息未找到，请检查!!");
  }

  @Transactional
  @Override
  public FishDamageEntity update(FishDamageEntity fishDamageEntity) {
    FishDamageEntity current = this.updateForm(fishDamageEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  }

  @Transactional
  @Override
  public FishDamageEntity updateForm(FishDamageEntity fishDamageEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     *
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */

    this.updateValidation(fishDamageEntity);
    // ===================基本信息
    String currentId = fishDamageEntity.getId();
    Optional<FishDamageEntity> op_currentFishDamageEntity = this.fishDamageEntityRepository.findById(currentId);
    FishDamageEntity currentFishDamageEntity = op_currentFishDamageEntity.orElse(null);
    currentFishDamageEntity = Validate.notNull(currentFishDamageEntity, "未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentFishDamageEntity.setFormInstanceId(fishDamageEntity.getFormInstanceId());
    currentFishDamageEntity.setCreateTime(fishDamageEntity.getCreateTime());
    currentFishDamageEntity.setCreateName(fishDamageEntity.getCreateName());
    currentFishDamageEntity.setCreatePosition(fishDamageEntity.getCreatePosition());
    currentFishDamageEntity.setModifyTime(fishDamageEntity.getModifyTime());
    currentFishDamageEntity.setModifyName(fishDamageEntity.getModifyName());
    currentFishDamageEntity.setModifyPosition(fishDamageEntity.getModifyPosition());
    currentFishDamageEntity.setCode(fishDamageEntity.getCode());
    currentFishDamageEntity.setDamageTime(fishDamageEntity.getDamageTime());
    currentFishDamageEntity.setDamageReason(fishDamageEntity.getDamageReason());
    currentFishDamageEntity.setCount(fishDamageEntity.getCount());
    currentFishDamageEntity.setWeight(fishDamageEntity.getWeight());
    currentFishDamageEntity.setNote(fishDamageEntity.getNote());
    currentFishDamageEntity.setPicture(fishDamageEntity.getPicture());
    currentFishDamageEntity.setPond(fishDamageEntity.getPond());
    currentFishDamageEntity.setFishType(fishDamageEntity.getFishType());
    currentFishDamageEntity.setWorkOrder(fishDamageEntity.getWorkOrder());
    currentFishDamageEntity.setSpecs(fishDamageEntity.getSpecs());
    if (!DateUtils.between(currentFishDamageEntity.getDamageTime(), currentFishDamageEntity.getWorkOrder().getCreateTime(), new Date())) {
      throw new RuntimeException("填写时间不在养殖周期内，请重新填写");
    }
    this.fishDamageEntityRepository.saveAndFlush(currentFishDamageEntity);
    return currentFishDamageEntity;
  }

  /**
   * 在更新一个已有的FishDamageEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(FishDamageEntity fishDamageEntity) {
    Validate.isTrue(!StringUtils.isBlank(fishDamageEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");

    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(fishDamageEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(fishDamageEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(fishDamageEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(fishDamageEntity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    Validate.notBlank(fishDamageEntity.getCode(), "修改信息时，编码不能为空！");

    // 重复性判断，基本属性，需要满足unique = true
    FishDamageEntity currentForFormInstanceId = this.findByFormInstanceId(fishDamageEntity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId(), fishDamageEntity.getId()), "表单实例编号已存在,请检查");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(fishDamageEntity.getFormInstanceId() == null || fishDamageEntity.getFormInstanceId().length() < 255, "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishDamageEntity.getCreateName() == null || fishDamageEntity.getCreateName().length() < 255, "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishDamageEntity.getCreatePosition() == null || fishDamageEntity.getCreatePosition().length() < 255, "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishDamageEntity.getModifyName() == null || fishDamageEntity.getModifyName().length() < 255, "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishDamageEntity.getModifyPosition() == null || fishDamageEntity.getModifyPosition().length() < 255, "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishDamageEntity.getCode() == null || fishDamageEntity.getCode().length() < 255, "编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishDamageEntity.getDamageReason() == null || fishDamageEntity.getDamageReason().length() < 255, "损鱼原因,在进行修改时填入值超过了限定长度(255)，请检查!");

    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
    PondEntity currentForPond = fishDamageEntity.getPond();
    Validate.notNull(currentForPond, "修改信息时，塘口必须传入，请检查!!");
    FishTypeEntity currentForFishType = fishDamageEntity.getFishType();
    Validate.notNull(currentForFishType, "修改信息时，鱼种必须传入，请检查!!");
  }

  @Override
  public FishDamageEntity findDetailsById(String id) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(id)) {
      return null;
    }
    FishDamageEntity current = this.fishDamageEntityRepository.findDetailsById(id);
    if (current == null) {
      return null;
    }
    return current;
  }

  @Override
  public FishDamageEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }

    Optional<FishDamageEntity> op = fishDamageEntityRepository.findById(id);
    return op.orElse(null);
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    FishDamageEntity current = this.findById(id);
    if (current != null) {
      this.fishDamageEntityRepository.delete(current);
    }
  }

  @Override
  public FishDamageEntity findDetailsByFormInstanceId(String formInstanceId) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    FishDamageEntity current = this.fishDamageEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if (current == null) {
      return null;
    }
    return current;
  }

  @Override
  public FishDamageEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    return this.fishDamageEntityRepository.findByFormInstanceId(formInstanceId);
  }

  @Override
  public List<FishDamageEntity> findByPondAndTimes(String pondId, Date startTime, Date endTime) {
    if (StringUtils.isBlank(pondId) || startTime == null || endTime == null) {
      return null;
    }
    return fishDamageEntityRepository.findByPondAndTimes(pondId, startTime, endTime);
  }

  @Override
  public List<FishDamageEntity> findByPondAndTimesAndFishType(String pondId, Date startTime, Date endTime, String fishTypeId) {
    if (StringUtils.isBlank(pondId) || startTime == null || endTime == null || fishTypeId == null) {
      return null;
    }
    return fishDamageEntityRepository.findByPondAndTimesAndFishType(pondId, startTime, endTime, fishTypeId);
  }

  @Override
  public List<FishDamageEntity> findByPondAndWorkOrderIdAndFishType(String pondId, String workOrderId, String fishTypeId) {
    if (StringUtils.isBlank(pondId) || StringUtils.isBlank(workOrderId) || StringUtils.isBlank(fishTypeId)) {
      return null;
    }
    return fishDamageEntityRepository.findByPondAndWorkOrderIdAndFishType(pondId, workOrderId, fishTypeId);
  }

  @Override
  public List<FishDamageReportVo> fishDamageReport(ReportParamVo reportParamVo) {
    Date startTime = reportParamVo.getStartTime();
    Date endTime = reportParamVo.getEndTime();
    String baseId = reportParamVo.getBaseId();
    String pondId = reportParamVo.getPondId();
    String fishTypeId = reportParamVo.getFishTypeId();
    List<FishDamageReportVo> result = Lists.newArrayList();
    List<FishDamageEntity> fishDamageEntities = this.fishDamageEntityRepository.findByConditions(startTime, endTime, baseId, pondId, fishTypeId);
    if (CollectionUtils.isEmpty(fishDamageEntities)) {
      return Lists.newArrayList();
    }
    Map<String, PondEntity> ponds = Maps.newTreeMap();
    for (FishDamageEntity fishDamageEntity : fishDamageEntities) {
      ponds.put(fishDamageEntity.getPond().getId(), fishDamageEntity.getPond());
    }
    ponds.entrySet().forEach(entry -> {
      fishDamageReportByPond(result, fishDamageEntities, entry.getValue(), startTime, endTime);
    });
    return result;
  }

  private void fishDamageReportByPond(List<FishDamageReportVo> result, List<FishDamageEntity> fishDamageEntities, PondEntity pond, Date startTime, Date endTime) {
    Map<String, FishTypeEntity> fishTypes = Maps.newTreeMap();
    for (FishDamageEntity fishDamageEntity : fishDamageEntities) {
      if (pond.getId().equals(fishDamageEntity.getPond().getId()) && fishDamageEntity.getFishType() != null) {
        fishTypes.put(fishDamageEntity.getFishType().getId(), fishDamageEntity.getFishType());
      }
    }
    fishTypes.entrySet().forEach(entry -> {
      fishDamageReportByFishType(result, fishDamageEntities, pond, startTime, endTime, entry.getValue());
    });
  }

  private void fishDamageReportByFishType(List<FishDamageReportVo> result, List<FishDamageEntity> fishDamageEntities, PondEntity pond, Date startTime, Date endTime, FishTypeEntity fishTypeEntity) {
    FishDamageReportVo vo = new FishDamageReportVo();
    vo.setStartTime(startTime);
    vo.setEndTime(endTime);
    vo.setPondId(pond.getId());
    vo.setPondName(pond.getName());
    vo.setBaseId(pond.getBase().getId());
    vo.setBaseName(pond.getBase().getBaseOrg().getOrgName());
    double count = 0;
    double weight = 0;
    for (FishDamageEntity fishDamageEntity : fishDamageEntities) {
      if (pond.getId().equals(fishDamageEntity.getPond().getId()) && fishTypeEntity.getId().equals(fishDamageEntity.getFishType().getId())) {
        count += fishDamageEntity.getCount() == null ? 0 : fishDamageEntity.getCount();
        weight += fishDamageEntity.getWeight() == null ? 0 : fishDamageEntity.getWeight();
      }
    }
    vo.setCount(count);
    vo.setWeight(weight);
    vo.setFishTypeId(fishTypeEntity.getId());
    vo.setFishTypeName(fishTypeEntity.getName());
    if (pond.getBase().getBaseOrg().getParent() != null) {
      vo.setCompanyName(pond.getBase().getBaseOrg().getParent().getOrgName());
      vo.setCompanyId(pond.getBase().getBaseOrg().getParent().getId());
    }
    double deadRate;
    double liveRate;
    if (startTime == null) {
      startTime = DateUtils.dateParse("1971-01-01", DateUtils.DEFAULT_DATE_SIMPLE_PATTERN);
    }
    if (endTime == null) {
      endTime = DateUtils.dateParse("2200-01-01", DateUtils.DEFAULT_DATE_SIMPLE_PATTERN);
    }
    double exiteCount = proofYieldEntityService.computeLastExitCount(pond.getId(), startTime, endTime, fishTypeEntity.getId());
    if (exiteCount + vo.getCount() == 0) {
      deadRate = 0;
      liveRate = 1;
    } else {
      deadRate = vo.getCount() / (exiteCount + vo.getCount());
      liveRate = 1 - deadRate;
    }
    vo.setDeadRate(MathUtil.round(deadRate, 6));
    vo.setLiveRate(MathUtil.round(liveRate, 6));
    result.add(vo);
  }

  private void fishDamageReport(List<FishDamageReportVo> result, PondEntity pond, Date startTime, Date endTime, FishTypeEntity fishTypeEntity, FishDamageEntity fishDamageEntity) {

  }

} 
