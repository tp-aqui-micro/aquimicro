package com.tw.aquaculture.process.service.internal;

import com.tw.aquaculture.common.entity.base.FishPriceEntity;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.process.repository.FishPriceEntityRepository;
import com.tw.aquaculture.process.service.FishPriceEntityService;
import com.tw.aquaculture.process.service.FishTypeEntityService;
import com.tw.aquaculture.process.service.TwBaseEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * FishPriceEntity业务模型的服务层接口实现
 * @author saturn
 */
@Service("FishPriceEntityServiceImpl")
public class FishPriceEntityServiceImpl implements FishPriceEntityService {
  @Autowired
  private TwBaseEntityService twBaseEntityService;
  @Autowired
  private FishTypeEntityService fishTypeEntityService;
  @Autowired
  private FishPriceEntityRepository fishPriceEntityRepository;
  @Transactional
  @Override
  public FishPriceEntity create(FishPriceEntity fishPriceEntity) {
   return this.createForm(fishPriceEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public FishPriceEntity createForm(FishPriceEntity fishPriceEntity) {
   /* 
    * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
    * 1、如果当前模型对象不是主模型
    * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
    * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
    * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
    * 2、如果当前模型对象是主业务模型
    *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
    *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
    *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
    * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
    *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
    *   2.3.2、以及验证每个分组的OneToMany明细信息
    * */
    this.createValidation(fishPriceEntity);
    
    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    
    this.fishPriceEntityRepository.saveAndFlush(fishPriceEntity);
    
    // 返回最终处理的结果，里面带有详细的关联信息
    return fishPriceEntity;
  }
  /**
   * 在创建一个新的FishPriceEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(FishPriceEntity fishPriceEntity) {
    Validate.notNull(fishPriceEntity , "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(fishPriceEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    fishPriceEntity.setId(null);
    Validate.notBlank(fishPriceEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(fishPriceEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(fishPriceEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(fishPriceEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notNull(fishPriceEntity.getSpecsMin(), "添加信息时，最小值不能为空！");
    Validate.notNull(fishPriceEntity.getSpecsMax(), "添加信息时，最大值不能为空！");
    Validate.notNull(fishPriceEntity.getPrice(), "添加信息时，价格不能为空！");
    Validate.notNull(fishPriceEntity.getStartTime(), "添加信息时，开始时间不能为空！");
    Validate.notNull(fishPriceEntity.getEndTime(), "添加信息时，结束时间不能为空！");
    Validate.notBlank(fishPriceEntity.getProvince(), "添加信息时，省不能为空！");
    Validate.notBlank(fishPriceEntity.getCity(), "添加信息时，市不能为空！");
    Validate.notNull(fishPriceEntity.getEnableState(), "添加信息时，生效状态不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(fishPriceEntity.getFormInstanceId() == null || fishPriceEntity.getFormInstanceId().length() < 255 , "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishPriceEntity.getCreateName() == null || fishPriceEntity.getCreateName().length() < 255 , "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishPriceEntity.getCreatePosition() == null || fishPriceEntity.getCreatePosition().length() < 255 , "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishPriceEntity.getModifyName() == null || fishPriceEntity.getModifyName().length() < 255 , "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishPriceEntity.getModifyPosition() == null || fishPriceEntity.getModifyPosition().length() < 255 , "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishPriceEntity.getProvince() == null || fishPriceEntity.getProvince().length() < 255 , "省,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishPriceEntity.getCity() == null || fishPriceEntity.getCity().length() < 255 , "市,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishPriceEntity.getArea() == null || fishPriceEntity.getArea().length() < 255 , "区/县,在进行添加时填入值超过了限定长度(255)，请检查!");
    FishPriceEntity currentFishPriceEntity = this.findByFormInstanceId(fishPriceEntity.getFormInstanceId());
    Validate.isTrue(currentFishPriceEntity == null, "表单实例编号已存在,请检查");
    // 验证ManyToOne关联：基地绑定值的正确性
    TwBaseEntity currentBase = fishPriceEntity.getBase();
    Validate.notNull(currentBase , "基地信息必须传入，请检查!!");
    String currentPkBase = currentBase.getId();
    Validate.notBlank(currentPkBase, "创建操作时，当前基地信息必须关联！");
    Validate.notNull(this.twBaseEntityService.findById(currentPkBase) , "基地关联信息未找到，请检查!!");
    // 验证ManyToOne关联：鱼种绑定值的正确性
    FishTypeEntity currentFishType = fishPriceEntity.getFishType();
    Validate.notNull(currentFishType , "鱼种信息必须传入，请检查!!");
    String currentPkFishType = currentFishType.getId();
    Validate.notBlank(currentPkFishType, "创建操作时，当前鱼种信息必须关联！");
    Validate.notNull(this.fishTypeEntityService.findById(currentPkFishType) , "鱼种关联信息未找到，请检查!!");
  }
  @Transactional
  @Override
  public FishPriceEntity update(FishPriceEntity fishPriceEntity) {
     return this.updateForm(fishPriceEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public FishPriceEntity updateForm(FishPriceEntity fishPriceEntity) {
    /* 
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     * 
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    
    this.updateValidation(fishPriceEntity);
    // ===================基本信息
    String currentId = fishPriceEntity.getId();
    Optional<FishPriceEntity> opCurrentFishPriceEntity = this.fishPriceEntityRepository.findById(currentId);
    FishPriceEntity currentFishPriceEntity = opCurrentFishPriceEntity.orElse(null);
    Validate.notNull(currentFishPriceEntity ,"未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentFishPriceEntity.setFormInstanceId(fishPriceEntity.getFormInstanceId());
    currentFishPriceEntity.setCreateTime(fishPriceEntity.getCreateTime());
    currentFishPriceEntity.setCreateName(fishPriceEntity.getCreateName());
    currentFishPriceEntity.setCreatePosition(fishPriceEntity.getCreatePosition());
    currentFishPriceEntity.setModifyTime(fishPriceEntity.getModifyTime());
    currentFishPriceEntity.setModifyName(fishPriceEntity.getModifyName());
    currentFishPriceEntity.setModifyPosition(fishPriceEntity.getModifyPosition());
    currentFishPriceEntity.setSpecsMin(fishPriceEntity.getSpecsMin());
    currentFishPriceEntity.setSpecsMax(fishPriceEntity.getSpecsMax());
    currentFishPriceEntity.setPrice(fishPriceEntity.getPrice());
    currentFishPriceEntity.setStartTime(fishPriceEntity.getStartTime());
    currentFishPriceEntity.setEndTime(fishPriceEntity.getEndTime());
    currentFishPriceEntity.setProvince(fishPriceEntity.getProvince());
    currentFishPriceEntity.setCity(fishPriceEntity.getCity());
    currentFishPriceEntity.setArea(fishPriceEntity.getArea());
    currentFishPriceEntity.setEnableState(fishPriceEntity.getEnableState());
    currentFishPriceEntity.setBase(fishPriceEntity.getBase());
    currentFishPriceEntity.setFishType(fishPriceEntity.getFishType());
    
    this.fishPriceEntityRepository.saveAndFlush(currentFishPriceEntity);
    return currentFishPriceEntity;
  }
  /**
   * 在更新一个已有的FishPriceEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(FishPriceEntity fishPriceEntity) {
    Validate.isTrue(!StringUtils.isBlank(fishPriceEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
    
    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(fishPriceEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(fishPriceEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(fishPriceEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(fishPriceEntity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    Validate.notNull(fishPriceEntity.getSpecsMin(), "修改信息时，最小值不能为空！");
    Validate.notNull(fishPriceEntity.getSpecsMax(), "修改信息时，最大值不能为空！");
    Validate.notNull(fishPriceEntity.getPrice(), "修改信息时，价格不能为空！");
    Validate.notNull(fishPriceEntity.getStartTime(), "修改信息时，开始时间不能为空！");
    Validate.notNull(fishPriceEntity.getEndTime(), "修改信息时，结束时间不能为空！");
    Validate.notBlank(fishPriceEntity.getProvince(), "修改信息时，省不能为空！");
    Validate.notBlank(fishPriceEntity.getCity(), "修改信息时，市不能为空！");
    Validate.notNull(fishPriceEntity.getEnableState(), "修改信息时，生效状态不能为空！");
    
    // 重复性判断，基本属性，需要满足unique = true
    FishPriceEntity currentForFormInstanceId = this.findByFormInstanceId(fishPriceEntity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId() , fishPriceEntity.getId()) , "表单实例编号已存在,请检查"); 
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(fishPriceEntity.getFormInstanceId() == null || fishPriceEntity.getFormInstanceId().length() < 255 , "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishPriceEntity.getCreateName() == null || fishPriceEntity.getCreateName().length() < 255 , "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishPriceEntity.getCreatePosition() == null || fishPriceEntity.getCreatePosition().length() < 255 , "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishPriceEntity.getModifyName() == null || fishPriceEntity.getModifyName().length() < 255 , "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishPriceEntity.getModifyPosition() == null || fishPriceEntity.getModifyPosition().length() < 255 , "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishPriceEntity.getProvince() == null || fishPriceEntity.getProvince().length() < 255 , "省,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishPriceEntity.getCity() == null || fishPriceEntity.getCity().length() < 255 , "市,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(fishPriceEntity.getArea() == null || fishPriceEntity.getArea().length() < 255 , "区/县,在进行修改时填入值超过了限定长度(255)，请检查!");
    
    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
    TwBaseEntity currentForBase = fishPriceEntity.getBase();
    Validate.notNull(currentForBase , "修改信息时，基地必须传入，请检查!!");
    FishTypeEntity currentForFishType = fishPriceEntity.getFishType();
    Validate.notNull(currentForFishType , "修改信息时，鱼种必须传入，请检查!!");
  } 
  @Override
  public FishPriceEntity findDetailsById(String id) {
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(id)) { 
      return null; 
    } 
    FishPriceEntity current = this.fishPriceEntityRepository.findDetailsById(id);
    if(current == null) {
      return null;
    } 
    return current;
  }
  @Override
  public FishPriceEntity findById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    
    Optional<FishPriceEntity> op = fishPriceEntityRepository.findById(id);
    return op.orElse(null); 
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id , "进行删除时，必须给定主键信息!!");
    FishPriceEntity current = this.findById(id);
    if(current != null) { 
      this.fishPriceEntityRepository.delete(current);
    }
  }
  @Override
  public FishPriceEntity findDetailsByFormInstanceId(String formInstanceId) {
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(formInstanceId)) { 
      return null; 
    } 
    FishPriceEntity current = this.fishPriceEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if(current == null) {
      return null;
    } 
    return current;
  }
  @Override
  public FishPriceEntity findByFormInstanceId(String formInstanceId) {
    if(StringUtils.isBlank(formInstanceId)) { 
      return null;
    }
    return this.fishPriceEntityRepository.findByFormInstanceId(formInstanceId);
  } 
} 
