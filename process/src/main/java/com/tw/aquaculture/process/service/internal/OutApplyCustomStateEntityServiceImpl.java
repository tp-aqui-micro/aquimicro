package com.tw.aquaculture.process.service.internal;

import com.tw.aquaculture.common.entity.process.OutApplyCustomStateEntity;
import com.tw.aquaculture.process.repository.OutApplyCustomStateEntityRepository;
import com.tw.aquaculture.process.service.OutApplyCustomStateEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * OutApplyCustomStateEntity业务模型的服务层接口实现
 * @author saturn
 */
@Service("OutApplyCustomStateEntityServiceImpl")
public class OutApplyCustomStateEntityServiceImpl implements OutApplyCustomStateEntityService {
  @Autowired
  private OutApplyCustomStateEntityRepository outApplyCustomStateEntityRepository;

  @Transactional
  @Override
  public OutApplyCustomStateEntity create(OutApplyCustomStateEntity outApplyCustomStateEntity) {
    return  this.createForm(outApplyCustomStateEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public OutApplyCustomStateEntity createForm(OutApplyCustomStateEntity outApplyCustomStateEntity) {
   /* 
    * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
    * 1、如果当前模型对象不是主模型
    * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
    * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
    * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
    * 2、如果当前模型对象是主业务模型
    *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
    *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
    *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
    * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
    *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
    *   2.3.2、以及验证每个分组的OneToMany明细信息
    * */
    if(outApplyCustomStateEntity.getState()==null){
      outApplyCustomStateEntity.setState(0);
    }
    this.createValidation(outApplyCustomStateEntity);
    
    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    this.outApplyCustomStateEntityRepository.save(outApplyCustomStateEntity);
    
    // 返回最终处理的结果，里面带有详细的关联信息
    return outApplyCustomStateEntity;
  }
  /**
   * 在创建一个新的OutApplyCustomStateEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(OutApplyCustomStateEntity outApplyCustomStateEntity) {
    Validate.notNull(outApplyCustomStateEntity , "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(outApplyCustomStateEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    outApplyCustomStateEntity.setId(null);
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况）
  }
  @Transactional
  @Override
  public OutApplyCustomStateEntity update(OutApplyCustomStateEntity outApplyCustomStateEntity) {
    return this.updateForm(outApplyCustomStateEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public OutApplyCustomStateEntity updateForm(OutApplyCustomStateEntity outApplyCustomStateEntity) {
    if(StringUtils.isNotBlank(outApplyCustomStateEntity.getId())){
      throw new UnsupportedOperationException("暂不支持修改");
    }
    /* 
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     * 
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    if(outApplyCustomStateEntity.getState()==null){
      outApplyCustomStateEntity.setState(0);
    }
    this.updateValidation(outApplyCustomStateEntity);
    // ===================基本信息
    String currentId = outApplyCustomStateEntity.getId();
    Optional<OutApplyCustomStateEntity> opCurrentOutApplyCustomStateEntity = this.outApplyCustomStateEntityRepository.findById(currentId);
    OutApplyCustomStateEntity currentOutApplyCustomStateEntity = opCurrentOutApplyCustomStateEntity.orElse(null);
    Validate.notNull(currentOutApplyCustomStateEntity ,"未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentOutApplyCustomStateEntity.setState(outApplyCustomStateEntity.getState());
    
    this.outApplyCustomStateEntityRepository.saveAndFlush(currentOutApplyCustomStateEntity);
    return currentOutApplyCustomStateEntity;
  }
  /**
   * 在更新一个已有的OutApplyCustomStateEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(OutApplyCustomStateEntity outApplyCustomStateEntity) {
    Validate.isTrue(!StringUtils.isBlank(outApplyCustomStateEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
    
    // 基础信息判断，基本属性，需要满足not null
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    
    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
  } 
  @Override
  public OutApplyCustomStateEntity findDetailsByOutFishApply(String outFishApply) {
    if(StringUtils.isBlank(outFishApply)) { 
      return null;
    }
    return this.outApplyCustomStateEntityRepository.findDetailsByOutFishApply(outFishApply);
  }
  @Override
  public OutApplyCustomStateEntity findDetailsByCustomer(String customer) {
    if(StringUtils.isBlank(customer)) { 
      return null;
    }
    return this.outApplyCustomStateEntityRepository.findDetailsByCustomer(customer);
  }
  @Override
  public OutApplyCustomStateEntity findDetailsById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    return this.outApplyCustomStateEntityRepository.findDetailsById(id);
  }
  @Override
  public OutApplyCustomStateEntity findById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    
    Optional<OutApplyCustomStateEntity> op = outApplyCustomStateEntityRepository.findById(id);
    return op.orElse(null); 
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id , "进行删除时，必须给定主键信息!!");
    OutApplyCustomStateEntity current = this.findById(id);
    if(current != null) { 
      this.outApplyCustomStateEntityRepository.delete(current);
    }
  }

  @Override
  public List<OutApplyCustomStateEntity> findByOutFishApplyIdAndState(String outFishApplyId, int state) {
    if(StringUtils.isBlank(outFishApplyId)){
      return Collections.emptyList();
    }
    if(state == -1){
      return this.outApplyCustomStateEntityRepository.findByOutFishApplyId(outFishApplyId);
    }
    return this.outApplyCustomStateEntityRepository.findByOutFishApplyIdAndState(outFishApplyId, state);
  }

  @Override
  public List<OutApplyCustomStateEntity> findByOutFishApply(String outFishApplyId) {
    if(StringUtils.isBlank(outFishApplyId)){
      return Collections.emptyList();
    }
    return outApplyCustomStateEntityRepository.findByOutFishApplyId(outFishApplyId);
  }
} 
