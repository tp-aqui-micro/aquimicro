package com.tw.aquaculture.process.service.internal;

import com.bizunited.platform.kuiper.starter.service.KuiperToolkitService;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.process.CustomerInfo;
import com.tw.aquaculture.common.entity.process.OutApplyCustomStateEntity;
import com.tw.aquaculture.common.entity.process.OutFishApplyEntity;
import com.tw.aquaculture.common.entity.process.RemainInfoEntity;
import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.enums.ResureStateEnum;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.common.vo.outfishapply.ApplyBackCustomerVo;
import com.tw.aquaculture.common.vo.outfishapply.OutFishApplyVo;
import com.tw.aquaculture.common.vo.outfishapply.PageParam;
import com.tw.aquaculture.common.vo.outfishapply.PageResult;
import com.tw.aquaculture.process.remote.saleapply.SaleRemoteService;
import com.tw.aquaculture.process.repository.OutFishApplyEntityRepository;
import com.tw.aquaculture.process.service.CustomerInfoService;
import com.tw.aquaculture.process.service.OutApplyCustomStateEntityService;
import com.tw.aquaculture.process.service.OutFishApplyEntityService;
import com.tw.aquaculture.process.service.PondEntityService;
import com.tw.aquaculture.process.service.RemainInfoEntityService;
import com.tw.aquaculture.process.service.WorkOrderEntityService;
import com.tw.aquaculture.process.service.base.CodeGenerateService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * OutFishApplyEntity业务模型的服务层接口实现
 * @author saturn
 */
@Service("OutFishApplyEntityServiceImpl")
public class OutFishApplyEntityServiceImpl implements OutFishApplyEntityService {
  @Autowired
  private PondEntityService pondEntityService;
  @Autowired
  private RemainInfoEntityService remainInfoEntityService;
  @Autowired
  private CustomerInfoService customerInfoService;
  @Autowired
  private WorkOrderEntityService workOrderEntityService;
  @Autowired
  private OutFishApplyEntityRepository outFishApplyEntityRepository;
  @Autowired
  private CodeGenerateService codeGenerateService;
  @Autowired
  private SaleRemoteService saleRemoteService;
  @Autowired
  private OutApplyCustomStateEntityService outApplyCustomStateEntityService;
  /** 
   * Kuiper表单引擎用于减少编码工作量的工具包 
   */ 
  @Autowired
  private KuiperToolkitService kuiperToolkitService;
  @Transactional
  @Override
  public OutFishApplyEntity create(OutFishApplyEntity outFishApplyEntity) {
    return this.createForm(outFishApplyEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public OutFishApplyEntity createForm(OutFishApplyEntity outFishApplyEntity) {
   /* 
    * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
    * 1、如果当前模型对象不是主模型
    * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
    * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
    * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
    * 2、如果当前模型对象是主业务模型
    *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
    *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
    *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
    * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
    *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
    *   2.3.2、以及验证每个分组的OneToMany明细信息
    * */
    String account = this.getPrincipal().getName();
    outFishApplyEntity.setCode(codeGenerateService.generateBusinessCode(CodeTypeEnum.OUT_FISH_APPLY_ENTITY));
    List<WorkOrderEntity> workOrderEntities = workOrderEntityService.findByStateAndPond(outFishApplyEntity.getPond().getId(), EnableStateEnum.DISABLE.getState());
    if(!CollectionUtils.isEmpty(workOrderEntities) && StringUtils.isNotBlank(workOrderEntities.get(0).getId())){
      outFishApplyEntity.setWorkOrder(workOrderEntities.get(workOrderEntities.size()-1));
    }else{
      throw new IllegalArgumentException("请先对该塘口注水");
    }
    if(!DateUtils.between(outFishApplyEntity.getApplyTime(), outFishApplyEntity.getWorkOrder().getCreateTime(), new Date())){
      throw new IllegalArgumentException("填写时间不在养殖周期内，请重新填写");
    }
    this.createValidation(outFishApplyEntity);
    
    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    outFishApplyEntity.setResureState(ResureStateEnum.UN_SUBMIT.getState());
    this.outFishApplyEntityRepository.saveAndFlush(outFishApplyEntity);
    // 处理明细信息：存塘信息
    Set<RemainInfoEntity> remainInfoEntityItems = outFishApplyEntity.getRemainInfos();
    if(remainInfoEntityItems != null) {
      for (RemainInfoEntity remainInfoEntityItem : remainInfoEntityItems) {
        remainInfoEntityItem.setOutFishApply(outFishApplyEntity);
        this.remainInfoEntityService.create(remainInfoEntityItem);
      }
    }

    this.outFishApplyEntityRepository.flush();
    // 返回最终处理的结果，里面带有详细的关联信息
    //提交审批流

    //提交审批
    this.saleRemoteService.saleApply(outFishApplyEntity, account);
    return outFishApplyEntity;
  }
  /**
   * 在创建一个新的OutFishApplyEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(OutFishApplyEntity outFishApplyEntity) {
    Validate.notNull(outFishApplyEntity , "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(outFishApplyEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    outFishApplyEntity.setId(null);
    Validate.notBlank(outFishApplyEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(outFishApplyEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(outFishApplyEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(outFishApplyEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notBlank(outFishApplyEntity.getCode(), "添加信息时，编码不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(outFishApplyEntity.getFormInstanceId() == null || outFishApplyEntity.getFormInstanceId().length() < 255 , "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishApplyEntity.getCreateName() == null || outFishApplyEntity.getCreateName().length() < 255 , "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishApplyEntity.getCreatePosition() == null || outFishApplyEntity.getCreatePosition().length() < 255 , "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishApplyEntity.getModifyName() == null || outFishApplyEntity.getModifyName().length() < 255 , "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishApplyEntity.getModifyPosition() == null || outFishApplyEntity.getModifyPosition().length() < 255 , "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishApplyEntity.getCode() == null || outFishApplyEntity.getCode().length() < 255 , "编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishApplyEntity.getApplyReason() == null || outFishApplyEntity.getApplyReason().length() < 255 , "申请理由,在进行添加时填入值超过了限定长度(255)，请检查!");
    OutFishApplyEntity currentOutFishApplyEntity = this.findByFormInstanceId(outFishApplyEntity.getFormInstanceId());
    Validate.isTrue(currentOutFishApplyEntity == null, "表单实例编号已存在,请检查");
    // 验证ManyToOne关联：塘口绑定值的正确性
    PondEntity currentPond = outFishApplyEntity.getPond();
    Validate.notNull(currentPond , "塘口信息必须传入，请检查!!");
    String currentPkPond = currentPond.getId();
    Validate.notBlank(currentPkPond, "创建操作时，当前塘口信息必须关联！");
    Validate.notNull(this.pondEntityService.findById(currentPkPond) , "塘口关联信息未找到，请检查!!");
  }
  @Transactional
  @Override
  public OutFishApplyEntity update(OutFishApplyEntity outFishApplyEntity) {
    return this.updateForm(outFishApplyEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public OutFishApplyEntity updateForm(OutFishApplyEntity outFishApplyEntity) {
    /* 
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     * 
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    
    this.updateValidation(outFishApplyEntity);
    // ===================基本信息
    String currentId = outFishApplyEntity.getId();
    Optional<OutFishApplyEntity> opCurrentOutFishApplyEntity = this.outFishApplyEntityRepository.findById(currentId);
    OutFishApplyEntity currentOutFishApplyEntity = opCurrentOutFishApplyEntity.orElse(null);
    Validate.notNull(currentOutFishApplyEntity ,"未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentOutFishApplyEntity.setFormInstanceId(outFishApplyEntity.getFormInstanceId());
    currentOutFishApplyEntity.setCreateTime(outFishApplyEntity.getCreateTime());
    currentOutFishApplyEntity.setCreateName(outFishApplyEntity.getCreateName());
    currentOutFishApplyEntity.setCreatePosition(outFishApplyEntity.getCreatePosition());
    currentOutFishApplyEntity.setModifyTime(outFishApplyEntity.getModifyTime());
    currentOutFishApplyEntity.setModifyName(outFishApplyEntity.getModifyName());
    currentOutFishApplyEntity.setModifyPosition(outFishApplyEntity.getModifyPosition());
    currentOutFishApplyEntity.setCode(outFishApplyEntity.getCode());
    currentOutFishApplyEntity.setApplyReason(outFishApplyEntity.getApplyReason());
    currentOutFishApplyEntity.setResureState(outFishApplyEntity.getResureState());
    currentOutFishApplyEntity.setApplyTime(outFishApplyEntity.getApplyTime());
    currentOutFishApplyEntity.setPredictTime(outFishApplyEntity.getPredictTime());
    currentOutFishApplyEntity.setPond(outFishApplyEntity.getPond());
    currentOutFishApplyEntity.setWorkOrder(outFishApplyEntity.getWorkOrder());
    currentOutFishApplyEntity.setInstid(outFishApplyEntity.getInstid());
    currentOutFishApplyEntity.setReqNo(outFishApplyEntity.getReqNo());
    currentOutFishApplyEntity.setResureIdea(outFishApplyEntity.getResureIdea());
    if(!DateUtils.between(currentOutFishApplyEntity.getApplyTime(), currentOutFishApplyEntity.getWorkOrder().getCreateTime(), new Date())){
      throw new IllegalArgumentException("填写时间不在养殖周期内，请重新填写");
    }
    this.outFishApplyEntityRepository.saveAndFlush(currentOutFishApplyEntity);
    // ==========准备处理OneToMany关系的详情remainInfos
    // 清理出那些需要删除的remainInfos信息
    Set<RemainInfoEntity> remainInfoEntitys = null;
    if(outFishApplyEntity.getRemainInfos() != null) { 
      remainInfoEntitys = Sets.newLinkedHashSet(outFishApplyEntity.getRemainInfos());
    } else { 
      remainInfoEntitys = Sets.newLinkedHashSet();
    } 
    Set<RemainInfoEntity> dbRemainInfoEntitys = this.remainInfoEntityService.findDetailsByOutFishApply(outFishApplyEntity.getId());
    if(dbRemainInfoEntitys == null) { 
      dbRemainInfoEntitys = Sets.newLinkedHashSet();
    } 
    // 求得的差集既是需要删除的数据 
    Set<String> mustDeleteRemainInfosPks = kuiperToolkitService.collectionDiffent(dbRemainInfoEntitys, remainInfoEntitys, RemainInfoEntity::getId);
    if(mustDeleteRemainInfosPks != null && !mustDeleteRemainInfosPks.isEmpty()) { 
      for (String mustDeleteRemainInfosPk : mustDeleteRemainInfosPks) { 
        this.remainInfoEntityService.deleteById(mustDeleteRemainInfosPk);
      } 
    } 
    // 对传来的记录进行添加或者修改操作 
    for (RemainInfoEntity remainInfoEntityItem : remainInfoEntitys) {
      remainInfoEntityItem.setOutFishApply(outFishApplyEntity); 
      if(StringUtils.isBlank(remainInfoEntityItem.getId())) {
        remainInfoEntityService.create(remainInfoEntityItem); 
      } else { 
        remainInfoEntityService.update(remainInfoEntityItem);
      }
    }
    // ==========准备处理OneToMany关系的详情customerInfos
    // 清理出那些需要删除的customerInfos信息
    setCustomerInfos(outFishApplyEntity);
    return currentOutFishApplyEntity;
  }

  /**
   * 清理出那些需要删除的customerInfos信息
   * @param outFishApplyEntity 出鱼申请信息
   */
  private void setCustomerInfos(OutFishApplyEntity outFishApplyEntity){
    Set<CustomerInfo> customerInfos = null;
    if(outFishApplyEntity.getCustomerInfos() != null) {
      customerInfos = Sets.newLinkedHashSet(outFishApplyEntity.getCustomerInfos());
    } else {
      customerInfos = Sets.newLinkedHashSet();
    }
    Set<CustomerInfo> dbCustomerInfos = this.customerInfoService.findDetailsByOutFishApply(outFishApplyEntity.getId());
    if(dbCustomerInfos == null) {
      dbCustomerInfos = Sets.newLinkedHashSet();
    }
    // 求得的差集既是需要删除的数据
    Set<String> mustDeleteCustomerInfosPks = kuiperToolkitService.collectionDiffent(dbCustomerInfos, customerInfos, CustomerInfo::getId);
    if(mustDeleteCustomerInfosPks != null && !mustDeleteCustomerInfosPks.isEmpty()) {
      for (String mustDeleteCustomerInfosPk : mustDeleteCustomerInfosPks) {
        this.customerInfoService.deleteById(mustDeleteCustomerInfosPk);
      }
    }
    // 对传来的记录进行添加或者修改操作
    for (CustomerInfo customerInfoItem : customerInfos) {
      customerInfoItem.setOutFishApply(outFishApplyEntity);
      if(StringUtils.isBlank(customerInfoItem.getId())) {
        customerInfoService.create(customerInfoItem);
      } else {
        customerInfoService.update(customerInfoItem);
      }
    }
  }

  /**
   * 在更新一个已有的OutFishApplyEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(OutFishApplyEntity outFishApplyEntity) {
    Validate.isTrue(!StringUtils.isBlank(outFishApplyEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
    
    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(outFishApplyEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(outFishApplyEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(outFishApplyEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(outFishApplyEntity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    Validate.notBlank(outFishApplyEntity.getCode(), "修改信息时，编码不能为空！");
    
    // 重复性判断，基本属性，需要满足unique = true
    OutFishApplyEntity currentForFormInstanceId = this.findByFormInstanceId(outFishApplyEntity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId() , outFishApplyEntity.getId()) , "表单实例编号已存在,请检查"); 
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(outFishApplyEntity.getFormInstanceId() == null || outFishApplyEntity.getFormInstanceId().length() < 255 , "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishApplyEntity.getCreateName() == null || outFishApplyEntity.getCreateName().length() < 255 , "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishApplyEntity.getCreatePosition() == null || outFishApplyEntity.getCreatePosition().length() < 255 , "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishApplyEntity.getModifyName() == null || outFishApplyEntity.getModifyName().length() < 255 , "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishApplyEntity.getModifyPosition() == null || outFishApplyEntity.getModifyPosition().length() < 255 , "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishApplyEntity.getCode() == null || outFishApplyEntity.getCode().length() < 255 , "编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishApplyEntity.getApplyReason() == null || outFishApplyEntity.getApplyReason().length() < 255 , "申请理由,在进行修改时填入值超过了限定长度(255)，请检查!");
    
    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
    PondEntity currentForPond = outFishApplyEntity.getPond();
    Validate.notNull(currentForPond , "修改信息时，塘口必须传入，请检查!!");
  } 
  @Override
  public OutFishApplyEntity findDetailsById(String id) {
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(id)) { 
      return null; 
    } 
    OutFishApplyEntity current = this.outFishApplyEntityRepository.findDetailsById(id);
    if(current == null) {
      return null;
    } 
    String currentPkValue = current.getId();
    // 2、======
    // 存塘信息 的明细信息查询
    Collection<RemainInfoEntity> oneToManyRemainInfosDetails = this.remainInfoEntityService.findDetailsByOutFishApply(currentPkValue);
    current.setRemainInfos(Sets.newLinkedHashSet(oneToManyRemainInfosDetails));
    // 客户信息 的明细信息查询
    Collection<CustomerInfo> oneToManyCustomerInfosDetails = this.customerInfoService.findDetailsByOutFishApply(currentPkValue);
    current.setCustomerInfos(Sets.newLinkedHashSet(oneToManyCustomerInfosDetails));
    
    return current;
  }
  @Override
  public OutFishApplyEntity findById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    
    Optional<OutFishApplyEntity> op = outFishApplyEntityRepository.findById(id);
    return op.orElse(null); 
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id , "进行删除时，必须给定主键信息!!");
    OutFishApplyEntity current = this.findById(id);
    if(current != null) { 
      this.outFishApplyEntityRepository.delete(current);
    }
  }
  @Override
  public OutFishApplyEntity findDetailsByFormInstanceId(String formInstanceId) {
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(formInstanceId)) { 
      return null; 
    } 
    OutFishApplyEntity current = this.outFishApplyEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if(current == null) {
      return null;
    } 
    String currentPkValue = current.getId();
    // 2、======
    // 存塘信息 的明细信息查询
    Collection<RemainInfoEntity> oneToManyRemainInfosDetails = this.remainInfoEntityService.findDetailsByOutFishApply(currentPkValue);
    current.setRemainInfos(Sets.newLinkedHashSet(oneToManyRemainInfosDetails));
    // 客户信息 的明细信息查询
    Collection<CustomerInfo> oneToManyCustomerInfosDetails = this.customerInfoService.findDetailsByOutFishApply(currentPkValue);
    current.setCustomerInfos(Sets.newLinkedHashSet(oneToManyCustomerInfosDetails));
    
    return current;
  }
  @Override
  public OutFishApplyEntity findByFormInstanceId(String formInstanceId) {
    if(StringUtils.isBlank(formInstanceId)) { 
      return null;
    }
    return this.outFishApplyEntityRepository.findByFormInstanceId(formInstanceId);
  }

  @Override
  @Transactional
  public void updateResureState(String id, Integer resureState, String message) {
    if(StringUtils.isBlank(id) || resureState==null){
      throw new IllegalArgumentException("修改审批状态时出错,参数不对");
    }
    OutFishApplyEntity outFishApplyEntity = this.findDetailsById(id);
    Validate.notNull(outFishApplyEntity, "修改审批状态时出错,岀鱼申请不存在");
    outFishApplyEntity.setResureState(resureState);
    outFishApplyEntity.setResureIdea(message);
    this.update(outFishApplyEntity);
  }

  @Override
  public String reApply(String id) {
    if(StringUtils.isBlank(id)){
      return null;
    }
    OutFishApplyEntity outFishApplyEntity = this.findDetailsById(id);
    if(!ResureStateEnum.UN_SUBMIT.getState().equals(outFishApplyEntity.getResureState())
      && !ResureStateEnum.RETURN_BACK.getState().equals(outFishApplyEntity.getResureState())){
      throw new IllegalArgumentException("审批状态不正确，不能重复发起审批");
    }
    String account = this.getPrincipal().getName();
    this.saleRemoteService.saleApply(outFishApplyEntity, account);
    return "正在提交审批，请于5秒后刷新页面查看提交结果";
  }

  /**
   * 获取登陆者账号对象
   *
   * @return
   */
  private Principal getPrincipal() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    Validate.notNull(authentication, "未获取到当前系统的登录人");
    return authentication;
  }

  /**
   * 移动端根据基地名称、塘口名称、时间分页查询出鱼申请信息
   * @param params
   * @param pageable
   * @return
   */
  @Override
  public PageResult<List<OutFishApplyVo>> findDetailByOrgBaseNameOrPondNameOrTiem(Map<String, Object> params, PageParam pageable) {
    PageResult<List<OutFishApplyEntity>> page = outFishApplyEntityRepository.findDetailByOrgBaseNameOrPondNameOrTiem(params, pageable);
    List<OutFishApplyEntity> outFishApplyEntities = page.getContent();
    List<OutFishApplyVo> outFishApplyVos = Lists.newArrayList();
    if(!CollectionUtils.isEmpty(outFishApplyEntities)){
      for(OutFishApplyEntity outFishApplyEntity : outFishApplyEntities){
        OutFishApplyVo outFishApplyVo = new OutFishApplyVo();
        outFishApplyVo.setApplyTime(outFishApplyEntity.getApplyTime());
        outFishApplyVo.setCode(outFishApplyEntity.getCode());
        outFishApplyVo.setId(outFishApplyEntity.getId());
        outFishApplyVo.setPondCode(outFishApplyEntity.getPond().getCode());
        outFishApplyVo.setPondId(outFishApplyEntity.getPond().getId());
        outFishApplyVo.setPondName(outFishApplyEntity.getPond().getName());
        outFishApplyVo.setResureState(outFishApplyEntity.getResureState());
        List<OutApplyCustomStateEntity> states = outApplyCustomStateEntityService.findByOutFishApply(outFishApplyEntity.getId());
        List<ApplyBackCustomerVo> applyBackCustomerVos = Lists.newArrayList();
        buildApplyBackCustomerVos(applyBackCustomerVos, states);
        outFishApplyVo.setCustomerVoList(applyBackCustomerVos);
        outFishApplyVos.add(outFishApplyVo);
      }
    }
    return new PageResult<>(outFishApplyVos, pageable, page.getTotal());
  }

  private void buildApplyBackCustomerVos(List<ApplyBackCustomerVo> applyBackCustomerVos, List<OutApplyCustomStateEntity> states) {
    if(CollectionUtils.isEmpty(states)){
      return;
    }
    for(OutApplyCustomStateEntity out : states){
      ApplyBackCustomerVo applyBackCustomerVo = new ApplyBackCustomerVo();
      applyBackCustomerVo.setCustomerId(out.getId());
      applyBackCustomerVo.setCustomerName(out.getCustomer().getName());
      applyBackCustomerVo.setFishTypeName(out.getVarietiesSales());
      applyBackCustomerVo.setSpecs(out.getRequireSpecification());
      applyBackCustomerVos.add(applyBackCustomerVo);
    }
  }
} 
