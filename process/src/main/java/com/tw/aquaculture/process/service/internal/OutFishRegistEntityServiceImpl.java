package com.tw.aquaculture.process.service.internal;

import com.bizunited.platform.kuiper.starter.service.KuiperToolkitService;
import com.google.common.collect.Sets;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.process.OutFishInfoEntity;
import com.tw.aquaculture.common.entity.process.OutFishRegistEntity;
import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.process.repository.OutFishRegistEntityRepository;
import com.tw.aquaculture.process.service.OutFishInfoEntityService;
import com.tw.aquaculture.process.service.OutFishRegistEntityService;
import com.tw.aquaculture.process.service.PondEntityService;
import com.tw.aquaculture.process.service.WorkOrderEntityService;
import com.tw.aquaculture.process.service.base.CodeGenerateService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.*;

/**
 * OutFishRegistEntity业务模型的服务层接口实现
 * @author saturn
 */
@Service("OutFishRegistEntityServiceImpl")
public class OutFishRegistEntityServiceImpl implements OutFishRegistEntityService {
  @Autowired
  private PondEntityService pondEntityService;
  @Autowired
  private OutFishInfoEntityService outFishInfoEntityService;
  @Autowired
  private WorkOrderEntityService workOrderEntityService;
  @Autowired
  private OutFishRegistEntityRepository outFishRegistEntityRepository;
  @Autowired
  private CodeGenerateService codeGenerateService;
  /** 
   * Kuiper表单引擎用于减少编码工作量的工具包 
   */ 
  @Autowired
  private KuiperToolkitService kuiperToolkitService;
  @Transactional
  @Override
  public OutFishRegistEntity create(OutFishRegistEntity outFishRegistEntity) {
   return this.createForm(outFishRegistEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public OutFishRegistEntity createForm(OutFishRegistEntity outFishRegistEntity) {
   /* 
    * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
    * 1、如果当前模型对象不是主模型
    * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
    * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
    * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
    * 2、如果当前模型对象是主业务模型
    *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
    *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
    *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
    * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
    *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
    *   2.3.2、以及验证每个分组的OneToMany明细信息
    * */
    outFishRegistEntity.setCode(codeGenerateService.generateBusinessCode(CodeTypeEnum.OUT_FISH_REGIST_ENTITY));
    List<WorkOrderEntity> workOrderEntities = workOrderEntityService.findByStateAndPond(outFishRegistEntity.getPond().getId(), 1);
    if(!CollectionUtils.isEmpty(workOrderEntities) && StringUtils.isNotBlank(workOrderEntities.get(0).getId())){
      outFishRegistEntity.setWorkOrder(workOrderEntities.get(workOrderEntities.size()-1));
    }else{
      throw new IllegalArgumentException("请先对该塘口注水");
    }
    if(!DateUtils.between(outFishRegistEntity.getRegistTime(), outFishRegistEntity.getWorkOrder().getCreateTime(), new Date())){
      throw new IllegalArgumentException("填写时间不在养殖周期内，请重新填写");
    }
    this.createValidation(outFishRegistEntity);
    
    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    
    this.outFishRegistEntityRepository.saveAndFlush(outFishRegistEntity);
    // 处理明细信息：出鱼信息
    Set<OutFishInfoEntity> outFishInfoEntityItems = outFishRegistEntity.getOutFishInfos();
    if(outFishInfoEntityItems != null) {
      for (OutFishInfoEntity outFishInfoEntityItem : outFishInfoEntityItems) {
        outFishInfoEntityItem.setOutFishRegist(outFishRegistEntity);
        this.outFishInfoEntityService.create(outFishInfoEntityItem);
      }
    }
    
    this.outFishRegistEntityRepository.flush();
    // 返回最终处理的结果，里面带有详细的关联信息
    return outFishRegistEntity;
  }
  /**
   * 在创建一个新的OutFishRegistEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(OutFishRegistEntity outFishRegistEntity) {
    Validate.notNull(outFishRegistEntity , "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(outFishRegistEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    outFishRegistEntity.setId(null);
    Validate.notBlank(outFishRegistEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(outFishRegistEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(outFishRegistEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(outFishRegistEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notBlank(outFishRegistEntity.getCode(), "添加信息时，编码不能为空！");
    Validate.notNull(outFishRegistEntity.getOutFishApply(), "添加信息时岀鱼申请不能为空");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(outFishRegistEntity.getFormInstanceId() == null || outFishRegistEntity.getFormInstanceId().length() < 255 , "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishRegistEntity.getCreateName() == null || outFishRegistEntity.getCreateName().length() < 255 , "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishRegistEntity.getCreatePosition() == null || outFishRegistEntity.getCreatePosition().length() < 255 , "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishRegistEntity.getModifyName() == null || outFishRegistEntity.getModifyName().length() < 255 , "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishRegistEntity.getModifyPosition() == null || outFishRegistEntity.getModifyPosition().length() < 255 , "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishRegistEntity.getCode() == null || outFishRegistEntity.getCode().length() < 255 , "编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishRegistEntity.getSupervisor() == null || outFishRegistEntity.getSupervisor().length() < 255 , "监磅人,在进行添加时填入值超过了限定长度(255)，请检查!");
    OutFishRegistEntity currentOutFishRegistEntity = this.findByFormInstanceId(outFishRegistEntity.getFormInstanceId());
    Validate.isTrue(currentOutFishRegistEntity == null, "表单实例编号已存在,请检查");
    // 验证ManyToOne关联：塘口绑定值的正确性
    PondEntity currentPond = outFishRegistEntity.getPond();
    Validate.notNull(currentPond , "塘口信息必须传入，请检查!!");
    String currentPkPond = currentPond.getId();
    Validate.notBlank(currentPkPond, "创建操作时，当前塘口信息必须关联！");
    Validate.notNull(this.pondEntityService.findById(currentPkPond) , "塘口关联信息未找到，请检查!!");
  }
  @Transactional
  @Override
  public OutFishRegistEntity update(OutFishRegistEntity outFishRegistEntity) {
    return this.updateForm(outFishRegistEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public OutFishRegistEntity updateForm(OutFishRegistEntity outFishRegistEntity) {
    /* 
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     * 
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    
    this.updateValidation(outFishRegistEntity);
    // ===================基本信息
    String currentId = outFishRegistEntity.getId();
    Optional<OutFishRegistEntity> opCurrentOutFishRegistEntity = this.outFishRegistEntityRepository.findById(currentId);
    OutFishRegistEntity currentOutFishRegistEntity = opCurrentOutFishRegistEntity.orElse(null);
    Validate.notNull(currentOutFishRegistEntity ,"未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentOutFishRegistEntity.setFormInstanceId(outFishRegistEntity.getFormInstanceId());
    currentOutFishRegistEntity.setCreateTime(outFishRegistEntity.getCreateTime());
    currentOutFishRegistEntity.setCreateName(outFishRegistEntity.getCreateName());
    currentOutFishRegistEntity.setCreatePosition(outFishRegistEntity.getCreatePosition());
    currentOutFishRegistEntity.setModifyTime(outFishRegistEntity.getModifyTime());
    currentOutFishRegistEntity.setModifyName(outFishRegistEntity.getModifyName());
    currentOutFishRegistEntity.setModifyPosition(outFishRegistEntity.getModifyPosition());
    currentOutFishRegistEntity.setCode(outFishRegistEntity.getCode());
    currentOutFishRegistEntity.setSupervisor(outFishRegistEntity.getSupervisor());
    currentOutFishRegistEntity.setRegistTime(outFishRegistEntity.getRegistTime());
    currentOutFishRegistEntity.setNote(outFishRegistEntity.getNote());
    currentOutFishRegistEntity.setPicture(outFishRegistEntity.getPicture());
    currentOutFishRegistEntity.setPond(outFishRegistEntity.getPond());
    currentOutFishRegistEntity.setCustomer(outFishRegistEntity.getCustomer());
    currentOutFishRegistEntity.setGroupLeader(outFishRegistEntity.getGroupLeader());
    currentOutFishRegistEntity.setWorkOrder(outFishRegistEntity.getWorkOrder());
    currentOutFishRegistEntity.setOutFishApply(outFishRegistEntity.getOutFishApply());
    currentOutFishRegistEntity.setWillCleanPond(outFishRegistEntity.getWillCleanPond());
    if(!DateUtils.between(currentOutFishRegistEntity.getRegistTime(), currentOutFishRegistEntity.getWorkOrder().getCreateTime(), new Date())){
      throw new IllegalArgumentException("填写时间不在养殖周期内，请重新填写");
    }
    this.outFishRegistEntityRepository.saveAndFlush(currentOutFishRegistEntity);
    // ==========准备处理OneToMany关系的详情outFishInfos
    // 清理出那些需要删除的outFishInfos信息
    Set<OutFishInfoEntity> outFishInfoEntitys = null;
    if(outFishRegistEntity.getOutFishInfos() != null) { 
      outFishInfoEntitys = Sets.newLinkedHashSet(outFishRegistEntity.getOutFishInfos());
    } else { 
      outFishInfoEntitys = Sets.newLinkedHashSet();
    } 
    Set<OutFishInfoEntity> dbOutFishInfoEntitys = this.outFishInfoEntityService.findDetailsByOutFishRegist(outFishRegistEntity.getId());
    if(dbOutFishInfoEntitys == null) { 
      dbOutFishInfoEntitys = Sets.newLinkedHashSet();
    } 
    // 求得的差集既是需要删除的数据 
    Set<String> mustDeleteOutFishInfosPks = kuiperToolkitService.collectionDiffent(dbOutFishInfoEntitys, outFishInfoEntitys, OutFishInfoEntity::getId);
    if(mustDeleteOutFishInfosPks != null && !mustDeleteOutFishInfosPks.isEmpty()) { 
      for (String mustDeleteOutFishInfosPk : mustDeleteOutFishInfosPks) { 
        this.outFishInfoEntityService.deleteById(mustDeleteOutFishInfosPk);
      } 
    } 
    // 对传来的记录进行添加或者修改操作 
    for (OutFishInfoEntity outFishInfoEntityItem : outFishInfoEntitys) {
      outFishInfoEntityItem.setOutFishRegist(outFishRegistEntity); 
      if(StringUtils.isBlank(outFishInfoEntityItem.getId())) {
        outFishInfoEntityService.create(outFishInfoEntityItem); 
      } else { 
        outFishInfoEntityService.update(outFishInfoEntityItem);
      }
    }
    
    return currentOutFishRegistEntity;
  }
  /**
   * 在更新一个已有的OutFishRegistEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(OutFishRegistEntity outFishRegistEntity) {
    Validate.isTrue(!StringUtils.isBlank(outFishRegistEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
    
    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(outFishRegistEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(outFishRegistEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(outFishRegistEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(outFishRegistEntity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    Validate.notBlank(outFishRegistEntity.getCode(), "修改信息时，编码不能为空！");
    Validate.notNull(outFishRegistEntity.getOutFishApply(), "修改信息时岀鱼申请不能为空");
    
    // 重复性判断，基本属性，需要满足unique = true
    OutFishRegistEntity currentForFormInstanceId = this.findByFormInstanceId(outFishRegistEntity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId() , outFishRegistEntity.getId()) , "表单实例编号已存在,请检查"); 
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(outFishRegistEntity.getFormInstanceId() == null || outFishRegistEntity.getFormInstanceId().length() < 255 , "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishRegistEntity.getCreateName() == null || outFishRegistEntity.getCreateName().length() < 255 , "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishRegistEntity.getCreatePosition() == null || outFishRegistEntity.getCreatePosition().length() < 255 , "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishRegistEntity.getModifyName() == null || outFishRegistEntity.getModifyName().length() < 255 , "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishRegistEntity.getModifyPosition() == null || outFishRegistEntity.getModifyPosition().length() < 255 , "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishRegistEntity.getCode() == null || outFishRegistEntity.getCode().length() < 255 , "编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(outFishRegistEntity.getSupervisor() == null || outFishRegistEntity.getSupervisor().length() < 255 , "监磅人,在进行修改时填入值超过了限定长度(255)，请检查!");

    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
    PondEntity currentForPond = outFishRegistEntity.getPond();
    Validate.notNull(currentForPond , "修改信息时，塘口必须传入，请检查!!");
  } 
  @Override
  public OutFishRegistEntity findDetailsById(String id) {
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(id)) { 
      return null; 
    } 
    OutFishRegistEntity current = this.outFishRegistEntityRepository.findDetailsById(id);
    if(current == null) {
      return null;
    } 
    String currentPkValue = current.getId();
    // 2、======
    // 出鱼信息 的明细信息查询
    Collection<OutFishInfoEntity> oneToManyOutFishInfosDetails = this.outFishInfoEntityService.findDetailsByOutFishRegist(currentPkValue);
    current.setOutFishInfos(Sets.newLinkedHashSet(oneToManyOutFishInfosDetails));
    
    return current;
  }
  @Override
  public OutFishRegistEntity findById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    
    Optional<OutFishRegistEntity> op = outFishRegistEntityRepository.findById(id);
    return op.orElse(null); 
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id , "进行删除时，必须给定主键信息!!");
    OutFishRegistEntity current = this.findById(id);
    if(current != null) { 
      this.outFishRegistEntityRepository.delete(current);
    }
  }
  @Override
  public OutFishRegistEntity findDetailsByFormInstanceId(String formInstanceId) {
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(formInstanceId)) { 
      return null; 
    } 
    OutFishRegistEntity current = this.outFishRegistEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if(current == null) {
      return null;
    } 
    String currentPkValue = current.getId();
    // 2、======
    // 出鱼信息 的明细信息查询
    Collection<OutFishInfoEntity> oneToManyOutFishInfosDetails = this.outFishInfoEntityService.findDetailsByOutFishRegist(currentPkValue);
    current.setOutFishInfos(Sets.newLinkedHashSet(oneToManyOutFishInfosDetails));
    
    return current;
  }
  @Override
  public OutFishRegistEntity findByFormInstanceId(String formInstanceId) {
    if(StringUtils.isBlank(formInstanceId)) { 
      return null;
    }
    return this.outFishRegistEntityRepository.findByFormInstanceId(formInstanceId);
  }

  @Override
  public List<OutFishRegistEntity> findByPondAndTimes(String pondId, Date startTime, Date endTime) {
    if(StringUtils.isBlank(pondId) || startTime == null || endTime == null){
      return Collections.emptyList();
    }
    return outFishRegistEntityRepository.findByPondAndTimes(pondId, startTime, endTime);
  }

  @Override
  public List<OutFishRegistEntity> findOutFishRegistByOutFishApply(String outFishApplyId) {
    if(StringUtils.isBlank(outFishApplyId)){
      return Collections.emptyList();
    }
    return outFishRegistEntityRepository.findOutFishRegistByOutFishApply(outFishApplyId);
  }

  @Override
  public List<OutFishRegistEntity> findByPondAndWorkOrderId(String pondId, String workOrderId) {
    if(StringUtils.isBlank(pondId) || StringUtils.isBlank(workOrderId)){
      return Collections.emptyList();
    }
    return outFishRegistEntityRepository.findByPondAndWorkOrderId(pondId, workOrderId);
  }
} 
