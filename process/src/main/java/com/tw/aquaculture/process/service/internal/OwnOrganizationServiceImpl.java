package com.tw.aquaculture.process.service.internal;

import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.core.repository.OrganizationRepository;
import com.tw.aquaculture.process.repository.OwnOrganizationRepository;
import com.tw.aquaculture.process.service.OwnOrganizationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 组织实现类
 *
 * @author 陈荣
 * @date 2019/11/27 11:41
 */
@Service
public class OwnOrganizationServiceImpl implements OwnOrganizationService {

  @Autowired
  private OrganizationRepository organizationRepository;
  @Autowired
  private OwnOrganizationRepository ownOrganizationRepository;

  @Override
  public OrganizationEntity findDetailsById(String baseOrgId) {
    if (StringUtils.isBlank(baseOrgId)) {
      return null;
    }
    return organizationRepository.findDetailsById(baseOrgId);
  }

  @Override
  public OrganizationEntity findById(String baseOrgId) {
    if (StringUtils.isBlank(baseOrgId)) {
      return null;
    }
    return organizationRepository.findById(baseOrgId).orElse(null);
  }

  @Override
  public OrganizationEntity findOwnDetailsById(String id) {
    return ownOrganizationRepository.findDetailsById(id);
  }

  @Override
  public OrganizationEntity create(OrganizationEntity organizationEntity) {

    return organizationRepository.saveAndFlush(organizationEntity);
  }

}
