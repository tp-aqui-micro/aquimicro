package com.tw.aquaculture.process.service.internal;

import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.process.repository.PondEntityRepository;
import com.tw.aquaculture.process.service.PondEntityService;
import com.tw.aquaculture.process.service.TwBaseEntityService;
import com.tw.aquaculture.process.service.base.CodeGenerateService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * PondEntity业务模型的服务层接口实现
 * @author saturn
 */
@Service("PondEntityServiceImpl")
public class PondEntityServiceImpl implements PondEntityService {
  @Autowired
  private TwBaseEntityService twBaseEntityService;
  @Autowired
  private PondEntityRepository pondEntityRepository;
  @Autowired
  private CodeGenerateService codeGenerateService;
  @Transactional
  @Override
  public PondEntity create(PondEntity pondEntity) {
    return this.createForm(pondEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public PondEntity createForm(PondEntity pondEntity) {
   /* 
    * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
    * 1、如果当前模型对象不是主模型
    * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
    * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
    * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
    * 2、如果当前模型对象是主业务模型
    *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
    *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
    *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
    * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
    *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
    *   2.3.2、以及验证每个分组的OneToMany明细信息
    * */
    pondEntity.setCode(codeGenerateService.generateCode(CodeTypeEnum.POND_ENTITY));
    this.createValidation(pondEntity);
    
    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    if(pondEntity.getGroupLeader()==null || pondEntity.getGroupLeader().getId()==null){
      pondEntity.setGroupLeader(null);
    }
    if(pondEntity.getTgroup()==null || pondEntity.getTgroup().getId()==null){
      pondEntity.setTgroup(null);
    }
    this.pondEntityRepository.saveAndFlush(pondEntity);
    
    // 返回最终处理的结果，里面带有详细的关联信息
    return pondEntity;
  }
  /**
   * 在创建一个新的PondEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(PondEntity pondEntity) {
    Validate.notNull(pondEntity , "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(pondEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    pondEntity.setId(null);
    Validate.notBlank(pondEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(pondEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(pondEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(pondEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notBlank(pondEntity.getCode(), "添加信息时，编码不能为空！");
    Validate.notBlank(pondEntity.getName(), "添加信息时，塘口名称不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(pondEntity.getFormInstanceId() == null || pondEntity.getFormInstanceId().length() < 255 , "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondEntity.getCreateName() == null || pondEntity.getCreateName().length() < 255 , "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondEntity.getCreatePosition() == null || pondEntity.getCreatePosition().length() < 255 , "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondEntity.getModifyName() == null || pondEntity.getModifyName().length() < 255 , "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondEntity.getModifyPosition() == null || pondEntity.getModifyPosition().length() < 255 , "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondEntity.getCode() == null || pondEntity.getCode().length() < 255 , "编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondEntity.getName() == null || pondEntity.getName().length() < 255 , "塘口名称,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondEntity.getEbsCode() == null || pondEntity.getEbsCode().length() < 255 , "EBS编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    PondEntity currentPondEntity = this.findByFormInstanceId(pondEntity.getFormInstanceId());
    Validate.isTrue(currentPondEntity == null, "表单实例编号已存在,请检查");
    // 验证ManyToOne关联：基地绑定值的正确性
    TwBaseEntity currentBase = pondEntity.getBase();
    Validate.notNull(currentBase , "基地信息必须传入，请检查!!");
    String currentPkBase = currentBase.getId();
    Validate.notBlank(currentPkBase, "创建操作时，当前基地信息必须关联！");
    Validate.notNull(this.twBaseEntityService.findById(currentPkBase) , "基地关联信息未找到，请检查!!");
  }
  @Transactional
  @Override
  public PondEntity update(PondEntity pondEntity) {
    return this.updateForm(pondEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  }
  @Transactional
  @Override
  public PondEntity updateForm(PondEntity pondEntity) {
    /* 
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     * 
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    if(pondEntity.getGroupLeader()==null || pondEntity.getGroupLeader().getId()==null){
      pondEntity.setGroupLeader(null);
    }
    if(pondEntity.getTgroup()==null || pondEntity.getTgroup().getId()==null){
      pondEntity.setTgroup(null);
    }
    this.updateValidation(pondEntity);
    // ===================基本信息
    String currentId = pondEntity.getId();
    Optional<PondEntity> opCurrentPondEntity = this.pondEntityRepository.findById(currentId);
    PondEntity currentPondEntity = opCurrentPondEntity.orElse(null);
    Validate.notNull(currentPondEntity ,"未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentPondEntity.setFormInstanceId(pondEntity.getFormInstanceId());
    currentPondEntity.setCreateTime(pondEntity.getCreateTime());
    currentPondEntity.setCreateName(pondEntity.getCreateName());
    currentPondEntity.setCreatePosition(pondEntity.getCreatePosition());
    currentPondEntity.setModifyTime(pondEntity.getModifyTime());
    currentPondEntity.setModifyName(pondEntity.getModifyName());
    currentPondEntity.setModifyPosition(pondEntity.getModifyPosition());
    currentPondEntity.setCode(pondEntity.getCode());
    currentPondEntity.setName(pondEntity.getName());
    currentPondEntity.setEbsCode(pondEntity.getEbsCode());
    currentPondEntity.setState(pondEntity.getState());
    currentPondEntity.setPondSize(pondEntity.getPondSize());
    currentPondEntity.setEnableState(pondEntity.getEnableState());
    currentPondEntity.setBase(pondEntity.getBase());
    currentPondEntity.setTgroup(pondEntity.getTgroup());
    currentPondEntity.setGroupLeader(pondEntity.getGroupLeader());
    currentPondEntity.setWorker(pondEntity.getWorker());
    currentPondEntity.setTechnician(pondEntity.getTechnician());
    currentPondEntity.setWaterDeep(pondEntity.getWaterDeep());
    currentPondEntity.setStores(pondEntity.getStores());
    currentPondEntity.setModelTypeCode(pondEntity.getModelTypeCode());
    currentPondEntity.setPondType(pondEntity.getPondType());
    this.pondEntityRepository.saveAndFlush(currentPondEntity);
    return currentPondEntity;
  }
  /**
   * 在更新一个已有的PondEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(PondEntity pondEntity) {
    Validate.isTrue(!StringUtils.isBlank(pondEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
    
    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(pondEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(pondEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(pondEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(pondEntity.getCode(), "修改信息时，编码不能为空！");
    Validate.notBlank(pondEntity.getName(), "修改信息时，塘口名称不能为空！");
    
    // 重复性判断，基本属性，需要满足unique = true
    PondEntity currentForFormInstanceId = this.findByFormInstanceId(pondEntity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId() , pondEntity.getId()) , "表单实例编号已存在,请检查"); 
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(pondEntity.getFormInstanceId() == null || pondEntity.getFormInstanceId().length() < 255 , "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondEntity.getCreateName() == null || pondEntity.getCreateName().length() < 255 , "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondEntity.getCreatePosition() == null || pondEntity.getCreatePosition().length() < 255 , "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondEntity.getModifyName() == null || pondEntity.getModifyName().length() < 255 , "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondEntity.getModifyPosition() == null || pondEntity.getModifyPosition().length() < 255 , "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondEntity.getCode() == null || pondEntity.getCode().length() < 255 , "编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondEntity.getName() == null || pondEntity.getName().length() < 255 , "塘口名称,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondEntity.getEbsCode() == null || pondEntity.getEbsCode().length() < 255 , "EBS编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    
    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
    TwBaseEntity currentForBase = pondEntity.getBase();
    Validate.notNull(currentForBase , "修改信息时，基地必须传入，请检查!!");
  } 
  @Override
  public PondEntity findDetailsById(String id) {
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(id)) { 
      return null; 
    } 
    PondEntity current = this.pondEntityRepository.findDetailsById(id);
    if(current == null) {
      return null;
    } 
    return current;
  }
  @Override
  public PondEntity findById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    
    Optional<PondEntity> op = pondEntityRepository.findById(id);
    return op.orElse(null); 
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id , "进行删除时，必须给定主键信息!!");
    PondEntity current = this.findById(id);
    if(current != null) { 
      this.pondEntityRepository.delete(current);
    }
  }
  @Override
  public PondEntity findDetailsByFormInstanceId(String formInstanceId) {
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(formInstanceId)) { 
      return null; 
    } 
    PondEntity current = this.pondEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if(current == null) {
      return null;
    } 
    return current;
  }
  @Override
  public PondEntity findByFormInstanceId(String formInstanceId) {
    if(StringUtils.isBlank(formInstanceId)) { 
      return null;
    }
    return this.pondEntityRepository.findByFormInstanceId(formInstanceId);
  }

  @Override
  public Set<PondEntity> findDetailsByBase(String baseId) {
    if(StringUtils.isBlank(baseId)){
      return Collections.emptySet();
    }
    return pondEntityRepository.findDetailsByBase(baseId);
  }

  @Override
  public Set<PondEntity> findByTwGroup(String twGroupId) {
    if (StringUtils.isBlank(twGroupId)) {
       return Collections.emptySet();
    }
    return pondEntityRepository.findByTwGroup(twGroupId);
  }

  @Override
  public List<PondEntity> findPondsByBaseId(String baseId, int enableState) {
    if(StringUtils.isBlank(baseId)){
      return Collections.emptyList();
    }
    if(enableState == -1){
      return this.pondEntityRepository.findByBaseId(baseId);
    }
    return this.pondEntityRepository.findPondsByBaseIdAndEnableState(baseId, enableState);
  }

  @Override
  public List<PondEntity> findByPondType(String pondTypeId) {
    if(StringUtils.isBlank(pondTypeId)){
      return null;
    }
    return pondEntityRepository.findByPondType(pondTypeId);
  }
} 
