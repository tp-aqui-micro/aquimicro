package com.tw.aquaculture.process.service.internal;

import com.bizunited.platform.kuiper.starter.service.KuiperToolkitService;
import com.tw.aquaculture.common.entity.base.PondTypeEntity;
import com.tw.aquaculture.process.repository.PondTypeEntityRepository;
import com.tw.aquaculture.process.service.PondTypeEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * PondTypeEntity业务模型的服务层接口实现
 * @author saturn
 */
@Service("PondTypeEntityServiceImpl")
public class PondTypeEntityServiceImpl implements PondTypeEntityService {
  @Autowired
  private PondTypeEntityService pondTypeEntityService;
  @Autowired
  private PondTypeEntityRepository pondTypeEntityRepository;
  /** 
   * Kuiper表单引擎用于减少编码工作量的工具包 
   */ 
  @Autowired
  private KuiperToolkitService kuiperToolkitService;
  @Transactional
  @Override
  public PondTypeEntity create(PondTypeEntity pondTypeEntity) {
    PondTypeEntity current = this.createForm(pondTypeEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  } 
  @Transactional
  @Override
  public PondTypeEntity createForm(PondTypeEntity pondTypeEntity) { 
   /* 
    * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
    * 1、如果当前模型对象不是主模型
    * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
    * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
    * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
    * 2、如果当前模型对象是主业务模型
    *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
    *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
    *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
    * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
    *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
    *   2.3.2、以及验证每个分组的OneToMany明细信息
    * */
    if(pondTypeEntity.getParent()==null || StringUtils.isBlank(pondTypeEntity.getParent().getId())){
      pondTypeEntity.setParent(null);
    }
    this.createValidation(pondTypeEntity);
    
    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    
    this.pondTypeEntityRepository.saveAndFlush(pondTypeEntity);
    
    // 返回最终处理的结果，里面带有详细的关联信息
    return pondTypeEntity;
  }
  /**
   * 在创建一个新的PondTypeEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(PondTypeEntity pondTypeEntity) { 
    Validate.notNull(pondTypeEntity , "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(pondTypeEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    pondTypeEntity.setId(null);
    Validate.notBlank(pondTypeEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(pondTypeEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(pondTypeEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(pondTypeEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(pondTypeEntity.getFormInstanceId() == null || pondTypeEntity.getFormInstanceId().length() < 255 , "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondTypeEntity.getCreateName() == null || pondTypeEntity.getCreateName().length() < 255 , "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondTypeEntity.getCreatePosition() == null || pondTypeEntity.getCreatePosition().length() < 255 , "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondTypeEntity.getModifyName() == null || pondTypeEntity.getModifyName().length() < 255 , "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondTypeEntity.getModifyPosition() == null || pondTypeEntity.getModifyPosition().length() < 255 , "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondTypeEntity.getName() == null || pondTypeEntity.getName().length() < 255 , "名称,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondTypeEntity.getTypeForm() == null || pondTypeEntity.getTypeForm().length() < 255 , "分类类型,在进行添加时填入值超过了限定长度(255)，请检查!");
    PondTypeEntity currentPondTypeEntity = this.findByFormInstanceId(pondTypeEntity.getFormInstanceId());
    Validate.isTrue(currentPondTypeEntity == null, "表单实例编号已存在,请检查");
  }
  @Transactional
  @Override
  public PondTypeEntity update(PondTypeEntity pondTypeEntity) { 
    PondTypeEntity current = this.updateForm(pondTypeEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  } 
  @Transactional
  @Override
  public PondTypeEntity updateForm(PondTypeEntity pondTypeEntity) { 
    /* 
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     * 
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    
    this.updateValidation(pondTypeEntity);
    if(pondTypeEntity.getParent()==null || StringUtils.isBlank(pondTypeEntity.getParent().getId())){
      pondTypeEntity.setParent(null);
    }
    // ===================基本信息
    String currentId = pondTypeEntity.getId();
    Optional<PondTypeEntity> op_currentPondTypeEntity = this.pondTypeEntityRepository.findById(currentId);
    PondTypeEntity currentPondTypeEntity = op_currentPondTypeEntity.orElse(null);
    currentPondTypeEntity = Validate.notNull(currentPondTypeEntity ,"未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentPondTypeEntity.setFormInstanceId(pondTypeEntity.getFormInstanceId());
    currentPondTypeEntity.setCreateTime(pondTypeEntity.getCreateTime());
    currentPondTypeEntity.setCreateName(pondTypeEntity.getCreateName());
    currentPondTypeEntity.setCreatePosition(pondTypeEntity.getCreatePosition());
    currentPondTypeEntity.setModifyTime(pondTypeEntity.getModifyTime());
    currentPondTypeEntity.setModifyName(pondTypeEntity.getModifyName());
    currentPondTypeEntity.setModifyPosition(pondTypeEntity.getModifyPosition());
    currentPondTypeEntity.setName(pondTypeEntity.getName());
    currentPondTypeEntity.setTypeForm(pondTypeEntity.getTypeForm());
    currentPondTypeEntity.setEnableState(pondTypeEntity.getEnableState());
    currentPondTypeEntity.setParent(pondTypeEntity.getParent());
    
    this.pondTypeEntityRepository.saveAndFlush(currentPondTypeEntity);
    return currentPondTypeEntity;
  }
  /**
   * 在更新一个已有的PondTypeEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(PondTypeEntity pondTypeEntity) { 
    Validate.isTrue(!StringUtils.isBlank(pondTypeEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
    
    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(pondTypeEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(pondTypeEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(pondTypeEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(pondTypeEntity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    
    // 重复性判断，基本属性，需要满足unique = true
    PondTypeEntity currentForFormInstanceId = this.findByFormInstanceId(pondTypeEntity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId() , pondTypeEntity.getId()) , "表单实例编号已存在,请检查"); 
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(pondTypeEntity.getFormInstanceId() == null || pondTypeEntity.getFormInstanceId().length() < 255 , "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondTypeEntity.getCreateName() == null || pondTypeEntity.getCreateName().length() < 255 , "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondTypeEntity.getCreatePosition() == null || pondTypeEntity.getCreatePosition().length() < 255 , "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondTypeEntity.getModifyName() == null || pondTypeEntity.getModifyName().length() < 255 , "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondTypeEntity.getModifyPosition() == null || pondTypeEntity.getModifyPosition().length() < 255 , "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondTypeEntity.getName() == null || pondTypeEntity.getName().length() < 255 , "名称,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pondTypeEntity.getTypeForm() == null || pondTypeEntity.getTypeForm().length() < 255 , "分类类型,在进行修改时填入值超过了限定长度(255)，请检查!");
  } 
  @Override
  public PondTypeEntity findDetailsById(String id) { 
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(id)) { 
      return null; 
    } 
    PondTypeEntity current = this.pondTypeEntityRepository.findDetailsById(id);
    if(current == null) {
      return null;
    } 
    String currentPkValue = current.getId();
    return current;
  }
  @Override
  public PondTypeEntity findById(String id) { 
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    
    Optional<PondTypeEntity> op = pondTypeEntityRepository.findById(id);
    return op.orElse(null); 
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id , "进行删除时，必须给定主键信息!!");
    PondTypeEntity current = this.findById(id);
    if(current != null) { 
      this.pondTypeEntityRepository.delete(current);
    }
  }
  @Override
  public PondTypeEntity findDetailsByFormInstanceId(String formInstanceId) { 
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(formInstanceId)) { 
      return null; 
    } 
    PondTypeEntity current = this.pondTypeEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if(current == null) {
      return null;
    } 
    String currentPkValue = current.getId();
    return current;
  }
  @Override
  public PondTypeEntity findByFormInstanceId(String formInstanceId) { 
    if(StringUtils.isBlank(formInstanceId)) { 
      return null;
    }
    return this.pondTypeEntityRepository.findByFormInstanceId(formInstanceId);
  } 
} 
