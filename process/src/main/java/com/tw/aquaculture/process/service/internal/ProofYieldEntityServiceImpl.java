package com.tw.aquaculture.process.service.internal;

import com.bizunited.platform.kuiper.starter.service.KuiperToolkitService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.FishUpRateEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.PondTypeEntity;
import com.tw.aquaculture.common.entity.process.*;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.common.utils.MathUtil;
import com.tw.aquaculture.common.utils.UnitUtil;
import com.tw.aquaculture.common.vo.reportslist.ExistAmountReportVo;
import com.tw.aquaculture.common.vo.reportslist.ExitCountVo;
import com.tw.aquaculture.common.vo.reportslist.ReportParamVo;
import com.tw.aquaculture.process.repository.ProofYieldEntityRepository;
import com.tw.aquaculture.process.service.*;
import com.tw.aquaculture.process.service.base.CodeGenerateService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * ProofYieldEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("ProofYieldEntityServiceImpl")
public class ProofYieldEntityServiceImpl implements ProofYieldEntityService {
  @Autowired
  private PondEntityService pondEntityService;
  @Autowired
  private FishTypeEntityService fishTypeEntityService;
  @Autowired
  private ProofYieldItemEntityService proofYieldItemEntityService;
  @Autowired
  private WorkOrderEntityService workOrderEntityService;
  @Autowired
  private ProofYieldEntityRepository proofYieldEntityRepository;

  @Autowired
  private PushFryEntityService pushFryEntityService;
  @Autowired
  private DividePondEntityService dividePondEntityService;
  @Autowired
  private OutFishRegistEntityService outFishRegistEntityService;
  @Autowired
  private FishDamageEntityService fishDamageEntityService;
  @Autowired
  private CodeGenerateService codeGenerateService;
  @Autowired
  private TwBaseEntityService twBaseEntityService;
  @Autowired
  private PushFeedEntityService pushFeedEntityService;
  /**
   * Kuiper表单引擎用于减少编码工作量的工具包
   */
  @Autowired
  private KuiperToolkitService kuiperToolkitService;
  @Autowired
  private FishUpRateEntityService fishUpRateEntityService;

  @Transactional
  @Override
  public ProofYieldEntity create(ProofYieldEntity proofYieldEntity) {
    ProofYieldEntity current = this.createForm(proofYieldEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  }

  @Transactional
  @Override
  public ProofYieldEntity createForm(ProofYieldEntity proofYieldEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *   2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    proofYieldEntity.setCode(codeGenerateService.generateBusinessCode(CodeTypeEnum.PROOF_YIELD_ENTITY));
    this.createValidation(proofYieldEntity);

    Validate.notNull(proofYieldEntity.getCode(), "编码不能为空");
    List<WorkOrderEntity> workOrderEntities = workOrderEntityService.findByStateAndPond(proofYieldEntity.getPond().getId(), EnableStateEnum.DISABLE.getState());
    if (!CollectionUtils.isEmpty(workOrderEntities)) {
      proofYieldEntity.setWorkOrder(workOrderEntities.get(workOrderEntities.size() - 1));
      List<ProofYieldEntity> proofYieldEntities = proofYieldEntityRepository.findByWorkOrderDesc(proofYieldEntity.getWorkOrder().getId());
      if (!CollectionUtils.isEmpty(proofYieldEntities)) {
        proofYieldEntity.setLastYield(proofYieldEntities.get(0));
      } else {
        proofYieldEntity.setLastYield(null);
      }
    } else {
      throw new RuntimeException("请先对该塘口注水");
    }
    if (!DateUtils.between(proofYieldEntity.getProofTime(), proofYieldEntity.getWorkOrder().getCreateTime(), new Date())) {
      throw new RuntimeException("填写时间不在养殖周期内，请重新填写");
    }
    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    computeProofYield(proofYieldEntity);
    this.proofYieldEntityRepository.saveAndFlush(proofYieldEntity);
    // 处理明细信息：打样估产明细表
    Set<ProofYieldItemEntity> proofYieldItemEntityItems = proofYieldEntity.getProofYieldItems();
    if (proofYieldItemEntityItems != null) {
      for (ProofYieldItemEntity proofYieldItemEntityItem : proofYieldItemEntityItems) {
        proofYieldItemEntityItem.setProofYield(proofYieldEntity);
        this.proofYieldItemEntityService.create(proofYieldItemEntityItem);
      }
    }

    this.proofYieldEntityRepository.flush();
    // 返回最终处理的结果，里面带有详细的关联信息
    return proofYieldEntity;
  }

  /**
   * 计算打样本次打样测产相关数据
   *
   * @param proofYieldEntity
   */
  private void computeProofYield(ProofYieldEntity proofYieldEntity) {
    if (proofYieldEntity == null) {
      return;
    }
    if (proofYieldEntity.getPond() == null || proofYieldEntity.getFishType() == null) {
      throw new RuntimeException("塘口和鱼种不能为空");
    }
    if (proofYieldEntity.getWorkOrder() == null) {
      throw new RuntimeException("没有查到本期有效工单，请确认是否注水");
    }
    String pondId = proofYieldEntity.getPond().getId();
    String workOrderId = proofYieldEntity.getWorkOrder().getId();
    Date start = proofYieldEntity.getWorkOrder().getCreateTime();
    Date end = new Date();
    List<PushFryEntity> pushFryEntities = pushFryEntityService.findByPondAndWorkOrderIdAndFishType(pondId, workOrderId, proofYieldEntity.getFishType().getId());
    List<DividePondEntity> inPonds = dividePondEntityService.findByInPondAndTimesAndFishType(pondId, start, end, proofYieldEntity.getFishType().getId());
    List<DividePondEntity> outPonds = dividePondEntityService.findByOutPondAndWorkOrderIdAndFishType(pondId, workOrderId, proofYieldEntity.getFishType().getId());
    List<OutFishRegistEntity> outFishRegistEntities = outFishRegistEntityService.findByPondAndWorkOrderId(pondId, workOrderId);
    List<FishDamageEntity> fishDamageEntities = fishDamageEntityService.findByPondAndWorkOrderIdAndFishType(pondId, workOrderId, proofYieldEntity.getFishType().getId());
    //本期打样重量
    double proofWeight = 0;
    //本期打样数量
    double proofCount = 0;
    //本期投入数量
    double inCount;
    //本期投苗数量
    double pushFryCount = 0;
    //本期转入数量
    double inPondCount = 0;
    //本期转出数量
    double outCount;
    //本期出鱼数量
    double outFishCount = 0;
    //本期转分塘转出数量
    double outPondCount = 0;
    //本期损鱼数量
    double damageCount = 0;
    //期初数量
    double lastExistCount = 0;
    if (proofYieldEntity != null && !CollectionUtils.isEmpty(proofYieldEntity.getProofYieldItems())) {
      for (ProofYieldItemEntity item : proofYieldEntity.getProofYieldItems()) {
        proofCount += item.getCount();
        proofWeight += item.getWeight();
      }
    }
    if (proofCount != 0 && proofWeight != 0) {
      proofYieldEntity.setSpecs(round(proofWeight / proofCount, 2));
    }
    if (!CollectionUtils.isEmpty(pushFryEntities)) {
      for (PushFryEntity e : pushFryEntities) {
        pushFryCount += e.getCount();
      }
    }
    if (!CollectionUtils.isEmpty(inPonds)) {
      for (DividePondEntity e : inPonds) {
        inPondCount += computeDivideCount(e.getDividePondItems());
      }
    }
    inCount = pushFryCount + inPondCount;
    proofYieldEntity.setThePushCount(round(inCount, 2));
    if (!CollectionUtils.isEmpty(outPonds)) {
      for (DividePondEntity e : outPonds) {
        outPondCount += computeDivideCount(e.getDividePondItems());
      }
    }
    if (!CollectionUtils.isEmpty(outFishRegistEntities)) {
      for (OutFishRegistEntity e : outFishRegistEntities) {
        outFishCount += computeOutFishRegist(e.getOutFishInfos(), proofYieldEntity.getFishType().getId());
      }
    }
    outCount = outPondCount + outFishCount;
    proofYieldEntity.setTheOutCount(round(outCount, 2));
    if (!CollectionUtils.isEmpty(fishDamageEntities)) {
      for (FishDamageEntity e : fishDamageEntities) {
        damageCount += e.getCount();
      }
    }
    proofYieldEntity.setTheDamageCount(round(damageCount, 2));
    if (proofYieldEntity.getLastYield() != null && proofYieldEntity.getLastYield().getId() != null) {
      lastExistCount = proofYieldEntity.getLastYield().getExistCount();
    }
    proofYieldEntity.setExistCount(round(lastExistCount - damageCount - outCount + inCount, 2));
    proofYieldEntity.setExistWeight(round(proofYieldEntity.getExistCount() * proofYieldEntity.getSpecs(), 2));
  }

  /**
   * 按尾数取约分
   *
   * @param num
   * @param n
   * @return
   */
  private double round(Double num, int n) {
    if (num == null) {
      return 0;
    }
    double divider = 10;
    for (int i = 0; i < n; i++) {
      divider *= 10;
    }
    return Math.round(num * divider) / divider;
  }

  /**
   * 计算出鱼数量
   *
   * @param outFishInfos
   * @return
   */
  private double computeOutFishRegist(Set<OutFishInfoEntity> outFishInfos) {
    double count = 0;
    if (CollectionUtils.isEmpty(outFishInfos)) {
      return count;
    }
    for (OutFishInfoEntity info : outFishInfos) {
      count += info.getTotalWeight() / info.getSpecs();
    }
    return count;
  }

  /**
   * 计算分塘出/入鱼量
   *
   * @param dividePondItems
   * @return
   */
  private double computeDivideCount(Set<DividePondItemEntity> dividePondItems) {
    double count = 0;
    if (CollectionUtils.isEmpty(dividePondItems)) {
      return count;
    }
    for (DividePondItemEntity item : dividePondItems) {
      count += item.getCount();
    }
    return count;
  }


  /**
   * 在创建一个新的ProofYieldEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(ProofYieldEntity proofYieldEntity) {
    Validate.notNull(proofYieldEntity, "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(proofYieldEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    proofYieldEntity.setId(null);
    Validate.notBlank(proofYieldEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(proofYieldEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(proofYieldEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(proofYieldEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notBlank(proofYieldEntity.getCode(), "添加信息时，编码不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(proofYieldEntity.getFormInstanceId() == null || proofYieldEntity.getFormInstanceId().length() < 255, "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(proofYieldEntity.getCreateName() == null || proofYieldEntity.getCreateName().length() < 255, "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(proofYieldEntity.getCreatePosition() == null || proofYieldEntity.getCreatePosition().length() < 255, "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(proofYieldEntity.getModifyName() == null || proofYieldEntity.getModifyName().length() < 255, "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(proofYieldEntity.getModifyPosition() == null || proofYieldEntity.getModifyPosition().length() < 255, "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(proofYieldEntity.getCode() == null || proofYieldEntity.getCode().length() < 255, "编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(proofYieldEntity.getSupervisor() == null || proofYieldEntity.getSupervisor().length() < 255, "监磅人,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(proofYieldEntity.getNote() == null || proofYieldEntity.getNote().length() < 1000, "备注,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(proofYieldEntity.getPicture() == null || proofYieldEntity.getPicture().length() < 10000, "图片,在进行添加时填入值超过了限定长度(255)，请检查!");
    ProofYieldEntity currentProofYieldEntity = this.findByFormInstanceId(proofYieldEntity.getFormInstanceId());
    Validate.isTrue(currentProofYieldEntity == null, "表单实例编号已存在,请检查");
    // 验证ManyToOne关联：塘口绑定值的正确性
    PondEntity currentPond = proofYieldEntity.getPond();
    Validate.notNull(currentPond, "塘口信息必须传入，请检查!!");
    String currentPkPond = currentPond.getId();
    Validate.notBlank(currentPkPond, "创建操作时，当前塘口信息必须关联！");
    Validate.notNull(this.pondEntityService.findById(currentPkPond), "塘口关联信息未找到，请检查!!");
    // 验证ManyToOne关联：鱼种绑定值的正确性
    FishTypeEntity currentFishType = proofYieldEntity.getFishType();
    Validate.notNull(currentFishType, "鱼种信息必须传入，请检查!!");
    String currentPkFishType = currentFishType.getId();
    Validate.notBlank(currentPkFishType, "创建操作时，当前鱼种信息必须关联！");
    Validate.notNull(this.fishTypeEntityService.findById(currentPkFishType), "鱼种关联信息未找到，请检查!!");
  }

  @Transactional
  @Override
  public ProofYieldEntity update(ProofYieldEntity proofYieldEntity) {
    ProofYieldEntity current = this.updateForm(proofYieldEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  }

  @Transactional
  @Override
  public ProofYieldEntity updateForm(ProofYieldEntity proofYieldEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     *
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */

    this.updateValidation(proofYieldEntity);
    // ===================基本信息
    String currentId = proofYieldEntity.getId();
    Optional<ProofYieldEntity> op_currentProofYieldEntity = this.proofYieldEntityRepository.findById(currentId);
    ProofYieldEntity currentProofYieldEntity = op_currentProofYieldEntity.orElse(null);
    currentProofYieldEntity = Validate.notNull(currentProofYieldEntity, "未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentProofYieldEntity.setFormInstanceId(proofYieldEntity.getFormInstanceId());
    currentProofYieldEntity.setCreateTime(proofYieldEntity.getCreateTime());
    currentProofYieldEntity.setCreateName(proofYieldEntity.getCreateName());
    currentProofYieldEntity.setCreatePosition(proofYieldEntity.getCreatePosition());
    currentProofYieldEntity.setModifyTime(proofYieldEntity.getModifyTime());
    currentProofYieldEntity.setModifyName(proofYieldEntity.getModifyName());
    currentProofYieldEntity.setModifyPosition(proofYieldEntity.getModifyPosition());
    currentProofYieldEntity.setCode(proofYieldEntity.getCode());
    currentProofYieldEntity.setProofTime(proofYieldEntity.getProofTime());
    currentProofYieldEntity.setSupervisor(proofYieldEntity.getSupervisor());
    currentProofYieldEntity.setDeviationRate(proofYieldEntity.getDeviationRate());
    currentProofYieldEntity.setSpecs(proofYieldEntity.getSpecs());
    currentProofYieldEntity.setExistCount(proofYieldEntity.getExistCount());
    currentProofYieldEntity.setExistWeight(proofYieldEntity.getExistWeight());
    currentProofYieldEntity.setPredictExistWeight(proofYieldEntity.getPredictExistWeight());
    currentProofYieldEntity.setNote(proofYieldEntity.getNote());
    currentProofYieldEntity.setPicture(proofYieldEntity.getPicture());
    currentProofYieldEntity.setThePushCount(proofYieldEntity.getThePushCount());
    currentProofYieldEntity.setTheOutCount(proofYieldEntity.getTheOutCount());
    currentProofYieldEntity.setTheDamageCount(proofYieldEntity.getTheDamageCount());
    currentProofYieldEntity.setTheAnatomyCount(proofYieldEntity.getTheAnatomyCount());
    currentProofYieldEntity.setPond(proofYieldEntity.getPond());
    currentProofYieldEntity.setFishType(proofYieldEntity.getFishType());
    currentProofYieldEntity.setGroupLeader(proofYieldEntity.getGroupLeader());
    currentProofYieldEntity.setLastYield(proofYieldEntity.getLastYield());
    currentProofYieldEntity.setWorkOrder(proofYieldEntity.getWorkOrder());
    if (!DateUtils.between(currentProofYieldEntity.getProofTime(), currentProofYieldEntity.getWorkOrder().getCreateTime(), new Date())) {
      throw new RuntimeException("填写时间不在养殖周期内，请重新填写");
    }
    this.proofYieldEntityRepository.saveAndFlush(currentProofYieldEntity);
    // ==========准备处理OneToMany关系的详情proofYieldItems
    // 清理出那些需要删除的proofYieldItems信息
    Set<ProofYieldItemEntity> proofYieldItemEntitys = null;
    if (proofYieldEntity.getProofYieldItems() != null) {
      proofYieldItemEntitys = Sets.newLinkedHashSet(proofYieldEntity.getProofYieldItems());
    } else {
      proofYieldItemEntitys = Sets.newLinkedHashSet();
    }
    Set<ProofYieldItemEntity> dbProofYieldItemEntitys = this.proofYieldItemEntityService.findDetailsByProofYield(proofYieldEntity.getId());
    if (dbProofYieldItemEntitys == null) {
      dbProofYieldItemEntitys = Sets.newLinkedHashSet();
    }
    // 求得的差集既是需要删除的数据 
    Set<String> mustDeleteProofYieldItemsPks = kuiperToolkitService.collectionDiffent(dbProofYieldItemEntitys, proofYieldItemEntitys, ProofYieldItemEntity::getId);
    if (mustDeleteProofYieldItemsPks != null && !mustDeleteProofYieldItemsPks.isEmpty()) {
      for (String mustDeleteProofYieldItemsPk : mustDeleteProofYieldItemsPks) {
        this.proofYieldItemEntityService.deleteById(mustDeleteProofYieldItemsPk);
      }
    }
    // 对传来的记录进行添加或者修改操作 
    for (ProofYieldItemEntity proofYieldItemEntityItem : proofYieldItemEntitys) {
      proofYieldItemEntityItem.setProofYield(proofYieldEntity);
      if (StringUtils.isBlank(proofYieldItemEntityItem.getId())) {
        proofYieldItemEntityService.create(proofYieldItemEntityItem);
      } else {
        proofYieldItemEntityService.update(proofYieldItemEntityItem);
      }
    }

    return currentProofYieldEntity;
  }

  /**
   * 在更新一个已有的ProofYieldEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(ProofYieldEntity proofYieldEntity) {
    Validate.isTrue(!StringUtils.isBlank(proofYieldEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");

    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(proofYieldEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(proofYieldEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(proofYieldEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(proofYieldEntity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    Validate.notBlank(proofYieldEntity.getCode(), "修改信息时，编码不能为空！");

    // 重复性判断，基本属性，需要满足unique = true
    ProofYieldEntity currentForFormInstanceId = this.findByFormInstanceId(proofYieldEntity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId(), proofYieldEntity.getId()), "表单实例编号已存在,请检查");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(proofYieldEntity.getFormInstanceId() == null || proofYieldEntity.getFormInstanceId().length() < 255, "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(proofYieldEntity.getCreateName() == null || proofYieldEntity.getCreateName().length() < 255, "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(proofYieldEntity.getCreatePosition() == null || proofYieldEntity.getCreatePosition().length() < 255, "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(proofYieldEntity.getModifyName() == null || proofYieldEntity.getModifyName().length() < 255, "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(proofYieldEntity.getModifyPosition() == null || proofYieldEntity.getModifyPosition().length() < 255, "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(proofYieldEntity.getCode() == null || proofYieldEntity.getCode().length() < 255, "编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(proofYieldEntity.getSupervisor() == null || proofYieldEntity.getSupervisor().length() < 255, "监磅人,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(proofYieldEntity.getNote() == null || proofYieldEntity.getNote().length() < 1000, "备注,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(proofYieldEntity.getPicture() == null || proofYieldEntity.getPicture().length() < 10000, "图片,在进行修改时填入值超过了限定长度(255)，请检查!");

    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
    PondEntity currentForPond = proofYieldEntity.getPond();
    Validate.notNull(currentForPond, "修改信息时，塘口必须传入，请检查!!");
    FishTypeEntity currentForFishType = proofYieldEntity.getFishType();
    Validate.notNull(currentForFishType, "修改信息时，鱼种必须传入，请检查!!");
  }

  @Override
  public ProofYieldEntity findDetailsById(String id) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ProofYieldEntity current = this.proofYieldEntityRepository.findDetailsById(id);
    if (current == null) {
      return null;
    }
    String currentPkValue = current.getId();
    // 2、======
    // 打样估产明细表 的明细信息查询
    Collection<ProofYieldItemEntity> oneToManyProofYieldItemsDetails = this.proofYieldItemEntityService.findDetailsByProofYield(currentPkValue);
    current.setProofYieldItems(Sets.newLinkedHashSet(oneToManyProofYieldItemsDetails));

    return current;
  }

  @Override
  public ProofYieldEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }

    Optional<ProofYieldEntity> op = proofYieldEntityRepository.findById(id);
    return op.orElse(null);
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    ProofYieldEntity current = this.findById(id);
    if (current != null) {
      this.proofYieldEntityRepository.delete(current);
    }
  }

  @Override
  public ProofYieldEntity findDetailsByFormInstanceId(String formInstanceId) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ProofYieldEntity current = this.proofYieldEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if (current == null) {
      return null;
    }
    String currentPkValue = current.getId();
    // 2、======
    // 打样估产明细表 的明细信息查询
    Collection<ProofYieldItemEntity> oneToManyProofYieldItemsDetails = this.proofYieldItemEntityService.findDetailsByProofYield(currentPkValue);
    current.setProofYieldItems(Sets.newLinkedHashSet(oneToManyProofYieldItemsDetails));

    return current;
  }

  @Override
  public ProofYieldEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    return this.proofYieldEntityRepository.findByFormInstanceId(formInstanceId);
  }

  @Override
  public ProofYieldEntity findLastProofYield(String pondId) {
    if (StringUtils.isBlank(pondId)) {
      return null;
    }
    List<WorkOrderEntity> workOrderEntities = this.workOrderEntityService.findByStateAndPond(pondId, EnableStateEnum.DISABLE.getState());
    if (CollectionUtils.isEmpty(workOrderEntities)) {
      return null;
    }
    List<ProofYieldEntity> proofYieldEntities = this.proofYieldEntityRepository.findByWorkOrderDesc(
            workOrderEntities.get(workOrderEntities.size() - 1).getId());
    if (CollectionUtils.isEmpty(proofYieldEntities)) {
      return null;
    }
    // TODO 这句什么意思？
    proofYieldEntities.get(0).getGroupLeader().getUserName();
    return proofYieldEntities.get(0);
  }

  @Override
  public double computeLastExitCount(String pondId) {

    List<WorkOrderEntity> workOrderEntities = workOrderEntityService.findByStateAndPond(pondId, EnableStateEnum.DISABLE.getState());
    Date endTime = new Date();
    Date startTime;
    if (CollectionUtils.isEmpty(workOrderEntities)) {
      throw new RuntimeException("没有查到本期有效工单，请确认是否晒唐");
    }
    startTime = workOrderEntities.get(workOrderEntities.size() - 1).getCreateTime();
    List<PushFryEntity> pushFryEntities = pushFryEntityService.findByPondAndTimes(pondId, startTime, endTime);
    List<DividePondEntity> inPonds = dividePondEntityService.findByInPondAndTimes(pondId, startTime, endTime);
    List<DividePondEntity> outPonds = dividePondEntityService.findByOutPondAndTimes(pondId, startTime, endTime);
    List<OutFishRegistEntity> outFishRegistEntities = outFishRegistEntityService.findByPondAndTimes(pondId, startTime, endTime);
    List<FishDamageEntity> fishDamageEntities = fishDamageEntityService.findByPondAndTimes(pondId, startTime, endTime);
    //本期投入数量
    double inCount;
    //本期投苗数量
    double pushFryCount = 0;
    //本期转入数量
    double inPondCount = 0;
    //本期转出数量
    double outCount;
    //本期出鱼数量
    double outFishCount = 0;
    //本期转分塘转出数量
    double outPondCount = 0;
    //本期损鱼数量
    double damageCount = 0;
    //本期存塘量
    double result = 0;
    if (!CollectionUtils.isEmpty(pushFryEntities)) {
      for (PushFryEntity e : pushFryEntities) {
        pushFryCount += e.getCount();
      }
    }
    if (!CollectionUtils.isEmpty(inPonds)) {
      for (DividePondEntity e : inPonds) {
        inPondCount += computeDivideCount(e.getDividePondItems());
      }
    }
    inCount = pushFryCount + inPondCount;
    if (!CollectionUtils.isEmpty(outPonds)) {
      for (DividePondEntity e : outPonds) {
        outPondCount += computeDivideCount(e.getDividePondItems());
      }
    }
    if (!CollectionUtils.isEmpty(outFishRegistEntities)) {
      for (OutFishRegistEntity e : outFishRegistEntities) {
        outFishCount += computeOutFishRegist(e.getOutFishInfos());
      }
    }
    outCount = outPondCount + outFishCount;
    if (!CollectionUtils.isEmpty(fishDamageEntities)) {
      for (FishDamageEntity e : fishDamageEntities) {
        damageCount += e.getCount();
      }
    }
    result = round(inCount - damageCount - outCount, 2);
    return result;
  }

  @Override
  public ProofYieldEntity findLastProofYieldByFishType(String pondId, String fishTypeId) {
    if (StringUtils.isBlank(pondId)) {
      return null;
    }
    List<WorkOrderEntity> workOrderEntities = this.workOrderEntityService.findByStateAndPond(pondId, EnableStateEnum.DISABLE.getState());
    if (CollectionUtils.isEmpty(workOrderEntities)) {
      return null;
    }
    List<ProofYieldEntity> proofYieldEntities = this.proofYieldEntityRepository.findByWorkOrderAndFishTyeIdDesc(
            workOrderEntities.get(workOrderEntities.size() - 1).getId(), fishTypeId);
    if (CollectionUtils.isEmpty(proofYieldEntities)) {
      return null;
    }
    return proofYieldEntities.get(0);
  }

  @Override
  public List<ProofYieldEntity> findDetailsByPondAndWorkOrder(String pondId, String workOrderId) {
    if (StringUtils.isBlank(pondId) || StringUtils.isBlank(workOrderId)) {
      return null;
    }
    return proofYieldEntityRepository.findDetailsByPondAndWorkOrder(pondId, workOrderId);
  }

  @Override
  public double findExitCountByPondAndFishType(String pondId, String fishTypeId) {
    if (StringUtils.isBlank(pondId) || StringUtils.isBlank(fishTypeId)) {
      return 0;
    }
    List<WorkOrderEntity> workOrderEntities = workOrderEntityService.findByStateAndPond(pondId, EnableStateEnum.DISABLE.getState());
    /*Date endTime = new Date();
    Date startTime;*/
    if (CollectionUtils.isEmpty(workOrderEntities)) {
      throw new RuntimeException("没有查到本期有效工单，请确认是否注水");
    }
    Date start = workOrderEntities.get(workOrderEntities.size() - 1).getCreateTime();
    Date end = new Date();
    String workOrderId = workOrderEntities.get(workOrderEntities.size() - 1).getId();
    List<PushFryEntity> pushFryEntities = pushFryEntityService.findByPondAndWorkOrderIdAndFishType(pondId, workOrderId, fishTypeId);
    List<DividePondEntity> inPonds = dividePondEntityService.findByInPondAndTimesAndFishType(pondId, start, end, fishTypeId);
    List<DividePondEntity> outPonds = dividePondEntityService.findByOutPondAndWorkOrderIdAndFishType(pondId, workOrderId, fishTypeId);
    List<OutFishRegistEntity> outFishRegistEntities = outFishRegistEntityService.findByPondAndWorkOrderId(pondId, workOrderId);
    List<FishDamageEntity> fishDamageEntities = fishDamageEntityService.findByPondAndWorkOrderIdAndFishType(pondId, workOrderId, fishTypeId);
    //本期投入数量
    double inCount;
    //本期投苗数量
    double pushFryCount = 0;
    //本期转入数量
    double inPondCount = 0;
    //本期转出数量
    double outCount;
    //本期出鱼数量
    double outFishCount = 0;
    //本期转分塘转出数量
    double outPondCount = 0;
    //本期损鱼数量
    double damageCount = 0;
    //本期存塘量
    double result = 0;
    if (!CollectionUtils.isEmpty(pushFryEntities)) {
      for (PushFryEntity e : pushFryEntities) {
        pushFryCount += e.getCount();
      }
    }
    if (!CollectionUtils.isEmpty(inPonds)) {
      for (DividePondEntity e : inPonds) {
        inPondCount += computeDivideCount(e.getDividePondItems());
      }
    }
    inCount = pushFryCount + inPondCount;
    if (!CollectionUtils.isEmpty(outPonds)) {
      for (DividePondEntity e : outPonds) {
        outPondCount += computeDivideCount(e.getDividePondItems());
      }
    }
    if (!CollectionUtils.isEmpty(outFishRegistEntities)) {
      for (OutFishRegistEntity e : outFishRegistEntities) {
        outFishCount += computeOutFishRegist(e.getOutFishInfos(), fishTypeId);
      }
    }
    outCount = outPondCount + outFishCount;
    if (!CollectionUtils.isEmpty(fishDamageEntities)) {
      for (FishDamageEntity e : fishDamageEntities) {
        damageCount += e.getCount();
      }
    }
    result = round(inCount - damageCount - outCount, 2);
    return result;
  }

  @Override
  public double computeLastExitCount(String pondId, Date startTime, Date endTime, String fishTypeId) {
    if (StringUtils.isBlank(pondId) || StringUtils.isBlank(fishTypeId)) {
      return 0.0;
    }
    List<PushFryEntity> pushFryEntities = pushFryEntityService.findByPondAndTimesAndFishType(pondId, startTime, endTime, fishTypeId);
    List<DividePondEntity> inPonds = dividePondEntityService.findByInPondAndTimesAndFishType(pondId, startTime, endTime, fishTypeId);
    List<DividePondEntity> outPonds = dividePondEntityService.findByOutPondAndTimesAndFishType(pondId, startTime, endTime, fishTypeId);
    List<OutFishRegistEntity> outFishRegistEntities = outFishRegistEntityService.findByPondAndTimes(pondId, startTime, endTime);
    List<FishDamageEntity> fishDamageEntities = fishDamageEntityService.findByPondAndTimesAndFishType(pondId, startTime, endTime, fishTypeId);
    //本期投入数量
    double inCount;
    //本期投苗数量
    double pushFryCount = 0;
    //本期转入数量
    double inPondCount = 0;
    //本期转出数量
    double outCount;
    //本期出鱼数量
    double outFishCount = 0;
    //本期转分塘转出数量
    double outPondCount = 0;
    //本期损鱼数量
    double damageCount = 0;
    //本期存塘量
    double result;
    if (!CollectionUtils.isEmpty(pushFryEntities)) {
      for (PushFryEntity e : pushFryEntities) {
        pushFryCount += e.getCount();
      }
    }
    if (!CollectionUtils.isEmpty(inPonds)) {
      for (DividePondEntity e : inPonds) {
        inPondCount += computeDivideCount(e.getDividePondItems());
      }
    }
    inCount = pushFryCount + inPondCount;
    if (!CollectionUtils.isEmpty(outPonds)) {
      for (DividePondEntity e : outPonds) {
        outPondCount += computeDivideCount(e.getDividePondItems());
      }
    }
    if (!CollectionUtils.isEmpty(outFishRegistEntities)) {
      for (OutFishRegistEntity e : outFishRegistEntities) {
        outFishCount += computeOutFishRegist(e.getOutFishInfos(), fishTypeId);
      }
    }
    outCount = outPondCount + outFishCount;
    if (!CollectionUtils.isEmpty(fishDamageEntities)) {
      for (FishDamageEntity e : fishDamageEntities) {
        damageCount += e.getCount();
      }
    }
    result = round(inCount - damageCount - outCount, 2);
    return result;
  }

  @Override
  public List<ExistAmountReportVo> existAmountReport(ReportParamVo reportParamVo) {
    Validate.notNull(reportParamVo, "vo信息不能为空！！");
    String baseId = reportParamVo.getBaseId();
    if (StringUtils.isBlank(baseId)) {
      return Lists.newArrayList();
    }
    String pondId = reportParamVo.getPondId();
    String fishTypeId = reportParamVo.getFishTypeId();
    Date startTime = reportParamVo.getStartTime();
    Date endTime = reportParamVo.getEndTime();
    /*Validate.notNull(startTime, "开始时间必填");
    Validate.notNull(endTime, "结束时间必填");*/
    if (startTime == null) {
      startTime = DateUtils.getMonthStart();
    }
    if (endTime == null) {
      endTime = new Date();
    }
    List<ExistAmountReportVo> result = Lists.newArrayList();
    this.existAmountReport(baseId, pondId, fishTypeId, startTime, endTime, result);
    if (!CollectionUtils.isEmpty(result)) {
      List<ExistAmountReportVo> pondTypeResult = existAmountReportWithPondType(result, DateUtils.shortOfDay(startTime, endTime));
      computeAreaByPondType(pondTypeResult);
      return pondTypeResult;
    }
    return result;
  }

  /**
   * 计算塘口分类下的塘口面积
   *
   * @param pondTypeResult
   */
  private void computeAreaByPondType(List<ExistAmountReportVo> pondTypeResult) {
    if (CollectionUtils.isEmpty(pondTypeResult)) {
      return;
    }
    for (ExistAmountReportVo existAmountReportVo : pondTypeResult) {
      if (StringUtils.isBlank(existAmountReportVo.getPondTypeId())) {
        continue;
      }
      List<PondEntity> pondEntities = pondEntityService.findByPondType(existAmountReportVo.getPondTypeId());
      if (CollectionUtils.isEmpty(pondEntities)) {
        continue;
      }
      existAmountReportVo.setPondCount(pondEntities.size());
      computeAreaByPondType(existAmountReportVo, pondEntities);
    }
  }

  /**
   * 计算塘口分类下的塘口面积
   *
   * @param existAmountReportVo
   * @param pondEntities
   */
  private void computeAreaByPondType(ExistAmountReportVo existAmountReportVo, List<PondEntity> pondEntities) {
    if (existAmountReportVo == null || CollectionUtils.isEmpty(pondEntities)) {
      return;
    }
    List<String> pondIds = Lists.newArrayList();
    List<String> pondNames = Lists.newArrayList();
    pondEntities.forEach(pond -> {
      existAmountReportVo.setTotalArea(existAmountReportVo.getTotalArea() + (pond.getPondSize() == null ? 0 : pond.getPondSize()));
      pondIds.add(pond.getId());
      pondNames.add(pond.getName());
    });
    existAmountReportVo.setPondIds(pondIds);
    existAmountReportVo.setPondNames(pondNames);
  }

  /**
   * 将塘口维度的存塘量报表转化为塘口类型的维度
   *
   * @param result
   * @param days
   */
  private List<ExistAmountReportVo> existAmountReportWithPondType(List<ExistAmountReportVo> result, int days) {
    if (CollectionUtils.isEmpty(result)) {
      return result;
    }
    List<ExistAmountReportVo> nResult = Lists.newArrayList();
    Set<String> pondTypeIdSet = Sets.newTreeSet();
    Map<String, Double> pushFryCount = Maps.newTreeMap();
    Map<String, Double> pushFryWeight = Maps.newTreeMap();
    Map<String, Double> count = Maps.newTreeMap();
    Map<String, Double> weight = Maps.newTreeMap();
    Map<String, Double> earlyCount = Maps.newTreeMap();
    Map<String, Double> earlyWeight = Maps.newTreeMap();
    Map<String, Double> inCount = Maps.newTreeMap();
    Map<String, Double> inWeight = Maps.newTreeMap();
    Map<String, Double> outCount = Maps.newTreeMap();
    Map<String, Double> outWeight = Maps.newTreeMap();
    Map<String, Double> saleCount = Maps.newTreeMap();
    Map<String, Double> saleWeight = Maps.newTreeMap();
    Map<String, Double> damageCount = Maps.newTreeMap();
    Map<String, Double> damageWeight = Maps.newTreeMap();
    Map<String, Double> feedWeight = Maps.newTreeMap();
    Map<String, String> pondId = Maps.newTreeMap();
    for (ExistAmountReportVo existAmountReportVo : result) {
      if (StringUtils.isBlank(existAmountReportVo.getPondTypeId())) {
        continue;
      }
      pondTypeIdSet.add(existAmountReportVo.getPondTypeId().concat(",").concat(existAmountReportVo.getFishTypeId()).concat(",").concat(existAmountReportVo.getBaseId()));
    }
    for (String pondTypeId : pondTypeIdSet) {
      pushFryCount.put(pondTypeId, 0.0);
      pushFryWeight.put(pondTypeId, 0.0);
      count.put(pondTypeId, 0.0);
      weight.put(pondTypeId, 0.0);
      earlyCount.put(pondTypeId, 0.0);
      earlyWeight.put(pondTypeId, 0.0);
      inCount.put(pondTypeId, 0.0);
      inWeight.put(pondTypeId, 0.0);
      outCount.put(pondTypeId, 0.0);
      outWeight.put(pondTypeId, 0.0);
      saleCount.put(pondTypeId, 0.0);
      saleWeight.put(pondTypeId, 0.0);
      damageCount.put(pondTypeId, 0.0);
      damageWeight.put(pondTypeId, 0.0);
      feedWeight.put(pondTypeId, 0.0);
    }
    for (ExistAmountReportVo existAmountReportVo : result) {
      if (StringUtils.isBlank(existAmountReportVo.getPondTypeId()) || StringUtils.isBlank(existAmountReportVo.getFishTypeId())) {
        continue;
      }
      String key = existAmountReportVo.getPondTypeId().concat(",").concat(existAmountReportVo.getFishTypeId()).concat(",").concat(existAmountReportVo.getBaseId());
      pushFryCount.put(key, pushFryCount.get(key) + existAmountReportVo.getPushFryCount());
      pushFryWeight.put(key, pushFryWeight.get(key) + existAmountReportVo.getPushFryWeight());
      count.put(key, count.get(key) + existAmountReportVo.getCount());
      weight.put(key, weight.get(key) + existAmountReportVo.getWeight());
      earlyCount.put(key, earlyCount.get(key) + existAmountReportVo.getEarlyCount());
      earlyWeight.put(key, earlyWeight.get(key) + existAmountReportVo.getEarlyWeight());
      inCount.put(key, inCount.get(key) + existAmountReportVo.getInCount());
      inWeight.put(key, inWeight.get(key) + existAmountReportVo.getInWeight());
      outCount.put(key, outCount.get(key) + existAmountReportVo.getOutCount());
      outWeight.put(key, outWeight.get(key) + existAmountReportVo.getOutWeight());
      saleCount.put(key, saleCount.get(key) + existAmountReportVo.getSaleCount());
      saleWeight.put(key, saleWeight.get(key) + existAmountReportVo.getSaleWeight());
      damageCount.put(key, damageCount.get(key) + existAmountReportVo.getDamageCount());
      damageWeight.put(key, damageWeight.get(key) + existAmountReportVo.getDamageWeight());
      feedWeight.put(key, feedWeight.get(key) + existAmountReportVo.getFeedWeight());
      pondId.put(existAmountReportVo.getPondTypeId(), existAmountReportVo.getPondId());
    }
    for (String pondTypeId : pondTypeIdSet) {
      String pondTId = pondTypeId.split(",")[0];
      String fishTypeId = pondTypeId.split(",")[1];
      ExistAmountReportVo existAmountReportVo = new ExistAmountReportVo();
      existAmountReportVo.setPondTypeId(pondTId);
      existAmountReportVo.setFishTypeId(fishTypeId);
      existAmountReportVo.setFeedWeight(feedWeight.get(pondTypeId));
      existAmountReportVo.setCount(count.get(pondTypeId));
      existAmountReportVo.setWeight(weight.get(pondTypeId));
      existAmountReportVo.setPushFryCount(pushFryCount.get(pondTypeId));
      existAmountReportVo.setPushFryWeight(pushFryWeight.get(pondTypeId));
      existAmountReportVo.setDamageCount(damageCount.get(pondTypeId));
      existAmountReportVo.setDamageWeight(damageWeight.get(pondTypeId));
      existAmountReportVo.setEarlyCount(earlyCount.get(pondTypeId));
      existAmountReportVo.setEarlyWeight(earlyWeight.get(pondTypeId));
      existAmountReportVo.setInCount(inCount.get(pondTypeId));
      existAmountReportVo.setInWeight(inWeight.get(pondTypeId));
      existAmountReportVo.setOutCount(outCount.get(pondTypeId));
      existAmountReportVo.setOutWeight(outWeight.get(pondTypeId));
      existAmountReportVo.setSaleCount(saleCount.get(pondTypeId));
      existAmountReportVo.setSaleWeight(saleWeight.get(pondTypeId));
      existAmountReportVo.setAveragePushFryRate(MathUtil.round(existAmountReportVo.getFeedWeight() / days, 4));
      existAmountReportVo.setDeadRate(MathUtil.round(existAmountReportVo.getDamageCount() / (existAmountReportVo.getPushFryCount() + existAmountReportVo.getInCount()), 4));
      if (StringUtils.isBlank(pondId.get(pondTId))) {
        continue;
      }
      PondEntity pondEntity = pondEntityService.findDetailsById(pondId.get(pondTId));
      if (pondEntity == null) {
        continue;
      }
      FishTypeEntity fishTypeEntity = fishTypeEntityService.findDetailsById(fishTypeId);
      PondTypeEntity pondTypeEntity = pondEntity.getPondType();
      if (pondTypeEntity == null || fishTypeEntity == null) {
        continue;
      }
      FishUpRateEntity fishUpRateEntity = fishUpRateEntityService.findByPondTypeAndFishType(pondTypeEntity.getId(), fishTypeEntity.getId());
      existAmountReportVo.setBaseId(pondEntity.getBase().getId());
      existAmountReportVo.setBaseName(pondEntity.getBase().getBaseOrg().getOrgName());
      existAmountReportVo.setCompanyId(pondEntity.getBase().getBaseOrg() == null ? null : pondEntity.getBase().getBaseOrg().getParent().getId());
      existAmountReportVo.setCompanyName(pondEntity.getBase().getBaseOrg() == null ? null : pondEntity.getBase().getBaseOrg().getParent().getOrgName());
      existAmountReportVo.setFishTypeName(fishTypeEntity.getName());
      existAmountReportVo.setPondTypeName(pondTypeEntity.getName());
      if (fishUpRateEntity != null && fishUpRateEntity.getRate() != null && fishUpRateEntity.getRate() > 0) {
        existAmountReportVo.setUpRate(existAmountReportVo.getFeedWeight() / fishUpRateEntity.getRate());
      }
      nResult.add(existAmountReportVo);
    }
    return nResult;
  }

  @Override
  public List<ProofYieldEntity> findByPondAndTimes(String pondId, Date startTime, Date endTime) {
    if (StringUtils.isBlank(pondId) || startTime == null || endTime == null) {
      return null;
    }
    return proofYieldEntityRepository.findByPondAndTimes(pondId, startTime, endTime);
  }

  @Override
  public List<ProofYieldEntity> findByPondAndEndTime(String pid, Date endTime) {
    if (StringUtils.isBlank(pid) || endTime == null) {
      return null;
    }
    return proofYieldEntityRepository.findByPondAndEndTime(pid, endTime);
  }

  @Override
  public List<ProofYieldEntity> findByPond(String pid) {
    if (StringUtils.isBlank(pid)) {
      return null;
    }
    return proofYieldEntityRepository.findByPond(pid);
  }

  @Override
  public Double computeReportExitCount(String pondId, Date endTime, String fishTypeId) {
    if (StringUtils.isBlank(pondId) || StringUtils.isBlank(fishTypeId) || endTime == null) {
      return 0.0;
    }
    List<WorkOrderEntity> workOrderEntities = workOrderEntityService.findByPondAndTime(pondId, endTime);

    if (CollectionUtils.isEmpty(workOrderEntities)) {
      return 0.0;
    }
    return computeReportExitCount(pondId, endTime, fishTypeId, workOrderEntities.get(workOrderEntities.size() - 1));
  }

  private Double computeReportExitCount(String pondId, Date endTime, String fishTypeId, WorkOrderEntity workOrderEntity) {
    if (workOrderEntity == null) {
      return 0.0;
    }
    Date start = workOrderEntity.getCreateTime();
    Date end = workOrderEntity.getFinishTime() == null ? endTime : workOrderEntity.getFinishTime();
    String workOrderId = workOrderEntity.getId();
    List<PushFryEntity> pushFryEntities = pushFryEntityService.findByPondAndWorkOrderIdAndFishType(pondId, workOrderId, fishTypeId);
    List<DividePondEntity> inPonds = dividePondEntityService.findByInPondAndTimesAndFishType(pondId, start, end, fishTypeId);
    List<DividePondEntity> outPonds = dividePondEntityService.findByOutPondAndWorkOrderIdAndFishType(pondId, workOrderId, fishTypeId);
    List<OutFishRegistEntity> outFishRegistEntities = outFishRegistEntityService.findByPondAndWorkOrderId(pondId, workOrderId);
    List<FishDamageEntity> fishDamageEntities = fishDamageEntityService.findByPondAndWorkOrderIdAndFishType(pondId, workOrderId, fishTypeId);
    //本期投入数量
    double inCount;
    //本期投苗数量
    double pushFryCount = 0;
    //本期转入数量
    double inPondCount = 0;
    //本期转出数量
    double outCount;
    //本期出鱼数量
    double outFishCount = 0;
    //本期转分塘转出数量
    double outPondCount = 0;
    //本期损鱼数量
    double damageCount = 0;
    //本期存塘量
    double result = 0;
    if (!CollectionUtils.isEmpty(pushFryEntities)) {
      for (PushFryEntity e : pushFryEntities) {
        pushFryCount += e.getCount() == null ? 0 : e.getCount();
      }
    }
    if (!CollectionUtils.isEmpty(inPonds)) {
      for (DividePondEntity e : inPonds) {
        inPondCount += computeDivideCount(e.getDividePondItems());
      }
    }
    inCount = pushFryCount + inPondCount;
    if (!CollectionUtils.isEmpty(outPonds)) {
      for (DividePondEntity e : outPonds) {
        outPondCount += computeDivideCount(e.getDividePondItems());
      }
    }
    if (!CollectionUtils.isEmpty(outFishRegistEntities)) {
      for (OutFishRegistEntity e : outFishRegistEntities) {
        outFishCount += computeOutFishRegist(e.getOutFishInfos(), fishTypeId);
      }
    }
    outCount = outPondCount + outFishCount;
    if (!CollectionUtils.isEmpty(fishDamageEntities)) {
      for (FishDamageEntity e : fishDamageEntities) {
        damageCount += e.getCount();
      }
    }
    result = round(inCount - damageCount - outCount, 2);
    return result;
  }

  /**
   * 组装存塘量报表
   *
   * @param baseId
   * @param pondId
   * @param fishTypeId
   * @param startTime
   * @param endTime
   * @param result
   */
  private void existAmountReport(String baseId, String pondId, String fishTypeId, Date startTime, Date endTime, List<ExistAmountReportVo> result) {
    List<String> pondIds = Lists.newArrayList();
    List<String> baseIds = Lists.newArrayList();

    List<PondEntity> ponds = Lists.newArrayList();
    if (StringUtils.isNotBlank(pondId)) {
      pondIds = Arrays.asList(pondId.split(","));
    } else if (StringUtils.isNotBlank(baseId)) {
      baseIds = Arrays.asList(baseId.split(","));
    }

    if (CollectionUtils.isEmpty(pondIds)) {
      for (String b : baseIds) {
        List<PondEntity> pondEntities = pondEntityService.findPondsByBaseId(b, EnableStateEnum.ALL.getState());
        if (!CollectionUtils.isEmpty(pondEntities)) {
          ponds.addAll(pondEntities);
        }
      }
    } else {
      buildPonds(ponds, pondIds);
    }

    for (PondEntity pond : ponds) {
      Set<String> fishTypeIds = Sets.newHashSet();
      if (StringUtils.isNotBlank(fishTypeId)) {
        String[] strs = fishTypeId.split(",");
        for (String str : strs) {
          fishTypeIds.add(str);
        }
      }
      List<ProofYieldEntity> proofYieldEntitys = this.findByPondAndTimes(pond.getId(), startTime, endTime);
      if (CollectionUtils.isEmpty(proofYieldEntitys)) {
        proofYieldEntitys = this.findByPondAndEndTime(pond.getId(), endTime);
      }
      if (CollectionUtils.isEmpty(proofYieldEntitys)) {
        proofYieldEntitys = this.findByPond(pond.getId());
      }
      if (CollectionUtils.isEmpty(proofYieldEntitys)) {
        proofYieldEntitys = Lists.newArrayList();
      }
      WorkOrderEntity workOrderEntity = null;
      List<WorkOrderEntity> workOrderEntities = workOrderEntityService.findByPondAndTimeAndState(pond.getId(), endTime, EnableStateEnum.DISABLE.getState());
      Date startTimeParam = startTime;
      Date endTimeParam = endTime;
      if (!CollectionUtils.isEmpty(workOrderEntities)) {
        workOrderEntity = workOrderEntities.get(workOrderEntities.size() - 1);
//        startTimeParam = workOrderEntity.getCreateTime();
//        endTimeParam = workOrderEntity.getFinishTime() == null ? endTime : workOrderEntity.getFinishTime();
      }
      existAmountReportByPondId(pond, fishTypeIds, startTimeParam, endTimeParam, result, proofYieldEntitys,
              fishTypeId, workOrderEntity);
    }

  }

  /**
   * 根据塘口查询存塘量
   *
   * @param pond
   * @param fishTypeIds
   * @param startTimeParam
   * @param endTimeParam
   * @param result
   * @param proofYieldEntitys
   * @param fishT
   * @param workOrderEntity
   */
  private void existAmountReportByPondId(PondEntity pond, Set<String> fishTypeIds, Date startTimeParam, Date endTimeParam, List<ExistAmountReportVo> result,
                                         List<ProofYieldEntity> proofYieldEntitys, String fishT, WorkOrderEntity workOrderEntity) {
    Map<String, Double> speces = Maps.newHashMap();
    if (workOrderEntity == null) {
      return;
    }
    List<PushFryEntity> pushFryEntities = pushFryEntityService.findSimpleByPondAndTimes(pond.getId(), workOrderEntity.getCreateTime(), workOrderEntity.getFinishTime() == null ? endTimeParam : workOrderEntity.getFinishTime());
    if (StringUtils.isBlank(fishT)) {
      fishTypeIds.clear();
      if (!CollectionUtils.isEmpty(pushFryEntities)) {
        for (PushFryEntity pushFryEntity : pushFryEntities) {
          if (pushFryEntity.getFishType() != null && StringUtils.isNotBlank(pushFryEntity.getFishType().getId())) {
            fishTypeIds.add(pushFryEntity.getFishType().getId());
          }
          speces.put(pushFryEntity.getFishType().getId(), (pushFryEntity.getSummation() != null && pushFryEntity.getCount() != null && pushFryEntity.getCount() != 0)
                  ? (pushFryEntity.getSummation() / pushFryEntity.getCount()) : 0);
        }
      }
    }
    for (String fishTypeId : fishTypeIds) {
      if (workOrderEntity != null) {
        ExistAmountReportVo existAmountReportVo = this.existAmountReportWithFishType(pond, fishTypeId, speces,
                startTimeParam, endTimeParam, proofYieldEntitys, workOrderEntity);
        double totalCount = existAmountReportVo.getInCount() + existAmountReportVo.getPushFryCount() + existAmountReportVo.getEarlyCount();
        if (totalCount != 0) {
          existAmountReportVo.setDeadRate(MathUtil.round(existAmountReportVo.getOutCount() / totalCount, 4));
        }
        if (existAmountReportVo != null) {
          result.add(existAmountReportVo);
        }
      }
    }
  }

  /**
   * 查询私聊投喂量
   *
   * @param pond
   * @param startTimeParam
   * @param endTimeParam
   * @param existAmountReportVo
   */
  private void pushFeedReport(PondEntity pond, Date startTimeParam, Date endTimeParam, ExistAmountReportVo existAmountReportVo) {
    if (pond == null || StringUtils.isBlank(pond.getId()) || startTimeParam == null || endTimeParam == null) {
      return;
    }
    List<PushFeedEntity> pushFeedEntities = pushFeedEntityService.findByPondAndTimes(pond.getId(), startTimeParam, endTimeParam);
    if (CollectionUtils.isEmpty(pushFeedEntities)) {
      return;
    }
    double peedAmount = 0;
    for (PushFeedEntity pushFeedEntity : pushFeedEntities) {
      peedAmount += pushFeedEntity.getPushWeight() == null ? 0 : pushFeedEntity.getPushWeight();
    }
    existAmountReportVo.setFeedWeight(peedAmount);
    existAmountReportVo.setFeedUnit(UnitUtil.UNIT_JIN);
  }

  /**
   * 按鱼种查询村塘量
   *
   * @param pond
   * @param fishTypeId
   * @param speces
   * @param startTimeParam
   * @param endTimeParam
   * @param proofYieldEntitys
   * @param workOrderEntity
   * @return
   */
  private ExistAmountReportVo existAmountReportWithFishType(PondEntity pond, String fishTypeId, Map<String, Double> speces,
                                                            Date startTimeParam, Date endTimeParam, List<ProofYieldEntity> proofYieldEntitys, WorkOrderEntity workOrderEntity) {
    ExistAmountReportVo existAmountReportVo = new ExistAmountReportVo();
    FishTypeEntity fishTypeEntity = fishTypeEntityService.findById(fishTypeId);
    if (fishTypeEntity == null) {
      return null;
    }
    int days;
    Date start = workOrderEntity.getCreateTime();
    Date end = workOrderEntity.getFinishTime() == null ? endTimeParam : workOrderEntity.getFinishTime();
    if (DateUtils.bigThan(startTimeParam, workOrderEntity.getCreateTime())) {
      this.earlyExsitAmountReport(pond, start, startTimeParam, existAmountReportVo, fishTypeId);
    }
    this.currentExsitAmoutReport(pond, start, end, existAmountReportVo, fishTypeId);
    this.pushFeedReport(pond, start, end, existAmountReportVo);
    days = DateUtils.shortOfDay(start, end);
    existAmountReportVo.setAveragePushFryRate(MathUtil.round(existAmountReportVo.getFeedWeight() / days, 4));
    existAmountReportVo.setBaseId(pond.getBase().getId());
    existAmountReportVo.setBaseName(pond.getBase().getBaseOrg().getOrgName());
    existAmountReportVo.setPondId(pond.getId());
    existAmountReportVo.setPondName(pond.getName());
    existAmountReportVo.setPondTypeId(pond.getPondType() == null ? null : pond.getPondType().getId());
    existAmountReportVo.setPondTypeName(pond.getPondType() == null ? null : pond.getPondType().getName());
    existAmountReportVo.setCompanyId(pond.getBase().getBaseOrg().getParent() == null
            ? null : pond.getBase().getBaseOrg().getParent().getId());
    existAmountReportVo.setCompanyName(pond.getBase().getBaseOrg().getParent() == null
            ? null : pond.getBase().getBaseOrg().getParent().getOrgName());
    existAmountReportVo.setFishTypeName(fishTypeEntity.getName());
    existAmountReportVo.setFishTypeId(fishTypeId);
    existAmountReportVo.setWorkOrderCode(workOrderEntity == null ? null : workOrderEntity.getCode());
    existAmountReportVo.setWorkOrderId(workOrderEntity == null ? null : workOrderEntity.getId());
    double specs = 0;
    if (!CollectionUtils.isEmpty(proofYieldEntitys)) {
      for (ProofYieldEntity proofYieldEntity : proofYieldEntitys) {
        if (fishTypeId.equals(proofYieldEntity.getFishType().getId())) {
          specs = proofYieldEntity.getSpecs() == null ? 0 : proofYieldEntity.getSpecs();
          break;
        }
      }
    }
    if (specs == 0 && !CollectionUtils.isEmpty(speces)) {
      if (speces.containsKey(fishTypeId)) {
        specs = speces.get(fishTypeId);
      }
    }
    specs = MathUtil.round(specs, 4);
    if (specs != 0) {
      existAmountReportVo.setSpecs(specs);
      existAmountReportVo.setWeight(existAmountReportVo.getCount() * specs);
      existAmountReportVo.setEarlyWeight(existAmountReportVo.getEarlyCount() * specs);
      existAmountReportVo.setInWeight(existAmountReportVo.getInCount() * specs);
      existAmountReportVo.setOutWeight(existAmountReportVo.getOutCount() * specs);
      existAmountReportVo.setPushFryWeight(existAmountReportVo.getPushFryCount() * specs);
      existAmountReportVo.setDamageWeight(existAmountReportVo.getDamageCount() * specs);
      existAmountReportVo.setSaleWeight(existAmountReportVo.getSaleCount() * specs);
    }
    return existAmountReportVo;
  }

  /**
   * 查询期末存塘量
   *
   * @param pond
   * @param startTimeParam
   * @param endTimeParam
   * @param existAmountReportVo
   * @param fishTypeId
   */
  private void currentExsitAmoutReport(PondEntity pond, Date startTimeParam, Date endTimeParam, ExistAmountReportVo existAmountReportVo, String fishTypeId) {
    ExitCountVo exitCountVo = this.computeReportExitCountByTime(pond.getId(), startTimeParam, endTimeParam, fishTypeId);
    existAmountReportVo.setCount(exitCountVo.getExitCount());
    existAmountReportVo.setInCount(exitCountVo.getDividInCount());
    existAmountReportVo.setOutCount(exitCountVo.getDividOutCount());
    existAmountReportVo.setPushFryCount(exitCountVo.getPushFryCount());
    existAmountReportVo.setDamageCount(exitCountVo.getDamageCount());
    existAmountReportVo.setSaleCount(exitCountVo.getSaleCount());
  }

  /**
   * 查询期初存塘数量
   *
   * @param pond
   * @param startTimeParam
   * @param endTimeParam
   * @param existAmountReportVo
   * @param fishTypeId
   */
  private void earlyExsitAmountReport(PondEntity pond, Date startTimeParam, Date endTimeParam, ExistAmountReportVo existAmountReportVo, String fishTypeId) {
    ExitCountVo exitCountVo = this.computeReportExitCountByTime(pond.getId(), startTimeParam, endTimeParam, fishTypeId);
    existAmountReportVo.setEarlyCount(exitCountVo.getExitCount());
  }

  private ExitCountVo computeReportExitCountByTime(String pondId, Date start, Date end, String fishTypeId) {
    if (StringUtils.isBlank(pondId) || start == null || end == null || StringUtils.isBlank(fishTypeId)) {
      return new ExitCountVo();
    }
    ExitCountVo result = new ExitCountVo();
    List<PushFryEntity> pushFryEntities = pushFryEntityService.findByPondAndTimesAndFishType(pondId, start, end, fishTypeId);
    List<DividePondEntity> inPonds = dividePondEntityService.findByInPondAndTimesAndFishType(pondId, start, end, fishTypeId);
    List<DividePondEntity> outPonds = dividePondEntityService.findByOutPondAndTimesAndFishType(pondId, start, end, fishTypeId);
    List<OutFishRegistEntity> outFishRegistEntities = outFishRegistEntityService.findByPondAndTimes(pondId, start, end);
    List<FishDamageEntity> fishDamageEntities = fishDamageEntityService.findByPondAndTimesAndFishType(pondId, start, end, fishTypeId);
    //本期投入数量
    double inCount;
    //本期投苗数量
    double pushFryCount = 0;
    //本期转入数量
    double inPondCount = 0;
    //本期转出数量
    double outCount;
    //本期出鱼数量
    double outFishCount = 0;
    //本期转分塘转出数量
    double outPondCount = 0;
    //本期损鱼数量
    double damageCount = 0;
    //本期存塘量
    double exitCount;

    if (!CollectionUtils.isEmpty(pushFryEntities)) {
      for (PushFryEntity e : pushFryEntities) {
        pushFryCount += e.getCount() == null ? 0 : e.getCount();
      }
    }
    if (!CollectionUtils.isEmpty(inPonds)) {
      for (DividePondEntity e : inPonds) {
        inPondCount += computeDivideCount(e.getDividePondItems());
      }
    }
    inCount = pushFryCount + inPondCount;
    if (!CollectionUtils.isEmpty(outPonds)) {
      for (DividePondEntity e : outPonds) {
        outPondCount += computeDivideCount(e.getDividePondItems());
      }
    }
    if (!CollectionUtils.isEmpty(outFishRegistEntities)) {
      for (OutFishRegistEntity e : outFishRegistEntities) {
        outFishCount += computeOutFishRegist(e.getOutFishInfos(), fishTypeId);
      }
    }
    outCount = outPondCount + outFishCount;
    if (!CollectionUtils.isEmpty(fishDamageEntities)) {
      for (FishDamageEntity e : fishDamageEntities) {
        damageCount += e.getCount();
      }
    }
    exitCount = round(inCount - damageCount - outCount, 2);
    result.setInCount(inCount);
    result.setPushFryCount(pushFryCount);
    result.setDividInCount(inPondCount);
    result.setOutCount(outCount);
    result.setSaleCount(outFishCount);
    result.setDividOutCount(outPondCount);
    result.setDamageCount(damageCount);
    result.setExitCount(exitCount);
    return result;
  }

  /**
   * 塘口id列表转化为实例列表
   *
   * @param ponds
   * @param pondIds
   */
  private void buildPonds(List<PondEntity> ponds, List<String> pondIds) {
    if (CollectionUtils.isEmpty(pondIds)) {
      return;
    }
    for (String pondId : pondIds) {
      PondEntity pondEntity = pondEntityService.findDetailsById(pondId);
      if (pondEntity != null) {
        ponds.add(pondEntity);
      }
    }
  }

  private double computeOutFishRegist(Set<OutFishInfoEntity> outFishInfos, String fishTypeId) {
    double count = 0;
    if (CollectionUtils.isEmpty(outFishInfos)) {
      return count;
    }
    for (OutFishInfoEntity info : outFishInfos) {
      if (info.getFishType() != null && fishTypeId.equals(info.getFishType().getId())
              && info.getTotalWeight() != null && info.getSpecs() > 0) {
        count += info.getTotalWeight() / info.getSpecs();
      }
    }
    return count;
  }
} 
