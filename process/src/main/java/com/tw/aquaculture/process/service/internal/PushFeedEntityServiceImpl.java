package com.tw.aquaculture.process.service.internal;

import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.rbac.server.service.UserService;
import com.bizunited.platform.rbac.server.vo.UserVo;
import com.google.common.collect.Lists;
import com.tw.aquaculture.common.entity.base.MatterEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.common.entity.process.PushFeedEntity;
import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import com.tw.aquaculture.common.entity.store.StockOutEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.common.utils.UnitUtil;
import com.tw.aquaculture.common.vo.reportslist.PushFeedReportVo;
import com.tw.aquaculture.common.vo.reportslist.ReportParamVo;
import com.tw.aquaculture.process.repository.PushFeedEntityRepository;
import com.tw.aquaculture.process.service.FormInstanceHandleService;
import com.tw.aquaculture.process.service.PondEntityService;
import com.tw.aquaculture.process.service.PushFeedEntityService;
import com.tw.aquaculture.process.service.WorkOrderEntityService;
import com.tw.aquaculture.process.service.base.CodeGenerateService;
import com.tw.aquaculture.process.service.base.MatterEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;

/**
 * PushFeedEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("PushFeedEntityServiceImpl")
public class PushFeedEntityServiceImpl implements PushFeedEntityService {
  @Autowired
  private PondEntityService pondEntityService;
  @Autowired
  private WorkOrderEntityService workOrderEntityService;
  @Autowired
  private PushFeedEntityRepository pushFeedEntityRepository;
  @Autowired
  private CodeGenerateService codeGenerateService;

  @Autowired
  private UserService userService;
  @Autowired
  private MatterEntityService matterEntityService;
  @Autowired
  private FormInstanceHandleService formInstanceHandleService;

  @Transactional
  @Override
  public PushFeedEntity create(PushFeedEntity pushFeedEntity) {
    PushFeedEntity current = this.createForm(pushFeedEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  }

  @Transactional
  @Override
  public PushFeedEntity createForm(PushFeedEntity pushFeedEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *   2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    pushFeedEntity.setCode(codeGenerateService.generateBusinessCode(CodeTypeEnum.PUSH_FEED_ENTITY));
    List<WorkOrderEntity> workOrderEntities = workOrderEntityService.findByStateAndPond(pushFeedEntity.getPond().getId(), EnableStateEnum.DISABLE.getState());
    if (!CollectionUtils.isEmpty(workOrderEntities)) {
      pushFeedEntity.setWorkOrder(workOrderEntities.get(workOrderEntities.size() - 1));
    } else {
      throw new RuntimeException("请先对该塘口注水");
    }
    if (DateUtils.notBetween(pushFeedEntity.getPushTime(), pushFeedEntity.getWorkOrder().getCreateTime(), new Date())) {
      throw new RuntimeException("填写时间不在养殖周期内，请重新填写");
    }
    this.createValidation(pushFeedEntity);

    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================

    this.pushFeedEntityRepository.saveAndFlush(pushFeedEntity);
    //饲料出库
    this.outStock(pushFeedEntity, pushFeedEntity.getPushWeight());
    // 返回最终处理的结果，里面带有详细的关联信息
    return pushFeedEntity;
  }

  /**
   * 出库减库存
   */
  private void outStock(PushFeedEntity pushFeedEntity, Double amount) {
    if (amount == null || amount == 0) {
      return;
    }
    MatterEntity matter = matterEntityService.findDetailsById(pushFeedEntity.getFeed().getId());
    Validate.notNull(matter, "没获取到该饲料信息");
    StockOutEntity stockOutEntity = new StockOutEntity();
    //stockOutEntity.setPond(pushFeedEntity.getPond());
    TwBaseEntity twBaseEntity = new TwBaseEntity();
    twBaseEntity.setId(this.pondEntityService.findDetailsById(pushFeedEntity.getPond().getId()).getBase().getId());
    stockOutEntity.setTwBase(twBaseEntity);
    stockOutEntity.setMatter(matter);
    stockOutEntity.setQuantity(amount);
    if (stockOutEntity.getQuantity() == null || stockOutEntity.getQuantity() == 0) {
      return;
    }
    stockOutEntity.setStore(pushFeedEntity.getStore());
    stockOutEntity.setSpec(matter.getSpecs());
    stockOutEntity.setCategory(matter.getMatterTypeName());
    Principal userPrincipal = SecurityContextHolder.getContext().getAuthentication();
    Validate.notNull(userPrincipal, "未获取到当前系统的登录人");
    // 通过operator确定当前的登录者信息
    String account = userPrincipal.getName();
    Validate.notBlank(account, "not found op user!");
    UserVo userVo = userService.findByAccount(account);
    Validate.notNull(userVo, "未获取到当前系统的登录人");
    UserEntity userEntity = new UserEntity();
    userEntity.setId(userVo.getId());
    stockOutEntity.setReceiver(userEntity);
    stockOutEntity.setStockOutDate(pushFeedEntity.getPushTime());
    stockOutEntity.setUnit(pushFeedEntity.getUnit());
    stockOutEntity.setCreateName(pushFeedEntity.getCreateName());
    stockOutEntity.setCreateTime(pushFeedEntity.getCreateTime());
    stockOutEntity.setCreatePosition(pushFeedEntity.getCreatePosition());
    formInstanceHandleService.create(stockOutEntity, "StockOutEntity", "StockOutEntityService.create", false);
  }

  /**
   * 在创建一个新的PushFeedEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(PushFeedEntity pushFeedEntity) {
    Validate.notNull(pushFeedEntity, "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(pushFeedEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    pushFeedEntity.setId(null);
    Validate.notBlank(pushFeedEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(pushFeedEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(pushFeedEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(pushFeedEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notBlank(pushFeedEntity.getCode(), "添加信息时，编码不能为空！");
    Validate.notNull(pushFeedEntity.getFoodState(), "添加信息时，吃食情况不能为空！");
    Validate.isTrue(pushFeedEntity.getFeed() != null && pushFeedEntity.getFeed().getId() != null, "饲料不能为空");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(pushFeedEntity.getFormInstanceId() == null || pushFeedEntity.getFormInstanceId().length() < 255, "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFeedEntity.getCreateName() == null || pushFeedEntity.getCreateName().length() < 255, "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFeedEntity.getCreatePosition() == null || pushFeedEntity.getCreatePosition().length() < 255, "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFeedEntity.getModifyName() == null || pushFeedEntity.getModifyName().length() < 255, "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFeedEntity.getModifyPosition() == null || pushFeedEntity.getModifyPosition().length() < 255, "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFeedEntity.getCode() == null || pushFeedEntity.getCode().length() < 255, "编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFeedEntity.getSpecs() == null || pushFeedEntity.getSpecs().length() < 255, "规格,在进行添加时填入值超过了限定长度(255)，请检查!");
    PushFeedEntity currentPushFeedEntity = this.findByFormInstanceId(pushFeedEntity.getFormInstanceId());
    Validate.isTrue(currentPushFeedEntity == null, "表单实例编号已存在,请检查");
    // 验证ManyToOne关联：塘口绑定值的正确性
    PondEntity currentPond = pushFeedEntity.getPond();
    Validate.notNull(currentPond, "塘口信息必须传入，请检查!!");
    String currentPkPond = currentPond.getId();
    Validate.notBlank(currentPkPond, "创建操作时，当前塘口信息必须关联！");
    Validate.notNull(this.pondEntityService.findById(currentPkPond), "塘口关联信息未找到，请检查!!");
  }

  @Transactional
  @Override
  public PushFeedEntity update(PushFeedEntity pushFeedEntity) {
    PushFeedEntity current = this.updateForm(pushFeedEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  }

  @Transactional
  @Override
  public PushFeedEntity updateForm(PushFeedEntity pushFeedEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     *
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */

    this.updateValidation(pushFeedEntity);
    // ===================基本信息
    String currentId = pushFeedEntity.getId();
    Optional<PushFeedEntity> op_currentPushFeedEntity = this.pushFeedEntityRepository.findById(currentId);
    PushFeedEntity currentPushFeedEntity = op_currentPushFeedEntity.orElse(null);
    boolean changeMatter = !currentPushFeedEntity.getFeed().getId().equals(pushFeedEntity.getFeed().getId());
    Double ducAmount;
    Double amount = null;
    MatterEntity duMatter;
    MatterEntity matter = null;
    if (!changeMatter) {
      ducAmount = pushFeedEntity.getPushWeight() - currentPushFeedEntity.getPushWeight();
      duMatter = pushFeedEntity.getFeed();
    } else {
      amount = pushFeedEntity.getPushWeight();
      ducAmount = -currentPushFeedEntity.getPushWeight();
      matter = pushFeedEntity.getFeed();
      duMatter = currentPushFeedEntity.getFeed();
    }
    currentPushFeedEntity = Validate.notNull(currentPushFeedEntity, "未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentPushFeedEntity.setFormInstanceId(pushFeedEntity.getFormInstanceId());
    currentPushFeedEntity.setCreateTime(pushFeedEntity.getCreateTime());
    currentPushFeedEntity.setCreateName(pushFeedEntity.getCreateName());
    currentPushFeedEntity.setCreatePosition(pushFeedEntity.getCreatePosition());
    currentPushFeedEntity.setModifyTime(pushFeedEntity.getModifyTime());
    currentPushFeedEntity.setModifyName(pushFeedEntity.getModifyName());
    currentPushFeedEntity.setModifyPosition(pushFeedEntity.getModifyPosition());
    currentPushFeedEntity.setCode(pushFeedEntity.getCode());
    currentPushFeedEntity.setFeed(pushFeedEntity.getFeed());
    currentPushFeedEntity.setFoodState(pushFeedEntity.getFoodState());
    currentPushFeedEntity.setSpecs(pushFeedEntity.getSpecs());
    currentPushFeedEntity.setPushWeight(pushFeedEntity.getPushWeight());
    currentPushFeedEntity.setPushTime(pushFeedEntity.getPushTime());
    currentPushFeedEntity.setNote(pushFeedEntity.getNote());
    currentPushFeedEntity.setPicture(pushFeedEntity.getPicture());
    currentPushFeedEntity.setPond(pushFeedEntity.getPond());
    currentPushFeedEntity.setWorkOrder(pushFeedEntity.getWorkOrder());
    currentPushFeedEntity.setStore(pushFeedEntity.getStore());
    if (!DateUtils.between(currentPushFeedEntity.getPushTime(), currentPushFeedEntity.getWorkOrder().getCreateTime(), new Date())) {
      throw new RuntimeException("填写时间不在养殖周期内，请重新填写");
    }
    this.pushFeedEntityRepository.saveAndFlush(currentPushFeedEntity);
    //出库
    pushFeedEntity.setFeed(matter);
    this.outStock(pushFeedEntity, amount);
    pushFeedEntity.setFeed(duMatter);
    this.outStock(pushFeedEntity, ducAmount);

    return currentPushFeedEntity;
  }

  /**
   * 在更新一个已有的PushFeedEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(PushFeedEntity pushFeedEntity) {
    Validate.isTrue(!StringUtils.isBlank(pushFeedEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");

    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(pushFeedEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(pushFeedEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(pushFeedEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(pushFeedEntity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    Validate.notBlank(pushFeedEntity.getCode(), "修改信息时，编码不能为空！");
    Validate.notNull(pushFeedEntity.getFoodState(), "修改信息时，吃食情况不能为空！");
    Validate.notNull(pushFeedEntity.getFeed(), "饲料不能为空");
    Validate.notBlank(pushFeedEntity.getFeed().getId(), "饲料不能为空");
    Validate.isTrue(pushFeedEntity.getFeed() != null && pushFeedEntity.getFeed().getId() != null, "饲料不能为空");

    // 重复性判断，基本属性，需要满足unique = true
    PushFeedEntity currentForFormInstanceId = this.findByFormInstanceId(pushFeedEntity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId(), pushFeedEntity.getId()), "表单实例编号已存在,请检查");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(pushFeedEntity.getFormInstanceId() == null || pushFeedEntity.getFormInstanceId().length() < 255, "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFeedEntity.getCreateName() == null || pushFeedEntity.getCreateName().length() < 255, "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFeedEntity.getCreatePosition() == null || pushFeedEntity.getCreatePosition().length() < 255, "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFeedEntity.getModifyName() == null || pushFeedEntity.getModifyName().length() < 255, "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFeedEntity.getModifyPosition() == null || pushFeedEntity.getModifyPosition().length() < 255, "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFeedEntity.getCode() == null || pushFeedEntity.getCode().length() < 255, "编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFeedEntity.getSpecs() == null || pushFeedEntity.getSpecs().length() < 255, "规格,在进行修改时填入值超过了限定长度(255)，请检查!");

    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
    PondEntity currentForPond = pushFeedEntity.getPond();
    Validate.notNull(currentForPond, "修改信息时，塘口必须传入，请检查!!");
  }

  @Override
  public PushFeedEntity findDetailsById(String id) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(id)) {
      return null;
    }
    PushFeedEntity current = this.pushFeedEntityRepository.findDetailsById(id);
    if (current == null) {
      return null;
    }
    return current;
  }

  @Override
  public PushFeedEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }

    Optional<PushFeedEntity> op = pushFeedEntityRepository.findById(id);
    return op.orElse(null);
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    PushFeedEntity current = this.findById(id);
    if (current != null) {
      this.pushFeedEntityRepository.delete(current);
    }
  }

  @Override
  public PushFeedEntity findDetailsByFormInstanceId(String formInstanceId) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    PushFeedEntity current = this.pushFeedEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if (current == null) {
      return null;
    }
    return current;
  }

  @Override
  public PushFeedEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    return this.pushFeedEntityRepository.findByFormInstanceId(formInstanceId);
  }

  @Override
  public Double sumConsumeByTimes(String baseId, String matterId, Date start, Date end) {

    return pushFeedEntityRepository.sumConsumeByTimes(baseId, matterId, start, end);
  }

  @Override
  public List<PushFeedReportVo> pushFeedReport(ReportParamVo reportParamVo) {
    Date startTime = reportParamVo.getStartTime();
    Date endTime = reportParamVo.getEndTime();
    String baseId = reportParamVo.getBaseId();
    String pondId = reportParamVo.getPondId();
    String matterId = reportParamVo.getMatterId();
    List<PushFeedEntity> pushFeedEntities = pushFeedEntityRepository.findByConditions(startTime, endTime, baseId, pondId, matterId);
    if(CollectionUtils.isEmpty(pushFeedEntities)){
      return Lists.newArrayList();
    }
    if (startTime == null || DateUtils.bigThan(startTime, pushFeedEntities.get(0).getPushTime())) {
      startTime = pushFeedEntities.get(0).getPushTime();
    }
    if (endTime == null || DateUtils.bigThan(pushFeedEntities.get(pushFeedEntities.size() - 1).getPushTime(), endTime)) {
      endTime = pushFeedEntities.get(pushFeedEntities.size() - 1).getPushTime();
    }
    return computeReportByPond(pushFeedEntities, startTime, endTime);
  }

  @Override
  public List<PushFeedEntity> findByPondAndTimes(String pondId, Date startTime, Date endTime) {
    if(StringUtils.isBlank(pondId) || startTime==null || endTime==null){
      return null;
    }
    return pushFeedEntityRepository.findByPondAndTimes(pondId, startTime, endTime);
  }

  private List<PushFeedReportVo> computeReportByPond(List<PushFeedEntity> pushFeedEntities, Date startTime, Date endTime) {
    List<PushFeedReportVo> result = Lists.newArrayList();
    Set<String> ponds = new HashSet<>();
    for (PushFeedEntity pushFeedEntity : pushFeedEntities) {
      ponds.add(pushFeedEntity.getPond().getId());
    }
    Validate.notEmpty(ponds, "塘口异常");
    for(String pondId : ponds){
      List<PushFeedReportVo> pushFeedReportVos = computeReportByMatter(pushFeedEntities, pondId, startTime, endTime);
      if(!CollectionUtils.isEmpty(pushFeedReportVos)){
        result.addAll(pushFeedReportVos);
      }
    }
    return result;
  }

  /**
   * 计算饲料投喂报表
   *
   * @param pushFeedEntities
   * @param pondId
   * @param startTime
   * @param endTime
   * @return
   */
  private List<PushFeedReportVo> computeReportByMatter(List<PushFeedEntity> pushFeedEntities, String pondId, Date startTime, Date endTime) {
    Set<String> feeds = new HashSet<>();
    List<PushFeedReportVo> result = Lists.newArrayList();
    for (PushFeedEntity pushFeedEntity : pushFeedEntities) {
      if(pondId.equals(pushFeedEntity.getPond().getId())){
        feeds.add(pushFeedEntity.getFeed().getId());
      }
    }
    PondEntity pond = pondEntityService.findDetailsById(pondId);
    TwBaseEntity base = pond.getBase();
    for (String feedId : feeds) {
      List<PushFeedReportVo> pushFeedReportVos = computeReport(findMatter(pushFeedEntities, feedId), base, pond, startTime, endTime, pushFeedEntities);
      result.addAll(pushFeedReportVos);
    }
    return CollectionUtils.isEmpty(result) ? null : result;
  }

  /**
   * 获取物料对象
   * @param pushFeedEntities
   * @param feedId
   * @return
   */
  private MatterEntity findMatter(List<PushFeedEntity> pushFeedEntities, String feedId) {
    if(CollectionUtils.isEmpty(pushFeedEntities)){
      return null;
    }
    for(PushFeedEntity pushFeedEntity : pushFeedEntities){
      if(feedId.equals(pushFeedEntity.getFeed().getId())){
        return pushFeedEntity.getFeed();
      }
    }
    return null;
  }

  /**
   * 按物料计算饲料投喂报表
   *
   * @param matter
   * @param base
   * @param pond
   * @param startTime
   * @param endTime
   * @param pushFeedEntities
   */
  private List<PushFeedReportVo> computeReport(MatterEntity matter, TwBaseEntity base, PondEntity pond, Date startTime, Date endTime, List<PushFeedEntity> pushFeedEntities) {
    List<PushFeedReportVo> rel = Lists.newArrayList();
    PushFeedReportVo pushFeedReportVo = new PushFeedReportVo();
    if (base != null) {
      if (base.getBaseOrg() != null && base.getBaseOrg().getParent() != null) {
        pushFeedReportVo.setCompanyId(base.getBaseOrg().getParent().getId());
        pushFeedReportVo.setCompanyName(base.getBaseOrg().getParent().getOrgName());
      }
      pushFeedReportVo.setBaseId(base.getId());
      if(base.getBaseOrg()!=null){
        pushFeedReportVo.setBaseName(base.getBaseOrg().getOrgName());
      }
    }
    if (pond != null) {
      pushFeedReportVo.setPondId(pond.getId());
      pushFeedReportVo.setPondName(pond.getName());
    }
    pushFeedReportVo.setFeedId(matter.getId());
    pushFeedReportVo.setFeedName(matter.getName());
    pushFeedReportVo.setStartTime(startTime);
    pushFeedReportVo.setEndTime(endTime);
    int count = 0;
    double amount = 0;
    double amountT = 0;
    for (PushFeedEntity pushFeedEntity : pushFeedEntities) {
      if (pushFeedEntity.getPond()!=null && pond.getId().equals(pushFeedEntity.getPond().getId())
              && pushFeedEntity.getFeed() != null && matter.getId().equals(pushFeedEntity.getFeed().getId())) {
        count += 1;
        amount += pushFeedEntity.getPushWeight() == null ? 0 : pushFeedEntity.getPushWeight();
        Double cAmoutT = UnitUtil.compute(pushFeedEntity.getUnit(), pushFeedEntity.getPushWeight(), matter);
        amountT += cAmoutT == null ? 0 : cAmoutT;
      }
    }
    pushFeedReportVo.setCount(count);
    pushFeedReportVo.setAmount(amount);
    pushFeedReportVo.setAmountT(amountT);
    if(count>0){
      rel.add(pushFeedReportVo);
    }
    return rel;
  }
} 
