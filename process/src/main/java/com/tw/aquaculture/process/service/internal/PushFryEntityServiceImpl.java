package com.tw.aquaculture.process.service.internal;

import com.bizunited.platform.core.service.invoke.InvokeProxyException;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.process.PushFryEntity;
import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.common.vo.reportslist.ReportParamVo;
import com.tw.aquaculture.process.repository.PushFryEntityRepository;
import com.tw.aquaculture.process.service.FishTypeEntityService;
import com.tw.aquaculture.process.service.FormInstanceHandleService;
import com.tw.aquaculture.process.service.PondEntityService;
import com.tw.aquaculture.process.service.PushFryEntityService;
import com.tw.aquaculture.process.service.WorkOrderEntityService;
import com.tw.aquaculture.process.service.base.CodeGenerateService;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.*;

/**
 * PushFryEntity业务模型的服务层接口实现
 * @author saturn
 */
@Service("PushFryEntityServiceImpl")
public class PushFryEntityServiceImpl implements PushFryEntityService {
  @Autowired
  private PondEntityService pondEntityService;
  @Autowired
  private FishTypeEntityService fishTypeEntityService;
  @Autowired
  private WorkOrderEntityService workOrderEntityService;
  @Autowired
  private PushFryEntityRepository pushFryEntityRepository;
  @Autowired
  private CodeGenerateService codeGenerateService;
  @Autowired
  private FormInstanceHandleService formInstanceHandleService;
  @Transactional
  @Override
  public PushFryEntity create(PushFryEntity pushFryEntity) throws InvokeProxyException {
    return this.createForm(pushFryEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public PushFryEntity createForm(PushFryEntity pushFryEntity) throws InvokeProxyException {
   /* 
    * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
    * 1、如果当前模型对象不是主模型
    * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
    * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
    * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
    * 2、如果当前模型对象是主业务模型
    *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
    *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
    *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
    * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
    *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
    *   2.3.2、以及验证每个分组的OneToMany明细信息
    * */
    pushFryEntity.setCode(codeGenerateService.generateBusinessCode(CodeTypeEnum.PUSH_FRY_ENTITY));
    Validate.notNull(pushFryEntity.getCode(), "编码不能为空");
    List<WorkOrderEntity> workOrderEntities = workOrderEntityService.findByStateAndPond(pushFryEntity.getPond().getId(), EnableStateEnum.DISABLE.getState());
    if(!CollectionUtils.isEmpty(workOrderEntities)){
      pushFryEntity.setWorkOrder(workOrderEntities.get(workOrderEntities.size()-1));
    }else{
      WorkOrderEntity workOrderEntity = new WorkOrderEntity();
      workOrderEntity.setPond(pushFryEntity.getPond());
      workOrderEntity.setEnableState(EnableStateEnum.DISABLE.getState());
      workOrderEntity.setCreateTime(pushFryEntity.getPushTime());
      workOrderEntity.setFormInstanceId(UUID.randomUUID().toString());
      //formInstanceHandleService.create(workOrderEntity, "WorkOrderEntity", "WorkOrderEntityService.create", false);
      workOrderEntityService.create(workOrderEntity);
      List<WorkOrderEntity> nWorkOrderEntities = workOrderEntityService.findByStateAndPond(pushFryEntity.getPond().getId(), EnableStateEnum.DISABLE.getState());
      if(!CollectionUtils.isEmpty(nWorkOrderEntities)){
        pushFryEntity.setWorkOrder(nWorkOrderEntities.get(0));
      }else{
        throw new IllegalArgumentException ("请先对该塘口注水");
      }
      if(!DateUtils.between(pushFryEntity.getPushTime(), pushFryEntity.getWorkOrder().getCreateTime(), new Date())){
        throw new IllegalArgumentException ("填写时间不在养殖周期内，请重新填写");
      }
    }
    this.createValidation(pushFryEntity);
    
    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    
    this.pushFryEntityRepository.saveAndFlush(pushFryEntity);
    
    // 返回最终处理的结果，里面带有详细的关联信息
    return pushFryEntity;
  }

  /**
   * 在创建一个新的PushFryEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(PushFryEntity pushFryEntity) {
    Validate.notNull(pushFryEntity , "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(pushFryEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    pushFryEntity.setId(null);
    Validate.notBlank(pushFryEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(pushFryEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(pushFryEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(pushFryEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notBlank(pushFryEntity.getCode(), "添加信息时，编码不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(pushFryEntity.getFormInstanceId() == null || pushFryEntity.getFormInstanceId().length() < 255 , "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getCreateName() == null || pushFryEntity.getCreateName().length() < 255 , "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getCreatePosition() == null || pushFryEntity.getCreatePosition().length() < 255 , "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getModifyName() == null || pushFryEntity.getModifyName().length() < 255 , "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getModifyPosition() == null || pushFryEntity.getModifyPosition().length() < 255 , "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getCode() == null || pushFryEntity.getCode().length() < 255 , "编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getSpecs() == null || pushFryEntity.getSpecs().length() < 255 , "规格,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getShipper() == null || pushFryEntity.getShipper().length() < 255 , "承运方,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getSupplyMarket() == null || pushFryEntity.getSupplyMarket().length() < 255 , "供应市场,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getSupplyCustomer() == null || pushFryEntity.getSupplyCustomer().length() < 255 , "供应客户,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getCustomerPhone() == null || pushFryEntity.getCustomerPhone().length() < 255 , "客户电话,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getNote() == null || pushFryEntity.getNote().length() < 1000 , "备注,在进行添加时填入值超过了限定长度(1000)，请检查!");
    Validate.isTrue(pushFryEntity.getPicture() == null || pushFryEntity.getPicture().length() < 10000 , "图片,在进行添加时填入值超过了限定长度(10000)，请检查!");
    PushFryEntity currentPushFryEntity = this.findByFormInstanceId(pushFryEntity.getFormInstanceId());
    Validate.isTrue(currentPushFryEntity == null, "表单实例编号已存在,请检查");
    // 验证ManyToOne关联：塘口绑定值的正确性
    PondEntity currentPond = pushFryEntity.getPond();
    Validate.notNull(currentPond , "塘口信息必须传入，请检查!!");
    String currentPkPond = currentPond.getId();
    Validate.notBlank(currentPkPond, "创建操作时，当前塘口信息必须关联！");
    Validate.notNull(this.pondEntityService.findById(currentPkPond) , "塘口关联信息未找到，请检查!!");
    // 验证ManyToOne关联：鱼种绑定值的正确性
    FishTypeEntity currentFishType = pushFryEntity.getFishType();
    Validate.notNull(currentFishType , "鱼种信息必须传入，请检查!!");
    String currentPkFishType = currentFishType.getId();
    Validate.notBlank(currentPkFishType, "创建操作时，当前鱼种信息必须关联！");
    Validate.notNull(this.fishTypeEntityService.findById(currentPkFishType) , "鱼种关联信息未找到，请检查!!");
  }
  @Transactional
  @Override
  public PushFryEntity update(PushFryEntity pushFryEntity) {
    return this.updateForm(pushFryEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public PushFryEntity updateForm(PushFryEntity pushFryEntity) {
    /* 
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     * 
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    
    this.updateValidation(pushFryEntity);
    // ===================基本信息
    String currentId = pushFryEntity.getId();
    Optional<PushFryEntity> opCurrentPushFryEntity = this.pushFryEntityRepository.findById(currentId);
    PushFryEntity currentPushFryEntity = opCurrentPushFryEntity.orElse(null);
    Validate.notNull(currentPushFryEntity ,"未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentPushFryEntity.setFormInstanceId(pushFryEntity.getFormInstanceId());
    currentPushFryEntity.setCreateTime(pushFryEntity.getCreateTime());
    currentPushFryEntity.setCreateName(pushFryEntity.getCreateName());
    currentPushFryEntity.setCreatePosition(pushFryEntity.getCreatePosition());
    currentPushFryEntity.setModifyTime(pushFryEntity.getModifyTime());
    currentPushFryEntity.setModifyName(pushFryEntity.getModifyName());
    currentPushFryEntity.setModifyPosition(pushFryEntity.getModifyPosition());
    currentPushFryEntity.setCode(pushFryEntity.getCode());
    currentPushFryEntity.setSpecs(pushFryEntity.getSpecs());
    currentPushFryEntity.setCount(pushFryEntity.getCount());
    currentPushFryEntity.setSummation(pushFryEntity.getSummation());
    currentPushFryEntity.setPushTime(pushFryEntity.getPushTime());
    currentPushFryEntity.setPrice(pushFryEntity.getPrice());
    currentPushFryEntity.setAmount(pushFryEntity.getAmount());
    currentPushFryEntity.setTransportFee(pushFryEntity.getTransportFee());
    currentPushFryEntity.setShipper(pushFryEntity.getShipper());
    currentPushFryEntity.setSupplyMarket(pushFryEntity.getSupplyMarket());
    currentPushFryEntity.setSupplyCustomer(pushFryEntity.getSupplyCustomer());
    currentPushFryEntity.setCustomerPhone(pushFryEntity.getCustomerPhone());
    currentPushFryEntity.setNote(pushFryEntity.getNote());
    currentPushFryEntity.setPicture(pushFryEntity.getPicture());
    currentPushFryEntity.setPond(pushFryEntity.getPond());
    currentPushFryEntity.setFishType(pushFryEntity.getFishType());
    currentPushFryEntity.setWorkOrder(pushFryEntity.getWorkOrder());
    if(!DateUtils.between(currentPushFryEntity.getPushTime(), currentPushFryEntity.getWorkOrder().getCreateTime(), new Date())){
      throw new IllegalArgumentException ("填写时间不在养殖周期内，请重新填写");
    }
    this.pushFryEntityRepository.saveAndFlush(currentPushFryEntity);
    return currentPushFryEntity;
  }
  /**
   * 在更新一个已有的PushFryEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(PushFryEntity pushFryEntity) {
    Validate.isTrue(!StringUtils.isBlank(pushFryEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
    
    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(pushFryEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(pushFryEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(pushFryEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(pushFryEntity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    Validate.notBlank(pushFryEntity.getCode(), "修改信息时，编码不能为空！");
    
    // 重复性判断，基本属性，需要满足unique = true
    PushFryEntity currentForFormInstanceId = this.findByFormInstanceId(pushFryEntity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId() , pushFryEntity.getId()) , "表单实例编号已存在,请检查"); 
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(pushFryEntity.getFormInstanceId() == null || pushFryEntity.getFormInstanceId().length() < 255 , "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getCreateName() == null || pushFryEntity.getCreateName().length() < 255 , "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getCreatePosition() == null || pushFryEntity.getCreatePosition().length() < 255 , "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getModifyName() == null || pushFryEntity.getModifyName().length() < 255 , "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getModifyPosition() == null || pushFryEntity.getModifyPosition().length() < 255 , "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getCode() == null || pushFryEntity.getCode().length() < 255 , "编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getSpecs() == null || pushFryEntity.getSpecs().length() < 255 , "规格,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getShipper() == null || pushFryEntity.getShipper().length() < 255 , "承运方,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getSupplyMarket() == null || pushFryEntity.getSupplyMarket().length() < 255 , "供应市场,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getSupplyCustomer() == null || pushFryEntity.getSupplyCustomer().length() < 255 , "供应客户,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getCustomerPhone() == null || pushFryEntity.getCustomerPhone().length() < 255 , "客户电话,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(pushFryEntity.getNote() == null || pushFryEntity.getNote().length() < 1000 , "备注,在进行修改时填入值超过了限定长度(1000)，请检查!");
    Validate.isTrue(pushFryEntity.getPicture() == null || pushFryEntity.getPicture().length() < 10000 , "图片,在进行修改时填入值超过了限定长度(10000)，请检查!");
    
    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
    PondEntity currentForPond = pushFryEntity.getPond();
    Validate.notNull(currentForPond , "修改信息时，塘口必须传入，请检查!!");
    FishTypeEntity currentForFishType = pushFryEntity.getFishType();
    Validate.notNull(currentForFishType , "修改信息时，鱼种必须传入，请检查!!");
  } 
  @Override
  public PushFryEntity findDetailsById(String id) {
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(id)) { 
      return null; 
    } 
    PushFryEntity current = this.pushFryEntityRepository.findDetailsById(id);
    if(current == null) {
      return null;
    } 
    return current;
  }
  @Override
  public PushFryEntity findById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    
    Optional<PushFryEntity> op = pushFryEntityRepository.findById(id);
    return op.orElse(null); 
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id , "进行删除时，必须给定主键信息!!");
    PushFryEntity current = this.findById(id);
    if(current != null) { 
      this.pushFryEntityRepository.delete(current);
    }
  }
  @Override
  public PushFryEntity findDetailsByFormInstanceId(String formInstanceId) {
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(formInstanceId)) { 
      return null; 
    } 
    PushFryEntity current = this.pushFryEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if(current == null) {
      return null;
    } 
    return current;
  }
  @Override
  public PushFryEntity findByFormInstanceId(String formInstanceId) {
    if(StringUtils.isBlank(formInstanceId)) { 
      return null;
    }
    return this.pushFryEntityRepository.findByFormInstanceId(formInstanceId);
  }

  @Override
  public List<PushFryEntity> findByPondAndTimes(String pondId, Date startTime, Date endTime) {
    if(StringUtils.isBlank(pondId) || startTime==null || endTime==null){
      return Collections.emptyList();
    }
    return pushFryEntityRepository.findByPondAndTime(pondId, startTime, endTime);
  }

  @Override
  public List<PushFryEntity> findByPondAndTimesAndFishType(String pondId, Date startTime, Date endTime, String fishTypeId) {
    if(StringUtils.isBlank(pondId) || startTime==null || endTime==null || fishTypeId==null){
      return Collections.emptyList();
    }
    return pushFryEntityRepository.findByPondAndTimesAndFishType(pondId, startTime, endTime, fishTypeId);
  }

  @Override
  public List<PushFryEntity> findByPondAndWorkOrderIdAndFishType(String pondId, String workOrderId, String fishTypeId) {
    if(StringUtils.isBlank(pondId) || StringUtils.isBlank(workOrderId) || StringUtils.isBlank(fishTypeId)){
      return Collections.emptyList();
    }
    return pushFryEntityRepository.findByPondAndWorkOrderIdAndFishType(pondId, workOrderId, fishTypeId);
  }

  @Override
  public List<PushFryEntity> findByPondAndWorkOrderId(String pondId, String workOrderId) {
    if(StringUtils.isBlank(pondId) || StringUtils.isBlank(workOrderId)){
      return Collections.emptyList();
    }
    return pushFryEntityRepository.findByPondAndWorkOrderId(pondId, workOrderId);
  }

  @Override
  public List<PushFryEntity> findSimpleByPondAndTimes(String pondId, Date startTime, Date endTime) {
    if(StringUtils.isBlank(pondId) || startTime==null || endTime==null){
      return null;
    }
    return pushFryEntityRepository.findSimpleByPondAndTimes(pondId, startTime, endTime);
  }

  @Override
  public List<PushFryEntity> findByPond(String pondId) {
    if(StringUtils.isBlank(pondId)){
      return null;
    }
    return pushFryEntityRepository.findByPond(pondId);
  }

  @Override
  public List<PushFryEntity> findByPondAndFishTypeAndTimes(ReportParamVo reportParamVo) {
    if (reportParamVo == null) {
      return Lists.newArrayList();
    }
    String pondId = reportParamVo.getPondId();
    String fishTypeId = reportParamVo.getFishTypeId();
    Date startTime = reportParamVo.getStartTime();
    Date endTime = reportParamVo.getEndTime();
    return this.pushFryEntityRepository.findByPondAndFishTypeAndTimes(pondId, fishTypeId, startTime, endTime);
  }
} 
