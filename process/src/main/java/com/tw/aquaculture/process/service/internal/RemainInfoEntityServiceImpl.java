package com.tw.aquaculture.process.service.internal;

import com.google.common.collect.Sets;
import com.tw.aquaculture.common.entity.process.RemainInfoEntity;
import com.tw.aquaculture.process.repository.RemainInfoEntityRepository;
import com.tw.aquaculture.process.service.RemainInfoEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;

/**
 * RemainInfoEntity业务模型的服务层接口实现
 * @author saturn
 */
@Service("RemainInfoEntityServiceImpl")
public class RemainInfoEntityServiceImpl implements RemainInfoEntityService {
  @Autowired
  private RemainInfoEntityRepository remainInfoEntityRepository;
  @Transactional
  @Override
  public RemainInfoEntity create(RemainInfoEntity remainInfoEntity) {
    return this.createForm(remainInfoEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public RemainInfoEntity createForm(RemainInfoEntity remainInfoEntity) {
   /* 
    * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
    * 1、如果当前模型对象不是主模型
    * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
    * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
    * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
    * 2、如果当前模型对象是主业务模型
    *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
    *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
    *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
    * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
    *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
    *   2.3.2、以及验证每个分组的OneToMany明细信息
    * */
    this.createValidation(remainInfoEntity);
    
    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    
    this.remainInfoEntityRepository.save(remainInfoEntity);
    
    // 返回最终处理的结果，里面带有详细的关联信息
    return remainInfoEntity;
  }
  /**
   * 在创建一个新的RemainInfoEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(RemainInfoEntity remainInfoEntity) {
    Validate.notNull(remainInfoEntity , "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(remainInfoEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    remainInfoEntity.setId(null);
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(remainInfoEntity.getLowPrice() == null || remainInfoEntity.getLowPrice().length() < 255 , "低价,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(remainInfoEntity.getUpPrice() == null || remainInfoEntity.getUpPrice().length() < 255 , "高价,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(remainInfoEntity.getWeight() == null || remainInfoEntity.getWeight().length() < 255 , "数量,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(remainInfoEntity.getOutIdea() == null || remainInfoEntity.getOutIdea().length() < 1000 , "出鱼意见,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.notNull(remainInfoEntity.getFishType(), "鱼种不能为空");
  }
  @Transactional
  @Override
  public RemainInfoEntity update(RemainInfoEntity remainInfoEntity) {
    return this.updateForm(remainInfoEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public RemainInfoEntity updateForm(RemainInfoEntity remainInfoEntity) {
    /* 
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     * 
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    
    this.updateValidation(remainInfoEntity);
    // ===================基本信息
    String currentId = remainInfoEntity.getId();
    Optional<RemainInfoEntity> opCurrentRemainInfoEntity = this.remainInfoEntityRepository.findById(currentId);
    RemainInfoEntity currentRemainInfoEntity = opCurrentRemainInfoEntity.orElse(null);
    Validate.notNull(currentRemainInfoEntity ,"未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentRemainInfoEntity.setPushFryTime(remainInfoEntity.getPushFryTime());
    currentRemainInfoEntity.setLowPrice(remainInfoEntity.getLowPrice());
    currentRemainInfoEntity.setUpPrice(remainInfoEntity.getUpPrice());
    currentRemainInfoEntity.setSpecs(remainInfoEntity.getSpecs());
    currentRemainInfoEntity.setPredictOutTime(remainInfoEntity.getPredictOutTime());
    currentRemainInfoEntity.setWeight(remainInfoEntity.getWeight());
    currentRemainInfoEntity.setOutIdea(remainInfoEntity.getOutIdea());
    currentRemainInfoEntity.setFishType(remainInfoEntity.getFishType());
    currentRemainInfoEntity.setOutFishApply(remainInfoEntity.getOutFishApply());
    
    this.remainInfoEntityRepository.saveAndFlush(currentRemainInfoEntity);
    return currentRemainInfoEntity;
  }
  /**
   * 在更新一个已有的RemainInfoEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(RemainInfoEntity remainInfoEntity) {
    Validate.isTrue(!StringUtils.isBlank(remainInfoEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(remainInfoEntity.getLowPrice() == null || remainInfoEntity.getLowPrice().length() < 255 , "低价,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(remainInfoEntity.getUpPrice() == null || remainInfoEntity.getUpPrice().length() < 255 , "高价,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(remainInfoEntity.getWeight() == null || remainInfoEntity.getWeight().length() < 255 , "数量,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(remainInfoEntity.getOutIdea() == null || remainInfoEntity.getOutIdea().length() < 1000 , "出鱼意见,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.notNull(remainInfoEntity.getFishType(), "鱼种不能为空");
    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
  } 
  @Override
  public Set<RemainInfoEntity> findDetailsByFishType(String fishType) {
    if(StringUtils.isBlank(fishType)) { 
      return Sets.newHashSet();
    }
    return this.remainInfoEntityRepository.findDetailsByFishType(fishType);
  }
  @Override
  public Set<RemainInfoEntity> findDetailsByOutFishApply(String outFishApply) {
    if(StringUtils.isBlank(outFishApply)) { 
      return Sets.newHashSet();
    }
    return this.remainInfoEntityRepository.findDetailsByOutFishApply(outFishApply);
  }
  @Override
  public RemainInfoEntity findDetailsById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    return this.remainInfoEntityRepository.findDetailsById(id);
  }
  @Override
  public RemainInfoEntity findById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    
    Optional<RemainInfoEntity> op = remainInfoEntityRepository.findById(id);
    return op.orElse(null); 
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id , "进行删除时，必须给定主键信息!!");
    RemainInfoEntity current = this.findById(id);
    if(current != null) { 
      this.remainInfoEntityRepository.delete(current);
    }
  }
} 
