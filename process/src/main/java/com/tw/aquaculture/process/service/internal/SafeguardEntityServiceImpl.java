package com.tw.aquaculture.process.service.internal;

import com.bizunited.platform.core.entity.UserEntity;
import com.bizunited.platform.kuiper.starter.service.KuiperToolkitService;
import com.bizunited.platform.rbac.server.service.UserService;
import com.bizunited.platform.rbac.server.vo.UserVo;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.tw.aquaculture.common.entity.base.MatterEntity;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.common.entity.process.MedicineFeedBackEntity;
import com.tw.aquaculture.common.entity.process.SafeguardEntity;
import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import com.tw.aquaculture.common.entity.store.StockOutEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.common.utils.UnitUtil;
import com.tw.aquaculture.common.vo.reportslist.ReportParamVo;
import com.tw.aquaculture.common.vo.reportslist.SafeReportVo;
import com.tw.aquaculture.process.repository.SafeguardEntityRepository;
import com.tw.aquaculture.process.service.FormInstanceHandleService;
import com.tw.aquaculture.process.service.MedicineFeedBackEntityService;
import com.tw.aquaculture.process.service.PondEntityService;
import com.tw.aquaculture.process.service.SafeguardEntityService;
import com.tw.aquaculture.process.service.WorkOrderEntityService;
import com.tw.aquaculture.process.service.base.CodeGenerateService;
import com.tw.aquaculture.process.service.base.MatterEntityService;
import com.tw.aquaculture.process.service.base.StockOutEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;

/**
 * SafeguardEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("SafeguardEntityServiceImpl")
public class SafeguardEntityServiceImpl implements SafeguardEntityService {
  @Autowired
  private PondEntityService pondEntityService;
  @Autowired
  private WorkOrderEntityService workOrderEntityService;
  @Autowired
  private MedicineFeedBackEntityService medicineFeedBackEntityService;
  @Autowired
  private SafeguardEntityRepository safeguardEntityRepository;
  @Autowired
  private CodeGenerateService codeGenerateService;
  @Autowired
  private UserService userService;
  @Autowired
  private MatterEntityService matterEntityService;
  @Autowired
  private FormInstanceHandleService formInstanceHandleService;
  @Autowired
  private StockOutEntityService stockOutEntityService;
  /**
   * Kuiper表单引擎用于减少编码工作量的工具包
   */
  @Autowired
  private KuiperToolkitService kuiperToolkitService;

  @Transactional
  @Override
  public SafeguardEntity create(SafeguardEntity safeguardEntity) {
    SafeguardEntity current = this.createForm(safeguardEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  }

  @Transactional
  @Override
  public SafeguardEntity createForm(SafeguardEntity safeguardEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *   2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    safeguardEntity.setCode(codeGenerateService.generateBusinessCode(CodeTypeEnum.SAFEGUARD_ENTITY));
    List<WorkOrderEntity> workOrderEntities = workOrderEntityService.findByStateAndPond(safeguardEntity.getPond().getId(), EnableStateEnum.DISABLE.getState());
    if (!CollectionUtils.isEmpty(workOrderEntities)) {
      safeguardEntity.setWorkOrder(workOrderEntities.get(workOrderEntities.size() - 1));
      if (!DateUtils.between(safeguardEntity.getMedicineTime(), safeguardEntity.getWorkOrder().getCreateTime(), new Date())) {
        throw new RuntimeException("填写时间不在养殖周期内，请重新填写");
      }
    } /*else {
      throw new RuntimeException("请先对该塘口注水");
    }*/
    this.createValidation(safeguardEntity);

    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    if(safeguardEntity.getWorkOrder()!=null && StringUtils.isNotBlank(safeguardEntity.getWorkOrder().getId())){
      WorkOrderEntity workOrderEntity = new WorkOrderEntity();
      workOrderEntity.setId(safeguardEntity.getWorkOrder().getId());
      safeguardEntity.setWorkOrder(workOrderEntity);
    }
    PondEntity pondEntity = this.pondEntityService.findDetailsById(safeguardEntity.getPond().getId());
    pondEntity.setWaterDeep(safeguardEntity.getWaterDepth());
    pondEntity.setState(safeguardEntity.getSafeState());
    this.outStock(safeguardEntity, safeguardEntity.getMedicineCount());
    this.pondEntityService.update(pondEntity);

    this.safeguardEntityRepository.saveAndFlush(safeguardEntity);
    // 处理明细信息：用药反馈
    Set<MedicineFeedBackEntity> medicineFeedBackEntityItems = safeguardEntity.getMedicineFeedBacks();
    if (medicineFeedBackEntityItems != null) {
      for (MedicineFeedBackEntity medicineFeedBackEntityItem : medicineFeedBackEntityItems) {
        medicineFeedBackEntityItem.setSafeguard(safeguardEntity);
        this.medicineFeedBackEntityService.create(medicineFeedBackEntityItem);
      }
    }

    // 返回最终处理的结果，里面带有详细的关联信息
    return safeguardEntity;
  }

  /**
   * 出库减库存
   */
  private void outStock(SafeguardEntity safeguardEntity, Double amount) {
    if (amount == null || amount == 0) {
      return;
    }
    MatterEntity matter = matterEntityService.findDetailsById(safeguardEntity.getMedicine().getId());
    Validate.notNull(matter, "没获取到该饲料信息");
    StockOutEntity stockOutEntity = new StockOutEntity();
    //stockOutEntity.setPond(safeguardEntity.getPond());
    TwBaseEntity twBaseEntity = new TwBaseEntity();
    twBaseEntity.setId(this.pondEntityService.findDetailsById(safeguardEntity.getPond().getId()).getBase().getId());
    stockOutEntity.setTwBase(twBaseEntity);
    stockOutEntity.setMatter(matter);
    stockOutEntity.setQuantity(amount);
    if (stockOutEntity.getQuantity() == null || stockOutEntity.getQuantity() == 0) {
      return;
    }
    stockOutEntity.setUnit(safeguardEntity.getUnit());
    stockOutEntity.setStore(safeguardEntity.getStore());
    stockOutEntity.setSpec(matter.getSpecs());
    stockOutEntity.setCategory(matter.getMatterTypeName());
    Principal userPrincipal = SecurityContextHolder.getContext().getAuthentication();
    if(userPrincipal!=null && userPrincipal.getName()!=null){
      // 通过operator确定当前的登录者信息
      String account = userPrincipal.getName();
      UserVo userVo = userService.findByAccount(account);
      if(userVo!=null){
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userVo.getId());
        stockOutEntity.setReceiver(userEntity);
      }
    }
    stockOutEntity.setStockOutDate(safeguardEntity.getMedicineTime());
    stockOutEntity.setUnit(matter.getMatterHostUnit());
    stockOutEntity.setCreateName(safeguardEntity.getCreateName());
    stockOutEntity.setCreateTime(safeguardEntity.getCreateTime());
    stockOutEntity.setCreatePosition(safeguardEntity.getCreatePosition());
    stockOutEntity.setFormInstanceId(UUID.randomUUID().toString());
    stockOutEntityService.create(stockOutEntity);


    //formInstanceHandleService.create(stockOutEntity, "StockOutEntity", "StockOutEntityService.create", false);
  }

  /**
   * 在创建一个新的SafeguardEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(SafeguardEntity safeguardEntity) {
    Validate.notNull(safeguardEntity, "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(safeguardEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    safeguardEntity.setId(null);
    Validate.notBlank(safeguardEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(safeguardEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(safeguardEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(safeguardEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notBlank(safeguardEntity.getCode(), "添加信息时，编码不能为空！");
    Validate.notNull(safeguardEntity.getMedicine(), "物料不能为空");
    Validate.notNull(safeguardEntity.getMedicineCount(), "用药量不能为空");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(safeguardEntity.getFormInstanceId() == null || safeguardEntity.getFormInstanceId().length() < 255, "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(safeguardEntity.getCreateName() == null || safeguardEntity.getCreateName().length() < 255, "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(safeguardEntity.getCreatePosition() == null || safeguardEntity.getCreatePosition().length() < 255, "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(safeguardEntity.getModifyName() == null || safeguardEntity.getModifyName().length() < 255, "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(safeguardEntity.getModifyPosition() == null || safeguardEntity.getModifyPosition().length() < 255, "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(safeguardEntity.getCode() == null || safeguardEntity.getCode().length() < 255, "编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(safeguardEntity.getMedicineType() == null || safeguardEntity.getMedicineType().length() < 255, "用药类型,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(safeguardEntity.getReason() == null || safeguardEntity.getReason().length() < 255, "用药原因,在进行添加时填入值超过了限定长度(255)，请检查!");
    SafeguardEntity currentSafeguardEntity = this.findByFormInstanceId(safeguardEntity.getFormInstanceId());
    Validate.isTrue(currentSafeguardEntity == null, "表单实例编号已存在,请检查");
    // 验证ManyToOne关联：塘口绑定值的正确性
    Validate.notNull(safeguardEntity.getSafeState(), "发病状态不能为空");
    PondEntity currentPond = safeguardEntity.getPond();
    Validate.notNull(currentPond, "塘口信息必须传入，请检查!!");
    String currentPkPond = currentPond.getId();
    Validate.notBlank(currentPkPond, "创建操作时，当前塘口信息必须关联！");
    Validate.notNull(this.pondEntityService.findById(currentPkPond), "塘口关联信息未找到，请检查!!");
  }

  @Transactional
  @Override
  public SafeguardEntity update(SafeguardEntity safeguardEntity) {
    SafeguardEntity current = this.updateForm(safeguardEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  }

  @Transactional
  @Override
  public SafeguardEntity updateForm(SafeguardEntity safeguardEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     *
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */

    this.updateValidation(safeguardEntity);
    // ===================基本信息
    String currentId = safeguardEntity.getId();
    Optional<SafeguardEntity> op_currentSafeguardEntity = this.safeguardEntityRepository.findById(currentId);
    SafeguardEntity currentSafeguardEntity = op_currentSafeguardEntity.orElse(null);
    boolean changeMatter = !currentSafeguardEntity.getMedicine().getId().equals(safeguardEntity.getMedicine().getId());
    Double ducAmount;
    Double amount = null;
    MatterEntity duMatter;
    MatterEntity matter = null;
    if(!changeMatter){
      ducAmount = safeguardEntity.getMedicineCount()-currentSafeguardEntity.getMedicineCount();
      duMatter = safeguardEntity.getMedicine();
    }else{
      amount = safeguardEntity.getMedicineCount();
      ducAmount = - currentSafeguardEntity.getMedicineCount();
      matter = safeguardEntity.getMedicine();
      duMatter = currentSafeguardEntity.getMedicine();
    }
    //出库
    safeguardEntity.setMedicine(matter);
    this.outStock(safeguardEntity, amount);
    safeguardEntity.setMedicine(duMatter);
    this.outStock(safeguardEntity, ducAmount);
    currentSafeguardEntity = Validate.notNull(currentSafeguardEntity, "未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentSafeguardEntity.setFormInstanceId(safeguardEntity.getFormInstanceId());
    currentSafeguardEntity.setCreateTime(safeguardEntity.getCreateTime());
    currentSafeguardEntity.setCreateName(safeguardEntity.getCreateName());
    currentSafeguardEntity.setCreatePosition(safeguardEntity.getCreatePosition());
    currentSafeguardEntity.setModifyTime(safeguardEntity.getModifyTime());
    currentSafeguardEntity.setModifyName(safeguardEntity.getModifyName());
    currentSafeguardEntity.setModifyPosition(safeguardEntity.getModifyPosition());
    currentSafeguardEntity.setCode(safeguardEntity.getCode());
    currentSafeguardEntity.setWaterDepth(safeguardEntity.getWaterDepth());
    currentSafeguardEntity.setMedicineType(safeguardEntity.getMedicineType());
    currentSafeguardEntity.setReason(safeguardEntity.getReason());
    currentSafeguardEntity.setMedicineTime(safeguardEntity.getMedicineTime());
    currentSafeguardEntity.setMedicineCount(safeguardEntity.getMedicineCount());
    currentSafeguardEntity.setNote(safeguardEntity.getNote());
    currentSafeguardEntity.setPicture(safeguardEntity.getPicture());
    currentSafeguardEntity.setPond(safeguardEntity.getPond());
    currentSafeguardEntity.setWorkOrder(safeguardEntity.getWorkOrder());
    currentSafeguardEntity.setMedicine(safeguardEntity.getMedicine());
    currentSafeguardEntity.setSafeState(safeguardEntity.getSafeState());
    currentSafeguardEntity.setStore(safeguardEntity.getStore());
    currentSafeguardEntity.setUnit(safeguardEntity.getUnit());
    /*if (!DateUtils.between(currentSafeguardEntity.getMedicineTime(), currentSafeguardEntity.getWorkOrder().getCreateTime(), new Date())) {
      throw new RuntimeException("填写时间不在养殖周期内，请重新填写");
    }*/
    this.safeguardEntityRepository.saveAndFlush(currentSafeguardEntity);
    // ==========准备处理OneToMany关系的详情medicineFeedBacks
    // 清理出那些需要删除的medicineFeedBacks信息
    Set<MedicineFeedBackEntity> medicineFeedBackEntitys = null;
    if (safeguardEntity.getMedicineFeedBacks() != null) {
      medicineFeedBackEntitys = Sets.newLinkedHashSet(safeguardEntity.getMedicineFeedBacks());
    } else {
      medicineFeedBackEntitys = Sets.newLinkedHashSet();
    }
    Set<MedicineFeedBackEntity> dbMedicineFeedBackEntitys = this.medicineFeedBackEntityService.findDetailsBySafeguard(safeguardEntity.getId());
    if (dbMedicineFeedBackEntitys == null) {
      dbMedicineFeedBackEntitys = Sets.newLinkedHashSet();
    }
    // 求得的差集既是需要删除的数据 
    Set<String> mustDeleteMedicineFeedBacksPks = kuiperToolkitService.collectionDiffent(dbMedicineFeedBackEntitys, medicineFeedBackEntitys, MedicineFeedBackEntity::getId);
    if (mustDeleteMedicineFeedBacksPks != null && !mustDeleteMedicineFeedBacksPks.isEmpty()) {
      for (String mustDeleteMedicineFeedBacksPk : mustDeleteMedicineFeedBacksPks) {
        this.medicineFeedBackEntityService.deleteById(mustDeleteMedicineFeedBacksPk);
      }
    }
    // 对传来的记录进行添加或者修改操作 
    for (MedicineFeedBackEntity medicineFeedBackEntityItem : medicineFeedBackEntitys) {
      medicineFeedBackEntityItem.setSafeguard(safeguardEntity);
      if (StringUtils.isBlank(medicineFeedBackEntityItem.getId())) {
        medicineFeedBackEntityService.create(medicineFeedBackEntityItem);
      } else {
        medicineFeedBackEntityService.update(medicineFeedBackEntityItem);
      }
    }
    PondEntity pondEntity = this.pondEntityService.findDetailsById(safeguardEntity.getPond().getId());
    Double waterDeep = safeguardEntity.getWaterDepth();
    pondEntity.setWaterDeep(waterDeep);
    pondEntity.setState(safeguardEntity.getSafeState());
    this.pondEntityService.update(pondEntity);


    return currentSafeguardEntity;
  }

  /**
   * 在更新一个已有的SafeguardEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(SafeguardEntity safeguardEntity) {
    Validate.isTrue(!StringUtils.isBlank(safeguardEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");

    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(safeguardEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(safeguardEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(safeguardEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(safeguardEntity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    Validate.notBlank(safeguardEntity.getCode(), "修改信息时，编码不能为空！");
    Validate.notNull(safeguardEntity.getSafeState(), "发病状态不能为空");
    Validate.notNull(safeguardEntity.getMedicine(), "物料不能为空");
    Validate.notNull(safeguardEntity.getMedicineCount(), "用药量不能为空");

    // 重复性判断，基本属性，需要满足unique = true
    SafeguardEntity currentForFormInstanceId = this.findByFormInstanceId(safeguardEntity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId(), safeguardEntity.getId()), "表单实例编号已存在,请检查");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(safeguardEntity.getFormInstanceId() == null || safeguardEntity.getFormInstanceId().length() < 255, "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(safeguardEntity.getCreateName() == null || safeguardEntity.getCreateName().length() < 255, "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(safeguardEntity.getCreatePosition() == null || safeguardEntity.getCreatePosition().length() < 255, "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(safeguardEntity.getModifyName() == null || safeguardEntity.getModifyName().length() < 255, "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(safeguardEntity.getModifyPosition() == null || safeguardEntity.getModifyPosition().length() < 255, "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(safeguardEntity.getCode() == null || safeguardEntity.getCode().length() < 255, "编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(safeguardEntity.getMedicineType() == null || safeguardEntity.getMedicineType().length() < 255, "用药类型,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(safeguardEntity.getReason() == null || safeguardEntity.getReason().length() < 255, "用药原因,在进行修改时填入值超过了限定长度(255)，请检查!");

    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
    PondEntity currentForPond = safeguardEntity.getPond();
    Validate.notNull(currentForPond, "修改信息时，塘口必须传入，请检查!!");
  }

  @Override
  public SafeguardEntity findDetailsById(String id) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(id)) {
      return null;
    }
    SafeguardEntity current = this.safeguardEntityRepository.findDetailsById(id);
    if (current == null) {
      return null;
    }
    String currentPkValue = current.getId();
    // 2、======
    // 用药反馈 的明细信息查询
    Collection<MedicineFeedBackEntity> oneToManyMedicineFeedBacksDetails = this.medicineFeedBackEntityService.findDetailsBySafeguard(currentPkValue);
    current.setMedicineFeedBacks(Sets.newLinkedHashSet(oneToManyMedicineFeedBacksDetails));

    return current;
  }

  @Override
  public SafeguardEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }

    Optional<SafeguardEntity> op = safeguardEntityRepository.findById(id);
    return op.orElse(null);
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    SafeguardEntity current = this.findById(id);
    if (current != null) {
      this.safeguardEntityRepository.delete(current);
    }
  }

  @Override
  public SafeguardEntity findDetailsByFormInstanceId(String formInstanceId) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    SafeguardEntity current = this.safeguardEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if (current == null) {
      return null;
    }
    String currentPkValue = current.getId();
    // 2、======
    // 用药反馈 的明细信息查询
    Collection<MedicineFeedBackEntity> oneToManyMedicineFeedBacksDetails = this.medicineFeedBackEntityService.findDetailsBySafeguard(currentPkValue);
    current.setMedicineFeedBacks(Sets.newLinkedHashSet(oneToManyMedicineFeedBacksDetails));

    return current;
  }

  @Override
  public SafeguardEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    return this.safeguardEntityRepository.findByFormInstanceId(formInstanceId);
  }

  @Override
  public List<SafeguardEntity> findNoneWorkOrderByPond(String pondId) {
    if(StringUtils.isBlank(pondId)){
      return null;
    }
    return this.safeguardEntityRepository.findNoneWorkOrderByPond(pondId);
  }

  @Override
  public List<SafeReportVo> safeReport(ReportParamVo reportParamVo) {
    Date startTime = reportParamVo.getStartTime();
    Date endTime = reportParamVo.getEndTime();
    String baseId = reportParamVo.getBaseId();
    String pondId = reportParamVo.getPondId();
    String matterId = reportParamVo.getMatterId();
    List<SafeguardEntity> safeguardEntities  = this.safeguardEntityRepository.findByConditions(startTime, endTime, baseId, pondId, matterId);
    if(CollectionUtils.isEmpty(safeguardEntities)){
      return Lists.newArrayList();
    }
    if (startTime == null || DateUtils.bigThan(startTime, safeguardEntities.get(0).getMedicineTime())) {
      startTime = safeguardEntities.get(0).getMedicineTime();
    }
    if (endTime == null || DateUtils.bigThan(safeguardEntities.get(safeguardEntities.size() - 1).getMedicineTime(), endTime)) {
      endTime = safeguardEntities.get(safeguardEntities.size() - 1).getMedicineTime();
    }
    return computeReportByPond(safeguardEntities, startTime, endTime);
  }

  private List<SafeReportVo> computeReportByPond(List<SafeguardEntity> safeguardEntities, Date startTime, Date endTime) {
    List<SafeReportVo> result = Lists.newArrayList();
    Map<String, PondEntity> ponds = new TreeMap<>();
    for (SafeguardEntity safeguardEntity : safeguardEntities) {
      ponds.put(safeguardEntity.getPond().getId(), safeguardEntity.getPond());
    }
    Validate.notEmpty(ponds, "塘口异常");
    ponds.entrySet().forEach(pond -> {
      computeReportByMatter(safeguardEntities, pond.getValue().getBase().getId(), pond.getValue().getId(), startTime, endTime, result);
    });
    return result;
  }

  /**
   * 计算饲料投喂报表
   *
   * @param safeguardEntities
   * @param baseId
   * @param pondId
   * @param startTime
   * @param endTime
   * @param result
   * @return
   */
  private List<SafeReportVo> computeReportByMatter(List<SafeguardEntity> safeguardEntities, String baseId, String pondId,
                                                   Date startTime, Date endTime, List<SafeReportVo> result) {
    Map<String, MatterEntity> feeds = new TreeMap<>();
    for (SafeguardEntity safeguardEntity : safeguardEntities) {
      feeds.put(safeguardEntity.getMedicine().getId(), safeguardEntity.getMedicine());
    }
    PondEntity pond = pondEntityService.findDetailsById(pondId);
    TwBaseEntity base = pond.getBase();
    Validate.notEmpty(feeds, "饲料异常");
    Set<Map.Entry<String, MatterEntity>> entrySet = feeds.entrySet();
    for (Map.Entry<String, MatterEntity> entry : entrySet) {
      computeReport(entry.getValue(), result, base, pond, startTime, endTime, safeguardEntities);
    }
    return CollectionUtils.isEmpty(result) ? null : result;
  }

  /**
   * 按物料计算饲料投喂报表
   *
   * @param matter
   * @param result
   * @param base
   * @param pond
   * @param startTime
   * @param endTime
   * @param safeguardEntities
   */
  private void computeReport(MatterEntity matter, List<SafeReportVo> result, TwBaseEntity base, PondEntity pond,
                             Date startTime, Date endTime, List<SafeguardEntity> safeguardEntities) {

    SafeReportVo safeReportVo = new SafeReportVo();
    if (base != null) {
      if (base.getBaseOrg() != null && base.getBaseOrg().getParent() != null) {
        safeReportVo.setCompanyId(base.getBaseOrg().getParent().getId());
        safeReportVo.setCompanyName(base.getBaseOrg().getParent().getOrgName());
      }
      safeReportVo.setBaseId(base.getId());
      if(base.getBaseOrg()!=null){
        safeReportVo.setBaseName(base.getBaseOrg().getOrgName());
      }
    }
    if (pond != null) {
      safeReportVo.setPondId(pond.getId());
      safeReportVo.setPondName(pond.getName());
    }
    safeReportVo.setMedicineId(matter.getId());
    safeReportVo.setMedicineName(matter.getName());
    safeReportVo.setStartTime(startTime);
    safeReportVo.setEndTime(endTime);
    int count = 0;
    double amount = 0;
    double amountT = 0;
    double amountA = 0;
    for (SafeguardEntity safeguardEntity : safeguardEntities) {
      if (safeguardEntity.getPond()!=null && pond.getId().equals(safeguardEntity.getPond().getId())
              && safeguardEntity.getMedicine() != null && matter.getId().equals(safeguardEntity.getMedicine().getId())) {
        count += 1;
        amount += safeguardEntity.getMedicineCount() == null ? 0 : safeguardEntity.getMedicineCount();
        Double cAmoutT = UnitUtil.compute(safeguardEntity.getUnit(), safeguardEntity.getMedicineCount(), matter);
        amountT += cAmoutT == null ? 0 : cAmoutT;
        amountA += UnitUtil.computeAssist(matter.getMatterHostUnitCode(), cAmoutT, matter);
        safeReportVo.setHostUnit(matter.getMatterHostUnit());
        safeReportVo.setAssistUnit(matter.getMatterAssistUnit());
      }
    }
    safeReportVo.setCount(count);
    //safeReportVo.setAmount(amount);
    safeReportVo.setAmountT(amountT);
    safeReportVo.setAmountA(amountA);
    safeReportVo.setUnit(matter.getMatterHostUnit());
    if(count>0){
      result.add(safeReportVo);
    }
  }
} 
