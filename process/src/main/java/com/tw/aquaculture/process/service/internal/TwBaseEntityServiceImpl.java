package com.tw.aquaculture.process.service.internal;

import com.bizunited.platform.core.entity.OrganizationEntity;
import com.bizunited.platform.rbac.server.service.OrganizationService;
import com.bizunited.platform.rbac.server.vo.OrganizationVo;
import com.google.common.collect.Sets;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.process.repository.TwBaseEntityRepository;
import com.tw.aquaculture.process.service.PondEntityService;
import com.tw.aquaculture.process.service.TwBaseEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * TwBaseEntity业务模型的服务层接口实现
 * @author saturn
 */
@Service("TwBaseEntityServiceImpl")
public class TwBaseEntityServiceImpl implements TwBaseEntityService {
  @Autowired
  private PondEntityService pondEntityService;
  @Autowired
  private OrganizationService organizationEntityService;
  @Autowired
  private TwBaseEntityRepository twBaseEntityRepository;

  @Transactional
  @Override
  public TwBaseEntity create(TwBaseEntity twBaseEntity) {
    return this.createForm(twBaseEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public TwBaseEntity createForm(TwBaseEntity twBaseEntity) {
   /* 
    * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
    * 1、如果当前模型对象不是主模型
    * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
    * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
    * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
    * 2、如果当前模型对象是主业务模型
    *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
    *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
    *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
    * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
    *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
    *   2.3.2、以及验证每个分组的OneToMany明细信息
    * */
    this.createValidation(twBaseEntity);

    if(twBaseEntity.getBaseOrg()==null || twBaseEntity.getBaseOrg().getId()==null){
      throw new IllegalArgumentException ("所属组织不能为空");
    }
    OrganizationVo organizationVo = organizationEntityService.findDetailsById(twBaseEntity.getBaseOrg().getId());
    twBaseEntity.setCode(organizationVo.getCode());
    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    //校验是否已存在该基地
    if(!CollectionUtils.isEmpty(this.twBaseEntityRepository.findDetailsByBaseOrgId(twBaseEntity.getBaseOrg().getId()))){
      throw new IllegalArgumentException ("已存在该基地，不能重复添加");
    }
    this.twBaseEntityRepository.saveAndFlush(twBaseEntity);

    // 返回最终处理的结果，里面带有详细的关联信息
    return twBaseEntity;
  }
  /**
   * 在创建一个新的TwBaseEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(TwBaseEntity twBaseEntity) {
    Validate.notNull(twBaseEntity , "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(twBaseEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    twBaseEntity.setId(null);
    Validate.notBlank(twBaseEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(twBaseEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(twBaseEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(twBaseEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notNull(twBaseEntity.getAreaSize(), "添加信息时，面积不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(twBaseEntity.getFormInstanceId() == null || twBaseEntity.getFormInstanceId().length() < 255 , "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(twBaseEntity.getCreateName() == null || twBaseEntity.getCreateName().length() < 255 , "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(twBaseEntity.getCreatePosition() == null || twBaseEntity.getCreatePosition().length() < 255 , "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(twBaseEntity.getModifyName() == null || twBaseEntity.getModifyName().length() < 255 , "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(twBaseEntity.getModifyPosition() == null || twBaseEntity.getModifyPosition().length() < 255 , "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(twBaseEntity.getAddress() == null || twBaseEntity.getAddress().length() < 255 , "地址,在进行添加时填入值超过了限定长度(255)，请检查!");
    TwBaseEntity currentTwBaseEntity = this.findByFormInstanceId(twBaseEntity.getFormInstanceId());
    Validate.isTrue(currentTwBaseEntity == null, "表单实例编号已存在,请检查");
    // 验证ManyToOne关联：所属基地绑定值的正确性
    OrganizationEntity currentBaseOrg = twBaseEntity.getBaseOrg();
    Validate.notNull(currentBaseOrg , "所属分公司信息必须传入，请检查!!");
    String currentPkBaseOrg = currentBaseOrg.getId();
    Validate.notBlank(currentPkBaseOrg, "创建操作时，当前所属基地信息必须关联！");
    Validate.notNull(this.organizationEntityService.findDetailsById(currentPkBaseOrg) , "所属基地关联信息未找到，请检查!!");
  }
  @Transactional
  @Override
  public TwBaseEntity update(TwBaseEntity twBaseEntity) {
    return this.updateForm(twBaseEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  } 
  @Transactional
  @Override
  public TwBaseEntity updateForm(TwBaseEntity twBaseEntity) {
    /* 
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     * 
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    
    this.updateValidation(twBaseEntity);
    // ===================基本信息
    String currentId = twBaseEntity.getId();
    Optional<TwBaseEntity> opCurrentTwBaseEntity = this.twBaseEntityRepository.findById(currentId);
    TwBaseEntity currentTwBaseEntity = opCurrentTwBaseEntity.orElse(null);
    Validate.notNull(currentTwBaseEntity ,"未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentTwBaseEntity.setFormInstanceId(twBaseEntity.getFormInstanceId());
    currentTwBaseEntity.setCreateTime(twBaseEntity.getCreateTime());
    currentTwBaseEntity.setCreateName(twBaseEntity.getCreateName());
    currentTwBaseEntity.setCreatePosition(twBaseEntity.getCreatePosition());
    currentTwBaseEntity.setModifyTime(twBaseEntity.getModifyTime());
    currentTwBaseEntity.setModifyName(twBaseEntity.getModifyName());
    currentTwBaseEntity.setModifyPosition(twBaseEntity.getModifyPosition());
    currentTwBaseEntity.setAreaSize(twBaseEntity.getAreaSize());
    currentTwBaseEntity.setAddress(twBaseEntity.getAddress());
    currentTwBaseEntity.setEnableState(twBaseEntity.getEnableState());
    currentTwBaseEntity.setBaseOrg(twBaseEntity.getBaseOrg());
    currentTwBaseEntity.setLeader(twBaseEntity.getLeader());
    currentTwBaseEntity.setProvince(twBaseEntity.getProvince());
    currentTwBaseEntity.setCity(twBaseEntity.getCity());
    currentTwBaseEntity.setArea(twBaseEntity.getArea());
    currentTwBaseEntity.setName(twBaseEntity.getName());
    currentTwBaseEntity.setAbbreviation(twBaseEntity.getAbbreviation());


    //校验所属基地是否改变
    TwBaseEntity beforTwBaseEntity = twBaseEntityRepository.findDetailsById(twBaseEntity.getId());
    if(!beforTwBaseEntity.getBaseOrg().getId().equals(currentTwBaseEntity.getBaseOrg().getId())){
      throw new IllegalArgumentException("所属基地不能修改");
    }
    this.twBaseEntityRepository.saveAndFlush(currentTwBaseEntity);
    return currentTwBaseEntity;
  }
  /**
   * 在更新一个已有的TwBaseEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(TwBaseEntity twBaseEntity) {
    Validate.isTrue(!StringUtils.isBlank(twBaseEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
    
    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(twBaseEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(twBaseEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(twBaseEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(twBaseEntity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    Validate.notNull(twBaseEntity.getAreaSize(), "修改信息时，面积不能为空！");
    
    // 重复性判断，基本属性，需要满足unique = true
    TwBaseEntity currentForFormInstanceId = this.findByFormInstanceId(twBaseEntity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId() , twBaseEntity.getId()) , "表单实例编号已存在,请检查"); 
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(twBaseEntity.getFormInstanceId() == null || twBaseEntity.getFormInstanceId().length() < 255 , "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(twBaseEntity.getCreateName() == null || twBaseEntity.getCreateName().length() < 255 , "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(twBaseEntity.getCreatePosition() == null || twBaseEntity.getCreatePosition().length() < 255 , "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(twBaseEntity.getModifyName() == null || twBaseEntity.getModifyName().length() < 255 , "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(twBaseEntity.getModifyPosition() == null || twBaseEntity.getModifyPosition().length() < 255 , "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(twBaseEntity.getAddress() == null || twBaseEntity.getAddress().length() < 255 , "地址,在进行修改时填入值超过了限定长度(255)，请检查!");
    
    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
    OrganizationEntity currentForBaseOrg = twBaseEntity.getBaseOrg();
    Validate.notNull(currentForBaseOrg , "修改信息时，所属基地必须传入，请检查!!");
  } 
  @Override
  public TwBaseEntity findDetailsById(String id) {
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(id)) { 
      return null; 
    } 
    TwBaseEntity current = this.twBaseEntityRepository.findDetailsById(id);
    if(current == null) {
      return null;
    } 
    String currentPkValue = current.getId();
    // 2、======
    // 塘口 的明细信息查询
    Collection<PondEntity> oneToManyPondsDetails = this.pondEntityService.findDetailsByBase(currentPkValue);
    current.setPonds(Sets.newLinkedHashSet(oneToManyPondsDetails));
    
    return current;
  }
  @Override
  public TwBaseEntity findById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    
    Optional<TwBaseEntity> op = twBaseEntityRepository.findById(id);
    return op.orElse(null); 
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id , "进行删除时，必须给定主键信息!!");
    TwBaseEntity current = this.findById(id);
    if(current != null) { 
      this.twBaseEntityRepository.delete(current);
    }
  }
  @Override
  public TwBaseEntity findDetailsByFormInstanceId(String formInstanceId) {
    /* 
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息 
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */
    
    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(formInstanceId)) { 
      return null; 
    } 
    TwBaseEntity current = this.twBaseEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if(current == null) {
      return null;
    } 
    String currentPkValue = current.getId();
    // 2、======
    // 塘口 的明细信息查询
    Collection<PondEntity> oneToManyPondsDetails = this.pondEntityService.findDetailsByBase(currentPkValue);
    current.setPonds(Sets.newLinkedHashSet(oneToManyPondsDetails));
    
    return current;
  }
  @Override
  public TwBaseEntity findByFormInstanceId(String formInstanceId) {
    if(StringUtils.isBlank(formInstanceId)) { 
      return null;
    }
    return this.twBaseEntityRepository.findByFormInstanceId(formInstanceId);
  }

  @Override
  public List<TwBaseEntity> findDetailAll() {

    return twBaseEntityRepository.findDetailAll();
  }

  @Override
  public TwBaseEntity findDetailsByBaseOrgId(String baseOrgId) {
    if(StringUtils.isBlank(baseOrgId)){
      return null;
    }
    List<TwBaseEntity> twBaseEntities = twBaseEntityRepository.findDetailsByBaseOrgId(baseOrgId);
    if(CollectionUtils.isEmpty(twBaseEntities)){
      return null;
    }
    return twBaseEntities.get(twBaseEntities.size()-1);
  }

  @Override
  public Page<TwBaseEntity> findByConditions(Pageable pageable, Map<String, Object> params) {
    return twBaseEntityRepository.findByConditions(pageable, params);
  }

  @Override
  public List<TwBaseEntity> findAll() {

    return twBaseEntityRepository.findAll();
  }
} 
