package com.tw.aquaculture.process.service.internal;

import com.bizunited.platform.rbac.server.service.UserService;
import com.bizunited.platform.rbac.server.vo.UserVo;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.entity.process.BaskPondEntity;
import com.tw.aquaculture.common.entity.process.SafeguardEntity;
import com.tw.aquaculture.common.entity.process.WorkOrderEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.enums.PondStateEnum;
import com.tw.aquaculture.common.utils.DateUtils;
import com.tw.aquaculture.process.repository.WorkOrderEntityRepository;
import com.tw.aquaculture.process.service.BaskPondEntityService;
import com.tw.aquaculture.process.service.PondEntityService;
import com.tw.aquaculture.process.service.SafeguardEntityService;
import com.tw.aquaculture.process.service.WorkOrderEntityService;
import com.tw.aquaculture.process.service.base.CodeGenerateService;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.*;

/**
 * WorkOrderEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("WorkOrderEntityServiceImpl")
public class WorkOrderEntityServiceImpl implements WorkOrderEntityService {
  @Autowired
  private PondEntityService pondEntityService;
  @Autowired
  private WorkOrderEntityRepository workOrderEntityRepository;
  @Autowired
  private CodeGenerateService codeGenerateService;
  @Autowired
  private UserService userService;
  @Autowired
  private BaskPondEntityService baskPondEntityService;
  @Autowired
  private SafeguardEntityService safeguardEntityService;

  @Transactional
  @Override
  public WorkOrderEntity create(WorkOrderEntity workOrderEntity) {
    return this.createForm(workOrderEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  }

  @Transactional
  @Override
  public WorkOrderEntity createForm(WorkOrderEntity workOrderEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *   2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    workOrderEntity.setCode(codeGenerateService.generateBusinessCode(CodeTypeEnum.WORK_ORDER_ENTITY));
    Validate.notNull(workOrderEntity.getCode(), "编码不能为空");
    workOrderEntity.setEnableState(EnableStateEnum.DISABLE.getState());
    if(workOrderEntity.getCreateTime()==null){
      workOrderEntity.setCreateTime(new Date());
    }
    String account = SecurityContextHolder.getContext().getAuthentication().getName();
    Validate.notNull(account, "当前登录账号已过期，请重新登录");
    UserVo userVo = userService.findByAccount(account);
    if(userVo!=null){
      userVo = userService.findDetailsById(userVo.getId());
      workOrderEntity.setCreateName(userVo.getUserName());
      String positionName = null;
      if (!CollectionUtils.isEmpty(userVo.getPositions())) {
        positionName = userVo.getPositions().iterator().next().getName();
        workOrderEntity.setCreatePosition(positionName);
      } else {
        workOrderEntity.setCreatePosition("暂无");
      }
    }
    this.createValidation(workOrderEntity);

    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    List<WorkOrderEntity> oldWorks = workOrderEntityRepository.findByStateAndPond(workOrderEntity.getPond().getId(), EnableStateEnum.DISABLE.getState());
    if (!CollectionUtils.isEmpty(oldWorks)) {
      throw new IllegalArgumentException ("塘口还处于养殖状态，不能开启下一个养殖周期");
    }
    workOrderEntity = this.workOrderEntityRepository.saveAndFlush(workOrderEntity);
    //后绑定晒唐
    /*List<BaskPondEntity> baskPondEntities = baskPondEntityService.findNoneWorkOrderByPond(workOrderEntity.getPond().getId());
    if(!CollectionUtils.isEmpty(baskPondEntities)){
      for(BaskPondEntity bask : baskPondEntities){
        bask.setWorkOrder(workOrderEntity);
        this.baskPondEntityService.update(bask);
      }
      if(DateUtils.bigThan(workOrderEntity.getCreateTime(), baskPondEntities.get(0).getCreateTime())){
        workOrderEntity.setCreateTime(baskPondEntities.get(0).getCreateTime());
        workOrderEntity.setModifyTime(baskPondEntities.get(0).getCreateTime());
      }
    }*/
    //后绑定动保
    List<SafeguardEntity> safeguardEntities = safeguardEntityService.findNoneWorkOrderByPond(workOrderEntity.getPond().getId());
    if(!CollectionUtils.isEmpty(safeguardEntities)){
      for(SafeguardEntity safe : safeguardEntities){
        safe.setWorkOrder(workOrderEntity);
        safeguardEntityService.update(safe);
      }
      if(DateUtils.bigThan(workOrderEntity.getCreateTime(), safeguardEntities.get(0).getCreateTime())){
        workOrderEntity.setCreateTime(safeguardEntities.get(0).getCreateTime());
        workOrderEntity.setModifyTime(safeguardEntities.get(0).getCreateTime());
      }
    }
    this.workOrderEntityRepository.saveAndFlush(workOrderEntity);
    /**
     * 注水之后将塘口状态改为正常
     */
    PondEntity pondEntity = pondEntityService.findDetailsById(workOrderEntity.getPond().getId());
    pondEntity.setState(PondStateEnum.ZHENG_CHANG.getState());
    pondEntityService.update(pondEntity);


    // 返回最终处理的结果，里面带有详细的关联信息
    return workOrderEntity;
  }

  /**
   * 在创建一个新的WorkOrderEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(WorkOrderEntity workOrderEntity) {
    Validate.notNull(workOrderEntity, "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(workOrderEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    workOrderEntity.setId(null);
    Validate.notBlank(workOrderEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(workOrderEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(workOrderEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(workOrderEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notBlank(workOrderEntity.getCode(), "添加信息时，工单编号不能为空！");
    Validate.notNull(workOrderEntity.getEnableState(), "添加信息时，生效状态不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(workOrderEntity.getFormInstanceId() == null || workOrderEntity.getFormInstanceId().length() < 255, "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(workOrderEntity.getCreateName() == null || workOrderEntity.getCreateName().length() < 255, "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(workOrderEntity.getCreatePosition() == null || workOrderEntity.getCreatePosition().length() < 255, "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(workOrderEntity.getModifyName() == null || workOrderEntity.getModifyName().length() < 255, "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(workOrderEntity.getModifyPosition() == null || workOrderEntity.getModifyPosition().length() < 255, "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(workOrderEntity.getCode() == null || workOrderEntity.getCode().length() < 255, "工单编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    WorkOrderEntity currentWorkOrderEntity = this.findByFormInstanceId(workOrderEntity.getFormInstanceId());
    Validate.isTrue(currentWorkOrderEntity == null, "表单实例编号已存在,请检查");
    // 验证ManyToOne关联：塘口绑定值的正确性
    PondEntity currentPond = workOrderEntity.getPond();
    Validate.notNull(currentPond, "塘口信息必须传入，请检查!!");
    String currentPkPond = currentPond.getId();
    Validate.notBlank(currentPkPond, "创建操作时，当前塘口信息必须关联！");
    Validate.notNull(this.pondEntityService.findById(currentPkPond), "塘口关联信息未找到，请检查!!");
  }

  @Transactional
  @Override
  public WorkOrderEntity update(WorkOrderEntity workOrderEntity) {
    return this.updateForm(workOrderEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
  }

  @Transactional
  @Override
  public WorkOrderEntity updateForm(WorkOrderEntity workOrderEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     *
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */

    this.updateValidation(workOrderEntity);
    // ===================基本信息
    String currentId = workOrderEntity.getId();
    Optional<WorkOrderEntity> opCurrentWorkOrderEntity = this.workOrderEntityRepository.findById(currentId);
    WorkOrderEntity currentWorkOrderEntity = opCurrentWorkOrderEntity.orElse(null);
    Validate.notNull(currentWorkOrderEntity, "未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentWorkOrderEntity.setFormInstanceId(workOrderEntity.getFormInstanceId());
    currentWorkOrderEntity.setCreateTime(workOrderEntity.getCreateTime());
    currentWorkOrderEntity.setCreateName(workOrderEntity.getCreateName());
    currentWorkOrderEntity.setCreatePosition(workOrderEntity.getCreatePosition());
    currentWorkOrderEntity.setModifyTime(workOrderEntity.getModifyTime());
    currentWorkOrderEntity.setModifyName(workOrderEntity.getModifyName());
    currentWorkOrderEntity.setModifyPosition(workOrderEntity.getModifyPosition());
    currentWorkOrderEntity.setCode(workOrderEntity.getCode());
    currentWorkOrderEntity.setEnableState(workOrderEntity.getEnableState());
    currentWorkOrderEntity.setFinishTime(workOrderEntity.getFinishTime());
    currentWorkOrderEntity.setPond(workOrderEntity.getPond());

    this.workOrderEntityRepository.saveAndFlush(currentWorkOrderEntity);
    return currentWorkOrderEntity;
  }

  /**
   * 在更新一个已有的WorkOrderEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(WorkOrderEntity workOrderEntity) {
    Validate.isTrue(!StringUtils.isBlank(workOrderEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");

    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(workOrderEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(workOrderEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(workOrderEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(workOrderEntity.getCode(), "修改信息时，工单编号不能为空！");
    Validate.notNull(workOrderEntity.getEnableState(), "修改信息时，生效状态不能为空！");

    // 重复性判断，基本属性，需要满足unique = true
    WorkOrderEntity currentForFormInstanceId = this.findByFormInstanceId(workOrderEntity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId(), workOrderEntity.getId()), "表单实例编号已存在,请检查");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(workOrderEntity.getFormInstanceId() == null || workOrderEntity.getFormInstanceId().length() < 255, "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(workOrderEntity.getCreateName() == null || workOrderEntity.getCreateName().length() < 255, "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(workOrderEntity.getCreatePosition() == null || workOrderEntity.getCreatePosition().length() < 255, "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(workOrderEntity.getModifyName() == null || workOrderEntity.getModifyName().length() < 255, "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(workOrderEntity.getModifyPosition() == null || workOrderEntity.getModifyPosition().length() < 255, "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(workOrderEntity.getCode() == null || workOrderEntity.getCode().length() < 255, "工单编号,在进行修改时填入值超过了限定长度(255)，请检查!");

    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
    PondEntity currentForPond = workOrderEntity.getPond();
    Validate.notNull(currentForPond, "修改信息时，塘口必须传入，请检查!!");
  }

  @Override
  public WorkOrderEntity findDetailsById(String id) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(id)) {
      return null;
    }
    WorkOrderEntity current = this.workOrderEntityRepository.findDetailsById(id);
    if (current == null) {
      return null;
    }
    return current;
  }

  @Override
  public WorkOrderEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }

    Optional<WorkOrderEntity> op = workOrderEntityRepository.findById(id);
    return op.orElse(null);
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    WorkOrderEntity current = this.findById(id);
    if (current != null) {
      this.workOrderEntityRepository.delete(current);
    }
  }

  @Override
  public WorkOrderEntity findDetailsByFormInstanceId(String formInstanceId) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    WorkOrderEntity current = this.workOrderEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if (current == null) {
      return null;
    }
    return current;
  }

  @Override
  public WorkOrderEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    return this.workOrderEntityRepository.findByFormInstanceId(formInstanceId);
  }

  @Override
  public Page<WorkOrderEntity> findByConditions(Pageable pageable, Map<String, Object> params) {

    return workOrderEntityRepository.findByConditions(pageable, params);
  }

  @Override
  public List<WorkOrderEntity> findByPondAndTime(String pondId, Date createTime) {
    if (StringUtils.isBlank(pondId) || createTime == null) {
      return Collections.emptyList();
    }
    return workOrderEntityRepository.findByPondAndTime(pondId, createTime);
  }

  @Override
  public void save(WorkOrderEntity w) {
    if (w == null) {
      return;
    }
    workOrderEntityRepository.saveAndFlush(w);
  }

  @Override
  public List<WorkOrderEntity> findByStateAndPond(String pondId, int state) {
    if (pondId == null) {
      return Collections.emptyList();
    }
    return workOrderEntityRepository.findByStateAndPond(pondId, state);
  }

  @Override
  public List<WorkOrderEntity> findByPondAndTimeAndState(String pondId, Date endTime, Integer state) {
    if(StringUtils.isBlank(pondId) || endTime==null || state==null){
      return null;
    }
    return this.workOrderEntityRepository.findByPondAndTimeAndState(pondId, endTime, state);
  }
} 
