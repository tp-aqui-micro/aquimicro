package com.tw.aquaculture.store.configaration.security;

import java.util.Collection;

import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * 访问权限判定逻辑
 * @author yinwenjie
 */
@Component("customAccessDecisionManager")
public class CustomAccessDecisionManager implements AccessDecisionManager { 
  /*
   * (non-Javadoc)
   * 
   * @see org.springframework.security.access.AccessDecisionManager#decide(org.springframework.security.core.Authentication, java.lang.Object, java.util.Collection)
   */
  @Override
  public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes)
      throws AccessDeniedException, InsufficientAuthenticationException {
    /*
     * 这里只需要return即可，也就是说任何接口，对于任何角色都放行
     * */
    return;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.springframework.security.access.AccessDecisionManager#supports(org.springframework.security
   * .access.ConfigAttribute)
   */
  @Override
  public boolean supports(ConfigAttribute attribute) {
    return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.springframework.security.access.AccessDecisionManager#supports(java.lang.Class)
   */
  @Override
  public boolean supports(Class<?> clazz) {
    return true;
  }
}