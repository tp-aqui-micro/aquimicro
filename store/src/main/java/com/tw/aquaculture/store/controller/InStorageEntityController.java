package com.tw.aquaculture.store.controller;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.store.InStorageEntity;
import com.tw.aquaculture.store.service.InStorageEntityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * InStorageEntity业务模型的MVC Controller层实现，基于HTTP Restful风格
 *
 * @author saturn
 */
@RestController
@RequestMapping("/v1/inStorageEntitys")
public class InStorageEntityController extends BaseController {
  /** 日志 */
  private static final Logger LOGGER = LoggerFactory.getLogger(InStorageEntityController.class);

  @Autowired private InStorageEntityService inStorageEntityService;

  /**
   * 相关的创建过程，http接口。请注意该创建过程除了可以创建inStorageEntity中的基本信息以外，还可以对inStorageEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（InStorageEntity）模型的创建操作传入的inStorageEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(
      value =
          "相关的创建过程，http接口。请注意该创建过程除了可以创建inStorageEntity中的基本信息以外，还可以对inStorageEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（InStorageEntity）模型的创建操作传入的inStorageEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "")
  public ResponseModel create(
      @RequestBody
          @ApiParam(
              name = "inStorageEntity",
              value =
                  "相关的创建过程，http接口。请注意该创建过程除了可以创建inStorageEntity中的基本信息以外，还可以对inStorageEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（InStorageEntity）模型的创建操作传入的inStorageEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
          InStorageEntity inStorageEntity) {
    try {
      InStorageEntity current = this.inStorageEntityService.create(inStorageEntity);
      return this.buildHttpResultW(current, new String[] {});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（InStorageEntity）的修改操作传入的inStorageEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(
      value =
          "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（InStorageEntity）的修改操作传入的inStorageEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "update")
  public ResponseModel update(
      @RequestBody
          @ApiParam(
              name = "inStorageEntity",
              value =
                  "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（InStorageEntity）的修改操作传入的inStorageEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
          InStorageEntity inStorageEntity) {
    try {
      InStorageEntity current = this.inStorageEntityService.update(inStorageEntity);
      return this.buildHttpResultW(current, new String[] {});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照InStorageEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照InStorageEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @RequestMapping(
      value = "/findDetailsById",
      method = {RequestMethod.GET})
  public ResponseModel findDetailsById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      InStorageEntity result = this.inStorageEntityService.findDetailsById(id);
      return this.buildHttpResultW(
          result, new String[] {"matter", "store", "createUser", "twBase"});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }
  /**
   * 按照InStorageEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param formInstanceId 表单实例编号
   */
  @ApiOperation(value = "按照InStorageEntity实体中的（formInstanceId）表单实例编号进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @RequestMapping(
      value = "/findDetailsByFormInstanceId",
      method = {RequestMethod.GET})
  public ResponseModel findDetailsByFormInstanceId(
      @RequestParam("formInstanceId") @ApiParam("表单实例编号") String formInstanceId) {
    try {
      InStorageEntity result =
          this.inStorageEntityService.findDetailsByFormInstanceId(formInstanceId);
      return this.buildHttpResultW(
          result, new String[] {"matter", "store", "createUser", "twBase", "twBase.baseOrg"});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }
  /**
   * 按照InStorageEntity实体中的（formInstanceId）表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @ApiOperation(value = "按照InStorageEntity实体中的（formInstanceId）表单实例编号进行查询")
  @RequestMapping(
      value = "/findByFormInstanceId",
      method = {RequestMethod.GET})
  public ResponseModel findByFormInstanceId(
      @RequestParam("formInstanceId") @ApiParam("表单实例编号") String formInstanceId) {
    try {
      InStorageEntity result = this.inStorageEntityService.findByFormInstanceId(formInstanceId);
      return this.buildHttpResultW(result, new String[] {});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据id查询")
  @RequestMapping(value = "/findById", method = {RequestMethod.GET})
  public ResponseModel findById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      InStorageEntity result = this.inStorageEntityService.findById(id);
      return this.buildHttpResultW(result, new String[]{});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }
}
