package com.tw.aquaculture.store.controller;

import com.bizunited.platform.core.controller.BaseController;
import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.store.StockEntity;
import com.tw.aquaculture.store.service.StockEntityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

/**
 * StockEntity业务模型的MVC Controller层实现，基于HTTP Restful风格
 *
 * @author saturn
 */
@RestController
@RequestMapping("/v1/stockEntitys")
public class StockEntityController extends BaseController {
  /** 日志 */
  private static final Logger LOGGER = LoggerFactory.getLogger(StockEntityController.class);

  @Autowired private StockEntityService stockEntityService;

  /**
   * 相关的创建过程，http接口。请注意该创建过程除了可以创建stockEntity中的基本信息以外，还可以对stockEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（StockEntity）模型的创建操作传入的stockEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(
      value =
          "相关的创建过程，http接口。请注意该创建过程除了可以创建stockEntity中的基本信息以外，还可以对stockEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（StockEntity）模型的创建操作传入的stockEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "")
  public ResponseModel create(
      @RequestBody
          @ApiParam(
              name = "stockEntity",
              value =
                  "相关的创建过程，http接口。请注意该创建过程除了可以创建stockEntity中的基本信息以外，还可以对stockEntity中属于OneToMany关联的明细信息一同进行创建注意：基于（StockEntity）模型的创建操作传入的stockEntityJSON对象，其主键信息不能有值，服务端将会自动为其赋予相关值。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
          StockEntity stockEntity) {
    try {
      StockEntity current = this.stockEntityService.create(stockEntity);
      return this.buildHttpResultW(current, new String[] {});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（StockEntity）的修改操作传入的stockEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）
   */
  @ApiOperation(
      value =
          "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（StockEntity）的修改操作传入的stockEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
  @PostMapping(value = "/update")
  public ResponseModel update(
      @RequestBody
          @ApiParam(
              name = "stockEntity",
              value =
                  "相关的更新过程，http接口。请注意该更新过程只会更新在模型层被标记为了updateable的属性，包括一般属性、ManyToOne和OneToOne性质的关联属性，而ManyToMany、OneToMany的关联属性，虽然也会传入，但需要开发人员自行在Service层完善其更新过程注意：基于模型（StockEntity）的修改操作传入的stockEntityJSON对象，其主键信息必须有值，服务端将验证这个主键值是否已经存在。另外，创建操作成功后，系统将返回该对象的基本信息（不包括任何关联信息）")
          StockEntity stockEntity) {
    try {
      StockEntity current = this.stockEntityService.update(stockEntity);
      return this.buildHttpResultW(current, new String[] {});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  /**
   * 按照StockEntity实体中的（base）关联的 基地进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param base 关联的 基地
   */
  @ApiOperation(value = "按照StockEntity实体中的（base）关联的 基地进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @RequestMapping(
      value = "/findDetailsByBase",
      method = {RequestMethod.GET})
  public ResponseModel findDetailsByBase(@RequestParam("base") @ApiParam("关联的 基地") String base) {
    try {
      Set<StockEntity> result = this.stockEntityService.findDetailsByBase(base);
      return this.buildHttpResultW(result, new String[] {"base", "store", "matter"});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }
  /**
   * 按照StockEntity实体中的（store）关联的 仓库进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param store 关联的 仓库
   */
  @ApiOperation(value = "按照StockEntity实体中的（store）关联的 仓库进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @RequestMapping(
      value = "/findDetailsByStore",
      method = {RequestMethod.GET})
  public ResponseModel findDetailsByStore(@RequestParam("store") @ApiParam("关联的 仓库") String store) {
    try {
      Set<StockEntity> result = this.stockEntityService.findDetailsByStore(store);
      return this.buildHttpResultW(result, new String[] {"base", "store", "matter"});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }
  /**
   * 按照StockEntity实体中的（matter）关联的 物料进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param matter 关联的 物料
   */
  @ApiOperation(value = "按照StockEntity实体中的（matter）关联的 物料进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @RequestMapping(
      value = "/findDetailsByMatter",
      method = {RequestMethod.GET})
  public ResponseModel findDetailsByMatter(
      @RequestParam("matter") @ApiParam("关联的 物料") String matter) {
    try {
      Set<StockEntity> result = this.stockEntityService.findDetailsByMatter(matter);
      return this.buildHttpResultW(result, new String[] {"base", "store", "matter"});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }
  @RequestMapping(
          value = "/findByMatterAndStore",
          method = {RequestMethod.GET})
  public ResponseModel findByMatterAndStore(
          @RequestParam("matter") @ApiParam("关联的 物料") String matter, @RequestParam("storeId") String storeId) {
    try {
      StockEntity result = this.stockEntityService.findByMatterAndStore(matter, storeId);
      return this.buildHttpResultW(result, new String[] {"base", "store", "matter"});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }
  /**
   * 按照StockEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。
   *
   * @param id 主键
   */
  @ApiOperation(value = "按照StockEntity实体中的（id）主键进行查询明细查询，查询的明细包括当前业务表单所有的关联属性。")
  @RequestMapping(
      value = "/findDetailsById",
      method = {RequestMethod.GET})
  public ResponseModel findDetailsById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      StockEntity result = this.stockEntityService.findDetailsById(id);
      return this.buildHttpResultW(result, new String[] {"base", "store", "matter"});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据id查询")
  @RequestMapping(value = "/findById", method = {RequestMethod.GET})
  public ResponseModel findById(@RequestParam("id") @ApiParam("主键") String id) {
    try {
      StockEntity result = this.stockEntityService.findById(id);
      return this.buildHttpResultW(result, new String[]{});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }

  @ApiOperation(value = "根据id查询")
  @RequestMapping(value = "/findByBaseId", method = {RequestMethod.GET})
  public ResponseModel findByBaseId(@RequestParam("baseId") @ApiParam("主键") String baseId) {
    try {
      List<StockEntity> result = this.stockEntityService.findByBaseId(baseId);
      return this.buildHttpResultW(result, new String[]{"matter", "store", "base"});
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return this.buildHttpResultForException(e);
    }
  }


}
