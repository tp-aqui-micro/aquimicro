package com.tw.aquaculture.store.feign.process;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.tw.aquaculture.common.entity.base.FishTypeEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 陈荣
 * @date 2019/12/4 14:37
 */
@FeignClient(qualifier = "FishTypeFeign", name = "${aquiServiceName.process}")
public interface FishTypeFeign {

  /**
   * 创建
   *
   * @param fishTypeEntity
   * @return
   */
  @PostMapping(value = "/v1/fishTypeEntitys")
  public ResponseModel create(@RequestBody FishTypeEntity fishTypeEntity);

  /**
   * 修改
   *
   * @param fishTypeEntity
   * @return
   */
  @PostMapping(value = "/v1/fishTypeEntitys/update")
  public ResponseModel update(@RequestBody FishTypeEntity fishTypeEntity);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/fishTypeEntitys/findDetailsById")
  public ResponseModel findDetailsById(@RequestParam("id") String id);

  /**
   * 根据id查询
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/fishTypeEntitys/findById")
  ResponseModel findById(@RequestParam("id") String id);

  /**
   * 格局表单实例编号查详情
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/fishTypeEntitys/findDetailsByFormInstanceId")
  public ResponseModel findDetailsByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据表单实例编号查询
   *
   * @param formInstanceId
   * @return
   */
  @GetMapping(value = "/v1/fishTypeEntitys/findByFormInstanceId")
  public ResponseModel findByFormInstanceId(@RequestParam("formInstanceId") String formInstanceId);

  /**
   * 根据id删除
   *
   * @param id
   * @return
   */
  @GetMapping(value = "/v1/fishTypeEntitys/deleteById")
  public ResponseModel deleteById(@RequestParam("id") String id);

  /**
   * 查询所有
   *
   * @return
   */
  @GetMapping(value = "/v1/fishTypeEntitys/findAll")
  public ResponseModel findAll();
}
