package com.tw.aquaculture.store.repository;

import com.tw.aquaculture.common.entity.store.InStorageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * InStorageEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_InStorageEntityRepository")
public interface InStorageEntityRepository
    extends JpaRepository<InStorageEntity, String>, JpaSpecificationExecutor<InStorageEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query(
      "select distinct inStorageEntity from InStorageEntity inStorageEntity "
          + " left join fetch inStorageEntity.twBase inStorageEntity_twBase "
          + " left join fetch inStorageEntity.store inStorageEntity_store "
          + " left join fetch inStorageEntity.matter inStorageEntity_matter "
          + " left join fetch inStorageEntity.createUser inStorageEntity_createUser "
          + " where inStorageEntity.id=:id ")
  public InStorageEntity findDetailsById(@Param("id") String id);
  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(
      "select distinct inStorageEntity from InStorageEntity inStorageEntity "
          + " left join fetch inStorageEntity.twBase inStorageEntity_twBase "
              + " left join fetch inStorageEntity_twBase.baseOrg baseOrg "
          + " left join fetch inStorageEntity.store inStorageEntity_store "
          + " left join fetch inStorageEntity.matter inStorageEntity_matter "
          + " left join fetch inStorageEntity.createUser inStorageEntity_createUser "
          + " where inStorageEntity.formInstanceId=:formInstanceId ")
  public InStorageEntity findDetailsByFormInstanceId(
      @Param("formInstanceId") String formInstanceId);
  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from InStorageEntity f where f.formInstanceId = :formInstanceId")
  public InStorageEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);
}
