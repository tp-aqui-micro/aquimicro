package com.tw.aquaculture.store.repository;

import com.tw.aquaculture.common.entity.base.MatterEntity;
import com.tw.aquaculture.store.repository.internal.MatterEntityRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * MatterEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_MatterEntityRepository")
public interface MatterEntityRepository
    extends JpaRepository<MatterEntity, String>,
        JpaSpecificationExecutor<MatterEntity>,
        MatterEntityRepositoryCustom {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query(
      "select distinct matterEntity from MatterEntity matterEntity "
          + " where matterEntity.id=:id ")
  public MatterEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(
      "select distinct matterEntity from MatterEntity matterEntity "
          + " where matterEntity.formInstanceId=:formInstanceId ")
  public MatterEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from MatterEntity f where f.formInstanceId = :formInstanceId")
  public MatterEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据编码查询
   *
   * @param code
   * @return
   */
  MatterEntity findByCode(String code);

  /**
   * 根据ebs物料编码和ebs组织编码查询
   * @param ebsCode
   * @param ebsOrgCode
   * @return
   */
  MatterEntity findByEbsCodeAndEbsOrgCode(String ebsCode, String ebsOrgCode);
}
