package com.tw.aquaculture.store.repository;

import com.tw.aquaculture.common.entity.store.StockEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * StockEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_StockEntityRepository")
public interface StockEntityRepository
    extends JpaRepository<StockEntity, String>, JpaSpecificationExecutor<StockEntity> {
  /**
   * 按照基地进行详情查询（包括关联信息）
   *
   * @param base 基地
   */
  @Query(
      "select distinct stockEntity from StockEntity stockEntity "
          + " left join fetch stockEntity.base stockEntity_base "
          + " left join fetch stockEntity.store stockEntity_store "
          + " left join fetch stockEntity.matter stockEntity_matter "
          + " where stockEntity_base.id = :id")
  public Set<StockEntity> findDetailsByBase(@Param("id") String id);

  /**
   * 按照仓库进行详情查询（包括关联信息）
   *
   * @param store 仓库
   */
  @Query(
      "select distinct stockEntity from StockEntity stockEntity "
          + " left join fetch stockEntity.base stockEntity_base "
              + " left join fetch stockEntity_base.baseOrg baseOrg "
          + " left join fetch stockEntity.store stockEntity_store "
          + " left join fetch stockEntity.matter stockEntity_matter "
          + " where stockEntity_store.id = :id")
  public Set<StockEntity> findDetailsByStore(@Param("id") String id);

  /**
   * 按照物料进行详情查询（包括关联信息）
   *
   * @param matter 物料
   */
  @Query(
      "select distinct stockEntity from StockEntity stockEntity "
          + " left join fetch stockEntity.base stockEntity_base "
          + " left join fetch stockEntity.store stockEntity_store "
          + " left join fetch stockEntity.matter stockEntity_matter "
          + " where stockEntity_matter.id = :id")
  public Set<StockEntity> findDetailsByMatter(@Param("id") String id);

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query(
      "select distinct stockEntity from StockEntity stockEntity "
          + " left join fetch stockEntity.base stockEntity_base "
          + " left join fetch stockEntity.store stockEntity_store "
          + " left join fetch stockEntity.matter stockEntity_matter "
          + " where stockEntity.id=:id ")
  public StockEntity findDetailsById(@Param("id") String id);

  /**
   * 根据仓库、仓库和规格查询
   *
   * @param storeId
   * @param matterId
   * @return
   */
  @Query(
      "select distinct stockEntity from StockEntity stockEntity "
          + " left join fetch stockEntity.base stockEntity_base "
          + " left join fetch stockEntity.store stockEntity_store "
          + " left join fetch stockEntity.matter stockEntity_matter "
          + " where stockEntity_store.id=:storeId and stockEntity_matter.id=:matterId")
  StockEntity findByMatterAndStore(@Param("matterId") String matterId,@Param("storeId") String storeId);

  /**
   * 根据基地id查询
   *
   * @param baseId
   * @return
   */
  @Query(
      "select distinct stockEntity from StockEntity stockEntity "
          + " left join fetch stockEntity.base stockEntity_base "
          + " left join fetch stockEntity.store stockEntity_store "
          + " left join fetch stockEntity.matter stockEntity_matter "
          + " where stockEntity_base.id=:baseId ")
  List<StockEntity> findByBaseId(@Param("baseId") String baseId);

  /**
   * 根据物料查询库存
   * @param matterId
   * @return
   */
  @Query("select distinct stockEntity from StockEntity stockEntity "
          + " left join fetch stockEntity.base stockEntity_base "
          + " left join fetch stockEntity.store stockEntity_store "
          + " left join fetch stockEntity.matter stockEntity_matter "
          + " where stockEntity_matter.id=:matterId")
  StockEntity findByMatter(@Param("matterId") String matterId);
}
