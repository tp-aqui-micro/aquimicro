package com.tw.aquaculture.store.repository;

import com.tw.aquaculture.common.entity.store.StockOutEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * StockOutEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_StockOutEntityRepository")
public interface StockOutEntityRepository
    extends
    JpaRepository<StockOutEntity, String>
    , JpaSpecificationExecutor<StockOutEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query("select distinct stockOutEntity from StockOutEntity stockOutEntity "
      + " left join fetch stockOutEntity.twBase stockOutEntity_twBase "
      + " left join fetch stockOutEntity.store stockOutEntity_store "
      + " left join fetch stockOutEntity.matter stockOutEntity_matter "
      + " left join fetch stockOutEntity.pond stockOutEntity_pond "
      + " left join fetch stockOutEntity.receiver stockOutEntity_receiver "
      + " where stockOutEntity.id=:id ")
  public StockOutEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query("select distinct stockOutEntity from StockOutEntity stockOutEntity "
      + " left join fetch stockOutEntity.twBase stockOutEntity_twBase "
          + " left join fetch stockOutEntity_twBase.baseOrg baseOrg "
      + " left join fetch stockOutEntity.store stockOutEntity_store "
      + " left join fetch stockOutEntity.matter stockOutEntity_matter "
      + " left join fetch stockOutEntity.pond stockOutEntity_pond "
      + " left join fetch stockOutEntity.receiver stockOutEntity_receiver "
      + " where stockOutEntity.formInstanceId=:formInstanceId ")
  public StockOutEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from StockOutEntity f where f.formInstanceId = :formInstanceId")
  public StockOutEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据基地和时间范围查询出库
   * @param baseId
   * @param start
   * @param end
   * @return
   */
  @Query("select distinct stockOutEntity from StockOutEntity stockOutEntity "
      + " left join fetch stockOutEntity.twBase stockOutEntity_twBase "
      + " left join fetch stockOutEntity.matter stockOutEntity_matter "
      + " where stockOutEntity_twBase.id=:baseId and stockOutEntity.createTime>=:start and stockOutEntity.createTime<:end  ")
  List<StockOutEntity> findByBaseAndTimes(@Param("baseId") String baseId, @Param("start") Date start, @Param("end") Date end);

  /**
   * 统计相同物料在某时间段内消耗量
   * @param baseId
   * @param matterId
   * @param start
   * @param end
   * @return
   */
  @Query("select sum(stockOutEntity.quantity) from StockOutEntity stockOutEntity "
      + " left join stockOutEntity.twBase stockOutEntity_twBase "
      + " left join stockOutEntity.matter stockOutEntity_matter "
      + " where stockOutEntity_twBase.id=:baseId and stockOutEntity_matter.id=:matterId and stockOutEntity.createTime>=:start and stockOutEntity.createTime<:end  ")
  Double sumConsumeByTimes(@Param("baseId") String baseId, @Param("matterId") String matterId, @Param("start") Date start, @Param("end") Date end);
}