package com.tw.aquaculture.store.repository;

import com.tw.aquaculture.common.entity.base.StoreEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * StoreEntity业务模型的数据库方法支持
 *
 * @author saturn
 */
@Repository("_StoreEntityRepository")
public interface StoreEntityRepository
    extends JpaRepository<StoreEntity, String>, JpaSpecificationExecutor<StoreEntity> {

  /**
   * 按照主键进行详情查询（包括关联信息）
   *
   * @param id 主键
   */
  @Query(
      "select distinct storeEntity from StoreEntity storeEntity "
          + " left join fetch storeEntity.company storeEntity_company "
          + " left join fetch storeEntity.twBase storeEntity_twBase "
          + " left join fetch storeEntity.ponds ponds "
          + " where storeEntity.id=:id ")
  public StoreEntity findDetailsById(@Param("id") String id);

  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(
      "select distinct storeEntity from StoreEntity storeEntity "
          + " left join fetch storeEntity.company storeEntity_company "
          + " left join fetch storeEntity.twBase storeEntity_twBase "
              + " left join fetch storeEntity_twBase.baseOrg baseOrg "
          + " left join fetch storeEntity.ponds ponds "
          + " where storeEntity.formInstanceId=:formInstanceId ")
  public StoreEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 按照表单实例编号进行查询
   *
   * @param formInstanceId 表单实例编号
   */
  @Query(" from StoreEntity f where f.formInstanceId = :formInstanceId")
  public StoreEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);

  /**
   * 根据基地id查询
   *
   * @param baseId
   * @return
   */
  @Query(
      "select distinct storeEntity from StoreEntity storeEntity "
          + " left join fetch storeEntity.twBase storeEntity_twBase "
          + " where storeEntity_twBase.id=:baseId ")
  List<StoreEntity> findStoresByBaseId(@Param("baseId") String baseId);
}
