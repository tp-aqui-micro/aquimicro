package com.tw.aquaculture.store.repository;

import com.tw.aquaculture.common.entity.store.TransferItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * TransferItemEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_TransferItemEntityRepository")
public interface TransferItemEntityRepository
    extends
      JpaRepository<TransferItemEntity, String>
      ,JpaSpecificationExecutor<TransferItemEntity>
  {
  /**
   * 按照物料进行详情查询（包括关联信息）
   * @param matter 物料
   * */
  @Query("select distinct transferItemEntity from TransferItemEntity transferItemEntity "
      + " left join fetch transferItemEntity.matter transferItemEntity_matter "
      + " left join fetch transferItemEntity.transferStore transferItemEntity_transferStore "
       + " where transferItemEntity_matter.id = :id")
  public Set<TransferItemEntity> findDetailsByMatter(@Param("id") String id);
  /**
   * 按照转库进行详情查询（包括关联信息）
   * @param transferStore 转库
   * */
  @Query("select distinct transferItemEntity from TransferItemEntity transferItemEntity "
      + " left join fetch transferItemEntity.matter transferItemEntity_matter "
      + " left join fetch transferItemEntity.transferStore transferItemEntity_transferStore "
       + " where transferItemEntity_transferStore.id = :id")
  public Set<TransferItemEntity> findDetailsByTransferStore(@Param("id") String id);

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct transferItemEntity from TransferItemEntity transferItemEntity "
      + " left join fetch transferItemEntity.matter transferItemEntity_matter "
      + " left join fetch transferItemEntity.transferStore transferItemEntity_transferStore "
      + " where transferItemEntity.id=:id ")
  public TransferItemEntity findDetailsById(@Param("id") String id);



}