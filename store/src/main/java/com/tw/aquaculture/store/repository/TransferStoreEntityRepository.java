package com.tw.aquaculture.store.repository;

import com.tw.aquaculture.common.entity.store.TransferStoreEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * TransferStoreEntity业务模型的数据库方法支持
 * @author saturn
 */
@Repository("_TransferStoreEntityRepository")
public interface TransferStoreEntityRepository
    extends
      JpaRepository<TransferStoreEntity, String>
      ,JpaSpecificationExecutor<TransferStoreEntity>
  {

  /**
   * 按照主键进行详情查询（包括关联信息）
   * @param id 主键
   * */
  @Query("select distinct transferStoreEntity from TransferStoreEntity transferStoreEntity "
      + " left join fetch transferStoreEntity.outStore transferStoreEntity_outStore "
      + " left join fetch transferStoreEntity.inStore transferStoreEntity_inStore "
          + " left join fetch transferStoreEntity.transferItems transferItems "
      + " where transferStoreEntity.id=:id ")
  public TransferStoreEntity findDetailsById(@Param("id") String id);
  /**
   * 按照表单实例编号进行详情查询（包括关联信息）
   * @param formInstanceId 表单实例编号
   * */
  @Query("select distinct transferStoreEntity from TransferStoreEntity transferStoreEntity "
      + " left join fetch transferStoreEntity.outStore transferStoreEntity_outStore "
      + " left join fetch transferStoreEntity.inStore transferStoreEntity_inStore "
          + " left join fetch transferStoreEntity.transferItems transferItems "
      + " where transferStoreEntity.formInstanceId=:formInstanceId ")
  public TransferStoreEntity findDetailsByFormInstanceId(@Param("formInstanceId") String formInstanceId);
  /**
   * 按照表单实例编号进行查询
   * @param formInstanceId 表单实例编号
   * */
  @Query(" from TransferStoreEntity f where f.formInstanceId = :formInstanceId")
  public TransferStoreEntity findByFormInstanceId(@Param("formInstanceId") String formInstanceId);



}