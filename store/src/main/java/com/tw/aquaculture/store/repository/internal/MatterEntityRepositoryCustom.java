package com.tw.aquaculture.store.repository.internal;

import com.tw.aquaculture.common.entity.base.MatterEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * 基地信息自定义查询
 *
 * @author 陈荣
 * @date 2019/11/21 9:00
 */
public interface MatterEntityRepositoryCustom {
  /**
   * 条件分页查询基地
   *
   * @param pageable
   * @param params
   * @return
   */
  public Page<MatterEntity> findByMatterConditions(Pageable pageable, Map<String, Object> params);
}
