package com.tw.aquaculture.store.repository.internal;

import com.tw.aquaculture.common.entity.base.MatterEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author 陈荣
 * @date 2019/11/21 9:05
 */
@Repository("MatterEntityRepositoryImpl")
public class MatterEntityRepositoryImpl implements MatterEntityRepositoryCustom {

  @Autowired private EntityManager entityManager;

  @SuppressWarnings("unchecked")
  @Override
  public Page<MatterEntity> findByMatterConditions(Pageable pageable, Map<String, Object> params) {
    StringBuilder hql =
        new StringBuilder("select distinct matterEntity from MatterEntity matterEntity ");
    hql.append(" where 1=1 ");
    StringBuilder countHql =
        new StringBuilder("select count(distinct matterEntity) from MatterEntity matterEntity ");
    countHql.append(" where 1=1 ");

    StringBuilder condition = new StringBuilder();
    if (params != null) {
      if (params.get("matterTypeCode") != null) {
        condition.append(" and matterEntity.matterTypeCode = :matterTypeCode ");
      }
    }
    hql.append(condition.toString());
    // hql.append(condition.toString()).append(" order by pi.latestSubmitTime desc");
    countHql.append(condition.toString());
    Query query = entityManager.createQuery(hql.toString());
    Query countQuery = entityManager.createQuery(countHql.toString());
    if (params != null) {
      params.forEach(
          (k, v) -> {
            query.setParameter(k, v);
            countQuery.setParameter(k, v);
          });
    }
    query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
    query.setMaxResults(pageable.getPageSize());
    List<MatterEntity> result = new ArrayList<>();
    long count = (long) countQuery.getResultList().get(0);
    if (count > 0) {
      result = query.getResultList();
    }
    return new PageImpl<>(result, pageable, count);
  }
}
