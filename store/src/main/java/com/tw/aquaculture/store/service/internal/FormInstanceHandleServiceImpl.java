package com.tw.aquaculture.store.service.internal;

import com.alibaba.fastjson.JSONObject;
import com.bizunited.platform.kuiper.entity.InstanceEntity;
import com.bizunited.platform.kuiper.service.InstanceService;
import com.google.common.collect.Maps;
import com.tw.aquaculture.common.utils.BeanUtil;
import com.tw.aquaculture.common.utils.FieldUtils;
import com.tw.aquaculture.store.service.FormInstanceHandleService;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.security.Principal;
import java.util.Map;
import java.util.UUID;

/**
 * @author 陈荣
 * @date 2019/12/16 16:53
 */
@Service
public class FormInstanceHandleServiceImpl implements FormInstanceHandleService {

  private static final Logger LOGGER = LoggerFactory.getLogger(FormInstanceHandleServiceImpl.class);

  @Autowired
  private InstanceService instanceService;

  @Override
  public Object create(Object tEntity, String entityName, String serviceAndMethodName, boolean flag) {
    Object result = null;
    try {
      if(!flag){
        this.createRemoteForm(tEntity, entityName, serviceAndMethodName);
        String[] strs = serviceAndMethodName.split("\\.");
        Class<?> c = BeanUtil.getBean(strs[0]+"Impl").getClass();
        Method method = c.getDeclaredMethod("create", tEntity.getClass());
        method.setAccessible(true);
        result = method.invoke(BeanUtil.getBean(strs[0]+"Impl"), tEntity);
      }else{
        result = createRemoteForm(tEntity, entityName, serviceAndMethodName);
      }
    } catch (Exception e) {
      LOGGER.error(e.getMessage());
      throw new RuntimeException("创建实例失败:"+tEntity.getClass().getName()+"\n"+e.getMessage());
    }
    return result;
  }

  @Override
  public InstanceEntity createRemoteForm(Object tEntity, String entityName, String serviceAndMethodName) {
    String templateCode = (tEntity.getClass().getResource("").toString().substring(
            tEntity.getClass().getResource("").toString().lastIndexOf("com/")) + entityName)
            .replace("/", ".");
    Map<String , String> params = Maps.newHashMap();
    params.put("opType", serviceAndMethodName.substring(serviceAndMethodName.indexOf(".")+1));
    params.put("serviceName", serviceAndMethodName);

    return this.instanceService.create(templateCode, UUID.randomUUID().toString(), new UsernamePasswordAuthenticationToken("admin", ""));
  }

  /**
   * 获取登陆者账号对象
   *
   * @return
   */
  @Override
  public Principal getPrincipal() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    Validate.notNull(authentication, "未获取到当前系统的登录人");
    return authentication;
  }

  @Override
  public String create(Object tEntity, String entityName, String serviceAndMethodName) {
    InstanceEntity instance = this.createRemoteForm(tEntity, entityName, serviceAndMethodName);
    Validate.notNull(instance,"未能成功创建实例信息，请检查！！");
    return instance.getId();
  }

}
