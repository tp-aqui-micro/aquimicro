package com.tw.aquaculture.store.service.internal;

import com.tw.aquaculture.common.entity.base.MatterEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.store.repository.MatterEntityRepository;
import com.tw.aquaculture.store.service.MatterEntityService;
import com.tw.aquaculture.store.service.remote.CodeGenerateService;
import com.tw.aquaculture.store.service.remote.MatterRemoteService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * MatterEntity业务模型的服务层接口实现
 * @author saturn
 */
@Service("MatterEntityServiceImpl")
public class MatterEntityServiceImpl implements MatterEntityService {
  @Autowired
  private MatterEntityRepository matterEntityRepository;
  /**
   * Kuiper表单引擎用于减少编码工作量的工具包
   */
  @Autowired
  private MatterRemoteService matterRemoteService;
  @Autowired
  private CodeGenerateService codeGenerateService;

  @Transactional
  @Override
  public MatterEntity create(MatterEntity matterEntity) {
    MatterEntity current = this.createForm(matterEntity);
    //====================================================
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  }
  @Transactional
  @Override
  public MatterEntity createForm(MatterEntity matterEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *   2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    matterEntity.setCode(codeGenerateService.generateCode(CodeTypeEnum.MATTER_ENTITY));
    if(StringUtils.isBlank(matterEntity.getEbsCode())){
      matterEntity.setEbsCode("ebscode");
    }
    if(StringUtils.isBlank(matterEntity.getEbsOrgCode())){
      matterEntity.setEbsOrgCode(UUID.randomUUID().toString());
    }
    this.createValidation(matterEntity);

    // ===============================
    //  和业务有关的验证填写在这个区域
    // ===============================
    matterEntity.setCreateTime(new Date());
    this.matterEntityRepository.saveAndFlush(matterEntity);
    // 返回最终处理的结果，里面带有详细的关联信息
    return matterEntity;
  }
  /**
   * 在创建一个新的MatterEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(MatterEntity matterEntity) {
    Validate.notNull(matterEntity , "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(matterEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    matterEntity.setId(null);
    Validate.notBlank(matterEntity.getCode(), "添加信息时，物料编码不能为空！");
    Validate.notBlank(matterEntity.getName(), "添加信息时，物料名称不能为空！");
    Validate.notBlank(matterEntity.getMatterTypeCode(), "添加信息时，物料类型不能为空！");
    Validate.notNull(matterEntity.getEnableState(), "添加信息时，生效状态不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况）
    Validate.isTrue(matterEntity.getCode() == null || matterEntity.getCode().length() < 255 , "物料编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(matterEntity.getName() == null || matterEntity.getName().length() < 255 , "物料名称,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(matterEntity.getMatterTypeCode() == null || matterEntity.getMatterTypeCode().length() < 255 , "物料类型,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(matterEntity.getMatterHostUnit() == null || matterEntity.getMatterHostUnit().length() < 255 , "主单位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(matterEntity.getMatterAssistUnit() == null || matterEntity.getMatterAssistUnit().length() < 255 , "辅单位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(matterEntity.getSpecs() == null || matterEntity.getSpecs().length() < 255 , "规格,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(matterEntity.getScaleFactor() == null || matterEntity.getScaleFactor().length() < 255 , "换算系数,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(matterEntity.getUnitCode() == null || matterEntity.getUnitCode().length() < 255 , "计量单位编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(matterEntity.getUnitName() == null || matterEntity.getUnitName().length() < 255 , "计量单位,在进行添加时填入值超过了限定长度(255)，请检查!");
  }
  @Transactional
  @Override
  public MatterEntity update(MatterEntity matterEntity) {
    MatterEntity current = this.updateForm(matterEntity);
    //====================================================
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  }
  @Transactional
  @Override
  public MatterEntity updateForm(MatterEntity matterEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     *
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */

    this.updateValidation(matterEntity);
    // ===================基本信息
    String currentId = matterEntity.getId();
    Optional<MatterEntity> op_currentMatterEntity = this.matterEntityRepository.findById(currentId);
    MatterEntity currentMatterEntity = op_currentMatterEntity.orElse(null);
    currentMatterEntity = Validate.notNull(currentMatterEntity ,"未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentMatterEntity.setCode(matterEntity.getCode());
    currentMatterEntity.setName(matterEntity.getName());
    currentMatterEntity.setMatterTypeCode(matterEntity.getMatterTypeCode());
    currentMatterEntity.setMatterHostUnit(matterEntity.getMatterHostUnit());
    currentMatterEntity.setMatterAssistUnit(matterEntity.getMatterAssistUnit());
    currentMatterEntity.setSpecs(matterEntity.getSpecs());
    currentMatterEntity.setEnableState(matterEntity.getEnableState());
    currentMatterEntity.setScaleFactor(matterEntity.getScaleFactor());
    currentMatterEntity.setUnitCode(matterEntity.getUnitCode());
    currentMatterEntity.setUnitName(matterEntity.getUnitName());
    currentMatterEntity.setMatterTypeName(matterEntity.getMatterTypeName());
    currentMatterEntity.setCreateTime(matterEntity.getCreateTime());
    currentMatterEntity.setPiceWeight(matterEntity.getPiceWeight());
    currentMatterEntity.setMatterHostUnitCode(matterEntity.getMatterHostUnitCode());
    currentMatterEntity.setMatterTypeSecondCode(matterEntity.getMatterTypeSecondCode());
    currentMatterEntity.setMatterTypeSecondName(matterEntity.getMatterTypeSecondName());
    currentMatterEntity.setMatterTypeThirdCode(matterEntity.getMatterTypeThirdCode());
    currentMatterEntity.setMatterTypeThirdName(matterEntity.getMatterTypeThirdName());
    currentMatterEntity.setLastUpdateTime(matterEntity.getLastUpdateTime());
    currentMatterEntity.setModifyTime(new Date());
    currentMatterEntity.setMatterAssistUnitCode(matterEntity.getMatterAssistUnitCode());
    currentMatterEntity.setCompanyCode(matterEntity.getCompanyCode());
    currentMatterEntity.setCompanyId(matterEntity.getCompanyId());
    currentMatterEntity.setCompanyName(matterEntity.getCompanyName());
    currentMatterEntity.setAquiState(matterEntity.getAquiState());
    this.matterEntityRepository.saveAndFlush(currentMatterEntity);
    return currentMatterEntity;
  }
  /**
   * 在更新一个已有的MatterEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(MatterEntity matterEntity) {
    Validate.isTrue(!StringUtils.isBlank(matterEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");

    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(matterEntity.getCode(), "修改信息时，物料编码不能为空！");
    Validate.notBlank(matterEntity.getName(), "修改信息时，物料名称不能为空！");
    Validate.notBlank(matterEntity.getMatterTypeCode(), "修改信息时，物料类型不能为空！");
    Validate.notNull(matterEntity.getEnableState(), "修改信息时，生效状态不能为空！");

    // 重复性判断，基本属性，需要满足unique = true
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(matterEntity.getCode() == null || matterEntity.getCode().length() < 255 , "物料编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(matterEntity.getName() == null || matterEntity.getName().length() < 255 , "物料名称,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(matterEntity.getMatterTypeCode() == null || matterEntity.getMatterTypeCode().length() < 255 , "物料类型,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(matterEntity.getMatterHostUnit() == null || matterEntity.getMatterHostUnit().length() < 255 , "主单位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(matterEntity.getMatterAssistUnit() == null || matterEntity.getMatterAssistUnit().length() < 255 , "辅单位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(matterEntity.getSpecs() == null || matterEntity.getSpecs().length() < 255 , "规格,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(matterEntity.getScaleFactor() == null || matterEntity.getScaleFactor().length() < 255 , "换算系数,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(matterEntity.getUnitCode() == null || matterEntity.getUnitCode().length() < 255 , "计量单位编码,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(matterEntity.getUnitName() == null || matterEntity.getUnitName().length() < 255 , "计量单位,在进行修改时填入值超过了限定长度(255)，请检查!");
  }
  @Override
  public MatterEntity findDetailsById(String id) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(id)) {
      return null;
    }
    MatterEntity current = this.matterEntityRepository.findDetailsById(id);
    if(current == null) {
      return null;
    }
    return current;
  }
  @Override
  public MatterEntity findById(String id) {
    if(StringUtils.isBlank(id)) {
      return null;
    }

    Optional<MatterEntity> op = matterEntityRepository.findById(id);
    return op.orElse(null);
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id , "进行删除时，必须给定主键信息!!");
    MatterEntity current = this.findById(id);
    if(current != null) {
      this.matterEntityRepository.delete(current);
    }
  }
  @Override
  public MatterEntity findDetailsByFormInstanceId(String formInstanceId) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if(StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    MatterEntity current = this.matterEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if(current == null) {
      return null;
    }
    return current;
  }
  @Override
  public MatterEntity findByFormInstanceId(String formInstanceId) {
    if(StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    return this.matterEntityRepository.findByFormInstanceId(formInstanceId);
  }

  @Override
  public Page<MatterEntity> findByMatterConditions(Map<String, Object> params, Pageable pageable) {

    return matterEntityRepository.findByMatterConditions(pageable, params);
  }

  @Override
  public MatterEntity findByCode(String code) {
    if(StringUtils.isBlank(code)){
      return null;
    }
    return matterEntityRepository.findByCode(code);
  }

  @Override
  public MatterEntity findByEbsCodeAndEbsOrgCode(String ebsCode, String ebsOrgCode) {
    if (StringUtils.isBlank(ebsCode) || StringUtils.isBlank(ebsOrgCode)) {
      return null;
    }
    return matterEntityRepository.findByEbsCodeAndEbsOrgCode(ebsCode, ebsOrgCode);
  }

  @Override
  public void pullMatters() {

    matterRemoteService.pullMatter();
  }
}