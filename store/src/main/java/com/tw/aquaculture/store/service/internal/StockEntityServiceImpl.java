package com.tw.aquaculture.store.service.internal;

import com.google.common.collect.Sets;
import com.tw.aquaculture.common.entity.store.StockEntity;
import com.tw.aquaculture.store.repository.StockEntityRepository;
import com.tw.aquaculture.store.service.StockEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * StockEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("StockEntityServiceImpl")
public class StockEntityServiceImpl implements StockEntityService {
  @Autowired private StockEntityRepository stockEntityRepository;

  @Transactional
  @Override
  public StockEntity create(StockEntity stockEntity) {
    StockEntity current = this.createForm(stockEntity);
    // ====================================================
    //    这里可以处理第三方系统调用（或特殊处理过程）
    // ====================================================
    return current;
  }

  @Transactional
  @Override
  public StockEntity createForm(StockEntity stockEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *   2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    this.createValidation(stockEntity);

    // ===============================
    //  和业务有关的验证填写在这个区域
    // ===============================

    this.stockEntityRepository.save(stockEntity);

    // 返回最终处理的结果，里面带有详细的关联信息
    return stockEntity;
  }
  /** 在创建一个新的StockEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值 */
  private void createValidation(StockEntity stockEntity) {
    Validate.notNull(stockEntity, "进行当前操作时，信息对象必须传入!!");
    Validate.notNull(stockEntity.getBase(), "进行当前操作时，基地对象必须传入!!");
    Validate.notNull(stockEntity.getMatter(), "进行当前操作时，物料对象必须传入!!");
    Validate.notNull(stockEntity.getStore(), "进行当前操作时，仓库对象必须传入!!");
    Validate.notNull(stockEntity.getSpecs(), "进行当前操作时，规格必须传入!!");
    Validate.notNull(stockEntity.getStorage(), "进行当前操作时，库存量必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(stockEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    stockEntity.setId(null);
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况）
    Validate.isTrue(
        stockEntity.getSpecs() == null || stockEntity.getSpecs().length() < 255,
        "规格,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
        stockEntity.getUnit() == null || stockEntity.getUnit().length() < 255,
        "单位,在进行添加时填入值超过了限定长度(255)，请检查!");
  }

  @Transactional
  @Override
  public StockEntity update(StockEntity stockEntity) {
    StockEntity current = this.updateForm(stockEntity);
    // ====================================================
    //    这里可以处理第三方系统调用（或特殊处理过程）
    // ====================================================
    return current;
  }

  @Transactional
  @Override
  public StockEntity updateForm(StockEntity stockEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     *
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */

    this.updateValidation(stockEntity);
    // ===================基本信息
    String currentId = stockEntity.getId();
    Optional<StockEntity> op_currentStockEntity = this.stockEntityRepository.findById(currentId);
    StockEntity currentStockEntity = op_currentStockEntity.orElse(null);
    currentStockEntity = Validate.notNull(currentStockEntity, "未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentStockEntity.setSpecs(stockEntity.getSpecs());
    currentStockEntity.setUnit(stockEntity.getUnit());
    currentStockEntity.setStorage(stockEntity.getStorage());
    currentStockEntity.setBase(stockEntity.getBase());
    currentStockEntity.setStore(stockEntity.getStore());
    currentStockEntity.setMatter(stockEntity.getMatter());

    this.stockEntityRepository.saveAndFlush(currentStockEntity);
    return currentStockEntity;
  }
  /** 在更新一个已有的StockEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值 */
  private void updateValidation(StockEntity stockEntity) {
    Validate.isTrue(!StringUtils.isBlank(stockEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
    Validate.notNull(stockEntity.getBase(), "修改信息时，基地对象必须传入!!");
    Validate.notNull(stockEntity.getMatter(), "修改信息时，物料对象必须传入!!");
    Validate.notNull(stockEntity.getStore(), "修改信息时，仓库对象必须传入!!");
    Validate.notNull(stockEntity.getSpecs(), "修改信息时，规格必须传入!!");
    Validate.notNull(stockEntity.getStorage(), "修改信息时，库存量必须传入!!");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(
        stockEntity.getSpecs() == null || stockEntity.getSpecs().length() < 255,
        "规格,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(
        stockEntity.getUnit() == null || stockEntity.getUnit().length() < 255,
        "单位,在进行修改时填入值超过了限定长度(255)，请检查!");
  }

  @Override
  public Set<StockEntity> findDetailsByBase(String base) {
    if (StringUtils.isBlank(base)) {
      return Sets.newHashSet();
    }
    return this.stockEntityRepository.findDetailsByBase(base);
  }

  @Override
  public Set<StockEntity> findDetailsByStore(String store) {
    if (StringUtils.isBlank(store)) {
      return Sets.newHashSet();
    }
    return this.stockEntityRepository.findDetailsByStore(store);
  }

  @Override
  public Set<StockEntity> findDetailsByMatter(String matter) {
    if (StringUtils.isBlank(matter)) {
      return Sets.newHashSet();
    }
    return this.stockEntityRepository.findDetailsByMatter(matter);
  }

  @Override
  public StockEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    return this.stockEntityRepository.findDetailsById(id);
  }

  @Override
  public StockEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }

    Optional<StockEntity> op = stockEntityRepository.findById(id);
    return op.orElse(null);
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    StockEntity current = this.findById(id);
    if (current != null) {
      this.stockEntityRepository.delete(current);
    }
  }

  @Override
  public StockEntity findByMatterAndStore(String matterId, String storeId) {
    if (StringUtils.isBlank(matterId) || StringUtils.isBlank(storeId)) {
      return null;
    }
    return stockEntityRepository.findByMatterAndStore(matterId, storeId);
  }

  @Override
  public List<StockEntity> findByBaseId(String baseId) {
    if (StringUtils.isBlank(baseId)) {
      return null;
    }
    return stockEntityRepository.findByBaseId(baseId);
  }
}
