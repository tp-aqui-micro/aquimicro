package com.tw.aquaculture.store.service.internal;

import com.bizunited.platform.kuiper.starter.service.KuiperToolkitService;
import com.google.common.collect.Sets;
import com.tw.aquaculture.common.entity.base.MatterEntity;
import com.tw.aquaculture.common.entity.base.StoreEntity;
import com.tw.aquaculture.common.entity.base.TwBaseEntity;
import com.tw.aquaculture.common.entity.store.InStorageEntity;
import com.tw.aquaculture.common.entity.store.StockOutEntity;
import com.tw.aquaculture.common.entity.store.TransferItemEntity;
import com.tw.aquaculture.common.entity.store.TransferStoreEntity;
import com.tw.aquaculture.common.enums.CodeTypeEnum;
import com.tw.aquaculture.store.repository.TransferStoreEntityRepository;
import com.tw.aquaculture.store.service.InStorageEntityService;
import com.tw.aquaculture.store.service.MatterEntityService;
import com.tw.aquaculture.store.service.StockOutEntityService;
import com.tw.aquaculture.store.service.StoreEntityService;
import com.tw.aquaculture.store.service.TransferItemEntityService;
import com.tw.aquaculture.store.service.TransferStoreEntityService;
import com.tw.aquaculture.store.service.remote.CodeGenerateService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * TransferStoreEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("TransferStoreEntityServiceImpl")
public class TransferStoreEntityServiceImpl implements TransferStoreEntityService {
  @Autowired
  private StoreEntityService storeEntityService;
  @Autowired
  private TransferItemEntityService transferItemEntityService;
  @Autowired
  private TransferStoreEntityRepository transferStoreEntityRepository;
  @Autowired
  private MatterEntityService matterEntityService;
  @Autowired
  private CodeGenerateService codeGenerateService;
  @Autowired
  private StockOutEntityService stockOutEntityService;
  @Autowired
  private InStorageEntityService inStorageEntityService;
  /**
   * Kuiper表单引擎用于减少编码工作量的工具包
   */
  @Autowired
  private KuiperToolkitService kuiperToolkitService;

  @Transactional
  @Override
  public TransferStoreEntity create(TransferStoreEntity transferStoreEntity) {
    TransferStoreEntity current = this.createForm(transferStoreEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  }

  @Transactional
  @Override
  public TransferStoreEntity createForm(TransferStoreEntity transferStoreEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *   2.3.2、以及验证每个分组的OneToMany明细信息
     * */
    transferStoreEntity.setCode(codeGenerateService.generateBusinessCode(CodeTypeEnum.TRANSFER_STORE_ENTITY));
    this.createValidation(transferStoreEntity);
    if (StringUtils.isBlank(transferStoreEntity.getOutStore().getId()) || StringUtils.isBlank(transferStoreEntity.getInStore().getId())) {
      throw new IllegalArgumentException("参数错误，仓库不能为空");
    }
    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    StoreEntity outStore = storeEntityService.findDetailsById(transferStoreEntity.getOutStore().getId());
    StoreEntity inStore = storeEntityService.findDetailsById(transferStoreEntity.getInStore().getId());
    if (outStore == null || StringUtils.isBlank(outStore.getId()) || inStore == null || StringUtils.isBlank(inStore.getId())) {
      throw new IllegalArgumentException("参数错误，转出或转入仓库不存在");
    }
    this.transferStoreEntityRepository.saveAndFlush(transferStoreEntity);

    // 处理明细信息：转入仓库
    Set<TransferItemEntity> transferItemEntityItems = transferStoreEntity.getTransferItems();
    if (transferItemEntityItems != null) {
      for (TransferItemEntity transferItemEntityItem : transferItemEntityItems) {
        transferItemEntityItem.setTransferStore(transferStoreEntity);
        this.transferItemEntityService.create(transferItemEntityItem);
        transferStore(transferItemEntityItem, outStore, inStore, transferStoreEntity.getTransferTime(), transferStoreEntity.getCreateName());
      }
    }

    this.transferStoreEntityRepository.flush();
    // 返回最终处理的结果，里面带有详细的关联信息
    return transferStoreEntity;
  }

  private void transferStore(TransferItemEntity item, StoreEntity outStore, StoreEntity inStore, Date time, String username) {
    /*UserVo user = userService.findByAccount(account);
    Validate.notNull(user, "当前用户不存在，请确认是否登录");
    UserEntity userEntity = new UserEntity();
    userEntity.setId(user.getId());*/
    /*RBucket<String> nowPosition = redissonClient.getBucket(Constants.NOW_POSITION);
    PositionVo positionVo = JSONArray.parseObject(nowPosition.get(), PositionVo.class);*/
    MatterEntity matter = matterEntityService.findDetailsById(item.getMatter().getId());
    if (matter == null) {
      return;
    }
    MatterEntity paramMatter = new MatterEntity();
    paramMatter.setId(matter.getId());
    //出库
    StockOutEntity stockOutEntity = new StockOutEntity();
    TwBaseEntity outBase = new TwBaseEntity();
    outBase.setId(outStore.getTwBase().getId());
    stockOutEntity.setTwBase(outBase);
    stockOutEntity.setUnit(item.getUnit());
    stockOutEntity.setSpec(item.getMatter().getSpecs());
    stockOutEntity.setStockOutDate(time);
    //stockOutEntity.setReceiver(userEntity);
    stockOutEntity.setCategory(matter.getMatterHostUnitCode());
    StoreEntity paramOutStore = new StoreEntity();
    paramOutStore.setId(outStore.getId());
    stockOutEntity.setStore(paramOutStore);
    stockOutEntity.setQuantity(item.getCount());
    stockOutEntity.setMatter(paramMatter);
    stockOutEntity.setCreateName(username);
    stockOutEntity.setCreateTime(new Date());
    stockOutEntity.setUnit(item.getUnit());
    //stockOutEntity.setCreatePosition();
    stockOutEntity.setCreatePosition("暂无");
    stockOutEntity.setSpec(matter.getSpecs());
    stockOutEntity.setFormInstanceId(UUID.randomUUID().toString());
    //formInstanceHandleService.create(stockOutEntity, "StockOutEntity", "StockOutEntityService.create", false);
    stockOutEntityService.create(stockOutEntity);
    //入库
    InStorageEntity inStorageEntity = new InStorageEntity();
    TwBaseEntity inBase = new TwBaseEntity();
    inBase.setId(inStore.getTwBase().getId());
    inStorageEntity.setTwBase(inBase);
    inStorageEntity.setUnit(item.getUnit());
    inStorageEntity.setSpec(item.getMatter().getSpecs());
    inStorageEntity.setInStorageDate(time);
    inStorageEntity.setCategory(matter.getMatterHostUnitCode());
    StoreEntity paramInstore = new StoreEntity();
    paramInstore.setId(inStore.getId());
    inStorageEntity.setStore(paramInstore);
    inStorageEntity.setQuantity(item.getCount());
    inStorageEntity.setMatter(paramMatter);
    inStorageEntity.setCreateName(username);
    inStorageEntity.setCreateTime(new Date());
    inStorageEntity.setSpec(matter.getSpecs());
    inStorageEntity.setUnit(item.getUnit());

    //inStorageEntity.setCreatePosition();
    inStorageEntity.setCreatePosition("暂无");
    inStorageEntity.setFormInstanceId(UUID.randomUUID().toString());
    //formInstanceHandleService.create(inStorageEntity, "InStorageEntity", "InStorageEntityService.create", false);
    inStorageEntityService.create(inStorageEntity);
  }

  /**
   * 在创建一个新的TransferStoreEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
   */
  private void createValidation(TransferStoreEntity transferStoreEntity) {
    Validate.notNull(transferStoreEntity, "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(transferStoreEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    transferStoreEntity.setId(null);
    Validate.notBlank(transferStoreEntity.getFormInstanceId(), "添加信息时，表单实例编号不能为空！");
    Validate.notNull(transferStoreEntity.getCreateTime(), "添加信息时，创建时间不能为空！");
    Validate.notBlank(transferStoreEntity.getCreateName(), "添加信息时，创建人姓名不能为空！");
    Validate.notBlank(transferStoreEntity.getCreatePosition(), "添加信息时，创建人职位不能为空！");
    Validate.notBlank(transferStoreEntity.getCode(), "添加信息时，编码不能为空！");
    Validate.notNull(transferStoreEntity.getTransferTime(), "添加信息时，转库时间不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
    Validate.isTrue(transferStoreEntity.getFormInstanceId() == null || transferStoreEntity.getFormInstanceId().length() < 255, "表单实例编号,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(transferStoreEntity.getCreateName() == null || transferStoreEntity.getCreateName().length() < 255, "创建人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(transferStoreEntity.getCreatePosition() == null || transferStoreEntity.getCreatePosition().length() < 255, "创建人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(transferStoreEntity.getModifyName() == null || transferStoreEntity.getModifyName().length() < 255, "修改人姓名,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(transferStoreEntity.getModifyPosition() == null || transferStoreEntity.getModifyPosition().length() < 255, "修改人职位,在进行添加时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(transferStoreEntity.getCode() == null || transferStoreEntity.getCode().length() < 255, "编码,在进行添加时填入值超过了限定长度(255)，请检查!");
    TransferStoreEntity currentTransferStoreEntity = this.findByFormInstanceId(transferStoreEntity.getFormInstanceId());
    Validate.isTrue(currentTransferStoreEntity == null, "表单实例编号已存在,请检查");
    // 验证ManyToOne关联：转出仓库绑定值的正确性
    StoreEntity currentOutStore = transferStoreEntity.getOutStore();
    Validate.notNull(currentOutStore, "转出仓库信息必须传入，请检查!!");
    String currentPkOutStore = currentOutStore.getId();
    Validate.notBlank(currentPkOutStore, "创建操作时，当前转出仓库信息必须关联！");
    Validate.notNull(this.storeEntityService.findById(currentPkOutStore), "转出仓库关联信息未找到，请检查!!");
    // 验证ManyToOne关联：转入仓库绑定值的正确性
    StoreEntity currentInStore = transferStoreEntity.getInStore();
    Validate.notNull(currentInStore, "转入仓库信息必须传入，请检查!!");
    String currentPkInStore = currentInStore.getId();
    Validate.notBlank(currentPkInStore, "创建操作时，当前转入仓库信息必须关联！");
    Validate.notNull(this.storeEntityService.findById(currentPkInStore), "转入仓库关联信息未找到，请检查!!");
  }

  @Transactional
  @Override
  public TransferStoreEntity update(TransferStoreEntity transferStoreEntity) {
    TransferStoreEntity current = this.updateForm(transferStoreEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  }

  @Transactional
  @Override
  public TransferStoreEntity updateForm(TransferStoreEntity transferStoreEntity) {
    /*
     * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
     * 1、如果当前模型对象不是主模型
     * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
     * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
     * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
     *
     * 2、如果当前模型对象是主业务模型
     *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
     *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
     *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
     *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
     *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
     *    2.3.2、以及验证每个分组的OneToMany明细信息
     * */

    this.updateValidation(transferStoreEntity);
    // ===================基本信息
    String currentId = transferStoreEntity.getId();
    Optional<TransferStoreEntity> op_currentTransferStoreEntity = this.transferStoreEntityRepository.findById(currentId);
    TransferStoreEntity currentTransferStoreEntity = op_currentTransferStoreEntity.orElse(null);
    currentTransferStoreEntity = Validate.notNull(currentTransferStoreEntity, "未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentTransferStoreEntity.setFormInstanceId(transferStoreEntity.getFormInstanceId());
    currentTransferStoreEntity.setCreateTime(transferStoreEntity.getCreateTime());
    currentTransferStoreEntity.setCreateName(transferStoreEntity.getCreateName());
    currentTransferStoreEntity.setCreatePosition(transferStoreEntity.getCreatePosition());
    currentTransferStoreEntity.setModifyTime(transferStoreEntity.getModifyTime());
    currentTransferStoreEntity.setModifyName(transferStoreEntity.getModifyName());
    currentTransferStoreEntity.setModifyPosition(transferStoreEntity.getModifyPosition());
    currentTransferStoreEntity.setCode(transferStoreEntity.getCode());
    currentTransferStoreEntity.setTransferTime(transferStoreEntity.getTransferTime());
    currentTransferStoreEntity.setOutStore(transferStoreEntity.getOutStore());
    currentTransferStoreEntity.setInStore(transferStoreEntity.getInStore());

    this.transferStoreEntityRepository.saveAndFlush(currentTransferStoreEntity);
    // ==========准备处理OneToMany关系的详情transferItems
    // 清理出那些需要删除的transferItems信息
    Set<TransferItemEntity> transferItemEntitys = null;
    if (transferStoreEntity.getTransferItems() != null) {
      transferItemEntitys = Sets.newLinkedHashSet(transferStoreEntity.getTransferItems());
    } else {
      transferItemEntitys = Sets.newLinkedHashSet();
    }
    Set<TransferItemEntity> dbTransferItemEntitys = this.transferItemEntityService.findDetailsByTransferStore(transferStoreEntity.getId());
    if (dbTransferItemEntitys == null) {
      dbTransferItemEntitys = Sets.newLinkedHashSet();
    }
    //回滚所有的转库明细, 交换出入库仓库
    transferStores(dbTransferItemEntitys, currentTransferStoreEntity.getTransferTime(), currentTransferStoreEntity.getInStore(), currentTransferStoreEntity.getOutStore(), transferStoreEntity.getCreateName());
    //添加当前所有的出入库明细
    transferStores(transferItemEntitys, transferStoreEntity.getTransferTime(), transferStoreEntity.getInStore(), transferStoreEntity.getOutStore(), transferStoreEntity.getCreateName());
    // 求得的差集既是需要删除的数据 
    Set<String> mustDeleteTransferItemsPks = kuiperToolkitService.collectionDiffent(dbTransferItemEntitys, transferItemEntitys, TransferItemEntity::getId);
    if (mustDeleteTransferItemsPks != null && !mustDeleteTransferItemsPks.isEmpty()) {
      for (String mustDeleteTransferItemsPk : mustDeleteTransferItemsPks) {
        this.transferItemEntityService.deleteById(mustDeleteTransferItemsPk);
      }
    }
    // 对传来的记录进行添加或者修改操作 
    for (TransferItemEntity transferItemEntityItem : transferItemEntitys) {
      transferItemEntityItem.setTransferStore(transferStoreEntity);
      if (StringUtils.isBlank(transferItemEntityItem.getId())) {
        transferItemEntityService.create(transferItemEntityItem);
      } else {
        transferItemEntityService.update(transferItemEntityItem);
      }
    }

    return currentTransferStoreEntity;
  }

  /**
   * 转库
   *  @param dbTransferItemEntitys
   * @param transferTime
   * @param outStore
   * @param inStore
   * @param username
   */
  private void transferStores(Set<TransferItemEntity> dbTransferItemEntitys, Date transferTime, StoreEntity outStore, StoreEntity inStore, String username) {
    if (CollectionUtils.isEmpty(dbTransferItemEntitys)) {
      return;
    }
    for (TransferItemEntity item : dbTransferItemEntitys) {
      transferStore(item, outStore, inStore, transferTime, username);
    }
  }

  /**
   * 在更新一个已有的TransferStoreEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
   */
  private void updateValidation(TransferStoreEntity transferStoreEntity) {
    Validate.isTrue(!StringUtils.isBlank(transferStoreEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");

    // 基础信息判断，基本属性，需要满足not null
    Validate.notBlank(transferStoreEntity.getFormInstanceId(), "修改信息时，表单实例编号不能为空！");
    Validate.notNull(transferStoreEntity.getCreateTime(), "修改信息时，创建时间不能为空！");
    Validate.notBlank(transferStoreEntity.getCreateName(), "修改信息时，创建人姓名不能为空！");
    Validate.notBlank(transferStoreEntity.getCreatePosition(), "修改信息时，创建人职位不能为空！");
    Validate.notBlank(transferStoreEntity.getCode(), "修改信息时，编码不能为空！");
    Validate.notNull(transferStoreEntity.getTransferTime(), "修改信息时，转库时间不能为空！");

    // 重复性判断，基本属性，需要满足unique = true
    TransferStoreEntity currentForFormInstanceId = this.findByFormInstanceId(transferStoreEntity.getFormInstanceId());
    Validate.isTrue(currentForFormInstanceId == null || StringUtils.equals(currentForFormInstanceId.getId(), transferStoreEntity.getId()), "表单实例编号已存在,请检查");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    Validate.isTrue(transferStoreEntity.getFormInstanceId() == null || transferStoreEntity.getFormInstanceId().length() < 255, "表单实例编号,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(transferStoreEntity.getCreateName() == null || transferStoreEntity.getCreateName().length() < 255, "创建人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(transferStoreEntity.getCreatePosition() == null || transferStoreEntity.getCreatePosition().length() < 255, "创建人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(transferStoreEntity.getModifyName() == null || transferStoreEntity.getModifyName().length() < 255, "修改人姓名,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(transferStoreEntity.getModifyPosition() == null || transferStoreEntity.getModifyPosition().length() < 255, "修改人职位,在进行修改时填入值超过了限定长度(255)，请检查!");
    Validate.isTrue(transferStoreEntity.getCode() == null || transferStoreEntity.getCode().length() < 255, "编码,在进行修改时填入值超过了限定长度(255)，请检查!");

    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
    StoreEntity currentForOutStore = transferStoreEntity.getOutStore();
    Validate.notNull(currentForOutStore, "修改信息时，转出仓库必须传入，请检查!!");
    StoreEntity currentForInStore = transferStoreEntity.getInStore();
    Validate.notNull(currentForInStore, "修改信息时，转入仓库必须传入，请检查!!");
  }

  @Override
  public TransferStoreEntity findDetailsById(String id) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(id)) {
      return null;
    }
    TransferStoreEntity current = this.transferStoreEntityRepository.findDetailsById(id);
    if (current == null) {
      return null;
    }
    String currentPkValue = current.getId();
    // 2、======
    // 转入仓库 的明细信息查询
    Collection<TransferItemEntity> oneToManyTransferItemsDetails = this.transferItemEntityService.findDetailsByTransferStore(currentPkValue);
    current.setTransferItems(Sets.newLinkedHashSet(oneToManyTransferItemsDetails));

    return current;
  }

  @Override
  public TransferStoreEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }

    Optional<TransferStoreEntity> op = transferStoreEntityRepository.findById(id);
    return op.orElse(null);
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id, "进行删除时，必须给定主键信息!!");
    TransferStoreEntity current = this.findById(id);
    if (current != null) {
      this.transferStoreEntityRepository.delete(current);
    }
  }

  @Override
  public TransferStoreEntity findDetailsByFormInstanceId(String formInstanceId) {
    /*
     * 1、首先查询这个主业务模型的基本信息和关联的（ManyToOne）单选信息和（ManyToMany）多选信息
     * 2、然后查询这个主业务模型的明细关联信息（OneToMany关联），如下：
     *   2.1、每一个明细关联信息都需要按照当前主业务表的id，查询其下的一般信息、单选信息和多选信息
     *   2.2、查询到的信息将返回给主业务模型的相关字段进行关联
     * 3、查询这个主业务模型的分组关联信息（OneToOne关联），如下：
     *   3.1、每一分组信息都要查询分组的一般信息，以及分组的单选和多选关联信息
     *   3.2、然后查询分组信息下可能的明细信息（实际上就是以上”步骤2“的操作步骤的重用）
     *   3.3、最后将得到的分组信息，和主业务表的相关字段进行关联
     * 注意：不能偷懒只写一个HQL，因为关联信息很多，数据表一旦过大，会有10多个左外连接，而且经常出现重复数据
     * */

    // 这是主模型下的明细查询过程
    // 1、=======
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    TransferStoreEntity current = this.transferStoreEntityRepository.findDetailsByFormInstanceId(formInstanceId);
    if (current == null) {
      return null;
    }
    String currentPkValue = current.getId();
    // 2、======
    // 转入仓库 的明细信息查询
    Collection<TransferItemEntity> oneToManyTransferItemsDetails = this.transferItemEntityService.findDetailsByTransferStore(currentPkValue);

    if (!CollectionUtils.isEmpty(oneToManyTransferItemsDetails)) {
      for (TransferItemEntity item : oneToManyTransferItemsDetails) {
        item.setMatterOtherId(item.getMatter() == null ? null : item.getMatter().getId());
        item.setMatterCode(item.getMatter() == null ? null : item.getMatter().getCode());
        item.setMatterName(item.getMatter() == null ? null : item.getMatter().getName());
      }
    }
    current.setTransferItems(Sets.newLinkedHashSet(oneToManyTransferItemsDetails));

    return current;
  }

  @Override
  public TransferStoreEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    return this.transferStoreEntityRepository.findByFormInstanceId(formInstanceId);
  }
} 
