package com.tw.aquaculture.store.service.remote;

import com.tw.aquaculture.common.enums.CodeTypeEnum;

/**
 * @author 陈荣
 * @date 2019/11/22 11:22
 */
public interface CodeGenerateService {

  /**
   * 生成基础流水编码
   *
   * @param codeType
   * @param prefix
   * @param length
   * @return
   */
  public String generateCode(String codeType, String prefix, int length);

  /**
   * 根据编码类型枚举生成基础流水编码
   *
   * @param codeTypeEnum
   * @return
   */
  public String generateCode(CodeTypeEnum codeTypeEnum);

  /**
   * 根据编码类型枚举生成业务流水
   *
   * @param codeTypeEnum
   * @return
   */
  public String generateBusinessCode(CodeTypeEnum codeTypeEnum);

  String generateBusinessCode(String name, String prefix, int i);
}
