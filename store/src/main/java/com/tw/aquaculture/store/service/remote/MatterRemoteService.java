package com.tw.aquaculture.store.service.remote;

/**
 * @author 陈荣
 * @date 2019/12/19 11:26
 */
public interface MatterRemoteService {

  /**
   * 拉取主数据物料
   */
  void pullAMatters();

  /**
   * 拉取主数据物料
   */
  Object pullMatter();
}
