package com.tw.aquaculture.store.service.remote.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.base.CustomerEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.store.feign.process.CustomerFeign;
import com.tw.aquaculture.store.service.remote.CustomerEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * CustomerEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("CustomerEntityServiceImpl")
public class CustomerEntityServiceImpl implements CustomerEntityService {
  @Autowired
  private CustomerFeign customerFeign;

  @Transactional
  @Override
  public CustomerEntity create(CustomerEntity customerEntity) {
    ResponseModel responseModel = customerFeign.create(customerEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CustomerEntity>() {
    });
  }

  @Transactional
  @Override
  public CustomerEntity createForm(CustomerEntity customerEntity) {
    return null;
  }

  @Transactional
  @Override
  public CustomerEntity update(CustomerEntity customerEntity) {
    ResponseModel responseModel = customerFeign.update(customerEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CustomerEntity>() {
    });

  }

  @Transactional
  @Override
  public CustomerEntity updateForm(CustomerEntity customerEntity) {
    return null;
  }

  @Override
  public CustomerEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = customerFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CustomerEntity>() {
    });
  }

  @Override
  public CustomerEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = customerFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CustomerEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {

    customerFeign.deleteById(id);
  }

  @Override
  public CustomerEntity findDetailsByFormInstanceId(String formInstanceId) {

    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = customerFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CustomerEntity>() {
    });
  }

  @Override
  public CustomerEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = customerFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<CustomerEntity>() {
    });
  }

} 
