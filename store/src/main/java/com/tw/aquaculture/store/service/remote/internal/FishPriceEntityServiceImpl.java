package com.tw.aquaculture.store.service.remote.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.base.FishPriceEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.store.feign.process.FishPriceFeign;
import com.tw.aquaculture.store.service.remote.FishPriceEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * FishPriceEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("FishPriceEntityServiceImpl")
public class FishPriceEntityServiceImpl implements FishPriceEntityService {

  @Autowired
  private FishPriceFeign fishPriceFeign;

  @Transactional
  @Override
  public FishPriceEntity create(FishPriceEntity fishPriceEntity) {

    ResponseModel responseModel = fishPriceFeign.create(fishPriceEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishPriceEntity>() {
    });
  }

  @Transactional
  @Override
  public FishPriceEntity createForm(FishPriceEntity fishPriceEntity) {
    return null;
  }

  @Transactional
  @Override
  public FishPriceEntity update(FishPriceEntity fishPriceEntity) {
    ResponseModel responseModel = fishPriceFeign.update(fishPriceEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishPriceEntity>() {
    });
  }

  @Transactional
  @Override
  public FishPriceEntity updateForm(FishPriceEntity fishPriceEntity) {
    return null;
  }

  @Override
  public FishPriceEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = fishPriceFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishPriceEntity>() {
    });
  }

  @Override
  public FishPriceEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = fishPriceFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishPriceEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    if (StringUtils.isBlank(id)) {
      return;
    }
    fishPriceFeign.deleteById(id);
  }

  @Override
  public FishPriceEntity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = fishPriceFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishPriceEntity>() {
    });
  }

  @Override
  public FishPriceEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = fishPriceFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<FishPriceEntity>() {
    });
  }
} 
