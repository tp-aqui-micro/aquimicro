package com.tw.aquaculture.store.service.remote.internal;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tw.aquaculture.common.entity.base.MatterEntity;
import com.tw.aquaculture.common.enums.EnableStateEnum;
import com.tw.aquaculture.common.enums.MatterEbsStateEnum;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.common.vo.remote.matter.MatterRemoteVo;
import com.tw.aquaculture.common.vo.remote.matter.MatterResponseVo;
import com.tw.aquaculture.store.service.MatterEntityService;
import com.tw.aquaculture.store.service.remote.MatterRemoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * 物料相关定时任务
 *
 * @author 陈荣
 * @date 2019/11/20 14:13
 */
@Service("MatterRemoteService")
public class MatterRemoteServiceImpl implements MatterRemoteService {

  private static final Logger LOGGER = LoggerFactory.getLogger(MatterRemoteServiceImpl.class);

  /**
   * 物料接口
   */
  private final static String URL = "http://protwosb3.tongwei.com/WP_ESB_SOA/APP_EBS_SERVICE/Proxy_Services/TA_COM/getItemDataRest_PS?_wadl";

  /**
   * 开始记录下标
   */
  private int start = 1;

  /**
   * 结束记录下标
   */
  private int end = PAGE_SIZE;

  /**
   * 每页条数
   */
  private final static int PAGE_SIZE = 2000;

  /**
   * 单个查询条件最多拉取条数
   */
  private final static int PICE_MAX_SIZE = 20000;

  /**
   * 需要拉取的物料类型
   */
  private static List<Map<String, String>> CATEGORY_FIRSTS;

  static {
    CATEGORY_FIRSTS = Lists.newArrayList();
    Map<String, String> p1 = Maps.newHashMap();

    Map<String, String> p6 = Maps.newHashMap();
    p6.put("CATEGORY_FIRST", "13");
    p6.put("CATEGORY_SEC", "30");
    p6.put("CATEGORY_THIRD", "01$$02$$03");
    //p6.put("OU_CODE", "2104$$2071$$2113$$2021$$2042");
    CATEGORY_FIRSTS.add(p6);

    p1.put("CATEGORY_FIRST", "11");
    p1.put("CATEGORY_SEC", "30");
    p1.put("CATEGORY_THIRD", "01$$06$$07$$08");
    //p1.put("OU_CODE", "2104$$2071$$2113$$2021$$2042");
    CATEGORY_FIRSTS.add(p1);
  }

  private int count = 0;

  /**
   * 超时最多尝试次数
   */
  private final static int MAX_TIME = 3;

  private boolean flag = false;


  @Autowired
  private MatterEntityService matterEntityService;

  @Autowired
  private RemoteUtil<JSONObject> remoteUtil;

  @Override
  public Object pullMatter() {
    this.pullAMatters();
    return null;
  }

  @Override
//  @DynamicTaskService(cornExpression = "0 0 1 * * ?", taskDesc = "拉取物料定时任务, 每天1点执行一次")
  /**
   * 同步拉取全量物料信息
   */
  synchronized public void pullAMatters() {
    for (Map<String, String> param : CATEGORY_FIRSTS) {
      this.pullByPage(param);
      start = 1;
      end = PAGE_SIZE;
    }
  }

  /**
   * 分页拉取物料信息
   * @param param
   */
  private void pullByPage(Map<String, String> param) {
    flag = true;
    while (flag && start <= PICE_MAX_SIZE) {
      this.pullForTimeOut(param);
      start += PAGE_SIZE;
      end += PAGE_SIZE;
    }
  }

  /**
   * 程序异常时，尝试3次再次拉取物料信息
   * @param param
   */
  private void pullForTimeOut(Map<String, String> param) {
    count = 0;
    while (count < MAX_TIME) {
      this.pullMa(param);
    }
  }


  /**
   * 拉取物料信息主逻辑
   * @param param
   */
  private void pullMa(Map<String, String> param) {
    String json;
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("P_START_NUMBER", start);
    jsonObject.put("P_END_NUMBER", end);
    Set<Map.Entry<String, String>> entrySet = param.entrySet();
    JSONArray jsonArray = new JSONArray();
    for (Map.Entry<String, String> entry : entrySet) {
      JSONObject paramJson = new JSONObject();
      paramJson.put("KEY_CODE", entry.getKey());
      paramJson.put("KEY_VALUE", entry.getValue());
      paramJson.put("KEY_DATA_TYPE", "C");
      paramJson.put("SIGN_VALUE", entry.getValue().contains("$$") ? "IN" : "EQ");
      jsonArray.add(paramJson);
    }
    JSONObject jsonObject1 = new JSONObject();
    jsonObject1.put("P_TAB_PARA_ITEM", jsonArray);
    jsonObject.put("P_TAB_PARA", jsonObject1);
    try {
      json = remoteUtil.post(URL, jsonObject);
    } catch (Exception e) {
      LOGGER.error("定时任务==物料数据同步异常：" + e.getMessage());
      count++;
      return;
    }


    MatterResponseVo result = JsonUtil.json2Obj(json, new TypeReference<MatterResponseVo>() {});
    if(result == null || result.getMatterResponseBodyVo() == null || CollectionUtils.isEmpty(result.getMatterResponseBodyVo().getMatterRemoteVoList())){
      flag = false;
      count = 100;
      return;
    }

    List<MatterRemoteVo> matterRemoteVos = result.getMatterResponseBodyVo().getMatterRemoteVoList();
    if (matterRemoteVos.size() < PAGE_SIZE) {
      flag = false;
    }
    for (MatterRemoteVo matterRemoteVo : matterRemoteVos) {
      MatterEntity matterEntity = new MatterEntity();
      for (MatterEbsStateEnum em : MatterEbsStateEnum.values()) {
        if (em.getCode().equals(matterRemoteVo.getState())) {
          matterEntity.setEnableState(em.getState());
          break;
        }
      }

      if(!EnableStateEnum.ENABLE.getState().equals(matterEntity.getEnableState()) || StringUtils.isEmpty(matterRemoteVo.getCode()) || StringUtils.isEmpty(matterRemoteVo.getOrgCode())){
        continue;
      }
      matterEntity.setCode(matterRemoteVo.getCode());
      matterEntity.setName(matterRemoteVo.getName());
      matterEntity.setMatterHostUnitCode(matterRemoteVo.getHostUnitCode());
      matterEntity.setMatterHostUnit(matterRemoteVo.getHostUnitName());
      matterEntity.setMatterAssistUnit(matterRemoteVo.getAssistUnitName());
      matterEntity.setMatterAssistUnitCode(matterRemoteVo.getAssistUnitCode());
      matterEntity.setMatterTypeCode(matterRemoteVo.getPackBigClassCode());
      matterEntity.setMatterTypeName(matterRemoteVo.getPackBigClassName());
      matterEntity.setMatterTypeSecondCode(matterRemoteVo.getPackMediumClassCode());
      matterEntity.setMatterTypeSecondName(matterRemoteVo.getPackMediumClassName());
      matterEntity.setMatterTypeThirdCode(matterRemoteVo.getPackSmallClassCode());
      matterEntity.setMatterTypeThirdName(matterRemoteVo.getPackSmallClassName());
      matterEntity.setSpecs(matterRemoteVo.getSpecs());
      matterEntity.setPiceWeight(matterRemoteVo.getPieceWeight());
      matterEntity.setFormInstanceId(UUID.randomUUID().toString());
      matterEntity.setLastUpdateTime(matterRemoteVo.getLastUpdateTime());
      matterEntity.setScaleFactor(matterRemoteVo.getConvertFactor() == null ? null : Double.toString(matterRemoteVo.getConvertFactor()));
      matterEntity.setCompanyName(matterRemoteVo.getCompanyName());
      matterEntity.setCompanyId(matterRemoteVo.getCompanyId());
      matterEntity.setCompanyCode(matterRemoteVo.getCompanyCode());
      matterEntity.setEbsCode(matterRemoteVo.getCode());
      matterEntity.setEbsOrgCode(matterRemoteVo.getOrgCode());
      matterEntity.setEbsOrgId(matterRemoteVo.getOrgId());
      matterEntity.setEbsOrgName(matterRemoteVo.getOrgName());
      MatterEntity oldMatter = matterEntityService.findByEbsCodeAndEbsOrgCode(matterEntity.getEbsCode(), matterEntity.getEbsOrgCode());
      if (oldMatter != null) {
        String id = oldMatter.getId();
        String code = oldMatter.getCode();
        Date createTime = oldMatter.getCreateTime();
        Integer aquiState = oldMatter.getAquiState();
        BeanUtils.copyProperties(matterEntity, oldMatter);
        oldMatter.setId(id);
        oldMatter.setCode(code);
        oldMatter.setModifyTime(new Date());
        oldMatter.setCreateTime(createTime);
        oldMatter.setAquiState(aquiState);
        matterEntityService.update(oldMatter);
      } else {
        matterEntity.setAquiState(0);
        matterEntityService.create(matterEntity);
      }
    }
    count = 100;
  }

}
