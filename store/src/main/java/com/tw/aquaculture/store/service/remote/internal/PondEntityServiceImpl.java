package com.tw.aquaculture.store.service.remote.internal;

import com.bizunited.platform.core.controller.model.ResponseModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.tw.aquaculture.common.entity.base.PondEntity;
import com.tw.aquaculture.common.utils.JsonUtil;
import com.tw.aquaculture.store.feign.process.PondFeign;
import com.tw.aquaculture.store.service.remote.PondEntityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

/**
 * PondEntity业务模型的服务层接口实现
 *
 * @author saturn
 */
@Service("PondEntityServiceImpl")
public class PondEntityServiceImpl implements PondEntityService {

  @Autowired
  private PondFeign pondFeign;

  @Transactional
  @Override
  public PondEntity create(PondEntity pondEntity) {
    ResponseModel responseModel = pondFeign.create(pondEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PondEntity>() {
    });
  }

  @Transactional
  @Override
  public PondEntity createForm(PondEntity pondEntity) {
    return null;
  }

  @Transactional
  @Override
  public PondEntity update(PondEntity pondEntity) {
    ResponseModel responseModel = pondFeign.update(pondEntity);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PondEntity>() {
    });
  }

  @Transactional
  @Override
  public PondEntity updateForm(PondEntity pondEntity) {
    return null;
  }

  @Override
  public PondEntity findDetailsById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = pondFeign.findDetailsById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PondEntity>() {
    });
  }

  @Override
  public PondEntity findById(String id) {
    if (StringUtils.isBlank(id)) {
      return null;
    }
    ResponseModel responseModel = pondFeign.findById(id);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PondEntity>() {
    });
  }

  @Override
  @Transactional
  public void deleteById(String id) {
    if (StringUtils.isBlank(id)) {
      return;
    }
    pondFeign.deleteById(id);
  }

  @Override
  public PondEntity findDetailsByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = pondFeign.findDetailsByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PondEntity>() {
    });
  }

  @Override
  public PondEntity findByFormInstanceId(String formInstanceId) {
    if (StringUtils.isBlank(formInstanceId)) {
      return null;
    }
    ResponseModel responseModel = pondFeign.findByFormInstanceId(formInstanceId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<PondEntity>() {
    });
  }

  @Override
  public Set<PondEntity> findDetailsByBase(String baseId) {
    if (StringUtils.isBlank(baseId)) {
      return null;
    }
    ResponseModel responseModel = pondFeign.findDetailsByBase(baseId);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<Set<PondEntity>>() {
    });
  }

  @Override
  public List<PondEntity> findPondsByBaseId(String baseId, int enableState) {
    if (StringUtils.isBlank(baseId)) {
      return null;
    }
    ResponseModel responseModel = pondFeign.findPondsByBaseId(baseId, enableState);
    return JsonUtil.parseResponseModel(responseModel, new TypeReference<List<PondEntity>>() {
    });
  }
} 
