package com.tw.aquaculture.store.service.remote.internal;

/**
 * 接口对接公共常量
 *
 * @author bo shi
 */
public abstract class RemoteCommonConstants {

  /** 系统默认标识 */
  public static final String DEFAULT_SYSTEM_FLAG = "SCYZ";

  /** 对接fbc系统时，本系统标识 */
  public static final String TO_FBC_SYSTEM_FLAG = DEFAULT_SYSTEM_FLAG;
}
