package com.tw.aquaculture.store.service.remote.internal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

/**
 * @author 陈荣
 * @date 2019/12/11 17:41
 */
@Component
public class RemoteUtil<T> {

  //VARCHAR2(50)	事务ID编号		是	必填，唯一值
  // NOT USE
//  private String BIZTRANSACTIONID;
  //	VARCHAR2(100)	调用此服务的系统标识		是	必填，如：SCYZ
  private static final String CONSUMER = RemoteCommonConstants.DEFAULT_SYSTEM_FLAG;
  //VARCHAR2(100)	服务消费系统所在板块标识		是	必填,如：饲料板块-水产养殖
  private static final String COMPANY = "饲料板块-水产养殖";
  //VARCHAR2(5)	服务级别		是	必填。值为1时，记录日志并落地报文；值为2时，记录调用日志但不落地报文；值为3时，不记录调用日志也不落地报文，默认服务级别为1
  private static final String SRVLEVEL = "1";
  //	VARCHAR2(30)	账户
  private String ACCOUNT = null;
  //	VARCHAR2(30)	密码
  private String PASSWORD = null;
  //VARCHAR2(30)	交易数量			必填，默认填0
  private String COUNT = "0";
  //VARCHAR2(240)	接口用途		是	必填
  private String USE = "";
  //VARCHAR2(240)	备注
  private String COMMENTS;


  @Autowired
  RestTemplate restTemplate;

  public String post(String url, T data){
    HttpHeaders header = new HttpHeaders();
    header.add("BIZTRANSACTIONID", UUID.randomUUID().toString());
    header.add("CONSUMER", CONSUMER);
    header.add("COMPANY", COMPANY);
    header.add("SRVLEVEL", SRVLEVEL);
    if(ACCOUNT!=null){
      header.add("ACCOUNT", ACCOUNT);
    }
    if(PASSWORD!=null){
      header.add("PASSWORD", PASSWORD);
    }
    header.add("COUNT", COUNT);
    header.add("USE", USE);
    header.add("COMMENTS", COMMENTS);
    header.setContentType(MediaType.APPLICATION_JSON_UTF8);
    HttpEntity<T> httpEntity = new HttpEntity<>(data, header);
    String response = restTemplate.postForEntity(url, httpEntity, String.class).getBody();
    return response;
  }

}
