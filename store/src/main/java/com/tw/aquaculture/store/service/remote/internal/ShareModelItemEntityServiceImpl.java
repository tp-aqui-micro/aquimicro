package com.tw.aquaculture.store.service.remote.internal;/*
package com.tw.aquaculture.manager.service.base.internal;

import com.bizunited.platform.kuiper.starter.service.KuiperToolkitService;
import com.google.common.collect.Sets;
import com.tongwei.aquaculture.service.entity.base.ShareModelItemEntity;
import com.tongwei.aquaculture.service.repository.base.ShareModelItemEntityRepository;
import com.tongwei.aquaculture.service.service.base.FishTypeEntityService;
import com.tongwei.aquaculture.service.service.base.ShareModelEntityService;
import com.tongwei.aquaculture.service.service.base.ShareModelItemEntityService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;

*/
/**
 * ShareModelItemEntity业务模型的服务层接口实现
 *
 * @author saturn
 * <p>
 * Kuiper表单引擎用于减少编码工作量的工具包
 * <p>
 * 在创建一个新的ShareModelItemEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
 * <p>
 * 在更新一个已有的ShareModelItemEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
 *//*

@Service("ShareModelItemEntityServiceImpl")
public class ShareModelItemEntityServiceImpl implements ShareModelItemEntityService {
  @Autowired
  private FishTypeEntityService fishTypeEntityService;
  @Autowired
  private ShareModelEntityService shareModelEntityService;
  @Autowired
  private ShareModelItemEntityRepository shareModelItemEntityRepository;
  */
/**
 * Kuiper表单引擎用于减少编码工作量的工具包
 *//*

  @Autowired
  private KuiperToolkitService kuiperToolkitService;
  @Transactional
  @Override
  public ShareModelItemEntity create(ShareModelItemEntity shareModelItemEntity) {
    ShareModelItemEntity current = this.createForm(shareModelItemEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  } 
  @Transactional
  @Override
  public ShareModelItemEntity createForm(ShareModelItemEntity shareModelItemEntity) {
   */
/*
 * 针对1.1.3版本的需求，这个对静态模型的保存操作做出调整，新的包裹过程为：
 * 1、如果当前模型对象不是主模型
 * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
 * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
 * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理
 * 2、如果当前模型对象是主业务模型
 *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
 *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
 *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
 * 2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
 *   2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
 *   2.3.2、以及验证每个分组的OneToMany明细信息
 * *//*

    this.createValidation(shareModelItemEntity);
    
    // ===============================
    //  和业务有关的验证填写在这个区域    
    // ===============================
    
    this.shareModelItemEntityRepository.save(shareModelItemEntity);
    
    // 返回最终处理的结果，里面带有详细的关联信息
    return shareModelItemEntity;
  }
  */
/**
 * 在创建一个新的ShareModelItemEntity模型对象之前，检查对象各属性的正确性，其主键属性必须没有值
 *//*

  private void createValidation(ShareModelItemEntity shareModelItemEntity) {
    Validate.notNull(shareModelItemEntity , "进行当前操作时，信息对象必须传入!!");
    // 判定那些不能为null的输入值：条件为 caninsert = true，且nullable = false
    Validate.isTrue(StringUtils.isBlank(shareModelItemEntity.getId()), "添加信息时，当期信息的数据编号（主键）不能有值！");
    shareModelItemEntity.setId(null);
    Validate.notNull(shareModelItemEntity.getFishState(), "添加信息时，是否主养鱼不能为空！");
    Validate.notNull(shareModelItemEntity.getRatio(), "添加信息时，比例不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK （注意连续空字符串的情况） 
  }
  @Transactional
  @Override
  public ShareModelItemEntity update(ShareModelItemEntity shareModelItemEntity) {
    ShareModelItemEntity current = this.updateForm(shareModelItemEntity);
    //==================================================== 
    //    这里可以处理第三方系统调用（或特殊处理过程）
    //====================================================
    return current;
  } 
  @Transactional
  @Override
  public ShareModelItemEntity updateForm(ShareModelItemEntity shareModelItemEntity) {
    */
/*
 * 针对1.1.3版本的需求，这个对静态模型的修改操作做出调整，新的过程为：
 * 1、如果当前模型对象不是主模型
 * 1.1、那么创建前只会验证基本信息，直接的ManyToOne关联（单选）和ManyToMany关联（多选）
 * 1.2、验证完成后，也只会保存当前对象的基本信息，直接的单选
 * TODO 1.3、ManyToMany的关联（多选），暂时需要开发人员自行处理（求删除、新增绑定的代码已生成）
 *
 * 2、如果当前模型对象是主业务模型
 *  2.1、创建前会验证当前模型的基本属性，单选和多选属性
 *  2.2、然后还会验证当前模型关联的各个OneToMany明细信息，调用明细对象的服务，明每一条既有明细进行验证
 *  （2.2的步骤还需要注意，如果当前被验证的关联对象是回溯对象，则不需要验证了）
 *  2.3、还会验证当前模型关联的各个OneToOne分组，调用分组对象的服务，对分组中的信息进行验证
 *    2.3.1、包括验证每一个分组项的基本信息、直接的单选、多选信息
 *    2.3.2、以及验证每个分组的OneToMany明细信息
 * *//*

    
    this.updateValidation(shareModelItemEntity);
    // ===================基本信息
    String currentId = shareModelItemEntity.getId();
    Optional<ShareModelItemEntity> op_currentShareModelItemEntity = this.shareModelItemEntityRepository.findById(currentId);
    ShareModelItemEntity currentShareModelItemEntity = op_currentShareModelItemEntity.orElse(null);
    currentShareModelItemEntity = Validate.notNull(currentShareModelItemEntity ,"未发现指定的原始模型对象信");
    // 开始重新赋值——一般属性
    currentShareModelItemEntity.setFishState(shareModelItemEntity.getFishState());
    currentShareModelItemEntity.setRatio(shareModelItemEntity.getRatio());
    currentShareModelItemEntity.setFishType(shareModelItemEntity.getFishType());
    currentShareModelItemEntity.setShareModel(shareModelItemEntity.getShareModel());
    
    this.shareModelItemEntityRepository.saveAndFlush(currentShareModelItemEntity);
    return currentShareModelItemEntity;
  }
  */
/**
 * 在更新一个已有的ShareModelItemEntity模型对象之前，该私有方法检查对象各属性的正确性，其id属性必须有值
 *//*

  private void updateValidation(ShareModelItemEntity shareModelItemEntity) {
    Validate.isTrue(!StringUtils.isBlank(shareModelItemEntity.getId()), "修改信息时，当期信息的数据编号（主键）必须有值！");
    
    // 基础信息判断，基本属性，需要满足not null
    Validate.notNull(shareModelItemEntity.getFishState(), "修改信息时，是否主养鱼不能为空！");
    Validate.notNull(shareModelItemEntity.getRatio(), "修改信息时，比例不能为空！");
    // 验证长度，被验证的这些字段符合特征: 字段类型为String，且不为PK，且canupdate = true
    
    // 关联性判断，关联属性判断，需要满足ManyToOne或者OneToOne，且not null 且是主模型
  } 
  @Override
  public Set<ShareModelItemEntity> findDetailsByFishType(String fishType) {
    if(StringUtils.isBlank(fishType)) { 
      return Sets.newHashSet();
    }
    return this.shareModelItemEntityRepository.findDetailsByFishType(fishType);
  }
  @Override
  public Set<ShareModelItemEntity> findDetailsByShareModel(String shareModel) {
    if(StringUtils.isBlank(shareModel)) { 
      return Sets.newHashSet();
    }
    return this.shareModelItemEntityRepository.findDetailsByShareModel(shareModel);
  }
  @Override
  public ShareModelItemEntity findDetailsById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    return this.shareModelItemEntityRepository.findDetailsById(id);
  }
  @Override
  public ShareModelItemEntity findById(String id) {
    if(StringUtils.isBlank(id)) { 
      return null;
    }
    
    Optional<ShareModelItemEntity> op = shareModelItemEntityRepository.findById(id);
    return op.orElse(null); 
  }
  @Override
  @Transactional
  public void deleteById(String id) {
    // 只有存在才进行删除
    Validate.notBlank(id , "进行删除时，必须给定主键信息!!");
    ShareModelItemEntity current = this.findById(id);
    if(current != null) { 
      this.shareModelItemEntityRepository.delete(current);
    }
  }
} 
*/
